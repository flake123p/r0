#!/bin/bash

g++ -std=c++17 demo.cpp

readelf -n  a.out

# Enabling/Customizing Build ID:
# You can enable or customize the Build ID generation with linker flags:

# --build-id:      Enables Build ID generation with default settings.
# --build-id=none: Disables Build ID generation.
# --build-id=sha1: Specifies a particular hash algorithm (e.g., md5, uuid, sha1).