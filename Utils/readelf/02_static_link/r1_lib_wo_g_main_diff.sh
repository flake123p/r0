#!/bin/bash

cd lib
g++ -c demo_lib.cpp -o libdemo_lib1.a
readelf -aCw libdemo_lib1.a | c++filt >1.log

g++ -g -c demo_lib.cpp -o libdemo_lib2.a
readelf -aCw libdemo_lib2.a | c++filt >2.log

# 1: lib wo g, main wo g
# 2: lib wo g, main w  g
# 3: lib w  g, main wo g
# 4: lib w  g, main w  g
# 5: lib wo g, mian w  g
# 6: lib w  g, main w  g
cd ..
g++ demo.cpp -I./lib -L./lib -ldemo_lib1 -o a1.out
readelf -aCw a1.out | c++filt >1.log

g++ -g demo.cpp -I./lib -L./lib -ldemo_lib1 -o a2.out
readelf -aCw a2.out | c++filt >2.log

meld 1.log 2.log