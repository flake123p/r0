#!/bin/bash

rm *.out -f
rm *.txt -f
rm *.log -f
rm lib/*.log -f
rm lib/*.a -f
rm lib/*.o -f