#!/bin/bash

cd lib
g++ -c demo_lib.cpp -o libdemo_lib1.a
readelf -aCw libdemo_lib1.a | c++filt >1.log

g++ -g -c demo_lib.cpp -o libdemo_lib2.a
readelf -aCw libdemo_lib2.a | c++filt >2.log

meld 1.log 2.log