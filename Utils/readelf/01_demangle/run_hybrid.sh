#!/bin/bash

g++ -std=c++17 -g demo.cpp

readelf -a  a.out>1.pure.log

readelf -a  a.out | c++filt >2.cppfilt.log

readelf -a -C a.out | c++filt>3.hybrid.log

meld 1.pure.log 2.cppfilt.log 3.hybrid.log