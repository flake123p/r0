#!/bin/bash

cd lib
g++ -c demo_lib.cpp -fPIC
g++ -shared -o libdemo_lib1.so demo_lib.o

g++ -g -c demo_lib.cpp -fPIC
g++ -shared -o libdemo_lib2.so demo_lib.o

# 1: lib wo g, main wo g
# 2: lib wo g, main w  g
# 3: lib w  g, main wo g
# 4: lib w  g, main w  g
# 5: lib wo g, mian w  g
# 6: lib w  g, main w  g
cd ..
g++ demo.cpp -I./lib -L./lib -ldemo_lib1 -o a1.out
readelf -aCw a1.out | c++filt >1.log

g++ -g demo.cpp -I./lib -L./lib -ldemo_lib1 -o a2.out
readelf -aCw a2.out | c++filt >2.log

meld 1.log 2.log

# export LD_LIBRARY_PATH=./lib
# ./a1.out