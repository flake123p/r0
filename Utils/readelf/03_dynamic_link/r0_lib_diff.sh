#!/bin/bash

cd lib

g++ -c demo_lib.cpp -fPIC
g++ -shared -o libdemo_lib1.so demo_lib.o
readelf -aCw libdemo_lib1.so | c++filt >1.log

g++ -g -c demo_lib.cpp -fPIC
g++ -shared -o libdemo_lib2.so demo_lib.o
readelf -aCw libdemo_lib2.so | c++filt >2.log

meld 1.log 2.log