
# LCOV_EXCL_START & LCOV_EXCL_STOP for block
e.g.:
    // LCOV_EXCL_START 
    default: 
        BASIC_ASSERT(0);
        break;
    // LCOV_EXCL_STOP


# LCOV_EXCL_LINE for a line
e.g.:
    default: // LCOV_EXCL_LINE
        BASIC_ASSERT(0); // LCOV_EXCL_LINE
        break; // LCOV_EXCL_LINE