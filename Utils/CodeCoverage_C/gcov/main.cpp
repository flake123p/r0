#include <stdio.h>

void bar(int n);

void foo(int n) 
{
    if (n > 0) {
        printf("foo n:%d bigger than 0\n", n);
    } else {
        printf("foo n:%d smaller than 0\n", n);
    }
}

int main()
{
    foo(3);
    bar(3);
}