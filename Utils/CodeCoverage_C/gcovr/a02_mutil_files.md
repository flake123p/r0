# For examples:
04
05


# Result of 04_two_files:
Home Page:
bar.cpp
main.cpp
| --        | Exec | Total | Coverage |
|-----------|------|-------|----------|
| Lines:    | 12   | 14    | 85.7%    |
| Branches: | 2    | 4     | 50.0%    |

bar.cpp
| --        | Exec | Total | Coverage |
|-----------|------|-------|----------|
| Lines:    | 4    | 7     | 57.1%    |
| Branches: | 1    | 4     | 25.0%    |

main.cpp
| --        | Exec | Total | Coverage |
|-----------|------|-------|----------|
| Lines:    | 8    | 9     | 88.9%    |
| Branches: | 1    | 2     | 50.0%    |



# Result of 05_two_files_w_filter:
bar.cpp
| --        | Exec | Total | Coverage |
|-----------|------|-------|----------|
| Lines:    | 4    | 7     | 57.1%    |
| Branches: | 1    | 4     | 25.0%    |

## 1. Create filter.cfg:
filter = bar.cpp
json = yes

## 2. use it in gcovr command

    mkdir ./coverage_local -p

    gcovr --config filter.cfg > filter.json

    gcovr --add-tracefile filter.json -r . --html --html-details -o ./coverage_local/coverage.html
