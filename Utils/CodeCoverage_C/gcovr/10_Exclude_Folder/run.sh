rm *.gcda -rf
rm *.gcno -rf

# ./01_compile_n_gen_gcno.sh
g++ -fprofile-arcs -ftest-coverage main.cpp src/BAR/bar.cpp src/FOO/foo.cpp

# ./02_run_n_gen_gcda_n_gcovr.sh
./a.out
# gcovr

# ./03_gen_coverage_html.sh
echo
echo gcovr --exclude='.*BAR.*' -r . 
gcovr --exclude='.*BAR.*' -r . 

echo
echo gcovr --exclude='.*BAR.*' --exclude='.*FOO.*' -r . 
gcovr --exclude='.*BAR.*' --exclude='.*FOO.*' -r . 

echo
echo gcovr --exclude='.*src.*' -r . 
gcovr --exclude='.*src.*' -r . 

echo
echo gcovr --exclude='.*BAR.*|.*FQQ.*' -r . 
gcovr --exclude='.*BAR.*|.*FQQ.*' -r . 

echo
echo gcovr --config config.conf
gcovr --config config.conf
