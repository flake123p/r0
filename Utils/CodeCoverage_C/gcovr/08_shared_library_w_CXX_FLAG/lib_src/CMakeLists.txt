cmake_minimum_required(VERSION 3.6)
project(bar)

set(BUILD_SHARED_LIBS ON) # Flake: keep this at top

message("CMAKE_INSTALL_PREFIX = ${CMAKE_INSTALL_PREFIX}")
message("USE_EXTERNAL_INSTALL_PREFIX = ${USE_EXTERNAL_INSTALL_PREFIX}")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fprofile-arcs -ftest-coverage")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fprofile-arcs -ftest-coverage")

if(DEFINED USE_EXTERNAL_INSTALL_PREFIX)
else()
    set(CMAKE_INSTALL_PREFIX ${PROJECT_SOURCE_DIR})
endif()

#set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS YES CACHE BOOL "Export all symbols")

add_library(bar bar.c car.c)
set_target_properties(
    bar PROPERTIES
    PUBLIC_HEADER 
    bar.h
)
    
install(
    TARGETS bar
    LIBRARY DESTINATION lib_local
    ARCHIVE DESTINATION lib_local
    RUNTIME DESTINATION bin
    PUBLIC_HEADER DESTINATION inc_local
)