# 1. cd lib_src
# 2. mkdir
# 3. cmake ..
# 4. make
# 5. make install

bash clean.sh

cur=$(pwd)
cd lib_src
bash build.sh
cd $cur

#
# No cov gcc flag in main.c building in 08 case.
#

# gcc -fprofile-arcs -ftest-coverage -o a.out -Ilib_src/inc_local main.c -lbar -Llib_src/lib_local
gcc -o a.out -Ilib_src/inc_local main.c -lbar -Llib_src/lib_local

export LD_LIBRARY_PATH=$(pwd)/lib_src/lib_local

./a.out

mkdir coverage_local -p
gcovr
