# For examples:
06
07


# Default config name for gcovr
gcovr.cfg


# Use config file

    gcovr --config my_gcovr.cfg


# Use configs

    gcovr --filter="lib_src"
or
    gcovr --config my_gcovr.cfg -r . --html --html-details -o ./coverage_local/coverage.html


# Config file example (exclude)
    root = .
    exclude = lib_src
    exclude = lib_src2
    exclude = lib_src3
    html = yes
    html-details = yes
    output = ./coverage_local/coverage.html


# Config file example (include)
    root = .
    filter = lib_src
    filter = lib_src2
    filter = lib_src3
    html = yes
    html-details = yes
    output = ./coverage_local/coverage.html