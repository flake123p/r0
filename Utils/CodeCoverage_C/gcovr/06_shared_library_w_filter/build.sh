# 1. cd lib_src
# 2. mkdir
# 3. cmake ..
# 4. make
# 5. make install

bash clean.sh

cur=$(pwd)
cd lib_src
bash build.sh
cd $cur

gcc -fprofile-arcs -ftest-coverage -o a.out -Ilib_src/inc_local main.c -lbar -Llib_src/lib_local
# gcc -o a.out -Ilib_src/inc_local main.c -lbar -Llib_src/lib_local

export LD_LIBRARY_PATH=$(pwd)/lib_src/lib_local

./a.out

gcovr --filter="lib_src"

mkdir coverage_local -p
# gcovr --config filter.cfg > filter_local.json
# gcovr --add-tracefile filter_local.json -r . --html --html-details -o ./coverage_local/coverage.html
gcovr --config my_gcovr.cfg