rm *.gcda -rf
rm *.gcno -rf

# ./01_compile_n_gen_gcno.sh
g++ -fprofile-arcs -ftest-coverage main.cpp bar.cpp foo.cpp

# ./02_run_n_gen_gcda_n_gcovr.sh
./a.out
# gcovr

# ./03_gen_coverage_html.sh
mkdir ./coverage_local -p

#
# With config
#
gcovr --config config.conf --txt coverage_w_config_local.log --html --html-details -o ./coverage_local/coverage.html
# gcovr --config config.json -r . --html --html-details -o ./coverage_local/coverage.html

#
# Without config
#
gcovr --txt coverage_wo_config_local.log --html --html-details -o ./coverage_local/coverage.html

