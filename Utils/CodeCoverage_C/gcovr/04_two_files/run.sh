
# ./01_compile_n_gen_gcno.sh
g++ -fprofile-arcs -ftest-coverage main.cpp

# ./02_run_n_gen_gcda_n_gcovr.sh
./a.out
gcovr

# ./03_gen_coverage_html.sh
mkdir ./coverage_local -p
gcovr -r . --html --html-details -o ./coverage_local/coverage.html
