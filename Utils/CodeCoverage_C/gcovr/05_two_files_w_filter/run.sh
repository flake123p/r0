
# ./01_compile_n_gen_gcno.sh
g++ -fprofile-arcs -ftest-coverage main.cpp bar.cpp

# ./02_run_n_gen_gcda_n_gcovr.sh
./a.out
gcovr

# ./03_gen_coverage_html.sh
mkdir ./coverage_local -p
gcovr --config filter.cfg > filter_local.json
gcovr --add-tracefile filter_local.json -r . --html --html-details -o ./coverage_local/coverage.html
