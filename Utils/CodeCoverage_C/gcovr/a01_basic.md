ChatGPT :　Simple Usage of gcovr

# For examples:
00
01
02


# 1. Install gcovr

    pip install gcovr

  **Flake: sudo apt install gcovr**



#　2. Compile Your Code (gen .gcno)
Ensure your code is compiled with coverage options enabled:

-fprofile-arcs:  Enables branch coverage data.
-ftest-coverage: Enables test coverage data.

For example:

    gcc -fprofile-arcs -ftest-coverage -o my_program my_program.c

This will generate .gcno files during compilation, which record code coverage information.

  **Flake: use both or profile nothing!!!**
  **Flake: set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fprofile-arcs -ftest-coverage")**



# 3. Run Your Tests (gen .gcda)
Run the compiled program to execute your tests. This will generate .gcda files, which record executed code blocks. For example:

    ./my_program



# 4. Generate Coverage Reports with gcovr
## Text-Based Coverage Report
Run the following command to generate a simple coverage summary:

    gcovr

## Detailed HTML Report
Generate a visual HTML report:

    mkdir coverage_local -p
    gcovr -r . --html --html-details -o ./coverage_local/coverage.html

-r .            specifies the root directory of your project.
--html          enables HTML report generation.
--html-details  includes detailed line-by-line coverage in the report.

## Generate an XML Report
If you want to integrate with tools like SonarQube, you can generate an XML report:

    gcovr -r . --xml -o coverage.xml



# 5. Common Options
| Option         | Description                                                   |
|----------------|---------------------------------------------------------------|
| -r or --root   | Specifies the root directory of the project.                  |
| --html         | Generates a basic HTML report.                                |
| --html-details | Generates a detailed HTML report.                             |
| --xml          | Generates an XML format report.                               |
| --exclude      | Excludes directories or files (supports regex).               |
| --include      | Includes only specific directories or files (supports regex). |



# Example Workflow

## 1. Compile your code
    
    gcc -fprofile-arcs -ftest-coverage -o test_program test_program.c

## 2. Run your tests
    
    ./test_program

## 3. Generate a coverage report

    gcovr -r . --html --html-details -o coverage.html

After completing these steps, open coverage.html in your browser to view the detailed coverage information.



# Ref:
Statement Coverage : line count (exclude variables declaration)
Branch Coverage    : branch count
Function Coverage  : function count