#include <stdio.h>

void foo(int n) 
{
    if (n > 0) {
        printf("n:%d bigger than 0\n", n);
    } else {
        printf("n:%d smaller than 0\n", n);
    }
}

int main()
{
    foo(3);
    foo(-3); // 100% coverage
}