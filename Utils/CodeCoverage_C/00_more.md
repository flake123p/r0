
# reset & flush
https://stackoverflow.com/questions/39368611/disable-gcov-coverage-at-run-time
extern "C" void __gcov_dump(void);                                              
extern "C" void __gcov_reset(void);
extern "C" void __gcov_flush(void);
__gcov_reset();
__gcov_flush();


# dump
https://gcc.gnu.org/onlinedocs/gcc/Gcov-and-Optimization.html#Gcov-and-Optimization
__gcov_dump();


# GCOVR_EXCL_START, GCOVR_EXCL_STOP
https://gcovr.com/en/stable/guide/exclusion-markers.html