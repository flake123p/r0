
# list info summary in text format (for remove)

lcov --list filtered_coverage_v2.info


# Remove Key Notes:

Use ./* for the Root Folder: When using --remove, the ./ prefix indicates the root folder.

Wildcards Are Your Friend: Use patterns like ./folder/* to match specific directories within the root.

Check Before Excluding: Use lcov --list on your original .info file to confirm which files and directories are present and need exclusion.