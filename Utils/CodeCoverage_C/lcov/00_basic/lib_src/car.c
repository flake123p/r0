#include <stdio.h>

void car(int n) 
{
    if (n > 0) {
        printf("car n:%d bigger than 0\n", n);
    } else if (n == 0){
        printf("car n:%d equal 0\n", n);
    } else {
        printf("car n:%d smaller than 0\n", n);
    }
}
