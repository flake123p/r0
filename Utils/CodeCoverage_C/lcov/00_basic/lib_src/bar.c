#include <stdio.h>

void bar(int n) 
{
    if (n > 0) {
        printf("bar n:%d bigger than 0\n", n);
    } else if (n == 0){
        printf("bar n:%d equal 0\n", n);
    } else {
        printf("bar n:%d smaller than 0\n", n);
    }
}
