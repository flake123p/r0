
bash clean.sh

cur=$(pwd)
cd lib_src
bash build.sh
cd $cur

find . -name '*.gcda' -delete

gcc -fprofile-arcs -ftest-coverage -o a.out -Ilib_src/inc_local main.c -lbar -Llib_src/lib_local

export LD_LIBRARY_PATH=$(pwd)/lib_src/lib_local

# lcov --directory . --capture --initial --output-file baseline.info

./a.out

lcov --directory . --capture --output-file coverage.info

lcov --remove coverage.info '/usr/*' '*/test/*' --output-file filtered_coverage.info

genhtml filtered_coverage.info --output-directory coverage_report_v4_local