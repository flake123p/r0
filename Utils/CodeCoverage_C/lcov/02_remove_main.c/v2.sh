
bash clean.sh

cur=$(pwd)
cd lib_src
bash build.sh
cd $cur

find . -name '*.gcda' -delete

gcc -fprofile-arcs -ftest-coverage -o a.out -Ilib_src/inc_local main.c -lbar -Llib_src/lib_local

export LD_LIBRARY_PATH=$(pwd)/lib_src/lib_local

lcov --directory . --capture --initial --output-file baseline.info

./a.out

lcov --directory . --capture  --output-file coverage.info
lcov --add-tracefile baseline.info --add-tracefile coverage.info --output-file combined.info

#
# REMOVE main folder, proposed by ChatGPT and not working...
#
# lcov --remove combined.info './*' --output-file filtered_coverage_v2.info

lcov --extract combined.info '*/lib_src/*' --output-file filtered_coverage_v2.info

genhtml filtered_coverage_v2.info --output-directory coverage_report_v2_local
