#include <stdio.h>
#include "bar.h"

void foo(int n) 
{
    if (n > 0) {
        printf("foo n:%d bigger than 0\n", n);
    } else {
        printf("foo n:%d smaller than 0\n", n);
    }
}

int main(int argc, char *argv[])
{
    printf("argc = %d\n", argc);

    if (argc == 2) {
        bar(0);
    } else {
        foo(3);
        bar(3);
        car(3);
    }
    return 0;
}