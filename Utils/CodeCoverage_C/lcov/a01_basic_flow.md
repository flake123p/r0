ChatGPT

# 1. Install lcov
On most Linux distributions, lcov can be installed via the package manager:

For Debian/Ubuntu:

    sudo apt-get install lcov

For Red Hat/Fedora:

    sudo dnf install lcov



# 2. Compile Code with Coverage Options
To use lcov, compile your code with the following GCC flags:

-fprofile-arcs:  Instruments the code for branch coverage.
-ftest-coverage: Enables test coverage analysis.

Example:

    gcc -fprofile-arcs -ftest-coverage -o my_program my_program.c

This generates:
.gcno files: Contain coverage metadata.


# 3. Run Your Program
Execute the compiled program to generate .gcda files that track runtime execution:

    ./my_program

The .gcda files are created in the same directory as the .gcno files and contain the execution counts.


# 4. Capture Initial Coverage Data
Before running tests or the program, use lcov to capture the baseline coverage (usually zero coverage). 

The baseline coverage (--initial) captures all executable lines in your source code (as defined by .gcno files) but marks them as not executed. This helps track which lines have not yet been covered during your test runs.

    lcov --directory . --capture --initial --output-file baseline.info

--directory : Specifies the directory containing .gcno and .gcda files.
--capture   : Captures coverage data.
--initial   : Marks all executable lines as not executed (baseline).
--output-file baseline.info : Saves the baseline data to a file.


# 5. Run Your Tests
After capturing the baseline, execute your program or tests:

    ./my_program


# 6. Capture Coverage Data & Combine Baseline
Use lcov again to capture the coverage data after running your program:

    lcov --directory . --capture --output-file coverage.info
    lcov --add-tracefile baseline.info --add-tracefile coverage.info --output-file combined.info

coverage.info contains the actual coverage data.


# 7. Filter Out Unwanted Coverage Data
Remove coverage data for external libraries, headers, or other irrelevant files using the --remove option:

    lcov --remove combined.info '/usr/*' '*/test/*' --output-file filtered_coverage.info

This excludes:
    Files in /usr/ (e.g., standard libraries).
    Files in the test/ directory.


# 8. Generate HTML Report
Convert the .info file to an HTML report for easy visualization:

    genhtml filtered_coverage.info --output-directory coverage_report

The HTML report will be saved in the coverage_report directory.
Open the index.html file in your browser to view the coverage details.


# 9. Complete Workflow Example

    gcc -fprofile-arcs -ftest-coverage -o my_program my_program.c

    lcov --directory . --capture --initial --output-file baseline.info

    ./my_program

    lcov --directory . --capture --output-file coverage.info
    lcov --add-tracefile baseline.info --add-tracefile coverage.info --output-file combined.info

    lcov --remove combined.info '/usr/*' '*/test/*' --output-file filtered_coverage.info

    genhtml filtered_coverage.info --output-directory coverage_report


# 10. Common Commands
| Command/Option       | Description                                        |
|----------------------|----------------------------------------------------|
| --capture            | Captures coverage data from .gcda and .gcno.       |
| --directory <path>   | Specifies the directory containing coverage files. |
| --initial            | Captures baseline (initial) coverage.              |
| --remove <patterns>  | Excludes files matching the specified patterns.    |
| --output-file <file> | Saves coverage data to a specified file.           |
| genhtml <info_file>  | Converts .info data to an HTML report.             |



# 11. Tips and Best Practices

## Clean Old Coverage Files: 
Remove stale .gcda and .gcno files before running new tests:

    find . -name '*.gcda' -delete

## Combine Info Files: 
If you have multiple test runs, combine their results:

    lcov --add-tracefile coverage1.info --add-tracefile coverage2.info --output-file combined.info

## Automate with CI:
Integrate lcov and genhtml into your CI/CD pipeline to generate coverage reports after every build.