
module adder_tb;
    logic [3:0] a, b;
    logic [3:0] sum;

    adder uut(.a(a), .b(b), .sum(sum));

    initial begin
        $display("Time | a   | b   | sum");
        $display("%4t | %b | %b | %b", $time, a, b, sum);

        a = 4'b0001; b = 4'b0010; #10;
        a = 4'b0110; b = 4'b0011; #10;
        a = 4'b1111; b = 4'b0001; #10;

        $finish;
    end
endmodule