// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vadder_tb.h for the primary calling header

#include "Vadder_tb.h"
#include "Vadder_tb__Syms.h"

//==========

VL_CTOR_IMP(Vadder_tb) {
    Vadder_tb__Syms* __restrict vlSymsp = __VlSymsp = new Vadder_tb__Syms(this, name());
    Vadder_tb* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    _ctor_var_reset();
}

void Vadder_tb::__Vconfigure(Vadder_tb__Syms* vlSymsp, bool first) {
    if (false && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
    if (false && this->__VlSymsp) {}  // Prevent unused
    Verilated::timeunit(-12);
    Verilated::timeprecision(-12);
}

Vadder_tb::~Vadder_tb() {
    VL_DO_CLEAR(delete __VlSymsp, __VlSymsp = NULL);
}

void Vadder_tb::_settle__TOP__1(Vadder_tb__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vadder_tb::_settle__TOP__1\n"); );
    Vadder_tb* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->adder_tb__DOT__sum = (0xfU & ((IData)(vlTOPp->adder_tb__DOT__a) 
                                          + (IData)(vlTOPp->adder_tb__DOT__b)));
}

void Vadder_tb::_initial__TOP__2(Vadder_tb__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vadder_tb::_initial__TOP__2\n"); );
    Vadder_tb* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    VL_WRITEF("Time | a   | b   | sum\n");
    VL_WRITEF("%4t | %b | %b | %b\n",64,VL_TIME_UNITED_Q(1),
              4,(IData)(vlTOPp->adder_tb__DOT__a),4,
              vlTOPp->adder_tb__DOT__b,4,(IData)(vlTOPp->adder_tb__DOT__sum));
    vlTOPp->adder_tb__DOT__a = 0xfU;
    vlTOPp->adder_tb__DOT__b = 1U;
    VL_FINISH_MT("adder_tb.sv", 16, "");
}

void Vadder_tb::_eval_initial(Vadder_tb__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vadder_tb::_eval_initial\n"); );
    Vadder_tb* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_initial__TOP__2(vlSymsp);
}

void Vadder_tb::final() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vadder_tb::final\n"); );
    // Variables
    Vadder_tb__Syms* __restrict vlSymsp = this->__VlSymsp;
    Vadder_tb* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void Vadder_tb::_eval_settle(Vadder_tb__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vadder_tb::_eval_settle\n"); );
    Vadder_tb* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_settle__TOP__1(vlSymsp);
}

void Vadder_tb::_ctor_var_reset() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vadder_tb::_ctor_var_reset\n"); );
    // Body
    adder_tb__DOT__a = VL_RAND_RESET_I(4);
    adder_tb__DOT__b = VL_RAND_RESET_I(4);
    adder_tb__DOT__sum = VL_RAND_RESET_I(4);
}
