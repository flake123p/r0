
# verilator seems can't support delay statement
https://www.reddit.com/r/FPGA/comments/ruavh6/verilator_seems_cant_support_delay_statement/

Yes, Verilator can only simulate synthesizable code, therefore you cannot write a testbench for verilator in HDL, 
instead you'll usually write it in C++ (native) or Python (cocotb) where you can implement delays and other 
non-synthesizable testbench constructs.

---

Verilator DOES support delay statements in recent versions. There seems to be some misunderstanding online, 
but it is clearly said in the verilator documentation that you need to add the "--timing" command line argument.

https://veripool.org/guide/latest/languages.html

This is a nice tutorial with a proof of concept.

https://lsifrontend.hatenablog.com/entry/2022/11/14/150102