function [dispatchedAry] = DispatchPacketByPSR(psrAry,pktNum)
    len = length(psrAry);
    %dispatchedAry = zeros(len,1);
    s = sum(psrAry);
    dispatchedAry = psrAry * pktNum / s + 0.0001;
    dispatchedAry = floor(dispatchedAry)
    integerLeft = pktNum - sum(dispatchedAry);
    if integerLeft > 1
        disp('Packet Number Error!!!!!!!!!!!!!!!');
        disp(integerLeft);
    end
    [M,I] = max(psrAry);
    dispatchedAry(I) = dispatchedAry(I) + integerLeft;
end

