function [finishedCntr] = InterleaveExp(perAry, dispatchedAry, pktMax)
    finishedCntr = 0;
    pktAry = dispatchedAry;
    completePktCntr = 0; % to pktMax
    for i = 1 : 1000000
        for j = 1 : length(pktAry)
            if (pktAry(j) > 0)
                if rand(1) > perAry(j)
                    % transmit successfully
                    completePktCntr = completePktCntr + 1;
                    pktAry(j) = pktAry(j) - 1;
                else
                    % transmit failed
                end
            end
        end
        % final check
        finishedCntr = finishedCntr + 1;
        if completePktCntr == pktMax
            break;
        end
    end
end

