
https://blog.csdn.net/euzmin/article/details/128546070
https://home.gamer.com.tw/creationDetail.php?sn=571356

Theorem：定理。是⽂章中重要的數學化的論述，⼀般都有嚴格的數學證明。

Theory：定理(理論)。相較於Theorem，其往往不需要嚴格的數學證明。當我們想提出⼀個理論，⽽理論
無法抽象化成⼀個完備的數學問題，就應該⽤Theory ⽽不是Theorem。

Proposition：命題。相較於Theorem 和Theory，如果⼀個理論只能⽤於特定的問題且無法泛化到普
遍情況，則⽤Proposition。

Lemma：引理。⼀般提出Lemma 是為了進⼀步提出Theorem，即Lemma … Theorem …。

Corollary：推論。由Theorem推出來的結論，通常我們會直接說this is a corollary of Theorem A，即

Theorem … Corollary …，可以與Lemma 對⽐記憶。

Claim：主張。作者認為不那麼重要，但是需要聲明的理論or 結果。