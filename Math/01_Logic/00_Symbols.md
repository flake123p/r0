# Book
Introduction to Mathematical Thinking ... by Keith Devlin
http://www.mat.ufrgs.br/~portosil/curso-Devlin.pdf

# Symbols
https://en.wikipedia.org/wiki/List_of_logic_symbols


# Imply
⇒ : implies, if ... then ...,

→

⊃ : may mean the same as ⇒ (the symbol may also mean superset).


# If and only if, iff, xnor
⇔
↔
≡

Example: x + 5 = y + 2 ⇔ x + 3 = y

# Not
¬
~
!

# And
∧
·
&

# Or
∨
+
∥

# Xor
⊻
⊕
↮
≢

# Truth (top)
⊤
T
1

# Falsity (bottom)
⊥
F
0

# For all

∀   : given any, for all, for every, for each, for any
()  : same

# Exist
∃  : there exists, for some
∃! : there exists exactly one