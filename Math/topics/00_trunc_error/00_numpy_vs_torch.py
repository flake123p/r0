import math
from math import pow, log2, log10
import torch

for i in range(10):
    val = float(i+1)
    L1 = pow(10, log10(val))
    L2 = pow(10, log10(L1))
    L3 = pow(10, log10(L2))
    print(f'numpy(f64): {L3:010.40f}')

for i in range(10):
    val = torch.tensor(i+1).type(torch.float32)
    L1 = torch.pow(10, torch.log10(val))
    L2 = torch.pow(10, torch.log10(L1))
    L3 = torch.pow(10, torch.log10(L2))
    print(f'torch f32 : {L3:010.40f}')

for i in range(10):
    val = torch.tensor(i+1).type(torch.float64)
    L1 = torch.pow(10, torch.log10(val))
    L2 = torch.pow(10, torch.log10(L1))
    L3 = torch.pow(10, torch.log10(L2))
    print(f'torch f32 : {L3:010.40f}')