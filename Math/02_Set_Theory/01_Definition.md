
# Proper subset
https://www.geeksforgeeks.org/proper-subset/

# Definition of Proper Subset
For two sets A and B, A is said to be a proper subset of B i.e., A ⊂ B; if and only if for every element x in A, x is also in B, and there exists at least one element y in B such that y is not in A.

Which can be written in mathematical notation as follows:

∀x (x ∈ A → x ∈ B) ∧ ∃y (y ∈ B ∧ y ∉ A)