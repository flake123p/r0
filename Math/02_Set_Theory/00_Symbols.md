
Mathematical and scientific symbols
https://www.uefap.com/speaking/symbols/symbols.htm

# Satisfy
{x | A(x)} :  If A(x) is some property, the set of all those x which satisfy A(x)

{x ∈ X | A(x)} : the set of all x in X such that A(X)
               : restrict the x to those which are members of a certain set X ...

# Belongs to
∈ : belongs to; a member of;  an element of
∉ : does not belong to; is not a member of; is not an element of

# Contain
⊂ : contained in;  a proper subset of
⊆ : contained in; subset 

# Interscetion
⋂

# Union
⋃

# For all
∀

//
// https://www.cuemath.com/algebra/superset/
//

# Proper Superset (Strictly Superset)
⊃ : Proper superset is represented by '⊃' (where X⊃Y but X is strictly NOT equal to Y)
⊃, which is read as "superset of". This means it represents "strictly superset of" but NOT "equal to".
A ⊃ B, A is superset

# Superset (Improper Superset)
⊇ : Improper superset is represented by '⊇' (Where X may or may not be equal to Y)

# Proper Subset
⊂
A ⊂ B, A is subset

# Subset (Improper Subset)
⊆