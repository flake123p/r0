/*
	Web Resource:
	http://stackoverflow.com/questions/2844/how-do-you-printf-an-unsigned-long-long-intthe-format-specifier-for-unsigned-lo

	Single Float(WIKI):
	https://en.wikipedia.org/wiki/Single-precision_floating-point_format
	sign : 1
	exponent : 8 (-127 for real)
	fraction : 23 (Mantissa)
	inf = exponent = 128(255-127)

	Double Float(WIKI):
	https://en.wikipedia.org/wiki/Single-precision_floating-point_format
	sign : 1
	exponent : 11
	fraction : 53 (Mantissa)

	Ref:
	https://ithelp.ithome.com.tw/articles/10266532
		Kahan Summation
		Precision
		Accuracy
	https://chi_gitbook.gitbooks.io/personal-note/content/floating_point_basic.html
*/

#include <stdio.h>
#include <stdint.h>
#include "My_Basics.h"

typedef struct {
	union {
		float val_f32;
		uint32_t val_u32;
		struct {
			uint32_t sign : 1;
			uint32_t expo : 8; // 127(0x3F) for 0
			uint32_t frac : 23;
		} be; //big endian
		struct {
			uint32_t frac : 23;
			uint32_t expo : 8; // 127(0x3F) for 0
			uint32_t sign : 1;
		} le; //little endian
	};
} single_fp_t;

void dump_single_fp(float val, const char *name)
{
	single_fp_t fp;
	fp.val_f32 = val;
	printf("\n[FLOAT Dump]: \"%s\" :\n", name);
	printf("f32 = %.30f\n", fp.val_f32);
	printf("u32 = %u, (0x%08X)\n", fp.val_u32, fp.val_u32);
	if (IS_BIG_ENDIAN()) {
		printf("be.sign = %u\n", fp.be.sign);
		printf("be.expo = 0x%02X (%d) (%d)\n", fp.be.expo, (int)fp.be.expo, (int)fp.be.expo-127);
		printf("be.frac = 0x%06X\n", fp.be.frac);
	} else {
		printf("le.sign = %u\n", fp.le.sign);
		printf("le.expo = 0x%02X (%d) (%d)\n", fp.le.expo, (int)fp.le.expo, (int)fp.le.expo-127);
		printf("le.frac = 0x%06X\n", fp.le.frac);
	}
}
#define DUMP_SINGLE_FP(a) dump_single_fp(a,#a)

int main(int argc, char *argv[])
{
	DUMP_SINGLE_FP(10000.0 + 3.123456789);
	DUMP_SINGLE_FP(10003.123456789);

	float c = ((10000.0 + 3.123456789) - 10000.0) - 3.123456789;
	DUMP_SINGLE_FP(c);
	DUMP_SINGLE_FP(10003.123456789 + c); //no use ...
	printf("no use ........................\n");

	return 0;
}
