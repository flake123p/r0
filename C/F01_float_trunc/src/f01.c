
/*
	Web Resource:
	http://stackoverflow.com/questions/2844/how-do-you-printf-an-unsigned-long-long-intthe-format-specifier-for-unsigned-lo

	Single Float(WIKI):
	https://en.wikipedia.org/wiki/Single-precision_floating-point_format
	sign : 1
	exponent : 8
	fraction : 23

	Double Float(WIKI):
	https://en.wikipedia.org/wiki/Single-precision_floating-point_format
	sign : 1
	exponent : 11
	fraction : 53
*/

#include <stdio.h>
#include <stdint.h>

int main(int argc, char *argv[])
{
	float a = (float)0x00000001;
	double b = (double)0x0000000000000001;

	printf("with casting 0x00000001:\n");
	printf("a = %.16f\n", a);
	printf("b = %.16f\n", b);

	printf("\nwith division - 1.0 / 0xFFFFFFFF:\n");
	a = 1.0 / 0xFFFFFFFF;
	b = 1.0 / 0xFFFFFFFF;
	printf("a = %.16f\n", a);
	printf("b = %.16f\n", b);

	printf("\nwith division - 1.0 / 0xFFFFFFFFFFFFFFFF:\n");
	a = 1.0 / 0xFFFFFFFFFFFFFFFF;
	b = 1.0 / 0xFFFFFFFFFFFFFFFF;
	printf("a = %.16f\n", a);
	printf("b = %.16f\n", b);

	printf("\nwith division - 1.0 / 1000000.0:\n");
	a = 1.0 / 1000000.0;
	b = 1.0 / 1000000.0;
	printf("a = %.32f\n", a);
	printf("b = %.32f\n", b);

	printf("\nwith assign - 0.0000001:\n");
	a = 0.0000001;
	b = 0.0000001;
	printf("a = %.32f\n", a);
	printf("b = %.32f\n", b);

	printf("\nwith assign - 0.0000000002328306:\n");
	a = 0.0000000002328306;
	b = 0.0000000002328306;
	printf("a = %.16f (%x)\n", a, *((uint32_t *)&a));
	printf("b = %.16f (%lx)\n", b, *((uint64_t *)&b));

	printf("\nwith assign - 0.3:\n");
	a = 0.3;
	b = 0.3;
	printf("a = %.16f (%x)\n", a, *((uint32_t *)&a));
	printf("b = %.16f (%lx)\n", b, *((uint64_t *)&b));

	printf("\nwith assign - -0.3:\n");
	a = -0.3;
	b = -0.3;
	printf("a = %.16f (%x)\n", a, *((uint32_t *)&a));
	printf("b = %.30f (%lx)\n", b, *((uint64_t *)&b));

	return 0;
}
