/*
	Purpose: 
		Hook function at beginning, irrelevant to following flow.
*/
#include <stdio.h>

int abcc(int a, int b, int c)
{
    return (a == 0 || b == 0 && c == 0);
}

int abxx(int a, int b, int c)
{
    return (a == 0 || (b == 0 && c == 0));
}

int abzz(int a, int b, int c)
{
    return ((a == 0 || b == 0) && c == 0);
}

int main(int argc, char *argv[])
{
    int a, b, c;

    a = 0; b = 0; c = 0;    printf("abcc = %d, abxx = %d, abzz = %d\n", abcc(a, b, c), abxx(a, b, c), abzz(a, b, c));
    a = 0; b = 0; c = 1;    printf("abcc = %d, abxx = %d, abzz = %d\n", abcc(a, b, c), abxx(a, b, c), abzz(a, b, c));
    a = 0; b = 1; c = 0;    printf("abcc = %d, abxx = %d, abzz = %d\n", abcc(a, b, c), abxx(a, b, c), abzz(a, b, c));
    a = 0; b = 1; c = 1;    printf("abcc = %d, abxx = %d, abzz = %d\n", abcc(a, b, c), abxx(a, b, c), abzz(a, b, c));
    a = 1; b = 0; c = 0;    printf("abcc = %d, abxx = %d, abzz = %d\n", abcc(a, b, c), abxx(a, b, c), abzz(a, b, c));
    a = 1; b = 0; c = 1;    printf("abcc = %d, abxx = %d, abzz = %d\n", abcc(a, b, c), abxx(a, b, c), abzz(a, b, c));
    a = 1; b = 1; c = 0;    printf("abcc = %d, abxx = %d, abzz = %d\n", abcc(a, b, c), abxx(a, b, c), abzz(a, b, c));
    a = 1; b = 1; c = 1;    printf("abcc = %d, abxx = %d, abzz = %d\n", abcc(a, b, c), abxx(a, b, c), abzz(a, b, c));

    return 0;
}
