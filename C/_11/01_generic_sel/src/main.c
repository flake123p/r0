/*
	Purpose: 
		Hook function at beginning, irrelevant to following flow.
*/
#include <stdio.h>
#include <stdint.h>
#include "My_Basics.h"

/*
	C11 - anonymous struct
	https://www.geeksforgeeks.org/g-fact-38-anonymous-union-and-structure/
*/

// Possible implementation of the tgmath.h macro cbrt

float go_float_impl(float i)
{
    return i + 1;
}

double go_double_impl(double i)
{
    return i + 2;
}

uint32_t go_u32_impl(uint32_t i)
{
    return i + 3;
}

int32_t go_s32_impl(int32_t i)
{
    return i + 4;
}

#define go(X) _Generic((X), \
        float: go_float_impl, \
        double: go_double_impl,  \
        uint32_t: go_u32_impl,  \
        int32_t: go_s32_impl  \
    )(X)
 
int main()
{
    float a = 10;
    double b = 100;
    uint32_t c = 1000;
    int32_t d = 10000;

    a = go(a);
    b = go(b);
    c = go(c);
    d = go(d);

    printf("a = %f\n", a);
    printf("b = %f\n", b);
    printf("c = %u\n", c);
    printf("d = %d\n", d);
 
    return 0;
}
