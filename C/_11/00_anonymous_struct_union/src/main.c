/*
	Purpose: 
		Hook function at beginning, irrelevant to following flow.
*/
#include <stdio.h>
#include "My_Basics.h"

/*
	C11 - anonymous struct
	https://www.geeksforgeeks.org/g-fact-38-anonymous-union-and-structure/
*/

struct Scope {
    // Anonymous union
    union {
        char alpha;
        int num;
    };
};
 
int main()
{
    struct Scope x, y;
	x.num = 0; //clear
	y.num = 0; //clear

    x.num = 65;
    y.alpha = 'A';
 
    // Note that members of union are accessed directly
    printf("y.alpha = %c, x.num = %d\n", y.alpha, x.num);

	printf("y.num   = %d, x.alpha = %c\n", y.num, x.alpha);

	y.alpha = 'B';
	printf("y.alpha = %c, y.num = %d\n", y.alpha, y.num);
 
    return 0;
}
