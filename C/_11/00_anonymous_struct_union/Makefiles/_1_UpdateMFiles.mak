

# 載入公用變數
include _0_Env.mak

# 為每個 .cpp .c 檔 製作個別的 Makefile
.PHONY: Update_MakeFiles
Update_MakeFiles: $(mfile_cpp) $(mfile_c)

$(mfile_cpp): %.d: $(SRC_DIR)/%.cpp
	$(CC) $(INC) -MM $(CFLAGS) $< > $(mfile_dir)/$@
	echo $(MyTab)$(CC) $(INC) -c $(CFLAGS) $< -o $(build_dir)/$(@:.d=.o) >> $(mfile_dir)/$@

$(mfile_c): %.d: $(SRC_DIR)/%.c
	$(CC) $(INC) -MM $(CFLAGS) $< > $(mfile_dir)/$@
	echo $(MyTab)$(CC) $(INC) -c $(CFLAGS) $< -o $(build_dir)/$(@:.d=.o) >> $(mfile_dir)/$@
