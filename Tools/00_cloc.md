# Brief
Count lines of code

# Installation

    sudo apt install cloc


# Github
https://github.com/AlDanial/cloc


# Usage: a file

    cloc hello.c


# Usage: a dir

    cloc gcc-5.2.0/gcc/c