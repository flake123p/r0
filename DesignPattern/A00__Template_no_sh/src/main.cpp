
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>


class base {
public:
	base() { printf("class base Constructor\n"); };
	virtual void func(void) = 0;
	virtual void func2(void) { printf("class base func2()\n"); };
	void func3(void) { printf("class base func3()\n"); };
};

class derivedA : public base {
public:
	void func(void)  { printf("class derivedA func()\n"); };
	void func2(void) { printf("class derivedA func2()\n"); };
	void func3(void) { printf("class derivedA func3()\n"); base::func3();}; // func3 is an override function calls the overriden function
};

int main(int argc, char *argv[])
{
	class derivedA obj;
	class base *p = dynamic_cast<base *>(&obj);
	
	p->func();
	p->func2();
	p->func3();
	
	obj.func();
	obj.func2();
	obj.func3();
	
	return 0;
}
