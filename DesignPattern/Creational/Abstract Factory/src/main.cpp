
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

class Button { // Abstract Class
public:
	virtual void show() = 0;
	virtual ~Button() {printf("Deleting Button ...\n");};
};

class MacButton: public Button {
public:
	void show() {printf("MacButon\n");};
	~MacButton() {printf("Deleting MacButon ...\n");};
};

class WinButton: public Button {
public:
	void show() {printf("WinButton\n");};
	~WinButton() {printf("Deleting WinButton ...\n");};
};

class Border { // Abstract Class
public:
	virtual void dump() = 0;
	virtual ~Border() {printf("Deleting Border ...\n");};
};

class MacBorder: public Border {
public:
	void dump() {printf("MacBorder\n");}
	~MacBorder() {printf("Deleting MacBorder ...\n");};
};

class WinBorder: public Border {
public:
	void dump() {printf("WinBorder\n");}
	~WinBorder() {printf("Deleting WinBorder ...\n");};
};

class AbstractFactory {
public:
    virtual Button* CreateButton() =0;
    virtual Border* CreateBorder() =0;
};

class MacFactory: public AbstractFactory {
public:
    MacButton* CreateButton() { return new MacButton; }
    MacBorder* CreateBorder() { return new MacBorder; }
};

class WinFactory: public AbstractFactory {
public:
    WinButton* CreateButton() { return new WinButton; }
    WinBorder* CreateBorder() { return new WinBorder; }
};

int main(int argc, char *argv[])
{
	int style = 1;
	AbstractFactory* fac;
	switch (style) {
	case 0://MAC:
		fac = new MacFactory;
		break;
	case 1://WIN:
		fac = new WinFactory;
		break;
	}
	Button* button = fac->CreateButton();
	Border* border = fac->CreateBorder();

	button->show();
	border->dump();
	
	delete(button);
	delete(border);
	
	return 0;
}
