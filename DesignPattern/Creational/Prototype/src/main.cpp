
#include <iostream>
#include <vector>
#include <algorithm>
#include <typeinfo>
#include <My_Basics.hpp>

//https://en.wikipedia.org/wiki/Prototype_pattern
//https://fbb-git.gitlab.io/cppannotations/cppannotations/html/cplusplus14.html#l318

// Base and its inline member:
class Base
{
public:
	virtual ~Base(){};
	virtual Base *clone() = 0;
};


// Derived and its inline member:
class Derived1: public Base
{
public:
	~Derived1()
	{
		std::cout << "~Derived1() called\n";
	};
	virtual Base *clone()
	{
		return new Derived1(*this);
	};
};


// The main function:
using namespace std;

int main()
{
	class Base *b0 = new(Derived1);
	class Base *b1 = b0->clone();
	delete(b0);
	delete(b1);
}
