
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

#include <string>
using namespace std;

// Empty vocabulary of actual object
class IPerson
{
public:
    virtual string GetName() = 0;
	virtual ~IPerson(){};
};

class Villager : public IPerson
{
public:
    string GetName()
    {
        return "Village Person";
    };
};

class CityPerson : public IPerson
{
    string GetName()
    {
        return "City Person";
    };
};

enum PersonType
{
    Rural,
    Urban
};

/// <summary>
/// Implementation of Factory - Used to create objects.
/// </summary>
class Factory
{
public:
    IPerson *GetPerson(PersonType type)
    {
        switch (type)
        {
            case PersonType::Rural:
                return new Villager();
            case PersonType::Urban:
                return new CityPerson();
            //default:
                //throw new NotSupportedException();
        }
		return NULL;
    };
};

void GetByType()
{
	Factory fac;
	IPerson *person = fac.GetPerson(Rural);
	DNSPP(person->GetName());
	delete(person);
}

// =================================================
// =================================================
// =================================================

class IProduct
{
public:
    virtual string GetName() = 0;
    virtual bool SetPrice(double price) = 0;
	virtual ~IProduct(){};
	virtual void Dump() = 0;
};

class Phone : public IProduct
{
public:
    double _price;

    string GetName()
    {
        return "Apple TouchPad";
    };

    bool SetPrice(double price)
    {
        _price = price;
        return true;
    };
	
	void Dump()
	{
		DNF(_price);
		DNSPP(GetName());
	};
};

/* Almost same as Factory, just an additional exposure to do something with the created method */
class ProductAbstractFactory
{
protected:
    virtual IProduct *MakeProduct() = 0;
public:
    virtual IProduct *GetObject() // Implementation of Factory Method.
    {
        return this->MakeProduct();
    }
};

class PhoneConcreteFactory : public ProductAbstractFactory
{
protected:
    IProduct *MakeProduct()
    {
        IProduct *product = new Phone();
        // Do something with the object after you get the object.
        product->SetPrice(20.30);
        return product;
    }
};

void ConstructByDerived()
{
	class PhoneConcreteFactory fac;
	IProduct *product = fac.GetObject();
	product->Dump();
	delete(product);
}

int main(int argc, char *argv[])
{
	pn("====== DEMO 1 ======");
	GetByType();
	pn("====== DEMO 2 ======");
	ConstructByDerived();
	
	return 0;
}
