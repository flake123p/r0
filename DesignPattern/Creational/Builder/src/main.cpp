
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

#include <string>
using namespace std;
/// <summary>
/// Represents a product created by the builder
/// </summary>
class Bicycle
{
public:
    string Make;
    string Model;
    int Height;
    string Colour;

    Bicycle(string make, string model, string colour, int height)
    {
        Make = make;
        Model = model;
        Colour = colour;
        Height = height;
    }
};

/// <summary>
/// The builder abstraction
/// </summary>
class IBicycleBuilder
{
public:
    string Colour;
    int Height;

    virtual Bicycle *GetResult() = 0;
};

/// <summary>
/// Concrete builder implementation
/// </summary>
class GTBuilder : public IBicycleBuilder
{
public:
    Bicycle *GetResult()
    {	
        return Height == 29 ? new Bicycle("GT", "Avalanche", Colour, Height) : null;        
    }
};

/// <summary>
/// The director
/// </summary>
class MountainBikeBuildDirector
{
public:
    IBicycleBuilder *_builder;
    MountainBikeBuildDirector(IBicycleBuilder *builder = NULL) 
    {
        _builder = builder;
    }

    void Construct()
    {
        _builder->Colour = "Red";
        _builder->Height = 29;
    }
};


void DoSomethingWithBicycles()
{
	GTBuilder builder;
	MountainBikeBuildDirector director = MountainBikeBuildDirector(&builder);

	director.Construct();
	Bicycle *myMountainBike = builder.GetResult();
	DNS(myMountainBike->Make.c_str());
	DNS(myMountainBike->Model.c_str());
	DND(myMountainBike->Height);
	DNS(myMountainBike->Colour.c_str());
	delete(myMountainBike);
}


int main(int argc, char *argv[])
{
	DoSomethingWithBicycles();
	
	return 0;
}
