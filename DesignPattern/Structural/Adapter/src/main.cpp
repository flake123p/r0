
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

class LightningPhone {
public:
    void recharge() {
		pn("LightningPhone Recharge");
    }
};

class MicroUsbPhone {
public:
    void recharge() {
		pn("MicroUsbPhone Recharge");
    }
};

/* exposing the target interface while wrapping source object */
class LightningToMicroUsbAdapter : public MicroUsbPhone {
public:
    LightningPhone *lightningPhone;

    LightningToMicroUsbAdapter(LightningPhone *lightningPhone) {
        this->lightningPhone = lightningPhone;
    }

    void recharge() {
		MicroUsbPhone::recharge();
        lightningPhone->recharge();
    }
};

int main(int argc, char *argv[])
{
	LightningPhone iphone;
	LightningToMicroUsbAdapter ltou(&iphone);
	
	ltou.recharge();

	return 0;
}
