
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

/*
Facade:
	Intent:
		Provide a unified interface to a set of interfaces in a subsystem.
		Facade defines a higher-level interface that makes the subsystem easier to use. 
	Flake:
		mutiple object members, not inherit them

Sample code:
	https://zh.wikipedia.org/wiki/%E5%A4%96%E8%A7%80%E6%A8%A1%E5%BC%8F
*/

#define BOOT_ADDRESS 0
#define BOOT_SECTOR 0
#define SECTOR_SIZE 0

using namespace std;

class CPU {
public:
	static void freeze() { printf("%s()\n", __func__); };
	static void jump(long position) { printf("%s()\n", __func__); };
	static void execute() { printf("%s()\n", __func__); };
};

class Memory {
public:
	static void load(long position, char* data) { printf("%s()\n", __func__); };
};

class HardDrive {
public:
	static char* read(long lba, int size) { printf("%s()\n", __func__); return 0;};
};

/* Façade */

class Computer {
public:
	void startComputer() {
		CPU::freeze();
		Memory::load(BOOT_ADDRESS, HardDrive::read(BOOT_SECTOR, SECTOR_SIZE));
		CPU::jump(BOOT_ADDRESS);
		CPU::execute();
	};
};

int main(void)
{
	Computer *facade = new Computer();
	facade->startComputer();
	delete(facade);
	return 0;
}