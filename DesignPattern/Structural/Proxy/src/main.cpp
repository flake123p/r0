
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

#include <memory>
//using namespace std;

class BaseCar {
 public:
  virtual ~BaseCar() { std::cout << "BaseCar destructor!" << std::endl; }

  virtual void DriveCar() = 0;
};

class Car : public BaseCar {
 public:
  void DriveCar() override { std::cout << "Car has been driven!" << std::endl; }
};

class ProxyCar : public BaseCar {
 public:
  ProxyCar(int driver_age) : driver_age_(driver_age) {}

  void DriveCar() override {
    if (driver_age_ > 16) {
      real_car_->DriveCar();
    } else {
      std::cout << "Sorry, the driver is too young to drive." << std::endl;
    }
  }

 private:
  //std::unique_ptr<BaseCar> real_car_ = std::make_unique<Car>();
  BaseCar *real_car_ = new(Car);
  int driver_age_;
};

int main() {
  //std::unique_ptr<BaseCar> car = std::make_unique<ProxyCar>(16);
  BaseCar *car = new ProxyCar(16);
  car->DriveCar();
  delete(car);
  //car = std::make_unique<ProxyCar>(25);
  car = new ProxyCar(25);
  car->DriveCar();
  delete(car);
}