
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

//
// This is from folder:"Cpp" sample code: A05_Virtual
//

// Abstract class = there is virtual function inside
class MedMgr
{
public:
	virtual void show(int level = 0) = 0; //pure virtual
};

class Med : public MedMgr
{
public:
	int aryCtr;
	class MedMgr *ary[10];
	Med(void) {
		aryCtr = 0;
	};
	void add(class MedMgr *node) {
		ary[aryCtr] = node;
		aryCtr++;
	};
	void show(int level = 0) { 
		int i;
		for (i=0; i<level; i++) {
			printf("  ");
		}
		std::cout << "current is Med" << std::endl;
		for (i=0; i<aryCtr; i++) {
			ary[i]->show(level+1);
		}
	}
};

class Phy : public MedMgr
{
public:
	void show(int level = 0) { 
		int i;
		for (i=0; i<level; i++) {
			printf("  ");
		}
		std::cout << "current is Phy" << std::endl;
	}
};

int main(int argc, char *argv[])
{
	class Med mR, m1, m2, m3, m31, m32;
	class Phy p11, p12;
	class Phy p21, p22;
	class Phy p311, p312;
	class Phy p321, p322, p323;

	//t2
	mR.add(&m1);
	mR.add(&m2);
	mR.add(&m3);
	
	//t3
	m1.add(&p11);
	m1.add(&p12);
	m2.add(&p21);
	m2.add(&p22);
	m3.add(&m31);
	m3.add(&m32);
	
	//t4
	m31.add(&p311);
	m31.add(&p312);
	m32.add(&p321);
	m32.add(&p322);
	m32.add(&p323);
	
	mR.show();
	
	return 0;
}
