
#include <iostream>
#include <map>
#include <string>
#include <My_Basics.hpp>

//https://en.wikipedia.org/wiki/Flyweight_pattern#Example_in_C++
//The C++ Standard Template Library provides several containers that allow unique objects to be mapped to a key. 
//The use of containers helps further reduce memory usage by removing the need for temporary objects to be created. 

// Instances of Tenant will be the Flyweights
class Tenant {
private:
    std::string m_name;
public:
    Tenant(const std::string& name = "") : m_name(name) {}

    std::string name() const {
        return m_name;
    }
};

// Registry acts as a factory and cache for Tenant flyweight objects
class Registry {
private:
    std::map<std::string,Tenant> tenants;
public:
    Registry() : tenants() {}

    Tenant findByName(const std::string& name) {
        if (tenants.count(name) != 0) return tenants[name];
        auto newTenant = Tenant(name);
        tenants[name] = newTenant;
        return newTenant;
    }
};

// Apartment maps a unique tenant to their room number.
class Apartment {
private:
    std::map<int,Tenant> m_occupants;
    Registry m_registry;
public:
    Apartment() : m_occupants(), m_registry() {}

    void addOccupant(const std::string& name, int room) {
        m_occupants[room] = m_registry.findByName(name);
    }

    void show_tenants() {
        for (auto i : m_occupants) {
            const int room = i.first;
            const Tenant tenant = i.second;
            std::cout << tenant.name() << " occupies room " << room << std::endl;
        }
    }
};

int main() {
    Apartment apartment;
    apartment.addOccupant("David", 1);
    apartment.addOccupant("Sarah", 3);
    apartment.addOccupant("George", 2);
    apartment.addOccupant("Lisa", 12);
    apartment.addOccupant("Michael", 10);
	apartment.addOccupant("David", 7);
	apartment.addOccupant("David", 8);
    apartment.show_tenants();

    return 0;
}