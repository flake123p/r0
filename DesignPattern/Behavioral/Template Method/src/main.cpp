
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

//
// This is from folder:"Cpp" sample code: A05_Virtual
//

// Abstract class = there is virtual function inside
class csma_class
{
	public:
		virtual void show() = 0; //pure virtual
		virtual void simp() {printf("simple - parent\n");}; //simple virtual
		void noneVir() {printf("noneVir - parent\n");}; //none virtual
};

class csma_cd : public csma_class
{
	public:
		// Must implement show()
		void show() { std::cout << "current is cd" << std::endl; }
		void simp() { std::cout << "simple - cd" << std::endl; }
		void noneVir() { std::cout << "noneVir - cd" << std::endl; }
};

class csma_ca : public csma_class
{
	public:
		// Must implement show()
		void show() { std::cout << "current is ca" << std::endl; }
};

int main(int argc, char *argv[])
{
	csma_class *csma;
	csma_cd obj1;
	csma_ca obj2;
	
	printf("obj1:\n");
	csma = &obj1;
	csma->show();
	csma->simp();
	csma->noneVir();
	obj1.noneVir();
	
	printf("obj2:\n");
	csma = &obj2;
	csma->show();
	csma->simp();
	csma->noneVir();
	
	return 0;
}
