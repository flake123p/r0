
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

enum LogLevel
{
	None = 0,                 //        0
	Info = 1,                 //        1
	Debug = 2,                //       10
	Warning = 4,              //      100
	Error = 8,                //     1000
	FunctionalMessage = 16,   //    10000
	FunctionalError = 32,     //   100000
	All = 63                  //   111111
};

/// <summary>
/// Abstract Handler in chain of responsibility pattern.
/// </summary>
class Logger {
public:
	enum LogLevel logMask;

	// The next Handler in the chain
	class Logger *next;

	Logger(LogLevel mask)
	{
		next = NULL;
		this->logMask = mask;
	};
	virtual ~Logger(){};

	/// <summary>
	/// Sets the Next logger to make a list/chain of Handlers.
	/// </summary>
	class Logger *SetNext(class Logger *nextlogger)
	{
		class Logger *lastLogger = this;

		while (lastLogger->next != null)
		{
			lastLogger = lastLogger->next;
		}

		lastLogger->next = nextlogger;
		return this;
	};

	void Message(const char *msg, LogLevel severity)
	{
		if ((severity & logMask) != 0) // True only if any of the logMask bits are set in severity
		{
			WriteMessage(msg);
		}
		if (next != null) 
		{
			next->Message(msg, severity); 
		}
	};

	virtual void WriteMessage(const char *msg) = 0;
};

class ConsoleLogger : public Logger {
public:
	ConsoleLogger(LogLevel mask) : Logger(mask){};

	void WriteMessage(const char *msg)
	{
		printf("Writing to console: %s\n", msg);
	};
};

class EmailLogger : public Logger {
public:
	EmailLogger (LogLevel mask) : Logger(mask){};

	void WriteMessage(const char *msg)
	{
		printf("Writing to email: %s\n", msg);
	};
};

class FileLogger  : public Logger {
public:
	FileLogger (LogLevel mask) : Logger(mask){};

	void WriteMessage(const char *msg)
	{
		printf("Writing to file: %s\n", msg);
	};
};

int main(int argc, char *argv[])
{
	// Build the chain of responsibility
	/*
	This is c# style
	
	Logger logger;
	logger = new ConsoleLogger(LogLevel.All)
					 .SetNext(new EmailLogger(LogLevel.FunctionalMessage | LogLevel.FunctionalError))
					 .SetNext(new FileLogger(LogLevel.Warning | LogLevel.Error));
	*/
	class Logger *logger  = new ConsoleLogger(LogLevel::All);
	class Logger *logger2 = new EmailLogger((LogLevel)(LogLevel::FunctionalMessage | LogLevel::FunctionalError));
	class Logger *logger3 = new FileLogger((LogLevel)(LogLevel::Warning | LogLevel::Error));
	logger->SetNext(logger2);
	logger->SetNext(logger3);
	
	// Handled by ConsoleLogger since the console has a loglevel of all
	logger->Message("Entering function ProcessOrder().", LogLevel::Debug);
	logger->Message("Order record retrieved.", LogLevel::Info);

	// Handled by ConsoleLogger and FileLogger since filelogger implements Warning & Error
	logger->Message("Customer Address details missing in Branch DataBase.", LogLevel::Warning);
	logger->Message("Customer Address details missing in Organization DataBase.", LogLevel::Error);

	// Handled by ConsoleLogger and EmailLogger as it implements functional error
	logger->Message("Unable to Process Order ORD1 Dated D1 For Customer C1.", LogLevel::FunctionalError);

	// Handled by ConsoleLogger and EmailLogger
	logger->Message("Order Dispatched.", LogLevel::FunctionalMessage);

	delete(logger);
	delete(logger2);
	delete(logger3);

	return 0;
}

/* Ideal Output
Writing to console: Entering function ProcessOrder().
Writing to console: Order record retrieved.
Writing to console: Customer Address details missing in Branch DataBase.
Writing to Log File: Customer Address details missing in Branch DataBase.
Writing to console: Customer Address details missing in Organization DataBase.
Writing to Log File: Customer Address details missing in Organization DataBase.
Writing to console: Unable to Process Order ORD1 Dated D1 For Customer C1.
Sending via email: Unable to Process Order ORD1 Dated D1 For Customer C1.
Writing to console: Order Dispatched.
Sending via email: Order Dispatched.
*/