
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

//
// This is from folder:"Cpp" sample code: A05_Virtual
//

// Abstract class = there is virtual function inside
class csma_class
{
	public:
		virtual void show() = 0; //pure virtual
		virtual void simp() {printf("simple - parent\n");}; //simple virtual
		void noneVir() {printf("noneVir - parent\n");}; //none virtual
};

class csma_cd : public csma_class
{
	public:
		// Must implement show()
		void show() { std::cout << "current is cd" << std::endl; }
		void simp() { std::cout << "simple - cd" << std::endl; }
		void noneVir() { std::cout << "noneVir - cd" << std::endl; }
};

class csma_ca : public csma_class
{
	public:
		// Must implement show()
		void show() { std::cout << "current is ca" << std::endl; }
};

class Composition { 
public: 
	class csma_class* strategy; 
	void Go()
	{
		strategy->show();
	};
}; 

int main(int argc, char *argv[])
{
	class Composition c0, c1;
	csma_cd obj1;
	csma_ca obj2;
	c0.strategy = &obj1;
	c1.strategy = &obj2;
	
	c0.Go();
	c1.Go();
	
	return 0;
}
