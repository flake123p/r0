
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

#include <string>
//public delegate void MessageReceivedEventHandler(string message, string sender);

class Mediator
{
public:
	std::string name;
	class Mediator *first;
	class Mediator *next;
    //public event MessageReceivedEventHandler MessageReceived;

	Mediator(class Mediator **pFirst, const char *inName)
	{
		name = inName;
		first=*pFirst;
		next=NULL;
		if (first == NULL) {
			first = this;
			*pFirst = this;
		} else {
			class Mediator *curr = first;
			while (curr->next != NULL) {
				curr = curr->next;				
			}
			curr->next = this;
		}
	};
    void Send(const char *message)
    {
		class Mediator *curr;
		
		printf("%s send message : %s\n", name.c_str(), message);
		for (curr = first; curr != NULL; curr = curr->next) {
			if (curr != this)
				curr->Receive(message);
		}
    };
	void Receive(const char *message)
	{
		printf("%s received message : %s\n", name.c_str(), message);
	};
};
class Mediator *pFirst00 = NULL;

int main(int argc, char *argv[])
{
	class Mediator a = Mediator(&pFirst00, "(a)");
	class Mediator b = Mediator(&pFirst00, "(b)");
	class Mediator c = Mediator(&pFirst00, "(c)");
	b.Send("Hello");
	return 0;
}
