
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

#include <vector>
#include <string>
using namespace std;

class Memento_Originator
{
public:
	string savedState;

    Memento_Originator(string stateToSave)
    {
        savedState = stateToSave;
    }
	
	//Originator
	string state;

	// The class could also contain additional data that is not part of the
	// state saved in the memento.

	void Set(const char *state)
	{
		printf("Originator: Setting state to %s\n", state);
		this->state = state;
	};
	Memento_Originator *SaveToMemento()
	{
		printf("Originator: Saving to Memento.");
		return new Memento_Originator(state);
	};
	void RestoreFromMemento(Memento_Originator *memento)
	{
		state = memento->savedState;
		printf("Originator: State after restoring from Memento: %s\n", state.c_str());
	};
};

static void Caretaker()
{
	vector<Memento_Originator *> savedStates;

	Memento_Originator *originator = new Memento_Originator("");
	originator->Set("State1");
	originator->Set("State2");
	savedStates.push_back(originator->SaveToMemento());
	originator->Set("State3");
	// We can request multiple mementos, and choose which one to roll back to.
	savedStates.push_back(originator->SaveToMemento());
	originator->Set("State4");

	originator->RestoreFromMemento(savedStates[1]);
}

int main(int argc, char *argv[])
{
	Caretaker();
	return 0;
}
