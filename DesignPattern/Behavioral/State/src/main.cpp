
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

#include <string>
using namespace std;

/*
	May use with Singleton!!
	May use with Singleton!!
	May use with Singleton!!
	State objects are often Singletons (144). 
	State objects are often Singletons (144). 
	State objects are often Singletons (144). 
*/

class StateContext;

class State {
public:
    virtual void writeName(StateContext *context, string name) = 0;
	virtual ~State(){};
};

class StateContext {
public:
    class State *state;
    
    StateContext() {
        //state = new LowerCaseState();
    };

    /**
     * Set the current state.
     * Normally only called by classes implementing the State interface.
     * @param newState the new state of this context
     */
    //void setState(State newState) {
        //state = newState;
    //}

    void writeName(string name) {
        state->writeName(this, name);
    };
};

class UpperCaseState : public State {
	void writeName(StateContext *context, string name);
};

class LowerCaseState : public State {
public:
	void writeName(StateContext *context, string name) {
        printf("Lower Case : %s\n", name.c_str());
		delete(context->state);
        context->state = (class State *)new UpperCaseState;
    };
};

void UpperCaseState::writeName(StateContext *context, string name) {
	printf("Upper Case : %s\n", name.c_str());
	delete(context->state);
	context->state = (State *)new LowerCaseState;
};

int main(int argc, char *argv[])
{
	class StateContext context;
	context.state = (State *)new LowerCaseState;
	context.writeName("Monday");
	context.writeName("Tuesday");
	context.writeName("Wednesday");
	
	return 0;
}
