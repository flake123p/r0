
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

/*
Ref:
https://en.wikipedia.org/wiki/Interpreter_pattern

From C# !!
*/
#include <string>
#include <vector>
using namespace std;

class Context {
public:
	//public Stack<string> Result = new Stack<string>();
	vector<string> Result;

	//Context(){Result.clear();};
};

class Expression {
public:
	virtual void Interpret(Context *pContext) = 0;
	VIR_DES(Expression);
};

class OperatorExpression : public Expression {
public:
	Expression *Left;
	Expression *Right;

	void Interpret(Context *pContext)
	{
		Left->Interpret(pContext);
		string leftValue = pContext->Result.back();
		pContext->Result.pop_back();

		Right->Interpret(pContext);
		string rightValue = pContext->Result.back();
		pContext->Result.pop_back();

		DoInterpret(pContext, leftValue, rightValue);
	};

	virtual void DoInterpret(Context *pContext, string leftValue, string rightValue) = 0;
};

class EqualsExpression : public OperatorExpression { 
public:
	void DoInterpret(Context *pContext, string leftValue, string rightValue)
	{
		pContext->Result.push_back(leftValue == rightValue ? "true" : "false");
	};
};

class OrExpression : public OperatorExpression {
public:
	void DoInterpret(Context *pContext, string leftValue, string rightValue)
	{
		pContext->Result.push_back(leftValue == "true" || rightValue == "true" ? "true" : "false");
	};
};

class MyExpression : public Expression {
public:
	string Value;

	void Interpret(Context *pContext)
	{
		pContext->Result.push_back(Value);
	};
};
	

int main()
{
	//var context = new Context();
	class Context context;
	//var input = new MyExpression();
	class MyExpression input;

/*
	var expression = new OrExpression
	{
		Left = new EqualsExpression
		{
			Left = input, 
			Right = new MyExpression { Value = "4" }
		},
		Right = new EqualsExpression
		{
			Left = input,
			Right = new MyExpression { Value = "four" }
		}
	};
*/
	class OrExpression orExp;
	class EqualsExpression orExpLeft, orExpRight;
	orExp.Left = &orExpLeft;
	orExp.Right = &orExpRight;
	orExpLeft.Left = &input;
	orExpRight.Left = &input;
	class MyExpression orExpLeftRight, orExpRightRight;
	orExpLeftRight.Value = "4";
	orExpRightRight.Value = "four";
	orExpLeft.Right = &orExpLeftRight;
	orExpRight.Right = &orExpRightRight;
/*
orExp
	L:orExpLeft
		L:input(string)
		R:orExpLeftRight, Value="4"
	R:orExpRight
		L:input(sting)
		R:orExpRightRight, Value="four"
*/
	
	input.Value = "four";
	orExp.Interpret(&context);
	// Output: "true" 
	printf("1. %s\n", context.Result.back().c_str());
	context.Result.pop_back();

	input.Value = "44";
	orExp.Interpret(&context);
	// Output: "false"
	//Console.WriteLine(context.Result.Pop());
	printf("2. %s\n", context.Result.back().c_str());
	context.Result.pop_back();
	
	return 0;
}