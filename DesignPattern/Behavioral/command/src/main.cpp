
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>
#include <vector>
/*
Command:
	Intent:
		Encapsulate a request as an object, thereby letting you parameterizeclients with 
		different requests, queue or log requests, and supportundoable operations. 
	Flake:
		objectlize commands

Sample code:
	https://zh.wikipedia.org/wiki/%E5%91%BD%E4%BB%A4%E6%A8%A1%E5%BC%8F
*/

class ICommand
{
    public:
        virtual void Execute() = 0;
		virtual ~ICommand(){};
};

class Swicher
{
    private:
      std::vector<ICommand *> _commands;

    public:
        void StoreAndExecute(ICommand *command)
        {
            if (command){
                _commands.push_back(command);
                command->Execute();
            }
        }
};

class Light
{
    public:
        void TurnOn()
        {
            std::cout<<"The light is on."<<std::endl;
        }

        void TurnOff()
        {
            std::cout << "The light is off." << std::endl;
        }
};

/* The Command for turning on the light - ConcreteCommand #1 */
class FlipUpCommand : public ICommand
{
  private:
    Light *_light;

  public:
    FlipUpCommand(Light *light)
    {
        _light = light;
    }

    void Execute()
    {
        _light->TurnOn();
    }
	
	~FlipUpCommand(){};
};

/* The Command for turning off the light - ConcreteCommand #2 */
class FlipDownCommand : public ICommand
{
  private:
    Light *_light;

  public:
    FlipDownCommand(Light *light)
    {
        _light = light;
    }

    void Execute()
    {
        _light->TurnOff();
    }
	
	~FlipDownCommand(){};
};

int main()
{
    Light *light = new Light();
    ICommand *switchOn = dynamic_cast<ICommand *>(new FlipUpCommand(light));
    ICommand *switchDown = dynamic_cast<ICommand *>(new FlipDownCommand(light));

    Swicher *switcher = new Swicher();
    switcher->StoreAndExecute(switchOn);
    switcher->StoreAndExecute(switchDown);

    delete switcher;
    delete switchOn;
    delete switchDown;
    delete light;
}