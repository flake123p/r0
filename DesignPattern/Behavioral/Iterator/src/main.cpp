
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

#include "DesignPatternBasics.hpp"

class SimpleIt : public It {
public:
	static class SimpleIt *first;
	static class SimpleIt *next;
	static class SimpleIt *curr;
	static class SimpleIt *FirstOne() {curr = first; return curr;};
	static class SimpleIt *NextOne() {if(curr)curr=curr->nextOne; return curr;};
	static int IsNotDone() { return curr != NULL;};
	
	class SimpleIt *First() {return FirstOne();};
	class SimpleIt *Next() {return NextOne();};
	class SimpleIt *CurrentItem() const {return curr;};
	int IsDone() const { return curr == NULL;}; //dangerous!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
	
	class SimpleIt *nextOne;
	SimpleIt(){nextOne=NULL;};
	u32 val;
	/*
	DLList_Entry_t entry;
	class ACList *curr;
	virtual class ACList *First() { curr = (class ACList *)pHead->head; return curr; };
	virtual class ACList *Next() { if(curr != NULL)curr = (class ACList *)curr->entry.next; return curr; };
	virtual int IsDone() const { return curr == NULL;};
	virtual class ACList *CurrentItem() const { return curr; };

	void InsertLast(void) { DLLIST_INSERT_LAST2(pHead, this, class ACList); };
	*/
	
};

class SimpleIt *SimpleIt::first = NULL;
class SimpleIt *SimpleIt::next = NULL;
class SimpleIt *SimpleIt::curr = NULL;

int main(int argc, char *argv[])
{
	DNP(SimpleIt::FirstOne());
	
	class SimpleIt o1, o2;
	o2.val = 2222;
	o1.val = 1111;
	SimpleIt::first = &o2;
	o2.nextOne = &o1;
	
	class It *it;
	for (it = SimpleIt::FirstOne(); it!=NULL; it = SimpleIt::NextOne()) {
		class SimpleIt *simpIt = (class SimpleIt *)it;
		DND(simpIt->val);
	}
	
	// Don't use simpIt2->IsDone(), because simpIt2 will become 0x00000000 at last loop !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// Don't use simpIt2->IsDone(), because simpIt2 will become 0x00000000 at last loop !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// Don't use simpIt2->IsDone(), because simpIt2 will become 0x00000000 at last loop !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// Don't use simpIt2->IsDone(), because simpIt2 will become 0x00000000 at last loop !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// Don't use simpIt2->IsDone(), because simpIt2 will become 0x00000000 at last loop !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	class SimpleIt *simpIt2;
	for (simpIt2 = simpIt2->FirstOne(); simpIt2->IsNotDone(); simpIt2 = simpIt2->NextOne()) {
		DND(simpIt2->val);
		DND(simpIt2->IsDone());
	}

	return 0;
}
