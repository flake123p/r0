
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

#include <list>

class Subject;

class Observer { 
public: 
  virtual ~ Observer(){}; 
  virtual void Update(Subject* theChangedSubject) = 0; 
protected: 
  Observer(){}; 
}; 

class Subject { 
public: 
  virtual ~Subject(){}; 
  virtual void Attach(Observer* o) {        _observers.push_back(o);    } ; 
  virtual void Detach(Observer* o) {        _observers.remove(o);    } ; 
  virtual void Notify()
  { 
    for (std::list<Observer *>::iterator it = _observers.begin(); it != _observers.end(); it++)
	{ 
      (*it)->Update(this); 
    };
} 
protected: 
  Subject(){}; 
private: 
  std::list<Observer *> _observers; 
}; 

class Sub : public Subject {
public:
	int idx;
	Sub(int inIdx){idx=inIdx;};
};

class Obs : public Observer {
public:
	int idx;
	Obs(int inIdx){idx=inIdx;};
	void Update(Subject* theChangedSubject)
	{
		class Sub *pSub = (class Sub *)theChangedSubject;
		printf("obs(%d) get updated by sub(%d)\n", idx, pSub->idx);
	};
};

int main(int argc, char *argv[])
{
	class Obs dude1(1), dude2(2), dude3(3), dude4(4);
	class Sub girlA(100), girlB(200);
	girlA.Attach(&dude1);
	girlA.Attach(&dude2);
	girlB.Attach(&dude3);
	girlB.Attach(&dude4);
	girlA.Notify();
	girlB.Notify();
	return 0;
}
