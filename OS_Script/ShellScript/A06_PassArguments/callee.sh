#!/bin/bash

echo 1st argument  is:  $1
echo 2nd argument  is:  $2
echo 3rd argument  is:  $3
echo all arguments are: $@

if [ $2 = 5 ]; then
    echo "\$2 is 5"
elif [ $2 = 6 ]; then
    echo "\$2 is 6"
else
    echo "\$2 is not 5 or 6"
fi