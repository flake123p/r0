
# env.sh
curr_folder=$PWD
build_folder='build_sanity'
job_num='-j12'
prof_file='prof_sanity.txt'


# Use Case:
. ../shell_env.sh

. ./env.sh # build_folder, job_num

cd ../$build_folder

make $job_num
make_result=$?

echo ====== Build Done!! Errorlevel = $make_result ======

if [ $make_result == 0 ]; then
    cd .. # to app_sc2/
    python prof.py $build_folder $prof_file
    if [ $prof_verbose != 0 ]; then
        cat $prof_file
    fi
fi