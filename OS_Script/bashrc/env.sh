# . ~/ws/r0/OS_Script/bashrc/env.sh
alias a='./a.out'
alias b='./build.sh'
alias c='./clean.sh'
alias cb='rm build -rf; mkdir build; cd build; cmake ..; make -j10'
alias cov='./cov.sh'
alias covs='./cov_show.sh'
alias d='./debug.sh'
alias f='./fire.sh'
alias g='./go.sh'
alias r='./run.sh'
alias p='python'
alias s='./simple.sh'
alias t='./test.sh'
alias m='./my.sh'
alias n='node'
alias lo='./local.sh'
alias v='./vcd.sh'
alias i='./install.sh'
alias u='./uninstall.sh'
alias cft='c++filt'
alias condaa='conda activate'
alias condad='conda deactivate'
alias vprof='valgrind --tool=callgrind'
alias vshow='kcachegrind'
alias vshowx='unset GTK_PATH && kcachegrind'
alias gitcom='git add -u && git commit -m "update"'
alias gitpp='git pull; git push'
alias gitp='git fetch origin; git merge -m MERGE'
alias cdb='cd ..'
alias cdbb='cd ../..'
alias cdbbb='cd ../../..'
alias mak='make -j10'
act () {
        pth=~/ws/envs
        if [ $1 = 'list' ] ; then
                ls $pth -l
        elif [ $1 = 'conda' ] ; then
                source ~/anaconda3/bin/activate base
        else
                xxx=$pth/$1/bin/activate
                echo source : $xxx
                source $xxx
        fi
}
alias dact='deactivate'
alias sbc='source ~/.bashrc'
alias ps6d='pyside6-designer'
alias ps6u='pyside6-uic'
alias mk='rm build -rf; mkdir build; cd build;cmake ..'
alias mkr='rm * -rf; cmake ..; mak;'
alias lk='ls -al --block-size=K'
alias lm='ls -al --block-size=M'