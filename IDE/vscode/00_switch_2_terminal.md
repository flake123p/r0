https://stackoverflow.com/questions/42796887/switch-focus-between-editor-and-integrated-terminal



1. Open the Command Palette (Ctrl+Shift+P Windows/Linux or ...).

2. Type "Preferences: Open Keyboard Shortcuts (JSON)" and press Enter.

3. Add the following entries to the keybindings.json file:

// Toggle between terminal and editor focus
{
    "key":     "ctrl+`",
    "command": "workbench.action.terminal.focus"
},
{
    "key":     "ctrl+`",
    "command": "workbench.action.focusActiveEditorGroup",
    "when":    "terminalFocus"
}
