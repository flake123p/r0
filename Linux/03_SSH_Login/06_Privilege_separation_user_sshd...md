
Privilege separation user sshd does not exist

https://support.hpe.com/hpesc/public/docDisplay?docId=c01551680&docLocale=en_US

$ sudo vim /etc/passwd

add this at bottom:

sshd:x:138:65534::/var/run/sshd:/usr/sbin/nologin

138 is for the latest id: 127