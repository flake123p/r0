
# Download Old Version (SSH)
https://askubuntu.com/questions/874194/where-can-i-find-old-versions-of-openssh-server-particularly-7-2-before-patch

https://www.openssh.com/portable.html#http

Then:
    ./configure --prefix=/home/pupil/temp/ssh/791/install/
    (Build ssl 1.1.1 and fix path issue...)


# Download Old Version (SSL)
https://www.openssl.org/source/old/1.1.1/index.html

to system:
    ./config --prefix=/usr/local/openssl --shared
        or
    ./config
    make
    sudo make install

remove version 3.x:
    sudo apt-get remove libssl-dev

!!! Then add openssl lib to /etc/ld.so.conf.d/ !!!
    sudo cp libc.conf my_openssl.conf
    sudo vim my_openssl.conf
    sudo ldconfig


# Check version (SSH7.9 + SSL1.1.1 = Good):

ssh -v localhost

sshd -v