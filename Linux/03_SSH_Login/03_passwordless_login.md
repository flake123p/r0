# Passwordless Login

https://www.tecmint.com/ssh-passwordless-login-using-ssh-keygen-in-5-easy-steps/


# Optional: add user on remote

    sudo adduser pupil

    sudo usermod -aG sudo pupil


# 1 Gen key on host
    
    ssh-keygen -t dsa

    or

    ssh-keygen -t rsa

No passphrase, or you have to type-in passphrase everytime

File name : x
Output:
    x
    x.pub


# 2 Copy key to remote

    ssh-copy-id -i x.pub ai1@ai1  (not default pub key)

    or

    ssh-copy-id ai1@ai1 (default pub key: ~/.ssh/id_rsa.pub)

    ...

    ssh ai1@ai1 "chmod 700 ~/.ssh && chmod 600 ~/.ssh/authorized_keys"


# 3 Make sure the authorized_keys again

    Re-copy-paste pub key to authorized_keys

The premission of .ssh/ is 700 !!!
The premission of .ssh/ is 700 !!!
The premission of .authorized_keys/ is 600 !!!
The premission of .authorized_keys/ is 600 !!!
The premission of /home/mpi can't be 777 !!!
The premission of /home/mpi can't be 777 !!!

# 4 Done, re-login


**********
**********
**********
**********

# Ubunt22 not working issue

## SSH
- [ ] /etc/ssh/ssh_config 
https://askubuntu.com/questions/1404049/ssh-without-password-does-not-work-after-upgrading-from-18-04-to-22-04

sudo vim /etc/ssh/ssh_config
or
sudo vim /usr/local/etc/ssh_config

PubkeyAcceptedKeyTypes +ssh-rsa
HostkeyAlgorithms +ssh-rsa
HostbasedAcceptedKeyTypes +ssh-rsa

sudo systemctl restart sshd ssh ; sudo service ssh restart

## SSHD
- [ ] /etc/ssh/sshd_config 
https://linuxwizardry.com/how-to-configuring-an-ssh-login-without-password-on-ubuntu-22/

sudo vim /etc/ssh/sshd_config
or
sudo vim /usr/local/etc/sshd_config

RSAAuthentication yes
PubkeyAuthentication yes
#PasswordAuthentication yes


sudo systemctl restart sshd ssh ; sudo service ssh restart

## Authorization Required but No Authorization Protocol Specified
https://tech.sadaalomma.com/ubuntu/ubuntu-authorization-required-but-no-authorization-protocol-specified/

sudo chown root:root /usr/bin/pkexec
sudo chmod 4755 /usr/bin/pkexec

sudo apt-get install --reinstall policykit-1
sudo apt-get install --reinstall libpam-modules

sudo apt-get update
sudo apt-get upgrade

sudo systemctl restart dbus