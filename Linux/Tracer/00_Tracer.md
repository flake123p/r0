# From
https://www.cntofu.com/book/46/linux_kernel/diao_shi_gong_ju_ltrace_strace_ftrace_de_shi_yong.md


https://stackoverflow.com/questions/311840/tool-to-trace-local-function-calls-in-linux?lq=1
```
ltrace only traces dynamic library calls ...
strace only traces system calls ...
```
# Hirarchy

```
  App
  |
  |  <--------ltrace
  |
libc ld  < -------strace
  |
  |     <----------systemtap
 kernel  <---------ftrace
```

# ltrace
```
  -a, --align=COLUMN  align return values in a secific column.
  -A MAXELTS          maximum number of array elements to print.
  -b, --no-signals    don't print signals.
  -c                  count time and calls, and report a summary on exit.
  -C, --demangle      decode low-level symbol names into user-level names.
  -D, --debug=MASK    enable debugging (see -Dh or --debug=help).
  -Dh, --debug=help   show help on debugging.
  -e FILTER           modify which library calls to trace.
  -f                  trace children (fork() and clone()).
  -F, --config=FILE   load alternate configuration file (may be repeated).
  -h, --help          display this help and exit.
  -i                  print instruction pointer at time of library call.
  -l, --library=LIBRARY_PATTERN only trace symbols implemented by this library.
  -L                  do NOT display library calls.
  -n, --indent=NR     indent output by NR spaces for each call level nesting.
  -o, --output=FILENAME write the trace output to file with given name.
  -p PID              attach to the process with the process ID pid.
  -r                  print relative timestamps.
  -s STRSIZE          specify the maximum string size to print.
  -S                  trace system calls as well as library calls.
  -t, -tt, -ttt       print absolute timestamps.
  -T                  show the time spent inside each call.
  -u USERNAME         run command with the userid, groupid of username.
  -V, --version       output version information and exit.
  -x FILTER           modify which static functions to trace.
  ```

# ltrace common arg (Useless)
```
ltrace -o __dummy_ltrace.txt -cCfirST python example.py
```

# ltrace final edition (must use x option for cuda)
https://stackoverflow.com/questions/65375233/how-to-trace-dynamically-loaded-library-calls-with-ltrace
```
ltrace -o __dummy_ltraceX.txt -x 'cu*' python pth.py

ltrace -o __dummy_ltraceE.txt -e 'cu*' python pth.py

nvprof --log-file nvprof.txt python3 pth.py
```


# strace common arg (Libraries Loading Trace!!!)
```
strace -o __dummy_strace.txt python tf.py

strace -o __dummy_strace.txt -f -F  python pth.py
```


# uftrace (Not many help now)
```
uftrace --force --libname -a -P main python example.py>__dummy_uftrace.txt
```

# libtree & cuda
https://terminalroot.com/libtree-a-more-modern-option-to-ldd/

```
libtree /usr/lib/x86_64-linux-gnu/libcublas.so

libcublas.so.11
├── libcublasLt.so.11 [runpath]
│   └── librt.so.1 [runpath]
└── librt.so.1 [runpath]
```

```
libtree /usr/lib/x86_64-linux-gnu/libcuda.so

libcuda.so.1
└── librt.so.1 [ld.so.conf]
```

# List symbols

# python yep
?? useless

# Nvidia prof
```
nvprof --openacc-profiling off python pth.py
```


