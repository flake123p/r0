
# [Atom]
1. Install flatpak on ubuntu
https://flathub.org/setup/Ubuntu
```
  sudo apt install flatpak

  sudo apt install gnome-software-plugin-flatpak

  flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

  (reboot)
```

2. Install Atom
https://flathub.org/apps/io.atom.Atom
```
  flatpak install flathub io.atom.Atom

  flatpak run io.atom.Atom
```
