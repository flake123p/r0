
# Link
https://unix.stackexchange.com/questions/101332/generate-file-of-a-certain-size


# Example

dd if=/dev/zero of=./dummy.dat  bs=1G  count=500

dd if=/dev/zero of=./dummy.dat  bs=1M  count=500



# A lot of files
https://stackoverflow.com/questions/17034941/how-to-create-multiple-files-with-random-data-with-bash

for i in {0..20000}; do dd if=/dev/urandom bs=1M count=1 of=file$i; done


Empty trash can:
rm -rf ~/.local/share/Trash/*