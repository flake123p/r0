1. Swap conda env to portable python env.

1.1. Create seed with python version (includes virtualenv).
    Example:
    $ conda create -n py39_15 python=3.9.15 virtualenv

    copy ~/anaconda3/envs/py39_15/ to local disk.
    Or you can use this seed:
    /AI_training/flake/seeds/py39_15s/
    Make sure the seed is in local disk, and remember the path!
    Make seed small is better，you can use ftp to transfer seeds.
    You can use an existing conda env as a seed too!!

1.2. Create python venv with seed python.
    Example:
    $ cd <path_to_local_disk>
    $ <path_to_seed_folder>/bin/python -m virtualenv <your_env_name>

1.3 Activate venv
    $ source <path_to_your_env>/bin/activate
    You can put this in your ~/.bashrc

1.4 Copy site-packages
    Copy you site-packages in any conda/python env to your env.
    Example:
    $ cp <any_conda_env>/lib/python3.9/site-packages/* <path_to_your_env>/lib/python3.9/sitepackages/ -r
    Or use site-packages/*.pth to link multiple site-packages folders.
    Make sure the site-packages folders are in local disk.

1.5 Torchrun Copy
    Maker sure the Pytorch site-packages folders are ALL copied to your site-packages
    Then copy <old_env>/bin/torchrun to <new_env>/bin.
    Correct the python path in torchrun
    $ vim torchrun
    Then modify the first line: the python path to your new env python path.

1.6 Spyder Copy
    Same as torchrun copy. (Ch 1.5)
    For spyder installation: please install it with pip, not conda!
    $ pip install spyder numpy scipy pandas matplotlib sympy cython

1.7 Example
    1. Create seed on PC, and copy it to server.
    2. Use the seed on PC to create template_env on PC. (For site-packages/)
    3. Pip install packages on template_env.
    4. Copy template_env to server.
    5. Create your env on server with seed.
    6. Copy site-packages from template_env to your env.
    7. Take care the script in bin folder. (Spyder & torchrun).

2. VSCode
    Use this vscode on server:
    /AI_training/flake/code
    To Execute it:
    $ <path_to_vscode>/bin/code

3. Pycharm
3.1 Download the Community Edition to server and un-tar it.
    Link: https://www.jetbrains.com/pycharm/download/?section=linux
    $ tar zxf <pycharm....tar.gz>

3.2 Run it
    $ cd <pycharm_folder>/bin
    $ bash pycharm.sh

3.3 Set Python Interpreter
    Ref: https://www.jetbrains.com/help/pycharm/creating-virtualenvironment.html#python_create_virtual_env
    1: Ctrl+Alt+S.
    2: Select Project on Left-Side-Bar.
    3: Select Python Interpreter.
    4: Hit "Add Interpreter".
    5: Hit "Add Local Interpreter".
    6: Select Virtualenv Environment on sidebar.
    7: Choose Existing.
    8: Set the Location (<your_env>/bin/python )to pycharm.