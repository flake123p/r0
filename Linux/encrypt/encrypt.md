
# 5 Linux Tools

https://www.tutorialspoint.com/5-tools-to-encrypt-decrypt-and-password-protect-files-in-linux

GPG
OpenSSL
Ccrypt
...

# GPG
## install
    sudo apt-get install gnupg
## encrypt
    gpg -c filename
## decrypt
    gpg filename.gpg

# OpenSSL
## install
    sudo apt-get install openssl
## encrypt
    openssl enc -aes-256-cbc -salt -in filename -out filename.enc
## decrypt
    openssl enc -aes-256-cbc -d -in filename.enc -out filename