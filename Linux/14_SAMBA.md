# Ref:
https://ubuntu.com/tutorials/install-and-configure-samba#4-setting-up-user-accounts-and-connecting-to-share


sudo apt update
sudo apt install samba
sudo vim /etc/samba/smb.conf 
```
[<folder_name_to_client>]
    comment = Samba on Ubuntu
    path = /home/john/sda/ws/c2
    read only = no
    browsable = yes

or

[tempws]
    comment = Samba on Ubuntu
    path = /home/john/sda/ws/c2
    read only = no
    browsable = yes
```
sudo service smbd restart
sudo ufw allow samba
sudo smbpasswd -a <SERVER_USER_NAME>


# Connect to server
Open browser ->
    smb://192.168.10.10
    smb://192.168.2.102


# Mount
    sudo apt install cifs-utils psmisc

    sudo mount -t cifs -o username="pupil",password="123456" //172.16.60.108/sambashare /home/ai1/sda/ws/smb/c2/