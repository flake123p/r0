

# [ip route add] sudo ip route add 192.168.10.0/24 via 192.168.2.1
https://www.cyberciti.biz/faq/ip-route-add-network-command-for-linux-explained/

dump: $ ip route
  or: $ ip r

# [Show Default GATEWAY] route -n
https://www.howtouselinux.com/post/linux-command-get-network-gateway

$ route -n