# Show device
ip a

# Up device
ifconfig ethx up
ifconfig enx00e04c361b18 up

# Set IP/Mask
ifconfig ethx xxx.xxx.xxx.xxx netmask xxx.xxx.xxx.xxx
ifconfig enx00e04c361b18 192.168.2.100 netmask 255.255.255.0

# Show gateway
route -n
netstat -rn

# Set(Delete) gateway
route add default gw 192.168.50.1 dev enp3s0
route add default gw 192.168.2.1 dev enx00e04c361b18
ip route add default via 192.168.2.1 dev enx00e04c361b18
ip route add default via 192.168.2.222 dev eno1

route delete default gw 192.168.1.1 eth1

# Show DNS
nmcli device show enx00e04c361b18

GENERAL.DEVICE:                         enx00e04c361b18
GENERAL.TYPE:                           ethernet
GENERAL.HWADDR:                         00:E0:4C:36:1B:18
GENERAL.MTU:                            1500
GENERAL.STATE:                          100 (connected)
GENERAL.CONNECTION:                     Wired connection 2
GENERAL.CON-PATH:                       /org/freedesktop/NetworkManager/ActiveConnection/5
WIRED-PROPERTIES.CARRIER:               on
IP4.ADDRESS[1]:                         192.168.2.100/24
IP4.GATEWAY:                            192.168.2.1
IP4.ROUTE[1]:                           dst = 192.168.2.0/24, nh = 0.0.0.0, mt = 101
IP4.ROUTE[2]:                           dst = 0.0.0.0/0, nh = 192.168.2.1, mt = 101
IP4.DNS[1]:                             192.168.2.1
IP4.DOMAIN[1]:                          www.tendawifi.com
IP6.ADDRESS[1]:                         fe80::c235:1670:315b:a91f/64
IP6.GATEWAY:                            --
IP6.ROUTE[1]:                           dst = fe80::/64, nh = ::, mt = 1024

# Change DNS
https://support.nordvpn.com/hc/en-us/articles/20094975629585-Change-your-DNS-servers-on-Linux-with-NordVPN
sudo vim /etc/systemd/resolved.conf
DNS=192.168.2.1
FallbackDNS=192.168.2.1

sudo systemctl restart systemd-resolved