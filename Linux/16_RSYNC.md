# Ref
https://blog.gtwang.org/linux/rsync-local-remote-file-synchronization-commands/

rsync 參數 來源檔案 目的檔案

-v：verbose 模式，輸出比較詳細的訊息。
-r：遞迴（recursive）備份所有子目錄下的目錄與檔案。
-a：封裝備份模式，相當於 -rlptgoD，遞迴備份所有子目錄下的目錄與檔案，保留連結檔、檔案的擁有者、群組、權限以及時間戳記。
-z：啟用壓縮。
-h：將數字以比較容易閱讀的格式輸出。


# Folder Sync (With exclude)

    //Flake zip(z) transfer size is more... WTH
    rsync -zrh --exclude='build' --exclude='*.so*' /local10T/flake/ws/cuda_test/ .

    rsync -rh --exclude='build' --exclude='*.so*' /local10T/flake/ws/cuda_test/ .

    20240221
        rsync -rh ./c2/* ./c2s