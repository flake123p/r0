

echo 'SUBSYSTEM=="usb",  ENV{DEVTYPE}=="usb_device", GROUP="plugdev", MODE="0664"' | sudo tee /etc/udev/rules.d/10-libuvc.rules 
sudo udevadm trigger


# ttyUSB0
https://askubuntu.com/questions/133235/how-do-i-allow-non-root-access-to-ttyusb0
sudo adduser <the user you want to add> dialout
sudo reboot