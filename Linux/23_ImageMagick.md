https://ubuntuhandbook.org/index.php/2013/07/how-to-convert-png-to-jpg-on-ubuntu-via-command/

1.) Install the required package by running below command in terminal:

    sudo apt-get install imagemagick


2.) Then you can convert an .png image to .jpg format via below command. It takes “ubuntuhandbook.png” in the current directory and creates a JPEG image from it.

    convert ubuntuhandbook.png ubuntuhandbook.jpg

You can also specify a compression level for JPEG images.

    convert ubuntuhandbook.png -quality 90 ubuntuhandbook.jpg


3.) If you want to convert a batch of photo images, for example all .png files in “Pictures” folder, use try this command instead:

    cd ~/Pictures && for file in *.png; do convert $file ${file%.png}.jpg; done


4.) To convert all PNG files into JPEG files with that same filename but a different suffix. However be warned that if existing file with the same name will be over-written.

    mogrify -format jpg *.png