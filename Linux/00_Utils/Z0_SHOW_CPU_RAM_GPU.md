# CPU
> lscpu


# RAM
https://askubuntu.com/questions/391173/how-to-find-the-frequency-and-type-of-my-current-ram
> sudo lshw -short -C memory


# GPU (PCIE version)
https://askubuntu.com/questions/20256/how-can-i-determine-which-gpu-card-is-running-at-pci-express-2-0-x16-which-is

2.5 GT/s = PCI-e gen 1,
5   GT/s = PCI-e gen 2,
8   GT/s = PCI-e gen 3 ...

and the width is the number of lanes.

> sudo lspci -vv