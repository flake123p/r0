
# Find word in a file:
e.g.:
    gawk 'BEGIN{IGNORECASE=1} /this/' 20_SED_replace_string.md

output:
    : echo 'bbc' | sed 's/b/B/'
    : echo 'bbc' | sed 's/b/B/g'


# Ignore case : works on gawk, not mawk(ubuntu default)
e.g.:
    gawk 'BEGIN{IGNORECASE=1} /this/' 20_SED_replace_string.md

Install gawk:
    sudo apt install gawk