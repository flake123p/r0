> grep -RnwI '/path/to/somewhere/' -e 'pattern'

-r or -R is recursive ; use -R to search entirely
-n is line number, and
-w stands for match the whole word.
-l (lower-case L) can be added to just give the file name of matching files.
-e is the pattern used during the search
-i ignore case-insensitive search (https://labex.io/questions/how-to-perform-caseinsensitive-search-with-grep-271291)

Along with these, --exclude, --include, --exclude-dir flags could be used for efficient searching:

This will only search through those files which have .c or .h extensions:
> grep --include=\*.{c,h} -rnw '/path/to/somewhere/' -e "pattern"

This will exclude searching all the files ending with .o extension:
> grep --exclude=\*.o -rnw '/path/to/somewhere/' -e "pattern"

For directories it's possible to exclude one or more directories using the --exclude-dir parameter. For example, this will exclude the dirs dir1/, dir2/ and all of them matching *.dst/:
> grep --exclude-dir={dir1,dir2,*.dst} -rnw '/path/to/search/' -e "pattern"

This works very well for me, to achieve almost the same purpose like yours.

For more options, see man grep.


# Find in .h
grep --include=\*.h -rnw '/' -e "cudaLaunchConfig_t"


# No Binary file
https://www.gnu.org/software/grep/manual/html_node/Usage.html

To eliminate the “Binary file matches” messages, use the 

    -I or ‘--binary-files=without-match’ 

option.


# Default Options
https://labex.io/questions/how-to-perform-caseinsensitive-search-with-grep-271291
e.g.:
    export GREP_OPTIONS='-i'


# Common scripts
grep --include=\*.{py,md,c,cpp,h,hpp} -Irnw -ie "linux.*path"