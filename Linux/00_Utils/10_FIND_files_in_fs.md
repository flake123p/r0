# Find File
https://superuser.com/questions/400078/how-can-i-search-a-file-by-its-name-and-partial-path

> find . -path "*content/docs/file.xml"

--

$ find . -name *.c -print

find: paths must precede expression

This happens because *.c has been expanded by the shell resulting in find actually receiving a command line like this:

find . -name bigram.c code.c frcode.c locate.c -print

That command is of course not going to work. Instead of doing things this way, you should enclose the pattern in quotes or escape the wild‐ card:

$ find . -name \*.c -print


# Find Folder

> find ~/anaconda3/ -name nvidia -type d


## Without recursion / Set Max Depth
https://stackoverflow.com/questions/3925337/find-without-recursion

-maxdepth levels
          Descend at most levels (a non-negative integer) levels of direc-
          tories below the command line arguments.   `-maxdepth  0'  means
          only  apply the tests and actions to the command line arguments.


# Find & exec

find . -name "*.sh" -exec chmod 755 {} \;