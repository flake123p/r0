# No Chinese Format
       -R, --rfc-email
              output date and time in RFC 5322 format.  Example: Mon, 14 Aug 2006 02:34:56 -0600

e.g.:
    date -R

    date -R > z ; g > log.log ; cat z ; rm z ; date

    date -R > /tmp/flake ; g > log.log ; cat /tmp/flake ; date


# Chinese(Local) Language Format
e.g.:
    date