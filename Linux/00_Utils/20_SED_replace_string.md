# Replace string in stream:
https://www.hy-star.com.tw/tech/linux/sed/sed.html

<!-- replace once -->
: echo 'bbc' | sed 's/b/B/'
> Bbc

<!-- replace all (g) -->
: echo 'bbc' | sed 's/b/B/g'
> BBc

<!-- error replacement -->
: echo 'This is a book' | sed 's/is/IS/g'
> ThIS IS a book

<!-- fix with space -->
: echo 'This is a book' | sed 's/ is/ IS/g'
> This IS a book

<!-- fix with whole word match (regex). Flake: not formal regex!! -->
: echo 'This is a book' | sed 's/\<is\>/IS/g'
> This IS a book

<!-- fix with whole word match (regex word boundaries). Flake: use this!! -->
: echo 'This is a book' | sed 's/\bis\b/IS/g'
> This IS a book