https://peterli.website/如何在ubuntu-16-04或18-04上安裝與設定vnc-server/

https://itnixpro.com/install-and-configure-vnc-server-on-ubuntu-22-04/ **install this**

https://www.shubhamdipt.com/blog/how-to-create-vncpasswda-systemd-service-in-linux/

Diff Desktop
https://bytexd.com/how-to-install-configure-vnc-server-on-ubuntu/


# pre-installation

sudo apt-get update
sudo apt-get install -y xfce4-session
sudo apt-get install -y xfce4 xfce4-goodies autocutsel


# installation

sudo apt install tigervnc-standalone-server tigervnc-xorg-extension
sudo apt install xserver-xorg-core
sudo apt install ubuntu-gnome-desktop
sudo systemctl start gdm


# One Liner

sudo apt-get update

sudo apt-get install -y xfce4-session xfce4 xfce4-goodies autocutsel tigervnc-standalone-server tigervnc-xorg-extension xserver-xorg-core ubuntu-gnome-desktop

sudo systemctl start gdm


# [Personal] set password first

vncpasswd

vim ~/.vnc/xstartup
chmod 777 ~/.vnc/xstartup


## xstartup

#!/bin/sh 
unset SESSION_MANAGER 
unset DBUS_SESSION_BUS_ADDRESS 
exec startxfce4


## kill command

vncserver -kill :*


## init command

vncserver -localhost no -geometry 1920x1080 -depth 24
vncserver -localhost no -geometry 2560x1080 -depth 24
vncserver -localhost no -geometry 960x2200 -depth 24


## service (need more bug fixing)

sudo vim /etc/systemd/system/vncserver_john.service
sudo chmod 777 /etc/systemd/system/vncserver_john.service
sudo systemctl daemon-reload


## vncserver_ai0.service

[Unit]
Description= Tiger VNC Server service john
After=syslog.target network.target

[Service]
Type=forking
User=john
WorkingDirectory=/home/john
#ExecStartPre=/usr/bin/vncserver -kill :*
#ExecStartPre=echo 123 $USER
#ExecStart=echo 124 $USER $PWD

# 6445 + 5900 = 12345
ExecStart=/usr/bin/vncserver -geometry 1920x1080 -depth 24 -localhost no :88
#ExecStop=/usr/bin/vncserver -kill :*


[Install]
WantedBy=multi-user.target


##　Enable service

sudo systemctl start vncserver_john.service
sudo systemctl status vncserver_john.service
sudo systemctl enable vncserver_john.service


## CentOS xstartup:

#!/bin/sh

unset SESSION_MANAGER
unset DBUS_SESSION_BUS_ADDRESS
/etc/X11/xinit/xinitrc
# Assume either Gnome or KDE will be started by default when installed
# We want to kill the session automatically in this case when user logs out. In case you modify
# /etc/X11/xinit/Xclients or ~/.Xclients yourself to achieve a different result, then you should
# be responsible to modify below code to avoid that your session will be automatically killed
if [ -e /usr/bin/gnome-session -o -e /usr/bin/startkde ]; then
vncserver -kill $DISPLAY
fi


## Firefox on VNC

https://askubuntu.com/questions/1399383/how-to-install-firefox-as-a-traditional-deb-package-without-snap-in-ubuntu-22/1404401#1404401



## No Copy-Paste

1. Step by step
2. Ref.

### 1. Step by step: (TigerVNC)
1. sudo vim /etc/tigervnc/vncserver-config-mandatory
2. Add these:
$SendCutText = "1";
$AcceptCutText = "1";
3. Reboot

### 2. Ref
1. Files (TigerVNC)
https://manpages.ubuntu.com/manpages/jammy/man8/tigervncsession.8.html
2. Configs
https://manpages.ubuntu.com/manpages/jammy/man1/Xtigervnc.1.html
3. Ask ChatGPT (不準)
vncserver 怎麼取消 SendCutText