# ls all recursively:
https://docstore.mik.ua/orelly/unix3/upt/ch08_04.htm
ls -R

# ls 2 layer
https://stackoverflow.com/questions/4909751/how-do-i-list-all-the-files-in-a-directory-and-subdirectories-in-reverse-chronol
ls -d * */*

# ls 3 layer
ls -d * */* */*/*