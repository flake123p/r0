# Sym-Link a file: (-f for force)
ln -s <old_one> <new_one> -f


# Sym-Link a folder: (SAME...)
ln -s <old_one> <new_one> -f


# SymLink all files
ln -s ../source/*.bar .


Example:

    <to ln folder!!>

    ln -s ../conda/*/lib/* .

Export:

    export LD_LIBRARY_PATH=~/ws/VENV/pkgs/conda_ln:$LD_LIBRARY_PATH