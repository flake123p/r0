# Link
How to make a temporary file in RAM?
https://unix.stackexchange.com/questions/188536/how-to-make-a-temporary-file-in-ram


# df -T (SHOW : Find out tmps & Memory usage)
Example:
    Filesystem     Type      1K-blocks      Used  Available Use% Mounted on
    tmpfs          tmpfs       6557164      3948    6553216   1% /run
    /dev/nvme0n1p2 ext4      490617784 193520096  272102208  42% /
    tmpfs          tmpfs      32785812    405464   32380348   2% /dev/shm
    tmpfs          tmpfs          5120         4       5116   1% /run/lock
    efivarfs       efivarfs        192       140         48  75% /sys/firmware/efi/efivars
    /dev/nvme0n1p1 vfat         523248      6220     517028   2% /boot/efi
    /dev/sda       ext4     1921725720 488163596 1335870012  27% /home/pupil/sda
    tmpfs          tmpfs       6557160       152    6557008   1% /run/user/1000


# df -h
Example:
    Filesystem      Size  Used Avail Use% Mounted on
    tmpfs           6.3G  4.1M  6.3G   1% /run
    /dev/nvme0n1p2  938G  111G  780G  13% /
    tmpfs            32G  108M   32G   1% /dev/shm
    tmpfs           5.0M  4.0K  5.0M   1% /run/lock
    efivarfs        256K  127K  125K  51% /sys/firmware/efi/efivars
    /dev/nvme0n1p1  511M  6.1M  505M   2% /boot/efi
    tmpfs           6.3G  128K  6.3G   1% /run/user/1000


# Candidate for non-root user
/dev/shm


# Mount a tmpfs
mount -t tmpfs -o size=500m tmpfs /mountpoint


## with memory percentage
    size=[num]%


# KEYWORDS
tmpfs, tempfs, ramdisk