# Periodic Execution (periodic)

while true; do echo -n "This is a test of while loop";date ; sleep 5; done

while true; do date -R; python main.py; date -R; sleep 60; done



# chmod multiple files
// For all *.sh
find . -name "*.sh" -exec chmod 755 {} \;

// For all file
sudo find . -type f -exec sudo chmod 777 {} \;

// Or
sudo find . -type f -name "*.*" -exec sudo chmod 777 {} \;



# Multiple execution
find . -name "*.c" -exec ./run.sh {} \;


# Multiple execution with sort (Not working)
for item in "$(find . -maxdepth 1 -name "*.md" | sort)" ; do echo "$item" ; done


# Multiple execution with sort (OK)
for item in *.c ; do ./run.sh "$item" ; done | tee log.log


# While & Break
workdone=0
while : ; do
  ...
  if [ "$workdone" -ne 0 ]; then
      break
  fi
done

##### Example 1:
workdone=0
while : ; do
  python tester.py  FOLDER=mathBasic/add/  TEST=add_pw_1d_1_1d_1.py  CMP=MY_SW  NO_PRINT_FILTER=ON | tee log.log
  grep CANDI_DUMP log.log ; workdone=$?
  if [ "$workdone" -ne 0 ]; then
      break
  fi
done

##### Example 2:
workdone=0
while : ; do
  python tester.py  FOLDER=mathBasic/add/  TEST=add_pw_1d_1_1d_1.py  CMP=MY_SW  NO_PRINT_FILTER=ON | tee log.log
  grep FAILED log.log ; workdone=$?
  if [ "$workdone" = 0 ]; then
      break
  fi
done