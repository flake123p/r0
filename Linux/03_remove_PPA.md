
# Symptom

```c
$ sudo apt update
Hit:1 http://tw.archive.ubuntu.com/ubuntu jammy InRelease
Hit:2 http://tw.archive.ubuntu.com/ubuntu jammy-updates InRelease                                                                                                                           
Hit:3 http://tw.archive.ubuntu.com/ubuntu jammy-backports InRelease                                                                                                                         
Hit:4 https://dl.google.com/linux/chrome/deb stable InRelease                                                                                                                               
Hit:5 https://repo.steampowered.com/steam stable InRelease                                                                                                                       
Hit:6 https://packages.microsoft.com/repos/code stable InRelease                                                                                           
Hit:7 http://security.ubuntu.com/ubuntu jammy-security InRelease                                     
Ign:8 https://ppa.launchpadcontent.net/ubuntu-on-rails/ppa/ubuntu jammy InRelease
Err:9 https://ppa.launchpadcontent.net/ubuntu-on-rails/ppa/ubuntu jammy Release
  404  Not Found [IP: 185.125.190.80 443]
Reading package lists... Done
E: The repository 'https://ppa.launchpadcontent.net/ubuntu-on-rails/ppa/ubuntu jammy Release' does not have a Release file.
N: Updating from such a repository can't be done securely, and is therefore disabled by default.
N: See apt-secure(8) manpage for repository creation and user configuration details.
```


# Solution

    $ sudo add-apt-repository --remove ppa:ubuntu-on-rails


# More: ppa-purge

    # sudo apt install ppa-purge

    # sudo ppa-purge ppa:some-ppa