
# Backup
```
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH':/home/pupil/temp/p1/pupil_p1/cv/lib'

alias ldlib='echo $LD_LIBRARY_PATH'

alias gitcom='git add -u && git commit -m "update"'
alias mkbu='mkdir build && cd build'
alias rmbu='cd .. && rm build -rf && mkdir build && cd build && cmake .. && make -j6'
alias mak='make -j6'

export PATH="$PATH:/home/pupil/sda/ws/r5_toolchain/body/bin"
alias spike32='spike --isa=rv32i /home/pupil/sda/ws/r5_toolchain/body/riscv32-unknown-elf/bin/pk'

alias vprof='valgrind --tool=callgrind'
alias vshow='kcachegrind'
alias vshowx='unset GTK_PATH && kcachegrind'
alias pdf='wine "/home/pupil/.wine/drive_c/Program Files/Tracker Software/PDF Viewer/PDFXCview.exe" &'
alias r5gdb='riscv32-unknown-elf-gdb -x gdbrc -quiet'
alias rvgdb='riscv32-unknown-elf-gdb -x gdbat -batch -quiet'
alias renod='python ../../misc/python/renode.py'
alias log='python log.py'
alias cyc='python cycle.py'
alias gows='cd ~/temp/p1/pupil_p1/'
# Connect to Share
alias conShare='sudo mount -t cifs -o username=nui_users,password=NUI#1234 //192.168.10.10/share_all/ /home/pupil/sda/ws/nfs/nui_share/'
# HANK29 ip route
alias con29='sudo ip route add 192.168.10.0/24 via 192.168.2.1 dev enx00051bd1071c ; sudo systemctl restart networking'
alias nvsmi='watch -n1 nvidia-smi'
alias cult='ltrace -o __dummy_ltrace.txt -x cu*'
alias fltr='python /home/pupil/sda/ws/r0/Accelerator/cuda/topics/02_trace_api/filter.py'
```

# Good
```
alias condaa='conda activate'
alias condad='conda deactivate'
alias cdb='cd ..'
alias cdbb='cd ../..'
alias cdbbb='cd ../../..'
```
