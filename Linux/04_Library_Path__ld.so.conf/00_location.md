
# 3 methods:
https://unix.stackexchange.com/questions/22926/where-do-executables-look-for-shared-objects-at-runtime

In a nutshell, when it's looking for a dynamic library (.so file) the linker tries:
    directories listed in the LD_LIBRARY_PATH environment variable (DYLD_LIBRARY_PATH on OSX);

    directories listed in the executable's rpath;
    
    directories on the system search path, which (on Linux at least) consists of the entries in 
    /etc/ld.so.conf plus /lib and /usr/lib.


# Restart for ld.so.conf.d
sudo ldconfig


# Linux system library path

https://stackoverflow.com/questions/13428910/how-to-set-the-environment-variable-ld-library-path-in-linux


/etc/ld.so.conf

/etc/ld.so.conf.d

Save and run :
    
    $ sudo ldconfig 

to update the system with this libs.


<!-- Keywords : Linux system library LD_LIBRARY_PATH path -->