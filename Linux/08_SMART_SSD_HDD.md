https://askubuntu.com/questions/528072/how-can-i-check-the-smart-status-of-a-ssd-or-hdd-on-current-versions-of-ubuntu-1

# 1 (Only HDD)

sudo apt install libatasmart-bin

sudo skdump /dev/sda

# 2 (HDD and SSD)

sudo apt install smartmontools

sudo smartctl --all /dev/sda

sudo smartctl --all /dev/nvme0

## Show error log
sudo smartctl -l error /dev/nvme0