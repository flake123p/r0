
# [Folder Size] du -sh .
https://linuxize.com/post/how-get-size-of-file-directory-linux/


# [File with Path] realpath xxx
https://www.baeldung.com/linux/list-files-full-path


# [Turn off screen] xset dpms force off
https://superuser.com/questions/374637/how-to-turn-off-screen-with-shortcut-in-linux


# [File Count] ls | wc -l
https://devconnected.com/how-to-count-files-in-directory-on-linux/


# [apt update Error] cd /etc/apt/sources.list.d
https://stackoverflow.com/questions/53800051/repository-does-not-have-a-release-file-error/61335671#61335671

[trusted=yes]


# [Redirect to /dev/null] date >/dev/null
https://www.cyberciti.biz/faq/how-to-redirect-output-and-errors-to-devnull/
e.g.:
    python pth.py 1>/dev/null 2>/dev/null


# [Print Error Number]
result1=$?
echo $result1


# ack cut sed