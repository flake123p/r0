
# Install
-i
--install
sudo dpkg -i bazelisk-amd64.deb


# Uninstall
-r
--remove
sudo dpkg -i bazelisk


# Uninstall MORE
https://unix.stackexchange.com/questions/195794/how-to-uninstall-a-deb-installed-with-dpkg

First of all you should check if this package is correctly installed in your system and being listed by dpkg tool:

dpkg -l '*urserver*'

It should have an option ii in the first column of the output - that means 'installed ok installed'.

If you'd like to remove the package itself (without the configuration files), you'll have to run:

dpkg -r urserver

If you'd like to delete (purge) the package completely (with configuration files), you'll have to run:

dpkg -P urserver

You may check if the package has been removed successfully - simply run again:

dpkg -l urserver