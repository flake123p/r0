#!/bin/sh
 
if [ "$1" = "123" ] && [ true ]; then
    echo "var1 is 123"
elif [ "$1" = "" ]; then
    echo "var1 is NULL"
else
    echo "var1 is 789"
fi


if [ -f "/home/phpini/testfile" ]; then
    echo "/home/phpini/testfile is a file"
else
    echo "/home/phpini/testfile is not a file"
fi


#
# https://www.digitalocean.com/community/tutorials/if-else-in-shell-scripts
#
# eq	Equality check
# -ne	Inequality check
# -lt	Less Than
# -le	Less Than or Equal
# -gt	Greater Than
# -ge	Greater Than or Equal

#
# https://www.ltsplus.com/linux/shell-script-if-else-elseif
#
# 除了 -f 外，Shell Script 還有很多運算子，包括：

# -d file file 是目錄回傳 true.
# -f file file 是檔案回傳 true.
# -r file file 可讀回傳 true.
# -s file file 的體積大於 0 (不是空檔案) 回傳 true.
# -w file file 可寫入回傳 true.
# -x file file 可執行回傳 true.

# $str1 = str2 判斷 $str1 與 $str2 字串是否相同。
# $str1 != $str2 判斷 $str1 與 $str2 字串是否不相同。
# -n $str 當 $str1 不是 null, 回傳 true.
# -z $str 當 $str1 是 null, 回傳 true.

# var1 -eq var2 var1 等於 var2 回傳 true.
# var1 -ne var2 var1 不等於 var2 回傳 true.
# var1 -gt var2 var1 大過 var2 回傳 true.
# var1 -ge var2 var1 大過或等如 var2 回傳 true.
# var1 -lt var2 var1 小過 var2 回傳 true.
# var1 -le var2 var1 小過或等如 var2 回傳 true.