# https://unix.stackexchange.com/questions/338000/bash-assign-output-of-pipe-to-a-variable
#
# Use command: var=$(date -R) # command
#
# pipe(sub-shell) is not working in BASH, may use zsh ...
#   echo 'hello' | message=$(</dev/stdin)       NOT WORKING on Bash
#   echo 'hello' | read message                 NOT WORKING on Bash

#!/bin/sh

myvar=$(python -c "import site; print(site.getsitepackages()[0])") # command

echo myvar = $myvar