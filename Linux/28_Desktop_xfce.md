
# Install
https://ubuntushell.com/different-desktop-environment-on-ubuntu/

1. sudo apt install xfce4 xfce4-screenshooter xfce4-whiskermenu-plugin
2. log out
3. (in login screen, bottom right icon) select xfce4


# Set main monitor
Unlock panel, drag it to correct monitor, then lock panel.


# Window Buttons (3 small vertical dashes in the right side of [X Applications(top right of screen)])
Properties -> Window grouping -> Select: Never


# Favorite
https://www.debugpoint.com/xfce-whisker-menu/ (Flake: hit this for picture)
sudo apt install xfce4-whiskermenu-plugin

Right-click on the panel and select Panel(X Applications) -> Panel -> Panel preferences -> Items

Select the Items tab and click Add -> Whisker Menu -> Then move it to the top

Tips: add fire fox to Favorite then drag the icon to panel


# Remove workspace (Optional)


# Disable screensaver

    xset s off -dpms