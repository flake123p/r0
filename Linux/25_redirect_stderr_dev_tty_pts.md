# Link
https://superuser.com/questions/901232/redirect-stderr-to-second-terminal-tty


# 2nd terminal (tty):
command: tty
example: /dev/pts/19


# 1st terminal:
./run.sh 2>/dev/pts/19
./run.sh 1>/dev/pts/19