# Basics

Ref:
https://philipzheng.gitbook.io/docker_practice/advanced_network/quick_guide

Image - Read ony template, like simple ubuntu, use it to create container
Container - To execute application, an instance, writable
Repository - Store Images, public repo: Docker Hub, "push" and "pull" an image to repo.
Registry - Multiple Repository.


# Installation

sudo apt update
sudo apt install docker.io


# Pull ubuntu24 image (you can push after login)

sudo docker pull ubuntu:24.04


# Commands

**list images** : sudo docker images
**run cmd** : 
    sudo docker run ubuntu:24.04 pwd
    sudo docker run -t -i ubuntu:24.04 /bin/bash
**remove container** : sudo docker rm
**remove image** : sudo docer rmi
**remove all dangling images** : sudo docker image prune
**Daemonize** : 
    sudo docker run -d ubuntu:24.04 /bin/bash -c "while true; do echo hello world; sleep 1; done"
    sudo docker run -d ubuntu:24.04 /bin/bash -c "while true; do date; sleep 1; done"
**ps alive** : sudo docker ps
**ps all** : sudo docker ps -a
**show logs of daemon** : sudo docker logs stupefied_knuth
**start** : sudo docker start b1d0ee968e59
**stop** : sudo docker stop b1d0ee968e59
**restart** : sudo docker restart b1d0ee968e59
**exec**
**enter/attach** : sudo docker attach flak
**detach** : ctrl + p  ->  ctrl + q
**nsenter**
**export** : sudo docker export 7691a814370e > ubuntu.tar
**import** : cat ubuntu.tar | sudo docker import - test/ubuntu:v1.0


## Search images in Docker Hub
sudo docker search centos
sudo docker search ubuntu

## Automated Builds
...

## Docker registry
...

## Repository flavor
...

## Data volumes (like mount)
use -v


## Data volumes containers
create:
sudo docker run -d -v /dbdata --name dbdata ubuntu:24.04 echo Data-only container for postgres

mount it:
sudo docker run -itd --volumes-from dbdata --name db1 ubuntu:24.04
sudo docker run -itd --volumes-from dbdata --name db2 ubuntu:24.04

use it:
There will be a folder : /dbdata for ...

## Backup a volume
...

## Restore a volume
...

## Access a volume via network
...

## Link containers
...


## pipework
https://github.com/jpetazzo/pipework