
import os, sys, platform

import enum
import logging
import platform
import re
import sys
import tempfile
import time
import traceback
from pathlib import Path

#import gl_utils
import numpy as np
import uvc
#from camera_models import Camera_Model
from pyglui import cygl, ui
#from version_utils import parse_version

#from .base_backend import Base_Manager, Base_Source, InitialisationError, SourceInfo
#from .utils import Check_Frame_Stripes, Exposure_Time

# check versions for our own depedencies as they are fast-changing
#assert parse_version(uvc.__version__) >= parse_version("0.13")

# logging
#logger = logging.getLogger(__name__)
#logger.setLevel(logging.DEBUG)
'''
__name__
__doc__
__package__
__loader__
__spec__
__file__
__builtins__
Frame
Device_List
Control
Capture
np
uvc_error_codes
CaptureError
StreamError
InitError
OpenError
DeviceNotFoundError
logging
logger
__version__
device_list
standard_ctrl_units
get_time_monotonic
is_accessible
__test__
__pyx_unpickle_Enum
'''

print('ver === ', uvc.__version__)
print('nam === ', uvc.__name__)
print('fil === ', uvc.__file__)
#print('doc === ', uvc.__doc__)
#print('dic === ', uvc.__dict__)
#print('loa === ', uvc.__loader__)

print_dict = 0
if print_dict:
    for a in uvc.__dict__:
        print(a)
        #print(type(a))
        pass

#f = open('dict.txt', 'w')
#f.write(str(uvc.Capture))
#f.close()

#from __future__ import print_function
do_default_example = 0
if do_default_example:
    import uvc
    import logging

    logging.basicConfig(level=logging.INFO)

    dev_list = uvc.device_list()
    print(dev_list)
    cap = uvc.Capture(dev_list[0]["uid"])

    # Uncomment the following lines to configure the Pupil 200Hz IR cameras:
    # controls_dict = dict([(c.display_name, c) for c in cap.controls])
    # controls_dict['Auto Exposure Mode'].value = 1
    # controls_dict['Gamma'].value = 200
    import cv2
    print(cap.avaible_modes)
    for x in range(10):
        print(x)
        cap.frame_mode = (640, 480, 30)
        for x in range(100):
            frame = cap.get_frame_robust()
            print(frame.img.shape)
            cv2.imshow("img",frame.gray)
            cv2.waitKey(1)
    cap = None