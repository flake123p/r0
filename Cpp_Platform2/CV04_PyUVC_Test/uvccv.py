import numpy as np
import cv2
import uvc as stub
from uvc import Device_List
from uvc import get_time_monotonic
import time

__version__ = '0.14'

class CaptureError(Exception):
    def __init__(self, message):
        super(CaptureError, self).__init__()
        self.message = message

class StreamError(CaptureError):
    def __init__(self, message):
        super(StreamError, self).__init__(message)
        self.message = message

class InitError(CaptureError):
    def __init__(self, message):
        super(InitError, self).__init__(message)
        self.message = message

class OpenError(InitError):
    def __init__(self, message):
        super(InitError, self).__init__(message)
        self.message = message

class DeviceNotFoundError(InitError):
    def __init__(self, message):
        super(InitError, self).__init__(message)
        self.message = message

'''
def device_list():
    devices = []
    devices.append({'name': 'unknown',
                    'manufacturer':'unknown',
                    'serialNumber': '73A55D40',
                    'idProduct':2075,
                    'idVendor':1133,
                    'device_address':9,
                    'bus_number':1,
                    'uid':'1:9'})
    return devices

class Device_List(list):
    def __init__(self):
        self.update()
        self[:] = device_list()
    def update(self):
        self[:] = device_list()
    def cleanup(self):
        pass
'''
class uvccv_Frame():
    def __init__(self,f):
        h, w, c = f.shape
        f = cv2.rotate(f, cv2.ROTATE_180)
        self.img = f
        self.bgr = f
        self.width = w
        self.height = h
        self.index = 0
        g = cv2.cvtColor(f, cv2.COLOR_BGR2GRAY)
        #g = cv2.rotate(g, cv2.ROTATE_180)
        self.gray = g
        self.timestamp = 0.0
        self.yuv_buffer = cv2.cvtColor(f, cv2.COLOR_BGR2YUV)
        self.yuv_subsampling = 0 #https://github.com/libjpeg-turbo/libjpeg-turbo/blob/main/turbojpeg.h
        
class uvccv_Capture():
    def __init__(self, uid, proc):
        print('uvccv init 1 ... uid = ', uid)
        self.stub = stub.Capture(uid)
        self.controls = self.stub.controls
        self.stub.close()
        x = 2
        print('uvccv init 2 ... sleep ', x)
        time.sleep(x)
        self.en_file = 1
        if self.en_file:
            #self.cap = cv2.VideoCapture('/home/ai0/sda/exp/cv/3d.avi')
            if proc == 0:
                self.cap = cv2.VideoCapture('/home/ai0/sda/sci_gaze/video3/frame.avi') 
            else:
                self.cap = cv2.VideoCapture('/home/ai0/sda/exp/cv/3d.avi')
        else:
            self.cap = cv2.VideoCapture(0)
        w = self.cap.get(cv2.CAP_PROP_FRAME_WIDTH)   # float `width`
        h = self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT)  # float `height`
        fps = self.cap.get(cv2.CAP_PROP_FPS)
        self.frame_size = (w, h)
        self.frame_rate = fps
        self.bandwidth_factor = 2.0
        self.name = uid
        #self.controls = {'Auto Exposure Mode': 8, 'Auto Exposure Priority': 1, 'Absolute Exposure Time': 336, 'Backlight Compensation': 0, 'Brightness': 128, 'Contrast': 32, 'Gain': 64, 'Power Line frequency': 2, 'Saturation': 32, 'Sharpness': 24, 'White Balance temperature': 4150, 'White Balance temperature,Auto': 1}
        self.frame_sizes = [self.frame_size]
        self.frame_rates = [self.frame_rate]
        print('uvccv init w = ', w)
        print('uvccv init h = ', h)
        print('uvccv init fps = ', fps)
        print('uvccv init done ...')
        self.ts = 100

    def get_frame(self, timeout):
        #print('uvccv get_frame 1 ...')
        ret, f = self.cap.read()
        time.sleep(timeout)
        if ret:
            fr = uvccv_Frame(f)
            fr.timestamp = self.ts
            self.ts = self.ts + 1
            return fr
        else:
            return None
    def get_frame_robust(self):
        return self.get_frame(0)
    def close(self):
        print('uvccv close ...')
        self.cap.release()

def Capture(uid, proc):
    return uvccv_Capture(uid, proc)

def is_accessible(dev_uid):
    return True
