import os, sys, platform

import enum
import logging
import platform
import re
import sys
import tempfile
import time
import traceback
from pathlib import Path

#import gl_utils
import numpy as np
#import uvc
import uvccv as uvc
#from camera_models import Camera_Model
from pyglui import cygl, ui
#from version_utils import parse_version

devices = uvc.Device_List()
print(devices)
devices_by_name = {dev["name"]: dev for dev in devices}
#print('devices_by_name = ', devices_by_name)
uid = ''
for d_name in devices_by_name.keys():
    uid_for_name = devices_by_name[d_name]["uid"]
    uid = uid_for_name
    print('uid = ', uid)
preferred_names = 'iam9527'
if uid:
    try:
        uvc_capture = uvc.Capture(uid, 0)
    except uvc.OpenError:
        print(
            f"Camera matching {preferred_names} found but not available"
        )
    except uvc.InitError:
        print("Camera failed to initialize.")
    except uvc.DeviceNotFoundError:
        print(
            "No camera found that matched {}".format(preferred_names)
        )
    #uvc_capture.frame_size = (640, 480)
    #uvc_capture.frame_rate = 30

    if 0:
        print('uvc_capture.name = ', uvc_capture.name)
        print('uvc_capture.controls = ', uvc_capture.controls)
        for control in uvc_capture.controls:
            print('control = ', control)
        print('uvc_capture.bandwidth_factor = ', uvc_capture.bandwidth_factor)
        print('uvc_capture.frame_size = ', uvc_capture.frame_size)
        print('uvc_capture.frame_rate = ', uvc_capture.frame_rate)
        print('uvc_capture.frame_sizes = ', uvc_capture.frame_sizes)
        #print('uvc_capture.frame_rates = ', uvc_capture.frame_rates) #??????

    controls_dict = dict([(c.display_name, c) for c in uvc_capture.controls])

    if 1:
        import cv2
        #cap.frame_mode = (640, 480, 30)
        for x in range(100):
            frame = uvc_capture.get_frame(0)
            #frame = uvc_capture.get_frame_robust()
            
            #print(frame.img.shape)
            cv2.imshow("img",frame.bgr)
            cv2.waitKey(80)

    #
    # if uid:
    #
    uvc_capture.close()
