
#ifndef __LIB_MSG_THR_HPP_INCLUDED_

typedef void * MSG_THR_HANDLE_t;

int LibMsgThr_NewHandle(MSG_THR_HANDLE_t *hdl_ptr, uint32_t priority);
int LibMsgThr_DestroyHandle(MSG_THR_HANDLE_t hdl);
int LibMsgThr_Create(MSG_THR_HANDLE_t hdl, ThreadEntryFunc cb, void *arg);
int LibMsgThr_WaitThread(MSG_THR_HANDLE_t hdl);
void LibMsgThr_MsgSend(MSG_THR_HANDLE_t hdl, void *msg);
void *LibMsgThr_MsgRecieve(MSG_THR_HANDLE_t hdl);
void LibMsgThr_EventWait(MSG_THR_HANDLE_t hdl);
void *LibMsgThr_WaitAndRecieve(MSG_THR_HANDLE_t hdl);
void *LibMsgThr_MsgGet(uint32_t size);
int LibMsgThr_MsgRelease(void *ptr);

int LibMsgThr_NewHandle_NoOS(MSG_THR_HANDLE_t *hdl_ptr, uint32_t priority);
int LibMsgThr_DestroyHandle_NoOS(MSG_THR_HANDLE_t hdl);
void LibMsgThr_MsgSend_NoOS(MSG_THR_HANDLE_t hdl, void *msg);
void *LibMsgThr_MsgRecieve_NoOS(MSG_THR_HANDLE_t hdl);

void LibMsgThr_Demo(void);

#define __LIB_MSG_THR_HPP_INCLUDED_
#endif
