#ifndef _MY_DEMO_HPP_INCLUDED_

#include <My_Basics.hpp>

class DemoBasic {
public:
    int i;
    int *p;
    DemoBasic(int in_i=9999){
        i = in_i;
        p = NULL;
        printf("construct DemoBasic - %d, %p\n", i, p);};
    ~DemoBasic(){printf("destruct DemoBasic - %d, %p\n", i, p);};
};

#define _MY_DEMO_HPP_INCLUDED_
#endif//_MY_DEMO_HPP_INCLUDED_

