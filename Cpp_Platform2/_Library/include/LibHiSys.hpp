
#ifndef __LIB_HI_SYS_HPP_INCLUDED_

typedef struct {
    uint32_t id;
    union {
        uint32_t val;
        uint32_t ret;
    };
    uint32_t size; // include sizeof(msg_t)
    uint32_t padding;
} msg_t;

typedef void *(*task_cb_t)(void *); //new
typedef int (*sm_cb_t)(msg_t *msg); //new

typedef struct {
    MSG_THR_HANDLE_t task_hdl;
    task_cb_t task_cb;
    sm_cb_t sm_cb;
    int sm_ret; //deprecate in the future?
} sys_hdl_t;

typedef void (*task_cb_lite_t)(sys_hdl_t *); //new

void *LibHiSys_GenericTask(void *hdl);

void LibHiSys_Demo();
void LibHiSys_Demo2();
void LibHiSys_Demo_NoOS();
void LibHiSys_Demo_NoOS2();

#define __LIB_HI_SYS_HPP_INCLUDED_
#endif
