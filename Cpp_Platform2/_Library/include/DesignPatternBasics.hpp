
#ifndef _DESIGN_PATTERN_BASICS_HPP_INCLUDED_


class It {
public:
	virtual class It *First() = 0;
	virtual class It *Next() = 0;
	virtual int IsDone() const = 0;
	virtual class It *CurrentItem() const = 0;
};



#define _DESIGN_PATTERN_BASICS_HPP_INCLUDED_
#endif//_DESIGN_PATTERN_BASICS_HPP_INCLUDED_

