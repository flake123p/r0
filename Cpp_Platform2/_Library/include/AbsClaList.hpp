
#ifndef _ABSTRACT_CLASS_LIST_HPP_INCLUDED_

#include "DesignPatternBasics.hpp"
#include "UniversalQueueMacros.h"

// DLList_Head_t
class ACList : public It{
public:
	DLList_Head_t *pHead;
	DLList_Entry_t entry;
	class ACList *curr;
	virtual class ACList *First() { curr = (class ACList *)pHead->head; return curr; };
	virtual class ACList *Next() { if(curr != NULL)curr = (class ACList *)curr->entry.next; return curr; };
	virtual int IsDone() const { return curr == NULL;};
	virtual class ACList *CurrentItem() const { return curr; };

	void InsertLast(void) { DLLIST_INSERT_LAST2(pHead, this, class ACList); };
};


#define _ABSTRACT_CLASS_LIST_HPP_INCLUDED_
#endif//_ABSTRACT_CLASS_LIST_HPP_INCLUDED_

