
#include "Everything_Lib_Mgr.hpp"

void *LibHiSys_GenericTask(void *hdl)
{
    sys_hdl_t *sys = (sys_hdl_t *)hdl;
    msg_t *msg;

    while(1)
    {
        msg = (msg_t *)LibMsgThr_WaitAndRecieve(sys->task_hdl);
        if (msg) {
            BASIC_ASSERT(sys->sm_cb != NULL);
            sys->sm_ret = sys->sm_cb(msg);
            if (sys->sm_ret) {
                return NULL;
            }
        }
    }
    return NULL;
}

typedef struct {
    msg_t hdr;
    int a;
    int b;
    int ab_add;
    int ab_mul;
} demo_msg_t;

static sys_hdl_t g_sys_hi;
static sys_hdl_t g_sys_lo;
static sys_hdl_t g_sys_tool;

static int no_os = 0;
static int ret_hi = 0;
static int ret_lo = 0;
static void Demo_Reset(int no_os_en)
{
    no_os = no_os_en;
    ret_hi = 0;
    ret_lo = 0;
}
static void Demo_MsgSend(MSG_THR_HANDLE_t hdl, void *msg)
{
    if (no_os) {
        LibMsgThr_MsgSend_NoOS(hdl, msg);
    } else {
        LibMsgThr_MsgSend(hdl, msg);
    }
}

static int state_hi(demo_msg_t *msg)
{
    demo_msg_t *msg_2_lo = (demo_msg_t *)LibMsgThr_MsgGet(sizeof(demo_msg_t));
    //printf("hi %d %d %d\n", msg->hdr.id, msg->a, msg->b);
    *msg_2_lo = *msg;
    msg_2_lo->ab_add = msg->a + msg->b;
    Demo_MsgSend(g_sys_lo.task_hdl, msg_2_lo);

    if (msg->hdr.id == 999) {
        LibMsgThr_MsgRelease(msg);
        return 1;
    }
    LibMsgThr_MsgRelease(msg);
    return 0;
}

static int state_lo(demo_msg_t *msg)
{
    demo_msg_t *msg_2_tool = (demo_msg_t *)LibMsgThr_MsgGet(sizeof(demo_msg_t));
    //printf("lo %d %d %d\n", msg->hdr.id, msg->a, msg->b);
    *msg_2_tool = *msg;
    msg_2_tool->ab_mul = msg->a * msg->b;
    Demo_MsgSend(g_sys_tool.task_hdl, msg_2_tool);

    if (msg->hdr.id == 999) {
        LibMsgThr_MsgRelease(msg);
        return 1;
    }
    LibMsgThr_MsgRelease(msg);
    return 0;
}

static int state_tool(demo_msg_t *msg)
{
    demo_msg_t *msg_2_hi;

    DND(msg->ab_add);
    DND(msg->ab_mul);
    if (msg->hdr.id == 999) {
        LibMsgThr_MsgRelease(msg);
        return 1;
    }

    msg_2_hi = (demo_msg_t *)LibMsgThr_MsgGet(sizeof(demo_msg_t));
    msg_2_hi->hdr.id = 111;
    msg_2_hi->a = msg->a + 1;
    msg_2_hi->b = msg->b + 1;

    if (msg_2_hi->b == 9) {
        msg_2_hi->hdr.id = 999;
    }
    Demo_MsgSend(g_sys_hi.task_hdl, msg_2_hi);
    LibMsgThr_MsgRelease(msg);
    return 0;
}

static void Demo_MsgSend2(MSG_THR_HANDLE_t hdl, void *msg)
{
    if (no_os) {
        LibMsgThr_MsgSend_NoOS(hdl, msg);
        {
            demo_msg_t *msg;
            while (1) {
                if (ret_hi == 0) {
                    msg = (demo_msg_t *)LibMsgThr_MsgRecieve_NoOS(g_sys_hi.task_hdl);
                    if (msg) {
                        ret_hi = state_hi(msg);
                        continue;
                    }
                }
                if (ret_lo == 0) {
                    msg = (demo_msg_t *)LibMsgThr_MsgRecieve_NoOS(g_sys_lo.task_hdl);
                    if (msg) {
                        ret_lo = state_lo(msg);
                        continue;
                    }
                }
                break;
            }
        }
    } else {
        LibMsgThr_MsgSend(hdl, msg);
    }
}

void *Demo_WaitAndRecieve2(MSG_THR_HANDLE_t hdl)
{
    if (no_os) {
        return LibMsgThr_MsgRecieve_NoOS(hdl);
    } else {
        return LibMsgThr_WaitAndRecieve(hdl);
    }
}

void *ToolTask(void *hdl)
{
    sys_hdl_t *sys = (sys_hdl_t *)hdl;
    demo_msg_t *msg_2_hi = (demo_msg_t *)LibMsgThr_MsgGet(sizeof(demo_msg_t));
    demo_msg_t *msg;

    msg_2_hi->hdr.id = 111;
    msg_2_hi->a = 3;
    msg_2_hi->b = 5;
    Demo_MsgSend2(g_sys_hi.task_hdl, msg_2_hi);

    msg_2_hi = (demo_msg_t *)LibMsgThr_MsgGet(sizeof(demo_msg_t));
    msg_2_hi->hdr.id = 111;
    msg_2_hi->a = 4;
    msg_2_hi->b = 7;
    Demo_MsgSend2(g_sys_hi.task_hdl, msg_2_hi);

    msg_2_hi = (demo_msg_t *)LibMsgThr_MsgGet(sizeof(demo_msg_t));
    msg_2_hi->hdr.id = 999;
    msg_2_hi->a = 5;
    msg_2_hi->b = 8;
    Demo_MsgSend2(g_sys_hi.task_hdl, msg_2_hi);

    msg = (demo_msg_t *)Demo_WaitAndRecieve2(sys->task_hdl);
    BASIC_ASSERT(msg != NULL);
    DND(msg->ab_add);
    DND(msg->ab_mul);
    LibMsgThr_MsgRelease(msg);

    msg = (demo_msg_t *)Demo_WaitAndRecieve2(sys->task_hdl);
    BASIC_ASSERT(msg != NULL);
    DND(msg->ab_add);
    DND(msg->ab_mul);
    LibMsgThr_MsgRelease(msg);

    msg = (demo_msg_t *)Demo_WaitAndRecieve2(sys->task_hdl);
    BASIC_ASSERT(msg != NULL);
    DND(msg->ab_add);
    DND(msg->ab_mul);
    LibMsgThr_MsgRelease(msg);

    return NULL;
}

void LibHiSys_Demo()
{
    PRLOC
    Demo_Reset(0);
    Lib_InitMT();
    g_sys_hi.task_cb = LibHiSys_GenericTask;
    g_sys_lo.task_cb = LibHiSys_GenericTask;
    g_sys_tool.task_cb = ToolTask;

    //sm
    g_sys_hi.sm_cb = (sm_cb_t)state_hi;
    g_sys_lo.sm_cb = (sm_cb_t)state_lo;

    LibMsgThr_NewHandle(&g_sys_hi.task_hdl, 0);
    LibMsgThr_NewHandle(&g_sys_lo.task_hdl, 0);
    LibMsgThr_NewHandle(&g_sys_tool.task_hdl, 0);

    LibMsgThr_Create(g_sys_hi.task_hdl, g_sys_hi.task_cb, &g_sys_hi);
    LibMsgThr_Create(g_sys_lo.task_hdl, g_sys_lo.task_cb, &g_sys_lo);
    LibMsgThr_Create(g_sys_tool.task_hdl, g_sys_tool.task_cb, &g_sys_tool);

    LibMsgThr_WaitThread(g_sys_hi.task_hdl);
    LibMsgThr_WaitThread(g_sys_lo.task_hdl);
    LibMsgThr_WaitThread(g_sys_tool.task_hdl);

    LibMsgThr_DestroyHandle(g_sys_hi.task_hdl);
    LibMsgThr_DestroyHandle(g_sys_lo.task_hdl);
    LibMsgThr_DestroyHandle(g_sys_tool.task_hdl);
    Lib_Uninit();
}

void LibHiSys_Demo2()
{
    PRLOC
    Demo_Reset(0);
    Lib_InitMT();
    g_sys_hi.task_cb = LibHiSys_GenericTask;
    g_sys_lo.task_cb = LibHiSys_GenericTask;
    g_sys_tool.task_cb = LibHiSys_GenericTask;

    //sm
    g_sys_hi.sm_cb = (sm_cb_t)state_hi;
    g_sys_lo.sm_cb = (sm_cb_t)state_lo;
    g_sys_tool.sm_cb = (sm_cb_t)state_tool;

    LibMsgThr_NewHandle(&g_sys_hi.task_hdl, 0);
    LibMsgThr_NewHandle(&g_sys_lo.task_hdl, 0);
    LibMsgThr_NewHandle(&g_sys_tool.task_hdl, 0);

    LibMsgThr_Create(g_sys_hi.task_hdl, g_sys_hi.task_cb, &g_sys_hi);
    LibMsgThr_Create(g_sys_lo.task_hdl, g_sys_lo.task_cb, &g_sys_lo);
    LibMsgThr_Create(g_sys_tool.task_hdl, g_sys_tool.task_cb, &g_sys_tool);

    {
        demo_msg_t *msg_2_hi = (demo_msg_t *)LibMsgThr_MsgGet(sizeof(demo_msg_t));
        msg_2_hi->hdr.id = 111;
        msg_2_hi->a = 6;
        msg_2_hi->b = 6;
        LibMsgThr_MsgSend(g_sys_hi.task_hdl, msg_2_hi);
    }

    LibMsgThr_WaitThread(g_sys_hi.task_hdl);
    LibMsgThr_WaitThread(g_sys_lo.task_hdl);
    LibMsgThr_WaitThread(g_sys_tool.task_hdl);

    LibMsgThr_DestroyHandle(g_sys_hi.task_hdl);
    LibMsgThr_DestroyHandle(g_sys_lo.task_hdl);
    LibMsgThr_DestroyHandle(g_sys_tool.task_hdl);
    Lib_Uninit();
}

void LibHiSys_Demo_NoOS()
{
    PRLOC
    Demo_Reset(1);
    Lib_InitMT();

    LibMsgThr_NewHandle(&g_sys_hi.task_hdl, 0);
    LibMsgThr_NewHandle(&g_sys_lo.task_hdl, 0);
    LibMsgThr_NewHandle(&g_sys_tool.task_hdl, 0);

    ToolTask(&g_sys_tool);

    LibMsgThr_DestroyHandle(g_sys_hi.task_hdl);
    LibMsgThr_DestroyHandle(g_sys_lo.task_hdl);
    LibMsgThr_DestroyHandle(g_sys_tool.task_hdl);
    Lib_Uninit();
}

void LibHiSys_Demo_NoOS2()
{
    PRLOC
    Demo_Reset(1);
    Lib_InitMT();

    LibMsgThr_NewHandle(&g_sys_hi.task_hdl, 0);
    LibMsgThr_NewHandle(&g_sys_lo.task_hdl, 0);
    LibMsgThr_NewHandle(&g_sys_tool.task_hdl, 0);

    {
        demo_msg_t *msg_2_hi = (demo_msg_t *)LibMsgThr_MsgGet(sizeof(demo_msg_t));
        msg_2_hi->hdr.id = 111;
        msg_2_hi->a = 6;
        msg_2_hi->b = 6;
        Demo_MsgSend(g_sys_hi.task_hdl, msg_2_hi);
    }

    {
        int ret_hi = 0;
        int ret_lo = 0;
        int ret_tool = 0;
        demo_msg_t *msg;
        while (1) {
            if (ret_hi == 0) {
                msg = (demo_msg_t *)LibMsgThr_MsgRecieve_NoOS(g_sys_hi.task_hdl);
                if (msg) {
                    ret_hi = state_hi(msg);
                }
            }
            if (ret_lo == 0) {
                msg = (demo_msg_t *)LibMsgThr_MsgRecieve_NoOS(g_sys_lo.task_hdl);
                if (msg) {
                    ret_lo = state_lo(msg);
                }
            }
            if (ret_tool == 0) {
                msg = (demo_msg_t *)LibMsgThr_MsgRecieve_NoOS(g_sys_tool.task_hdl);
                if (msg) {
                    ret_tool = state_tool(msg);
                }
            }
            if (ret_hi && ret_lo && ret_tool) {
                break;
            }
        }
    }

    LibMsgThr_DestroyHandle(g_sys_hi.task_hdl);
    LibMsgThr_DestroyHandle(g_sys_lo.task_hdl);
    LibMsgThr_DestroyHandle(g_sys_tool.task_hdl);
    Lib_Uninit();
}