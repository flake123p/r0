
#include "Everything_Lib_Mgr.hpp"

typedef struct {
    void *thre;
    EVENT_HANDLE_t q_evt;
    MUTEX_HANDLE_t q_mutex;
    LList_Head_t q_head;
} _MSG_THR_t;
typedef struct {
    LList_Entry_t en;
    int instance[];
} _msg_t;
int LibMsgThr_NewHandle(MSG_THR_HANDLE_t *hdl_ptr, uint32_t priority)
{
    int ret = 0;

    //TODO...
    _MSG_THR_t *task = (_MSG_THR_t *)MM_ALLOC(sizeof(_MSG_THR_t));
    BASIC_ASSERT(task != NULL);

    ret = LibThread_NewHandle(&task->thre, (THREAD_PRIORITY_t)priority);
    BASIC_ASSERT(ret == 0);
    BASIC_ASSERT(task->thre != NULL);
    if(ret) return ret;

    ret = LibIPC_Event_Create(&task->q_evt);
    BASIC_ASSERT(ret == 0);
    BASIC_ASSERT(task->q_evt != NULL);
    if(ret) return ret;

    ret = LibIPC_Mutex_Create(&task->q_mutex);
    BASIC_ASSERT(ret == 0);
    BASIC_ASSERT(task->q_mutex != NULL);
    if(ret) return ret;

    LLIST_HEAD_RESET(&task->q_head);

    *hdl_ptr = (MSG_THR_HANDLE_t)task;

    return 0;
}

int LibMsgThr_NewHandle_NoOS(MSG_THR_HANDLE_t *hdl_ptr, uint32_t priority)
{
    //int ret = 0;

    //TODO...
    _MSG_THR_t *task = (_MSG_THR_t *)MM_ALLOC(sizeof(_MSG_THR_t));
    BASIC_ASSERT(task != NULL);

    priority = priority;
    // ret = LibThread_NewHandle(&task->thre, (THREAD_PRIORITY_t)priority);
    // BASIC_ASSERT(ret == 0);
    // BASIC_ASSERT(task->thre != NULL);
    // if(ret) return ret;

    // ret = LibIPC_Event_Create(&task->q_evt);
    // BASIC_ASSERT(ret == 0);
    // BASIC_ASSERT(task->q_evt != NULL);
    // if(ret) return ret;

    // ret = LibIPC_Mutex_Create(&task->q_mutex);
    // BASIC_ASSERT(ret == 0);
    // BASIC_ASSERT(task->q_mutex != NULL);
    // if(ret) return ret;

    LLIST_HEAD_RESET(&task->q_head);

    *hdl_ptr = (MSG_THR_HANDLE_t)task;

    return 0;
}

int LibMsgThr_DestroyHandle(MSG_THR_HANDLE_t hdl)
{
    int ret = 0;
    _MSG_THR_t *task = (_MSG_THR_t *)hdl;

    LibThread_DestroyHandle(task->thre);

    LibIPC_Event_Destroy(task->q_evt);

    LibIPC_Mutex_Destroy(task->q_mutex);

    MM_FREE(hdl);

    return ret;
}

int LibMsgThr_DestroyHandle_NoOS(MSG_THR_HANDLE_t hdl)
{
    // int ret = 0;
    // _MSG_THR_t *task = (_MSG_THR_t *)hdl;

    // LibThread_DestroyHandle(task->thre);

    // LibIPC_Event_Destroy(task->q_evt);

    // LibIPC_Mutex_Destroy(task->q_mutex);

    MM_FREE(hdl);

    return 0;
}

int LibMsgThr_Create(MSG_THR_HANDLE_t hdl, ThreadEntryFunc cb, void *arg)
{
    _MSG_THR_t *task = (_MSG_THR_t *)hdl;
    return LibThread_Create(task->thre, cb, arg);
}

int LibMsgThr_WaitThread(MSG_THR_HANDLE_t hdl)
{
    _MSG_THR_t *task = (_MSG_THR_t *)hdl;
    return LibThread_WaitThread(task->thre);
}

void LibMsgThr_MsgSend(MSG_THR_HANDLE_t hdl, void *msg)
{
    _MSG_THR_t *task = (_MSG_THR_t *)hdl;
    _msg_t *_msg = STRUCT_ENTRY(msg, _msg_t, instance);

    //printf("1 %p\n", msg);
    //printf("2 %p\n", STRUCT_ENTRY(msg, smsg_t, instance));

    // lock linked list
    LibIPC_Mutex_Lock(task->q_mutex);

    //printf("1 %d\n", msg->id);
    WT(_msg->en.prev, NULL);
    WT(_msg->en.next, NULL);
    {
        //int *tt = (int *)msg;
        //WT(tt[20], 0);
    }

    //LLIST_INSERT_LAST(&task->q_head, msg);
    LLIST_INSERT_LAST(&task->q_head, _msg);

    LibIPC_Mutex_Unlock(task->q_mutex);

    // wake thread
    LibIPC_Event_Set(task->q_evt);
}

void LibMsgThr_MsgSend_NoOS(MSG_THR_HANDLE_t hdl, void *msg)
{
    _MSG_THR_t *task = (_MSG_THR_t *)hdl;
    _msg_t *_msg = STRUCT_ENTRY(msg, _msg_t, instance);

    //printf("1 %p\n", msg);
    //printf("2 %p\n", STRUCT_ENTRY(msg, smsg_t, instance));

    // lock linked list
    //LibIPC_Mutex_Lock(task->q_mutex);

    //printf("1 %d\n", msg->id);
    //WT(_msg->en.prev, NULL);
    //WT(_msg->en.next, NULL);
    {
        //int *tt = (int *)msg;
        //WT(tt[20], 0);
    }

    //LLIST_INSERT_LAST(&task->q_head, msg);
    LLIST_INSERT_LAST(&task->q_head, _msg);

    //LibIPC_Mutex_Unlock(task->q_mutex);

    // wake thread
    //LibIPC_Event_Set(task->q_evt);
}

void *LibMsgThr_MsgRecieve(MSG_THR_HANDLE_t hdl)
{
    _MSG_THR_t *task = (_MSG_THR_t *)hdl;
    _msg_t *_msg;

    // lock linked list
    LibIPC_Mutex_Lock(task->q_mutex);

    if (LLIST_IS_EMPTY(&task->q_head)) {
        LibIPC_Mutex_Unlock(task->q_mutex);
        return NULL;
    } else {
        _msg = (_msg_t *)LLIST_FIRST(&task->q_head);
        LLIST_REMOVE_FIRST_SAFELY(&task->q_head);
    }

    LibIPC_Mutex_Unlock(task->q_mutex);

    return _msg->instance;
}

void *LibMsgThr_MsgRecieve_NoOS(MSG_THR_HANDLE_t hdl)
{
    _MSG_THR_t *task = (_MSG_THR_t *)hdl;
    _msg_t *_msg;

    // lock linked list
    //LibIPC_Mutex_Lock(task->q_mutex);

    if (LLIST_IS_EMPTY(&task->q_head)) {
        //LibIPC_Mutex_Unlock(task->q_mutex);
        return NULL;
    } else {
        _msg = (_msg_t *)LLIST_FIRST(&task->q_head);
        LLIST_REMOVE_FIRST_SAFELY(&task->q_head);
    }

    //LibIPC_Mutex_Unlock(task->q_mutex);

    return _msg->instance;
}

void LibMsgThr_EventWait(MSG_THR_HANDLE_t hdl)
{
    _MSG_THR_t *task = (_MSG_THR_t *)hdl;
    LibIPC_Event_Wait(task->q_evt);
}

void *LibMsgThr_WaitAndRecieve(MSG_THR_HANDLE_t hdl)
{
    void *msg;

    while (1) {
        msg = LibMsgThr_MsgRecieve(hdl);
        if (msg) {
            return msg;
        } else {
            LibMsgThr_EventWait(hdl);
        }
    }
}

void *LibMsgThr_MsgGet(uint32_t size)
{
    _msg_t *_msg;
    _msg = (_msg_t *)MM_ALLOC(size + sizeof(_msg_t));
    return _msg->instance;
}

int LibMsgThr_MsgRelease(void *ptr)
{
    void *entry;
    entry = (void *)STRUCT_ENTRY(ptr, _msg_t, instance);
    MM_FREE(entry); 
    return 0;
}

typedef struct {
    int a;
    int b;
} LibMsgThre_demo_struct_t;

void *task_demo(void *hdl)
{
    LibMsgThre_demo_struct_t *msg;

    while(1)
    {
        LibMsgThr_EventWait(hdl);
        while (1) {
            msg = (LibMsgThre_demo_struct_t *)LibMsgThr_MsgRecieve(hdl);
            if (msg) {
                DND(msg->a);
                DND(msg->b);
                if (msg->b == 999) {
                    LibMsgThr_MsgRelease(msg);
                    return NULL;
                } else {
                    LibMsgThr_MsgRelease(msg);
                }
            } else {
                break;
            }
        }
    }
    return NULL;
}

void LibMsgThr_Demo(void)
{
    PRLOC;
    {
        Lib_InitMT();
    }
    MSG_THR_HANDLE_t hdl;
    LibMsgThre_demo_struct_t *m1 = (LibMsgThre_demo_struct_t *)LibMsgThr_MsgGet(sizeof(LibMsgThre_demo_struct_t));
    LibMsgThre_demo_struct_t *m2 = (LibMsgThre_demo_struct_t *)LibMsgThr_MsgGet(sizeof(LibMsgThre_demo_struct_t));
    m1->a = 11;
    m1->b = 22;
    m2->a = 111;
    m2->b = 999;
    LibMsgThr_NewHandle(&hdl, 0);
    LibMsgThr_MsgSend(hdl, m1);
    LibMsgThr_Create(hdl, task_demo, hdl);
    LibMsgThr_MsgSend(hdl, m2);
    LibMsgThr_WaitThread(hdl);
    LibMsgThr_DestroyHandle(hdl);
    {
        Lib_Uninit();
    }
}