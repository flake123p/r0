
#include "Everything_Lib_Mgr.hpp"

#define ENABLE_DIAGNOSE ( 1 )
#if ENABLE_DIAGNOSE
#define DIAGNOSE() LibLinkedList_Diagnose(info, __func__)
#else
#define DIAGNOSE()
#endif

void LibLinkedList_InitInfo(LinkedListInfo *info)
{
	info->head = NULL;
	info->tail = NULL;
	info->count = 0;

	DIAGNOSE();
}

u32 LibLinkedList_Length(LinkedListInfo *info)
{
	DIAGNOSE();
	
	return info->count;
}

void LibLinkedList_PushBack(LinkedListInfo *info, LinkedListNode *newNode)
{
	if (info->head == NULL) {
		info->head = newNode;
		info->tail = newNode;
		newNode->next = NULL;
		newNode->prev = NULL;
	} else {
		// 1. Handle tail
		LinkedListNode *oldLastnode = info->tail;
		info->tail = newNode;

		// 2. Handle old last node
		oldLastnode->next = newNode;

		// 3. Handle new node
		newNode->next = NULL;
		newNode->prev = oldLastnode;
	}

	info->count++;

	DIAGNOSE();
}

void LibLinkedList_PopBack(LinkedListInfo *info, AUTO_FREE autoFree /* = 0 */)
{
	if (info->head == NULL) {
		DIAGNOSE();
		return;
	}
	
	LinkedListNode *nodeForPop = info->tail;

	// 1 Handle head
	if (NODE_IS_HEAD(nodeForPop)) {
		info->head = NULL;
		info->tail = NULL;
	} else {
		LinkedListNode *nodeBeforePop = nodeForPop->prev;
		BASIC_ASSERT(nodeForPop->prev != NULL);
		
		// 2 Handle tail
		info->tail = nodeBeforePop;

		// 3 Handle prev node
		nodeBeforePop->next = NULL;
		
		// 4 Handle next node
		//NONE
	}

	info->count--;
	
	if (autoFree == DO_AUTO_FREE) {
		free(nodeForPop);
	}
	
	DIAGNOSE();
}

// Return remove node count, 0 for nothing removed
int LibLinkedList_Remove(LinkedListInfo *info, LinkedListNode *nodeForRemove, AUTO_FREE autoFree /* = 0 */)
{
	if (NODE_ISNT_IN_LIST ==LibLinkedList_IsThisNodeInList(info, nodeForRemove)) {
		return NODE_ISNT_IN_LIST;
	}

	BASIC_ASSERT(info->head != NULL);

	LinkedListNode *nodeBeforeRemove = nodeForRemove->prev;
	LinkedListNode *nodeAfterRemove = nodeForRemove->next;
	
	// 1 Handle head
	if (NODE_IS_HEAD(nodeForRemove)) {
		info->head = nodeAfterRemove;
	}
	// 2 Handle tail
	if (NODE_IS_TAIL(nodeForRemove)) {
		info->tail = nodeBeforeRemove;
	}

	// 3 Handle prev node
	if (nodeBeforeRemove != NULL) {
		nodeBeforeRemove->next = nodeAfterRemove;
	}

	// 4 Handle next node
	if (nodeAfterRemove != NULL) {
		nodeAfterRemove->prev = nodeBeforeRemove;
	}
	
	info->count--;
	
	if (autoFree == DO_AUTO_FREE) {
		free(nodeForRemove);
	}
	
	DIAGNOSE();
	return 0;
}

void LibLinkedList_RemoveAll(LinkedListInfo *info, AUTO_FREE autoFree /* = 0 */)
{
	LinkedListNode *currheadNode = info->head;
	while (currheadNode != NULL) {
		if (LibLinkedList_Remove(info, currheadNode, autoFree)) {
			BASIC_ASSERT(0);
		}
		currheadNode = info->head;
	}

	info->count = 0;

	DIAGNOSE();
	return;
}

int LibLinkedList_InsertBack(LinkedListInfo *info, LinkedListNode *nodeBeInserted, LinkedListNode *nodeForInsert)
{
	if (NODE_ISNT_IN_LIST == LibLinkedList_IsThisNodeInList(info, nodeBeInserted)) {
		DIAGNOSE();
		return NODE_ISNT_IN_LIST;
	}

	BASIC_ASSERT(nodeBeInserted != NULL);
	BASIC_ASSERT(nodeForInsert != NULL);
	
	LinkedListNode *nodeBeforeInsertNode = nodeBeInserted;
	LinkedListNode *nodeAfterInsertNode = nodeBeInserted->next;
	
	// 1 Handle head
	//NONE
	
	// 2 Handle tail
	if (NODE_IS_TAIL(nodeBeInserted)) {
		info->tail = nodeForInsert;
	}

	// 3 Handle prev node
	nodeBeforeInsertNode->next = nodeForInsert;

	// 4 Handle next node
	if (nodeAfterInsertNode != NULL) {
		nodeAfterInsertNode->prev = nodeForInsert;
	}

	// 5 Handle new node
	nodeForInsert->prev = nodeBeforeInsertNode;
	nodeForInsert->next = nodeAfterInsertNode;
	
	info->count++;

	DIAGNOSE();
	return 0;
}

int LibLinkedList_InsertFront(LinkedListInfo *info, LinkedListNode *nodeBeInserted, LinkedListNode *nodeForInsert)
{
	if (NODE_ISNT_IN_LIST == LibLinkedList_IsThisNodeInList(info, nodeBeInserted)) {
		DIAGNOSE();
		return NODE_ISNT_IN_LIST;
	}

	BASIC_ASSERT(nodeBeInserted != NULL);
	BASIC_ASSERT(nodeForInsert != NULL);
	
	LinkedListNode *nodeBeforeInsertNode = nodeBeInserted->prev;
	LinkedListNode *nodeAfterInsertNode = nodeBeInserted;
	
	// 1 Handle head
	if (NODE_IS_HEAD(nodeBeInserted)) {
		info->head = nodeForInsert;
	}
	
	// 2 Handle tail
	//NONE

	// 3 Handle prev node
	if (nodeBeforeInsertNode != NULL) {
		nodeBeforeInsertNode->next = nodeForInsert;
	}
	
	// 4 Handle next node
	nodeAfterInsertNode->prev = nodeForInsert;

	// 5 Handle new node
	nodeForInsert->prev = nodeBeforeInsertNode;
	nodeForInsert->next = nodeAfterInsertNode;
	
	info->count++;

	DIAGNOSE();
	return 0;
}

int LibLinkedList_IsThisNodeInList(LinkedListInfo *info, LinkedListNode *nodeForCheck)
{
	if (info->head == NULL) {
		DIAGNOSE();
		return NODE_ISNT_IN_LIST;
	}

	for (LinkedListNode *currNode = info->head; currNode != NULL; currNode = currNode->next) {
		if (currNode == nodeForCheck) {
			return 0;
		}
	}

	return NODE_ISNT_IN_LIST;
}

void LibLinkedList_Dump(LinkedListInfo *info)
{
	printf("==================================== %s() start\n", __func__);
	DUMPD(info->count);
	DUMPP(info->head);
	DUMPP(info->tail);
	LinkedListNode *currNode = info->head;
	for (u32 i = 0; i < info->count; i++) {
		printf("Node(%d):\n", i+1);
		printf("[ %p ]\n", currNode);
		DUMPP(currNode->prev);
		DUMPP(currNode->next);
		currNode = currNode->next;
	}
	BASIC_ASSERT(currNode == NULL);
	printf("==================================== %s() end\n", __func__);
}

void LibLinkedList_Diagnose(LinkedListInfo *info, const char *caller)
{
	BASIC_ASSERT(caller != NULL);

	// Length check
	u32 count = 0;
	LinkedListNode *currNode = info->head;
	while (currNode != NULL) {
		count++;
		currNode = currNode->next;
	}
	if (count != info->count) {
		DUMPD(count);
		DUMPD(info->count);
		CALLER_ASSERT(count == info->count);
	}
	// Length check in backward
	count = 0;
	currNode = info->tail;
	while (currNode != NULL) {
		count++;
		currNode = currNode->prev;
	}
	CALLER_ASSERT(count == info->count);

	// Generic check
	if (info->head != NULL) {
		CALLER_ASSERT(info->head->prev == NULL);
		CALLER_ASSERT(info->tail != NULL);
		CALLER_ASSERT(info->tail->next == NULL);

		// Check relation address between 2 nodes
		for (LinkedListNode *currNode = info->head; currNode != NULL; currNode = currNode->next) {
			if (currNode->next != NULL) {
				CALLER_ASSERT(currNode == currNode->next->prev);
			}
		}
		for (LinkedListNode *currNode = info->tail; currNode != NULL; currNode = currNode->prev) {
			if (currNode->prev != NULL) {
				CALLER_ASSERT(currNode == currNode->prev->next);
			}
		}
	} else {
		CALLER_ASSERT(info->tail == NULL);
	}
}

void LinkedListClass::InitInfo(void)
{
	LibLinkedList_InitInfo(&info);
}

u32  LinkedListClass::Length(void)
{
	return LibLinkedList_Length(&info);
}

void LinkedListClass::PushBack(LinkedListNode *newNode)
{
	LibLinkedList_PushBack(&info, newNode);
}

void LinkedListClass::PopBack(AUTO_FREE autoFree /* = 0 */)
{
	LibLinkedList_PopBack(&info, autoFree);
}

int LinkedListClass::Remove(LinkedListNode *nodeForRemove, AUTO_FREE autoFree /* = 0 */)
{
	return LibLinkedList_Remove(&info, nodeForRemove, autoFree);
}

void LinkedListClass::RemoveAll(AUTO_FREE autoFree /* = 0 */)
{
	LibLinkedList_RemoveAll(&info, autoFree);
}

int LinkedListClass::InsertBack(LinkedListNode *nodeBeInserted, LinkedListNode *nodeForInsert)
{
	return LibLinkedList_InsertBack(&info, nodeBeInserted, nodeForInsert);
}

int LinkedListClass::InsertFront(LinkedListNode *nodeBeInserted, LinkedListNode *nodeForInsert)
{
	return LibLinkedList_InsertFront(&info, nodeBeInserted, nodeForInsert);
}

int LinkedListClass::IsThisNodeInList(LinkedListNode *nodeForCheck)
{
	return LibLinkedList_IsThisNodeInList(&info, nodeForCheck);
}

void LinkedListClass::Dump(void)
{
	LibLinkedList_Dump(&info);
}

void LinkedListClass::Diagnose(const char *caller)
{
	LibLinkedList_Diagnose(&info, caller);
}

void LibLinkedList_Demo(void)
{
/*
	LinkedListInfo listInfo;
	LibLinkedList_InitInfo(&listInfo);
	LibLinkedList_Length(&listInfo);
	

	LibLinkedList_Dump(&listInfo);

	LinkedListNode newNode1;
	LibLinkedList_PushBack(&listInfo, &newNode1);
	LibLinkedList_Length(&listInfo);
	LibLinkedList_Dump(&listInfo);

	LinkedListNode newNode2;
	LibLinkedList_PushBack(&listInfo, &newNode2);
	LibLinkedList_Length(&listInfo);
	LibLinkedList_Dump(&listInfo);

	LinkedListNode newNode3;
	LibLinkedList_PushBack(&listInfo, &newNode3);
	LibLinkedList_Length(&listInfo);
	LibLinkedList_Dump(&listInfo);
*/
	LinkedListClass linkedList;
	linkedList.InitInfo();
	//linkedList.Dump();

	LinkedListNode newNode1;
	linkedList.PushBack(&newNode1);
	//linkedList.Dump();

	LinkedListNode newNode2;
	linkedList.PushBack(&newNode2);
	//linkedList.Dump();

	LinkedListNode newNode3;
	linkedList.PushBack(&newNode3);
	linkedList.Dump();

	//linkedList.PopBack();
	//linkedList.Dump();
	
	//linkedList.Remove(&newNode3);
	//linkedList.Dump();

	//linkedList.RemoveAll();
	//linkedList.Dump();
	LinkedListNode newNode4;
	linkedList.InsertBack(&newNode4, &newNode4);
	//linkedList.Dump();

}

list_header_t* list_header_new()
{
    list_header_t* lt = (list_header_t*) MM_ALLOC(sizeof(list_header_t));
    BASIC_ASSERT(lt != NULL);
    lt->node_num = 0;
    LLIST_HEAD_RESET(lt);
    return lt;
}

void list_header_init(list_header_t *p_header)
{
    LLIST_HEAD_RESET(p_header);
    p_header->node_num = 0;
}

void list_clear(list_header_t *p_header)
{
    if (LLIST_IS_EMPTY(p_header))
        return;

    do {
        LList_Entry_t *prev;
        LList_Entry_t *curr;
        LLIST_WHILE_START(p_header, curr, LList_Entry_t) {
            prev = curr;
            LLIST_WHILE_NEXT(curr, LList_Entry_t);
            MM_FREE(prev);
        }
        LLIST_HEAD_RESET(p_header);
    } while(0);

    p_header->node_num = 0;

    return;
}

void list_delete(list_header_t *p_header)
{
    list_clear(p_header);
    MM_FREE(p_header);
    return;
}

int list_append(list_header_t *head, void *node)
{
    LLIST_INSERT_LAST(head, node);
    head->node_num++;
    return 0;
}

void *list_pop(list_header_t *hd)
{
    void *last;
    BASIC_ASSERT(hd != NULL);

    if (LLIST_IS_EMPTY(hd)) {
        return NULL;
    } else {
        last = LLIST_LAST(hd);
        DLLIST_REMOVE_LAST_SAFELY(hd);
        hd->node_num--;
    }

    return last;
}

void* list_at(list_header_t *hd, int32_t idx)
{
    BASIC_ASSERT(hd != NULL);

    int32_t new_idx = idx >= 0 ? idx : hd->node_num+idx;
    BASIC_ASSERT(new_idx >= 0);
    BASIC_ASSERT((size_t)new_idx < hd->node_num);

    LList_Entry_t *curr;
    int32_t i=0;

    LLIST_FOREACH(hd, curr, LList_Entry_t) {
        if (i == new_idx) {
            return curr;
        }
        i++;
    }

    return NULL;
}

void *list_remove(list_header_t *hd, int32_t idx)
{
    BASIC_ASSERT(hd != NULL);
    
    int32_t new_idx = idx >= 0 ? idx : hd->node_num+idx;
    BASIC_ASSERT(new_idx >= 0);
    BASIC_ASSERT((size_t)new_idx < hd->node_num);

    int32_t i = 0;
    LList_Entry_t *curr;

    if (LLIST_IS_EMPTY(hd)) {
        return NULL;
    } else {
        LLIST_FOREACH(hd, curr, LList_Entry_t) {
            if (i == new_idx) {
                LLIST_REMOVE_NODE_SAFELY(hd, curr);
                hd->node_num--;
                return curr;
            }
            i++;
        }
    }
    BASIC_ASSERT(0);
    return NULL;
}

void list_diagnose(list_header_t *hd CALLER_PARA3)
{
	// Length check
    void *temp;
	u32 count = 0;
	list_header_t *currNode = (list_header_t *)LLIST_FIRST(hd);

    if (LLIST_IS_EMPTY(hd)) {
        return;
    }

	while (currNode != NULL) {
		count++;
		currNode = (list_header_t *)LLIST_NEXT(currNode);
	}
	if (count != hd->node_num) {
		DUMPD(count);
		DUMPD(hd->node_num);
		CALLER_ASSERT3(0);
	}
	// Length check in backward
	count = 0;
    temp = LLIST_LAST(hd);
	currNode = (list_header_t *)temp;
	while (currNode != hd) {
		count++;
		currNode = (list_header_t *)LLIST_PREV(currNode);
	}
	CALLER_ASSERT3(count == hd->node_num);

	// Generic checkPRLOC
	if (LLIST_IS_NOT_EMPTY(hd)) {
		CALLER_ASSERT3(LLIST_PREV(LLIST_FIRST(hd)) == hd);
		CALLER_ASSERT3(LLIST_TAIL(hd) != NULL);
		CALLER_ASSERT3(LLIST_NEXT(LLIST_TAIL(hd)) == NULL);

		// Check relation address between 2 nodes
		for (list_header_t *currNode = (list_header_t *)LLIST_HEAD(hd); currNode != NULL; currNode = (list_header_t *)LLIST_NEXT(currNode)) {
			if (LLIST_NEXT(currNode) != NULL) {
				CALLER_ASSERT3(currNode == (list_header_t *)LLIST_PREV(LLIST_NEXT(currNode)));
			}
		}
        temp = LLIST_LAST(hd);
		for (list_header_t *currNode = (list_header_t *)temp; currNode != hd; currNode = (list_header_t *)LLIST_PREV(currNode)) {
			if (LLIST_PREV(currNode) != NULL) {
				CALLER_ASSERT3(currNode == (list_header_t *)LLIST_NEXT(LLIST_PREV(currNode)));
			}
		}
	} else {
		CALLER_ASSERT3(LLIST_TAIL(hd) == NULL);
	}
}

typedef struct {
    LList_Entry_t en;
    int data;
} diagnose_demo_t;

void LibLinkedList_Diagnose_Demo(void)
{
    list_header_t hd_inst;
    list_header_t *hd = &hd_inst;
    list_header_init(hd);

	diagnose_demo_t a, b, c;
	a.data = 111;
	b.data = 222;
	c.data = 333;

    list_append(hd, &a);
    LIST_DIAGNOSE(hd);
    list_append(hd, &b);
    LIST_DIAGNOSE(hd);
    list_append(hd, &c);
    LIST_DIAGNOSE(hd);

    diagnose_demo_t *curr = (diagnose_demo_t *)list_at(hd, 1);
    LIST_DIAGNOSE(hd);
    BASIC_ASSERT(curr->data == 222);
	DND(curr->data);

    curr = (diagnose_demo_t *)list_remove(hd, 1);
    LIST_DIAGNOSE(hd);
    DND(curr->data);
    
    curr = (diagnose_demo_t *)list_pop(hd);
    LIST_DIAGNOSE(hd);
    BASIC_ASSERT(curr->data == 333);
    DND(curr->data);

    curr = (diagnose_demo_t *)list_pop(hd);
    LIST_DIAGNOSE(hd);
    BASIC_ASSERT(curr->data == 111);
    DND(curr->data);
}