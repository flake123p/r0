
#include "Everything_App.hpp"

void double_2_u64(double &src, uint64_t &dst)
{
    uint64_t *temp = (uint64_t *)&src;
    dst = *temp;
}

void u64_2_double(uint64_t &src, double &dst)
{
    double *temp = (double *)&src;
    dst = *temp;
}

void float_2_u32(float &src, uint32_t &dst)
{
    uint32_t *temp = (uint32_t *)&src;
    dst = *temp;
}

void u32_2_float(uint32_t &src, float &dst)
{
    float *temp = (float *)&src;
    dst = *temp;
}

/*
f32:
   1.0000001192092895507812500000000000000000
  10.0000009536743164062500000000000000000000
 100.0000076293945312500000000000000000000000
1000.0000610351562500000000000000000000000000
*/

#define CHNAGE_ME  1000

void fun1()
{
    double s;
    uint64_t x = 0x3300000001;

    printf("x 0x%lx\n", x);

    s = CHNAGE_ME;
    double_2_u64(s, x);
    printf("x 0x%lx\n", x);

    x = x + 1;
    u64_2_double(x, s);
    printf("s %.40f\n", s);
}

void fun2()
{
    float s;
    uint32_t x = 0x33000001;

    printf("x 0x%x\n", x);

    s = CHNAGE_ME;
    float_2_u32(s, x);
    printf("x 0x%x\n", x);

    x = x + 1;
    u32_2_float(x, s);
    printf("s %.40f\n", s);
}

int main(int argc, char *argv[])
{
    fun1();
    fun2();

    double s = 0.8654362897133816;
    uint64_t x ;
    double_2_u64(s, x);
    printf("x 0x%lx\n", x);
    return 0;
}

