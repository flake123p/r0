
#include "Everything_App.hpp"

int main(int argc, char *argv[])
{
    Lib_InitMT();
    
    void *curr[10];

    LibMem_DumpMemUsage();
    curr[0] = MM_ALLOC(2); 
    LibMem_DumpMemUsage();
    
    curr[1] = MM_ALLOC(3); 
    curr[2] = MM_ALLOC(4); 

    MM_FREE(curr[0]); 

    curr[3] = MM_ALLOC(5); 

    MM_FREE(curr[1]); 
    MM_FREE(curr[2]); 
    MM_FREE(curr[3]); 

    //BASIC_ASSERT(LibIPC_Mutex_GetCounter() == 0);

    Lib_Uninit();

    return 0;
}

