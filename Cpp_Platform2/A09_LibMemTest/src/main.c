
#include "Everything_App.hpp"

void test_0000(void)
{
    Lib_InitMT();
    //MM_INIT();
    
    MM_ALLOC(2);

    //BASIC_ASSERT(LibIPC_Mutex_GetCounter() == 0);

    LibThreadMgr_DemoEvent();

    //MM_UNINIT();
    Lib_Uninit();


    Lib_InitMT();
    Lib_Uninit();
    //MM_INIT();

    //MM_ALLOC(2);

    //MM_UNINIT();
    do {
        static int ctr = 0;
        ctr++;
        if (ctr == 1)
            break;
        else
            DND(ctr);
    } while (1);

    {
        /* code */
    }
}

void test_0001_libmem_callback(void)
{
    /*
        allocate mem
        enable write protect
        write to trigger assert
        dump user callback 
     */
    Lib_InitMT();
    {
        typedef struct {
            uint32_t a;
            uint32_t b;
        } ab_t;
        typedef struct {
            uint32_t a;
            uint32_t b;
            uint32_t c;
        } abc_t;
        DND(sizeof(ab_t));
        ab_t *ab = (ab_t *)MM_ALLOC(sizeof(ab_t));
        abc_t *abc = (abc_t *)ab;
        abc_t test = {11,22,33};
        test = test;
        abc = abc;

        WT(ab->a, 10);
        DND(ab->a);

        MM_FREE(ab);
    }
    Lib_Uninit();
}

typedef struct {
    int a;
    int b;
    //int c[100];
} LibMemTest_struct_t;

static MSG_THR_HANDLE_t hi, mi, lo, lo2;

static void *test_0002_task(void *hdl)
{
    LibMemTest_struct_t *msg;

    while(1)
    {
        LibMsgThr_EventWait(hdl);
        while (1) {
            msg = (LibMemTest_struct_t *)LibMsgThr_MsgRecieve(hdl);
            if (msg) {
                if (msg->a == 7777) {
                    LibMemTest_struct_t *new_msg = (LibMemTest_struct_t *)LibMsgThr_MsgGet(sizeof(LibMemTest_struct_t));
                    new_msg->a = 8888;
                    new_msg->b = msg->b;
                    LibMsgThr_MsgSend(mi, new_msg);
                } else if (msg->a == 8888) {
                    {
                        LibMemTest_struct_t *new_msg = (LibMemTest_struct_t *)LibMsgThr_MsgGet(sizeof(LibMemTest_struct_t));
                        new_msg->a = 9999;
                        new_msg->b = msg->b;
                        LibMsgThr_MsgSend(lo, new_msg);
                    }
                    {
                        LibMemTest_struct_t *new_msg = (LibMemTest_struct_t *)LibMsgThr_MsgGet(sizeof(LibMemTest_struct_t));
                        new_msg->a = 9999;
                        new_msg->b = msg->b;
                        LibMsgThr_MsgSend(lo2, new_msg);
                    }
                }
                if (msg->b == 999) {
                    DND(msg->b);
                    LibMsgThr_MsgRelease(msg);
                    return NULL;    
                }
                LibMsgThr_MsgRelease(msg);
            } else {
                break;
            }
        }
    }
    return NULL;
}

void test_0002_libmsgthr_burst_test(void)
{
    PRLOC;
    {
        Lib_InitMT();
    }

    LibMsgThr_NewHandle(&hi, 0);
    LibMsgThr_NewHandle(&mi, 0);
    LibMsgThr_NewHandle(&lo, 0);
    LibMsgThr_NewHandle(&lo2, 0);
    LibMsgThr_Create(hi, test_0002_task, hi);
    LibMsgThr_Create(mi, test_0002_task, mi);
    LibMsgThr_Create(lo, test_0002_task, lo);
    LibMsgThr_Create(lo2, test_0002_task, lo2);

    const int max = 1000;
    for (int i = 0; i < max; i++) 
    {
        LibMemTest_struct_t *m = (LibMemTest_struct_t *)LibMsgThr_MsgGet(sizeof(LibMemTest_struct_t));
        m->a = 7777;
        m->b = i%100;
        if (i == max-1) {
            m->b = 999;
        }
        LibMsgThr_MsgSend(hi, m);
    }

    LibMsgThr_WaitThread(hi);
    LibMsgThr_WaitThread(mi);
    LibMsgThr_WaitThread(lo);
    LibMsgThr_WaitThread(lo2);
    LibMsgThr_DestroyHandle(hi);
    LibMsgThr_DestroyHandle(mi);
    LibMsgThr_DestroyHandle(lo);
    LibMsgThr_DestroyHandle(lo2);
    {
        Lib_Uninit();
    }
}

static void *test_0003_lo(void *hdl)
{
    LibMemTest_struct_t *msg;

    while(1)
    {
        LibMsgThr_EventWait(hdl);
        while (1) {
            msg = (LibMemTest_struct_t *)LibMsgThr_MsgRecieve(hdl);
            if (msg) {
                if (msg->a == 7777) {
                    {
                        LibMemTest_struct_t *new_msg = (LibMemTest_struct_t *)LibMsgThr_MsgGet(sizeof(LibMemTest_struct_t)+28);
                        new_msg->a = 8888;
                        new_msg->b = msg->b;
                        LibMsgThr_MsgSend(hi, new_msg);
                    }
                    {
                        LibMemTest_struct_t *new_msg = (LibMemTest_struct_t *)LibMsgThr_MsgGet(sizeof(LibMemTest_struct_t));
                        new_msg->a = 8888;
                        new_msg->b = msg->b;
                        LibMsgThr_MsgSend(lo2, new_msg);
                    }
                }
                if (msg->b == 3333) {
                    DND(msg->b);
                    LibMsgThr_MsgRelease(msg);
                    return NULL;    
                }
                LibMsgThr_MsgRelease(msg);
            } else {
                break;
            }
        }
    }
    return NULL;
}

static void *test_0003_lo2(void *hdl)
{
    LibMemTest_struct_t *msg;

    while(1) {
        msg = (LibMemTest_struct_t *)LibMsgThr_WaitAndRecieve(hdl);
        if (msg) {
            if (msg->b == 3333) {
                DND(msg->b);
                LibMsgThr_MsgRelease(msg);
                return NULL;    
            }
            LibMsgThr_MsgRelease(msg);
        }
    }
    return NULL;
}

static void *test_0003_hi(void *hdl)
{
    const int max = 1000;
    for (int i = 0; i < max; i++) {
        LibMemTest_struct_t *new_msg = (LibMemTest_struct_t *)LibMsgThr_MsgGet(sizeof(LibMemTest_struct_t));
        new_msg->a = 7777;
        new_msg->b = i % 10;
        if (i == max-1)
            new_msg->b = 3333;
        LibMsgThr_MsgSend(lo, new_msg);
    }
    {
        int i = 0;
        LibMemTest_struct_t *msg;
        while(1) {
            msg = (LibMemTest_struct_t *)LibMsgThr_WaitAndRecieve(hi);
            i++;
            if (msg->b == 3333) {
                BASIC_ASSERT(i==max);
                DND(msg->b);
                LibMsgThr_MsgRelease(msg);
                return NULL;
            }
            LibMsgThr_MsgRelease(msg);
        }
    }
    return NULL;
}

void test_0003_libmsgthr_burst_rx_test(void)
{
    PRLOC;
    {
        Lib_InitMT();
    }

    LibMsgThr_NewHandle(&hi, 0);
    LibMsgThr_NewHandle(&lo, 0);
    LibMsgThr_NewHandle(&lo2, 0);
    LibMsgThr_Create(hi, test_0003_hi, hi);
    LibMsgThr_Create(lo, test_0003_lo, lo);
    LibMsgThr_Create(lo2, test_0003_lo2, lo2);
    LibMsgThr_WaitThread(hi);
    LibMsgThr_WaitThread(lo);
    LibMsgThr_WaitThread(lo2);
    LibMsgThr_DestroyHandle(hi);
    LibMsgThr_DestroyHandle(lo);
    LibMsgThr_DestroyHandle(lo2);
    {
        Lib_Uninit();
    }
}

#if 0
#undef CALLER_ASSERT3
#define CALLER_ASSERT3(...)
#undef CALLER_PARA3
#define CALLER_PARA3
#undef CALLER_ARG3
#define CALLER_ARG3
#endif

void test_0004_caller_assert_subfunc(int a CALLER_PARA3)
{
    PRLOC;
    DND(a);
    printf("caller_file = %s\n", caller_file);
    printf("caller_line = %lu\n", caller_line);
    //CALLER_ASSERT3(0);
}

#define TEST_0004(a) test_0004_caller_assert_subfunc(a CALLER_ARG3)

void test_0004_caller_assert_test(void)
{
    PRLOC;
    TEST_0004(567);
}

int main(int argc, char *argv[])
{
    //test_0001_libmem_callback();
    //LibMsgThr_Demo();
    FOREACH_I(0) {
        test_0002_libmsgthr_burst_test();
        DND(i);
    }
    FOREACH_I(0) {
        test_0003_libmsgthr_burst_rx_test();
        DND(i);
    }

    FOREACH_I(0) {
        LibHiSys_Demo();
        LibHiSys_Demo2();
        LibHiSys_Demo_NoOS();
        LibHiSys_Demo_NoOS2();
        DND(i);
    }

    test_0004_caller_assert_test();

    LibLinkedList_Diagnose_Demo();

    return 0;
}

