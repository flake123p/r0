
#include "Everything_App.hpp"

/*
    pos[0] = x min, to x
    pos[1] = x max, to y
    pos[2] = y min, to width
    pos[3] = y max, to height
*/
int position_to_roi(int *pos, int w, int h, int extend, int *out = NULL)
{
    int x_min = pos[0];
    int x_max = pos[1];
    int y_min = pos[2];
    int y_max = pos[3];

    if (x_max < x_min || y_max < y_min) {
        return 1;
    }
    if (x_max >= w) {
        return 2;
    }
    if (y_max >= h) {
        return 3;
    }

    x_min = x_min > extend ? x_min - extend : 0;
    y_min = y_min > extend ? y_min - extend : 0;

    x_max = x_max + extend >= w ? w - 1 : x_max + extend;
    y_max = y_max + extend >= h ? h - 1 : y_max + extend;

    int roi_w = x_max - x_min + 1;
    int roi_h = y_max - y_min + 1;

    BASIC_ASSERT(roi_w <= w);
    BASIC_ASSERT(roi_h <= h);

    if (out == NULL) {
        pos[0] = x_min;
        pos[1] = y_min;
        pos[2] = roi_w;
        pos[3] = roi_h;
    } else {
        out[0] = x_min;
        out[1] = y_min;
        out[2] = roi_w;
        out[3] = roi_h;
    }

    return 0;
}

typedef struct {
    int gin[4];
    int w;
    int h;
    int extend;
    int gout[4];
    int gret;
} position_to_roi_test_t;

position_to_roi_test_t g_roi_tests[] = {
    { {0,159,0,119}, 160, 120, 0, {0,0,160,120}, 0},
    { {0,160,0,119}, 160, 120, 0, {0,0,160,120}, 2},
    { {0,999,0,119}, 160, 120, 0, {0,0,160,120}, 2},
    { {0,159,0,120}, 160, 120, 0, {0,0,160,120}, 3},
    { {1,0,0,120}, 160, 120, 0, {0,0,160,120}, 1},
    { {0,1,1,0}, 160, 120, 0, {0,0,160,120}, 1},
    { {20,30,20,30}, 160, 120, 5, {15,15,21,21}, 0},
    { {20,30,20,30}, 160, 120, 200, {0,0,160,120}, 0},
    { {5,18,97,118}, 160, 120, 7, {0,90,26,30}, 0},
    { {120,159,97,118}, 160, 120, 90, {30,7,130,113}, 0},
};

static void run_pos_2_roi_test()
{
    int ret = 0;
    int out[4];
    for (size_t i = 0; i < ARRAY_LEN(g_roi_tests); i++) {
        printf("%-4lu : roi:%-3d, %-3d, %-3d, %-3d, r:%-3d, c:%-3d, e:%-3d || ", 
            i,
            g_roi_tests[i].gin[0], g_roi_tests[i].gin[1], g_roi_tests[i].gin[2], g_roi_tests[i].gin[3],
            g_roi_tests[i].w, g_roi_tests[i].h, g_roi_tests[i].extend 
        );
        ret = position_to_roi(g_roi_tests[i].gin, g_roi_tests[i].w, g_roi_tests[i].h, g_roi_tests[i].extend, out);
        printf("roi:%-3d, %-3d, %-3d, %-3d, ret:%-3d\n",
            out[0], out[1], out[2], out[3], ret);
        BASIC_ASSERT(ret == g_roi_tests[i].gret);
        if (g_roi_tests[i].gret == 0) {
            BASIC_ASSERT(out[0] == g_roi_tests[i].gout[0]);
            BASIC_ASSERT(out[1] == g_roi_tests[i].gout[1]);
            BASIC_ASSERT(out[2] == g_roi_tests[i].gout[2]);
            BASIC_ASSERT(out[3] == g_roi_tests[i].gout[3]);
        }
    }
}

int main(int argc, char *argv[])
{
    run_pos_2_roi_test();
    return 0;
}

