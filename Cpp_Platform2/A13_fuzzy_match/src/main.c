
#include "Everything_App.hpp"

#define TIMESTAMP_T       uint32_t
typedef double FLOAT_T;

typedef struct {
    TIMESTAMP_T ts; //timestamp
    FLOAT_T confidence;
    FLOAT_T x;
    FLOAT_T y;
    FLOAT_T angle;
    FLOAT_T minor_r;
    FLOAT_T major_r;
} pupil_detect_result_t;

typedef struct {
    int match_num;
    pupil_detect_result_t *db;
    pupil_detect_result_t *input;
    int result; // db index or -1 for not found
    FLOAT_T diff_sum;
    FLOAT_T x_diff;
    FLOAT_T y_diff;
} pupil_comp_desc_t;

pupil_detect_result_t the_db[2] = {
    {0, 0.990000, 114.895882, 43.564751 , 158.179779 , 22.487787 , 29.100397},
    {0, 0.990000, 113.598045, 51.476612 , 162.940308 , 22.876602 , 29.736017}
};

pupil_detect_result_t gin0 = {0, 0.990000, 115.598045, 53.476612 , 162.840308 , 22.776602 , 29.836017};

void do_comp(pupil_comp_desc_t *desc)
{
    FLOAT_T best_diff = 999999;
    int best_idx = -1;

    for (int i = 0; i < desc->match_num; i++) {
        FLOAT_T temp = 0;
        temp += ABS(desc->input->angle - desc->db[i].angle);
        temp += ABS(desc->input->minor_r - desc->db[i].minor_r);
        temp += ABS(desc->input->major_r - desc->db[i].major_r);
        printf("%d, %f, %f\n", i, best_diff, temp);
        if (temp < best_diff) {
            best_diff = temp;
            best_idx = i;
            desc->diff_sum = temp;
        }
    }

    desc->result = best_idx;
    if (best_idx != -1) {
        desc->x_diff = desc->db[best_idx].x - desc->input->x;
        desc->y_diff = desc->db[best_idx].y - desc->input->y;
    }
}

void run()
{
    pupil_comp_desc_t desc;
    
    desc.match_num = 2;
    desc.db = the_db;
    desc.input = &gin0;

    do_comp(&desc);

    printf("resutl;\n");
    printf("%d, %f, %f, %f\n", desc.result, desc.diff_sum, desc.x_diff, desc.y_diff);
}

int main(int argc, char *argv[])
{
    run();
    return 0;
}

