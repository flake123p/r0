
#
# Fixed Image
#

    <ImageView
        android:layout_width="327dp"
        android:layout_height="225dp"
        android:src="@drawable/kite"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.476"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.28" />

################################################################################################

#
# Dynamic Image
#
    <ImageView
        android:id="@+id/imageViewXX"
        android:layout_width="290dp"
        android:layout_height="210dp"
        android:layout_marginTop="468dp"
        android:scaleType="fitStart"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"/>


#
#  Not a pure example ...
#
public class MainActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AssetManager assetManager = this.getAssets();
        //File imgFile = new File("file:///android_assets/kite.jpg");
        try {
            textView = findViewById(R.id.textView00);
            textView.setText("dyn string");

            imageView = findViewById(R.id.imageViewXX);
            InputStream istr = assetManager.open("kite2.jpg");
            Bitmap imgBitmap = BitmapFactory.decodeStream(istr);
            imageView.setImageBitmap(imgBitmap);
        } catch (IOException e) {
            Log.e("NONONO", "file not exist");
            throw new RuntimeException(e);
        }
    }
}

#
# File from drawable/  (Easier)
#
            imageView = (ImageView) findViewById(R.id.imageViewXX);
            imageView.setImageResource(R.drawable.kite);