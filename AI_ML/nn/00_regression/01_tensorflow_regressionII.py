#
# For Tensorflow v2
#
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

'''
1.
$ tensorboard --logdir=log

2.
Open Your Browser with: http://localhost:6006/
'''
##################################################################################################################
# Input values
x = tf.constant([2.])
y = tf.constant([6.])
# Variables
w = tf.Variable(2.)
# Compute model and loss
model = tf.multiply(x, w)
loss = tf.reduce_sum(tf.pow(y -model, 2))
# Create optimizer
learn_rate = 0.05
num_epochs = 100
optimizer = tf.train.GradientDescentOptimizer(learn_rate).minimize(loss)
# Initialize variables
init = tf.global_variables_initializer()
# Launch session
with tf.Session() as sess:
    sess.run(init)
    # Run optimizer
    for epoch in range(num_epochs):
        sess.run(optimizer)
        print(f'e = {epoch:2}, ' + 'm =', sess.run(w))

'''
Output:

e =  0, m = 2.4
e =  1, m = 2.64
e =  2, m = 2.7840002
e =  3, m = 2.8704002
e =  4, m = 2.92224
e =  5, m = 2.953344
e =  6, m = 2.9720066
e =  7, m = 2.983204
e =  8, m = 2.9899223
e =  9, m = 2.9939535
e = 10, m = 2.996372
e = 11, m = 2.9978232
e = 12, m = 2.998694
e = 13, m = 2.9992163
e = 14, m = 2.9995298
e = 15, m = 2.999718
e = 16, m = 2.9998307
e = 17, m = 2.9998984
e = 18, m = 2.999939
e = 19, m = 2.9999633
e = 20, m = 2.999978
e = 21, m = 2.999987
e = 22, m = 2.9999921
e = 23, m = 2.9999952
e = 24, m = 2.9999971
e = 25, m = 2.9999983
e = 26, m = 2.999999
e = 27, m = 2.9999995
e = 28, m = 2.9999998
e = 29, m = 2.9999998
e = 30, m = 2.9999998
e = 31, m = 2.9999998
e = 32, m = 2.9999998
e = 33, m = 2.9999998
e = 34, m = 2.9999998
e = 35, m = 2.9999998
e = 36, m = 2.9999998
e = 37, m = 2.9999998
e = 38, m = 2.9999998
e = 39, m = 2.9999998
e = 40, m = 2.9999998
e = 41, m = 2.9999998
e = 42, m = 2.9999998
e = 43, m = 2.9999998
e = 44, m = 2.9999998
e = 45, m = 2.9999998
e = 46, m = 2.9999998
e = 47, m = 2.9999998
e = 48, m = 2.9999998
e = 49, m = 2.9999998
e = 50, m = 2.9999998
e = 51, m = 2.9999998
e = 52, m = 2.9999998
e = 53, m = 2.9999998
e = 54, m = 2.9999998
e = 55, m = 2.9999998
e = 56, m = 2.9999998
e = 57, m = 2.9999998
e = 58, m = 2.9999998
e = 59, m = 2.9999998
e = 60, m = 2.9999998
e = 61, m = 2.9999998
e = 62, m = 2.9999998
e = 63, m = 2.9999998
e = 64, m = 2.9999998
e = 65, m = 2.9999998
e = 66, m = 2.9999998
e = 67, m = 2.9999998
e = 68, m = 2.9999998
e = 69, m = 2.9999998
e = 70, m = 2.9999998
e = 71, m = 2.9999998
e = 72, m = 2.9999998
e = 73, m = 2.9999998
e = 74, m = 2.9999998
e = 75, m = 2.9999998
e = 76, m = 2.9999998
e = 77, m = 2.9999998
e = 78, m = 2.9999998
e = 79, m = 2.9999998
e = 80, m = 2.9999998
e = 81, m = 2.9999998
e = 82, m = 2.9999998
e = 83, m = 2.9999998
e = 84, m = 2.9999998
e = 85, m = 2.9999998
e = 86, m = 2.9999998
e = 87, m = 2.9999998
e = 88, m = 2.9999998
e = 89, m = 2.9999998
e = 90, m = 2.9999998
e = 91, m = 2.9999998
e = 92, m = 2.9999998
e = 93, m = 2.9999998
e = 94, m = 2.9999998
e = 95, m = 2.9999998
e = 96, m = 2.9999998
e = 97, m = 2.9999998
e = 98, m = 2.9999998
e = 99, m = 2.9999998
'''