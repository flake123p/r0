#
# For Tensorflow v2
#
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

'''
1.
$ tensorboard --logdir=log

2.
Open Your Browser with: http://localhost:6006/
'''
##################################################################################################################
# Input values
x = tf.constant([1., 2.])
y = tf.constant([3., 6.])
# Variables
w = tf.Variable(2.)
# Compute model and loss
model = tf.multiply(x, w)
loss = tf.reduce_sum(tf.pow(y -model, 2))
# Create optimizer
learn_rate = 0.05
num_epochs = 30
optimizer = tf.train.GradientDescentOptimizer(learn_rate).minimize(loss)
# Initialize variables
init = tf.global_variables_initializer()
# Launch session
with tf.Session() as sess:
    sess.run(init)
    # Run optimizer
    for epoch in range(num_epochs):
        sess.run(optimizer)
        print(f'e = {epoch:2}, ' + 'm =', sess.run(w))

'''
Output:

e =  0, m = 2.5
e =  1, m = 2.75
e =  2, m = 2.875
e =  3, m = 2.9375
e =  4, m = 2.96875
e =  5, m = 2.984375
e =  6, m = 2.9921875
e =  7, m = 2.9960938
e =  8, m = 2.9980469
e =  9, m = 2.9990234
e = 10, m = 2.9995117
e = 11, m = 2.9997559
e = 12, m = 2.999878
e = 13, m = 2.999939
e = 14, m = 2.9999695
e = 15, m = 2.9999847
e = 16, m = 2.9999924
e = 17, m = 2.9999962
e = 18, m = 2.999998
e = 19, m = 2.999999
e = 20, m = 2.9999995
e = 21, m = 2.9999998
e = 22, m = 3.0
e = 23, m = 3.0
e = 24, m = 3.0
e = 25, m = 3.0
e = 26, m = 3.0
e = 27, m = 3.0
e = 28, m = 3.0
e = 29, m = 3.0
'''