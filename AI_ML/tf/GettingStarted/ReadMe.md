
# Env install:
conda/x02_tf117p27.md


# Links
https://github.com/PacktPublishing/Getting-Started-with-TensorFlow


# Note
This books is TensorFlow V1


# Run V1 on V2
## 1: compat.va
```
import tensorflow.compat.v1 as tf

Or

tf.compat.v1...
```

## 2: Fix Session Run()
```
tf.disable_eager_execution()

or

tf.compat.v1.disable_eager_execution()
```
