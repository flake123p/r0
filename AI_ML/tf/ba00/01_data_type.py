
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import numpy as np
import matplotlib.pyplot as plt
import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

print("TensorFlow version:", tf.__version__)

#########################################################################################################################
'''

https://www.tensorflow.org/api_docs/python/tf/dtypes

Other Members
QUANTIZED_DTYPES	
{
 tf.qint16,
 tf.qint16_ref,
 tf.qint32,
 tf.qint32_ref,
 tf.qint8,
 tf.qint8_ref,
 tf.quint16,
 tf.quint16_ref,
 tf.quint8,
 tf.quint8_ref
}

bfloat16	Instance of tf.dtypes.DType     16-bit bfloat (brain floating point).
bool	    Instance of tf.dtypes.DType     Boolean.
complex128	Instance of tf.dtypes.DType     128-bit complex.
complex64	Instance of tf.dtypes.DType     64-bit complex.
double	    Instance of tf.dtypes.DType     64-bit (double precision) floating-point.
float16	    Instance of tf.dtypes.DType     16-bit (half precision) floating-point.
float32	    Instance of tf.dtypes.DType     32-bit (single precision) floating-point.
float64	    Instance of tf.dtypes.DType     64-bit (double precision) floating-point.
half	    Instance of tf.dtypes.DType     16-bit (half precision) floating-point.
int16	    Instance of tf.dtypes.DType     Signed 16-bit integer.
int32	    Instance of tf.dtypes.DType     Signed 32-bit integer.
int64	    Instance of tf.dtypes.DType     Signed 64-bit integer.
int8	    Instance of tf.dtypes.DType     Signed 8-bit integer.
qint16	    Instance of tf.dtypes.DType     Signed quantized 16-bit integer.
qint32	    Instance of tf.dtypes.DType     signed quantized 32-bit integer.
qint8	    Instance of tf.dtypes.DType     Signed quantized 8-bit integer.
quint16 	Instance of tf.dtypes.DType     Unsigned quantized 16-bit integer.
quint8	    Instance of tf.dtypes.DType     Unsigned quantized 8-bit integer.
resource	Instance of tf.dtypes.DType     Handle to a mutable, dynamically allocated resource.
string	    Instance of tf.dtypes.DType     Variable-length string, represented as byte array.
uint16	    Instance of tf.dtypes.DType     Unsigned 16-bit (word) integer.
uint32	    Instance of tf.dtypes.DType     Unsigned 32-bit (dword) integer.
uint64	    Instance of tf.dtypes.DType     Unsigned 64-bit (qword) integer.
uint8	    Instance of tf.dtypes.DType     Unsigned 8-bit (byte) integer.
variant	    Instance of tf.dtypes.DType     Data of arbitrary type (known at runtime).
'''
A = tf.Variable(3.3, dtype=tensorflow.float64)
print(A)

B = tf.Variable(3, dtype=tensorflow.int32)
print(B)

model = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(model)
    sess.run(tf.print(A))
    sess.run(tf.print(B))
