import numpy as np
import matplotlib.pyplot as plt
import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

print("TensorFlow version:", tf.__version__)

A = tf.Variable(3, dtype=tensorflow.float64)
B = tf.constant(2.0, dtype=tensorflow.float64)
Y = A*B

model = tf.global_variables_initializer()

with tf.Session() as session:
    session.run(model)
    print('A =', A.eval())
    print('B =', B.eval())
    print('Y =', Y.eval())

    print('Assign A to 3.1:')
    A.assign(3.1)
    print('A =', A.eval())
    print('B =', B.eval())
    print('Y =', Y.eval())

    print('session.run with 3.2:')
    session.run(A.assign(3.2))
    print('A =', A.eval())
    print('B =', B.eval())
    print('Y =', Y.eval())
