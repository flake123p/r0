
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

##################################################################################################################

a = tf.constant(2.5, name='first_val')
b = tf.constant(4.5, name='second_val')
sum = a + b

tf.train.write_graph(tf.get_default_graph(), os.getcwd(), 'graph.json')


'''
Similarly, an application can load a GraphDef from a file containing graph data by
calling one of two routines:

» TextFormat.Merge(data, graphdef): Initializes a GraphDef from text elements

» Creating GraphDefs: Converting a graph into a protocol buffer
'''