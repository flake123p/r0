
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

##################################################################################################################

a = tf.constant(2.5, name='first_val')
b = tf.constant(4.5, name='second_val')
sum = a + b

print(a)
print(b)
print(sum)
print(tf.get_default_graph().get_operations())
print(tf.get_default_graph().get_tensor_by_name('first_val:0'))

with tf.Session() as sess:
    sess.run(tf.print('sum =', sum))
    sess.run(tf.print('first =', tf.get_default_graph().get_tensor_by_name('first_val:0')))