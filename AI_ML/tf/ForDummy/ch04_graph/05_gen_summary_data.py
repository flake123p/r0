import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

##################################################################################################################
''' Summary Data Functions
scalar
histogram
audio
image
merge     : Merges the specified summary operations into one summary operation
merge_all : Merges summary operations into one summary operation
'''

# Add two scalars
a = tf.constant(2., name = 'A')
b = tf.constant(4., name = 'B')
c = tf.constant(10., name = 'C')
total = a * b + c
#total = tf.identity(total, name="TOTAL")
# Create operations that generate summary data
tf.summary.scalar("a", a)
tf.summary.scalar("b", b)
tf.summary.scalar("total", total)
# Merge the operations into a single operation
merged_op = tf.summary.merge_all()

fw = tf.summary.FileWriter("log", graph=tf.get_default_graph())

with tf.Session() as sess:
    summary = sess.run(merged_op)

'''
1.
$ tensorboard --logdir=log

2.
Open Your Browser with: http://localhost:6006/
'''