import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

##################################################################################################################

'''
The most important method of the Session class is run. This method accepts four
arguments, and only the first is required:

» fetches:      Identifies one or more operations or tensors to be executed
» feed_dict:    Data to be fed into a tensor
» options:      Configuration options for the session's execution
» run_metadata: Output data from the session
'''

tensr = tf.constant([2, 3])

t1 = tf.constant(7)
t2 = tf.constant(2)

with tf.Session() as sess:
    res = sess.run(tensr) # return an ndarray
    print(res) # Prints [2, 3]
    
    res = sess.run(t1 + t2)
    print(res) # Prints 9
    
    res1, res2 = sess.run([t1, t2])
    print(res1, res2) # Prints 7 2
