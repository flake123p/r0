import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

##################################################################################################################

'''
» TensorFlow enables logging through the tf.logging package.

» TensorFlow logging is based on regular Python logging, and many tf.
logging functions are identical to the methods of Python's Logger class.

» TensorFlow supports five logging levels. In order of severity, these are DEBUG,
INFO, WARN, ERROR, and FATAL.

» To enable logging, an application needs to call tf.logging set_verbosity
with the lowest level of severity that should be logged.

» By default, TensorFlow writes log messages to standard output. At the time of
this writing, TensorFlow logging doesn't support writing messages to a log file.
'''

tf.logging.set_verbosity(tf.logging.WARN)

t1 = tf.constant(7)
t2 = tf.constant(2)

with tf.Session() as sess:  
    res = sess.run(t1 + t2)
    tf.logging.debug('res: %f', res)
    tf.logging.info('res: %f', res)
    tf.logging.warn('res: %f', res)
    tf.logging.error('res: %f', res)
    tf.logging.fatal('res: %f', res)
    #tf.logging.flush()

'''
set_verbosity(level)        Enables logging for messages of the given severity level and greater severity
debug(msg, *args, **kwargs) Logs a message at DEBUG severity
info(msg, *args, **kwargs)  Logs a message at INFO severity
warn(msg, *args, **kwargs)  Logs a message at WARN severity
error(msg, *args, **kwargs) Logs a message at ERROR severity
fatal(msg, *args, **kwargs) Logs a message at FATAL severity
flush()                     Forces logging operations to complete
log(level, msg, *args, **kwargs)    Logs a message at the given severity level
log_if(level, msg, condition,*args) Logs a message at the given severity level if the condition is true
log_first_n(level, msg, n, *args)   Logs a message at the given severity level at most n times
log_every_n(level, msg, n, *args)   Logs a message at the given severity level once every n times
'''
