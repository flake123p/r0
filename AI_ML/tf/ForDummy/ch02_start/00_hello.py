"""A simple TensorFlow application"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

# Create tensor
msg = tf.string_join(["Hello ", "TensorFlow!"])
# Launch session
with tf.Session() as sess:
    print('PRINT >>>', sess.run(msg))