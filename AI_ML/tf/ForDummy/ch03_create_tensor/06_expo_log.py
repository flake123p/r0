
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

##################################################################################################################
'''
square(x, name=None)    Returns the square of the argument
squared_difference(x, y, name=None) Subtracts the first argument from the second and returns the square
sqrt(x, name=None)      Returns the square root of the argument
rsqrt(x, name=None)     Returns the reciprocal of the square root
pow(x, y, name=None)    Returns elements of the first tensor raised to the power of the elements of the second vector
exp(x, name=None)       Returns the exponential function of the argument
expm1(x, name=None)     Returns the exponential function of the argument minus one, exp(x) - 1
log(x, name=None)       Returns the natural logarithm of the argument
log1p(x, name=None)     Returns the natural logarithm of the argument plus 1, log(x + 1)
erf(x, name=None)       Returns the error function of the argument
erfc(x, name=None)      Returns the complementary error function of the argument
'''

t = tf.constant([2.718])
t1 = tf.square(t) # 
t2 = tf.sqrt(t) #
t3 = tf.rsqrt(t) #
t4 = tf.log(t) #

import sys
with tf.Session() as sess:
    sess.run(tf.print("t4 :", t4))
    



