
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

##################################################################################################################
'''
tensordot(a, b, axes, name=None)    Returns the sum of products for the elements in the given axes
cross(a, b, name=None)              Returns the element-wise cross product
diag(diagonal, name=None)           Returns a matrix with the given diagonal values, other values set to zero
trace(x, name=None)                 Returns the sum of the diagonal elements
transpose(x, perm=None, name='transpose') Switches rows and columns

eye(num_rows,
    num_columns=None,
    batch_shape=None,
    dtype=tf.float32,
    name=None)
Creates an identity matrix with the given
shape and data type


matmul(a, b,
    transpose_a=False,
    transpose_b=False,
    adjoint_a=False,
    adjoint_b=False,
    a_is_sparse=False,
    b_is_sparse=False,
    name=None)
Returns the product of the two input matrices


norm(tensor,
    ord='euclidean',
    axis=None,
    keep_dims=False,
    name=None)
Returns the norm of the given axis of the
input tensor with the specified order


matrix_solve(A, b,
    adjoint=None,
    name=None)
Returns the tensor x, such that Ax = b, where
A is a matrix, and b is a vector


svd(tensor,
    full_matrices=False,
    compute_uv=True,
    name=None)
Factors the matrix into a unitary matrix, a
diagonal matrix, and the conjugate transpose
of the unitary matrix

qr(input, full_matrices=None, name=None)    Returns the eigenvectors and eigenvalues of the given matrix or matrices
einsum(equation, *inputs)                   Executes a custom mathematical operation
'''



m1 = tf.constant([[1, 2], [3, 4]])
m2 = tf.constant([[5, 6], [7, 8]])
e1 = tf.einsum('ij->ji', m1) # [[1, 3], [2, 4]]
e2 = tf.einsum('ij,jk->ik', m1, m2) # [[19, 22], [43, 50]]

import sys
with tf.Session() as sess:
    sess.run(tf.print("e1 :", e1))
    sess.run(tf.print("e2 :", e2))
    



