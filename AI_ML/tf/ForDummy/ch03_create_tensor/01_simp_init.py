
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

##################################################################################################################

zero_tensor = tf.zeros([3])
print(zero_tensor)

one_tensor = tf.ones([4, 4])
print(one_tensor)

fill_tensor = tf.fill([1, 2, 3], 81.0)
print(fill_tensor)

lin_tensor = tf.linspace(5., 9., 5)
print(lin_tensor)

range_tensor = tf.range(3., 7., delta=0.5)
print(range_tensor)

range_tensor2 = tf.range(1.5, delta=0.3)
print(range_tensor2)


print('Session Run:')
import sys
with tf.Session() as sess:
    sess.run(tf.print(zero_tensor))
    sess.run(tf.print(one_tensor))
    sess.run(tf.print(fill_tensor))
    sess.run(tf.print(lin_tensor))
    sess.run(tf.print(range_tensor))
    sess.run(tf.print(range_tensor2))
