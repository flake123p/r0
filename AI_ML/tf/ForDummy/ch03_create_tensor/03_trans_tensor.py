
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

##################################################################################################################

import sys
with tf.Session() as sess:
    vec = tf.constant([[1.1, 2.2, 3.3, 4.4]])
    sess.run(tf.print("Original:", vec))

    vec = tf.cast(vec, dtype=tensorflow.int32)
    sess.run(tf.print("Cast:", vec))

    mat = tf.reshape(vec, [2, 2])
    sess.run(tf.print("Reshape:\n", mat))

    r0 = tf.reverse(mat, [0])
    sess.run(tf.print("Reverse 0:\n", r0))

    r1 = tf.reverse(mat, [1])
    sess.run(tf.print("Reverse 1:\n", r1))

    sq_pre = tf.constant([[1, 2]])
    sess.run(tf.print("Sq_Pre:", sq_pre))

    sq = tf.squeeze(sq_pre)
    sess.run(tf.print("Squeeze :", sq))

    mat = tf.constant([[1., 2., 3.], [4., 5., 6.], [7., 8., 9.]])
    slice_mat = tf.slice(mat, [1, 1], [2, 2])
    sess.run(tf.print("Slice :", slice_mat))

    t1 = tf.constant([1., 2.])
    t2 = tf.constant([3., 4.])
    t3 = tf.constant([5., 6.])
    t4 = tf.stack([t1, t2, t3])
    sess.run(tf.print("Stack :", t4))

    u0, u1, u2 = tf.unstack(t4)
    sess.run(tf.print("Unstack :", u0, u1, u2))



