
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

##################################################################################################################
'''
add(x, y, name=None)        Adds two tensors
subtract(x, y, name=None)   Subtracts two tensors
multiply(x, y, name=None)   Multiplies two tensors
divide(x, y, name=None)     Divides the elements of two tensors
div(x, y, name=None)        Divides the elements of two tensors (int in, int out)
add_n(inputs, name=None)    Adds multiple tensors
scalar_mul(scalar, x)       Scales a tensor by a scalar value
mod(x, y, name=None)        Performs the modulo operation
abs(x, name=None)           Computes the absolute value
negative(x, name=None)      Negates the tensor's elements
sign(x, name=None)          Extracts the signs of the tensor's element
reciprocal(x, name=None)    Computes the reciprocals
'''

a = tf.constant([3., 3., 3.])
b = tf.constant([2., 2., 2.])
sum = tf.add(a, b) # [ 5. 5. 5. ]
diff = tf.subtract(a, b) # [ 1. 1. 1. ]
prod = tf.multiply(a, b) # [ 6. 6. 6. ]
quot = tf.divide(a, b) # [ 1.5 1.5 1.5 ]
total = tf.add(a, b) # [ 5. 5. 5. ]
total2 = a + b # [ 5. 5. 5. ]

import sys
with tf.Session() as sess:
    sess.run(tf.print("a :", a))
    sess.run(tf.print("b :", b))
    sess.run(tf.print("sum :", sum))
    sess.run(tf.print("diff :", diff))
    sess.run(tf.print("prod :", prod))
    sess.run(tf.print("quot :", quot))
    sess.run(tf.print("total :", total))
    sess.run(tf.print("total2 :", total2))
    



