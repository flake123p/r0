
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

##################################################################################################################
'''
round(x, name=None)     Rounds to the nearest integer, rounding up if there are two nearest integers
rint(x, name=None)      Rounds to the nearest integer, rounding to the nearest even integer if there are two nearest integers
ceil(x, name=None)      Returns the smallest integer greater than the value
floor(x, name=None)     Returns the greatest integer less than the value
maximum(x, y, name=None) Returns a tensor containing the larger element of each input tensor
minimum(x, y, name=None) Returns a tensor containing the smaller element of each input tensor
argmax(x, axis=None, name=None, dimension=None) Returns the index of the greatest element in the tensor
argmin(x, axis=None, name=None, dimension=None) Returns the index of the smallest element in the tensor
'''

t = tf.constant([-6.5, -3.5, 3.5, 6.5])
r1 = tf.round(t) # [-6. -4. 4. 6.]
r2 = tf.rint(t) # [-6. -4. 4. 6.]
r3 = tf.ceil(t) # [-6. -3. 4. 7.]
r4 = tf.floor(t) # [-7. -4. 3. 6.]

t1 = tf.constant([0, -2, 4, 6])
t2 = tf.constant([[1, 3], [7, 2]])
r1 = tf.argmin(t1) # 1
r2 = tf.argmax(t2) # [ 1 0 ]

import sys
with tf.Session() as sess:
    sess.run(tf.print("r1 :", r1))
    sess.run(tf.print("r2 :", r2))
    



