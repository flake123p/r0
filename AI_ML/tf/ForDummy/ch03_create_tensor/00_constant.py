#
# https://stackoverflow.com/questions/46209217/what-does-the-output-mean-when-i-print-a-tensor-in-tensorflow
#
# Const_1 means the name of this operation and name of an operation should be unique in a single graph. 
# Port 0 means the first output tensor of this op. 
# Some operation has many output tensors like tf.split and these tensors are named with the a port after the name of this operation.

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

##################################################################################################################

t1 = tf.constant([1.5, 2.5, 3.5])

t2 = tf.constant([['b', 'b'], ['b', 'b'], ['c', 'c']])

t3 = tf.range(10)

print(t1)

print(t2)

print(t3)

print('Session Run:')
import sys
with tf.Session() as sess:
    sess.run(tf.print("t1:", t1, output_stream=sys.stdout))
    sess.run(tf.print(t2))
    sess.run(tf.print("t3:", t3, output_stream=sys.stdout))
