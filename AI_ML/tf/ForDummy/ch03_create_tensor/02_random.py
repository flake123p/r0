
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

##################################################################################################################

norm = tf.random_normal([1,3,2], mean=0.0, stddev=1.0, dtype=tf.float32, seed=None, name=None)
print(norm)

trun = tf.truncated_normal([5], mean=0.0, stddev=1.0, dtype=tf.float32, seed=None, name=None)
print(trun)

unif = tf.random_uniform([5], minval=0, maxval=None, dtype=tf.float32, seed=None, name=None)
print(unif)

tf.set_random_seed(1234)

print('Session Run:')
import sys
with tf.Session() as sess:
    sess.run(tf.print(norm))
    sess.run(tf.print(trun))
    before = sess.run(unif)
    after = tf.random_shuffle(before, seed=None, name=None)
    sess.run(tf.print(before))
    sess.run(tf.print(after))

