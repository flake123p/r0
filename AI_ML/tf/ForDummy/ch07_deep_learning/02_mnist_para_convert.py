import pickle
import math
from math import pow
import sys
from sys import exit
import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 


# Prototype
def ndarray_to_c(name:str, ary):
    with open(name + '.txt', 'w') as file:
        storage = np.ravel(ary)
        print(name)
        file.write('int ' + name + '_ndim = ' + "{:d};\n\n".format(ary.ndim))
        
        file.write('int ' + name + '_prod = ' + "{:d};\n\n".format(np.prod(ary.shape)))
        
        file.write('int ' + name + '_shape[] = {\n')
        for s in ary.shape:
            file.write("    {:d},\n".format(s))
        file.write('};\n\n')

        #if type(ary)
        print(type(storage[0]))
        if type(storage[0]) == np.float32:
            #print('npF32')
            file.write('float ' + name + '_storage[] = {\n')
            for s in storage:
                file.write("    {:.30f},\n".format(s))
            file.write('};\n\n')
        else:
            print('Unsupport type:', type(storage[0]))
            exit(1)



file = open('my_w0.p', 'rb')
w0 = pickle.load(file)
file.close()

# print(w0)
# print(w0.shape)
# print(w0.ndim)

w0 = np.transpose(w0)
ndarray_to_c('local_w0', w0)
# Weight shape is 784 x 200 => Better to treat it as A matrix
# x = np.ravel(w0)

file = open('my_w1.p', 'rb')
w1 = pickle.load(file)
file.close()
w1 = np.transpose(w1)
ndarray_to_c('local_w1', w1)

file = open('my_w2.p', 'rb')
w2 = pickle.load(file)
file.close()
w2 = np.transpose(w2)
ndarray_to_c('local_w2', w2)

file = open('my_w3.p', 'rb')
w3 = pickle.load(file)
file.close()
w3 = np.transpose(w3)
ndarray_to_c('local_w3', w3)

file = open('my_b0.p', 'rb')
b0 = pickle.load(file)
file.close()
ndarray_to_c('local_b0', b0)

file = open('my_b1.p', 'rb')
b1 = pickle.load(file)
file.close()
ndarray_to_c('local_b1', b1)

file = open('my_b2.p', 'rb')
b2 = pickle.load(file)
file.close()
ndarray_to_c('local_b2', b2)

file = open('my_b3.p', 'rb')
b3 = pickle.load(file)
file.close()
ndarray_to_c('local_b3', b3)