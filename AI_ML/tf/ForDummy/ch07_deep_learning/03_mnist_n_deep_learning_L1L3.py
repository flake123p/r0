import math
from math import pow
import sys
from sys import exit
import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow
import tensorflow as tf
import tensorflow.contrib.learn as learn
'''
conda create -n tf114p36 python=3.6 spyder
conda activate tf114p36

pip install --upgrade pip
pip install spyder numpy scipy pandas matplotlib sympy cython

pip install tensorflow==1.4.0
pip install tensorflow-gpu==1.4.0
conda install cudatoolkit=8.0 -c https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/linux-64/
conda install cudnn=6.0 -c https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/linux-64/

# Run withou python warning:
https://stackoverflow.com/questions/14463277/how-to-disable-python-warnings
python -W ignore foo.py
'''
##################################################################################################################
tensorflow.set_random_seed(0)
# Read MNIST data
dataset = learn.datasets.mnist.read_data_sets('MNIST-data',
one_hot=True)
# Placeholders for MNIST images
img_holder = tf.placeholder(tf.float32, [None, 784])
lbl_holder = tf.placeholder(tf.float32, [None, 10])
# Layer settings
hid_nodes = 200
out_nodes = 10
# Define weights
w0 = tf.Variable(tf.random_normal([784, hid_nodes]))
# w1 = tf.Variable(tf.random_normal([hid_nodes, hid_nodes]))
# w2 = tf.Variable(tf.random_normal([hid_nodes, hid_nodes]))
w3 = tf.Variable(tf.random_normal([hid_nodes, out_nodes]))
# Define biases
b0 = tf.Variable(tf.random_normal([hid_nodes]))
# b1 = tf.Variable(tf.random_normal([hid_nodes]))
# b2 = tf.Variable(tf.random_normal([hid_nodes]))
b3 = tf.Variable(tf.random_normal([out_nodes]))
# Create layers
layer_1 = tf.add(tf.matmul(img_holder, w0), b0)
layer_1 = tf.nn.relu(layer_1)
# layer_2 = tf.add(tf.matmul(layer_1, w1), b1)
# layer_2 = tf.nn.relu(layer_2)
# layer_3 = tf.add(tf.matmul(layer_2, w2), b2)
# layer_3 = tf.nn.relu(layer_3)
out_layer = tf.matmul(layer_1, w3) + b3
# Compute loss
loss = tf.reduce_mean(
  tf.nn.softmax_cross_entropy_with_logits(
    logits=out_layer, labels=lbl_holder))
# Create optimizer
learning_rate = 0.01
num_epochs = 15
batch_size = 100
num_batches = int(dataset.train.num_examples/batch_size)
optimizer = tf.train.AdamOptimizer(learning_rate).minimize(loss)
# Initialize variables
init = tf.global_variables_initializer()

# print("trainable variables:", out_layer.trainable_variables)
# print("all variables:", out_layer.variables)

# Launch session
with tf.Session() as sess:
  sess.run(init)
  # Loop over epochs
  for epoch in range(num_epochs):
  # Loop over batches
    for batch in range(num_batches):
      img_batch, lbl_batch = dataset.train.next_batch(batch_size)
      _, lossVal = sess.run([optimizer, loss], feed_dict={img_holder: img_batch, lbl_holder: lbl_batch})
    print('loss: ', lossVal)
  # Determine success rate
  prediction = tf.equal(tf.argmax(out_layer, 1), tf.argmax(lbl_holder, 1))
  success = tf.reduce_mean(tf.cast(prediction, tf.float32))
  print('[Test]  Success rate: ', sess.run(success, feed_dict={img_holder: dataset.test.images, lbl_holder: dataset.test.labels}))
  
  prediction = tf.equal(tf.argmax(out_layer, 1), tf.argmax(lbl_holder, 1))
  success = tf.reduce_mean(tf.cast(prediction, tf.float32))
  print('[Train] Success rate: ', sess.run(success, feed_dict={img_holder: dataset.train.images, lbl_holder: dataset.train.labels}))

# Log:
'''
loss:  2.4503686
loss:  0.416946
loss:  0.34614676
loss:  0.08907767
loss:  0.03168932
loss:  0.7256887
loss:  0.2519616
loss:  0.26227567
loss:  0.040619615
loss:  0.02289754
loss:  0.13949504
loss:  0.052693557
loss:  0.1681616
loss:  0.2411243
loss:  0.18837889
[Test]  Success rate:  0.966
[Train] Success rate:  0.9873273
'''

#
# tuning:
#
'''
Input standardization: Preprocesses input data to statistically resemble training data

Weight initialization: Obtains suitable values for initial weights

Batch normalization: Processes data before the activation function to reduce the likelihood of saturation

Regularization: Reduces the likelihood of overfitting
'''
