import math
from math import pow
import sys
from sys import exit
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()
'''
1.
$ tensorboard --logdir=log

2.
Open Your Browser with: http://localhost:6006/
'''
##################################################################################################################
a = tf.constant([1.0, 1.0])
smax1 = tf.nn.softmax(a)
b = tf.constant([0.0, -10.0])
smax2 = tf.nn.softmax(b)

init = tf.global_variables_initializer()
# Launch session
with tf.Session() as sess:
    print('smax1 =', sess.run(smax1))
    print('smax2 =', sess.run(smax2))
