import math
from math import pow
import sys
from sys import exit
import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow as tf
import tensorflow.contrib.learn as learn
'''
conda create -n tf114p36 python=3.6 spyder
conda activate tf114p36

pip install --upgrade pip
pip install spyder numpy scipy pandas matplotlib sympy cython

pip install tensorflow==1.4.0
pip install tensorflow-gpu==1.4.0
conda install cudatoolkit=8.0 -c https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/linux-64/
conda install cudnn=6.0 -c https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/linux-64/

# Run withou python warning:
https://stackoverflow.com/questions/14463277/how-to-disable-python-warnings
python -W ignore foo.py
'''
##################################################################################################################
# Read MNIST data
dataset = learn.datasets.mnist.read_data_sets('MNIST-data', one_hot=True)
# Placeholders for MNIST images
image_holder = tf.placeholder(tf.float32, [None, 784])
label_holder = tf.placeholder(tf.float32, [None, 10])
# Variables
m = tf.Variable(tf.zeros([784, 10]))
b = tf.Variable(tf.zeros([10]))
# Compute loss
loss = tf.reduce_mean(
  tf.nn.softmax_cross_entropy_with_logits(
    logits=tf.matmul(image_holder, m) + b, labels=label_holder))

# Create optimizer
learning_rate = 0.01
num_epochs = 25
batch_size = 100
num_batches = int(dataset.train.num_examples/batch_size)
optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss)
# Initialize variables
init = tf.global_variables_initializer()
# Launch session
with tf.Session() as sess:
  sess.run(init)
  # Loop over epochs
  for epoch in range(num_epochs):
    # Loop over batches
    for batch in range(num_batches):
      image_batch, label_batch = dataset.train.next_batch(batch_size)
      _, lossVal = sess.run([optimizer, loss], feed_dict={image_holder: image_batch, label_holder: label_batch})
    print(epoch, '/', num_epochs, ' - loss: ', lossVal)
  # Display the final loss
  print('Final loss: ', lossVal)
