
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

'''
1.
$ tensorboard --logdir=log

2.
Open Your Browser with: http://localhost:6006/
'''

#
# https://stackoverflow.com/questions/37743574/hard-limiting-threshold-activation-function-in-tensorflow
#
def binary_activation(x):
    cond = tf.less(x, tf.zeros(tf.shape(x)))
    out = tf.where(cond, tf.zeros(tf.shape(x)), tf.ones(tf.shape(x)))
    return out
##################################################################################################################
# Input values
N = 40
x = tf.lin_space(0., 5., N)
y = tf.constant([0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
                 1., 0., 0., 1., 0., 0., 0., 1., 0., 0.,
                 1., 0., 1., 1., 1., 1., 1., 1., 1., 1.,
                 1., 1., 1., 1., 1., 1., 1., 1., 1., 1.])
# Variables
m = tf.constant(1.)
b = tf.Variable(0.)
# Compute model and loss
model = tf.nn.sigmoid(tf.add(tf.multiply(x, m), b))
loss = tf.reduce_sum(tf.abs(y - model))
# Create optimizer
learn_rate = 0.005
num_epochs = 350
optimizer = tf.train.GradientDescentOptimizer(learn_rate).minimize(loss)
# Initialize variables
init = tf.global_variables_initializer()
# Launch session
with tf.Session() as sess:
    sess.run(init)
    # Run optimizer
    for epoch in range(num_epochs):
        sess.run(optimizer)
        print('loss =', sess.run(loss))
    # Display results
    print('x =', sess.run(x))
    print('prev  =', sess.run(tf.add(tf.multiply(x, m), b)))
    print('model =', sess.run(model))
    print('m =', sess.run(m))
    print('b =', sess.run(b))