
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

'''
1.
$ tensorboard --logdir=log

2.
Open Your Browser with: http://localhost:6006/
'''
##################################################################################################################
# Input values
N = 9
x = tf.lin_space(-8., 8., N)

model = tf.nn.sigmoid(x)

init = tf.global_variables_initializer()
# Launch session
with tf.Session() as sess:
    print('x =', sess.run(x))
    print('model =', sess.run(model))
