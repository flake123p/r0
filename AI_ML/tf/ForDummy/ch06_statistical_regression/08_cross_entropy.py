import math
from math import pow
import sys
from sys import exit
# import torch
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import tensorflow
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()
'''
1.
$ tensorboard --logdir=log

2.
Open Your Browser with: http://localhost:6006/
'''
##################################################################################################################

def sum_likelihood(sig, cla):
    if len(sig) != len(cla):
        print("Length error!!!")
        exit()
    sum = 0.0
    for i in range(len(sig)):
        l = pow(sig[i], cla[i]) * pow((1-sig[i]), (1-cla[i]))
        sum += l
    return sum

def cross_entropy_log(sig, cla):
    if len(sig) != len(cla):
        print("Length error!!!")
        exit()
    sum = 0.0
    for i in range(len(sig)):
        l = cla[i] * math.log10(sig[i] + 1e-45)
        sum += l
    return -sum

x = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0]

a = [0.0, 0.0, 0.0, 1.0, 1.0, 1.0]
b = [0.0, 0.0, 1.0, 1.0, 1.0, 1.0]

print("\n X in 2 scenarios:")
print(sum_likelihood(x, a))
print(sum_likelihood(x, b))

y = [0.0, 0.1, 0.3, 0.7, 0.9, 1.0]

print("\n Y in 2 scenarios:")
print(sum_likelihood(y, a))
print(sum_likelihood(y, b))

z = [0.0, 0.0, 0.0, 1.0, 1.0, 1.0]

print("\n Z in 2 scenarios:")
print(sum_likelihood(z, a))
print(sum_likelihood(z, b))

u    = [1.0, 0.0]
scnA = [0.0, 1.0]
scnB = [1.0, 0.0]
print("\n U in 2 scenarios:")
print('liklihood   A =', sum_likelihood(u, scnA))
print('liklihood   B =', sum_likelihood(u, scnB))
print('cro_en_log  A =   ', cross_entropy_log(u, scnA))
print('cro_en_log  B =   ', cross_entropy_log(u, scnB))
u    = [0.0, 0.6]
scnA = [0.0, 1.0]
scnB = [1.0, 0.0]
print("\n U in 2 scenarios:")
print('liklihood   A =', sum_likelihood(u, scnA))
print('liklihood   B =', sum_likelihood(u, scnB))
print('cro_en_log  A =   ', cross_entropy_log(u, scnA))
print('cro_en_log  B =   ', cross_entropy_log(u, scnB))

# u = torch.tensor([1.0, 0.0])
# a = torch.tensor([0.0, 1.0])
# b = torch.tensor([1.0, 0.0])
# result1 = torch.nn.functional.cross_entropy(u, a)
# result2 = torch.nn.functional.cross_entropy(u, b)
# print('troch CroEn =', result1)
# print('troch CroEn =', result2)
# loss = torch.nn.CrossEntropyLoss(reduction='none')
# result1 = loss(u, a)
# result2 = loss(u, b)
# print('troch CroEn =', result1)
# print('troch CroEn =', result2)

print('TF cross entropy')
# Input values
u = tf.constant([1., 0.])
ScnA = tf.constant([0., 1.])
ScnB = tf.constant([1., 0.])
lossA = tf.reduce_mean(
    tf.nn.softmax_cross_entropy_with_logits_v2(
        logits=u, labels=ScnA))
lossB = tf.reduce_mean(
    tf.nn.softmax_cross_entropy_with_logits_v2(
        logits=u, labels=ScnB))

init = tf.global_variables_initializer()
# Launch session
with tf.Session() as sess:
    print('lossA =', sess.run(lossA))
with tf.Session() as sess:
    print('lossB =', sess.run(lossB))