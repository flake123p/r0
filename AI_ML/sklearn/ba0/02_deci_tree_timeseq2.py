import numpy as np
from sklearn.tree import DecisionTreeRegressor, plot_tree
import matplotlib.pyplot as plt
import sys
import torch

def pickle_to(var, file : str):
    import pickle
    with open(file, 'wb') as my_file:
        pickle.dump(var, my_file)

def pickle_from(file : str):
    import pickle
    try:
        with open(file, 'rb') as my_file:
            return pickle.load(my_file)
    except IOError as exc:    # Python 2. For Python 3 use OSError
        tb = sys.exc_info()[-1]
        lineno = tb.tb_lineno
        filename = tb.tb_frame.f_code.co_filename
        print('{} \nException at {} line {}.'.format(exc.strerror, filename, lineno))
        sys.exit(exc.errno)
        return None

input = pickle_from('../../data/timeseq/db0/01_train_input.pkl')
label = pickle_from('../../data/timeseq/db0/02_train_label.pkl')

gLen = len(label)
print('gLen =', gLen)

input = input.numpy()
label = label.numpy()
print('Shape of input =', input.shape)
print('Shape of label =', label.shape)

x = list(range(0, gLen))

regr_1 = DecisionTreeRegressor(max_depth=6)
regr_2 = DecisionTreeRegressor(max_depth=5)
regr_3 = DecisionTreeRegressor(max_depth=1)
regr_1.fit(input, label)
regr_2.fit(input, label)
regr_3.fit(input, label)

# X_test = np.arange(0.0, 5.0, 0.01)[:, np.newaxis]
y_1 = regr_1.predict(input)
y_2 = regr_2.predict(input)
y_3 = regr_3.predict(input)

plt.figure(figsize=(12, 8))
plt.scatter(x, label, s=20, edgecolor="black", c="darkorange", label="data")
plt.plot(x, y_1, color="cornflowerblue", label="max_depth=2", linewidth=2)
plt.plot(x, y_2, color="yellowgreen", label="max_depth=5", linewidth=2)
plt.plot(x, y_3, color="orange", label="max_depth=1", linewidth=2)
plt.xlabel("data")
plt.ylabel("target")
plt.title("Decision Tree Regression")
plt.legend(bbox_to_anchor=[1.1, 0.5], loc='center right')
# plt.show()

plt.figure(figsize=(12, 8))
plot_tree(regr_2, filled=True)
plt.title("Decision tree trained on all the iris features")
# plt.show()
########################
###### Draw plots ######
########################
if 1:
    plt.show()


##############################
###### Output to a file ######
##############################
if 0:
    output = y_1
    output = torch.tensor(output, dtype=torch.float32)

    print(output)
    print(type(output))

    pickle_to(output, '02_other_label.pkl')