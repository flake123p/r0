import numpy as np
from sklearn.tree import DecisionTreeRegressor
import matplotlib.pyplot as plt

x = list(range(0, 10))
print(x)
X_test = [i / 10 for i in x]
X_test = np.array(X_test).reshape(10, 1)
y = np.array([0, 1, 1, 0, 1, 1, 1, 0, 0, 0])
# rng = np.random.RandomState(1)
# X = np.sort(5 * rng.rand(80, 1), axis=0)
# y = np.sin(X).ravel()
# y[::5] += 3 * (0.5 - rng.rand(16))

regr_1 = DecisionTreeRegressor(max_depth=2)
regr_2 = DecisionTreeRegressor(max_depth=5)
regr_3 = DecisionTreeRegressor(max_depth=1)
regr_1.fit(X_test, y)
regr_2.fit(X_test, y)
regr_3.fit(X_test, y)

# X_test = np.arange(0.0, 5.0, 0.01)[:, np.newaxis]
y_1 = regr_1.predict(X_test)
y_2 = regr_2.predict(X_test)
y_3 = regr_3.predict(X_test)



plt.figure()
plt.scatter(x, y, s=20, edgecolor="black", c="darkorange", label="data")
plt.plot(x, y_1, color="cornflowerblue", label="max_depth=2", linewidth=2)
plt.plot(x, y_2, color="yellowgreen", label="max_depth=5", linewidth=2)
plt.plot(x, y_3, color="orange", label="max_depth=1", linewidth=2)
plt.xlabel("data")
plt.ylabel("target")
plt.title("Decision Tree Regression")
plt.legend()
plt.show()