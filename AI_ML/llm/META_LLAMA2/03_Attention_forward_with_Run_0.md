
# Run 0 from 01_in_out_tokens_timing.md
Loaded in 7.27 seconds
in  tokens: 14
out tokens: 12
time: 735.18 ms
time: 61.26 ms/per out tokens

3 + 4 =

Assistant:  Sure! 3 + 4 = 7.


## inputs:
x = tensor(1, 14, 4096)
start_pos = 0
freqs_cis = tensor(14, 64)
mask = tensor(14, 14) (upper right triangle: -inf, lower left triangle: 0)


## line 23: bsz, seqlen, _ = x.shape
bsz = 1
seqlen = 14


## line 24: xq, xk, xv = self.wq(x), self.wk(x), self.wv(x)
wq, wk, wv = tensor(4096, 4096)
xq, xk, xv = tensor(1, 14, 4096)


## line 26: xq = xq.view(bsz, seqlen, self.n_local_heads, self.head_dim)
self.n_local_heads = 32
self.head_dim = 128
xq = tensor(1, 14, 32, 128)


## line 27: xk = xk.view(bsz, seqlen, self.n_local_kv_heads, self.head_dim)
self.n_local_kv_heads = 32
self.head_dim = 128
xk = tensor(1, 14, 32, 128)


## line 28: xv = xv.view(bsz, seqlen, self.n_local_kv_heads, self.head_dim)
self.n_local_kv_heads = 32
self.head_dim = 128
xv = tensor(1, 14, 32, 128)


## line 30: xq, xk = apply_rotary_emb(xq, xk, freqs_cis=freqs_cis)
xq = tensor(1, 14, 32, 128)
xk = tensor(1, 14, 32, 128)


## line 32: self.cache_k = self.cache_k.to(xq)
self.cache_k = tensor(6, 512, 32, 128) (all zeros)

torch.Tensor.to:
    Performs Tensor dtype and/or device conversion.

final dtype = fp16
final device = cuda


## line 33: self.cache_v = self.cache_v.to(xq)
(same as above)


## line 35: self.cache_k[:bsz, start_pos : start_pos + seqlen] = xk
bsz = 1
start_pos = 0
seq_len = 14

Before:
self.cache_k = tensor(6, 512, 32, 128) (all zeros)

After:
self.cache_k = 
tensor(0, 0, 32, 128) ~ tensor(0, 13, 32, 128) = xk = tensor(1, 14, 32, 128)


## line 36: self.cache_v[:bsz, start_pos : start_pos + seqlen] = xv
(same as above)


## line 38: keys = self.cache_k[:bsz, : start_pos + seqlen]
keys = tensor(1, 14, 32, 128)


## line 39: values = self.cache_v[:bsz, : start_pos + seqlen]
values = tensor(1, 14, 32, 128)


## line 42: keys = repeat_kv(keys, self.n_rep)  # (bs, cache_len + seqlen, n_local_heads, head_dim)
self.n_rep = 1
keys = tensor(1, 14, 32, 128)


## line 43: values = repeat_kv(values, self.n_rep)  # (bs, cache_len + seqlen, n_local_heads, head_dim)
self.n_rep = 1
values = tensor(1, 14, 32, 128)


## line 45: xq = xq.transpose(1, 2)  # (bs, n_local_heads, seqlen, head_dim)
Before:
xq = tensor(1, 14, 32, 128)
is_contiguous() = True

After:
xq = tensor(1, 32, 14, 128)
is_contiguous() = False


## line 46: keys = keys.transpose(1, 2) # (bs, n_local_heads, cache_len + seqlen, head_dim)
Before:
keys = tensor(1, 14, 32, 128)
is_contiguous() = True

After:
keys = tensor(1, 32, 14, 128)
is_contiguous() = False


## line 47: values = values.transpose(1, 2) # (bs, n_local_heads, cache_len + seqlen, head_dim)
Before:
values = tensor(1, 14, 32, 128)
is_contiguous() = True

After:
values = tensor(1, 32, 14, 128)
is_contiguous() = False


## line 48: scores = torch.matmul(xq, keys.transpose(2, 3)) / math.sqrt(self.head_dim)
scores = torch.matmul(tensor(1, 32, 14, 128), tensor(1, 32, 128, 14)) = tensor(1, 32, 14, 14)

self.head_dim = 128


## line 49: if mask is not None:
mask = tensor(14, 14) (upper right triangle: -inf, lower left triangle: 0)


## line 50: scores = scores + mask  # (bs, n_local_heads, seqlen, cache_len + seqlen)
Before:
scores = tensor(1, 32, 14, 14)
mask = tensor(14, 14) (fp16)

After:
scores = tensor(1, 32, 14, 14) (fp16)


## line 51: scores = F.softmax(scores.float(), dim=-1).type_as(xq)

self.float() is equivalent to self.to(torch.float32)

scores(fp16) -> scores(fp32) -> softmax -> output(fp32) -> back to fp16: scores = tensor(1, 32, 14, 14)

scores.is_contiguous() = True !!


## line 52: output = torch.matmul(scores, values)  # (bs, n_local_heads, seqlen, head_dim)
scores = tensor(1, 32, 14, 14)
scores.is_contiguous() = True

values = tensor(1, 32, 14, 128)
values.is_contiguous() = False

output = tensor(1, 32, 14, 128)
output.is_contiguous() = True


## line 53: output = output.transpose(1, 2).contiguous().view(bsz, seqlen, -1)
bsz = 1
seqlen = 14

output = tensor(1，14，4096)
output.is_contiguous() = True


## line 54: return self.wo(output)
self.wo.weight = tensor(4096, 4096)