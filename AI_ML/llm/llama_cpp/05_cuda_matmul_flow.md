# 0927
CPU:
ggml_vec_dot_q4_0_q8_0()

multipy 2 int8 vec and covert to float:
mul_sum_i8_pairs_float()
https://github.com/ggerganov/llama.cpp/blob/master/ggml.c#L2496

Scale back to float with fmadd
_mm256_fmadd_ps()
https://github.com/ggerganov/llama.cpp/blob/master/ggml.c#L2499

GPU:
vec_dot_q4_0_q8_1_impl()
// SIMD Integer dot product __dp4a() of quantized values
https://github.com/ggerganov/llama.cpp/blob/master/ggml-cuda.cu#L1596


# Old

key function: ggml_cuda_op_mul_mat_vec()

local variable use_mul_mat_vec_q() is alwayt TRUE !!!!

src1 will be quantized to Q8_1 : quantize_row_q8_1_cuda()

Execute: mul_mat_vec_q4_0_q8_1_cuda then.
    src0 is q4_0
    src1 is q8_0
