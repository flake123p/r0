
# The Script:
```
# convert the 7B model to ggml FP16 format
python3 convert.py models/7B/

# quantize the model to 4-bits (using q4_0 method)
./quantize ./models/7B/ggml-model-f16.bin ./models/7B/ggml-model-q4_0.bin q4_0
```

# python3 convert.py
## 1.  Log (llama2 will convert to F32)
```
Loading model file /home/pupil/sda/ws/llm/llama2/llama/llama-2-7b/consolidated.00.pth
Loading vocab file /home/pupil/sda/ws/llm/llama2/llama/tokenizer.model
params: n_vocab:32000 n_embd:4096 n_mult:256 n_head:32 n_layer:32
Writing vocab...
[  1/291] Writing tensor tok_embeddings.weight                  | size  32000 x   4096  | type UnquantizedDataType(name='F32')
[  2/291] Writing tensor norm.weight                            | size   4096           | type UnquantizedDataType(name='F32')
[  3/291] Writing tensor output.weight                          | size  32000 x   4096  | type UnquantizedDataType(name='F32')
[  4/291] Writing tensor layers.0.attention.wq.weight           | size   4096 x   4096  | type UnquantizedDataType(name='F32')
[  5/291] Writing tensor layers.0.attention.wk.weight           | size   4096 x   4096  | type UnquantizedDataType(name='F32')
[  6/291] Writing tensor layers.0.attention.wv.weight           | size   4096 x   4096  | type UnquantizedDataType(name='F32')
[  7/291] Writing tensor layers.0.attention.wo.weight           | size   4096 x   4096  | type UnquantizedDataType(name='F32')
[  8/291] Writing tensor layers.0.attention_norm.weight         | size   4096           | type UnquantizedDataType(name='F32')
[  9/291] Writing tensor layers.0.feed_forward.w1.weight        | size  11008 x   4096  | type UnquantizedDataType(name='F32')
[ 10/291] Writing tensor layers.0.feed_forward.w2.weight        | size   4096 x  11008  | type UnquantizedDataType(name='F32')
[ 11/291] Writing tensor layers.0.feed_forward.w3.weight        | size  11008 x   4096  | type UnquantizedDataType(name='F32')
[ 12/291] Writing tensor layers.0.ffn_norm.weight               | size   4096           | type UnquantizedDataType(name='F32')
[ 13/291] Writing tensor layers.1.attention.wq.weight           | size   4096 x   4096  | type UnquantizedDataType(name='F32')
[ 14/291] Writing tensor layers.1.attention.wk.weight           | size   4096 x   4096  | type UnquantizedDataType(name='F32')
[ 15/291] Writing tensor layers.1.attention.wv.weight           | size   4096 x   4096  | type UnquantizedDataType(name='F32')
[ 16/291] Writing tensor layers.1.attention.wo.weight           | size   4096 x   4096  | type UnquantizedDataType(name='F32')
[ 17/291] Writing tensor layers.1.attention_norm.weight         | size   4096           | type UnquantizedDataType(name='F32')
[ 18/291] Writing tensor layers.1.feed_forward.w1.weight        | size  11008 x   4096  | type UnquantizedDataType(name='F32')
```

## 2.  Log (llama1 will convert to F16)
```
Loading model file /home/pupil/sda/ws/llm/models/7B/consolidated.00.pth
Loading vocab file /home/pupil/sda/ws/llm/models/tokenizer.model
params: n_vocab:32000 n_embd:4096 n_mult:256 n_head:32 n_layer:32
Writing vocab...
[  1/291] Writing tensor tok_embeddings.weight                  | size  32000 x   4096  | type UnquantizedDataType(name='F16')
[  2/291] Writing tensor norm.weight                            | size   4096           | type UnquantizedDataType(name='F32')
[  3/291] Writing tensor output.weight                          | size  32000 x   4096  | type UnquantizedDataType(name='F16')
[  4/291] Writing tensor layers.0.attention.wq.weight           | size   4096 x   4096  | type UnquantizedDataType(name='F16')
[  5/291] Writing tensor layers.0.attention.wk.weight           | size   4096 x   4096  | type UnquantizedDataType(name='F16')
[  6/291] Writing tensor layers.0.attention.wv.weight           | size   4096 x   4096  | type UnquantizedDataType(name='F16')
[  7/291] Writing tensor layers.0.attention.wo.weight           | size   4096 x   4096  | type UnquantizedDataType(name='F16')
[  8/291] Writing tensor layers.0.attention_norm.weight         | size   4096           | type UnquantizedDataType(name='F32')
[  9/291] Writing tensor layers.0.feed_forward.w1.weight        | size  11008 x   4096  | type UnquantizedDataType(name='F16')
[ 10/291] Writing tensor layers.0.feed_forward.w2.weight        | size   4096 x  11008  | type UnquantizedDataType(name='F16')
[ 11/291] Writing tensor layers.0.feed_forward.w3.weight        | size  11008 x   4096  | type UnquantizedDataType(name='F16')
[ 12/291] Writing tensor layers.0.ffn_norm.weight               | size   4096           | type UnquantizedDataType(name='F32')
[ 13/291] Writing tensor layers.1.attention.wq.weight           | size   4096 x   4096  | type UnquantizedDataType(name='F16')
[ 14/291] Writing tensor layers.1.attention.wk.weight           | size   4096 x   4096  | type UnquantizedDataType(name='F16')
[ 15/291] Writing tensor layers.1.attention.wv.weight           | size   4096 x   4096  | type UnquantizedDataType(name='F16')
[ 16/291] Writing tensor layers.1.attention.wo.weight           | size   4096 x   4096  | type UnquantizedDataType(name='F16')
```

### params.json
```
{"dim": 4096, "multiple_of": 256, "n_heads": 32, "n_layers": 32, "norm_eps": 1e-06, "vocab_size": -1}
```

### do_necessary_conversions
handle_quantization() -> llama is not quantized

convert_transformers_to_orig() -> Not executed

filter_and_sort_tensors() -> discard *.freqs

### pick_output_type
output_type_str
or
layers.0.attention.wq.weight's type

### convert_to_output_type

1D tensors are always F32.

### default_outfile
args.outfile
or
default_outfile()

### OutputFile.write_all

check_vocab_size() -> same, no raise error

write_file_header() -> params from json

write_vocab() -> binary write

write_tensor_header() -> binary write

ndarray.tofile ...

# ./quantize
```
./bin/quantize  /home/pupil/sda/ws/llm/models/7B/ggml-model-f16.bin /home/pupil/sda/ws/llm/models/7B/ggml-model-q4_0.bin q4_0
```

```
[MyP] quantize.cpp start
main: build = 895 (84e09a7)
main: quantizing '/home/pupil/sda/ws/llm/models/7B/ggml-model-f16.bin' to '/home/pupil/sda/ws/llm/models/7B/ggml-model-q4_0.bin' as Q4_0
llama.cpp: loading model from /home/pupil/sda/ws/llm/models/7B/ggml-model-f16.bin
llama.cpp: saving model to /home/pupil/sda/ws/llm/models/7B/ggml-model-q4_0.bin
[   1/ 291]                tok_embeddings.weight -     4096 x 32000, type =    f16, quantizing to q4_0 .. size =   250.00 MB ->    70.31 MB | hist: 0.037 0.016 0.025 0.039 0.057 0.077 0.096 0.111 0.116 0.111 0.096 0.077 0.057 0.039 0.025 0.021 
[   2/ 291]                          norm.weight -             4096, type =    f32, size =    0.016 MB
[   3/ 291]                        output.weight -     4096 x 32000, type =    f16, quantizing to q6_K .. size =   250.00 MB ->   102.54 MB | hist: 
[   4/ 291]         layers.0.attention.wq.weight -     4096 x  4096, type =    f16, quantizing to q4_0 .. size =    32.00 MB ->     9.00 MB | hist: 0.035 0.012 0.019 0.030 0.047 0.069 0.097 0.129 0.152 0.129 0.098 0.070 0.047 0.031 0.019 0.016 
[   5/ 291]         layers.0.attention.wk.weight -     4096 x  4096, type =    f16, quantizing to q4_0 .. size =    32.00 MB ->     9.00 MB | hist: 0.035 0.012 0.020 0.032 0.049 0.072 0.098 0.125 0.139 0.125 0.099 0.072 0.050 0.033 0.021 0.017 
[   6/ 291]         layers.0.attention.wv.weight -     4096 x  4096, type =    f16, quantizing to q4_0 .. size =    32.00 MB ->     9.00 MB | hist: 0.036 0.015 0.024 0.037 0.055 0.075 0.096 0.114 0.124 0.114 0.096 0.075 0.055 0.038 0.024 0.020 
[   7/ 291]         layers.0.attention.wo.weight -     4096 x  4096, type =    f16, quantizing to q4_0 .. size =    32.00 MB ->     9.00 MB | hist: 0.036 0.013 0.021 0.033 0.051 0.073 0.099 0.123 0.133 0.123 0.099 0.073 0.051 0.033 0.021 0.018 
[   8/ 291]       layers.0.attention_norm.weight -             4096, type =    f32, size =    0.016 MB
[   9/ 291]      layers.0.feed_forward.w1.weight -     4096 x 11008, type =    f16, quantizing to q4_0 .. size =    86.00 MB ->    24.19 MB | hist: 0.036 0.016 0.025 0.039 0.057 0.077 0.096 0.111 0.117 0.111 0.096 0.077 0.057 0.039 0.025 0.021 
[  10/ 291]      layers.0.feed_forward.w2.weight -    11008 x  4096, type =    f16, quantizing to q4_0 .. size =    86.00 MB ->    24.19 MB | hist: 0.036 0.015 0.025 0.039 0.056 0.077 0.097 0.112 0.117 0.112 0.097 0.077 0.056 0.039 0.025 0.021 
[  11/ 291]      layers.0.feed_forward.w3.weight -     4096 x 11008, type =    f16, quantizing to q4_0 .. size =    86.00 MB ->    24.19 MB | hist: 0.036 0.016 0.025 0.039 0.056 0.077 0.096 0.112 0.118 0.112 0.097 0.077 0.056 0.039 0.025 0.021 
[  12/ 291]             layers.0.ffn_norm.weight -             4096, type =    f32, size =    0.016 MB
```

[MyP] n_attention_wv    = 32 (USE K QUANTS)
[MyP] n_feed_forward_w2 = 32 (USE K QUANTS)

## ggml_quantize_q4_0()

Run just 1 loop

Dump: b=0, nb=4096000, k=131072000

## quantize_row_q4_0_reference()

// reference implementation for deterministic creation of model files


## test data:
Q4_0
 -3.200000,  -3.100000,  -3.000000,  -2.900000,  -2.800000,  -2.700001,  -2.600001,  -2.500001, 
 -2.400001,  -2.300001,  -2.200001,  -2.100001,  -2.000001,  -1.900001,  -1.800001,  -1.700001, 
 -1.600001,  -1.500001,  -1.400001,  -1.300001,  -1.200001,  -1.100001,  -1.000001,  -0.900001, 
 -0.800001,  -0.700001,  -0.600001,  -0.500001,  -0.400001,  -0.300001,  -0.200001,  -0.100001, 
 -0.000001,   0.099999,   0.199999,   0.299999,   0.399999,   0.499999,   0.599999,   0.699999, 
  0.799999,   0.899999,   0.999999,   1.099999,   1.199999,   1.299999,   1.399999,   1.499999, 
  1.599999,   1.699999,   1.799999,   1.899999,   1.999999,   2.099999,   2.199999,   2.299999, 
  2.399999,   2.499999,   2.599999,   2.699999,   2.799999,   2.899999,   2.999999,   3.099998, 
yIdx = 0, delta = 13926 (0.399902)
       0,        0,        0,        1,        1,        1,        1,        2, 
       2,        2,        2,        3,        3,        3,        3,        4, 
       4,        4,        4,        5,        5,        5,        5,        6, 
       6,        6,        6,        7,        7,        7,        7,        8, 
yIdx = 1, delta = 46643 (-0.387451)
       8,        8,        7,        7,        7,        7,        6,        6, 
       6,        6,        5,        5,        5,        5,        4,        4, 
       4,        4,        3,        3,        3,        3,        2,        2, 
       2,        2,        1,        1,        1,        1,        0,        0, 
yIdx = 0, delta = 13926 (0.399902)
 -3.199219,  -3.199219,  -3.199219,  -2.799316,  -2.799316,  -2.799316,  -2.799316,  -2.399414, 
 -2.399414,  -2.399414,  -2.399414,  -1.999512,  -1.999512,  -1.999512,  -1.999512,  -1.599609, 
 -1.599609,  -1.599609,  -1.599609,  -1.199707,  -1.199707,  -1.199707,  -1.199707,  -0.799805, 
 -0.799805,  -0.799805,  -0.799805,  -0.399902,  -0.399902,  -0.399902,  -0.399902,   0.000000, 
yIdx = 1, delta = 46643 (-0.387451)
 -0.000000,  -0.000000,   0.387451,   0.387451,   0.387451,   0.387451,   0.774902,   0.774902, 
  0.774902,   0.774902,   1.162354,   1.162354,   1.162354,   1.162354,   1.549805,   1.549805, 
  1.549805,   1.549805,   1.937256,   1.937256,   1.937256,   1.937256,   2.324707,   2.324707, 
  2.324707,   2.324707,   2.712158,   2.712158,   2.712158,   2.712158,   3.099609,   3.099609,

  1. Section size: 32 Float
  2. Step = Max_Number / 8 (8 for Q4?? Why not 16?? Maybe this is simple algo)
  3. Traverse pattern: 0, 16, 1, 17, ... 15, 31

-----------------------------------------------------------------------------------------------------
Q4_1
yIdx = 0, delta = 12957 (0.206665), min = 49766 (-3.199219)
       0,        0,        1,        1,        2,        2,        3,        3, 
       4,        4,        5,        5,        6,        6,        7,        7, 
       8,        8,        9,        9,        a,        a,        b,        b, 
       c,        c,        d,        d,        e,        e,        f,        f, 
yIdx = 1, delta = 12957 (0.206665), min = 32782 (-0.000001)
       0,        0,        1,        1,        2,        2,        3,        3, 
       4,        4,        5,        5,        6,        6,        7,        7, 
       8,        8,        9,        9,        a,        a,        b,        b, 
       c,        c,        d,        d,        e,        e,        f,        f, 
yIdx = 0, delta = 12957 (0.206665), min = 49766 (-3.199219)
 -3.199219,  -3.199219,  -2.992554,  -2.992554,  -2.785889,  -2.785889,  -2.579224,  -2.579224, 
 -2.372559,  -2.372559,  -2.165894,  -2.165894,  -1.959229,  -1.959229,  -1.752563,  -1.752563, 
 -1.545898,  -1.545898,  -1.339233,  -1.339233,  -1.132568,  -1.132568,  -0.925903,  -0.925903, 
 -0.719238,  -0.719238,  -0.512573,  -0.512573,  -0.305908,  -0.305908,  -0.099243,  -0.099243, 
yIdx = 1, delta = 12957 (0.206665), min = 32782 (-0.000001)
 -0.000001,  -0.000001,   0.206664,   0.206664,   0.413329,   0.413329,   0.619994,   0.619994, 
  0.826659,   0.826659,   1.033324,   1.033324,   1.239989,   1.239989,   1.446654,   1.446654, 
  1.653319,   1.653319,   1.859985,   1.859985,   2.066649,   2.066649,   2.273314,   2.273314, 
  2.479980,   2.479980,   2.686645,   2.686645,   2.893310,   2.893310,   3.099975,   3.099975, 

Q4_0 : U8, Block Size 32, Equation: (val - 8) * delta
Q4_1 : U8, Block Size 32, Equation: val * delta + min

## quan func:
quan func:
    quantize_row_q4_0_reference()
    quantize_row_q4_1_reference()
    quantize_row_q8_0_reference()
    quantize_row_q8_1_reference()
    ...

dequan func:
    dequantize_row_q4_0()
    dequantize_row_q4_1()
    dequantize_row_q8_0()


## file_saver.write_tensor()

write raw data of quantized C structure * n