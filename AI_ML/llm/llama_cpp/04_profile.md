
# vec_dot

Table Auto Format:
Linux: Ctrl + Shift + I
Win:   Alt + Shift + F

| CPU                    | Called | Ins.C  | Percent |
| ---------------------- | ------ | ------ | ------- |
| ggml_vec_dot_q4_0_q8_0 | 28557K | 94183M | 97%     |
| ggml_vec_dot_q6_K_q8_K | 672K   | 1482M  | 1.5%    |
| ggml_vec_dot_f16       | 3062K  | 449M   | 0.46%   |
| quantize_row_q8_0      | 4704   | 37M    | 0.04%   |

Total = 97123M