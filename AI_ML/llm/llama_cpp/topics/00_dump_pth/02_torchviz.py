#
# https://stackoverflow.com/questions/52468956/how-do-i-visualize-a-net-in-pytorch
#
# https://appsilon.com/visualize-pytorch-neural-networks/
#
import torch
from torchviz import make_dot

pthfile = '/home/pupil/temp/llama/1/consolidated.00.pth'

net = torch.load(pthfile, map_location=torch.device('cpu'))

make_dot(net.mean(), params=dict(net.named_parameters())) # ?????????????????