#
# https://kuanhoong.medium.com/how-to-use-tensorboard-with-pytorch-e2b84aa55e67
#
import torch
from torch.utils.tensorboard import SummaryWriter

pthfile = '/home/pupil/temp/llama/1/consolidated.00.pth'

net = torch.load(pthfile, map_location=torch.device('cpu'))

writer = SummaryWriter("torchlogs/")
writer.add_graph(net)
writer.close()