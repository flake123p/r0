import torch

pthfile = '/home/pupil/temp/llama/1/consolidated.00.pth'

net = torch.load(pthfile, map_location=torch.device('cpu'))

for k, v in net.named_parameters():
    print(k, v)
# for key,value in net["model"].items():
#     print(key, value.size(), sep = "    ")

# for key,value in net["optimizer"].items():
#     print(key, type(value), sep = "    ")