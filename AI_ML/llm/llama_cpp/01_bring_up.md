
# Links
llama.cpp
https://github.com/ggerganov/llama.cpp

ggml:
https://github.com/ggerganov/ggml

llama-chat
https://github.com/randaller/llama-chat


pyllama (download llama 1)
https://github.com/juncongmoo/pyllama

Facebook llama
https://github.com/facebookresearch/llama


# 1. CMake (llama cpp GitHub step)
```
mkdir build
cd build
cmake ..
cmake --build . --config Release
```

# 2. 7B model & pyllama (pyllama GitHub step)
- [ ] Where can I download the weights of the 7B model? https://github.com/facebookresearch/llama/issues/149
- [ ] pyllama https://github.com/juncongmoo/pyllama
```
git clone

pip install pyllama -U

Download Model Files by: (Community Way, not Official Way!!)

python -m llama.download --model_size 7B
```

## 3. Find the 7B path for llama cpp

## 4. Prepare Data & Run (llama cpp GitHub step)

```
ls ./models
65B 30B 13B 7B tokenizer_checklist.chk tokenizer.model

# install Python dependencies
python3 -m pip install -r requirements.txt

# convert the 7B model to ggml FP16 format
python3 convert.py models/7B/

# quantize the model to 4-bits (using q4_0 method)
./quantize ./models/7B/ggml-model-f16.bin ./models/7B/ggml-model-q4_0.bin q4_0

# run the inference
./main -m ./models/7B/ggml-model-q4_0.bin -n 128
```

# Code Treace
## main() location
/examples/main/main.cpp