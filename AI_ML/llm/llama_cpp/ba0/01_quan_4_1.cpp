//
// How to interpret complex C/C++ declarations
//      https://www.codeproject.com/Articles/7042/How-to-interpret-complex-C-C-declarations
//

#include "all.hpp"

#ifdef __ARM_NEON
    // we use the built-in 16-bit float type
    typedef __fp16 ggml_fp16_t;
#else
    typedef uint16_t ggml_fp16_t;
#endif

#define QK4_0 32
typedef struct {
    ggml_fp16_t d;          // delta
    uint8_t qs[QK4_0 / 2];  // nibbles / quants
} block_q4_0;
static_assert(sizeof(block_q4_0) == sizeof(ggml_fp16_t) + QK4_0 / 2, "wrong q4_0 block size/padding");

#define QK4_1 32
typedef struct {
    ggml_fp16_t d;          // delta
    ggml_fp16_t m;          // min
    uint8_t qs[QK4_1 / 2];  // nibbles / quants
} block_q4_1;
static_assert(sizeof(block_q4_1) == 2 * sizeof(ggml_fp16_t) + QK4_1 / 2, "wrong q4_1 block size/padding");

#define QK5_0 32
typedef struct {
    ggml_fp16_t d;         // delta
    uint8_t qh[4];         // 5-th bit of quants
    uint8_t qs[QK5_0 / 2]; // nibbles / quants
} block_q5_0;
static_assert(sizeof(block_q5_0) == sizeof(ggml_fp16_t) + sizeof(uint32_t) + QK5_0 / 2, "wrong q5_0 block size/padding");

#define QK5_1 32
typedef struct {
    ggml_fp16_t d;         // delta
    ggml_fp16_t m;         // min
    uint8_t qh[4];         // 5-th bit of quants
    uint8_t qs[QK5_1 / 2]; // nibbles / quants
} block_q5_1;
static_assert(sizeof(block_q5_1) == 2 * sizeof(ggml_fp16_t) + sizeof(uint32_t) + QK5_1 / 2, "wrong q5_1 block size/padding");

#define QK8_0 32
typedef struct {
    ggml_fp16_t d;         // delta
    int8_t  qs[QK8_0];     // quants
} block_q8_0;
static_assert(sizeof(block_q8_0) == sizeof(ggml_fp16_t) + QK8_0, "wrong q8_0 block size/padding");

#define QK8_1 32
typedef struct {
    float d;               // delta
    float s;               // d * sum(qs[i])
    int8_t  qs[QK8_1];     // quants
} block_q8_1;
static_assert(sizeof(block_q8_1) == 2*sizeof(float) + QK8_1, "wrong q8_1 block size/padding");

#define GGML_FP32_TO_FP16(x) GGML_COMPUTE_FP32_TO_FP16(x)

#define GGML_COMPUTE_FP32_TO_FP16(x) ggml_compute_fp32_to_fp16(x)

static inline uint32_t fp32_to_bits(float f) {
    union {
        float as_value;
        uint32_t as_bits;
    } fp32;
    fp32.as_value = f;
    return fp32.as_bits;
}

static inline float fp32_from_bits(uint32_t w) {
    union {
        uint32_t as_bits;
        float as_value;
    } fp32;
    fp32.as_bits = w;
    return fp32.as_value;
}

static inline float ggml_compute_fp16_to_fp32(ggml_fp16_t h) {
    const uint32_t w = (uint32_t) h << 16;
    const uint32_t sign = w & UINT32_C(0x80000000);
    const uint32_t two_w = w + w;

    const uint32_t exp_offset = UINT32_C(0xE0) << 23;
#if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L) || defined(__GNUC__) && !defined(__STRICT_ANSI__)
    const float exp_scale = 0x1.0p-112f;
#else
    const float exp_scale = fp32_from_bits(UINT32_C(0x7800000));
#endif
    const float normalized_value = fp32_from_bits((two_w >> 4) + exp_offset) * exp_scale;

    const uint32_t magic_mask = UINT32_C(126) << 23;
    const float magic_bias = 0.5f;
    const float denormalized_value = fp32_from_bits((two_w >> 17) | magic_mask) - magic_bias;

    const uint32_t denormalized_cutoff = UINT32_C(1) << 27;
    const uint32_t result = sign |
        (two_w < denormalized_cutoff ? fp32_to_bits(denormalized_value) : fp32_to_bits(normalized_value));
    return fp32_from_bits(result);
}

static inline ggml_fp16_t ggml_compute_fp32_to_fp16(float f) {
#if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L) || defined(__GNUC__) && !defined(__STRICT_ANSI__)
    const float scale_to_inf = 0x1.0p+112f;
    const float scale_to_zero = 0x1.0p-110f;
#else
    const float scale_to_inf = fp32_from_bits(UINT32_C(0x77800000));
    const float scale_to_zero = fp32_from_bits(UINT32_C(0x08800000));
#endif
    float base = (fabsf(f) * scale_to_inf) * scale_to_zero;

    const uint32_t w = fp32_to_bits(f);
    const uint32_t shl1_w = w + w;
    const uint32_t sign = w & UINT32_C(0x80000000);
    uint32_t bias = shl1_w & UINT32_C(0xFF000000);
    if (bias < UINT32_C(0x71000000)) {
        bias = UINT32_C(0x71000000);
    }

    base = fp32_from_bits((bias >> 1) + UINT32_C(0x07800000)) + base;
    const uint32_t bits = fp32_to_bits(base);
    const uint32_t exp_bits = (bits >> 13) & UINT32_C(0x00007C00);
    const uint32_t mantissa_bits = bits & UINT32_C(0x00000FFF);
    const uint32_t nonsign = exp_bits + mantissa_bits;
    return (sign >> 16) | (shl1_w > UINT32_C(0xFF000000) ? UINT16_C(0x7E00) : nonsign);
}

#undef MIN
#undef MAX
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

// Flake: ...
#undef restrict
#define restrict 
/*
    tok_embeddings.weight -     4096 x 32000

    b=0, nb=4096000, k=131072000

    k=131072000, qk=32, nb=4096000
*/
static void quantize_row_q4_0_reference(const float * restrict x, block_q4_0 * restrict y, int k) {
    static const int debugDump = 0;
    static const int qk = QK4_0;

    assert(k % qk == 0);

    const int nb = k / qk;

    //printf("\n k=%d, qk=%d, nb=%d \n", k, qk, nb);

    for (int i = 0; i < nb; i++) {
        float amax = 0.0f; // absolute max
        float max  = 0.0f;

        for (int j = 0; j < qk; j++) {
            const float v = x[i*qk + j];
            if (amax < fabsf(v)) {
                amax = fabsf(v);
                max  = v;
            }
        }

        const float d  = max / -8;
        const float id = d ? 1.0f/d : 0.0f;

        y[i].d = GGML_FP32_TO_FP16(d);

        // Debug
        if (debugDump)
        {
            float fp32from16 = ggml_compute_fp16_to_fp32(y[i].d);
            printf("max = %f, amax = %f, d = %f, id = %f, y[i].d = %d(%f)\n", max, amax, d, id, y[i].d, fp32from16);
        }

        // file qs[] in y[i]
        for (int j = 0; j < qk/2; ++j) {
            const float x0 = x[i*qk + 0    + j]*id;
            const float x1 = x[i*qk + qk/2 + j]*id;

            const uint8_t xi0 = MIN(15, (int8_t)(x0 + 8.5f)); // sat to 0xF
            const uint8_t xi1 = MIN(15, (int8_t)(x1 + 8.5f)); // sat to 0xF

            if (debugDump) {
                printf("[%2d/%2d] 1st:%2d, 2nd:%2d (%10.6f / %10.6f) (%2d / %2d)\n", i, j, i*qk+0+j, i*qk+qk/2+j, x0, x1, xi0, xi1);
            }

            y[i].qs[j]  = xi0;
            y[i].qs[j] |= xi1 << 4;
        }
    }
}

static void quantize_row_q4_1_reference(const float * restrict x, block_q4_1 * restrict y, int k) {
    const int qk = QK4_1;

    assert(k % qk == 0);

    const int nb = k / qk;

    for (int i = 0; i < nb; i++) {
        float min = FLT_MAX;
        float max = -FLT_MAX;

        for (int j = 0; j < qk; j++) {
            const float v = x[i*qk + j];

            if (v < min) min = v;
            if (v > max) max = v;
        }

        const float d  = (max - min) / ((1 << 4) - 1);
        const float id = d ? 1.0f/d : 0.0f;

        y[i].d = GGML_FP32_TO_FP16(d);
        y[i].m = GGML_FP32_TO_FP16(min);

        for (int j = 0; j < qk/2; ++j) {
            const float x0 = (x[i*qk + 0    + j] - min)*id;
            const float x1 = (x[i*qk + qk/2 + j] - min)*id;

            const uint8_t xi0 = MIN(15, (int8_t)(x0 + 0.5f));
            const uint8_t xi1 = MIN(15, (int8_t)(x1 + 0.5f));

            y[i].qs[j]  = xi0;
            y[i].qs[j] |= xi1 << 4;
        }
    }
}

class QuanTester {
public:
    int number;
    float *x = NULL;
    block_q4_0 *y = NULL;
    block_q4_1 *y1 = NULL;
    ~QuanTester() {
        if (x) free(x);
        if (y) free(y);
        if (y1) free(y1);
    }

    void init(int float_number) {
        if (x) free(x);
        if (y) free(y);
        if (y1) free(y1);

        x = (float *)malloc(float_number * sizeof(float));
        y = (block_q4_0 *)malloc((float_number / QK4_0) * sizeof(block_q4_0));
        y1 = (block_q4_1 *)malloc((float_number / QK4_1) * sizeof(block_q4_1));

        this->number = float_number;
    }

    void setDummyToX() {
        float start = -3.2;
        for (int i = 0; i < this->number; i++) {
            x[i] = start;
            start = start + 0.1;
        }
    }

    void dumpX() {
        for (int i = 0; i < this->number; i++) {
            printf("%10.6f, ", x[i]);
            if (i % 8 == 7) {
                printf("\n");
            }
            else if (i == this->number -1) {
                printf("\n");
            }
        }
    }

    void dumpY_Q4_Old() {
        int yIdx = 0;
        int insideIdx;
        int is2ndNibble;
        uint8_t currU8;
        uint8_t currNibble;
        printf("yIdx = %d, delta = %d\n", yIdx, y[yIdx].d);
        for (int i = 0; i < this->number; i++) {
            if (yIdx != i / QK4_0) {
                yIdx = i / QK4_0;
                printf("yIdx = %d, delta = %d\n", yIdx, y[yIdx].d);
            }
            insideIdx = i % QK4_0;
            is2ndNibble = i % 2;

            currU8 = y[yIdx].qs[insideIdx];

            if (is2ndNibble) {
                currNibble = (currU8 >> 4) & 0x0F;
            } else {
                currNibble = currU8 & 0x0F;
            }

            printf("%8x, ", currNibble);
            if (i % 8 == 7) {
                printf("\n");
            }
            else if (i == this->number -1) {
                printf("\n");
            }
        }
    }

    float dequan_q4(uint8_t x, float d)
    {
        float temp = (float)x;
        temp = temp - 8.0f;
        return d*temp;
    }

    float dequan_q41(uint8_t x, float d, float m)
    {
        // x = (x & 0x8) ? (x | 0xF0) : x;
        // int8_t *ps8 = (int8_t *)&x;
        //float temp = (float)*ps8;
        float temp = (float)x;
        return d*temp + m;
    }

    void dumpY_Q4(int dequan = 0)
    {
        int yNum = this->number / QK4_0;
        uint8_t currU8;

        for (int i = 0; i < yNum; i++) {
            float fp32from16 = ggml_compute_fp16_to_fp32(y[i].d);
            printf("yIdx = %d, delta = %d (%f)\n", i, y[i].d, fp32from16);
            // 1st 16 & 2nd 16
            for (int half16 = 0; half16 < 2; half16++) {
                for (int j = 0; j < 16; j ++) {
                    currU8 = y[i].qs[j];
                    if (half16 == 0) {
                        currU8 = currU8 & 0x0F;
                    } else {
                        currU8 = (currU8 >> 4) & 0x0F;
                    }

                    if (dequan) {
                        printf("%10.6f, ", dequan_q4(currU8, fp32from16));
                    } else {
                        printf("%8x, ", currU8);
                    }

                    if (j % 8 == 7) {
                        printf("\n");
                    }
                }
            }
        }
    }

    void dumpY_Q41(int dequan = 0)
    {
        int yNum = this->number / QK4_1;
        uint8_t currU8;

        for (int i = 0; i < yNum; i++) {
            float dF32 = ggml_compute_fp16_to_fp32(y1[i].d);
            float mF32 = ggml_compute_fp16_to_fp32(y1[i].m);
            printf("yIdx = %d, delta = %d (%f), min = %d (%f)\n", i, y1[i].d, dF32, y1[i].m, mF32);
            // 1st 16 & 2nd 16
            for (int half16 = 0; half16 < 2; half16++) {
                for (int j = 0; j < 16; j ++) {
                    currU8 = y1[i].qs[j];
                    if (half16 == 0) {
                        currU8 = currU8 & 0x0F;
                    } else {
                        currU8 = (currU8 >> 4) & 0x0F;
                    }

                    if (dequan) {
                        printf("%10.6f, ", dequan_q41(currU8, dF32, mF32));
                    } else {
                        printf("%8x, ", currU8);
                    }

                    if (j % 8 == 7) {
                        printf("\n");
                    }
                }
            }
        }
    }
};

int main() 
{
    QuanTester qt;
    qt.init(64);
    qt.setDummyToX();
    qt.dumpX();

    quantize_row_q4_1_reference(qt.x, qt.y1, 64);

    qt.dumpY_Q41();
    qt.dumpY_Q41(1);

    return 0;
}