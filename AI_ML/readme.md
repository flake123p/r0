# Tensorflow To Ndarray
https://blog.finxter.com/how-to-convert-a-tensor-to-a-numpy-array-in-tensorflow/

## Tensorflow 2:
a = t.numpy()
print(a)
print(type(a))

## Tensorflow 1 compat:
a = t.eval(session=tf.compat.v1.Session())
print(a)
print(type(a))

## Tensorflow 1:
a = t.eval(session=tf.Session()) # or t.eval(session=Sess)
print(a)
print(type(a))


# 1
- [ ] PTQ vs QAT

# 2
model file formats:

TF: .ckpt .hdf5 .pb

TF Model Formats Page: https://www.tensorflow.org/hub/model_formats




...

PTQ
QAT
GPTQ
QLORA
Adapter fp16


- [ ] 00. 深度學習的緣起：單一神經元分類器 https://ithelp.ithome.com.tw/articles/10201407?sc=iThelpR
- [ ] 全連接神經網路 https://www.tinytsunami.info/fully-connected-neural-network/

![](https://pic3.zhimg.com/80/v2-a1f1d9f699697a8e05979abf749fbeae_720w.webp)

- [ ] Multi-version CUDA https://zhuanlan.zhihu.com/p/91334380
- [ ] LD_PRELOAD & Hijack https://zhuanlan.zhihu.com/p/642506918

(Good)
- [ ] Grid Block Thread http://www2.kimicat.com/cuda%E7%B0%A1%E4%BB%8B

![](http://www2.kimicat.com/arch2-large.PNG)