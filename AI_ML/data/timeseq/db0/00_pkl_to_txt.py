import sys
import torch

def pickle_to(var, file : str):
    import pickle
    with open(file, 'wb') as my_file:
        pickle.dump(var, my_file)

def pickle_from(file : str):
    import pickle
    try:
        with open(file, 'rb') as my_file:
            return pickle.load(my_file)
    except IOError as exc:    # Python 2. For Python 3 use OSError
        tb = sys.exc_info()[-1]
        lineno = tb.tb_lineno
        filename = tb.tb_frame.f_code.co_filename
        print('{} \nException at {} line {}.'.format(exc.strerror, filename, lineno))
        sys.exit(exc.errno)
        return None


with open('03_train_input.txt', 'w') as f:
    input = pickle_from('01_train_input.pkl')
    label = pickle_from('02_train_label.pkl')
    print('len check :', len(input), len(label))
    if len(input) != len(label):
        print('lengths not match ...')
        sys.exit(0)
    for i in range(len(input)):
        # print(label[i])
        la = int(label[i])
        f.write(f'{la:3d}' + ' | ')
        for j in input[i]:
            f.write(f'{float(j):.2f}' + ', ')
        f.write('\n')