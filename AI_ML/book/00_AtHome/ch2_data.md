# Open data

## Taiwan
https://data.gov.tw/

## Image-Net
https://image-net.org/index.php

## Kaggle
https://www.kaggle.com/

## Google Cloud
https://cloud.google.com/?_gl=1*6arl5y*_up*MQ..&gclid=CjwKCAjwwr6wBhBcEiwAfMEQs2OpGPCr89J_0eqAO01xxp_ekJE2bveDIJA4aZ4BtQMOfobj13MaDhoCHRAQAvD_BwE&gclsrc=aw.ds

## UCI Machine Learning Repository
https://archive.ics.uci.edu/

## Azure Open dataset
https://azure.microsoft.com/en-us/products/open-datasets/


# Label tools

## LabelImg
https://github.com/HumanSignal/labelImg

## RectLabel
https://rectlabel.com/
