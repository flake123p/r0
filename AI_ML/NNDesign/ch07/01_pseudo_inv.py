import numpy as np

p1 = np.array([[1, -1, -1]])
t1 = -1.

p2 = np.array([[1, 1, -1]])
t2 = 1.

# p1 = p1 / np.linalg.norm(p1)
# p2 = p2 / np.linalg.norm(p2)
Pt = np.concatenate((p1, p2), axis=0)
Pinv = np.linalg.pinv(np.transpose(Pt))
print("Pinv = \n", Pinv)

T = np.array([[t1, t2]])
print("T = \n", T)

W = np.matmul(T, Pinv)
print("W = \n", W)

print("Wp1 = ", np.matmul(W, np.transpose(p1)))
print("Wp2 = ", np.matmul(W, np.transpose(p2)))