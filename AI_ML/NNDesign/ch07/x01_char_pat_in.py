import numpy as np
from x00_char_pat import *

p0occlude50 = np.array([
    0, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    ])

p0occlude67 = np.array([
    0, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    ])

p0noisy = np.array([
    0, 0, 1, 1, 0,
    1, 0, 0, 1, 0,
    1, 0, 0, 0, 1,
    0, 0, 0, 0, 0,
    1, 0, 1, 0, 1,
    0, 1, 0, 1, 0,
    ])

p1occlude50 = np.array([
    0, 1, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    ])

p1occlude67 = np.array([
    0, 1, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    ])

p1noisy = np.array([
    0, 1, 1, 0, 0,
    0, 0, 1, 1, 0,
    1, 0, 1, 0, 0,
    0, 0, 0, 0, 1,
    0, 0, 1, 0, 0,
    1, 1, 1, 1, 0,
    ])

p2occlude50 = np.array([
    1, 1, 1, 0, 0,
    0, 0, 0, 1, 0,
    0, 0, 0, 1, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    ])

p2occlude67 = np.array([
    1, 1, 1, 0, 0,
    0, 0, 0, 1, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    ])

p2noisy = np.array([
    0, 1, 1, 0, 0,
    0, 1, 0, 1, 0,
    0, 0, 0, 0, 0,
    0, 1, 1, 0, 1,
    0, 0, 0, 0, 0,
    1, 1, 1, 0, 1,
    ])

if __name__ == '__main__':
    print('p0occlude50')
    print_char_pat(p0occlude50)
    print('p0occlude67')
    print_char_pat(p0occlude67)
    print('p0noisy')
    print_char_pat(p0noisy)
    
    print('p1occlude50')
    print_char_pat(p1occlude50)
    print('p1occlude67')
    print_char_pat(p1occlude67)
    print('p1noisy')
    print_char_pat(p1noisy)
    
    print('p2occlude50')
    print_char_pat(p2occlude50)
    print('p2occlude67')
    print_char_pat(p2occlude67)
    print('p2noisy')
    print_char_pat(p2noisy)