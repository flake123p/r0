import numpy as np

def print_char_pat(ary):
    ctr = 0
    for i in ary:
        ctr += 1
        if ctr == 5:
            ctr = 0
            print(i, end ="\n")
        else:
            print(i, end =" ")

p0 = np.array([
    0, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    0, 1, 1, 1, 0,
    ])

p1 = np.array([
    0, 1, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    ])

p2 = np.array([
    1, 1, 1, 0, 0,
    0, 0, 0, 1, 0,
    0, 0, 0, 1, 0,
    0, 1, 1, 0, 0,
    0, 1, 0, 0, 0,
    0, 1, 1, 1, 1,
    ])

if __name__ == '__main__':
    print('p0')
    print_char_pat(p0)
    print('p1')
    print_char_pat(p1)
    print('p2')
    print_char_pat(p2)