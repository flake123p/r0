import numpy as np
from x00_char_pat import *
from x01_char_pat_in import *

def hamming_dist_binary(a, b):
    diff = np.bitwise_xor(a, b)
    dist = np.count_nonzero(diff)
    return dist, diff 

def get_all_dist(a):
    d0, _ = hamming_dist_binary(p0, a)
    d1, _ = hamming_dist_binary(p1, a)
    d2, _ = hamming_dist_binary(p2, a)
    return np.array([d0, d1, d2])

if __name__ == '__main__':
    dist, diff = hamming_dist_binary(p0, p0occlude50)
    print('diff')
    print_char_pat(diff)
    print(dist)
    
    print('p0occlude50 score = ', get_all_dist(p0occlude50))
    print('p0occlude67 score = ', get_all_dist(p0occlude67))
    print('p0noisy     score = ', get_all_dist(p0noisy))
    print('p1occlude50 score = ', get_all_dist(p1occlude50))
    print('p1occlude67 score = ', get_all_dist(p1occlude67))
    print('p1noisy     score = ', get_all_dist(p1noisy))
    print('p2occlude50 score = ', get_all_dist(p2occlude50))
    print('p2occlude67 score = ', get_all_dist(p2occlude67))
    print('p2noisy     score = ', get_all_dist(p2noisy))