
def diff_abs(listA, listB):
    ret = []
    maxIndexB = len(listB) - 1
    for indexA in range(len(listA)):
        indexB = indexA
        if indexB > maxIndexB:
            indexB = maxIndexB
        ret.append(abs(listA[indexA] - listB[indexB]))
    return ret


a = [1,2,3]
b = [0,9]
c = diff_abs(a,b)

print('a =', a)
print('b =', b)
print('c =', c)

d = [2]
e = diff_abs(a,d)
print('e =', e)

f = diff_abs(a, [3,3,3])
print('f =', f)