import numpy as np

def softmax(x):
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum()

a = [1,2,3]
print(a)
b = softmax(a)
print(b)


a = [-1,2,3]
print(a)
b = softmax(a)
print(b)

a = [1,-2,3]
print(a)
b = softmax(a)
print(b)

a = [1,2,-3]
print(a)
b = softmax(a)
print(b)

a = [-1,-2,-3]
print(a)
b = softmax(a)
print(b)