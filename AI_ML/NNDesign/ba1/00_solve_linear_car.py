
# https://numpy.org/doc/stable/reference/generated/numpy.linalg.solve.html
#

import numpy as np

a = np.array([[0.36, -0.06], [1, 0]])
b = np.array([0, 1])
xy = np.linalg.solve(a, b)

print(xy)
print('ratio =', xy[1]/xy[0])



