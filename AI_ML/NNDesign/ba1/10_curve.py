#
# center = 1, 1
#

import random
import math
import matplotlib.pyplot as plt

def black_box(x):
   t1 = x
   t2 = t1*x
   t3 = t2*x
   t4 = t3*x 
   return t4 - (t3*2.0/3.0) - (t2*2.0) + (t1*2.0) + 4


x_axis = [i/100.0 for i in range(-200,199,1)]
y_axis = [black_box(x) for x in x_axis]
plt.grid(True)
plt.plot(x_axis, y_axis)
plt.show()
