#
# center = 1, 1
#

import random
import math
import matplotlib.pyplot as plt

def black_box(x):
   t1 = x
   t2 = t1*x
   t3 = t2*x
   t4 = t3*x 
   return t4 - (t3*2.0/3.0) - (t2*2.0) + (t1*2.0) + 4

def slope(x):
   y = black_box(x)
   rx = x + 0.01
   ry = black_box(rx)
   lx = x - 0.01
   ly = black_box(lx)
   return (y-ly)/0.01, (ry-y)/0.01


x_axis = [i/100.0 for i in range(-200,199,1)]
y_axis = [black_box(x) for x in x_axis]
plt.grid(True)
plt.plot(x_axis, y_axis)
# plt.show()

print('slope(1.5)  =', slope(1.5))
plt.scatter(1.5, black_box(1.5), color="red")

print('slope(-1.5) =', slope(-1.5))
plt.scatter(-1.5, black_box(-1.5), color="green")

plt.show()