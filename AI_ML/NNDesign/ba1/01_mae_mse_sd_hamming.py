#
# https://numpy.org/doc/stable/reference/generated/numpy.linalg.solve.html
#

import numpy as np

a = np.array([[5, 6, 8, 9]])

sum = np.sum(a)

mean = np.mean(a)

avg = np.average(a) # weighted

sd = np.std(a)

print('sum =', sum)
print('mean =', mean)
print('avg =', avg)
print('sd =', sd)

diff = a - mean
print('diff =', diff)
print('diff pow2 =', np.power(diff, 2))
print('diff pow2 mean =', np.mean(np.power(diff, 2)))
print('diff pow2 mean sqrt =', np.sqrt(np.mean(np.power(diff, 2))), " (SD)")

print('mae =', np.mean(np.abs(diff)))
print('mse =', np.mean(np.power(diff, 2)))
print('Hamming distance   =', np.count_nonzero(diff))
print('Euclidean distance =', np.sqrt(np.sum(np.power(diff, 2))))