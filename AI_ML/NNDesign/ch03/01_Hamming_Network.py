import pandas as pd
import matplotlib.pyplot as plt

def dot(K, L):
   if len(K) != len(L):
      return 0

   return sum(i[0] * i[1] for i in zip(K, L))

class HammingNetwork(object):
    def __init__(self):
        self.w11 = [1,-1,-1]
        self.w12 = [1,1,-1]
        self.b1 = [3,3]
        self.w21 = [1, -0.5]
        self.w22 = [-0.5, 1]
        self.a1 = [0,0]
        self.ctr = 0
        self.out = [0,0]

    def processStart(self, input):
        self.a1[0] = dot(input, self.w11) + self.b1[0]
        self.a1[1] = dot(input, self.w12) + self.b1[1]
        self.out[0] = self.a1[0]
        self.out[1] = self.a1[1]
        self.ctr = 1

    def processContinue(self):
        temp0 = max(0, dot(self.out, self.w21))
        temp1 = max(0, dot(self.out, self.w22))
        self.out[0] = temp0
        self.out[1] = temp1
        self.ctr += 1

  

hammingNetwork = HammingNetwork()

apple = [1, 1, -1]
orange = [1, -1, -1]
unknown = [-1, -1, -1]

hammingNetwork.processStart(unknown)
print(hammingNetwork.out)

hammingNetwork.processContinue()
print(hammingNetwork.out)

hammingNetwork.processContinue()
print(hammingNetwork.out)

hammingNetwork.processContinue()
print(hammingNetwork.out)
