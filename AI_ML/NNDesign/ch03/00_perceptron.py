import pandas as pd
import matplotlib.pyplot as plt

# Perceptron
class MyNetwork3In(object):
    def __init__(self):
        self.w2 = 0.0
        self.w1 = 1.0
        self.w0 = 0.0
        self.b = 0.0

    def process(self, input):
        ret = input[2]*self.w2 + input[1]*self.w1 + input[0]*self.w0 + self.b
        return 1 if ret > 0 else -1 # hardlims

myNet = MyNetwork3In()

# shape, texture, weight
orange = [1, -1, -1]
apple = [1, 1, -1]
unknown = [1, 1, 1]

orange_result = myNet.process(orange)
apple_result = myNet.process(apple)
unknown_result = myNet.process(unknown)

print('orange_result  =', orange_result)
print('apple_result   =', apple_result)
print('unknown_result =', unknown_result)