import pandas as pd
import matplotlib.pyplot as plt

def satlins(n):
  if n < -1:
    return -1
  elif n > 1:
    return 1
  else:
    return n

def dot(K, L):
   if len(K) != len(L):
      return 0

   return sum(i[0] * i[1] for i in zip(K, L))

class HopfieldNetwork(object):
    def __init__(self):
        self.w11 = [0.2, 0, 0]
        self.w12 = [0, 1.2, 0]
        self.w13 = [0, 0, 0.2]
        self.b1 = [0.9, 0, -0.9]
        self.a1 = [0,0,0]
        self.ctr = 0
        self.out = [0,0,0]

    def processStart(self, input):
        self.a1[0] = input[0]
        self.a1[1] = input[1]
        self.a1[2] = input[2]
        self.out[0] = self.a1[0]
        self.out[1] = self.a1[1]
        self.out[2] = self.a1[2]
        self.ctr = 1

    def processContinue(self):
        self.a1[0] = satlins(dot(self.a1, self.w11) + self.b1[0])
        self.a1[1] = satlins(dot(self.a1, self.w12) + self.b1[1])
        self.a1[2] = satlins(dot(self.a1, self.w13) + self.b1[2])
        self.out[0] = self.a1[0]
        self.out[1] = self.a1[1]
        self.out[2] = self.a1[2]
        self.ctr += 1

  

hopfieldNetwork = HopfieldNetwork()

apple = [1, 1, -1]
orange = [1, -1, -1]
unknown = [-1, -1, -1]

hopfieldNetwork.processStart(unknown)
print(hopfieldNetwork.out)

hopfieldNetwork.processContinue()
print(hopfieldNetwork.out)

hopfieldNetwork.processContinue()
print(hopfieldNetwork.out)

hopfieldNetwork.processContinue()
print(hopfieldNetwork.out)
