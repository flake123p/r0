def relu(i):
  if i < 0:
    return 0
  else:
    return i

input = [-3, -2, 0, 1, 2, 3, 5, 7]

for i in input:
  print(relu(i))
  
print('max():')
for i in input:
  print(max(i, 0))