import numpy as np
import matplotlib.pyplot as plt
import tensorflow.compat.v1 as tf

tf.disable_eager_execution()

A = tf.Variable(0.1)
B = tf.Variable(0.1)
y = (A*0.1) / ( (A*0.1) + (B*0.9) )

cost_function = tf.reduce_mean(tf.abs(0.4 - y))
optimizer = tf.train.GradientDescentOptimizer(0.1)
train = optimizer.minimize(cost_function)

model = tf.initialize_all_variables()

with tf.Session() as session:
    session.run(model)
    for step in range(0,1000):
        session.run(train)
        if step % 100 == 0:
            print(A.eval()/B.eval())

