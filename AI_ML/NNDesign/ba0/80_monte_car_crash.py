import random
import math
import matplotlib.pyplot as plt


max = 10000
y = 1
bestDiff = 99999
bestX = 0.1
bestY = 0.1
for i in range(1,max):
    x = random.uniform(0, 1)
    y = random.uniform(0, 1)
    ans = (0.1 * y) / ((0.9 * x) + (0.1 * y))
    diff = abs(0.4 - ans)
    if (diff < bestDiff):
       bestDiff = diff
       #print('bestDiff =', bestDiff)
       bestX = x
       bestY = y


print('bestRate =', bestY/bestX)
print('X =', bestX)
print('Y =', bestY)


