#
# center = 1, 1
#

import random
import math
import matplotlib.pyplot as plt

inside = 0
max = 100000
x_axis = []
y_axis = []
x_axis_inside = []
y_axis_inside = []

for i in range(max):
    x = random.uniform(0, 2)
    y = random.uniform(0, 2)
    dist = (x-1)**2 + (y-1)**2
    dist = math.sqrt(dist)
    if (dist <= 1):
        inside+=1

    if i < 1000:
      if (dist <= 1):
        x_axis_inside.append(x)
        y_axis_inside.append(y)
      else :
        x_axis.append(x)
        y_axis.append(y)


pi = inside * 4 / max # inside : total = PI : 4
print(pi)

plt.grid(True)
plt.scatter(x_axis_inside, y_axis_inside, color="red") # inside
plt.scatter(x_axis, y_axis, color="g")                 # outside
plt.show()


