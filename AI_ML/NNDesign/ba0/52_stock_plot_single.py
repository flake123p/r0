import pandas as pd
import matplotlib.pyplot as plt

def diff_abs(listA, listB):
    ret = []
    maxIndexB = len(listB) - 1
    for indexA in range(len(listA)):
        indexB = indexA
        if indexB > maxIndexB:
            indexB = maxIndexB
        ret.append(abs(listA[indexA] - listB[indexB]))
    return ret

#
# Main
#
dataframe = pd.read_csv('stocks.csv')

diction = dataframe.to_dict()

x_axis = []
y_axis = []
if True:
    # MSFT
    for i in range(len(diction["symbol"])):
        if diction["symbol"][i] == 'MSFT':
            y_axis.append(diction["price"][i])
            x_axis.append(i)
else:
    # AAPL
    ctr = 0
    for i in range(len(diction["symbol"])):
        if diction["symbol"][i] == 'AAPL':
            y_axis.append(diction["price"][i])
            x_axis.append(ctr)
            ctr += 1
        
plt.grid(True)
plt.plot(x_axis, y_axis, color='r')
#plt.show()

######################################################################
######################################################################
class SingleInputNeuron(object):
    def __init__(self):
        self.w = 1.0
        self.b = 0.0
    def process(self, input):
        return input*self.w + self.b

single00 = SingleInputNeuron()

new_axis = [single00.process(i) for i in x_axis]

plt.plot(x_axis, new_axis, color='g')

######################################################################
######################################################################
import random

max = 100000
best_diff = float("inf")
best_w = 0.0
best_b = 0.0
for i in range(max):
    single00.w = random.uniform(0, 10)
    single00.b = random.uniform(0, 60)
    new_data = [single00.process(i) for i in x_axis]
    total_diff_abs = sum(diff_abs(new_data, y_axis))
    #
    # update best
    #
    if total_diff_abs < best_diff:
        best_diff = total_diff_abs
        best_w = single00.w
        best_b = single00.b

single00.w = best_w
single00.b = best_b
new_axis = [single00.process(i) for i in x_axis]
plt.plot(x_axis, new_axis, color='b')

print('latest  = ' + str(y_axis[-1]))
print('predict = ' + str(single00.process(len(y_axis))))
print('best_w = ' + str(best_w))
print('best_b = ' + str(best_b))

avg_diff = best_diff / len(x_axis)
print('avg_diff = ' + str(avg_diff))

plt.show()