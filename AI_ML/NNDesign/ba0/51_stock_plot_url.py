import pandas as pd
import matplotlib.pyplot as plt

def diff_abs(listA, listB):
    ret = []
    maxIndexB = len(listB) - 1
    for indexA in range(len(listA)):
        indexB = indexA
        if indexB > maxIndexB:
            indexB = maxIndexB
        ret.append(abs(listA[indexA] - listB[indexB]))
    return ret

#
# Main
#

#
# https://github.com/vega/datalib/blob/master/test/data/stocks.csv
#
url = 'https://raw.githubusercontent.com/vega/datalib/master/test/data/stocks.csv'

dataframe = pd.read_csv(url)

diction = dataframe.to_dict()

x_axis = []
y_axis = []
for i in range(len(diction["symbol"])):
    if diction["symbol"][i] == 'MSFT':
        y_axis.append(diction["price"][i])
        x_axis.append(i)

        
plt.grid(True)
plt.plot(x_axis, y_axis, color='r')
#plt.show()

avg = sum(y_axis)/len(y_axis)
print('avg = ' + str(avg))

diff_abs_list = diff_abs(y_axis, [avg])
avg_diff = sum(diff_abs_list)/len(diff_abs_list)
print('avg_diff = ' + str(avg_diff))

avg_axis = [avg for i in y_axis]
plt.plot(x_axis, avg_axis, color='g')
plt.show()