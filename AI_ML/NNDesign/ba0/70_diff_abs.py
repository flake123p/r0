#
# MAE, MSE, MAPE, WMAE, WMAPE, MAD, SD
#
def diff_abs(listA, listB, do_abs = True):
    ret = []
    if len(listB) == 1:
        for indexA in range(len(listA)):
            diff = listA[indexA] - listB[0]
            diff = abs(diff) if do_abs else diff
            ret.append(diff)
    elif len(listA) == len(listB):
        for indexA in range(len(listA)):
            diff = listA[indexA] - listB[indexA]
            diff = abs(diff) if do_abs else diff
            ret.append(diff)
    return ret

def MAE(listA, listB):
    if len(listA) == 0:
        return None
    return sum(diff_abs(listA, listB)) / len(listA)

def MSE(listA, listB):
    if len(listA) == 0:
        return None
    temp = diff_abs(listA, listB)
    temp = [i*i for i in temp]
    return sum(temp) / len(temp)

def MLE(listA, listB):
    if len(listA) == 0:
        return None
    import numpy
    temp = diff_abs(listA, listB)
    temp = [numpy.log(i) if i != 0 else 0 for i in temp] # nature log
    return sum(temp) / len(temp)

def HAMMING_DIST(listA, listB):
    ret = 0
    results = diff_abs(listA, listB)
    for result in results:
        if result != 0:
            ret = ret + 1
    return ret

def WMAE_Basic(listA, listB):
    if len(listA) == 0:
        return None
    ret = []
    diff = diff_abs(listA, listB)
    for idx in range(len(diff)):
        weight = (idx+1) / len(diff)
        ret.append(diff[idx] * weight)
    return sum(ret) / len(ret)

def WMAE_Pow2(listA, listB):
    if len(listA) == 0:
        return None
    ret = []
    diff = diff_abs(listA, listB)
    for idx in range(len(diff)):
        weight = (idx+1) / len(diff)
        ret.append(diff[idx] * weight * weight)
    return sum(ret) / len(ret)

if __name__ == '__main__':
    print('hello main')
    a = [1,2,3]
    b = [0,9]
    c = diff_abs(a,b)

    print('a =', a)
    print('b =', b)
    print('c =', c)

    d = [2]
    e = diff_abs(a,d)
    print('e =', e)

    f = diff_abs(a, [3,3,3])
    print('f =', f)

    mae = MAE(a, [3,3,3])
    print('mae =', mae)
    mse = MSE(a, [3,3,3])
    print('mse =', mse)
    ham = HAMMING_DIST(a, [3,3,3])
    print('ham =', ham)


    origin = [1,2,3]
    predict1 = [2,2,2]
    predict2 = [3,3,3]
    print('origin   =', origin)
    print('predict1 =', predict1)
    print('predict2 =', predict2)
    print('DiffAbs1 =', diff_abs(origin, predict1))
    print('DiffAbs2 =', diff_abs(origin, predict2))


    mae1 = MAE(origin, predict1)
    mae2 = MAE(origin, predict2)
    wmae1 = WMAE_Basic(origin, predict1)
    wmae2 = WMAE_Basic(origin, predict2)

    print('mae1    =', mae1)
    print('mae2    =', mae2)
    print('wmae1   =', wmae1)
    print('wmae2   =', wmae2)

    wmae1 = WMAE_Pow2(origin, predict1)
    wmae2 = WMAE_Pow2(origin, predict2)
    print('wmae1x  =', wmae1)
    print('wmae2x  =', wmae2)

    import numpy
    diffAbs2 = diff_abs(origin, predict2)
    dist = numpy.linalg.norm(diffAbs2)
    print("dist using linalg  =", dist)
    print("dist using my func =", numpy.sqrt(MSE(origin, predict2)*len(origin)))