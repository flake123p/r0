#
# center = 1, 1
#

import random
import math

inside = 0
max = 100000

for i in range(max):
    x = random.uniform(0, 2)
    y = random.uniform(0, 2)
    dist = (x-1)**2 + (y-1)**2
    dist = math.sqrt(dist)
    if (dist <= 1):
        inside+=1

pi = inside * 4 / max # inside : total = PI : 4
print(pi)
