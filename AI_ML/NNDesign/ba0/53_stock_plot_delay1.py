import pandas as pd
import matplotlib.pyplot as plt

def diff_abs(listA, listB):
    ret = []
    maxIndexB = len(listB) - 1
    for indexA in range(len(listA)):
        indexB = indexA
        if indexB > maxIndexB:
            indexB = maxIndexB
        ret.append(abs(listA[indexA] - listB[indexB]))
    return ret

#
# Main
#

dataframe = pd.read_csv('stocks.csv')

diction = dataframe.to_dict()

x_axis = []
y_axis = []
if True:
    # MSFT
    for i in range(len(diction["symbol"])):
        if diction["symbol"][i] == 'MSFT':
            y_axis.append(diction["price"][i])
            x_axis.append(i)
else:
    # AAPL
    ctr = 0
    for i in range(len(diction["symbol"])):
        if diction["symbol"][i] == 'AAPL':
            y_axis.append(diction["price"][i])
            x_axis.append(ctr)
            ctr += 1
        
plt.grid(True)
plt.plot(x_axis, y_axis, color='r')
#plt.show()

######################################################################
######################################################################
class SingleInputNeuron(object):
    def __init__(self):
        self.w = 1.0
        self.b = 0.0
    def process(self, input):
        return input*self.w + self.b

single00 = SingleInputNeuron()

new_axis = [single00.process(i) for i in x_axis]

plt.plot(x_axis, new_axis, color='g')

######################################################################
######################################################################
import random

class DelayBlock(object):
  def __init__(self, a0):
    self.curr = a0

  def process(self, input):
    temp = self.curr
    self.curr = input
    return temp

class MyNetwork(object):
    def __init__(self):
        self.w0 = 1.0
        self.w1 = 1.0
        self.b = 0.0
        self.delay00 = DelayBlock(0)

    def process(self, input):
        return input*self.w0 + self.delay00.process(input)*self.w1 + self.b

max = 100000
best_diff = float("inf")
best_w0 = 0.0
best_w1 = 0.0
best_b = 0.0
myNetwork = MyNetwork()
for i in range(max):
    myNetwork.w0 = random.uniform(-10, 10)
    myNetwork.w1 = random.uniform(-10, 10)
    myNetwork.b = random.uniform(0, 40)
    myNetwork.delay00.curr = x_axis[0]
    new_data = [myNetwork.process(i) for i in x_axis]
    total_diff_abs = sum(diff_abs(new_data, y_axis))
    #
    # update best
    #
    if total_diff_abs < best_diff:
        best_diff = total_diff_abs
        best_w0 = myNetwork.w0
        best_w1 = myNetwork.w1
        best_b = myNetwork.b

myNetwork.w0 = best_w0
myNetwork.w1 = best_w1
myNetwork.b = best_b
new_axis = [myNetwork.process(i) for i in x_axis]
#
# Skip first 3 data
#
plt.plot(x_axis[3:], new_axis[3:], color='b')

print('latest  = ' + str(y_axis[-1]))
myNetwork.process(len(y_axis)-1) ## for delay block
print('predict = ' + str(myNetwork.process(len(y_axis))))
print('best_w0 = ' + str(best_w0))
print('best_w1 = ' + str(best_w1))
print('best_b = ' + str(best_b))

avg_diff = best_diff / len(x_axis)
print('avg_diff = ' + str(avg_diff))

plt.show()

# plt.plot(x_axis[3:], new_axis[3:], color='b')
# plt.show()