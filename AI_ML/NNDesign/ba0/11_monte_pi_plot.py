#
# center = 1, 1
#

import random
import math
import matplotlib.pyplot as plt

inside = 0
max = 100000
x_axis = []
y_axis = []

for i in range(max):
    x = random.uniform(0, 2)
    y = random.uniform(0, 2)
    dist = (x-1)**2 + (y-1)**2
    dist = math.sqrt(dist)
    if (dist <= 1):
        inside+=1

    if i % 1000 == 0:
      if (i == 0):
        pi = 3
      else:
        pi = inside * 4 / i # inside : total = PI : 4
      x_axis.append(i)
      y_axis.append(pi)

pi = inside * 4 / max # inside : total = PI : 4
print(pi)

plt.grid(True)
plt.plot(x_axis, y_axis)
plt.show()

import math
diff = [i - math.pi for i in y_axis]
plt.grid(True)
plt.plot(x_axis[2:], diff[2:])
plt.show()