import matplotlib.pyplot as plt

def doz(x,y):
  if x >= y:
    return x - y
  else:
    return 0

def my_max(x, y):
  return doz(x, y) + y

def my_min(x, y):
  return x - doz(x, y)

def relu(i):
  return my_max(i, 0)

input = [i/10 for i in range(-50,51,1)]
output = [my_min(i, 2) for i in input]

plt.grid(True)
plt.plot(input, output)

print('Example of doz')
x = 10; y = 8; print(doz(x,y))
x = 10; y = 10; print(doz(x,y))
x = 10; y = 12; print(doz(x,y))