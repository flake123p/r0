#
# center = 1, 1
#

import random
import math
import matplotlib.pyplot as plt
from util_diff_abs import *

x_axis = [0,1,2,3,4]
y_axis = [0,2,0,2,0]

plt.grid(True)
plt.scatter(x_axis, y_axis, color="r")
#plt.show()

mae = MAE(y_axis, [0])
mse = MSE(y_axis, [0])
mle = MLE(y_axis, [0])
mae_axis = [mae for i in x_axis]
mse_axis = [mse for i in x_axis]
mle_axis = [mle for i in x_axis]
plt.plot(x_axis, mae_axis, color="g")
plt.plot(x_axis, mse_axis, color="b")
plt.plot(x_axis, mle_axis, color="orange")

plt.show()