#
# center = 1, 1
#

import random
import math
import matplotlib.pyplot as plt
import numpy as np

max = 10000
inside = 0
x_axis = []
y_axis = []

for i in range(max):
    x = np.random.uniform(0, 2)
    y = np.random.uniform(0, 2)
    dist = (x-1)**2 + (y-1)**2
    dist = math.sqrt(dist)
    if (dist <= 1):
        inside+=1

    if i % 100 == 0:
      if (i == 0):
        pi = 3
      else:
        pi = inside * 4 / i # inside : total = PI : 4
      x_axis.append(i)
      y_axis.append(pi)

pi = inside * 4 / max # inside : total = PI : 4
print(pi)

plt.grid(True)
diff = [i - math.pi for i in y_axis]
plt.plot(x_axis[3:], diff[3:], color='r')

##
## Optimize
##
inside = 0
x_axis = []
y_axis = []

for i in range(max):
    x_intervals = 10
    index_x = i % x_intervals
    x = np.random.uniform(index_x*(2/x_intervals), (index_x+1)*(2/x_intervals))
    #
    # ??
    #
    y = np.random.uniform(0, 2)
    
    dist = (x-1)**2 + (y-1)**2
    dist = math.sqrt(dist)
    if (dist <= 1):
        inside+=1

    if i % 100 == 0:
      if (i == 0):
        pi = 3
      else:
        pi = inside * 4 / i # inside : total = PI : 4
      x_axis.append(i)
      y_axis.append(pi)

pi = inside * 4 / max # inside : total = PI : 4
print(pi)

plt.grid(True)
diff = [i - math.pi for i in y_axis]
plt.plot(x_axis[3:], diff[3:], color='g')

##
## Optimize 2: brute force
##
inside = 0
x_axis = []
y_axis = []
for i in range(100):
    for j in range(100):
      x = 0.02 * j
      y = 0.02 * i
      
      dist = (x-1)**2 + (y-1)**2
      dist = math.sqrt(dist)
      if (dist <= 1):
          inside+=1

      if j == 0:
        if i == 0:
          pi = 3
        else:
          pi = inside * 4 / ((i*100)+j) # inside : total = PI : 4
        x_axis.append(i*100)
        y_axis.append(pi)

pi = inside * 4 / max # inside : total = PI : 4
print(pi)

plt.grid(True)
diff = [i - math.pi for i in y_axis]
plt.plot(x_axis[3:], diff[3:], color='b')

#
#
#
plt.show()