import pandas as pd
import matplotlib.pyplot as plt

#
# Main
#
dataframe = pd.read_csv('stocks.csv')

diction = dataframe.to_dict()

x_axis = []
y_axis = []
symbolStr = ""

for i in range(len(diction["symbol"])):
    #print(diction['symbol'][i], diction['date'][i], diction['price'][i])
    printEndBrace = False
    if symbolStr != diction['symbol'][i]:
        if symbolStr != "":
            print(']')
        symbolStr = diction['symbol'][i]
        print(symbolStr, '= [')
        printEndBrace = True
    print('    ' + str(diction['price'][i]) + ',')
print(']')
#for i in diction:
#    print(i)
