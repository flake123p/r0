#
# center = 1, 1
#

import random
import math
import matplotlib.pyplot as plt
import numpy as np

max = 1000000
inside = 0
x_axis = []
y_axis = []


#
# Equation: x^2 (x square), Area between 0 ~ 6, Ans=72
#
def Equation(x):
   return x*x

for i in range(max):
    x = np.random.uniform(0, 6)
    y = np.random.uniform(0, 50)

    if y < Equation(x):
      inside+=1

area = 300 * inside / max # inside : total = PI : 4
print(area)
