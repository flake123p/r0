import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# Perceptron
class Percenptron22(object):
    def __init__(self):
        self.w = np.array([[0.0, 0.0], [0.0, 0.0]])
        self.b = np.array([0.0, 0.0])

    def process(self, input):
        ret = np.matmul(self.w, np.transpose(input)) + self.b
        act = [ 1 if r > 0 else 0 for r in ret] # hardlim
        return np.array(act) # 1x2

myNet = Percenptron22()

# shape, texture, weight
pattern = np.array([
    [1, 1],
    [1, 2],
    [2, -1],
    [2, 0],
    [-1, 2],
    [-2, 1],
    [-1, -1],
    [-2, -2]
    ], dtype = float)
target = np.array([
    [0, 0],
    [0, 0],
    [0, 1],
    [0, 1],
    [1, 0],
    [1, 0],
    [1, 1],
    [1, 1],
    ], dtype = float)

# return e vector, p vector
def do_process(index):
    e = []
    p = pattern[index]
    t = target[index]
    ret = myNet.process(p)
    e = t - ret       
    return e, p
#
# learning now
#
it = 0
while (True):
    pass_ctr = 0
    for i in range(len(pattern)):
        e, p = do_process(i)
        err_ctr = 0
        for j in range(2):
            err = e[j] # current e
            if err != 0:
                myNet.w[j] = myNet.w[j] + err * p
                myNet.b[j] = myNet.b[j] + err
                err_ctr += 1
        if err_ctr == 0:
            pass_ctr += 1       
    
    print('it =', it, ', w&b =', myNet.w.reshape(4), myNet.b)
    it += 1
    if pass_ctr == len(pattern):
        break


#
# Verify
#
print('Verify:')
for i in range(len(pattern)):
    p = pattern[i]
    t = target[i]
    a = myNet.process(p)
    print(p, t, a)


p = np.array([3.0, 3.0])
a = myNet.process(p)
print('Test pattern p&a =', p, a)