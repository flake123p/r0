import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# Perceptron
class Percenptron3(object):
    def __init__(self):
        self.w = np.array([13, 0, 0])
        self.b = 0.0

    def process(self, input):
        ret = np.sum(np.inner(input, self.w)) + self.b
        return 1 if ret > 0 else 0 # hardlim

myNet = Percenptron3()

# shape, texture, weight
orange = np.array([1, 0, 0]) # class = 0
oragne_class = 0.0
apple = np.array([1, 1, 0])  # class = 1
apple_class = 1.0
unknown = np.array([1, 1, 1])

#
# learning now
#
it = 0
while (True):
    pass_ctr = 0
    orange_result = myNet.process(orange)
    
    result_idx = orange_result
    class_idx = oragne_class
    pattern = orange
    
    if result_idx != class_idx:
        e = class_idx - result_idx
        myNet.w = myNet.w + (e * pattern)
        myNet.b = myNet.b + e
    else:
        pass_ctr += 1
    
    # next    
    apple_result = myNet.process(apple)
    
    result_idx = apple_result
    class_idx = apple_class
    pattern = apple
    
    if result_idx != class_idx:
        e = class_idx - result_idx
        myNet.w = myNet.w + (e * pattern)
        myNet.b = myNet.b + e
    else:
        pass_ctr += 1
        
    
    print('it =', it, ', w&b =', myNet.w, myNet.b)
    it += 1
    if pass_ctr == 2:
        break


orange_result = myNet.process(orange)
apple_result = myNet.process(apple)
unknown_result = myNet.process(unknown)

print('orange_result  =', orange_result)
print('apple_result   =', apple_result)
print('unknown_result =', unknown_result)