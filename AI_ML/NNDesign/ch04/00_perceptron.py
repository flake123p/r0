import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# Perceptron
class Percenptron3(object):
    def __init__(self):
        self.w = np.zeros((3)) 
        self.b = 0.0

    def process(self, input):
        ret = np.sum(np.inner(input, self.w)) + self.b
        return 1 if ret > 0 else 0 # hardlim

myNet = Percenptron3()

# shape, texture, weight
orange = [1, 0, 0] # class = 0
apple = [1, 1, 0]  # class = 1
unknown = [1, 1, 1]

orange_result = myNet.process(orange)
apple_result = myNet.process(apple)
unknown_result = myNet.process(unknown)

print('orange_result  =', orange_result)
print('apple_result   =', apple_result)
print('unknown_result =', unknown_result)