def harlim(i):
  if i < 0:
    return 0
  else:
    return 1

input = [-3, -2, -1, 0, 1, 2, 3]

for i in input:
  print(harlim(i))

output = [harlim(i) for i in input]

import matplotlib.pyplot as plt
plt.grid(True)
plt.plot(input, output)
plt.show()