class DelayBlock(object):
  def __init__(self, a0):
    self.curr = a0

  def process(self, input):
    temp = self.curr
    self.curr = input
    return temp

class IntegratorBlock(object):
  def __init__(self):
    self.accumulation = 0

  def process(self, input):
    self.accumulation += input
    return self.accumulation

delay00 = DelayBlock(a0 = 9)
inte00 = IntegratorBlock()

input = [0, 1, 2, 3, 5, 7]
print('input:')
print(input)

DelayBlock_DEMO = [delay00.process(i) for i in input]
print('DelayBlock_DEMO:')
print(DelayBlock_DEMO)

IntegratorBlock_DEMO = [inte00.process(i) for i in input]
print('IntegratorBlock_DEMO:')
print(IntegratorBlock_DEMO)
