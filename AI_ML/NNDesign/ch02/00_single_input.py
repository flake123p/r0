def harlim(i):
  if i < 0:
    return 0
  else:
    return 1

def SingleInputNeuron(input):
  w = 2
  b = 8
  return harlim((input*w)+b)

input = [i/10 for i in range(-50,50,1)]

print('input =', input)

output = [SingleInputNeuron(i) for i in input]

import matplotlib.pyplot as plt
plt.grid(True)
plt.plot(input, output)
plt.show()
