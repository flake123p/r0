import math
def logsig(i):
    return 1/(1+math.pow(math.e, -i))

input = [i/10 for i in range(-50,50,1)]

print(input)

output = [logsig(i) for i in input]

import matplotlib.pyplot as plt
plt.grid(True)
plt.plot(input, output)
plt.show()