def purelin(i):
    return i

input = [-3, -2, -1, 0, 1, 2, 3]

for i in input:
  print(purelin(i))

output = [purelin(i) for i in input]

import matplotlib.pyplot as plt
plt.grid(True)
plt.plot(input, output)
plt.show()