def satlins(n):
  if n < -1:
    return -1
  elif n > 1:
    return 1
  else:
    return n

input = [i/10 for i in range(-50,50,1)]

print(input)

output = [satlins(i) for i in input]

import matplotlib.pyplot as plt
plt.grid(True)
plt.plot(input, output)
plt.show()
