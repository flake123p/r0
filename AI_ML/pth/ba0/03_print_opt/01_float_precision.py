#
# https://pytorch.org/tutorials/beginner/introyt/tensors_deeper_tutorial.html
#
import torch

a = torch.arange(0, 4096, dtype=torch.float32)
print(a)
b = a[4095]/33333333.0
print(b, '\n')


torch.set_printoptions(precision=9)
print(a)
print(b, '\n')