import torch
import numpy as np

'''
from numpy: pthA = torch.from_numpy(npA)
to numpy: npA = pthA.numpy()

xq   shpae = [1, 21, 32, 128] => transpose(1, 2) => [1, 32, 21, 128]
keys shape = [1, 21, 32, 128] => transpose(1, 2) => [1, 32, 21, 128] => transpose(2, 3) => [1, 32, 128, 21]

xq = xq.transpose(1, 2)
keys = keys.transpose(1, 2)
scores = torch.matmul(xq, keys.transpose(2, 3)) => [1, 32, 21, 21]

scores = scores / mat.sqrt(self.head_dims) (128) => elementwise division

'''

torch.manual_seed(0)

A = torch.tensor([1, 2], dtype=torch.float32)
print('A =', A)

B = torch.tensor([[[0, 1], [2, 3]], [[4, 5], [6, 7]], [[8, 9], [10, 11]]], dtype=torch.float32)
print('B =', B)

B = B.transpose(0, 1)

C = torch.matmul(A, B.transpose(1, 2))
print('C =', C)

# A = tensor([1., 2.])
# B = tensor([[[ 0.,  1.],
#              [ 2.,  3.]],
#             [[ 4.,  5.],
#              [ 6.,  7.]],
#             [[ 8.,  9.],
#              [10., 11.]]])
# C = tensor([ 2., 14., 26.])


# c = c.reshape(2, 3)
# print(c)

# diff = 10
# A = np.arange(0-diff, 48-diff, dtype = np.float32)
# A = A.reshape([1, 2, 3, 8])
# A = torch.from_numpy(A)

# diff = 20
# B = np.arange(0-diff, 48-diff, dtype = np.float32)
# B = B.reshape([1, 2, 3, 8])
# B = torch.from_numpy(B)

# A = A.transpose(1, 2)
# B = B.transpose(1, 2)
# C = torch.matmul(A, B.transpose(2, 3))