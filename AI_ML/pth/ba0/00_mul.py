from __future__ import print_function
import torch
import numpy as np

if torch.cuda.is_available(): 
    dev = "cuda:0" 
else: 
    dev = "cpu" 
device = torch.device(dev)

a = torch.tensor([3.0], device=device)
#a = a.to(device)
b = torch.tensor([2.0], device=device)
#a = b.to(device)
c = a * b
#c = c.to(device)

print(c)

