import torch
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import numpy as np

'''
Example:

    y = wx + b

    Golden:
        w = 10
        b = -3
    
    Dataset:
        x   ->  y
        1       7
        2       17

'''
def Demo_Gen_Data_Npy():
    tests = np.array([[1], [2]], dtype = np.float32)
    labels = np.array([[7], [17]], dtype = np.float32)
    np.save("00_tests_local", tests)
    np.save("00_labels_local", labels)

class MyDataset(Dataset):
    def __init__(self, transform=None, target_transform=None):
        self.tests = np.load("00_tests_local.npy")
        self.labels = np.load("00_labels_local.npy")
        self.transform = transform
        self.target_transform = target_transform

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        return self.tests[idx], self.labels[idx]

Demo_Gen_Data_Npy()

training_data = MyDataset()
train_dataloader = DataLoader(training_data, batch_size=1, shuffle=False)

itor = iter(train_dataloader)

train_features, train_labels = next(itor)
print(f"Feature batch shape: {train_features.size()}") # Feature batch shape: torch.Size([1, 1])
print(f"Labels batch shape: {train_labels.size()}")    # Labels batch shape:  torch.Size([1, 1])

print(f"train_features: {train_features}") # train_features: tensor([[1.]])
print(f"train_labels: {train_labels}")     # train_labels:   tensor([[7.]])

train_features, train_labels = next(itor)
print(f"train_features: {train_features}") # train_features: tensor([[2.]])
print(f"train_labels: {train_labels}")     # train_labels:   tensor([[17.]])
