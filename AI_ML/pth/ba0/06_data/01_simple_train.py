import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import numpy as np

torch.manual_seed(0)
'''
Example:

    y = wx + b

    Golden:
        w = 10
        b = -3
    
    Dataset:
        x   ->  y
        1       7
        2       17

'''

class MyDataset(Dataset):
    def __init__(self, transform=None, target_transform=None):
        self.tests = np.load("00_tests_local.npy")
        self.labels = np.load("00_labels_local.npy")
        self.transform = transform
        self.target_transform = target_transform

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        return self.tests[idx], self.labels[idx]

training_data = MyDataset()
train_dataloader = DataLoader(training_data, batch_size=1, shuffle=False)

# itor = iter(train_dataloader)

# train_features, train_labels = next(itor)
# print(f"Feature batch shape: {train_features.size()}") # Feature batch shape: torch.Size([1, 1])
# print(f"Labels batch shape: {train_labels.size()}")    # Labels batch shape:  torch.Size([1, 1])

# print(f"train_features: {train_features}") # train_features: tensor([[1.]])
# print(f"train_labels: {train_labels}")     # train_labels:   tensor([[7.]])

# train_features, train_labels = next(itor)
# print(f"train_features: {train_features}") # train_features: tensor([[2.]])
# print(f"train_labels: {train_labels}")     # train_labels:   tensor([[17.]])

class MyNet(nn.Module):
    def __init__(self):
        super(MyNet, self).__init__()
        self.fc = nn.Linear(1, 1)
        print("fc weight = ", self.fc.weight.data)
        print("fc bias   = ", self.fc.bias.data)

    def forward(self, x):
        return self.fc(x)

model = MyNet()
loss_fn = nn.MSELoss()
optimizer = torch.optim.SGD(model.parameters(), lr=1e-1)

def train(model, train_dataloader, optimizer, loss_fn):
    model.train()
    for batch_idx, (data, target) in enumerate(train_dataloader):
        data, target = data.cpu(), target.cpu()
        # print(batch_idx)
        # print(data)
        # print(target)
        
        output = model(data)
        loss = loss_fn(output, target)
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()


for epoch in range(100):
    train(model, train_dataloader, optimizer, loss_fn)
    # test(model, device, test_loader)
    # scheduler.step()

    print('e:', epoch, " - model weight = ", model.fc.weight.data)
    print('e:', epoch, " - model bias   = ", model.fc.bias.data)