#
# https://pytorch.org/tutorials/beginner/introyt/tensors_deeper_tutorial.html
#
import torch

a = torch.arange(0, 4096, dtype=torch.float32)
print(a)
print(a.shape, '\n')

a = a.reshape((2048, 2))
print(a)
print(a.shape, '\n')

a = a.reshape((2, 2048))
print(a)
print(a.shape, '\n')

a = a.reshape((2, 2048, 1))
print(a)
print(a.shape, '\n')