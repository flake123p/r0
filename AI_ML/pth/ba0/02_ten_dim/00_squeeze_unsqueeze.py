#
# https://pytorch.org/tutorials/beginner/introyt/tensors_deeper_tutorial.html
#
import torch

batch_me = torch.rand(3, 226, 226)
print(batch_me.shape)
batch_me.unsqueeze_(0)
print(batch_me.shape)
batch_me.squeeze_(0)
print(batch_me.shape)