#
# https://pytorch.org/docs/stable/generated/torch.cuda.is_available.html
#
import torch

a = torch.tensor([1.1, 2.2])
print("should be in cpu:", a)

a = a.to("cuda")
print("should be in gpu:", a)

a = a.to("cpu")
print("should be in cpu:", a)

gpu_dev = torch.device("cuda")
a = a.to(gpu_dev)
print("should be in gpu:", a)

a = a.cpu()
print("should be in cpu:", a)

a = a.cuda()
print("should be in gpu:", a)