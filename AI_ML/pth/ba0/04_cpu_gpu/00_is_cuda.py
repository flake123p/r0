#
# https://pytorch.org/docs/stable/generated/torch.cuda.is_available.html
#
import torch

print(torch.cuda.is_available())
