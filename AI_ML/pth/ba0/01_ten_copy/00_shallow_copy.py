#
# https://pytorch.org/tutorials/beginner/introyt/tensors_deeper_tutorial.html
#
import torch

a = torch.ones(2, 2)
b = a

a[0][1] = 561  # we change a...

print('a = ', a)
print('b = ', b)       # ...and b is also altered

#
# b is pointer to a
#