#
# https://pytorch.org/tutorials/beginner/introyt/tensors_deeper_tutorial.html
#
import torch

a = torch.ones((2, 3), dtype=torch.int16)
print(a)

b = torch.rand((2, 3), dtype=torch.float64) * 20.
print(b)

c = b.to(torch.int32)
print(c)

'''
Available data types include:
    torch.bool
    torch.int8
    torch.uint8
    torch.int16
    torch.int32
    torch.int64
    torch.half
    torch.float
    torch.double
    torch.bfloat
'''