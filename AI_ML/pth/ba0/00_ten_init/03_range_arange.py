#
# https://pytorch.org/tutorials/beginner/introyt/tensors_deeper_tutorial.html
#
import torch

#
# Deprecated !!!!!!!!!!!!!!!!1
#
#a = torch.range(1, 6)
#print(a)

b = torch.arange(0, 6) # tensor([0, 1, 2, 3, 4, 5])
print(b)

c = torch.arange(0, 6, dtype=torch.float32) # tensor([0., 1., 2., 3., 4., 5.])
print(c)
