import torch
import math

torch.manual_seed(0)

shape = (2,3)
input = torch.Tensor(size = shape).uniform_(1, 9).type(torch.float32)
#input = torch.empty(size = shape) 

print("Before zeros_likes():")
print(input)
input = torch.zeros_like(input)
print("After zeros_likes():")
print(input)
print("\n")

shape = (2,1)
input = torch.Tensor(size = shape).uniform_(1, 9).type(torch.float32)
#input = torch.empty(size = shape) 

print("Before expand(-1, 3):")
print(input)
print('shape =', input.shape, 'stide =', input.stride(), 'conti =', input.is_contiguous())
print(input.storage())
input = input.expand(-1, 3)
print("After expand(-1, 3):")
print(input)
print('shape =', input.shape, 'stide =', input.stride(), 'conti =', input.is_contiguous())
print(input.storage())
print("\n")