import torch

torch.manual_seed(0)

shape = (2,3)
a = torch.Tensor(size=shape).uniform_(-4.0, 4.0)
print(a)

input1 = torch.Tensor(7).uniform_(-6.0, 6.0)
print('input1 =', input1)

input2 = torch.Tensor(size=(3,5)).uniform_(-6.0, 6.0).type(torch.float16)
print('input2 =', input2)

input3 = torch.Tensor(size=(3,5)).uniform_(-6.0, 6.0).type(torch.float32)
print('input3 =', input3)

input4 = torch.rand(size=(2, 3), dtype=torch.float64) # 0 ~ 1, uniform !!!!!!!
print('input4 =', input4)
