#
# https://pytorch.org/docs/stable/generated/torch.cuda.is_available.html
#

'''
from numpy: pthA = torch.from_numpy(npA)
to numpy: npA = pthA.numpy()
'''

import torch
import numpy as np

npA = np.array([(1, 3), (5, 7)], dtype = np.float32)

print("\n------ numpy ------")
print(npA)
print(type(npA))
print(npA.dtype)

print("\n------ numpy to torch------")
npA_pth = torch.from_numpy(npA)
print(npA_pth)
print(npA_pth.dtype)

print("\n------ torch to numpy ------")
npA_pth_np = npA_pth.numpy()
print(npA_pth_np)
print(type(npA_pth_np))
print(npA_pth_np.dtype)
