# Book:
Deep Learning with PyTorch

# Jupyter Github:
git clone https://github.com/deep-learning-with-pytorch/dlwpt-code

# Naming Rule:
_t : cpu tensor
_g : gpu tensor
_a : numpy array

# Other learning resource:
Grokking Deep Learning
www.manning.com/books/grokking-deep-learning

Goodfellow <<Deep Learning>>

# FLAG
https://www.flag.com.tw/bk/st/f1388
