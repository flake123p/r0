import torch
import math

if __name__ == '__main__':
    in1 = torch.zeros(4)

    print('in1.get_device() =', in1.get_device(), ', ( -1 for CPU )')

    in2 = torch.zeros(4).cuda()

    print('in2.get_device() =', in2.get_device())
