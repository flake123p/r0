import torch
import math

torch.manual_seed(0)

input = torch.Tensor(size=(1,5)).uniform_(-1.0, 6.0).type(torch.float32)
input[0][0] = 1.0
print('input =', input)

m = torch.nn.Sigmoid()
output = m(input)

print('output =', output)



input = torch.tensor([-1, 0.67])
print('input =', input)

m = torch.nn.Sigmoid()
output = m(input)

print('output =', output)



input = torch.tensor([0.00234])
print('input =', input)

m = torch.nn.Sigmoid()
output = m(input)

print('output =', output)