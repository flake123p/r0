import torch
import math


ctr = torch.zeros(4)

input = torch.tensor([0, 10, 3, 0], dtype=torch.float) 


for i in range(100000):
    output = torch.multinomial(input,1)
    ctr[output] += 1

print(ctr)
