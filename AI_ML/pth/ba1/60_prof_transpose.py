import torch
import math
import time

shape = (20000, 20000)
dType = torch.float32
input = torch.rand(size = shape, dtype = dType) # 0 ~ 1, uniform !!!!!!!
inputGPU = input.cuda()

loop = 1

def cpu_copy():
    # print("start cpu_copy()")
    start = time.time()
    for _ in range(loop):
        b = torch.zeros(size = shape, dtype = dType)
        b = b.copy_(input)
    end = time.time()
    print(f'cpu_copy   - {(end - start) * 1000:10.6f}', 'ms', 'b : conti =', b.is_contiguous())

def gpu_copy():
    start = time.time()
    for _ in range(loop):
        b = torch.zeros(size = shape, dtype = dType)
        b = b.copy_(inputGPU)
    end = time.time()
    print(f'gpu_copy   - {(end - start) * 1000:10.6f}', 'ms', 'b : conti =', b.is_contiguous())

def cpu_copy_T():
    # print("start cpu_copy()")
    start = time.time()
    for _ in range(loop):
        b = torch.zeros(size = shape, dtype = dType)
        b = b.copy_(input.T)
    end = time.time()
    print(f'cpu_copy_T - {(end - start) * 1000:10.6f}', 'ms', 'b : conti =', b.is_contiguous())

def gpu_copy_T():
    start = time.time()
    for _ in range(loop):
        b = torch.zeros(size = shape, dtype = dType)
        b = b.copy_(inputGPU.T)
    end = time.time()
    print(f'gpu_copy_T - {(end - start) * 1000:10.6f}', 'ms', 'b : conti =', b.is_contiguous())

cpu_copy()
gpu_copy()
cpu_copy_T()
gpu_copy_T()
