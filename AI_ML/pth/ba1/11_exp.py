import torch
import struct
import numpy

torch.manual_seed(0)

# Convert float to its IEEE 754 binary representation
def float_to_binary(num):
    # Pack the float into bytes
    packed = struct.pack('!f', num)
    # Convert bytes to integer
    integer = struct.unpack('!I', packed)[0]
    # Convert integer to binary string
    binary = f'{integer:032b}'
    return binary

def float_to_binary_dump(tensor):
    # Get the binary representation
    binary = float_to_binary(tensor.item())

    # Split the binary string into sign, exponent, and fraction
    sign = binary[0]
    exponent = binary[1:9]
    fraction = binary[9:]

    print(f"\nBinary representation (IEEE 754 single-precision):")
    print(f"Sign:     {sign}")
    print(f"Exponent: {exponent}")
    print(f"Fraction: {fraction}")

    print(f"\nFull binary: {binary}")

def exp_cpu_gpu(tensor):
    cpu = torch.exp(tensor)
    g = tensor.cuda()
    gpu = torch.exp(g)
    gpu = gpu.cpu()
    diff = (gpu-cpu)/cpu
    b = float_to_binary(gpu)
    # print('[torch.exp('+ f'{tensor:9.3f}' + ') : ' + f'{cpu :90.50f} / {gpu :90.50f} / {diff:-14.10f} / ' + b)
    print('[torch.exp('+ f'{tensor:9.3f}' + ') : ' + f'{gpu :-14.9e} / ' + b + ' / ' + float_to_binary(cpu))
    return cpu, gpu

# print('[torch.exp(-inf):', f'{torch.exp(torch.tensor(-torch.inf, dtype=torch.float32)) :19.9f}')
# print('[torch.exp( int):', f'{torch.exp(torch.tensor(torch.inf, dtype=torch.float32)) :19.9f}')
# print('[torch.exp(0.2) :', f'{torch.exp(torch.tensor(0.2, dtype=torch.float32)) :19.9f}')
# print('[torch.exp(1)   :', f'{torch.exp(torch.tensor(1,   dtype=torch.float32)) :19.9f}')
# print('[torch.exp(1.3) :', f'{torch.exp(torch.tensor(1.3, dtype=torch.float32)) :19.9f}')
# print('[torch.exp(20)  :', f'{torch.exp(torch.tensor(20,  dtype=torch.float32)) :19.9f}')

t = torch.tensor(-0.2, dtype=torch.float32)
exp_cpu_gpu(t)
t = torch.tensor(0.2, dtype=torch.float32)
exp_cpu_gpu(t)
t = torch.tensor(1, dtype=torch.float32)
exp_cpu_gpu(t)
t = torch.tensor(1.3, dtype=torch.float32)
exp_cpu_gpu(t)
t = torch.tensor(20, dtype=torch.float32)
exp_cpu_gpu(t)
t = torch.tensor(88.7, dtype=torch.float32)
c, g = exp_cpu_gpu(t)
t = torch.tensor(89, dtype=torch.float32)
exp_cpu_gpu(t)
t = torch.tensor(-103.97, dtype=torch.float32)
exp_cpu_gpu(t)
t = torch.tensor(-103.98, dtype=torch.float32)
exp_cpu_gpu(t)

c = torch.arange(-104, 89.1, 0.1, dtype=torch.float32) # tensor([0., 1., 2., 3., 4., 5.])
print(c)
input_npy = c.numpy()
numpy.save("11_golden_input.npy", input_npy)

cpu_golden = torch.exp(c)
print(cpu_golden)
cpu_npy = cpu_golden.numpy()
# numpy.save("11_golden_cpu.npy", cpu_npy)

gpu_golden = torch.exp(c.cuda()).cpu()
print(gpu_golden)
# for g in gpu_golden:
#     print(g)
gpu_npy = gpu_golden.numpy()
# numpy.save("11_golden_gpu.npy", gpu_npy)
# print('len =', len(gpu_npy))


# input = torch.Tensor(size=(1,200)).uniform_(-100.0, 100.0).type(torch.float32)
# # print(input)
# for i in input[0]:
#     # print(f'{i:19.9f}')
#     exp_cpu_gpu(i)

# for i in input[0]:
#     print(f'{i:50.40f}')
#     # exp_cpu_gpu(i)
