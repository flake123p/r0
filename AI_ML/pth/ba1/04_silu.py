import torch
import math

cudaF16 = []

input = torch.tensor([-6.0, -4.0, -2.0, -1.0, 1.0, 2.0, 4.0, 6.0], dtype = torch.float16).cuda()

m = torch.nn.SiLU()
output = m(input)

#print(output)
for i in output:
    print(type(i.item()), i.item())
    cudaF16.append(i.item())

cpuF32 = []

input = torch.tensor([-6.0, -4.0, -2.0, -1.0, 1.0, 2.0, 4.0, 6.0], dtype = torch.float32)

m = torch.nn.SiLU()
output = m(input)

#print(output)
for i in output:
    print(type(i.item()), i.item())
    cpuF32.append(i.item())

diff = []
for i in range(len(cpuF32)):
    d = math.fabs(cpuF32[i] - cudaF16[i])
    print(d)

my = [
-0.014831542968750000000000000000000000000000000000000000000000,
-0.071899414062500000000000000000000000000000000000000000000000,
-0.238403320312500000000000000000000000000000000000000000000000,
-0.268798828125000000000000000000000000000000000000000000000000,
0.730957031250000000000000000000000000000000000000000000000000 ,
1.760742187500000000000000000000000000000000000000000000000000 ,
3.927734375000000000000000000000000000000000000000000000000000 ,
5.984375000000000000000000000000000000000000000000000000000000 ,
]

for i in range(len(cpuF32)):
    d = math.fabs(cpuF32[i] - my[i])
    print(d)