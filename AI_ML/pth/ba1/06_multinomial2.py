import torch
import math

def bingo():
    input = torch.ones(16, dtype=torch.float)
    input[0] = 2

    output = torch.multinomial(input, 16) + 1

    print(output)


def multinomial_role(input, selectable):
    target = input * selectable
    
    sum = torch.sum(target)
    target = target / sum
    r = torch.rand(1)
    
    selected = 0
    target = torch.cumsum(target, dim=0)
    # print('r', r, target)
    for i in range(target.size()[0]):
        if target[i] == 0:
            selected += 1
            continue
        if (i != 0):
            if target[i] == target[i-1]:
                selected += 1
                continue
        if r < target[i]:
            break
        selected += 1
    return selected

def multinomial(input):
    output = torch.zeros(input.size())
    selectable = torch.ones(input.size())
    len = input.size()
    # sum = torch.sum(input)
    # print(len, sum)
    # print('selectable', selectable)

    curr = len[0]
    for i in range(len[0]):
        selected = multinomial_role(input, selectable)
        output[selected] = curr
        curr -= 1
        selectable[selected] = 0
        # print('selected', selected)
    
    output = input / output
    return output


def myAlgo():
    input = torch.tensor([7.0, 3.0])
    
    ctr = 0
    # print(input)
    # std = torch.std(input)
    # print('std', std)
    # var = torch.var(input)
    # print('var', var)
    for i in range(10000):
        multi_dist = multinomial(input)

        # r = (1 / torch.rand(2)) * input * input
        # print('multi_dist', multi_dist)
        ans = input / multi_dist
        # print(ans)
        if ans[0] > ans[1]:
            # print('bg')
            ctr += 1
    print(ctr)


def zeros_test():
    ctr = torch.zeros(1)
    input = torch.tensor([0.0, 0.0, 0.0, 7.0, 3.0])

    for i in range(2):
        output = torch.multinomial(input, 5)
        ctr += (output[2] * 100 + output[3] * 10 + output[4])
        # print(output[2], output[3])

    #print(output)
    print(ctr)

if __name__ == '__main__':
    print('hello main')
    #myAlgo()
    zeros_test()
