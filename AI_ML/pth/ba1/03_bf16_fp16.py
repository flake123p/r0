import torch
import math

input1 = torch.tensor([1.3], dtype = torch.float32)
input2 = torch.tensor([1.3], dtype = torch.float16)
input3 = torch.tensor([1.3], dtype = torch.bfloat16)
input4 = torch.tensor([-1.3], dtype = torch.bfloat16)

print(input1)
print(input2)
print(input3)
print(input4)

print(float.hex(input1.item()))
print(float.hex(input2.item()))
print(float.hex(input3.item()))
print(float.hex(input4.item()))
print(type(float.hex(input1.item())))
print(type(float.hex(input2.item())))
print(type(float.hex(input3.item())))
print(type(float.hex(input4.item())))