import torch
import math

shape = (3,4)
input1 = torch.arange(0, 12, dtype=torch.float32)
input1 = input1.reshape(shape)
# input1ori = torch.zeros(size=input1.shape)
# input1ori = input1ori.copy_(input1)
print('input 1 : BEFORE TRANSPOSE')
print('input 1 : shape =', input1.shape)
print('input 1 : conti =', input1.is_contiguous())
print('input 1 : stride  =', input1.stride())
print('input 1 : storage =', input1.storage())


# These two are indentical.
#input1 = input1.transpose(0, 1)
input1 = input1.T

print('input 1 : AFTER TRANSPOSE')
print('input 1 : shape =', input1.shape)
print('input 1 : conti =', input1.is_contiguous())
print('input 1 : stride  =', input1.stride())
print('input 1 : storage =', input1.storage())

input2 = torch.zeros(size=input1.shape)
input2 = input2.copy_(input1)
print('input 2 : copy_ input1')
print('input 2 : shape =', input2.shape)
print('input 2 : conti =', input2.is_contiguous())
print('input 2 : storage =', input2.storage())


print('\n [[[ view & contiguous exampe: ]]]')
shape = (1,2,3,4)
a = torch.arange(0, 24, dtype=torch.float32)
a = a.reshape(shape)
print('size of a =', a.size())
print('contiguous of a =', a.is_contiguous())
b = a.transpose(1, 2)
print('size of b =', b.size())
print('contiguous of b =', b.is_contiguous())
c = a.view(1, 3, 2, 4)
print('size of c =', c.size())
print('contiguous of c =', c.is_contiguous())
print('torch.equal(b, c) =', torch.equal(b, c))
# print(a)
# print(b)
# print(c)
bc = a.transpose(1, 2).contiguous()
print('size of bc =', bc.size())
print('contiguous of bc =', bc.is_contiguous())
print('torch.equal(b, bc) =', torch.equal(b, bc))