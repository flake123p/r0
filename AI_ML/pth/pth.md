
# PyTorch Loss Functions: The Ultimate Guide
https://neptune.ai/blog/pytorch-loss-functions


# Versions for installation:
https://pytorch.org/get-started/previous-versions/
e.g.:
    pip install torch==2.0.1 torchvision==0.15.2 torchaudio==2.0.2


# Failed to initialize NumPy: _ARRAY_API not found
https://stackoverflow.com/questions/78681145/pytorch-userwarning-failed-to-initialize-numpy-array-api-not-found-and-bertmo
    pip install --force-reinstall -v "numpy==1.25.2"