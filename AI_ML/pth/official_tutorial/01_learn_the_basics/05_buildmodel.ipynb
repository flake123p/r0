{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# For tips on running notebooks in Google Colab, see\n",
    "# https://pytorch.org/tutorials/beginner/colab\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Learn the Basics](intro.html) \\|\\|\n",
    "[Quickstart](quickstart_tutorial.html) \\|\\|\n",
    "[Tensors](tensorqs_tutorial.html) \\|\\| [Datasets &\n",
    "DataLoaders](data_tutorial.html) \\|\\|\n",
    "[Transforms](transforms_tutorial.html) \\|\\| **Build Model** \\|\\|\n",
    "[Autograd](autogradqs_tutorial.html) \\|\\|\n",
    "[Optimization](optimization_tutorial.html) \\|\\| [Save & Load\n",
    "Model](saveloadrun_tutorial.html)\n",
    "\n",
    "Build the Neural Network\n",
    "========================\n",
    "\n",
    "Neural networks comprise of layers/modules that perform operations on\n",
    "data. The [torch.nn](https://pytorch.org/docs/stable/nn.html) namespace\n",
    "provides all the building blocks you need to build your own neural\n",
    "network. Every module in PyTorch subclasses the\n",
    "[nn.Module](https://pytorch.org/docs/stable/generated/torch.nn.Module.html).\n",
    "A neural network is a module itself that consists of other modules\n",
    "(layers). This nested structure allows for building and managing complex\n",
    "architectures easily.\n",
    "\n",
    "In the following sections, we\\'ll build a neural network to classify\n",
    "images in the FashionMNIST dataset.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import os\n",
    "import torch\n",
    "from torch import nn\n",
    "from torch.utils.data import DataLoader\n",
    "from torchvision import datasets, transforms"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Get Device for Training\n",
    "=======================\n",
    "\n",
    "We want to be able to train our model on a hardware accelerator like the\n",
    "GPU or MPS, if available. Let\\'s check to see if\n",
    "[torch.cuda](https://pytorch.org/docs/stable/notes/cuda.html) or\n",
    "[torch.backends.mps](https://pytorch.org/docs/stable/notes/mps.html) are\n",
    "available, otherwise we use the CPU.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Using cpu device\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/home/pupil/sda/ws/VENV/envs/py39/lib/python3.9/site-packages/torch/cuda/__init__.py:118: UserWarning: CUDA initialization: CUDA unknown error - this may be due to an incorrectly set up environment, e.g. changing env variable CUDA_VISIBLE_DEVICES after program start. Setting the available devices to be zero. (Triggered internally at ../c10/cuda/CUDAFunctions.cpp:108.)\n",
      "  return torch._C._cuda_getDeviceCount() > 0\n"
     ]
    }
   ],
   "source": [
    "device = (\n",
    "    \"cuda\"\n",
    "    if torch.cuda.is_available()\n",
    "    else \"mps\"\n",
    "    if torch.backends.mps.is_available()\n",
    "    else \"cpu\"\n",
    ")\n",
    "print(f\"Using {device} device\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Define the Class\n",
    "================\n",
    "\n",
    "We define our neural network by subclassing `nn.Module`, and initialize\n",
    "the neural network layers in `__init__`. Every `nn.Module` subclass\n",
    "implements the operations on input data in the `forward` method.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "class NeuralNetwork(nn.Module):\n",
    "    def __init__(self):\n",
    "        super().__init__()\n",
    "        self.flatten = nn.Flatten()\n",
    "        self.linear_relu_stack = nn.Sequential(\n",
    "            nn.Linear(28*28, 512),\n",
    "            nn.ReLU(),\n",
    "            nn.Linear(512, 512),\n",
    "            nn.ReLU(),\n",
    "            nn.Linear(512, 10),\n",
    "        )\n",
    "\n",
    "    def forward(self, x):\n",
    "        x = self.flatten(x)\n",
    "        logits = self.linear_relu_stack(x)\n",
    "        return logits"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We create an instance of `NeuralNetwork`, and move it to the `device`,\n",
    "and print its structure.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "NeuralNetwork(\n",
      "  (flatten): Flatten(start_dim=1, end_dim=-1)\n",
      "  (linear_relu_stack): Sequential(\n",
      "    (0): Linear(in_features=784, out_features=512, bias=True)\n",
      "    (1): ReLU()\n",
      "    (2): Linear(in_features=512, out_features=512, bias=True)\n",
      "    (3): ReLU()\n",
      "    (4): Linear(in_features=512, out_features=10, bias=True)\n",
      "  )\n",
      ")\n"
     ]
    }
   ],
   "source": [
    "model = NeuralNetwork().to(device)\n",
    "print(model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To use the model, we pass it the input data. This executes the model\\'s\n",
    "`forward`, along with some [background\n",
    "operations](https://github.com/pytorch/pytorch/blob/270111b7b611d174967ed204776985cefca9c144/torch/nn/modules/module.py#L866).\n",
    "Do not call `model.forward()` directly!\n",
    "\n",
    "Calling the model on the input returns a 2-dimensional tensor with dim=0\n",
    "corresponding to each output of 10 raw predicted values for each class,\n",
    "and dim=1 corresponding to the individual values of each output. We get\n",
    "the prediction probabilities by passing it through an instance of the\n",
    "`nn.Softmax` module.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Predicted class: tensor([5])\n"
     ]
    }
   ],
   "source": [
    "X = torch.rand(1, 28, 28, device=device)\n",
    "logits = model(X)\n",
    "pred_probab = nn.Softmax(dim=1)(logits)\n",
    "y_pred = pred_probab.argmax(1)\n",
    "print(f\"Predicted class: {y_pred}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------------------------------------------------------------------------\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Model Layers\n",
    "============\n",
    "\n",
    "Let\\'s break down the layers in the FashionMNIST model. To illustrate\n",
    "it, we will take a sample minibatch of 3 images of size 28x28 and see\n",
    "what happens to it as we pass it through the network.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "torch.Size([3, 28, 28])\n"
     ]
    }
   ],
   "source": [
    "input_image = torch.rand(3,28,28)\n",
    "print(input_image.size())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "nn.Flatten\n",
    "==========\n",
    "\n",
    "We initialize the\n",
    "[nn.Flatten](https://pytorch.org/docs/stable/generated/torch.nn.Flatten.html)\n",
    "layer to convert each 2D 28x28 image into a contiguous array of 784\n",
    "pixel values ( the minibatch dimension (at dim=0) is maintained).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "torch.Size([3, 784])\n"
     ]
    }
   ],
   "source": [
    "flatten = nn.Flatten()\n",
    "flat_image = flatten(input_image)\n",
    "print(flat_image.size())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "nn.Linear\n",
    "=========\n",
    "\n",
    "The [linear\n",
    "layer](https://pytorch.org/docs/stable/generated/torch.nn.Linear.html)\n",
    "is a module that applies a linear transformation on the input using its\n",
    "stored weights and biases.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "torch.Size([3, 20])\n"
     ]
    }
   ],
   "source": [
    "layer1 = nn.Linear(in_features=28*28, out_features=20)\n",
    "hidden1 = layer1(flat_image)\n",
    "print(hidden1.size())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "nn.ReLU\n",
    "=======\n",
    "\n",
    "Non-linear activations are what create the complex mappings between the\n",
    "model\\'s inputs and outputs. They are applied after linear\n",
    "transformations to introduce *nonlinearity*, helping neural networks\n",
    "learn a wide variety of phenomena.\n",
    "\n",
    "In this model, we use\n",
    "[nn.ReLU](https://pytorch.org/docs/stable/generated/torch.nn.ReLU.html)\n",
    "between our linear layers, but there\\'s other activations to introduce\n",
    "non-linearity in your model.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Before ReLU: tensor([[ 1.0100, -0.1063,  0.4945,  0.1807, -0.8681, -0.1111,  0.2917,  0.1593,\n",
      "         -0.3541,  0.4575, -0.1327,  0.0260,  0.1402,  0.5356,  0.2170, -0.1206,\n",
      "          0.1790,  0.2209,  0.1943,  0.2060],\n",
      "        [ 0.8483, -0.0022,  0.0465,  0.2018, -0.6875, -0.2420,  0.2206,  0.0920,\n",
      "         -0.2863,  0.3724,  0.3620, -0.0485,  0.0386,  0.3361,  0.4306, -0.1227,\n",
      "          0.3416,  0.5689, -0.0339,  0.2546],\n",
      "        [ 0.7383,  0.1454,  0.3237, -0.1604, -0.9557, -0.1084,  0.1665, -0.2815,\n",
      "         -0.1335,  0.3015,  0.1553,  0.3362,  0.0596, -0.0574,  0.5837, -0.2576,\n",
      "          0.2913,  0.3808,  0.0380, -0.0362]], grad_fn=<AddmmBackward0>)\n",
      "\n",
      "\n",
      "After ReLU: tensor([[1.0100, 0.0000, 0.4945, 0.1807, 0.0000, 0.0000, 0.2917, 0.1593, 0.0000,\n",
      "         0.4575, 0.0000, 0.0260, 0.1402, 0.5356, 0.2170, 0.0000, 0.1790, 0.2209,\n",
      "         0.1943, 0.2060],\n",
      "        [0.8483, 0.0000, 0.0465, 0.2018, 0.0000, 0.0000, 0.2206, 0.0920, 0.0000,\n",
      "         0.3724, 0.3620, 0.0000, 0.0386, 0.3361, 0.4306, 0.0000, 0.3416, 0.5689,\n",
      "         0.0000, 0.2546],\n",
      "        [0.7383, 0.1454, 0.3237, 0.0000, 0.0000, 0.0000, 0.1665, 0.0000, 0.0000,\n",
      "         0.3015, 0.1553, 0.3362, 0.0596, 0.0000, 0.5837, 0.0000, 0.2913, 0.3808,\n",
      "         0.0380, 0.0000]], grad_fn=<ReluBackward0>)\n"
     ]
    }
   ],
   "source": [
    "print(f\"Before ReLU: {hidden1}\\n\\n\")\n",
    "hidden1 = nn.ReLU()(hidden1)\n",
    "print(f\"After ReLU: {hidden1}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "nn.Sequential\n",
    "=============\n",
    "\n",
    "[nn.Sequential](https://pytorch.org/docs/stable/generated/torch.nn.Sequential.html)\n",
    "is an ordered container of modules. The data is passed through all the\n",
    "modules in the same order as defined. You can use sequential containers\n",
    "to put together a quick network like `seq_modules`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "logits = tensor([[-0.1634, -0.2148, -0.1776,  0.1898,  0.0383, -0.4566,  0.1167,  0.0232,\n",
      "         -0.2577,  0.0128],\n",
      "        [-0.1093, -0.2548, -0.1627,  0.1839,  0.0257, -0.3445,  0.1496, -0.0543,\n",
      "         -0.2480,  0.1145],\n",
      "        [ 0.0443, -0.1779, -0.2898,  0.2120,  0.1289, -0.4315,  0.1135,  0.0276,\n",
      "         -0.4234,  0.2878]], grad_fn=<AddmmBackward0>)\n",
      "shape = torch.Size([3, 10])\n"
     ]
    }
   ],
   "source": [
    "seq_modules = nn.Sequential(\n",
    "    flatten,\n",
    "    layer1,\n",
    "    nn.ReLU(),\n",
    "    nn.Linear(20, 10)\n",
    ")\n",
    "input_image = torch.rand(3,28,28)\n",
    "logits = seq_modules(input_image)\n",
    "print('logits =', logits)\n",
    "print('shape =', logits.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "nn.Softmax\n",
    "==========\n",
    "\n",
    "The last linear layer of the neural network returns [logits]{.title-ref}\n",
    "- raw values in \\[-infty, infty\\] - which are passed to the\n",
    "[nn.Softmax](https://pytorch.org/docs/stable/generated/torch.nn.Softmax.html)\n",
    "module. The logits are scaled to values \\[0, 1\\] representing the\n",
    "model\\'s predicted probabilities for each class. `dim` parameter\n",
    "indicates the dimension along which the values must sum to 1.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "pred_probab = tensor([[0.0912, 0.0867, 0.0900, 0.1299, 0.1116, 0.0681, 0.1207, 0.1100, 0.0830,\n",
      "         0.1088],\n",
      "        [0.0947, 0.0819, 0.0898, 0.1269, 0.1084, 0.0748, 0.1227, 0.1000, 0.0824,\n",
      "         0.1184],\n",
      "        [0.1068, 0.0855, 0.0764, 0.1263, 0.1162, 0.0663, 0.1144, 0.1050, 0.0669,\n",
      "         0.1362]], grad_fn=<SoftmaxBackward0>)\n"
     ]
    }
   ],
   "source": [
    "softmax = nn.Softmax(dim=1)\n",
    "pred_probab = softmax(logits)\n",
    "print('pred_probab =', pred_probab)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Model Parameters\n",
    "================\n",
    "\n",
    "Many layers inside a neural network are *parameterized*, i.e. have\n",
    "associated weights and biases that are optimized during training.\n",
    "Subclassing `nn.Module` automatically tracks all fields defined inside\n",
    "your model object, and makes all parameters accessible using your\n",
    "model\\'s `parameters()` or `named_parameters()` methods.\n",
    "\n",
    "In this example, we iterate over each parameter, and print its size and\n",
    "a preview of its values.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Model structure: NeuralNetwork(\n",
      "  (flatten): Flatten(start_dim=1, end_dim=-1)\n",
      "  (linear_relu_stack): Sequential(\n",
      "    (0): Linear(in_features=784, out_features=512, bias=True)\n",
      "    (1): ReLU()\n",
      "    (2): Linear(in_features=512, out_features=512, bias=True)\n",
      "    (3): ReLU()\n",
      "    (4): Linear(in_features=512, out_features=10, bias=True)\n",
      "  )\n",
      ")\n",
      "\n",
      "\n",
      "Layer: linear_relu_stack.0.weight | Size: torch.Size([512, 784]) | Values : tensor([[ 0.0030, -0.0318, -0.0175,  ..., -0.0318, -0.0111,  0.0197],\n",
      "        [ 0.0178, -0.0338,  0.0126,  ...,  0.0280, -0.0200, -0.0107]],\n",
      "       grad_fn=<SliceBackward0>) \n",
      "\n",
      "Layer: linear_relu_stack.0.bias | Size: torch.Size([512]) | Values : tensor([ 0.0249, -0.0117], grad_fn=<SliceBackward0>) \n",
      "\n",
      "Layer: linear_relu_stack.2.weight | Size: torch.Size([512, 512]) | Values : tensor([[ 0.0213, -0.0153, -0.0248,  ..., -0.0278,  0.0052, -0.0382],\n",
      "        [-0.0157, -0.0073,  0.0074,  ...,  0.0012,  0.0301, -0.0110]],\n",
      "       grad_fn=<SliceBackward0>) \n",
      "\n",
      "Layer: linear_relu_stack.2.bias | Size: torch.Size([512]) | Values : tensor([-0.0279,  0.0057], grad_fn=<SliceBackward0>) \n",
      "\n",
      "Layer: linear_relu_stack.4.weight | Size: torch.Size([10, 512]) | Values : tensor([[ 0.0078,  0.0148, -0.0237,  ..., -0.0089,  0.0228, -0.0284],\n",
      "        [ 0.0023,  0.0271,  0.0416,  ..., -0.0163, -0.0248,  0.0026]],\n",
      "       grad_fn=<SliceBackward0>) \n",
      "\n",
      "Layer: linear_relu_stack.4.bias | Size: torch.Size([10]) | Values : tensor([0.0230, 0.0033], grad_fn=<SliceBackward0>) \n",
      "\n"
     ]
    }
   ],
   "source": [
    "print(f\"Model structure: {model}\\n\\n\")\n",
    "\n",
    "for name, param in model.named_parameters():\n",
    "    print(f\"Layer: {name} | Size: {param.size()} | Values : {param[:2]} \\n\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "------------------------------------------------------------------------\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Further Reading\n",
    "===============\n",
    "\n",
    "-   [torch.nn API](https://pytorch.org/docs/stable/nn.html)\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.15"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
