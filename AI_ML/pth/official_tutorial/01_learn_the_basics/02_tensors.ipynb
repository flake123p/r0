{
  "cells": [
    {
      "cell_type": "code",
      "execution_count": 1,
      "metadata": {
        "id": "kajtkXn0NS-K"
      },
      "outputs": [],
      "source": [
        "# For tips on running notebooks in Google Colab, see\n",
        "# https://pytorch.org/tutorials/beginner/colab\n",
        "%matplotlib inline"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "EOXjiw-5NS-N"
      },
      "source": [
        "[Learn the Basics](intro.html) \\|\\|\n",
        "[Quickstart](quickstart_tutorial.html) \\|\\| **Tensors** \\|\\| [Datasets &\n",
        "DataLoaders](data_tutorial.html) \\|\\|\n",
        "[Transforms](transforms_tutorial.html) \\|\\| [Build\n",
        "Model](buildmodel_tutorial.html) \\|\\|\n",
        "[Autograd](autogradqs_tutorial.html) \\|\\|\n",
        "[Optimization](optimization_tutorial.html) \\|\\| [Save & Load\n",
        "Model](saveloadrun_tutorial.html)\n",
        "\n",
        "Tensors\n",
        "=======\n",
        "\n",
        "Tensors are a specialized data structure that are very similar to arrays\n",
        "and matrices. In PyTorch, we use tensors to encode the inputs and\n",
        "outputs of a model, as well as the model's parameters.\n",
        "\n",
        "Tensors are similar to [NumPy's](https://numpy.org/) ndarrays, except\n",
        "that tensors can run on GPUs or other hardware accelerators. In fact,\n",
        "tensors and NumPy arrays can often share the same underlying memory,\n",
        "eliminating the need to copy data (see\n",
        "`bridge-to-np-label`{.interpreted-text role=\"ref\"}). Tensors are also\n",
        "optimized for automatic differentiation (we\\'ll see more about that\n",
        "later in the [Autograd](autogradqs_tutorial.html) section). If you're\n",
        "familiar with ndarrays, you'll be right at home with the Tensor API. If\n",
        "not, follow along!\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 2,
      "metadata": {
        "id": "tqPNMUbLNS-O"
      },
      "outputs": [],
      "source": [
        "import torch\n",
        "import numpy as np"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "DPFaiEwVNS-P"
      },
      "source": [
        "Initializing a Tensor\n",
        "=====================\n",
        "\n",
        "Tensors can be initialized in various ways. Take a look at the following\n",
        "examples:\n",
        "\n",
        "**Directly from data**\n",
        "\n",
        "Tensors can be created directly from data. The data type is\n",
        "automatically inferred.\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 3,
      "metadata": {
        "id": "lC6Z1BaYNS-P"
      },
      "outputs": [],
      "source": [
        "data = [[1, 2],[3, 4]]\n",
        "x_data = torch.tensor(data)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "Rt-bgIpsNS-P"
      },
      "source": [
        "**From a NumPy array**\n",
        "\n",
        "Tensors can be created from NumPy arrays (and vice versa - see\n",
        "`bridge-to-np-label`{.interpreted-text role=\"ref\"}).\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 4,
      "metadata": {
        "id": "Wg5UDtgENS-P"
      },
      "outputs": [],
      "source": [
        "np_array = np.array(data)\n",
        "x_np = torch.from_numpy(np_array)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "05UwYI5GNS-Q"
      },
      "source": [
        "**From another tensor:**\n",
        "\n",
        "The new tensor retains the properties (shape, datatype) of the argument\n",
        "tensor, unless explicitly overridden.\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 5,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "0PyDKu26NS-Q",
        "outputId": "15a7b502-5680-43f3-b0a7-634e92f1e4c9"
      },
      "outputs": [
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "Ones Tensor: \n",
            " tensor([[1, 1],\n",
            "        [1, 1]]) \n",
            "\n",
            "Random Tensor: \n",
            " tensor([[0.3196, 0.9888],\n",
            "        [0.2266, 0.5719]]) \n",
            "\n"
          ]
        }
      ],
      "source": [
        "x_ones = torch.ones_like(x_data) # retains the properties of x_data\n",
        "print(f\"Ones Tensor: \\n {x_ones} \\n\")\n",
        "\n",
        "x_rand = torch.rand_like(x_data, dtype=torch.float) # overrides the datatype of x_data\n",
        "print(f\"Random Tensor: \\n {x_rand} \\n\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "BnrheD1SNS-Q"
      },
      "source": [
        "**With random or constant values:**\n",
        "\n",
        "`shape` is a tuple of tensor dimensions. In the functions below, it\n",
        "determines the dimensionality of the output tensor.\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 6,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "qErjvaSENS-R",
        "outputId": "689b8060-681e-4537-ed0e-aae6b89c928b"
      },
      "outputs": [
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "Random Tensor: \n",
            " tensor([[0.4481, 0.3366, 0.4412],\n",
            "        [0.1082, 0.2983, 0.0197]]) \n",
            "\n",
            "Ones Tensor: \n",
            " tensor([[1., 1., 1.],\n",
            "        [1., 1., 1.]]) \n",
            "\n",
            "Zeros Tensor: \n",
            " tensor([[0., 0., 0.],\n",
            "        [0., 0., 0.]])\n"
          ]
        }
      ],
      "source": [
        "shape = (2,3,)\n",
        "rand_tensor = torch.rand(shape)\n",
        "ones_tensor = torch.ones(shape)\n",
        "zeros_tensor = torch.zeros(shape)\n",
        "\n",
        "print(f\"Random Tensor: \\n {rand_tensor} \\n\")\n",
        "print(f\"Ones Tensor: \\n {ones_tensor} \\n\")\n",
        "print(f\"Zeros Tensor: \\n {zeros_tensor}\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "AB0s0M1zNS-R"
      },
      "source": [
        "------------------------------------------------------------------------\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "mPBKBSi2NS-R"
      },
      "source": [
        "Attributes of a Tensor\n",
        "======================\n",
        "\n",
        "Tensor attributes describe their shape, datatype, and the device on\n",
        "which they are stored.\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 7,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "ABbFac6cNS-R",
        "outputId": "af855f4b-224b-4c2e-fa6c-ade1a2d95112"
      },
      "outputs": [
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "Shape of tensor: torch.Size([3, 4])\n",
            "Datatype of tensor: torch.float32\n",
            "Device tensor is stored on: cpu\n"
          ]
        }
      ],
      "source": [
        "tensor = torch.rand(3,4)\n",
        "\n",
        "print(f\"Shape of tensor: {tensor.shape}\")\n",
        "print(f\"Datatype of tensor: {tensor.dtype}\")\n",
        "print(f\"Device tensor is stored on: {tensor.device}\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "2LASxSgjNS-R"
      },
      "source": [
        "------------------------------------------------------------------------\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "pPXN5BKYNS-R"
      },
      "source": [
        "Operations on Tensors\n",
        "=====================\n",
        "\n",
        "Over 100 tensor operations, including arithmetic, linear algebra, matrix\n",
        "manipulation (transposing, indexing, slicing), sampling and more are\n",
        "comprehensively described\n",
        "[here](https://pytorch.org/docs/stable/torch.html).\n",
        "\n",
        "Each of these operations can be run on the GPU (at typically higher\n",
        "speeds than on a CPU). If you're using Colab, allocate a GPU by going to\n",
        "Runtime \\> Change runtime type \\> GPU.\n",
        "\n",
        "By default, tensors are created on the CPU. We need to explicitly move\n",
        "tensors to the GPU using `.to` method (after checking for GPU\n",
        "availability). Keep in mind that copying large tensors across devices\n",
        "can be expensive in terms of time and memory!\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 8,
      "metadata": {
        "id": "oqhfis3LNS-R"
      },
      "outputs": [],
      "source": [
        "# We move our tensor to the GPU if available\n",
        "if torch.cuda.is_available():\n",
        "    tensor = tensor.to(\"cuda\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "-A4_8cgVNS-R"
      },
      "source": [
        "Try out some of the operations from the list. If you\\'re familiar with\n",
        "the NumPy API, you\\'ll find the Tensor API a breeze to use.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "NmPMa2mlNS-R"
      },
      "source": [
        "**Standard numpy-like indexing and slicing:**\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 9,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "PH7KwkuvNS-R",
        "outputId": "3731d7ec-58f3-4fdd-a3bf-cb27b8f06354"
      },
      "outputs": [
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "First row: tensor([1., 1., 1., 1.])\n",
            "First column: tensor([1., 1., 1., 1.])\n",
            "Last column: tensor([1., 1., 1., 1.])\n",
            "tensor([[1., 0., 1., 1.],\n",
            "        [1., 0., 1., 1.],\n",
            "        [1., 0., 1., 1.],\n",
            "        [1., 0., 1., 1.]])\n"
          ]
        }
      ],
      "source": [
        "tensor = torch.ones(4, 4)\n",
        "print(f\"First row: {tensor[0]}\")\n",
        "print(f\"First column: {tensor[:, 0]}\")\n",
        "print(f\"Last column: {tensor[..., -1]}\")\n",
        "tensor[:,1] = 0\n",
        "print(tensor)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "qUH1VQ7eNS-S"
      },
      "source": [
        "**Joining tensors** You can use `torch.cat` to concatenate a sequence of\n",
        "tensors along a given dimension. See also\n",
        "[torch.stack](https://pytorch.org/docs/stable/generated/torch.stack.html),\n",
        "another tensor joining operator that is subtly different from\n",
        "`torch.cat`.\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 10,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "StX2BRnyNS-S",
        "outputId": "774d2f2f-ff02-4432-a93c-d1d4e414c02d"
      },
      "outputs": [
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "tensor([[1., 0., 1., 1., 1., 0., 1., 1., 1., 0., 1., 1.],\n",
            "        [1., 0., 1., 1., 1., 0., 1., 1., 1., 0., 1., 1.],\n",
            "        [1., 0., 1., 1., 1., 0., 1., 1., 1., 0., 1., 1.],\n",
            "        [1., 0., 1., 1., 1., 0., 1., 1., 1., 0., 1., 1.]])\n"
          ]
        }
      ],
      "source": [
        "t1 = torch.cat([tensor, tensor, tensor], dim=1)\n",
        "print(t1)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "RQ9s9wz7NS-S"
      },
      "source": [
        "**Arithmetic operations**\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 11,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "-W5q-euyNS-S",
        "outputId": "1c16a9e3-8d91-4a05-dd56-1828d2fbfeb1"
      },
      "outputs": [
        {
          "data": {
            "text/plain": [
              "tensor([[1., 0., 1., 1.],\n",
              "        [1., 0., 1., 1.],\n",
              "        [1., 0., 1., 1.],\n",
              "        [1., 0., 1., 1.]])"
            ]
          },
          "execution_count": 11,
          "metadata": {},
          "output_type": "execute_result"
        }
      ],
      "source": [
        "# This computes the matrix multiplication between two tensors. y1, y2, y3 will have the same value\n",
        "# ``tensor.T`` returns the transpose of a tensor\n",
        "y1 = tensor @ tensor.T\n",
        "y2 = tensor.matmul(tensor.T)\n",
        "\n",
        "y3 = torch.rand_like(y1)\n",
        "torch.matmul(tensor, tensor.T, out=y3)\n",
        "\n",
        "\n",
        "# This computes the element-wise product. z1, z2, z3 will have the same value\n",
        "z1 = tensor * tensor\n",
        "z2 = tensor.mul(tensor)\n",
        "\n",
        "z3 = torch.rand_like(tensor)\n",
        "torch.mul(tensor, tensor, out=z3)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "4z1kMOvMNS-T"
      },
      "source": [
        "**Single-element tensors** If you have a one-element tensor, for example\n",
        "by aggregating all values of a tensor into one value, you can convert it\n",
        "to a Python numerical value using `item()`:\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 12,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "wV0pFcutNS-T",
        "outputId": "ab3c1d28-3f11-471c-87b1-cfdaf0588f56"
      },
      "outputs": [
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "12.0 <class 'float'>\n"
          ]
        }
      ],
      "source": [
        "agg = tensor.sum()\n",
        "agg_item = agg.item()\n",
        "print(agg_item, type(agg_item))"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "tFQDWfkLNS-T"
      },
      "source": [
        "**In-place operations** Operations that store the result into the\n",
        "operand are called in-place. They are denoted by a `_` suffix. For\n",
        "example: `x.copy_(y)`, `x.t_()`, will change `x`.\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 13,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "o2UZm1VSNS-T",
        "outputId": "208efbfb-9f44-42e7-949e-f7264847a5a3"
      },
      "outputs": [
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "tensor([[1., 0., 1., 1.],\n",
            "        [1., 0., 1., 1.],\n",
            "        [1., 0., 1., 1.],\n",
            "        [1., 0., 1., 1.]]) \n",
            "\n",
            "tensor([[6., 5., 6., 6.],\n",
            "        [6., 5., 6., 6.],\n",
            "        [6., 5., 6., 6.],\n",
            "        [6., 5., 6., 6.]])\n"
          ]
        }
      ],
      "source": [
        "print(f\"{tensor} \\n\")\n",
        "tensor.add_(5)\n",
        "print(tensor)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "AUMCBmBXNS-T"
      },
      "source": [
        "<div style=\"background-color: #225555; color: #000; font-weight: 700; padding-left: 10px; padding-top: 5px; padding-bottom: 5px\"><strong>NOTE:</strong></div>\n",
        "<div style=\"background-color: #225522; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; padding-right: 10px\">\n",
        "<p>In-place operations save some memory, but can be problematic when computing derivatives because of an immediate lossof history. Hence, their use is discouraged.</p>\n",
        "</div>\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "16NrLDtnNS-T"
      },
      "source": [
        "------------------------------------------------------------------------\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "RWK6mW7eNS-T"
      },
      "source": [
        "Bridge with NumPy {#bridge-to-np-label}\n",
        "=================\n",
        "\n",
        "Tensors on the CPU and NumPy arrays can share their underlying memory\n",
        "locations, and changing one will change the other.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "szlrqLXlNS-T"
      },
      "source": [
        "Tensor to NumPy array\n",
        "=====================\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 14,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "L7QgTYHwNS-U",
        "outputId": "d7ba0778-29ad-4d88-a581-e211dd388c90"
      },
      "outputs": [
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "t: tensor([1., 1., 1., 1., 1.])\n",
            "n: [1. 1. 1. 1. 1.]\n"
          ]
        }
      ],
      "source": [
        "t = torch.ones(5)\n",
        "print(f\"t: {t}\")\n",
        "n = t.numpy()\n",
        "print(f\"n: {n}\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "BAbWdUHINS-U"
      },
      "source": [
        "A change in the tensor reflects in the NumPy array.\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 15,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "m4mkgv-0NS-U",
        "outputId": "0a65e5c5-ac74-455d-fc5b-8d2b07967848"
      },
      "outputs": [
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "t: tensor([2., 2., 2., 2., 2.])\n",
            "n: [2. 2. 2. 2. 2.]\n"
          ]
        }
      ],
      "source": [
        "t.add_(1)\n",
        "print(f\"t: {t}\")\n",
        "print(f\"n: {n}\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "A1Da9YqhNS-U"
      },
      "source": [
        "NumPy array to Tensor\n",
        "=====================\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 16,
      "metadata": {
        "id": "rx0of8exNS-U"
      },
      "outputs": [],
      "source": [
        "n = np.ones(5)\n",
        "t = torch.from_numpy(n)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "-Wztm5qoNS-U"
      },
      "source": [
        "Changes in the NumPy array reflects in the tensor.\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 17,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "6yIqmYhFNS-U",
        "outputId": "e9805f79-cee6-4b1d-f50f-b16e751de8c2"
      },
      "outputs": [
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "t: tensor([2., 2., 2., 2., 2.], dtype=torch.float64)\n",
            "n: [2. 2. 2. 2. 2.]\n"
          ]
        }
      ],
      "source": [
        "np.add(n, 1, out=n)\n",
        "print(f\"t: {t}\")\n",
        "print(f\"n: {n}\")"
      ]
    }
  ],
  "metadata": {
    "colab": {
      "provenance": []
    },
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.9.15"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}
