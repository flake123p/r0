{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# For tips on running notebooks in Google Colab, see\n",
    "# https://pytorch.org/tutorials/beginner/colab\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Saving and loading multiple models in one file using PyTorch\n",
    "============================================================\n",
    "\n",
    "Saving and loading multiple models can be helpful for reusing models\n",
    "that you have previously trained.\n",
    "\n",
    "Introduction\n",
    "------------\n",
    "\n",
    "When saving a model comprised of multiple `torch.nn.Modules`, such as a\n",
    "GAN, a sequence-to-sequence model, or an ensemble of models, you must\n",
    "save a dictionary of each model's state\\_dict and corresponding\n",
    "optimizer. You can also save any other items that may aid you in\n",
    "resuming training by simply appending them to the dictionary. To load\n",
    "the models, first initialize the models and optimizers, then load the\n",
    "dictionary locally using `torch.load()`. From here, you can easily\n",
    "access the saved items by simply querying the dictionary as you would\n",
    "expect. In this recipe, we will demonstrate how to save multiple models\n",
    "to one file using PyTorch.\n",
    "\n",
    "Setup\n",
    "-----\n",
    "\n",
    "Before we begin, we need to install `torch` if it isn't already\n",
    "available.\n",
    "\n",
    "``` {.sourceCode .sh}\n",
    "pip install torch\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Steps\n",
    "=====\n",
    "\n",
    "1.  Import all necessary libraries for loading our data\n",
    "2.  Define and initialize the neural network\n",
    "3.  Initialize the optimizer\n",
    "4.  Save multiple models\n",
    "5.  Load multiple models\n",
    "\n",
    "1. Import necessary libraries for loading our data\n",
    "--------------------------------------------------\n",
    "\n",
    "For this recipe, we will use `torch` and its subsidiaries `torch.nn` and\n",
    "`torch.optim`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import torch\n",
    "import torch.nn as nn\n",
    "import torch.optim as optim"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2. Define and initialize the neural network\n",
    "===========================================\n",
    "\n",
    "For sake of example, we will create a neural network for training\n",
    "images. To learn more see the Defining a Neural Network recipe. Build\n",
    "two variables for the models to eventually save.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "class Net(nn.Module):\n",
    "    def __init__(self):\n",
    "        super(Net, self).__init__()\n",
    "        self.conv1 = nn.Conv2d(3, 6, 5)\n",
    "        self.pool = nn.MaxPool2d(2, 2)\n",
    "        self.conv2 = nn.Conv2d(6, 16, 5)\n",
    "        self.fc1 = nn.Linear(16 * 5 * 5, 120)\n",
    "        self.fc2 = nn.Linear(120, 84)\n",
    "        self.fc3 = nn.Linear(84, 10)\n",
    "\n",
    "    def forward(self, x):\n",
    "        x = self.pool(F.relu(self.conv1(x)))\n",
    "        x = self.pool(F.relu(self.conv2(x)))\n",
    "        x = x.view(-1, 16 * 5 * 5)\n",
    "        x = F.relu(self.fc1(x))\n",
    "        x = F.relu(self.fc2(x))\n",
    "        x = self.fc3(x)\n",
    "        return x\n",
    "\n",
    "netA = Net()\n",
    "netB = Net()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "3. Initialize the optimizer\n",
    "===========================\n",
    "\n",
    "We will use SGD with momentum to build an optimizer for each model we\n",
    "created.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "optimizerA = optim.SGD(netA.parameters(), lr=0.001, momentum=0.9)\n",
    "optimizerB = optim.SGD(netB.parameters(), lr=0.001, momentum=0.9)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "4. Save multiple models\n",
    "=======================\n",
    "\n",
    "Collect all relevant information and build your dictionary.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Specify a path to save to\n",
    "PATH = \"model_local.pt\"\n",
    "\n",
    "torch.save({\n",
    "            'modelA_state_dict': netA.state_dict(),\n",
    "            'modelB_state_dict': netB.state_dict(),\n",
    "            'optimizerA_state_dict': optimizerA.state_dict(),\n",
    "            'optimizerB_state_dict': optimizerB.state_dict(),\n",
    "            }, PATH)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "4. Load multiple models\n",
    "=======================\n",
    "\n",
    "Remember to first initialize the models and optimizers, then load the\n",
    "dictionary locally.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "modelA = Net()\n",
    "modelB = Net()\n",
    "optimModelA = optim.SGD(modelA.parameters(), lr=0.001, momentum=0.9)\n",
    "optimModelB = optim.SGD(modelB.parameters(), lr=0.001, momentum=0.9)\n",
    "\n",
    "checkpoint = torch.load(PATH)\n",
    "modelA.load_state_dict(checkpoint['modelA_state_dict'])\n",
    "modelB.load_state_dict(checkpoint['modelB_state_dict'])\n",
    "optimizerA.load_state_dict(checkpoint['optimizerA_state_dict'])\n",
    "optimizerB.load_state_dict(checkpoint['optimizerB_state_dict'])\n",
    "\n",
    "modelA.eval()\n",
    "modelB.eval()\n",
    "# - or -\n",
    "modelA.train()\n",
    "modelB.train()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You must call `model.eval()` to set dropout and batch normalization\n",
    "layers to evaluation mode before running inference. Failing to do this\n",
    "will yield inconsistent inference results.\n",
    "\n",
    "If you wish to resuming training, call `model.train()` to ensure these\n",
    "layers are in training mode.\n",
    "\n",
    "Congratulations! You have successfully saved and loaded multiple models\n",
    "in PyTorch.\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.14"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
