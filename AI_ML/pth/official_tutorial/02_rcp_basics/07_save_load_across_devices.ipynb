{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# For tips on running notebooks in Google Colab, see\n",
    "# https://pytorch.org/tutorials/beginner/colab\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Saving and loading models across devices in PyTorch\n",
    "===================================================\n",
    "\n",
    "There may be instances where you want to save and load your neural\n",
    "networks across different devices.\n",
    "\n",
    "Introduction\n",
    "------------\n",
    "\n",
    "Saving and loading models across devices is relatively straightforward\n",
    "using PyTorch. In this recipe, we will experiment with saving and\n",
    "loading models across CPUs and GPUs.\n",
    "\n",
    "Setup\n",
    "-----\n",
    "\n",
    "In order for every code block to run properly in this recipe, you must\n",
    "first change the runtime to \"GPU\" or higher. Once you do, we need to\n",
    "install `torch` if it isn't already available.\n",
    "\n",
    "``` {.sourceCode .sh}\n",
    "pip install torch\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Steps\n",
    "=====\n",
    "\n",
    "1.  Import all necessary libraries for loading our data\n",
    "2.  Define and initialize the neural network\n",
    "3.  Save on a GPU, load on a CPU\n",
    "4.  Save on a GPU, load on a GPU\n",
    "5.  Save on a CPU, load on a GPU\n",
    "6.  Saving and loading `DataParallel` models\n",
    "\n",
    "1. Import necessary libraries for loading our data\n",
    "--------------------------------------------------\n",
    "\n",
    "For this recipe, we will use `torch` and its subsidiaries `torch.nn` and\n",
    "`torch.optim`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import torch\n",
    "import torch.nn as nn\n",
    "import torch.optim as optim"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2. Define and initialize the neural network\n",
    "===========================================\n",
    "\n",
    "For sake of example, we will create a neural network for training\n",
    "images. To learn more see the Defining a Neural Network recipe.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "class Net(nn.Module):\n",
    "    def __init__(self):\n",
    "        super(Net, self).__init__()\n",
    "        self.conv1 = nn.Conv2d(3, 6, 5)\n",
    "        self.pool = nn.MaxPool2d(2, 2)\n",
    "        self.conv2 = nn.Conv2d(6, 16, 5)\n",
    "        self.fc1 = nn.Linear(16 * 5 * 5, 120)\n",
    "        self.fc2 = nn.Linear(120, 84)\n",
    "        self.fc3 = nn.Linear(84, 10)\n",
    "\n",
    "    def forward(self, x):\n",
    "        x = self.pool(F.relu(self.conv1(x)))\n",
    "        x = self.pool(F.relu(self.conv2(x)))\n",
    "        x = x.view(-1, 16 * 5 * 5)\n",
    "        x = F.relu(self.fc1(x))\n",
    "        x = F.relu(self.fc2(x))\n",
    "        x = self.fc3(x)\n",
    "        return x\n",
    "\n",
    "net = Net()\n",
    "print(net)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "3. Save on GPU, Load on CPU\n",
    "===========================\n",
    "\n",
    "When loading a model on a CPU that was trained with a GPU, pass\n",
    "`torch.device('cpu')` to the `map_location` argument in the\n",
    "`torch.load()` function.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Specify a path to save to\n",
    "PATH = \"model.pt\"\n",
    "\n",
    "# Save\n",
    "torch.save(net.state_dict(), PATH)\n",
    "\n",
    "# Load\n",
    "device = torch.device('cpu')\n",
    "model = Net()\n",
    "model.load_state_dict(torch.load(PATH, map_location=device))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, the storages underlying the tensors are dynamically\n",
    "remapped to the CPU device using the `map_location` argument.\n",
    "\n",
    "4. Save on GPU, Load on GPU\n",
    "===========================\n",
    "\n",
    "When loading a model on a GPU that was trained and saved on GPU, simply\n",
    "convert the initialized model to a CUDA optimized model using\n",
    "`model.to(torch.device('cuda'))`.\n",
    "\n",
    "Be sure to use the `.to(torch.device('cuda'))` function on all model\n",
    "inputs to prepare the data for the model.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Save\n",
    "torch.save(net.state_dict(), PATH)\n",
    "\n",
    "# Load\n",
    "device = torch.device(\"cuda\")\n",
    "model = Net()\n",
    "model.load_state_dict(torch.load(PATH))\n",
    "model.to(device)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that calling `my_tensor.to(device)` returns a new copy of\n",
    "`my_tensor` on GPU. It does NOT overwrite `my_tensor`. Therefore,\n",
    "remember to manually overwrite tensors:\n",
    "`my_tensor = my_tensor.to(torch.device('cuda'))`.\n",
    "\n",
    "5. Save on CPU, Load on GPU\n",
    "===========================\n",
    "\n",
    "When loading a model on a GPU that was trained and saved on CPU, set the\n",
    "`map_location` argument in the `torch.load()` function to\n",
    "`cuda:device_id`. This loads the model to a given GPU device.\n",
    "\n",
    "Be sure to call `model.to(torch.device('cuda'))` to convert the model's\n",
    "parameter tensors to CUDA tensors.\n",
    "\n",
    "Finally, also be sure to use the `.to(torch.device('cuda'))` function on\n",
    "all model inputs to prepare the data for the CUDA optimized model.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Save\n",
    "torch.save(net.state_dict(), PATH)\n",
    "\n",
    "# Load\n",
    "device = torch.device(\"cuda\")\n",
    "model = Net()\n",
    "# Choose whatever GPU device number you want\n",
    "model.load_state_dict(torch.load(PATH, map_location=\"cuda:0\"))\n",
    "# Make sure to call input = input.to(device) on any input tensors that you feed to the model\n",
    "model.to(device)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "6. Saving `torch.nn.DataParallel` Models\n",
    "========================================\n",
    "\n",
    "`torch.nn.DataParallel` is a model wrapper that enables parallel GPU\n",
    "utilization.\n",
    "\n",
    "To save a `DataParallel` model generically, save the\n",
    "`model.module.state_dict()`. This way, you have the flexibility to load\n",
    "the model any way you want to any device you want.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Save\n",
    "torch.save(net.module.state_dict(), PATH)\n",
    "\n",
    "# Load to whatever device you want"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Congratulations! You have successfully saved and loaded models across\n",
    "devices in PyTorch.\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.14"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
