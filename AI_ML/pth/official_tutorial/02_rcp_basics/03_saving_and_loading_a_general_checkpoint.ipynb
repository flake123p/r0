{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# For tips on running notebooks in Google Colab, see\n",
    "# https://pytorch.org/tutorials/beginner/colab\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Saving and loading a general checkpoint in PyTorch\n",
    "==================================================\n",
    "\n",
    "Saving and loading a general checkpoint model for inference or resuming\n",
    "training can be helpful for picking up where you last left off. When\n",
    "saving a general checkpoint, you must save more than just the model's\n",
    "state\\_dict. It is important to also save the optimizer's state\\_dict,\n",
    "as this contains buffers and parameters that are updated as the model\n",
    "trains. Other items that you may want to save are the epoch you left off\n",
    "on, the latest recorded training loss, external `torch.nn.Embedding`\n",
    "layers, and more, based on your own algorithm.\n",
    "\n",
    "Introduction\n",
    "------------\n",
    "\n",
    "To save multiple checkpoints, you must organize them in a dictionary and\n",
    "use `torch.save()` to serialize the dictionary. A common PyTorch\n",
    "convention is to save these checkpoints using the `.tar` file extension.\n",
    "To load the items, first initialize the model and optimizer, then load\n",
    "the dictionary locally using torch.load(). From here, you can easily\n",
    "access the saved items by simply querying the dictionary as you would\n",
    "expect.\n",
    "\n",
    "In this recipe, we will explore how to save and load multiple\n",
    "checkpoints.\n",
    "\n",
    "Setup\n",
    "-----\n",
    "\n",
    "Before we begin, we need to install `torch` if it isn't already\n",
    "available.\n",
    "\n",
    "    pip install torch\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Steps\n",
    "=====\n",
    "\n",
    "1.  Import all necessary libraries for loading our data\n",
    "2.  Define and initialize the neural network\n",
    "3.  Initialize the optimizer\n",
    "4.  Save the general checkpoint\n",
    "5.  Load the general checkpoint\n",
    "\n",
    "1. Import necessary libraries for loading our data\n",
    "--------------------------------------------------\n",
    "\n",
    "For this recipe, we will use `torch` and its subsidiaries `torch.nn` and\n",
    "`torch.optim`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import torch\n",
    "import torch.nn as nn\n",
    "import torch.optim as optim"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2. Define and initialize the neural network\n",
    "===========================================\n",
    "\n",
    "For sake of example, we will create a neural network for training\n",
    "images. To learn more see the Defining a Neural Network recipe.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Net(\n",
      "  (conv1): Conv2d(3, 6, kernel_size=(5, 5), stride=(1, 1))\n",
      "  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)\n",
      "  (conv2): Conv2d(6, 16, kernel_size=(5, 5), stride=(1, 1))\n",
      "  (fc1): Linear(in_features=400, out_features=120, bias=True)\n",
      "  (fc2): Linear(in_features=120, out_features=84, bias=True)\n",
      "  (fc3): Linear(in_features=84, out_features=10, bias=True)\n",
      ")\n"
     ]
    }
   ],
   "source": [
    "class Net(nn.Module):\n",
    "    def __init__(self):\n",
    "        super(Net, self).__init__()\n",
    "        self.conv1 = nn.Conv2d(3, 6, 5)\n",
    "        self.pool = nn.MaxPool2d(2, 2)\n",
    "        self.conv2 = nn.Conv2d(6, 16, 5)\n",
    "        self.fc1 = nn.Linear(16 * 5 * 5, 120)\n",
    "        self.fc2 = nn.Linear(120, 84)\n",
    "        self.fc3 = nn.Linear(84, 10)\n",
    "\n",
    "    def forward(self, x):\n",
    "        x = self.pool(F.relu(self.conv1(x)))\n",
    "        x = self.pool(F.relu(self.conv2(x)))\n",
    "        x = x.view(-1, 16 * 5 * 5)\n",
    "        x = F.relu(self.fc1(x))\n",
    "        x = F.relu(self.fc2(x))\n",
    "        x = self.fc3(x)\n",
    "        return x\n",
    "\n",
    "net = Net()\n",
    "print(net)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "3. Initialize the optimizer\n",
    "===========================\n",
    "\n",
    "We will use SGD with momentum.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "4. Save the general checkpoint\n",
    "==============================\n",
    "\n",
    "Collect all relevant information and build your dictionary.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Additional information\n",
    "EPOCH = 5\n",
    "PATH = \"model_local.pt\"\n",
    "LOSS = 0.4\n",
    "\n",
    "torch.save({\n",
    "            'epoch': EPOCH,\n",
    "            'model_state_dict': net.state_dict(),\n",
    "            'optimizer_state_dict': optimizer.state_dict(),\n",
    "            'loss': LOSS,\n",
    "            }, PATH)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "5. Load the general checkpoint\n",
    "==============================\n",
    "\n",
    "Remember to first initialize the model and optimizer, then load the\n",
    "dictionary locally.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Net(\n",
       "  (conv1): Conv2d(3, 6, kernel_size=(5, 5), stride=(1, 1))\n",
       "  (pool): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)\n",
       "  (conv2): Conv2d(6, 16, kernel_size=(5, 5), stride=(1, 1))\n",
       "  (fc1): Linear(in_features=400, out_features=120, bias=True)\n",
       "  (fc2): Linear(in_features=120, out_features=84, bias=True)\n",
       "  (fc3): Linear(in_features=84, out_features=10, bias=True)\n",
       ")"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "model = Net()\n",
    "optimizer = optim.SGD(model.parameters(), lr=0.001, momentum=0.9)\n",
    "\n",
    "checkpoint = torch.load(PATH)\n",
    "model.load_state_dict(checkpoint['model_state_dict'])\n",
    "optimizer.load_state_dict(checkpoint['optimizer_state_dict'])\n",
    "epoch = checkpoint['epoch']\n",
    "loss = checkpoint['loss']\n",
    "\n",
    "model.eval()\n",
    "# - or -\n",
    "model.train()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You must call `model.eval()` to set dropout and batch normalization\n",
    "layers to evaluation mode before running inference. Failing to do this\n",
    "will yield inconsistent inference results.\n",
    "\n",
    "If you wish to resuming training, call `model.train()` to ensure these\n",
    "layers are in training mode.\n",
    "\n",
    "Congratulations! You have successfully saved and loaded a general\n",
    "checkpoint for inference and/or resuming training in PyTorch.\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.15"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
