#
# Source:
#   MNIST : https://github.com/pytorch/examples/blob/main/mnist/main.py
#
#   CIFAR10 : https://github.com/kuangliu/pytorch-cifar/tree/master
#

# Pytorch Installation

## conda create & activate py39
conda create -n py39 python=3.9
conda remove -n ENV_NAME --all

## Download Source
Github:
https://github.com/pytorch/pytorch#a-gpu-ready-tensor-library

```
git clone --recursive https://github.com/pytorch/pytorch
cd pytorch
# if you are updating an existing checkout
git submodule sync
git submodule update --init --recursive
```

## Prerequisites
Python 3.8 or later (for Linux, Python 3.8.1+ is needed)
A compiler that fully supports C++17, such as clang or gcc (especially for aarch64, gcc 9.4.0 or newer is required)

We highly recommend installing an Anaconda environment.

## Install Dependencies
**Common**
```
conda install cmake ninja

# Run this command from the PyTorch directory after cloning the source code using the “Get the PyTorch Source“ section below
pip install -r requirements.txt
```

**Linux**
```
conda install mkl mkl-include

# CUDA only: Add LAPACK support for the GPU if needed
conda install -c pytorch magma-cuda110  # or the magma-cuda* that matches your CUDA version from https://anaconda.org/pytorch/repo

# Flake: not now
# (optional) If using torch.compile with inductor/triton, install the matching version of triton
# Run from the pytorch directory after cloning
make triton
```

//20231115
conda install cudatoolkit
pip install mkl
conda install numpy ninja pyyaml mkl mkl-include setuptools cmake cffi typing_extensions future six requests dataclasses sympy
export LD_LIBRARY_PATH=/home/ai1/anaconda3/envs/py39/lib/
conda install -c conda-forge gcc=12.1.0
[CUDNN]https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html
[CUDNN]https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html

## Install & Build
```
(to torch root folder)
conda activate pth0

export CMAKE_PREFIX_PATH=${CONDA_PREFIX:-"$(dirname $(which conda))/../"}

//[20231116] add cuda flags
string(APPEND CMAKE_CUDA_FLAGS " -Xfatbin -compress-all --compiler-bindir=/usr/bin/g++")

//[20231116] use this one
USE_CUDA=1 DEBUG=1 python setup.py develop

//not this old one
CUDA_NVCC_EXECUTABLE="/usr/bin/nvcc -allow-unsupported-compiler" USE_CUDA=1 DEBUG=1 python setup.py develop

//not this old one
CUDA_NVCC_EXECUTABLE="/usr/bin/nvcc --compiler-bindir=/usr/bin/g++" USE_CUDA=1 DEBUG=1 python setup.py develop
```


## [NOT WORKING !!!] error: parameter packs not expanded with '...'
[] https://github.com/NVIDIA/nccl/issues/650
[CSDN] https://blog.csdn.net/u010442263/article/details/131366719
```
sudo apt install gcc-10
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-10 50
sudo update-alternatives --config gcc
```

```
sudo apt install g++-10
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-10 50
sudo update-alternatives --config g++
```

## [DO WORKING !!!]
https://github.com/NVlabs/instant-ngp/issues/119
```
$ sudo apt install gcc-10 g++-10
$ export CC=/usr/bin/gcc-10
$ export CXX=/usr/bin/g++-10
$ export CUDA_ROOT=/usr/lib/cuda
$ sudo ln -sf /usr/bin/gcc-10 $CUDA_ROOT/bin/gcc
$ sudo ln -sf /usr/bin/g++-10 $CUDA_ROOT/bin/g++
(Build Instant-NGP as described)

$ python setup.py develop
```

## version `GLIBCXX_3.4.30' not found
https://stackoverflow.com/questions/72540359/glibcxx-3-4-30-not-found-for-librosa-in-conda-virtual-environment-after-tryin
```
conda install -c conda-forge gcc=12.1.0
```
