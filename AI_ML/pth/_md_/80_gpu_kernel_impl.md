
Target Function: Kernel_MUL_FUNC_BROADCAST_F32()

```
_ZN2at6native18elementwise_kernelILi128ELi2EZNS0_15gpu_kernel_implINS0_13BinaryFunctorIfffNS0_15binary_internal10MulFunctorIfEEEEEEvRNS_18TensorIteratorBaseERKT_EUliE_EEviT1_

.
.
.

void at::native::elementwise_kernel
  <
   128, 
   2, 
   at::native::gpu_kernel_impl
     <
      at::native::BinaryFunctor< float, float, float, at::native::binary_internal::MulFunctor<float> >
     >
     (
      at::TensorIteratorBase&, 
      at::native::BinaryFunctor<float, float, float, at::native::binary_internal::MulFunctor<float> > const&
     )::{lambda(int)#1}
  >
  (
    int, 
    at::native::gpu_kernel_impl
      <
       at::native::BinaryFunctor<float, float, float, at::native::binary_internal::MulFunctor<float> > 
      >
      (
       at::TensorIteratorBase&, 
       at::native::BinaryFunctor<float, float, float, at::native::binary_internal::MulFunctor<float> > const&
      )::{lambda(int)#1}
  )
```

# 11
## 22

| Column 1           |            | Column 3 |                                                                                 |                                             |                  |
| ------------------ | ---------- | -------- | ------------------------------------------------------------------------------- | ------------------------------------------- | ---------------- |
| elementwise_kernel | __global__ | void     | template<int nt, int vt, typename func_t>                                       | (int N, func_t f)                           | CUDALoops.cuh    |
| gpu_kernel_impl    |            | void     | template <typename func_t>                                                      | (TensorIteratorBase& iter, const func_t& f) | Loops.cuh        |
| BinaryFunctor      |            | struct   | template <typename arg1_t, typename arg2_t, typename return_t, typename func_t> |                                             | Loops.cuh        |
| MulFunctor         |            | struct   | template <typename T>                                                           |                                             | BinaryInternal.h |


---
    [[[ elementwise_kernel() ]]]

__launch_bounds__
```=
max_threads_per_block : e.g.:128. Must smaller than 1024, if bigger than 1024 (CUDA_MAX_THREADS_PER_BLOCK), fallback to 256 (CUDA_THREADS_PER_BLOCK_FALLBACK)
min_blocks_per_sm     : e.g.:4.   128x4=512, if smaller than 2048 (CUDA_MAX_THREADS_PER_SM), it's 4. Else it's ceil(2048/threads_per_block) = 16
```

Every Block = 128 Thread, Do 2 functor() each = 256 elem-op per-block
   
f(idx); ==> idx is array index. For 2x4 tensor, the index in 0 ~ 7.
    
    
---
    [[[ gpu_kernel_impl ]]]
    
template <typename func_t>
void gpu_kernel_impl(TensorIteratorBase& iter, const func_t& f) {

ntensors = function_traits<func_t>::arity + 1; // func_t. Flake: copy output + inputs, the 1st is output

at::detail::Array<char*, ntensors> data;
for (int i = 0; i < ntensors; i++) {
    data[i] = (char*)iter.data_ptr(i);
    printf("iter.data_ptr = %p\n", data[i]);
}



---

From c++filt:
elementwise_kernel, gpu_kernel_impl, BinaryFunctor, MulFunctor

From code trace:
gpu_kernel(__host__) -> gpu_kernel_impl(__host__) -> elementwise_kernel(__global__) -> ...

---
input1 = torch.arange(1, 4, dtype=torch.float32).cuda() #[1., 2., 3.]
input2 = torch.tensor([3., 2., 1., 0.]).cuda()
output = torch.outer(input1, input2)
tensor([[3., 2., 1., 0.],
        [6., 4., 2., 0.],
        [9., 6., 3., 0.]])
My Output =
tensor([[3., 6., 9., 0.],
        [0., 0., 0., 0.],
        [0., 0., 0., 0.]])
---
Log:

MEMCPY_ASYNC[memcpy_and_sync()][97] d:0x7f49fa000000, s:0x6275fc0, n:12, k:1
MEMCPY_ASYNC[memcpy_and_sync()][97] d:0x7f49fa000200, s:0x6b4d1c0, n:16, k:1
[ 26] - ker: mul_cuda
[Flake] Loops.cuh, gpu_kernel() S
[Flake] Loops.cuh, gpu_kernel() call gpu_kernel_impl()
iter.data_ptr = 0x7f49fa000400
iter.data_ptr = 0x7f49fa000000
iter.data_ptr = 0x7f49fa000200
[gpu_kernel_impl] --- 226, ntensors:3, arity:2
[gpu_kernel_impl] BBB 233, dy:0, conti:0
[make_offset_calculator][105], N=3, ss=0, ndim:2, ntensors:3
[make_offset_calculator][109], iter.shape().data()[  0] = 4
[make_offset_calculator][109], iter.shape().data()[  1] = 3
[make_offset_calculator][113], strides[  0] = 0x7ffca82ddb68, [0]=4, [1]=16, [2]=8191
[make_offset_calculator][113], strides[  1] = 0x7ffca82ddbe8, [0]=0, [1]=4, [2]=139959631157928
[make_offset_calculator][113], strides[  2] = 0x7ffca82ddc68, [0]=4, [1]=0, [2]=139958356074423
i:0, arg:0, element_size:1, i_arg:4, arg_i:4
i:0, arg:1, element_size:1, i_arg:0, arg_i:0
i:0, arg:2, element_size:1, i_arg:4, arg_i:4
i:1, arg:0, element_size:1, i_arg:16, arg_i:16
i:1, arg:1, element_size:1, i_arg:4, arg_i:4
i:1, arg:2, element_size:1, i_arg:0, arg_i:0
[gpu_kernel_impl] BBB 237, rf:2, numel:12
[Flake] Loops.cuh, gpu_kernel() E
MEMCPY_ASYNC[memcpy_and_sync()][97] d:0x6b4b880, s:0x7f49fa000400, n:48, k:2
linear_idx= 0, [0]= 0, [1]= 0, [2]= 0 (.data[0/1/2]= 0/ 0/ 0)
linear_idx= 0, [0]= 4, [1]= 0, [2]= 4 (.data[0/1/2]= 4/ 0/ 4)
linear_idx= 0, [0]= 8, [1]= 0, [2]= 8 (.data[0/1/2]= 8/ 0/ 8)
linear_idx= 0, [0]=12, [1]= 0, [2]=12 (.data[0/1/2]=12/ 0/12)
linear_idx= 0, [0]=16, [1]= 4, [2]= 0 (.data[0/1/2]=16/ 4/ 0)
linear_idx= 0, [0]=20, [1]= 4, [2]= 4 (.data[0/1/2]=20/ 4/ 4)
linear_idx= 0, [0]=24, [1]= 4, [2]= 8 (.data[0/1/2]=24/ 4/ 8)
linear_idx= 0, [0]=28, [1]= 4, [2]=12 (.data[0/1/2]=28/ 4/12)
linear_idx= 0, [0]=32, [1]= 8, [2]= 0 (.data[0/1/2]=32/ 8/ 0)
linear_idx= 0, [0]=36, [1]= 8, [2]= 4 (.data[0/1/2]=36/ 8/ 4)
linear_idx= 0, [0]=40, [1]= 8, [2]= 8 (.data[0/1/2]=40/ 8/ 8)
linear_idx= 0, [0]=44, [1]= 8, [2]=12 (.data[0/1/2]=44/ 8/12)
elementwise_kernel - bid:0/0/0, tid:0/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:1/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:2/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:3/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:4/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:5/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:6/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:7/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:8/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:9/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:10/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:11/0/0, do:1, nt:128, vt:2, N:12
    

---

```
auto offset_calc = ::make_offset_calculator<traits::arity + 1>(iter);

auto offsets = offset_calc.get(idx);

arg0_t* out = (arg0_t*)(data[0] + offsets[0]);
*out = invoke(f, &data.data[1], &offsets.data[1], 1);
```
Q: What's the difference between offsets[0] and offsets.data[1] ???
A: SAME ... but the latter one is used in addressing case.

-- 

```
*out = invoke(f, &data.data[1], &offsets.data[1], 1);
```
Q: Use invoke1 or invoke2??
A: 2 !!!

--


'GADAM=0.000000000E+00' # TEST : mulbc_1d_3_1d_1.py
'GADAM=0.000000000E+00' # TEST : mulbc_2d_2_3_1d_1.py
'GADAM=1.550000003E-01' # TEST : mulbc_2d_2_3_1d_3.py        *
'GADAM=2.361111199E-01' # TEST : mulbc_3d_1_2_3_2d_1_3.py    *
'GADAM=2.232142880E-01' # TEST : mulbc_3d_1_2_3_2d_2_1.py    *
'GADAM=0.000000000E+00' # TEST : mulbc_3d_2_2_3_1d_1.py
'GADAM=3.294658074E-01' # TEST : mulbc_3d_2_2_3_1d_3.py      *

--
mulbc_2d_2_3_1d_3
<IN>
input1 = torch.tensor([[1., 2., 5.],[3., 4., 7.]]).cuda()
input2 = torch.tensor([3., 2., 4.]).cuda()
<OUT>
tensor([[ 3.,  4., 20.],
        [ 9.,  8., 28.]])

MEMCPY_ASYNC[memcpy_and_sync()][97] d:0x7f6002000000, s:0x6b6c5c0, n:24, k:1
MEMCPY_ASYNC[memcpy_and_sync()][97] d:0x7f6002000200, s:0x67c7ed80, n:12, k:1
[ 26] - ker: mul_cuda
[Flake] Loops.cuh, gpu_kernel() S
[Flake] Loops.cuh, gpu_kernel() call gpu_kernel_impl()
iter.data_ptr = 0x7f6002000400
iter.data_ptr = 0x7f6002000000
iter.data_ptr = 0x7f6002000200
[gpu_kernel_impl] --- 228, ntensors:3, arity:2
[gpu_kernel_impl] BBB 235, dy:0, conti:0
[make_offset_calculator][107], N=3, ss=0, ndim:2, ntensors:3
[make_offset_calculator][111], iter.shape().data()[  0] = 3
[make_offset_calculator][111], iter.shape().data()[  1] = 2
[make_offset_calculator][115], strides[  0] = 0x7ffe8154ed78, [0]=4, [1]=12, [2]=8191
[make_offset_calculator][115], strides[  1] = 0x7ffe8154edf8, [0]=4, [1]=12, [2]=140053438864040
[make_offset_calculator][115], strides[  2] = 0x7ffe8154ee78, [0]=4, [1]=0, [2]=140052983766967
i:0, arg:0, element_size:1, i_arg:4, arg_i:4
i:0, arg:1, element_size:1, i_arg:4, arg_i:4
i:0, arg:2, element_size:1, i_arg:4, arg_i:4
i:1, arg:0, element_size:1, i_arg:12, arg_i:12
i:1, arg:1, element_size:1, i_arg:12, arg_i:12
i:1, arg:2, element_size:1, i_arg:0, arg_i:0
[gpu_kernel_impl] BBB 239, rf:2, numel:6
[Flake] Loops.cuh, gpu_kernel() E
MEMCPY_ASYNC[memcpy_and_sync()][97] d:0x742f3c0, s:0x7f6002000400, n:24, k:2
linear_idx= 0, [0]= 0, [1]= 0, [2]= 0 (.data[0/1/2]= 0/ 0/ 0)
linear_idx= 0, [0]= 4, [1]= 4, [2]= 4 (.data[0/1/2]= 4/ 4/ 4)
linear_idx= 0, [0]= 8, [1]= 8, [2]= 8 (.data[0/1/2]= 8/ 8/ 8)
linear_idx= 0, [0]=12, [1]=12, [2]= 0 (.data[0/1/2]=12/12/ 0)
linear_idx= 0, [0]=16, [1]=16, [2]= 4 (.data[0/1/2]=16/16/ 4)
linear_idx= 0, [0]=20, [1]=20, [2]= 8 (.data[0/1/2]=20/20/ 8)
elementwise_kernel - bid:0/0/0, tid:0/0/0, do:1, nt:128, vt:2, N:6
elementwise_kernel - bid:0/0/0, tid:1/0/0, do:1, nt:128, vt:2, N:6
elementwise_kernel - bid:0/0/0, tid:2/0/0, do:1, nt:128, vt:2, N:6
elementwise_kernel - bid:0/0/0, tid:3/0/0, do:1, nt:128, vt:2, N:6
elementwise_kernel - bid:0/0/0, tid:4/0/0, do:1, nt:128, vt:2, N:6
elementwise_kernel - bid:0/0/0, tid:5/0/0, do:1, nt:128, vt:2, N:6

--
mulbc_3d_2_2_3_1d_3
<IN>
input1 = torch.tensor([[[1., 2., 5.],[3., 4., 7.]],[[11., 12., 15.],[13., 14., 17.]]]).cuda()
input2 = torch.tensor([3., 2., 4.]).cuda()
<OUT>
tensor([[[ 3.,  4., 20.],
         [ 9.,  8., 28.]],

        [[33., 24., 60.],
         [39., 28., 68.]]])

MEMCPY_ASYNC[memcpy_and_sync()][97] d:0x7fb7ba000000, s:0x67c3800, n:48, k:1
MEMCPY_ASYNC[memcpy_and_sync()][97] d:0x7fb7ba000200, s:0x65bdb500, n:12, k:1
[ 26] - ker: mul_cuda
[Flake] Loops.cuh, gpu_kernel() S
[Flake] Loops.cuh, gpu_kernel() call gpu_kernel_impl()
iter.data_ptr = 0x7fb7ba000400
iter.data_ptr = 0x7fb7ba000000
iter.data_ptr = 0x7fb7ba000200
[gpu_kernel_impl] --- 228, ntensors:3, arity:2
[gpu_kernel_impl] BBB 235, dy:0, conti:0
[make_offset_calculator][107], N=3, ss=0, ndim:2, ntensors:3
[make_offset_calculator][111], iter.shape().data()[  0] = 3
[make_offset_calculator][111], iter.shape().data()[  1] = 4
[make_offset_calculator][115], strides[  0] = 0x7ffd45b0b198, [0]=4, [1]=12, [2]=24
[make_offset_calculator][115], strides[  1] = 0x7ffd45b0b218, [0]=4, [1]=12, [2]=24
[make_offset_calculator][115], strides[  2] = 0x7ffd45b0b298, [0]=4, [1]=0, [2]=0
i:0, arg:0, element_size:1, i_arg:4, arg_i:4
i:0, arg:1, element_size:1, i_arg:4, arg_i:4
i:0, arg:2, element_size:1, i_arg:4, arg_i:4
i:1, arg:0, element_size:1, i_arg:12, arg_i:12
i:1, arg:1, element_size:1, i_arg:12, arg_i:12
i:1, arg:2, element_size:1, i_arg:0, arg_i:0
[gpu_kernel_impl] BBB 239, rf:2, numel:12
[Flake] Loops.cuh, gpu_kernel() E
MEMCPY_ASYNC[memcpy_and_sync()][97] d:0x65bdc580, s:0x7fb7ba000400, n:48, k:2
linear_idx= 0, [0]= 0, [1]= 0, [2]= 0 (.data[0/1/2]= 0/ 0/ 0)
linear_idx= 0, [0]= 4, [1]= 4, [2]= 4 (.data[0/1/2]= 4/ 4/ 4)
linear_idx= 0, [0]= 8, [1]= 8, [2]= 8 (.data[0/1/2]= 8/ 8/ 8)
linear_idx= 0, [0]=12, [1]=12, [2]= 0 (.data[0/1/2]=12/12/ 0)
linear_idx= 0, [0]=16, [1]=16, [2]= 4 (.data[0/1/2]=16/16/ 4)
linear_idx= 0, [0]=20, [1]=20, [2]= 8 (.data[0/1/2]=20/20/ 8)
linear_idx= 0, [0]=24, [1]=24, [2]= 0 (.data[0/1/2]=24/24/ 0)
linear_idx= 0, [0]=28, [1]=28, [2]= 4 (.data[0/1/2]=28/28/ 4)
linear_idx= 0, [0]=32, [1]=32, [2]= 8 (.data[0/1/2]=32/32/ 8)
linear_idx= 0, [0]=36, [1]=36, [2]= 0 (.data[0/1/2]=36/36/ 0)
linear_idx= 0, [0]=40, [1]=40, [2]= 4 (.data[0/1/2]=40/40/ 4)
linear_idx= 0, [0]=44, [1]=44, [2]= 8 (.data[0/1/2]=44/44/ 8)
elementwise_kernel - bid:0/0/0, tid:0/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:1/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:2/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:3/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:4/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:5/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:6/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:7/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:8/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:9/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:10/0/0, do:1, nt:128, vt:2, N:12
elementwise_kernel - bid:0/0/0, tid:11/0/0, do:1, nt:128, vt:2, N:12

--
For 2x3 x 1x1

MEMCPY_ASYNC[memcpy_and_sync()][97] d:0x7fee5c000000, s:0x5d6d700, n:24, k:1
MEMCPY_ASYNC[memcpy_and_sync()][97] d:0x7fee5c000200, s:0x687e4fc0, n:4, k:1
[ 26] - ker: mul_cuda
[Flake] Loops.cuh, gpu_kernel() S
[Flake] Loops.cuh, gpu_kernel() call gpu_kernel_impl()
iter.data_ptr = 0x7fee5c000400
iter.data_ptr = 0x7fee5c000000
iter.data_ptr = 0x7fee5c000200
[gpu_kernel_impl] --- 228, ntensors:3, arity:2
[gpu_kernel_impl] BBB 235, dy:0, conti:0
[make_offset_calculator][107], N=3, ss=0, ndim:1, ntensors:3
[make_offset_calculator][111], iter.shape().data()[  0] = 6
[make_offset_calculator][115], strides[  0] = 0x7ffe7e2898a8, [0]=4, [1]=12, [2]=8191
[make_offset_calculator][115], strides[  1] = 0x7ffe7e289928, [0]=4, [1]=12, [2]=140664567831208
[make_offset_calculator][115], strides[  2] = 0x7ffe7e2899a8, [0]=0, [1]=0, [2]=140664112734135
i:0, arg:0, element_size:1, i_arg:4, arg_i:4
i:0, arg:1, element_size:1, i_arg:4, arg_i:4
i:0, arg:2, element_size:1, i_arg:0, arg_i:0
[gpu_kernel_impl] BBB 239, rf:2, numel:6
[Flake] Loops.cuh, gpu_kernel() E
MEMCPY_ASYNC[memcpy_and_sync()][97] d:0x687e5f80, s:0x7fee5c000400, n:24, k:2
linear_idx= 0, [0]= 0, [1]= 0, [2]= 0 (.data[0/1/2]= 0/ 0/ 0)
linear_idx= 0, [0]= 4, [1]= 4, [2]= 0 (.data[0/1/2]= 4/ 4/ 0)
linear_idx= 0, [0]= 8, [1]= 8, [2]= 0 (.data[0/1/2]= 8/ 8/ 0)
linear_idx= 0, [0]=12, [1]=12, [2]= 0 (.data[0/1/2]=12/12/ 0)
linear_idx= 0, [0]=16, [1]=16, [2]= 0 (.data[0/1/2]=16/16/ 0)
linear_idx= 0, [0]=20, [1]=20, [2]= 0 (.data[0/1/2]=20/20/ 0)
elementwise_kernel - bid:0/0/0, tid:0/0/0, do:1, nt:128, vt:2, N:6
elementwise_kernel - bid:0/0/0, tid:1/0/0, do:1, nt:128, vt:2, N:6
elementwise_kernel - bid:0/0/0, tid:2/0/0, do:1, nt:128, vt:2, N:6
elementwise_kernel - bid:0/0/0, tid:3/0/0, do:1, nt:128, vt:2, N:6
elementwise_kernel - bid:0/0/0, tid:4/0/0, do:1, nt:128, vt:2, N:6
elementwise_kernel - bid:0/0/0, tid:5/0/0, do:1, nt:128, vt:2, N:6

--
Q: In get() method, print the address to member variables: dims, sizes_, strides_
linear_idx= 0, [0]= 0, [1]= 0, [2]= 0 (0x7f724dfff9b0)(0x7f724dfff9b4-0x7f724dfff9b4-0x7f724dfff9c0)(0x7f724dfffae0)
linear_idx= 0, [0]= 4, [1]= 0, [2]= 4 (0x7f724dfff9b0)(0x7f724dfff9b4-0x7f724dfff9b4-0x7f724dfff9c0)(0x7f724dfffae0)
linear_idx= 0, [0]= 8, [1]= 0, [2]= 8 (0x7f724dfff9b0)(0x7f724dfff9b4-0x7f724dfff9b4-0x7f724dfff9c0)(0x7f724dfffae0)
linear_idx= 0, [0]=12, [1]= 0, [2]=12 (0x7f724dfff9b0)(0x7f724dfff9b4-0x7f724dfff9b4-0x7f724dfff9c0)(0x7f724dfffae0)
linear_idx= 0, [0]=16, [1]= 4, [2]= 0 (0x7f724dfff9b0)(0x7f724dfff9b4-0x7f724dfff9b4-0x7f724dfff9c0)(0x7f724dfffae0)
linear_idx= 0, [0]=20, [1]= 4, [2]= 4 (0x7f724dfff9b0)(0x7f724dfff9b4-0x7f724dfff9b4-0x7f724dfff9c0)(0x7f724dfffae0)
linear_idx= 0, [0]=24, [1]= 4, [2]= 8 (0x7f724dfff9b0)(0x7f724dfff9b4-0x7f724dfff9b4-0x7f724dfff9c0)(0x7f724dfffae0)
linear_idx= 0, [0]=28, [1]= 4, [2]=12 (0x7f724dfff9b0)(0x7f724dfff9b4-0x7f724dfff9b4-0x7f724dfff9c0)(0x7f724dfffae0)
linear_idx= 0, [0]=32, [1]= 8, [2]= 0 (0x7f724dfff9b0)(0x7f724dfff9b4-0x7f724dfff9b4-0x7f724dfff9c0)(0x7f724dfffae0)
linear_idx= 0, [0]=36, [1]= 8, [2]= 4 (0x7f724dfff9b0)(0x7f724dfff9b4-0x7f724dfff9b4-0x7f724dfff9c0)(0x7f724dfffae0)
linear_idx= 0, [0]=40, [1]= 8, [2]= 8 (0x7f724dfff9b0)(0x7f724dfff9b4-0x7f724dfff9b4-0x7f724dfff9c0)(0x7f724dfffae0)
linear_idx= 0, [0]=44, [1]= 8, [2]=12 (0x7f724dfff9b0)(0x7f724dfff9b4-0x7f724dfff9b4-0x7f724dfff9c0)(0x7f724dfffae0)

    printf("linear_idx=%2d, [0]=%2u, [1]=%2u, [2]=%2u (%p)(%p-%p-%p)(%p)\n",
      linear_idx, offsets[0], offsets[1], offsets[2],
      &this->dims,
      this->sizes_,
      &this->sizes_[0].divisor,
      &this->sizes_[1].divisor,
      this->strides_);

sizes_ is object: at::cuda::detail::IntDivider<index_t> sizes_[MAX_DIMS];

MAX_DIMS is 25
IntDivider size is 12, pseudo structure = 
{
    u32 divisor;
    u32 reserved[2];
}

Summary:
dims in offset 0
size0_12 in offset 4
size1_12 in offset 16
size2_12 in offset 28 ...
strides_ in offset 304

--
Q: In gpu_kernel_impl(), what's the address of 1st param offset_calc and 2nd param data in lambda function?

printf("[lamda] of:%p, data:%p\n", &offset_calc, &data);

[lamda] of:0x7f436efff9b0, data:0x7f436efffc10

0xc10 - 0x9b0 = 0x260 = 608

Summary:
data array in offset 608