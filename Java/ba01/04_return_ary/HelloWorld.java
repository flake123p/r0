//
// https://docs.oracle.com/javase/tutorial/java/generics/types.html
//

import mypack.func;

public class HelloWorld
{
   public static void main(String[] args)
   {
      System.out.println("Hello World!");

      func<Integer> f = new func<>();
      
      boolean ret[] = f.return_ary();

      System.out.println(ret[0]);
      System.out.println(ret[1]);
      System.out.println(ret[2]);
   }
}
