//
// https://docs.oracle.com/javase/tutorial/java/generics/types.html
//

import mypack.func;

public class HelloWorld
{
   public static void main(String[] args)
   {
      System.out.println("Hello World!");

      func<Integer> f = new func<>();
      f.t = 10;
      f.run();
   }
}
