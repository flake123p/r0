package mypack;

interface Intf
{
   final double PI=3.14;        //final 可省略
   public abstract void show(); //public abstract 可省略
}

public class func<T> implements Intf
{
   public T t;

   public static void main(String[] args)
   {
      System.out.println("Hello func!");
   }

   public void run()
   {
      System.out.println("Run func! ... " + t);
      show();
   }

   public void show()
   {
      System.out.println("PI! ... " + PI);
   }
}