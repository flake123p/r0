//
// https://docs.oracle.com/javase/tutorial/java/generics/types.html
//

import mypack.func;
import java.util.Calendar;
import java.util.Date;

/*

API:
      udate.setDate(year, mon + 1, mday);
      udate.setTimeOfDay(hour, min, sec, 0);


      int y = date.getNormalizedYear();
      int m = date.getMonth();
      int d = date.getDayOfMonth();
      int hh = date.getHours();
      int mm = date.getMinutes();
      int ss = date.getSeconds();
      int ms = date.getMillis();
      TimeZone tz = date.getZone();
 */


public class HelloWorld
{
   public static void main(String[] args)
   {
      System.out.println("Hello World!");

      // func<Integer> f = new func<>();
      
      // boolean ret[] = f.return_ary();

      // System.out.println(ret[0]);
      // System.out.println(ret[1]);
      // System.out.println(ret[2]);

      Date currentTime = Calendar.getInstance().getTime();
      String dbg = String.valueOf(currentTime);

      System.out.println(dbg);
      Date currentTime2 = Calendar.getInstance().getTime();
      System.out.println(currentTime2);
      System.out.println(currentTime2.getTime());

      Calendar calendar = Calendar.getInstance();
      calendar.set(2018, 7, 1, 0, 0, 0);
      Date happyNewYearDate = calendar.getTime();
      System.out.println("happyNewYearDate with: (2018, 7, 1, 0, 0, 0)");
      System.out.println(happyNewYearDate);
      System.out.println(happyNewYearDate.getTime());

      Calendar calendar2 = Calendar.getInstance();
      calendar2.set(2018, 9-1, 1, 0, 0, 0);
      Date happyNewYearDate2 = calendar2.getTime();
      System.out.println("happyNewYearDate with: (2018, 9-1, 1, 0, 0, 0)");
      System.out.println(happyNewYearDate2);
      System.out.println(happyNewYearDate2.getTime());

      System.out.println(happyNewYearDate2.getTime() - happyNewYearDate.getTime());
      System.out.println(happyNewYearDate.getTime() - happyNewYearDate2.getTime());
   }
}
