package mypack;

public class func<T>
{
   public T t;

   public static void main(String[] args)
   {
      System.out.println("Hello func!");
   }

   public void run()
   {
      System.out.println("Run func! ... " + t);
   }
}