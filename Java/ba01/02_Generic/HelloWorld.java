//
// https://docs.oracle.com/javase/tutorial/java/generics/types.html
//

import mypack.func;

public class HelloWorld
{
   public static void main(String[] args)
   {
      System.out.println("Hello World!");

      func<Integer> f = new func<>();
      f.t = 10;
      f.run();

      func<Float> f2 = new func<>();
      f2.t = 10.01f;
      f2.run();
   }
}
