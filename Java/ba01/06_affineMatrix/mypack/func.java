package mypack;
import java.lang.Math;

public class func
{
   public void run()
   {
      System.out.println("Run func! ... " + Math.sin(Math.toRadians(45)));
      affineMatrixTest();
   }

   public void affineMatrixImpl(double[][] landmarks, int scale /* 2 */, double[][] outMat, int[] outSize) {
      double[] nose = new double[2];
      double[] left_eye = new double[2];
      double[] right_eye = new double[2];
      double[] eye_width = new double[2];

      nose[0] = landmarks[4][0];
      nose[1] = landmarks[4][1];
      boolean swapIndex2And3 = true;
      if (swapIndex2And3) {
         left_eye[0] = landmarks[3][0];
         left_eye[1] = landmarks[3][1];
      } else {
         left_eye[0] = landmarks[2][0];
         left_eye[1] = landmarks[2][1];
      }
      right_eye[0] = landmarks[0][0];
      right_eye[1] = landmarks[0][1];
      eye_width[0] = right_eye[0] - left_eye[0]; //63
      eye_width[1] = right_eye[1] - left_eye[1]; //-2

      //System.out.println("eye_width = " + eye_width[0]);
      //System.out.println("eye_width = " + eye_width[1]);

      double angle = Math.atan2((double)eye_width[1], (double)eye_width[0]); //-0.0317
      //System.out.println("angle = " + angle);

      double[] center = nose; //179, 147

      double alpha = Math.cos(angle);
      double beta = Math.sin(angle);

      double temp = ((double)eye_width[0]*eye_width[0]) + ((double)eye_width[1]*eye_width[1]);
      double w = Math.sqrt(temp) * scale;

      outMat[0][0] = (double)alpha;
      outMat[0][1] = (double)beta;
      outMat[0][2] = (double)((-alpha * center[0]) - (beta * center[1]) + (0.5 * w));
      outMat[1][0] = (double)-beta;
      outMat[1][1] = (double)alpha;
      outMat[1][2] = (double)((beta * center[0]) - (alpha * center[1]) + (0.5 * w));

      outSize[0] = (int)w;
      outSize[1] = (int)w;
   }

   public void affineMatrix(double[][] landmarks, double[][] outMat, int[] outSize) {
      affineMatrixImpl(landmarks, 2, outMat, outSize);
   }

   public float calculateDistance(float[] input) {
      double ret = 0.0f;
      for (int i = 0; i < input.length; i++) {
         ret = ret + (input[i] * input[i]);
      }
      return (float)Math.sqrt(ret);
   }

   public float calculateDistance2Input(float[] input, float[] input2) {
      if (input.length != input2.length) {
         return Float.POSITIVE_INFINITY;
      }
      double ret = 0.0f;
      float diff;
      for (int i = 0; i < input.length; i++) {
         diff = input[i] - input2[i];
         ret = ret + (diff * diff);
      }
      return (float)Math.sqrt(ret);
   }

   public void affineMatrixTest() {
   /*
      The indices of 7x2 output is: 9, 6, 3, 0, 18, 15, 12

      The origin affineMatrix input is: 9, 6, 0, 3, 18

      To reuse the 7x2 output = swap index 2 & 3
    */
      double[][] landmarks;

      boolean swapIndex2And3 = true;
      if (swapIndex2And3) {
         landmarks = new double[7][2];
         landmarks[0][0] = 215; landmarks[0][1] = 114;
         landmarks[1][0] = 197; landmarks[1][1] = 115;
         landmarks[3][0] = 152; landmarks[3][1] = 116;
         landmarks[2][0] = 168; landmarks[2][1] = 115;
         landmarks[4][0] = 179; landmarks[4][1] = 147;
      } else {
         landmarks = new double[5][2];
         landmarks[0][0] = 215; landmarks[0][1] = 114;
         landmarks[1][0] = 197; landmarks[1][1] = 115;
         landmarks[2][0] = 152; landmarks[2][1] = 116;
         landmarks[3][0] = 168; landmarks[3][1] = 115;
         landmarks[4][0] = 179; landmarks[4][1] = 147;
      }


      double[][] outMat = new double[2][3];
      int[] outSize = new int[2];

      affineMatrix(landmarks, outMat, outSize);

      System.out.print("Mat =\n");
      String dump;
      for (int i = 0; i < 2; i++) 
      {
         dump = String.format("%7.4f, %7.4f, %7.4f\n", outMat[i][0], outMat[i][1], outMat[i][2]); 
         System.out.print(dump);
      }

      dump = String.format("Size = %4d, %4d\n", outSize[0], outSize[1]); // verified ok
      System.out.print(dump);

      float[] distIn = new float[3];
      distIn[0] = 2f;
      distIn[1] = 1f;
      distIn[2] = 0f;
      System.out.println("Dist = " + calculateDistance(distIn));

      float[] input1 = new float[3];
      input1[0] = 1f;
      input1[1] = 2f;
      input1[2] = 3f;
      float[] input2 = new float[3];
      input2[0] = 3f;
      input2[1] = 3f;
      input2[2] = 3f;
      System.out.println("Dist = " + calculateDistance2Input(input1, input2));
   }
}