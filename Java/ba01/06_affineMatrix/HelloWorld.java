//
// https://docs.oracle.com/javase/tutorial/java/generics/types.html
//

import mypack.func;
import java.util.Calendar;
import java.util.Date;

/*

API:
      udate.setDate(year, mon + 1, mday);
      udate.setTimeOfDay(hour, min, sec, 0);


      int y = date.getNormalizedYear();
      int m = date.getMonth();
      int d = date.getDayOfMonth();
      int hh = date.getHours();
      int mm = date.getMinutes();
      int ss = date.getSeconds();
      int ms = date.getMillis();
      TimeZone tz = date.getZone();
 */


public class HelloWorld
{
   public static void main(String[] args)
   {
      System.out.println("Hello World!");

      func f = new func();
      f.run();
   }
}
