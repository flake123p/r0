interface intf {
   final double PI=3.14;        //final 可省略
   //public abstract static void show(); //public abstract 可省略
}
public class HelloWorld implements intf
{
   public static void show()
   {
      System.out.println("Hello World! " + this.PI);
   }

   public static void main(String[] args)
   {
      show();
   }
}
