# Orientation:


## DetectorActivity
DetectorActivity.java
	public void onPreviewSizeChosen(final Size size, final int rotation) ... 
    
***input rotation = 90***
***input Size = 640x480***
***caller ... ???***

sensorOrientation = rotation - getScreenOrientation();
/*
    sensorOrientation is private Integer member of class DetectorActivity
    DUMP:
        sensorOrientation      is 90
        rotation               is 90
        getScreenOrientation() is 0     ***Flake, always 0 when really rotate the phone***
*/

/*
CameraActivit::getScreenOrientation()

  protected int getScreenOrientation() {
    switch (getWindowManager().getDefaultDisplay().getRotation()) {
      case Surface.ROTATION_270:
        return 270;
      case Surface.ROTATION_180:
        return 180;
      case Surface.ROTATION_90:
        return 90;
      default:
        return 0;
    }
  }
*/

### TransformationMatrix
/*
frameToCropTransform =
        ImageUtils.getTransformationMatrix(
                previewWidth, previewHeight,
                cropSize, cropSize,
                sensorOrientation, MAINTAIN_ASPECT);

DUMP:
    previewWidth = 640
    previewHeight = 480
    cropSize = 320
    sensorOrientation = 90
    MAINTAIN_ASPECT = false
*/

### 2 Way Transformation
cropToFrameTransform = new Matrix();                 // 320x320 to 640x480
frameToCropTransform.invert(cropToFrameTransform);   // 640x480 to 320x320

