
# create
conda create -n <ENV_NAME> python=3.9

(with spyder)
conda create -n tf114p36 python=3.6 spyder


# delete
conda remove -n <ENV_NAME> --all


# list
conda env list


# list python version
conda search python