# PATH export in conda
## 1. Create functions in ~/.bashrc
https://stackoverflow.com/questions/11650840/remove-redundant-paths-from-path-variable
```
# Functions to help us manage paths.  Second argument is the name of the
# path variable to be modified (default: PATH)
pathremove () {
        local IFS=':'
        local NEWPATH
        local DIR
        local PATHVARIABLE=${2:-PATH}
        for DIR in ${!PATHVARIABLE} ; do
                if [ "$DIR" != "$1" ] ; then
                  NEWPATH=${NEWPATH:+$NEWPATH:}$DIR
                fi
        done
        export $PATHVARIABLE="$NEWPATH"
}

pathprepend () {
        pathremove $1 $2
        local PATHVARIABLE=${2:-PATH}
        export $PATHVARIABLE="$1${!PATHVARIABLE:+:${!PATHVARIABLE}}"
}

pathappend () {
        pathremove $1 $2
        local PATHVARIABLE=${2:-PATH}
        export $PATHVARIABLE="${!PATHVARIABLE:+${!PATHVARIABLE}:}$1"
}
```

## 2. Add activate script
in:
~/anaconda3/envs/(your_env)/etc/conda/activate.d/

With:
any_file_name.sh

```
# To PATH
pathprepend /tmp/temp
# To LD_LIBRARY_PATH
pathprepend /tmp/temp LD_LIBRARY_PATH
```
/tmp/temp is an example.

## 3. Add deactivate script
in:
~/anaconda3/envs/(your_env)/etc/conda/deactivate.d/

With:
any_file_name.sh

```
# Remove from PATH
pathremove /tmp/temp
# Remove from LD_LIBRARY_PATH
pathremove /tmp/temp LD_LIBRARY_PATH
```
/tmp/temp is an example.