#include "b.h"
#include <stdio.h>
#include "a.h"

typedef struct {
    const char  *start;
    const char  *end;
    const int    dontCareLen;
    KernelFunc_t callback;
} StartEndCmp_t;

// clang-format off
StartEndCmp_t budaFuncStartEndCmpDB[] = {
    {POW_SCALAR_F32_LAMBDA1_S, POW_SCALAR_F32_LAMBDA1_E, POW_SCALAR_F32_LAMBDA_DONT_CARE_LEN, Kernel_POW_SCALAR_LAMBDA_FAST<float, 2>},
    {POW_SCALAR_F32_LAMBDA2_S, POW_SCALAR_F32_LAMBDA2_E, POW_SCALAR_F32_LAMBDA_DONT_CARE_LEN, Kernel_POW_SCALAR_LAMBDA_FAST<float, 3>},
    {POW_SCALAR_F32_LAMBDA3_S, POW_SCALAR_F32_LAMBDA3_E, POW_SCALAR_F32_LAMBDA_DONT_CARE_LEN, Kernel_POW_SCALAR_LAMBDA_FAST<float, -2>},
    {POW_SCALAR_F32_LAMBDA4_S, POW_SCALAR_F32_LAMBDA4_E, POW_SCALAR_F32_LAMBDA_DONT_CARE_LEN, Kernel_POW_SCALAR_LAMBDA<float>},
    {POW_SCALAR_F16_LAMBDA1_S, POW_SCALAR_F16_LAMBDA1_E, POW_SCALAR_F16_LAMBDA_DONT_CARE_LEN, Kernel_POW_SCALAR_LAMBDA_FAST_F16<Fp16CellLt, 2>},
    {POW_SCALAR_F16_LAMBDA2_S, POW_SCALAR_F16_LAMBDA2_E, POW_SCALAR_F16_LAMBDA_DONT_CARE_LEN, Kernel_POW_SCALAR_LAMBDA_FAST_F16<Fp16CellLt, 3>},
    {POW_SCALAR_F16_LAMBDA3_S, POW_SCALAR_F16_LAMBDA3_E, POW_SCALAR_F16_LAMBDA_DONT_CARE_LEN, Kernel_POW_SCALAR_LAMBDA_FAST_F16<Fp16CellLt, -2>},
    {POW_SCALAR_F16_LAMBDA4_S, POW_SCALAR_F16_LAMBDA4_E, POW_SCALAR_F16_LAMBDA_DONT_CARE_LEN, Kernel_POW_SCALAR_LAMBDA_F16<Fp16CellLt>},
    {SILU_F32_S, SILU_F32_E, SILU_DONT_CARE_LEN, Kernel_SILU},
    {SILU_F16_S, SILU_F16_E, SILU_DONT_CARE_LEN, Kernel_SILU_F16},
};// clang-format on


int main()
{

                    printf("Hello World\n");
switch (1)
{
case 1 : {
}            break;   

case 2 :
{
break;
}

default:
break;
}
if (1>0) {}
if (1>0) {a;b;c;}
for(int i=0;i<10;i++){}//hi
for(int i=0;i<10;i++){a;b;c;}//hi

//
//
//
#ifdef x1
#ifdef x2
#define x3
#endif
#endif

#define X(a) a=3;
#define YY(a) a=3;\
a+=7;
/*123
456
*/
char*x;
foofoofoo(0x1111111111111111111111111111111111111aaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbbbbbbccccccccccccccccccccccccccccccccccccccccccccccccfdddddddddddddddddddddd11111111111,22222222222222222222222222222222222222222,333333333333333333333333333333333);
struct test demo[] ={
{56,23,"hello"},
{-1,93463,"world"},
{7,5,"!!"}
};
#define A 1
#define BB 2
#define CCC 3
int a = 1;
int somelongname = 2;
double c = 3;
int aaaa : 1;
int b:12;
int ccc:8;
double d = 4;
char *x;
char*x;
char* x;
char*x,*y;

int aaaa = 12;
float b = 23;

a &= 2;
bbb = 2;

switch (level) {
case log::info: return "info:";
case log::warning: return "warning:";
default: return "";
}
#define A \
int aaaa; \
int b; \
int dddddddddd;
int aaa = bbbbbbbbbbbbbbb
          + ccccccccccccccc;
int a;    // comment
int ab;       // comment

int abc;// comment
int abcd;     // comment
callFunction(
    aaaaaaaaaaaaaaaaaaaa,
             b,
             c,
             d);
while (true) {}
while (true) { continue; }

//
// AllowShortLambdasOnASingleLine
//
auto lambda = [](int a) {};
auto lambda2 = [](int a) {
    return a;
};
auto lambda = [](int x, int y) {
    x += y;
    return x < y;
};
sort(a.begin(), a.end(), [](int x, int y) { x += y; return x < y; });

//
// BreakAfterAttributes
//
[[maybe_unused]] const int i;
[[gnu::const]] [[maybe_unused]] int j;

[[nodiscard]] inline int f();
[[gnu::const]] [[nodiscard]] int g();

[[likely]] if (a)
  f();
else
  g();

switch (b) {
[[unlikely]] case 1:
  ++b;
  break;
[[likely]] default:
  return;
}

//
// EmptyLineAfterAccessModifier, EmptyLineBeforeAccessModifier IndentAccessModifiers
//
struct foo 
{
private:
    int i;
protected:
    int j;
  /* comment */
public:
    foo() {}
private:
protected:
};

class bar {
private:
    int i;
protected:
    int j;
  /* comment */
public:
    foo() {}
private:
protected:
};

return 0;
}

//
// BinPackParameters
//
void f(int aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa, int b,
       int c)
       {}



int f() {
  if (foo()) {
label1:
    bar();
  }
label2:
  return 1;
}

//
// NamespaceIndentation
//
namespace out {
int i;
namespace in {
int i;
}
}

//
//
//
typedef enum {
//vendor
CMD_REQ_TYPE_VENDOR = 0x0000,
CMD_REQ_ECHO,

//system (RT API)
CMD_REQ_TYPE_CU_RT = 0x1000,
CMD_REQ_MALLOC,
} X_T;

struct CMD_REQ_STRIDED_UNIRY_t {
CMD_REQ_HDR_t req_hdr;
uint64_t dst;
uint64_t src;
uint32_t dims;
CMD_REQ_STRIDED_UNIRY_PARA_t para[4];
};

typedef struct {
CMD_REQ_HDR_t req_hdr;
uint64_t dst;
uint64_t src;
uint32_t dims;
CMD_REQ_STRIDED_UNIRY_PARA_t para[4];
} CMD_REQ_STRIDED_UNIRY_t;

if (foo())
{
} else
{}
for (int i = 0; i < 10; ++i)
{x;b;c;}


template <typename T>
T foo() {
}

template <typename T> T foo() {
}

template <typename T> T foo(int aaaaaaaaaaaaaaaaaaaaa,
                            int bbbbbbbbbbbbbbbbbbbbb) {
}


