
cd conf0_Google_Attach/
clang-format ../before.cpp > ../after_conf0_Google_Attach_local.cpp ; cd ..

cd conf1_Google_Allman/
clang-format ../before.cpp > ../after_conf1_Google_Allman_local.cpp ; cd ..

cd conf2_LLVM_Allman/
clang-format ../before.cpp > ../after_conf2_LLVM_Allman_local.cpp ; cd ..

cd conf3_Chromium_Allman/
clang-format ../before.cpp > ../after_conf3_Chromium_Allman_local.cpp ; cd ..

cd conf4_Microsoft_Allman/
clang-format ../before.cpp > ../after_conf4_Microsoft_Allman_local.cpp ; cd ..

clang-format -style=file:./conf5_indent8/clang_hahaha.yml before.cpp > after_conf5_indent2.cpp