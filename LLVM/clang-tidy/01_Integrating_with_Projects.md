# Integrating with Projects
For larger projects, clang-tidy is typically used alongside compile_commands.json.

## 1. Generate compile_commands.json
Enable export of the compilation database in CMake:

    cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON .

The file will appear in the build directory.

## 2. Run Project-Wide Analysis
Use clang-tidy with compile_commands.json:

    clang-tidy -p /path/to/build example.cpp

-p specifies the directory containing compile_commands.json.

## 3. Batch Analysis
Use the script run-clang-tidy (provided by LLVM) to analyze all files in the project:

    run-clang-tidy

=========================================================================================
# Configuration File (.clang-tidy)
You can create a .clang-tidy file in the project root to customize checks. Example:

    Checks: 'modernize-*,-modernize-use-trailing-return-type'
    WarningsAsErrors: '*'
    HeaderFilterRegex: '.*'

With a .clang-tidy file, you no longer need to specify -checks explicitly:

    clang-tidy example.cpp -- -std=c++17


=========================================================================================
# Recommended Workflow
1. Enable compile_commands.json export in your build system (e.g., CMake).
2. Create a .clang-tidy file with your preferred configuration.
3. Run clang-tidy or run-clang-tidy to check your code.
4. Use -fix to automatically fix issues or manually review and fix them as needed.