# https://github.com/llvm/llvm-project/blob/main/clang/include/clang/Basic/LangStandards.def

c89
c99
c11
...

c++11
c++14
c++17
...

# Option:

-- -std=c++17