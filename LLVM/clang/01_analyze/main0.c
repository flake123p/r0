#include <stdio.h>
#include <stdlib.h>

void example() {
    int *ptr = (int *)malloc(sizeof(int));
    *ptr = 42;

    // Memory not freed (memory leak)
    printf("Value: %d\n", *ptr);
}

int main() {
    int *ptr = NULL;
    *ptr = 10; // Null pointer dereference
    
    example();

    return 0;
}