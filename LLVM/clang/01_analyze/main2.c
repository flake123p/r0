#include <stdio.h>
#include <stdlib.h>

int main() {
    FILE *file = fopen("test.txt", "w");
    if (!file) return 0;

    // File is not closed, leading to a resource leak
    fprintf(file, "Hello, World!");

    return 0;
}