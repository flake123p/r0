
#
# Default output main0.plist
#
clang --analyze -Xanalyzer -analyzer-checker=core,unix.Malloc,unix.MismatchedDeallocator main0.c -o main0_local.plist
clang --analyze -Xanalyzer -analyzer-checker=core,unix.Malloc,unix.MismatchedDeallocator main1.c -o main1_local.plist
clang --analyze -Xanalyzer -analyzer-checker=core,unix.Malloc,unix.MismatchedDeallocator main2.c -o main2_local.plist