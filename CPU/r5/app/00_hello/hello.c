#include <stdio.h>
//#include <stdlib.h>
//#include <stdint.h>
#include <math.h>

int my_calc() __attribute__((retain));
	
int my_calc()
{
	float ret = 0;
	for(int i = 0; i < 10; i++){
		//printf(".. %.10f\n", sinf(i));
		ret += sinf(i);
	}
	for(int i = 0; i < 10; i++){
		//printf(".. %.10f\n", sinf(i));
		ret += cosf(i);
	}
	for(int i = 0; i < 10; i++){
		//printf(".. %.10f\n", sinf(i));
		ret += sqrtf(i);
	}
	for(int i = 0; i < 10; i++){
		//printf(".. %.10f\n", sinf(i));
		ret += expf(i);
	}
	return (int)ret;
}

void __start() __attribute__((naked, used));
void __start()
{
	asm volatile ("la t0, _stack-16");
	asm volatile ("mv sp, t0");
	//asm volatile ("auipc sp, 0x4010");
	asm volatile ("j _start");
}

typedef float FLOAT_T;

static inline FLOAT_T mul_add_29(FLOAT_T *bdata, FLOAT_T *kernel) __attribute__((used));
static inline FLOAT_T mul_add_29(FLOAT_T *bdata, FLOAT_T *kernel)
{
    FLOAT_T s = 0;
    s += bdata[0    ] * kernel[0];
    s += bdata[1    ] * kernel[1];
    s += bdata[2    ] * kernel[2];
    s += bdata[3    ] * kernel[3];
    s += bdata[4    ] * kernel[4];
    s += bdata[5    ] * kernel[5];
    s += bdata[6    ] * kernel[6];
    s += bdata[7    ] * kernel[7];
    s += bdata[8    ] * kernel[8];
    s += bdata[9    ] * kernel[9];
    s += bdata[10   ] * kernel[10];
    s += bdata[11   ] * kernel[11];
    s += bdata[12   ] * kernel[12];
    s += bdata[13   ] * kernel[13];
    s += bdata[14   ] * kernel[14];
    s += bdata[15   ] * kernel[15];
    s += bdata[16   ] * kernel[16];
    s += bdata[17   ] * kernel[17];
    s += bdata[18   ] * kernel[18];
    s += bdata[19   ] * kernel[19];
    s += bdata[20   ] * kernel[20];
    s += bdata[21   ] * kernel[21];
    s += bdata[22   ] * kernel[22];
    s += bdata[23   ] * kernel[23];
    s += bdata[24   ] * kernel[24];
    s += bdata[25   ] * kernel[25];
    s += bdata[26   ] * kernel[26];
    s += bdata[27   ] * kernel[27];
    s += bdata[28   ] * kernel[28];
    return s;
}

#define rd_csr(reg) ({ unsigned long __tmp; \
  asm volatile ("csrr %0, " #reg : "=r"(__tmp)); \
  __tmp; })

int main()
{
	//uint8_t *p = (uint8_t *)malloc(4);
	//free(p);

//	for(int i = 0; i < 10; i++){
//		printf(".. %.10f\n", sinf(i));
//	}
	//printf("hello r5, spike %.10f, %.10f\n", sqrtf(2), sinf(20));
	
	__asm__ __volatile__("abxxxx:");
	
	//int x = my_calc();
	//printf("hello %d\n", x);
	
    FLOAT_T bdata[100] = {1};
    FLOAT_T kernel[100] = {2};
    //volatile int x = (__builtin_riscv_csrr ((0xb00)));
    //(void)x;
    volatile int y = rd_csr(0xB00);
    y = rd_csr(0xB00);
    y = rd_csr(0xB00);
    y = rd_csr(0xB00);
    volatile FLOAT_T s = mul_add_29(bdata, kernel);
    volatile int z = rd_csr(0xB00);
    z = z - y;

    (void)y;
    (void)z;
    (void)s;
    s = s;
	
	while (1) {;}
	
	return 0;
}
