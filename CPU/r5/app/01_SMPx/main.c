/*
    clint WriteDoubleWord 0x00 0xff

    clint WriteDoubleWord 0x04 0xff

    clint WriteDoubleWord 0x08 0xff

    clint WriteDoubleWord 0x0c 0xff
*/



#include <stdint.h>
#include <unistd.h>

/* Claim Register - 0x1000 per target */
#define PLIC_CLAIM_OFFSET               (0x00200004UL)
#define PLIC_CLAIM_SHIFT_PER_TARGET     12

#define _IO_(addr)              (addr)
#define PLIC_SW_BASE            _IO_(0xE6400000)
#define PLIC_CLAIM_OFFSET               (0x00200004UL)

#define NDS_MHARTID 0xF14


#define read_csr(reg) ({ unsigned long __tmp; \
  asm volatile ("csrr %0, " #reg : "=r"(__tmp)); \
  __tmp; })

#define  NDS_MIE              0x304
// #define IRQ_M_SOFT          	3
// #define MIP_MSIP            	(1 << IRQ_M_SOFT)

#define set_csr(reg, bit) ({ unsigned long __tmp; \
  asm volatile ("csrrs %0, " #reg ", %1" : "=r"(__tmp) : "rK"(bit)); \
  __tmp; })

#define clear_csr(reg, bit) ({ unsigned long __tmp; \
  asm volatile ("csrrc %0, " #reg ", %1" : "=r"(__tmp) : "rK"(bit)); \
  __tmp; })
  
#define HAL_MSWI_ENABLE()                               set_csr(0x304, (1 << 3))


#define IRQ_S_SOFT   1
#define IRQ_H_SOFT   2
#define IRQ_M_SOFT   3
#define IRQ_S_TIMER  5
#define IRQ_H_TIMER  6
#define IRQ_M_TIMER  7
#define IRQ_S_EXT    9
#define IRQ_H_EXT    10
#define IRQ_M_EXT    11
#define IRQ_COP      12
#define IRQ_HOST     13
#define MIP_SSIP            (1U << IRQ_S_SOFT)
#define MIP_HSIP            (1U << IRQ_H_SOFT)
#define MIP_MSIP            (1U << IRQ_M_SOFT)
#define MIP_STIP            (1U << IRQ_S_TIMER)
#define MIP_HTIP            (1U << IRQ_H_TIMER)
#define MIP_MTIP            (1U << IRQ_M_TIMER)
#define MIP_SEIP            (1U << IRQ_S_EXT)
#define MIP_HEIP            (1U << IRQ_H_EXT)
#define MIP_MEIP            (1U << IRQ_M_EXT)

#define CLINT_BASE         0xF0010000
typedef struct CLINT_Type_t
{
    volatile uint32_t MSIP[5];
    volatile uint32_t reserved1[(0x4000U - 0x14U)/4U];
    volatile uint64_t MTIMECMP[5];  /* mtime compare value for each hart. When mtime equals this value, interrupt is generated for particular hart */
    volatile uint32_t reserved2[((0xbff8U - 0x4028U)/4U)];
    volatile uint64_t MTIME;    /* contains the current mtime value */
} CLINT_Type;
#define CLINT    ((CLINT_Type *)CLINT_BASE)
static inline void clear_soft_interrupt(void)
{
    volatile uint32_t reg;
    uint64_t hart_id = read_csr(mhartid);
    CLINT->MSIP[hart_id] = 0x00U;   /*clear soft interrupt for hart0*/
    reg = CLINT->MSIP[hart_id];     /* we read back to make sure it has been written before moving on */
                                    /* todo: verify line above guaranteed and best way to achieve result */
    (void)reg;                      /* use reg to avoid compiler warning */
}
#define MSTATUS_MIE         0x00000008
void __enable_irq(void)
{
    set_csr(mstatus, MSTATUS_MIE);  /* mstatus Register- Machine Interrupt Enable */
}

volatile int enable_cpu1 = 0;
volatile int abxx = 0;
volatile int abcc = 0;
volatile unsigned int hid;
//__attribute__((always_inline)) static inline unsigned int __nds__plic_sw_claim_interrupt(void)
__attribute__((used)) void __nds__plic_sw_claim_interrupt(void)
{
//   //HAL_MSWI_ENABLE();
//   set_csr(0x344, (1 << 3));
//   unsigned int hart_id = read_csr(0xF14);
//   volatile unsigned int *claim_addr = (volatile unsigned int *)(PLIC_SW_BASE +
//                                                                 PLIC_CLAIM_OFFSET +
//                                                                 (hart_id << PLIC_CLAIM_SHIFT_PER_TARGET));
//   return  *claim_addr;

    clear_soft_interrupt();
    set_csr(mie, MIP_MSIP);

    hid = read_csr(mhartid);
    switch (hid) {
        case 0:
            abxx += 1;
            abcc = 10;
            enable_cpu1 = 1;
            CLINT->MSIP[1] = 1;
            while (enable_cpu1 == 1) {;}
            break;
        case 1:
            abxx += 10;
            abcc /= 2;
            enable_cpu1 = 0;
            break;
        case 2:
            abxx += 100;
            break;
        case 3:
            abxx += 1000;
            break;
    }

    /* Put this hart in WFI */
#if 1
    __asm("wfi");
    // clear_soft_interrupt();
    // __enable_irq();
#else
    do
    {
        __asm("wfi");
    }while(0 == (read_csr(mip) & MIP_MSIP));

    /* The hart is out of WFI, clear the SW interrupt. Here onwards application
     * can enable and use any interrupts as required */

    clear_soft_interrupt();

    __enable_irq();
#endif



    //return 0;
}


typedef struct
{
    uint32_t RxTx;
    uint32_t TxFull;
    uint32_t RxEmpty;
    uint32_t EventStatus;
    uint32_t EventPending;
    uint32_t EventEnable;
} UART;
const uint32_t TxEvent = 0b01;
const uint32_t RxEvent = 0b10;
volatile UART *const uart = (UART *)0x60001800;

#define PROC_STACK_SIZE 200

struct process
{
    size_t *sp;
    size_t stack[PROC_STACK_SIZE];
    char name;
};

void init_uart();
void init_processes();
void putc(char c);
void fancy_char(char c);
void swap_processes();

void aaa();
void bbb();

struct process a = {.name = 'a'};
struct process b = {.name = 'b'};
volatile struct process *current_process = NULL;

void init_uart()
{
    uart->EventEnable = RxEvent;
}

void init_processes()
{
    a.sp = &(a.stack[PROC_STACK_SIZE - 1]);
    b.sp = &(b.stack[PROC_STACK_SIZE - 1]);
    *(a.sp--) = (size_t)&aaa;
    *(b.sp--) = (size_t)&bbb;

    for (size_t i = 28; i > 0; i--)
    {
        *(a.sp--) = 0xdeedbeef;
        *(b.sp--) = 0xdeedbeef;
    }
}

void swap_processes()
{
    // consume buffer
    while (!uart->RxEmpty)
        uart->RxTx;
    putc('\n');

    if ('a' == current_process->name)
        current_process = &b;
    else
        current_process = &a;
}

void putc(char c)
{
    uart->RxTx = c;
}

void aaa()
{
    putc('$');
    while (1)
        for (char c = '0'; c <= '9'; c++)
            putc(c);
}

void bbb()
{
    putc('#');
    while (1)
        for (char c = 'a'; c <= 'z'; c++)
            putc(c);
}