1: include .resc file on Renode
    i @scripts/vexriscv.resc

2: load .repl (written in .resc file)
    machine LoadPlatformDescription @platforms/boards/miv-board-smp.repl

3: miv-board-smp.repl
    using "platforms/cpus/litex_vexriscv_smp.repl"

4: litex_vexriscv_smp.repl
    clint: IRQControllers.CoreLevelInterruptor @ sysbus 0xF0010000
        frequency: 1000000
        numberOfTargets: 4
        // 100 is machine level timer interrupt in VexRiscv
        // 101 is machine level software interrupt in VexRiscv
        [0, 1] -> cpu_0@[101, 100]
        [2, 3] -> cpu_1@[101, 100]
        [4, 5] -> cpu_2@[101, 100]
        [6, 7] -> cpu_3@[101, 100]

5: back to .resc
    change to:
        vexriscv.repl

6: