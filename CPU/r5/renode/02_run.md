
# Launch Renode (portable folder)

export PATH="`pwd`:$PATH"
renode

==========================================
=== For vscode (.vscode/settings.josn) ===
==========================================
{
    "restoreTerminals.terminals": [
        {
            "splitTerminals": [
                {
                    "name": "renode",
                    "commands": ["export PATH=\"`pwd`:$PATH\""]
                },
            ]
        },
    ],
}

# Commands

Available commands:
Name              | Description
================================================================================
alias             : sets ALIAS.
allowPrivates     : allow private fields and properties manipulation.
analyzers         : shows available analyzers for peripheral.
commandFromHistory: executes command from history.
createPlatform    : creates a platform.
currentTime       : prints out and logs the current emulation virtual and real time
displayImage      : Displays image in Monitor
execute           : executes a command or the content of a VARIABLE.
help              : prints this help message or info about specified command.
history           : prints command history.
include           : loads a Monitor script, Python code, platform file or a plugin class.
lastLog           : Logs last n logs.
log               : logs messages.
logFile           : sets the output file for logger.
logLevel          : sets logging level for backends.
mach              : list and manipulate machines available in the environment.
macro             : sets MACRO.
numbersMode       : sets the way numbers are displayed.
path              : allows modification of internal 'PATH' variable.
pause             : pauses the emulation.
peripherals       : prints list of registered and named peripherals.
python            : executes the provided python command.
quit              : quits the emulator.
require           : verifies the existence of a variable.
resd              : introspection for RESD files
runMacro          : executes a command or the content of a MACRO.
set               : sets VARIABLE.
showAnalyzer      : opens a peripheral backend analyzer.
start             : starts the emulation.
string            : treat given arguments as a single string.
using             : expose a prefix to avoid typing full object names.
verboseMode       : controls the verbosity of the Monitor.
version           : shows version information.
watch             : executes a command periodically, showing output in monitor
You can also provide a device name to access its methods.
Use <TAB> for auto-completion.