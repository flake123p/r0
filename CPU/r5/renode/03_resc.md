
resc
-> repl (platforms/boards)
    -> cpu repl (optional) (platforms/cpu)
    -> multiple repl (optional)
    -> multiple repl (optional)
    -> ...
-> LoadBinary
-> LoadELF

-----------------------------------------------------------------------------------------------

# resc file

= Renode Script file


# 1st example : quark_c1000

(monitor) include @scripts/single-node/quark_c1000.resc

# Parsing

## 1. using sysbus

    # The `using` command allows the user to omit a prefix when referring to a peripheral.
    # E.g. `using sysbus` allows to refer to the CPU device with `cpu` instead of `sysbus.cpu`.
    using sysbus

## 2. $name?="Quark-C1000" (Name on shell)

    # The ?= notation indicates a default value if the variable is not set.
    $name?="Quark-C1000-Flake"


## 3. REPL = HW desc
    # Load the hardware description from the REPL file.
    machine LoadPlatformDescription @platforms/boards/arduino_101-shield.repl


## 4. Bootloader
    # Set the WAIT_FOR_JTAG GPIO pin. The bootloader stalls if it's set to low.
    gpio OnGPIO 15 true

    # Load the bootloader. The address has to be specified as the binary files do not contain
    # any addressing information.
    sysbus LoadBinary $boot 0xffffe000


## 5. ELF
    # The ELF files already contain information on where to load each part. They also provide symbols, which
    # can be used for execution tracing.
    sysbus LoadELF $bin


## 6. Reset CPU
    # By default, the initial PC is set to the ELF entry point. Since the bootloader has to be started before the target application,
    # the PC needs to be set manually to the correct value.
    sysbus.cpu PC 0xfffffff0