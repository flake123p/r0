
https://renode.readthedocs.io/en/latest/basic/machines.html

# Create

(monitor) mach create

(monitor) mach create "my-machine"


# Switch

(machine-1) mach set "machine-0"

(machine-1) mach set 0


# List peripherals

(machine-0) peripherals


# Loading platforms

(machine-0) machine LoadPlatformDescription @platforms/cpus/miv.repl


# Show sysbus.drr commands

(machine-0) sysbus.drr

example:

(machine-0) sysbus.drr ZeroAll


# Loading binaries

(machine-0) sysbus LoadELF @my-project.elf

(machine-0) sysbus LoadELF @https://remote-server.com/my-project.elf

If you load multiple ELF files, Renode will look for the lowest loaded section of the last ELF file to locate the initial PC 
(or vector table offset on Cortex-M machines). That means the last ELF you load will set up your CPU starting point.

To override this, you can set the initial values manually, using:

sysbus.cpu PC 0xYourValue
or, on Cortex-M:

sysbus.cpu VectorTableOffset 0xYourValue

Renode supports other executable formats like raw binary, UImage and HEX as well. To load them, use 
    LoadBinary, 
    LoadUImage or 
    LoadHEX 
accordingly.


# Clearing the emulation

(machine-0) Clear