
https://renode.readthedocs.io/en/latest/basic/describing_platforms.html

# Defining peripherals

uart0: UART.MiV_CoreUART @ sysbus 0x80000000
    clockFrequency: 66000000


# Connecting peripherals

sensor: Sensors.SI70xx @ i2c0 0x80


# Connecting interrupt

timer: Timers.MiV_CoreTimer @ sysbus 0x1000000
    -> plic @ 31


# Including files

using "platforms/cpus/miv.repl"
using "/tmp/platform.repl"
using "./platform.repl"
using "../other/platform.repl"
