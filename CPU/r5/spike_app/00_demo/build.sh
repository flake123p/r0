#!/bin/bash

#riscv32-unknown-elf-gcc -march=rv32imaf -O2 -g -o hello hello.c -lm -Wl,--verbose -T ./elf32lriscv.lds
riscv32-unknown-elf-gcc -march=rv32imaf -O2 -g -o hello hello.c -lm
riscv32-unknown-elf-objdump -S hello>hello.s
#riscv32-unknown-elf-gcc -march=rv32imaf -O2 -S -o hello.ss hello.c -lm

cp hello /opt/renode/scripts/
