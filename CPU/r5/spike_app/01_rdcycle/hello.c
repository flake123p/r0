#include <stdio.h>
//#include <stdlib.h>
//#include <stdint.h>

static inline int rdcyc() {
  int val;
  asm volatile ("rdcycle %0 ;\n":"=r" (val) ::);
  return val;
}

// Flake: not work on user mode (no work on spike)
#define read_csr(reg) ({ unsigned long __tmp; \
  asm volatile ("csrr %0, " #reg : "=r"(__tmp)); \
  __tmp; })

volatile int aa = 0;
volatile int bb = 0;

int main()
{
    //aa = read_csr(0xB00);
    //bb = read_csr(0xB00);
    aa = rdcyc();
    aa--;
    bb = rdcyc();
    printf("hello %d\n", bb - aa);
    bb = rdcyc();
    printf("hello %d\n", bb - aa);

    return 95; // echo $? to see this on linux
}
