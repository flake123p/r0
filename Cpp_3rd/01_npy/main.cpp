#include <stdio.h>
#include "npy.hpp"
#include <vector>
#include <string>

int main()
{
    {
        const std::string path {"../f64.npy"};
        npy::npy_data d = npy::read_npy<double>(path);

        std::vector<double> data = d.data;
        std::vector<unsigned long> shape = d.shape;
        bool fortran_order = d.fortran_order;

        for (int i = 0; i < data.size(); i++) {
            printf("%f\n", data[i]);
        }
        for (int i = 0; i < shape.size(); i++) {
            printf("shape %d : %ld\n", i, shape[i]);
        }
    }
    {
        const std::string path {"../f32.npy"};
        npy::npy_data d = npy::read_npy<float>(path);

        std::vector<float> data = d.data;
        std::vector<unsigned long> shape = d.shape;
        bool fortran_order = d.fortran_order;

        for (int i = 0; i < data.size(); i++) {
            printf("%f\n", data[i]);
        }
        for (int i = 0; i < shape.size(); i++) {
            printf("shape %d : %ld\n", i, shape[i]);
        }
    }
    return 0;
}