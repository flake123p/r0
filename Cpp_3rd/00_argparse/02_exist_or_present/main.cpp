#include <iostream>
#include <argparse/argparse.hpp>

// argparse::ArgumentParser program("program_name"); // Global is OK

int main(int argc, char *argv[]) {
  argparse::ArgumentParser program("test");

  // Required = Must have
  program.add_argument("-o", "--output")
    .required()
    .help("specify the output file.");

  try {
    program.parse_args(argc, argv);
  }
  catch (const std::exception& err) {
    std::cerr << err.what() << std::endl;
    std::cerr << program;
    std::exit(1);
  }

  if (auto fn = program.present("-o")) {
    auto output = program.get<std::string>("-o");
    std::cout << "output exist: " << output << std::endl;
  } else {
    std::cout << "output NOT exist! " << std::endl;
  }
  return 0;
}