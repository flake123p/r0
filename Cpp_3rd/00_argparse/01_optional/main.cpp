#include <iostream>
#include <argparse/argparse.hpp>

// argparse::ArgumentParser program("program_name"); // Global is OK

int main(int argc, char *argv[]) {
  argparse::ArgumentParser program("test");

  // You can overwrite "-v":version.
  program.add_argument("--verbose")
    .help("increase output verbosity")
    .default_value(false)
    .implicit_value(true);
  
  // When defining flag arguments, you can use the shorthand flag() which is the same as 
  // default_value(false).implicit_value(true).

  try {
    program.parse_args(argc, argv);
  }
  catch (const std::exception& err) {
    std::cerr << err.what() << std::endl;
    std::cerr << program;
    std::exit(1);
  }

  if (program["--verbose"] == true) {
    std::cout << "Verbosity enabled" << std::endl;
  }
  return 0;
}