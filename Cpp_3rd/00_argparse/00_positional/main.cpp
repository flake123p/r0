#include <iostream>
#include <argparse/argparse.hpp>

// argparse::ArgumentParser program("program_name"); // Global is OK

int main(int argc, char *argv[]) {
  argparse::ArgumentParser program("program_name");

  program.add_argument("square")
    .help("display the square of a given integer")
    .scan<'i', int>();

  try {
    program.parse_args(argc, argv);
    printf("Parse done\n");
  }
  catch (const std::exception& err) {
    printf("Error Reason: (Missing positional arg ...)\n");
    std::cerr << err.what() << std::endl;

    printf("\nHelp:\n");
    std::cerr << program;
    return 1;
  }

  auto input = program.get<int>("square");
  std::cout << (input * input) << std::endl;

  bool ghost_exist = false;
  int ghost = 0;
  try {
    ghost = program.get<int>("ghost");
    printf("ghost exist\n");
  }
  catch (const std::exception& err) {
    printf("ghost Not exist\n");
  }
  return 0;
}