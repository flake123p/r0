function(basic_test_CMakeFunc  CURR_TARGET)
    target_include_directories(
        ${CURR_TARGET} PUBLIC
        "${CMAKE_CURRENT_SOURCE_DIR}/../../mod/basic_test"
    )
    if (NOT TARGET basic_test)
        add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../../mod/basic_test ${CMAKE_BINARY_DIR}/basic_test)
    endif()
    target_link_libraries(${CURR_TARGET} PUBLIC basic_test)
endfunction()
