import os
import sys

MODLISTS_FOLDER = os.getcwd()
MOD_FOLDER = MODLISTS_FOLDER + '/../../mod'
MODS = os.listdir( MOD_FOLDER )

print(MODS)

def make_modlist(name):
    print(f'function({name}_CMakeFunc  CURR_TARGET)')
    print( '    target_include_directories(')
    print( '        ${CURR_TARGET} PUBLIC')
    print( '        "${CMAKE_CURRENT_SOURCE_DIR}/../../mod/' + f'{name}"')
    print( '    )')
    print(f'    if (NOT TARGET {name})')
    print( '        add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../../mod/' + f'{name}' + ' ${CMAKE_BINARY_DIR}/' + f'{name})')
    print( '    endif()')
    print( '    target_link_libraries(${CURR_TARGET} PUBLIC ' + f'{name})')
    print( 'endfunction()')

for name in MODS:
    original_stdout = sys.stdout
    with open(name+'.cmake', 'w') as f:
        sys.stdout = f # Change the standard output to the file we created.
        make_modlist(name)
        sys.stdout = original_stdout # Reset the standard output to its original value