function(basic_CMakeFunc  CURR_TARGET)
    target_include_directories(
        ${CURR_TARGET} PUBLIC
        "${CMAKE_CURRENT_SOURCE_DIR}/../../mod"
    )
    if (NOT TARGET basic)
        add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../../mod/basic ${CMAKE_BINARY_DIR}/basic)
    endif()
    target_link_libraries(${CURR_TARGET} PUBLIC basic)
endfunction()
