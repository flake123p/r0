
cd build

lcov -c -d . -o cov.info
lcov --remove cov.info '/usr/*' --output-file cov.info
genhtml cov.info -o report_local
date -R