#include "basic_test_internal.h"

// LCOV_EXCL_START
// LCOV_EXCL_LINE
// LCOV_EXCL_STOP

int test_fmt()
{
    {
        float a = 10.12345678;
        StrFmt b = StrFmt(a);
        std::string c;

        c = b.pr();
        std::cout << c << std::endl;
        BASIC_ASSERT(c == std::string("10.123457"));
        c = b.pr(10, 0, 2);
        BASIC_ASSERT(c == std::string("     10.12"));
        c = b.pr(10, 1, 2);
        BASIC_ASSERT(c == std::string("10.12     "));
    }

    StreamFmt sfmt(std::cout);

    sfmt.Format(std::cout, 30);
    std::cout << "StreamFmt 1: ";
    sfmt.Format(std::cout, 30);
    std::cout << 1.123456789;
    sfmt.Format(std::cout, 30);
    std::cout << ",";
    sfmt.Format(std::cout, 30);
    std::cout << 33.33 << std::endl;

    sfmt.Format(std::cout, 30, 0);
    std::cout << "StreamFmt 2: ";
    sfmt.Format(std::cout, 30, 0);
    std::cout << 1.123456789;
    sfmt.Format(std::cout, 30, 0);
    std::cout << ",";
    sfmt.Format(std::cout, 30, 0);
    std::cout << 33.33 << std::endl;
    
    std::cout << "StreamFmt 2: " << 1.123456789 << std::endl;
    sfmt.Restore(std::cout);
    std::cout << "StreamFmt 3: " << 1.123456789 << std::endl;

    return 0;
}