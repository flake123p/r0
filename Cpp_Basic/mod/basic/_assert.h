#ifndef __ASSERT_H_
#define __ASSERT_H_

#ifndef _ASSERT_LEVEL_
#define _ASSERT_LEVEL_ 2
#endif

#if _ASSERT_LEVEL_ == 0
#define BASIC_ASSERT(...)
#elif _ASSERT_LEVEL_ == 1
#include <assert.h>
#define BASIC_ASSERT assert
#elif _ASSERT_LEVEL_ == 2
#include <stdio.h>
#define BASIC_ASSERT(a) if(a){;}else{FAIL_PRINT();FAIL_PROC();}
#endif

#define FAIL_PRINT() printf("[FAILED] \n Assertion failed:\n  File: %s\n  Function: %s(), line %d\n",__FILE__,__FUNCTION__,__LINE__)
// #define FAIL_PROC() while(1){;}
// #define FAIL_PROC() exit(1)
#define FAIL_PROC() {{printf("SELF TRIGGER SEG_FAULT:\n");}int *x=NULL;*x=0;}

#endif //__ASSERT_H_