#ifndef __FMT_H_
#define __FMT_H_

#include <string>
#include <iostream>
#include <sstream>
#include <iomanip> // setw

template <typename T>
class StrFmt
{
public:
    T var;

    StrFmt(T var)
    {
        this->var = var;
    };

    std::string pr(int w = -1, int align_left = 1, int prec = 6)
    {
        std::stringstream buffer;
        if (align_left) {
            buffer << std::left;
        } else {
            buffer << std::right;
        }
        buffer << std::setw(w) << std::fixed << std::setprecision(prec);
        buffer << var;
        return buffer.str();
    }
};

#include <ios>

class StreamFmt
{
public:
    std::ios_base::fmtflags f;

    template <typename T>
    StreamFmt(T &stream) {
        this->f = stream.flags();
    }

    template <typename T>
    void Format(T &stream, int w = -1, int align_left = 1, int prec = 6)
    {
        // this->f = stream.flags();

        if (align_left) {
            stream << std::left;
        } else {
            stream << std::right;
        }
        stream << std::setw(w) << std::fixed << std::setprecision(prec);
    }

    template <typename T>
    void Restore(T &stream)
    {
        stream.flags(f);
    }
};

#endif //__FMT_H_