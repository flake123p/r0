
R in Visual Studio Code - https://code.visualstudio.com/docs/languages/r

Installation: Linux - https://github.com/REditorSupport/vscode-R/wiki/Installation:-Linux

# 0. apt install
Ref: https://stackoverflow.com/questions/7765429/unable-to-install-r-package-in-ubuntu-11-04
sudo apt-get update
sudo apt-get install libxml2-dev
sudo apt-get install r-cran-xml
// Flake: you can use command Rscript now

# 1
Install R (>= 3.4.0) for your platform.

# 2. In R Terminal:
install.packages("languageserver")

install.packages("rmarkdown")

remotes::install_github("ManuelHentschel/vscDebugger")

install.packages("httpgd")

install.packages("lintr")

# 3. Exit Conda

pip install --user radian

Set these to vscode settings.josn
{
  "r.bracketedPaste": true,
  "r.rterm.linux": "/home/user/.local/bin/radian"
}

# 4. Run
Install R vscode extension
Open R file
Hit the run icon 