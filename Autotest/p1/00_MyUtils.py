import os
import sys
sys.path.append('../../Python/_my_pkgs/MyUtils/')
import check

target_folder = '../../Python/_my_pkgs/MyUtils_test/'

#
# 1. Run test and generate log.log
#
cmd = 'cd ' + target_folder + '; ./cov.sh'
os.system(cmd)

#
# 2. Check log.log
#
log_file = target_folder + 'log.log'

print('PASSED = ', not check.Check_IsAnyFailed(log_file))

print('Covrage = ', check.Check_TotalCoverage(log_file))
