

#ifndef _DESIGN_PATTER_HPP_INCLUDED_

#include <map>
#include "DP_Composite.hpp"
#include "DP_List.hpp"

typedef void (*TrackerDeleteCb)(void *obj);

class Tracker {
private:
    static int trackerState;
    static std::map<void *,TrackerDeleteCb> mapHdl;
    static int RecordProcessDeleteAll(void);
public:
    static int doPrintLog;
    static void Init(void){
        if(trackerState == 0) {
            mapHdl.clear();
            trackerState = 1;
        }
    };
    static int RecordAdd(void *obj, void *deleteCb);
    static int RecordRemove(void *obj);
    static void RecordDump(void);
    

    Tracker(){Init();}
    ~Tracker(){
        trackerState = 2; //blocking RecordRemove() from internal loop
        RecordProcessDeleteAll();
    };
};

class Trackee {
public:
    //void AddDeleteCb(TrackerDeleteCb cb){Tracker::RecordAdd((void *)this, (void *)cb);};
protected:
    Trackee(TrackerDeleteCb cb){Tracker::RecordAdd((void *)this, (void *)cb);};
    virtual ~Trackee(){Tracker::RecordRemove((void *)this);};
public:
    virtual void show(void){};
};

void DesignPatterDemo(void);


#define _DESIGN_PATTER_HPP_INCLUDED_
#endif//_DESIGN_PATTER_HPP_INCLUDED_



