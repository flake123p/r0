

#ifndef _DESIGN_PATTERN_LIST_HPP_INCLUDED_

#include "_LibLinkedList.hpp"

/*
    DPList is hierarchical list, the object is node/head combination
*/
struct DPListMd {
    union {
        DLList_Entry_t en; //entry
        struct {
            void *p0; //for pure head
            void *p1; //for pure head
        };
    };
    union {
        DLList_Head_t he; //head
        struct {
            void *p2; //for pure node
            void *p3; //for pure node
        };
    };
    struct DPListMd *pHeMd;
    void *local; //"this" pointer
    void *owner; //for the class owns this object
};

class DPList {
public:
    struct DPListMd md;
    DPList(){VARCLR(md);DLLIST_HEAD_RESET(&md.he);md.local=(void *)this;};
    virtual ~DPList(){};
    //GET functions
    inline DLList_Entry_t *entry(){return &md.en;};
    inline DLList_Head_t *head(){return &md.he;};
    inline void *prev(){return md.en.prev;};
    inline void *next(){return md.en.next;};
    inline void *first(){return md.he.head;};
    inline void *last(){return md.he.tail_or_self;};
    inline void *local(){return md.local;};
    inline void *owner(){return md.owner;};
    inline struct DPListMd *pHeadMd(){return md.pHeMd;};
    class DPList *pHeadObj(){if(md.pHeMd==NULL)return NULL;return (class DPList *)md.pHeMd->local;};
    class DPList *pFirstObj(){if(md.he.head==NULL)return NULL;return (class DPList *)(((struct DPListMd *)md.he.head)->local);};
    class DPList *pLastObj(){if(md.he.tail_or_self==&(md.he))return NULL;return (class DPList *)(((struct DPListMd *)md.he.tail_or_self)->local);};
    class DPList *pPrevObj(){if(md.en.prev==NULL)return NULL;return (class DPList *)(((struct DPListMd *)md.en.prev)->local);};
    class DPList *pNextObj(){if(md.en.next==NULL)return NULL;return (class DPList *)(((struct DPListMd *)md.en.next)->local);};
    inline class DPList *get_local(DPListMd *md){return (class DPList *)md->local;};

    int is_empty(){return DLLIST_IS_EMPTY(head());};

    virtual void show_node(u32 flag = 0){printf("show_node\n");};
    virtual void show_list(u32 flag = 0)
    {
        struct DPListMd *curr = (struct DPListMd *)first();
        class DPList *local;
        printf("%s(): \n", __func__);
        while (curr) {
            local = (class DPList *)curr->local;
            local->show_node();
            curr = (class DPListMd *)local->next();
        }
    };
    virtual void show_all(DPListMd *pHeadMd, u32 flag = 0)
    {
        struct DPListMd *curr;
        struct DPListMd *curr_first;
        class DPList *local;

        curr = (struct DPListMd *)pHeadMd->he.head;

        while (curr) {
            local = (class DPList *)curr->local;
            local->show_node();

            curr_first = (struct DPListMd *)curr->he.head;
            if (curr_first)
                show_all(curr, flag);

            curr = (class DPListMd *)local->next();
        }
    };
    virtual void show_all(u32 flag = 0)
    {
        show_all(&md, flag);
    }

    //For head mode
    virtual void insert_first(class DPList *node){DLLIST_INSERT_FIRST(head(), node->entry());node->md.pHeMd=&md;};
    virtual void insert_first(class DPList &node){insert_first(&node);};
    virtual void insert_last(class DPList *node){DLLIST_INSERT_LAST(head(), node->entry());node->md.pHeMd=&md;};
    virtual void insert_last(class DPList &node){insert_last(&node);};
    virtual void insert_after(class DPList *node, class DPList *new_node){DLLIST_INSERT_AFTER(head(), node->entry(), new_node->entry());new_node->md.pHeMd=&md;};
    virtual void insert_after(class DPList *node, class DPList &new_node){insert_after(node, &new_node);};
    virtual void insert_after(class DPList &node, class DPList *new_node){insert_after(&node, new_node);};
    virtual void insert_after(class DPList &node, class DPList &new_node){insert_after(&node, &new_node);};
    virtual void remove_first(class DPList *node){DLLIST_REMOVE_FIRST_SAFELY(head());};
    virtual void remove_first(class DPList &node){remove_first(&node);};
    virtual void remove_last(class DPList *node){DLLIST_REMOVE_LAST_SAFELY(head());};
    virtual void remove_last(class DPList &node){remove_last(&node);};
    virtual void remove_node(class DPList *node){DLLIST_REMOVE_NODE(head(), node->entry());};
    virtual void remove_node(class DPList &node){remove_node(&node);};

    virtual void sync_head_md() {
        class DPList *curr = pFirstObj();
        while (curr) {
            curr->md.pHeMd = &md;
            curr = curr->pNextObj();
        }
    }
    virtual void to_new_head(class DPList *node, class DPList *new_head, int do_sync = 1){DLLIST_TO_NEW_HEAD(head(), node->entry(), new_head->head());if(do_sync)new_head->sync_head_md();};
    virtual void to_new_head(class DPList &node, class DPList *new_head, int do_sync = 1){to_new_head(&node, new_head, do_sync);};
    virtual void to_new_head(class DPList *node, class DPList &new_head, int do_sync = 1){to_new_head(node, &new_head, do_sync);};
    virtual void to_new_head(class DPList &node, class DPList &new_head, int do_sync = 1){to_new_head(&node, &new_head, do_sync);};

    //void insert_first(DLList_Entry_t *node){DLLIST_INSERT_FIRST(&md.he, node);};
    //void insert_last(DLList_Entry_t *node){DLLIST_INSERT_LAST(&md.he, node);};
    //void insert_after(DLList_Entry_t *node, DLList_Entry_t *new_node){DLLIST_INSERT_AFTER(&md.he, node, new_node);};

    //virtual void insert_sort(class DPList *node){};
    ///virtual void insert_sort(class DPList &node){};
    //virtual void insert_sort(DLList_Entry_t *node){};

    void reverse_core(struct DPListMd *curr) //Flake recursive is for singly, not very good for doubly
    {
        if (curr) {
            reverse_core((struct DPListMd *)curr->en.next);
        } else {
            DLLIST_HEAD_RESET(&md.he);
            return;
        }
        DLLIST_INSERT_LAST(&md.he, curr);
    };
    void reverse()
    {
        struct DPListMd *curr = (struct DPListMd *)this->first();
        if (curr)
            reverse_core(curr);
    };

    virtual int dispatch_node(class DPList *srvHdl){return 0;};
    virtual int dispatch_list(class DPList *curr) {
        class DPList *currSub;
        while (curr) {
            curr->dispatch_node(this);
            currSub = curr->pFirstObj();
            if (currSub)
                dispatch_list(curr);
            curr = curr->pNextObj();
        }
        return 0;
    };
    virtual int dispatch_list() {
        dispatch_list(pFirstObj());
        return 0;
    };
};

#include <iostream>

template <typename VAL_TYPE> 
class DPDiffList : public DPList {
public: 
    VAL_TYPE val = 0;
    VAL_TYPE diff = 0; //diff to previous
    VAL_TYPE first_val = 0;
    union {
        u32 id ;
        struct {
            u16 id_sub;  //low btyes for sub id
            u16 id_main;
        };
    };

    DPDiffList(){id=0;};
    void set_id(u16 in_main, u16 in_sub){id_main=in_main;id_sub=in_sub;};
    // return if first val changed
    virtual int insert_diff(class DPDiffList *new_node) {
        struct DPListMd *curr = (struct DPListMd *)first();
        class DPDiffList *prev_obj;
        class DPDiffList *curr_obj;

        if (curr == NULL) {
            new_node->diff = new_node->val;
            insert_first(new_node);
            first_val = new_node->diff;//DND(first_val);
            return 1;
        }
        curr_obj = dynamic_cast<class DPDiffList *>(get_local(curr));
        if (curr_obj->diff > new_node->val){
            new_node->diff = new_node->val;
            curr_obj->diff -= new_node->val;
            insert_first(new_node);
            first_val = new_node->diff;//DND(first_val);
            return 1;
        } else {
            VAL_TYPE remain = new_node->val - curr_obj->diff;
            int is_inserted = 0;
            prev_obj = curr_obj;
            curr = (struct DPListMd *)curr->en.next;
            while (curr) {
                curr_obj = dynamic_cast<class DPDiffList *>(get_local(curr));
                if (remain >= curr_obj->diff) {
                    prev_obj = curr_obj;
                    curr = (struct DPListMd *)curr->en.next;
                    remain -= curr_obj->diff;
                    continue;
                } else {
                    is_inserted = 1;
                    new_node->diff = remain;
                    curr_obj->diff -= remain;
                    insert_after(prev_obj, new_node);
                    return 0;
                }
            }
            if (is_inserted == 0) {
                new_node->diff = remain;
                insert_last(new_node);
            }
        }
        return 0;
    };
    virtual int insert_diff(class DPDiffList &new_node) {return insert_diff(&new_node);};

    virtual int insert_diff_all(class DPDiffList *new_node) {
        class DPDiffList *headObj;
        if (insert_diff(new_node) && md.pHeMd) {
            headObj = dynamic_cast<class DPDiffList *>(pHeadObj());
            headObj->remove_node(this);
            val = first_val;
            headObj->insert_diff_all(this);
        }
        return 0;
    };
    virtual int insert_diff_all(class DPDiffList &new_node) {return insert_diff_all(&new_node);};

    virtual void show_node(u32 flag = 0)
    {
        u32 i = 0;
        //u32 main_id = id >> 16;
        for (i=0; i<id_main; i++) {
            printf(" ");
        }
        std::cout << '(' << id_main << '-' << id_sub << "/" << first_val << ')' << val << '/' << diff << '\n';
    };

    virtual void remove_node(class DPDiffList *node){
        struct DPDiffList *next = dynamic_cast<struct DPDiffList *>(node->pNextObj());
        if (next) {
            next->diff += node->diff;
        }
        DPList::remove_node(node);
    };
    virtual void remove_node(class DPDiffList &node){remove_node(&node);};

};

void DP_ListDemo(void);

#define _DESIGN_PATTERN_LIST_HPP_INCLUDED_
#endif//_DESIGN_PATTERN_LIST_HPP_INCLUDED_

