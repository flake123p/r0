
#include "Everything_DesignPattern.hpp"

class TestA : public DPList {
public:
    int val;
    void show_node(u32 flag)
    {
        printf("TestA, val = %d\n", val);
    };
};

class TestB : public DPList {
public:
    int val;
    void show_node(u32 flag)
    {
        printf("TestB, val = %d\n", val);
    };
};

void DP_ListDemo_Basic(void)
{
    class DPList testHe;
    class TestA a1, a2; //DPListNode
    class TestB b1, b2; //DPListNodeII (brief version)
    //printf("123 %p %p\n", &a1, a1.local());
    //printf("123 %p %p\n", &b1, b1.local());
    a1.val = 1;
    a2.val = 2;
    b1.val = 1;
    b2.val = 2;
    testHe.insert_last(&b1);
    testHe.insert_first(a1);
    testHe.insert_last(&b2);
    //testHe.insert_after(a1, a2);
    a1.pHeadObj()->insert_after(&a1, a2);
    testHe.reverse();

    testHe.show_list();
}

void DP_ListDemo_Diff(void)
{
    class DPDiffList<u32> ori,he,a,b,c,d,b1,b2;

    a.val = 7;
    b.val = 7;
    c.val = 14;
    d.val = 10;
    a.id = 1<<16;
    b.id = 1<<16;
    c.id = 1<<16;
    d.id = 1<<16;

    b1.id = 2<<16 | 1;
    b2.id = 2<<16 | 2;
    b1.val = 10;
    b2.val = 15;
    c.insert_diff(b2);
    c.insert_diff(b1);

    he.insert_diff(c);
    he.insert_diff(b);
    he.insert_diff(a);
    he.insert_diff(d);
    he.remove_node(b);
    he.remove_node(a);

    //he.show_all();
    ori.insert_last(he);
    ori.show_all();
}

void DP_ListDemo_Diff_2D(void)
{
    class DPDiffList<u32> ori,a,b,c,a1,a2,c1,c2,c3;
    a.set_id(1, 1);
    b.set_id(1, 2);
    c.set_id(1, 3);
    a1.set_id(2, 11); a1.val = 3;
    a2.set_id(2, 12); a2.val = 10;
    c1.set_id(2, 31); c1.val = 5;
    c2.set_id(2, 32); c2.val = 5;
    c3.set_id(2, 33); c3.val = 8;
    ori.insert_diff_all(a);
    ori.insert_diff_all(b);
    ori.insert_diff_all(c);
    //ori.show_all();
    c.insert_diff_all(c3);
    //ori.show_all();
    //PRLOC;
    a.insert_diff_all(a2);
    //ori.show_all();
    //PRLOC;
    c.insert_diff_all(c2);
    //ori.show_all();
    //PRLOC;
    a.insert_diff_all(a1);
    //ori.show_all();
    //PRLOC;
    a.insert_diff_all(c1);
    ori.show_all();
    PRLOC;
}

class RX : public DPList {
    int dispatch_node(class DPList * srvHdl) {
        printf("This is RX\n");
        return 0;
    };
};

class TX : public DPDiffList<u32> {
    int dispatch_node(class DPList * srvHdl) {
        printf("This is TX\n");
        return 0;
    };
};

void DP_ListDemo_Dispatch(void)
{
    class DPList head, head2;
    class RX r0, r1;
    class TX t0, t1;
    head.insert_last(&r0);
    head.insert_last(&r1);
    head.insert_last(&t0);
    head.insert_last(&t1);
    DNP(&head.md);
    DNP(&head2.md);
    DNP(t0.md.pHeMd);
    //head.dispatch_list();
    head.to_new_head(r1, head2);
    DNP(t0.md.pHeMd);
    //head2.insert_after(t0, r0);
    head2.dispatch_list();
}

void DP_ListDemo(void)
{
    DP_ListDemo_Dispatch();
    PRLOC;
}
