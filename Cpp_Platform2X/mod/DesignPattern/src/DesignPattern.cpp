
#include "Everything_DesignPattern.hpp"

int Tracker::doPrintLog = 0;
int Tracker::trackerState = 0;
std::map<void *,TrackerDeleteCb> Tracker::mapHdl;

int Tracker::RecordAdd(void *obj, void *deleteCb)
{
    Init();
#if 0 //reserved
    std::map<void *,TrackerDeleteCb>::iterator it = mapHdl.find(obj);
    if (it == mapHdl.end()) {
        ;
    } else {
        BASIC_ASSERT(0);
    }
#endif
    std::pair<std::map<void *,TrackerDeleteCb>::iterator,bool> ret;
    ret = mapHdl.insert ( std::pair<void *,TrackerDeleteCb>(obj, (TrackerDeleteCb)deleteCb) );
    if (ret.second==false) {
        BASIC_ASSERT(0);
    }
    return 0;
}

int Tracker::RecordRemove(void *obj)
{
    if (trackerState == 2)
        return 0;

    std::map<void *,TrackerDeleteCb>::iterator it = mapHdl.find(obj);
    if (it == mapHdl.end()) {
        //BASIC_ASSERT(0);
        return 0;
    }
    mapHdl.erase(it);

    return 0;
}

void Tracker::RecordDump(void)
{
    std::map<void *,TrackerDeleteCb>::iterator it;
    class Trackee *curr;
    PRLOC;
    for (it=mapHdl.begin(); it!=mapHdl.end(); it++) {
        printf("Record: 1st=%p, 2nd=%p\n", it->first, it->second);
        curr = (class Trackee *)it->first;
        curr->show();
    }
}

int Tracker::RecordProcessDeleteAll(void)
{
    std::map<void *,TrackerDeleteCb>::iterator it;
    if (doPrintLog) {
        PRLOC;
        RecordDump();
    }
    for (it=mapHdl.begin(); it!=mapHdl.end(); it++) {
//        if (doPrintLog) {
//            DNP(it->second);
//            DNP(it->first);
//        }
        (*((TrackerDeleteCb)(it->second)))((void *)it->first);
    }

    mapHdl.clear();
    return 0;
}

class A : public Trackee {
public:
    int aa;
    static void Destroy(void *obj){delete(static_cast<class A*>(obj));};

    A(void) : Trackee(Destroy) {};
    //A(void){AddDeleteCb(Destroy);PRLOC};
    ~A(){PRLOC};
    void show(void){printf("This is obj A\n");};
};

class B : public Trackee {
public:
    double bb;
    static void Destroy(void *obj){delete((class B*)obj);};

    B(void) : Trackee(Destroy) {};
    //B(void){AddDeleteCb(Destroy);PRLOC};
    ~B(){PRLOC};
    void show(void){printf("This is obj B\n");};
};

Tracker gTracker;
void DesignPatterDemo(void)
{
    Tracker::RecordDump();
    class A a;
    Tracker::RecordDump();
    class B *b = new(class B);
    b = b;
    Tracker::RecordDump();
    //A::Destroy(a);

    gTracker.doPrintLog = 1; //see what's left
}