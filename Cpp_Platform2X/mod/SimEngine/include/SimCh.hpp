
#ifndef _SIM_CH_HPP_INCLUDED_

#include "Everything_Lib_Mgr.hpp"

typedef struct {
    u32 power;
    u32 stamp;
    u32 data32;
    u32 dataRetainCycles;
    // user data
    u32 freq;
    u32 noise;
    void *self;
    void *hdlA;
    void *hdlB;
} SimChSimple32_t;

class SimChSimple32 {
public:
    SimChSimple32_t ct; //content

    SimChSimple32(void){Init();};
    ~SimChSimple32(void){};
    void Init(void){BLOCKCLR(&ct);ct.self=(void *)this;};
    void CarrierSense(u32 timeStamp);
    void Tx(u32 data, u32 timeStamp);
};

#define _SIM_CH_HPP_INCLUDED_
#endif//_SIM_CH_HPP_INCLUDED_