
#include "Everything_SimEngine.hpp"

void SimChSimple32::CarrierSense(u32 timeStamp)
{
    u32 diff = timeStamp - ct.stamp;
    if (diff < 0x80000000) {
        if (diff >= ct.dataRetainCycles) {
            ct.power = 0;
            ct.data32 = 0;
        }
    } else {
        ct.power = 0;
        ct.data32 = 0;
    }
}

void SimChSimple32::Tx(u32 data, u32 timeStamp)
{
    CarrierSense(timeStamp);
    ct.power = 1;
    ct.data32 = ct.data32 | data;
    ct.stamp = timeStamp;
}
