

#ifndef _NET_UTIL_RATE_EXP_HPP_INCLUDED_

class NetExp_Rate {
public:
    int inited;
    u32 error_rate_num;
    u32 *error_rate_default;
    u32 *error_rate;

    NetExp_Rate(){inited=0;error_rate_default=NULL;error_rate=NULL;};
    ~NetExp_Rate(){MM_FREE_SAFELY(error_rate_default);MM_FREE_SAFELY(error_rate);};
    void RateMemInit(u32 num, u32 *default_rate){
        if (inited) {
            MM_FREE_SAFELY(error_rate_default);
            MM_FREE_SAFELY(error_rate);
        }
        inited = 1;
        error_rate_default = (u32 *)MM_ALLOC(sizeof(u32) * num);
        error_rate = (u32 *)MM_ALLOC(sizeof(u32) * num);
        error_rate_num = num;
        MM_CPY(error_rate_default, default_rate, sizeof(u32) * num);
        MM_CPY(error_rate, default_rate, sizeof(u32) * num);
    };
    virtual void ResetToDefaultRate() {
        BASIC_ASSERT(inited == 1);
        MM_CPY(error_rate, error_rate_default, sizeof(u32) * error_rate_num);
    };
    int IsError(u32 index){
        BASIC_ASSERT(inited == 1);
        BASIC_ASSERT(index < error_rate_num);
        return LibUtil_IsPassedOn100(error_rate[index]);
    };
    int IsSuccess(u32 index){
        return !IsError(index);
    };

    u32 GetDefaultRate(u32 index) {
        BASIC_ASSERT(error_rate_default!=NULL);
        BASIC_ASSERT(index<error_rate_num);
        return error_rate_default[index];
    }

    u32 GetCurrentRate(u32 index) {
        BASIC_ASSERT(error_rate!=NULL);
        BASIC_ASSERT(index<error_rate_num);
        return error_rate[index];
    }
};

class NetExp_RateExp : public NetExp {
public:
    class NetExp_Rate *rates;
    class SeqnoBasicFactory tx;
    class SeqnoRxCentral rx;
    u32 pktNum;
    u32 loopCap; //TBD

    NetExp_RateExp(){pktNum=0;rates=NULL;};
    ~NetExp_RateExp(){};

    virtual void Init(u32 pktNum, u32 resultNum, class NetExp_Rate *rates) {
        this->pktNum = pktNum;
        NetExp::Reset(resultNum);
        tx.ReleaseList();
        rx.ReleaseList();
        this->rates = rates;
    }

    virtual void RunOnce_Entry(u32 currIdx) {
        BASIC_ASSERT(rates!=NULL);
        rates->ResetToDefaultRate();

        tx.Reset(0, 1);
        tx.NewNodesOnTheLast(pktNum);
        rx.RxReset(0, pktNum-1);
    };

    int IsSuccess(u32 index){
        BASIC_ASSERT(rates!=NULL);
        return rates->IsSuccess(index);
    };
};

class NetExp_1_Path : public NetExp_RateExp {
public:
    class NetExp_Rate rateObj;

    NetExp_1_Path(){};
    ~NetExp_1_Path(){};

    void Init(u32 pktNum, u32 resultNum, u32 rate){
        rateObj.RateMemInit(1, &rate);
        NetExp_RateExp::Init(pktNum, resultNum, &rateObj);
    };

    virtual double RunOnce(u32 currIdx) {
        u32 totalLoops = 0;
        struct SeqnoNode *node = tx.GetFirst(1);//delete from list
        while (1) {
            totalLoops++;
            if (node && IsSuccess(0)) {
                rx.RxPushNewNode(node);
                //rx.Dump();
                node = tx.GetFirst(1);
            }
            if (rx.RxIsDone()) {
                break;
            }
            if (totalLoops >= pktNum*100000) {
                rx.rxDone = 1;
                break;
            }
        }
        BASIC_ASSERT(rx.RxIsDone());
        //DND(totalLoop);
        return (double)totalLoops;
    };
};

class NetExp_2_Paths : public NetExp_RateExp {
public:
    class NetExp_Rate rateObj;
    DLList_Head_t txq;
    DLList_Head_t txq2;

    NetExp_2_Paths(){};
    ~NetExp_2_Paths(){};

    void Init(u32 pktNum, u32 resultNum, u32 *rates){
        rateObj.RateMemInit(2, rates);
        NetExp_RateExp::Init(pktNum, resultNum, &rateObj);
    };

    virtual void RunOnce_Entry(u32 currIdx) {
        DLLIST_HEAD_RESET(&txq);
        DLLIST_HEAD_RESET(&txq2);
        NetExp_RateExp::RunOnce_Entry(currIdx);
    };

    struct SeqnoNode *GetFirst_Txq() {
        struct SeqnoNode *node = (struct SeqnoNode *)DLLIST_FIRST(&txq);
        DLLIST_REMOVE_FIRST_SAFELY(&txq);
        return node;
    }

    struct SeqnoNode *GetFirst_Txq2() {
        struct SeqnoNode *node = (struct SeqnoNode *)DLLIST_FIRST(&txq2);
        DLLIST_REMOVE_FIRST_SAFELY(&txq2);
        return node;
    }
};

class NetExp_2_Paths_Intv : public NetExp_2_Paths {
public:
    class NetUtil_PktBalanceOnPer100 balance;

    NetExp_2_Paths_Intv(){};
    ~NetExp_2_Paths_Intv(){};

    virtual void RunOnce_Entry(u32 currIdx) {
        u32 i;
        struct SeqnoNode *node;
        u32 per[2];
        u32 balancedResult[2];
        //u32 balancedPktNum = 8;
        //u32 balancedPktNum2 = 4;

        per[0]=rateObj.GetDefaultRate(0);
        per[1]=rateObj.GetDefaultRate(1);
        balance.Init(2, pktNum, per, balancedResult);
        balance.Calc();
        //balance.Dump();

        NetExp_2_Paths::RunOnce_Entry(currIdx);

        for (i=0; i<balancedResult[0]; i++) {
            node = tx.GetFirst(1);
            DLLIST_INSERT_LAST(&txq, node);
        }

        for (i=0; i<balancedResult[1]; i++) {
            node = tx.GetFirst(1);
            DLLIST_INSERT_LAST(&txq2, node);
        }
    };

    virtual double RunOnce(u32 currIdx) {
        u32 totalLoop = 1;
        struct SeqnoNode *node = GetFirst_Txq();//delete from list
        struct SeqnoNode *node2 = GetFirst_Txq2();//delete from list
        
        while (1) {
            if (node && IsSuccess(0)) {
                rx.RxPushNewNode(node);
                //rx.Dump();
                node = GetFirst_Txq();
#if 1
            }
#else
                printf("[%4d][%4d]Path 1, ok\n", totalLoop, error_rate_curr);
            } else 
                printf("[%4d][%4d]Path 1, er\n", totalLoop, error_rate_curr);
#endif
            if (node2 && IsSuccess(1)) {
                rx.RxPushNewNode(node2);
                //rx.Dump();
                node2 = GetFirst_Txq2();
#if 1
            }
#else
                printf("[%4d][%4d]Path 2, ok\n", totalLoop, error_rate_curr);
            } else 
                printf("[%4d][%4d]Path 2, er\n", totalLoop, error_rate_curr);
#endif
            if (rx.RxIsDone()) {
                break;
            }
            totalLoop++;
            if (totalLoop >= pktNum*100000) {
                rx.rxDone = 1;
                break;
            }
        }
        if (node) {
            MM_FREE(node);
        }
        if (node2) {
            MM_FREE(node2);
        }
        BASIC_ASSERT(rx.RxIsDone());
        //DND(totalLoop);
        return (double)totalLoop;
    };
};

class NetExp_2_Paths_DupEx : public NetExp_2_Paths {
public:
    //class NetUtil_PktBalanceOnPer100 balance;
    class SeqnoBasicFactory tx2;

    NetExp_2_Paths_DupEx(){};
    ~NetExp_2_Paths_DupEx(){};

    virtual void RunOnce_Entry(u32 currIdx) {
        NetExp_2_Paths::RunOnce_Entry(currIdx);
        tx2.Reset(0, 1);
        tx2.NewNodesOnTheLast(pktNum);

        /*
        u32 i;
        struct SeqnoNode *node;
        //u32 per[2];
        //u32 balancedResult[2];
        //u32 balancedPktNum = 8;
        //u32 balancedPktNum2 = 4;


        ///per[0]=rateObj.GetDefaultRate(0);
        //per[1]=rateObj.GetDefaultRate(1);
        //balance.Init(2, pktNum, per, balancedResult);
        //balance.Calc();
        //balance.Dump();

        for (i=0; i<pktNum; i++) {
            node = tx.GetFirst(1);
            DLLIST_INSERT_LAST(&txq, node);
        }

        for (i=0; i<pktNum; i++) {
            node = tx2.GetFirst(1);
            DLLIST_INSERT_LAST(&txq2, node);
        }
        */
    };

    virtual double RunOnce(u32 currIdx) {
        u32 totalLoop = 1;
        int path_1_success = 0;
        int path_2_success = 0;
        u32 path_1_seqno;
        u32 path_2_seqno;
        struct SeqnoNode *temp;
        struct SeqnoNode *node = tx.GetFirst(1);//delete from list
        struct SeqnoNode *node2 = tx2.GetFirst(1);//delete from list
        while (1) {
            path_1_success = 0;
            path_2_success = 0;
            if (node && IsSuccess(0)) {
                path_1_success = 1;
                path_1_seqno = node->seqno;
                rx.RxPushNewNode(node);
                node = tx.GetFirst(1);
            }

            if (node2 && IsSuccess(1)) {
                path_2_success = 1;
                path_2_seqno = node2->seqno;
                rx.RxPushNewNode(node2);
                node2 = tx2.GetFirst(1);
            }

            if (path_1_success) {
                if (node2 && node2->seqno == path_1_seqno) {
                    MM_FREE(node2);
                    node2 = tx2.GetFirst(1);
                } else {
                    temp = tx2.FindSeqno(path_1_seqno, 1); //find and remove
                    MM_FREE_SAFELY(temp);
                }
            }

            if (path_2_success) {
                if (node && node->seqno == path_2_seqno) {
                    MM_FREE(node);
                    node = tx.GetFirst(1);
                } else {
                    temp = tx.FindSeqno(path_2_seqno, 1); //find and remove
                    MM_FREE_SAFELY(temp);
                }
            }

            if (rx.RxIsDone()) {
                break;
            }
            totalLoop++;
            if (totalLoop >= pktNum*100000) {
                rx.rxDone = 1;
                break;
            }
        }
        if (node) {
            MM_FREE(node);
        }
        if (node2) {
            MM_FREE(node2);
        }
        BASIC_ASSERT(rx.RxIsDone());
        //DND(totalLoop);
        return (double)totalLoop;
    };
};

class NetExp_2_Paths_Dup : public NetExp_2_Paths {
public:
    //class NetUtil_PktBalanceOnPer100 balance;
    class SeqnoBasicFactory tx2;

    NetExp_2_Paths_Dup(){};
    ~NetExp_2_Paths_Dup(){};

    virtual void RunOnce_Entry(u32 currIdx) {
        NetExp_2_Paths::RunOnce_Entry(currIdx);
        tx2.Reset(0, 1);
        tx2.NewNodesOnTheLast(pktNum);
    };

    virtual double RunOnce(u32 currIdx) {
        u32 totalLoop = 1;
        struct SeqnoNode *node = tx.GetFirst(1);//delete from list
        struct SeqnoNode *node2 = tx2.GetFirst(1);//delete from list
        while (1) {
            if (node && IsSuccess(0)) {
                rx.RxPushNewNode(node);
                node = tx.GetFirst(1);
            }

            if (node2 && IsSuccess(1)) {
                rx.RxPushNewNode(node2);
                node2 = tx2.GetFirst(1);
            }

            if (rx.RxIsDone()) {
                break;
            }
            totalLoop++;
            if (totalLoop >= pktNum*100000) {
                rx.rxDone = 1;
                break;
            }
        }
        if (node) {
            MM_FREE(node);
        }
        if (node2) {
            MM_FREE(node2);
        }
        BASIC_ASSERT(rx.RxIsDone());
        //DND(totalLoop);
        return (double)totalLoop;
    };
};

#define _NET_UTIL_RATE_EXP_HPP_INCLUDED_
#endif//_NET_UTIL_RATE_EXP_HPP_INCLUDED_





