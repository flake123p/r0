

#ifndef _NET_UTIL_HPP_INCLUDED_

struct SeqnoNode {
    DLList_Entry_t entry;
    u32 seqno;
    void *link;
};


/*
    for tx:
        create a list of SeqnoNode without seqno!!

    for rx:
        find in list with sequential sequence number, in other words: find next seqno
*/
class SeqnoBasicFactory {
public:
    u32 nextSeqno;
    u32 seqnoIncrement;
    DLList_Head_t *pHead; // runtime head (use default head in default)
    DLList_Head_t head;   // default head

    SeqnoBasicFactory(){nextSeqno=0;seqnoIncrement=1;pHead=&head;DLLIST_HEAD_RESET(&head);};
    ~SeqnoBasicFactory(){ReleaseList();};

    void Reset(u32 nextSeqno, u32 seqnoIncrement) {
        this->nextSeqno = nextSeqno;
        this->seqnoIncrement = seqnoIncrement;
        DLLIST_FREE_ALL(pHead);
        pHead=&head;
        DLLIST_HEAD_RESET(&head);
    }
    virtual u32 NewSeqno(){u32 num = nextSeqno; nextSeqno += seqnoIncrement ;return num;};
    struct SeqnoNode *NewNode(u32 newSeqno) {
        struct SeqnoNode *curr = (struct SeqnoNode *)MM_ALLOC(sizeof(struct SeqnoNode));
        curr->seqno = newSeqno;
        return curr;
    };
    struct SeqnoNode *GetFirst(int delFromList = 1) {
        struct SeqnoNode *curr;
        BASIC_ASSERT(pHead != NULL);
        curr = (struct SeqnoNode *)DLLIST_FIRST(pHead);
        if (curr && delFromList) {
            DLLIST_REMOVE_FIRST_SAFELY(pHead);
        }
        return curr;
    };
    struct SeqnoNode *FindSeqno(u32 seqno, int delFromList = 1) {
        struct SeqnoNode *curr;
        BASIC_ASSERT(pHead != NULL);
        DLLIST_FOREACH(pHead, curr, struct SeqnoNode) {
            if (curr->seqno == seqno) {
                if (delFromList) {
                    DLLIST_REMOVE_NODE(pHead, curr);
                }
                return curr;
            }
        }
        return NULL;
    };
    //return first
    struct SeqnoNode *NewNodesOnTheLast(u32 num) {
        struct SeqnoNode *first = NULL;
        struct SeqnoNode *curr;
        u32 i;
        BASIC_ASSERT(pHead != NULL);
        for (i = 0; i < num; i++) {
            curr = (struct SeqnoNode *)MM_ALLOC(sizeof(struct SeqnoNode));
            DLLIST_INSERT_LAST(pHead, curr);
            curr->seqno = NewSeqno();
            if (i == 0)
                first = curr;
        }
        return first;
    };

    void ReleaseList() {
        BASIC_ASSERT(pHead != NULL);
        DLLIST_FREE_ALL(pHead);
    };

    int InsertNodeWithSeqnoSearch(struct SeqnoNode *newNode) {
        struct SeqnoNode *prev, *curr;
        int i =0;

        BASIC_ASSERT(pHead != NULL);
        curr = (struct SeqnoNode *)DLLIST_FIRST(pHead);
        DLLIST_FOREACH(pHead, curr, struct SeqnoNode) {
            if (i == 0) {
                if (curr->seqno >= newNode->seqno) {
                    DLLIST_INSERT_FIRST(pHead, newNode);
                    return 0;
                }
            } else {
                if (curr->seqno > newNode->seqno) {
                    DLLIST_INSERT_AFTER(pHead, prev, newNode);
                    return 0;
                }
            }
            prev = curr;
            i++;
        }
        DLLIST_INSERT_LAST(pHead, newNode);
        return 0;
    };
    //List merge : TBD
    //int ListMergeWithSeqnoSort(DLList_Head_t *pHead, DLList_Head_t *pNewHead);
    void Dump() {
        struct SeqnoNode *curr;
        printf("SeqNo: ");
        DLLIST_FOREACH(pHead, curr, struct SeqnoNode) {
            printf("%d ", curr->seqno);
        }
        printf("\n");
    };
};

class SeqnoRxCentral : public SeqnoBasicFactory {
public:
    int rxDone;
    u32 rxNextSeqno;
    u32 rxFinalSeqno;

    SeqnoRxCentral(){rxDone=0;rxNextSeqno=0;rxFinalSeqno=0;};
    ~SeqnoRxCentral(){};

    void RxReset(u32 nextSeqno, u32 finalSeqno) {
        rxDone = 0;
        rxNextSeqno = nextSeqno;
        rxFinalSeqno = finalSeqno;
        ReleaseList();
    };
    int RxIsDone(){return rxDone;};
    void RxCheckListWithNextSeqno() {
        struct SeqnoNode *curr = FindSeqno(rxNextSeqno, 1);

        while (curr) {
            MM_FREE(curr);
            if (rxNextSeqno == rxFinalSeqno) {
                rxDone = 1;
                return;
            }
            rxNextSeqno += 1;
            curr = FindSeqno(rxNextSeqno, 1);
        }
    };
    // return rxDone
    int RxPushNewNode(struct SeqnoNode *newNode) {
        if (rxNextSeqno == newNode->seqno) {
            MM_FREE(newNode);
            if (rxNextSeqno == rxFinalSeqno) {
                rxDone = 1;
                return rxDone;
            }
            rxNextSeqno += 1;
        } else {
            InsertNodeWithSeqnoSearch(newNode);
        }

        RxCheckListWithNextSeqno();
        return rxDone;
    };
    int RxPushNewSeqno(u32 newSeqno) {
        if (rxNextSeqno == newSeqno) {
            if (rxNextSeqno == rxFinalSeqno) {
                rxDone = 1;
                return rxDone;
            }
            rxNextSeqno += 1;
        } else {
            struct SeqnoNode *curr = (struct SeqnoNode *)MM_ALLOC(sizeof(struct SeqnoNode));
            curr->seqno = newSeqno;
            InsertNodeWithSeqnoSearch(curr);
        }

        RxCheckListWithNextSeqno();
        return rxDone;
    };
};

class NetExp {
public:
    u32 resultNum;
    double *resultAry;
    u8 cb[64]; //control buffer

    class NetExp *innerExp;

    NetExp(){resultAry = NULL; innerExp = NULL;};
    ~NetExp(){MM_FREE_SAFELY(resultAry);};

    void Reset(u32 resultNum) {
        MM_FREE_SAFELY(resultAry);
        this->resultNum = resultNum;
        resultAry = MM_ALLOC_TYPE(double, resultNum);
    };
    virtual double ReturnResult(){return CalcAverage();};
    virtual void RunOnce_Entry(u32 currIdx) {};
    virtual void RunOnce_Exit(u32 currIdx) {};
    virtual double RunOnce(u32 currIdx) {return (double)currIdx;};
    virtual void RunAll_Entry(class NetExp *outter = NULL) {
    };
    virtual void RunAll_Exit(class NetExp *outter = NULL) {
    };
    virtual void RunAll(class NetExp *outter = NULL) {
        u32 i;
        BASIC_ASSERT(resultAry != NULL);
        RunAll_Entry(outter);
        for (i = 0; i < resultNum; i++) {
            if (innerExp) {
                innerExp->RunAll(this);
                resultAry[i] = innerExp->ReturnResult();
            } else {
                RunOnce_Entry(i);
                resultAry[i] = RunOnce(i);
                RunOnce_Exit(i);
            }
        }
        RunAll_Exit(outter);
    };
    virtual void Run(class NetExp *outter = NULL) {
        return RunAll(outter);
    };
    double CalcSum() {
        double ret = 0;
        u32 i;
        for (i = 0; i < resultNum; i++) {
            ret += resultAry[i];
        }
        return ret;
    };
    double CalcAverage() {
        return CalcSum() / resultNum;
    };
    void Dump() {
        printf("Sum=%f, Avg=%f\n", CalcSum(), CalcAverage());
    }
    void Dump_Verbose() {
        u32 i;
        for (i = 0; i < resultNum; i++) {
            printf("resultAry[%4u] = %f\n", i, resultAry[i]);
        }
        printf("Sum=%f, Avg=%f\n", CalcSum(), CalcAverage());
    }
};

class NetExp_SimpleRate : public NetExp {
public:
    int inited;
    u32 error_rate_start;
    u32 error_rate_increment;
    u32 error_rate_curr;

    NetExp_SimpleRate(){inited=0;};
    ~NetExp_SimpleRate(){};
    void RateReset(u32 start, u32 increment = 0){
        error_rate_curr = error_rate_start = start;
        error_rate_increment = increment;
        inited = 1;
    };
    virtual void RunAll_Entry(class NetExp *outter = NULL) {
        BASIC_ASSERT(inited);
        error_rate_curr = error_rate_start;
    };
    virtual void RunOnce_Exit(u32 currIdx) {
        BASIC_ASSERT(inited);
        error_rate_curr += error_rate_increment;
    };
    int IsError(){return LibUtil_IsPassedOn100(error_rate_curr);};
    int IsSuccess(){return !IsError();};
};

class NetExp_SinglePath : public NetExp_SimpleRate {
public:
    class SeqnoBasicFactory tx;
    class SeqnoRxCentral rx;
    u32 pktNum;
    u32 loopCap; //TBD

    NetExp_SinglePath(){pktNum=0;};
    ~NetExp_SinglePath(){};

    virtual void Init(u32 pktNum, u32 resultNum, u32 start, u32 increment = 0) {
        this->pktNum = pktNum;
        Reset(resultNum);
        RateReset(start, increment);
        tx.ReleaseList();
        rx.ReleaseList();
    }

    virtual void RunOnce_Entry(u32 currIdx) {RunExp_Entry_Single(currIdx);};
    virtual double RunOnce(u32 currIdx) {return RunExp_Single(currIdx);};

    virtual void RunExp_Entry_Single(u32 currIdx) {
        tx.Reset(0, 1);
        tx.NewNodesOnTheLast(pktNum);
        rx.RxReset(0, pktNum-1);
        //loopCap = pktNum * 10000 / (error_rate_curr + 1)
    };
    virtual void RunOnce_Exit(u32 currIdx) {
        NetExp_SimpleRate::RunOnce_Exit(currIdx);
    };
    virtual double RunExp_Single(u32 currIdx) {
        u32 totalLoop = 1;
        struct SeqnoNode *node = tx.GetFirst(1);//delete from list
        while (1) {
            if (node && IsSuccess()) {
                rx.RxPushNewNode(node);
                //rx.Dump();
                node = tx.GetFirst(1);
            }
            if (rx.RxIsDone()) {
                break;
            }
            totalLoop++;
            if (totalLoop >= pktNum*100000) {
                rx.rxDone = 1;
                break;
            }
        }
        BASIC_ASSERT(rx.RxIsDone());
        //DND(totalLoop);
        return (double)totalLoop;
    };
};

class NetExp_DualPath : public NetExp_SinglePath {
public:
    class SeqnoBasicFactory tx2;
    virtual void Init(u32 pktNum, u32 resultNum, u32 start, u32 increment = 0) {
        NetExp_SinglePath::Init(pktNum, resultNum, start, increment);
        tx2.ReleaseList();
    }

    virtual void RunOnce_Entry(u32 currIdx) {RunExp_Entry_Intv(currIdx);};
    virtual double RunOnce(u32 currIdx) {return RunExp_Intv(currIdx);};

    virtual void RunExp_Entry_Intv(u32 currIdx) {
        tx.Reset(0, 2);
        tx2.Reset(1, 2);
        tx.NewNodesOnTheLast(pktNum/2 + pktNum%2);
        tx2.NewNodesOnTheLast(pktNum/2);
        rx.RxReset(0, pktNum-1);
    };

    virtual double RunExp_Intv(u32 currIdx) {
        u32 totalLoop = 1;
        struct SeqnoNode *node = tx.GetFirst(1);//delete from list
        struct SeqnoNode *node2 = tx2.GetFirst(1);//delete from list
        while (1) {
            if (node && IsSuccess()) {
                rx.RxPushNewNode(node);
                //rx.Dump();
                node = tx.GetFirst(1);
#if 1
            }
#else
                printf("[%4d][%4d]Path 1, ok\n", totalLoop, error_rate_curr);
            } else 
                printf("[%4d][%4d]Path 1, er\n", totalLoop, error_rate_curr);
#endif
            if (node2 && IsSuccess()) {
                rx.RxPushNewNode(node2);
                //rx.Dump();
                node2 = tx2.GetFirst(1);
#if 1
            }
#else
                printf("[%4d][%4d]Path 2, ok\n", totalLoop, error_rate_curr);
            } else 
                printf("[%4d][%4d]Path 2, er\n", totalLoop, error_rate_curr);
#endif
            if (rx.RxIsDone()) {
                break;
            }
            totalLoop++;
            if (totalLoop >= pktNum*100000) {
                rx.rxDone = 1;
                break;
            }
        }
        if (node) {
            MM_FREE(node);
        }
        if (node2) {
            MM_FREE(node2);
        }
        BASIC_ASSERT(rx.RxIsDone());
        //DND(totalLoop);
        return (double)totalLoop;
    };
};

class NetExp_DualPath_Dup : public NetExp_DualPath {
public:
    virtual void RunOnce_Entry(u32 currIdx) {RunExp_Entry_Dup(currIdx);};
    virtual double RunOnce(u32 currIdx) {return RunExp_Dup(currIdx);};

    virtual void RunExp_Entry_Dup(u32 currIdx) {
        tx.Reset(0, 1);
        tx2.Reset(0, 1);
        tx.NewNodesOnTheLast(pktNum);
        tx2.NewNodesOnTheLast(pktNum);
        rx.RxReset(0, pktNum-1);
    };
    virtual double RunExp_Dup(u32 currIdx) {
        u32 totalLoop = 1;
        //int node_1_Hanging = 1, node_2_Hanging = 1;
        struct SeqnoNode *node = tx.GetFirst(1);//delete from list
        struct SeqnoNode *node2 = tx2.GetFirst(1);//delete from list
        while (1) {
            if (node && IsSuccess()) {
                rx.RxPushNewNode(node);
                //rx.Dump();
                node = tx.GetFirst(1);
            }

            if (node2 && IsSuccess()) {
                rx.RxPushNewNode(node2);
                //rx.Dump();
                node2 = tx2.GetFirst(1);
            }

            if (rx.RxIsDone()) {
                break;
            }
            totalLoop++;
            if (totalLoop >= pktNum*100000) {
                rx.rxDone = 1;
                break;
            }
        }
        if (node) {
            MM_FREE(node);
        }
        if (node2) {
            MM_FREE(node2);
        }
        BASIC_ASSERT(rx.RxIsDone());
        //DND(totalLoop);
        return (double)totalLoop;
    };
};

class NetExp_DualPath_DupEx : public NetExp_DualPath_Dup {
public:
    //virtual void RunOnce_Entry(u32 currIdx) {RunExp_Entry_Dup(currIdx);};
    virtual double RunOnce(u32 currIdx) {return RunExp_DupEx(currIdx);};

    virtual double RunExp_DupEx(u32 currIdx) {
        u32 totalLoop = 1;
        int path_1_success = 0;
        int path_2_success = 0;
        u32 path_1_seqno;
        u32 path_2_seqno;
        struct SeqnoNode *node = tx.GetFirst(1);//delete from list
        struct SeqnoNode *node2 = tx2.GetFirst(1);//delete from list
        struct SeqnoNode *temp;
        while (1) {
            path_1_success = 0;
            path_2_success = 0;
            if (node && IsSuccess()) {
                path_1_success = 1;
                path_1_seqno = node->seqno;
                rx.RxPushNewNode(node);
                node = tx.GetFirst(1);
            }

            if (node2 && IsSuccess()) {
                path_2_success = 1;
                path_2_seqno = node2->seqno;
                rx.RxPushNewNode(node2);
                node2 = tx2.GetFirst(1);
            }

            if (path_1_success) {
                if (node2 && node2->seqno == path_1_seqno) {
                    MM_FREE(node2);
                    node2 = tx2.GetFirst(1);
                } else {
                    temp = tx2.FindSeqno(path_1_seqno, 1); //find and remove
                    MM_FREE_SAFELY(temp);
                }
            }

            if (path_2_success) {
                if (node && node->seqno == path_2_seqno) {
                    MM_FREE(node);
                    node = tx.GetFirst(1);
                } else {
                    temp = tx.FindSeqno(path_2_seqno, 1); //find and remove
                    MM_FREE_SAFELY(temp);
                }
            }

            if (rx.RxIsDone()) {
                break;
            }
            totalLoop++;
            if (totalLoop >= pktNum*100000) {
                rx.rxDone = 1;
                break;
            }
        }
        if (node) {
            MM_FREE(node);
        }
        if (node2) {
            MM_FREE(node2);
        }
        BASIC_ASSERT(rx.RxIsDone());
        //DND(totalLoop);
        return (double)totalLoop;
    };
};

class NetExp_DualPath_Hybrid : public NetExp_DualPath_DupEx {
public:
    u32 threshold;
    virtual void RunOnce_Entry(u32 currIdx) {
        if (error_rate_curr > threshold)
            RunExp_Entry_Dup(currIdx);
        else
            RunExp_Entry_Intv(currIdx);
    };
    virtual double RunOnce(u32 currIdx) {
        if (error_rate_curr > threshold)
            return RunExp_Dup(currIdx);
        else
            return RunExp_Intv(currIdx);
    };
};

class NetExp_DualPath_HybridEx : public NetExp_DualPath_Hybrid {
public:
    virtual double RunOnce(u32 currIdx) {
        if (error_rate_curr > threshold)
            return RunExp_DupEx(currIdx);
        else
            return RunExp_Intv(currIdx);
    };
};

// PER100 = For rate is decimal 0~100(u32).
class NetUtil_PktBalanceOnPer100 {
public:
    u32 path_num;       //input
    u32 pkt_num;        //input
    u32 *path_per_ary;  //input
    u32 *path_pkts;     //input & return result
    NetUtil_PktBalanceOnPer100(){};
    ~NetUtil_PktBalanceOnPer100(){};

    void Init(u32 path_num, u32 pkt_num, u32 *path_per_ary, u32 *path_pkts) {
        BASIC_ASSERT(path_per_ary!=NULL);
        BASIC_ASSERT(path_pkts!=NULL);
        this->path_num = path_num;
        this->pkt_num = pkt_num;
        this->path_per_ary = path_per_ary;
        this->path_pkts = path_pkts;
    };

    void Calc() {
        u32 i,psr_sum=0,remain=0,best_path=0,best_per=0xFFFFFFFF;
        u32 *path_psr_ary=NULL;
        BASIC_ASSERT(path_per_ary!=NULL);
        BASIC_ASSERT(path_pkts!=NULL);
        path_psr_ary = (u32 *)MM_ALLOC(sizeof(u32) * path_num);

        for(i=0;i<path_num;i++) {
            // psr
            path_psr_ary[i]=100-path_per_ary[i];
            // psr sum
            psr_sum+=path_psr_ary[i];
            // best per
            if (path_per_ary[i]<best_per) {
                best_per=path_per_ary[i];
                best_path=i;
            }
        }
        BASIC_ASSERT(psr_sum>0);

        // 1st, use division to get balanced pkt num
        for(i=0;i<path_num;i++) {
            //DNU(i);
            //DNU(pkt_num);
            //DNU(path_per_ary[i]);
            //DNU(per_sum);
            path_pkts[i] = (pkt_num * path_psr_ary[i]) / psr_sum;
        }

        // 2nd, add remain packets to best PER path(not good for "multiple best paths")
        for(i=0;i<path_num;i++) {
            remain += path_pkts[i];
        }
        BASIC_ASSERT(remain<=pkt_num);
        remain = pkt_num - remain;
        if (remain) {
            path_pkts[best_path]+=remain;
        }

        MM_FREE_SAFELY(path_psr_ary);
    };

    void Dump() {
        DNU(path_num);
        DNU(pkt_num);
        ARRAYDUMPU(path_per_ary, path_num);
        ARRAYDUMPU(path_pkts, path_num);
    };

    void Demo2Path(u32 pkt_num, u32 per_1, u32 per_2) {
        u32 per[2];
        u32 result[2];
        per[0] = per_1;
        per[1] = per_2;
        Init(2, pkt_num, per, result);
        Calc();
        Dump();
    }
};

#include <math.h>       /* sqrt */
class NetUtil_AsymPerToSymPer_2Paths {
public:
    double avg;
    double root_square_avg;
    double avg_diff;
    double exp_rate_start;
    double exp_rate_end;
    double estimate_range;

    NetUtil_AsymPerToSymPer_2Paths(){estimate_range=0.03;};
    ~NetUtil_AsymPerToSymPer_2Paths(){};

    void Calc(double r1, double r2, int verbose = 0) {
        r1 = r1/100.0;
        r2 = r2/100.0;
        avg = (r1+r2)/2.0;
        {
            double psr1 = 1.0 - r1;
            double psr2 = 1.0 - r2;
            root_square_avg = (sqrt(r1*r2) + (1.0-sqrt(psr1*psr2))) / 2.0;
        }

        if (avg > root_square_avg) {
            avg_diff = avg - root_square_avg;
            exp_rate_start = root_square_avg - estimate_range;
            exp_rate_end = avg + estimate_range;
        } else {
            avg_diff = root_square_avg - avg;
            exp_rate_start = avg - estimate_range;
            exp_rate_end = root_square_avg + estimate_range;
        }


        if (verbose) {
            DNF(r1);
            DNF(r2);
            DNF(avg);
            DNF(root_square_avg);
            DNF(avg_diff);
            DNF(exp_rate_start);
            DNF(exp_rate_end);
        }
    };
};

#define _NET_UTIL_HPP_INCLUDED_
#endif//_NET_UTIL_HPP_INCLUDED_





