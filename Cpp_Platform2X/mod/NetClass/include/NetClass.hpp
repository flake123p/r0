

#ifndef _NET_CLASS_HPP_INCLUDED_

/*

NetClass - eazy classes

NetCsma - basic tx rx, (Right now "NetTxRx.c" is not created)

NetPhy - L1 common part
NetBle - PAN, L1 + L2
NetEther, NetWifi - LAN, L1 + L2
NetIp - L3

NetUtil - Utility

For experimental sim -
    NetClass
*/


#include "NetMedia.hpp"
#include "NetCsma.hpp"
#include "NetUtil.hpp"
#include "NetUtil_RateExp.hpp"

void NetClass_Demo(void);


#define _NET_CLASS_HPP_INCLUDED_
#endif//_NET_CLASS_HPP_INCLUDED_

