

#ifndef _NET_CSMA_HPP_INCLUDED_

typedef enum {
    CSMA_STATE_NONE             = 0,
    CSMA_STATE_IN_BACKOFF,
    CSMA_STATE_INITED,
    CSMA_STATE_CH_AVAILABLE,
    CSMA_STATE_CH_OCCUPIED,
    CSMA_STATE_TX_ING,
    CSMA_STATE_TX_COLLISION,
} CSMA_STATE_t;

typedef struct {
    u32 prevPower;
    u32 power;
    u32 data;
} CSMA_Ch_t;

typedef struct {
    u32 prevState;
    u32 state; //CSMA_STATE_t
    u32 backoff_ctr; // if this is true, than keep waiting
    u32 collision_accu; // If power == 2, this accu += 1. Collsion is true if this counter is bigger than or equal to 2.
    u32 collision_backoff; //another parameter for collision backoff, used 1P at least.

    Common_CB2_t txCb;
    Common_CB2_t txCollisionCb;
    Common_CB2_t txSuccessCb;
    void *txCbHdl;
    void *txCbPrivate;
    void *userData;
} CSMA_Content_t;

enum {
    CSMA_MODE_1P = 0, // 1-persistent
    CSMA_MODE_NP, // Non-persistent
    CSMA_MODE_PP, // P-persistent
    CSMA_MODE_OP, // TBD, O-persistent, https://en.wikipedia.org/wiki/Carrier-sense_multiple_access
    CSMA_MODE_CD,
    CSMA_MODE_CA,
    CSMA_MODE_CR, // TBD, CSMA with Collision Resolution, https://en.wikipedia.org/wiki/Carrier-sense_multiple_access
    CSMA_MODE_VT, // TBD, Virtual time CSMA, https://en.wikipedia.org/wiki/Carrier-sense_multiple_access

    CSMA_MODE_SW,
};

/*
    BusyBackoff     TX_Immediate    CollisionBackoff
1P  None            Yes             None
NP  Random          Yes             Random
PP  None            p               None (T.B.D.)

Flake: 
Issue of 1P :
    1P will make infinite loop in simulator, because every TX requests are actually happend at the same time!
Solution :
    Add an under table backoff in low probability.

Collision measurement:
    True if power == 2, 2 times in a row
    True if power > 2

Simulated CSense Algorithm:
    1. sense prev power (CURRENT)
    2. sense curr power, binding sense&TX, random dev selection
*/

// Simplest base class
class NetCSMA
{
public:
    CSMA_Ch_t *ch;
    CSMA_Content_t ct;

    NetCSMA(CSMA_Ch_t *in_ch = NULL)
    {
        Init(in_ch);
    };
    ~NetCSMA(void){};
    virtual int Init(CSMA_Ch_t *in_ch = NULL)
    {
        VARCLR(ct);
        ch=in_ch;
        if (ch) {
            BLOCKCLR(ch);
        }
        return 0;
    }
    virtual int GetMode(void){return CSMA_MODE_1P;};
    virtual int ChannelStartNewRound(int doErrorCheck = 1)
    {
        ch->prevPower = ch->power;
        ch->power = 0;
        ch->data = 0;
        return 0;
    };
    virtual int IsNowInBackoff(void){return (ct.backoff_ctr!=0);};
    virtual int BackoffCountdown(void){if(ct.backoff_ctr)ct.backoff_ctr--;return 0;};
    virtual int BackoffCheck_FSM(void)
    {
        if (IsNowInBackoff()) {
            ct.state = CSMA_STATE_IN_BACKOFF;
        } else {
            ct.state = CSMA_STATE_INITED;
        }
        BackoffCountdown();
        return 0;
    };
    virtual int CarrierSense(void)
    {
        BASIC_ASSERT(ch!=NULL);
        if (ch->prevPower) return 0;
        if (ch->power) return 0;
        return 1;
    };
    virtual int CarrierSense_FSM(void)
    {
        if (ct.state == CSMA_STATE_INITED) {
            if (CarrierSense()) {
                ct.state = CSMA_STATE_CH_AVAILABLE;
            } else {
                if (ct.prevState == CSMA_STATE_TX_ING) {
                    ct.state = CSMA_STATE_CH_AVAILABLE;
                } else {
                    ct.state = CSMA_STATE_CH_OCCUPIED;
                    ChannelBusyBackoff();
                }
            }
        }
        return 0;
    };
    virtual int ChannelBusyBackoff(void){/*do nothing in 1P*/return 0;};
    virtual int Transmit(u32 data, u32 power)
    {
        BASIC_ASSERT(ch!=NULL);
        ch->data|=data;
        ch->power+=power;
        return 0;
    };
    virtual int TransmitCollisionSense(void)
    {
        BASIC_ASSERT(ch!=NULL);
        if (ch->power < 2) {
            ct.collision_accu = 0;
            return 0;
        } else if (ch->power == 2) {
            if (ct.collision_accu >= 1)
                return 1;
            else
                ct.collision_accu++;
        } else {
            ct.collision_accu+=2;
            return 1;
        }
        return 0;
    };
    virtual int TransmitStart_FSM(void)
    {
        if (ct.state == CSMA_STATE_CH_AVAILABLE) {
            ct.state = CSMA_STATE_TX_ING;
            if (ct.txCb) {
                return (*(ct.txCb))(ct.txCbHdl, ct.txCbPrivate);
            }
        }
        return 0;
    };
    virtual int TransmitCollisionSense_FSM(void)
    {
        if(ct.state == CSMA_STATE_TX_ING) {
            if (TransmitCollisionSense()) {
                ct.state = CSMA_STATE_TX_COLLISION;
                TransmitCollisionBackoff();
                ct.backoff_ctr = 1;
                ct.backoff_ctr += ct.collision_backoff;
                if (ct.txCollisionCb) {
                    return (*(ct.txCollisionCb))(ct.txCbHdl, ct.txCbPrivate);
                }
            } else {
                if (ct.txSuccessCb) {
                    return (*(ct.txSuccessCb))(ct.txCbHdl, ct.txCbPrivate);
                }
            }
        }
        return 0;
    }
    virtual int TransmitCollisionBackoff(void){/*do nothing in 1P*/ct.collision_accu = 0;return 0;};
    virtual int FSM_0_StartNewRound(void)
    {
        return 0;
    };
    virtual int FSM_1_BackoffCheck(void)
    {
        return BackoffCheck_FSM();
    };
    virtual int FSM_2_CarrierSense(void)
    {
        return CarrierSense_FSM();
    };
    virtual int FSM_3_TxStart(void)
    {
        return TransmitStart_FSM();
    };
    virtual int FSM_4_TxCollisionSense(void)
    {
        return TransmitCollisionSense_FSM();
    }
    virtual int FSM_5_EndOfOneRound(void)
    {
        ct.prevState = ct.state;
        return 0;
    }
};

class NetCSMA_SW : public NetCSMA
{
public:
    u32 csma_sw_data;
    NetCSMA_SW(CSMA_Ch_t *in_ch = NULL){Init(in_ch);csma_sw_data=999;};
    int GetMode(void){DPD(csma_sw_data);return CSMA_MODE_SW;};
    int FSM_2_CarrierSense(void)
    {
        //move CarrierSense_FSM() to FSM_3_TxStart() = never collision
        return 0;
    };
    int FSM_3_TxStart(void)
    {
        CarrierSense_FSM();
        return TransmitStart_FSM();
    };
};

void NetCsma_Demo(void);



#define _NET_CSMA_HPP_INCLUDED_
#endif//_NET_CSMA_HPP_INCLUDED_




