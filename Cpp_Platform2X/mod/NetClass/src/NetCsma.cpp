
#include "Everything_NetClass.hpp"

// Simplest Algorithm
int NetCsma_CarrierSense(u32 power)
{
    int is_channel_available;

    switch (power) {
        case 0: is_channel_available = 1; break;
        default: is_channel_available = 0; break;
    }

    return is_channel_available;
}





void NetCsma_ChannelStartNewRound(class NetCSMA *dev)
{
    dev->ChannelStartNewRound();
}

void NetCsma_StartOfNewRound(class NetCSMA **devs, u32 devNum)
{
    u32 i;
    for (i = 0; i < devNum; i++) {
        devs[i]->FSM_0_StartNewRound();
    }
}

void NetCsma_BackoffCheck(class NetCSMA **devs, u32 devNum)
{
    u32 i;
    for (i = 0; i < devNum; i++) {
        devs[i]->FSM_1_BackoffCheck();
    }
}

void NetCsma_CarrierSense(class NetCSMA **devs, u32 devNum)
{
    u32 i;
    for (i = 0; i < devNum; i++) {
        devs[i]->FSM_2_CarrierSense();
    }
}

void NetCsma_TxStart(class NetCSMA **devs, u32 devNum)
{
    u32 i;
    for (i = 0; i < devNum; i++) {
        devs[i]->FSM_3_TxStart();
    }
}

void NetCsma_TxCollisionSense(class NetCSMA **devs, u32 devNum)
{
    u32 i;
    for (i = 0; i < devNum; i++) {
        devs[i]->FSM_4_TxCollisionSense();
    }
}

void NetCsma_EndOfOneRound(class NetCSMA **devs, u32 devNum)
{
    u32 i;
    for (i = 0; i < devNum; i++) {
        devs[i]->FSM_5_EndOfOneRound();
    }
}

typedef struct {
    u32 txDone;
    u32 txIdx;
    u8  *txBuf;
} Tx_Demo_t;
#define TX_DEMO_BUF_LEN ( 6 )

int NetCsma_TxCb(class NetCSMA *csmaDev, Tx_Demo_t *txInfo)
{
    if (txInfo->txDone == 0) {
        csmaDev->Transmit(txInfo->txBuf[txInfo->txIdx], 1);
    }
    return 0;
}

int NetCsma_TxCollisionCb(class NetCSMA *csmaDev, Tx_Demo_t *txInfo)
{
    txInfo->txIdx = 0;
    printf("TX collision\n");
    return 0;
}

int NetCsma_TxSuccessCb(class NetCSMA *csmaDev, Tx_Demo_t *txInfo)
{
    txInfo->txIdx++;
    if (txInfo->txIdx == TX_DEMO_BUF_LEN) {
        txInfo->txDone = 1;
        printf("TX done !!!!!!!!!!!!!!!!!\n");
    } else {
        printf("TX well\n");
    }
    return 0;
}

static Tx_Demo_t txInfo0 = {0};
static Tx_Demo_t txInfo1 = {0};
static u8 txbuf0[] = {0xAA, 0xAA, 0x01, 0x02, 0x03, 0x04};
static u8 txbuf1[] = {0xAA, 0xAA, 0x0A, 0x0B, 0x0C, 0x0D};

void NetCsma_Demo(void)
{
#define AVOID_1P_COLLISION ( 1 )
#define LOOP_MAX  (10)

    // init
    CSMA_Ch_t ch10;
#if 1
    class NetCSMA dev0(&ch10), dev1(&ch10);
#else
    class NetCSMA_SW dev0(&ch10), dev1(&ch10);
#endif
    u32 loop = 0;

#if AVOID_1P_COLLISION
    dev0.ct.collision_backoff = 1;
#endif
    txInfo0.txBuf = txbuf0;
    txInfo1.txBuf = txbuf1;
    dev0.ct.txCbHdl = &dev0;
    dev1.ct.txCbHdl = &dev1;
    dev0.ct.txCbPrivate = &txInfo0;
    dev1.ct.txCbPrivate = &txInfo1;
    dev0.ct.txCb = (Common_CB2_t)NetCsma_TxCb;
    dev1.ct.txCb = (Common_CB2_t)NetCsma_TxCb;
    dev0.ct.txCollisionCb = (Common_CB2_t)NetCsma_TxCollisionCb;
    dev1.ct.txCollisionCb = (Common_CB2_t)NetCsma_TxCollisionCb;
    dev0.ct.txSuccessCb = (Common_CB2_t)NetCsma_TxSuccessCb;
    dev1.ct.txSuccessCb = (Common_CB2_t)NetCsma_TxSuccessCb;

    class NetCSMA *devs[] = {&dev0, &dev1};
    //class NetCSMA *pdev0 = &dev0;
    //class NetCSMA *pdev1 = &dev1;
    printf("CSMA mode of dev0 = %d\n", devs[0]->GetMode());
    printf("CSMA mode of dev1 = %d\n", devs[1]->GetMode());

    for (loop = 0; loop < LOOP_MAX; loop++)
    {
        printf("[LOOP %d]\n", loop);

        NetCsma_ChannelStartNewRound(devs[0]);

        NetCsma_StartOfNewRound(devs, 2);

        NetCsma_BackoffCheck(devs, 2);

        NetCsma_CarrierSense(devs, 2);

        NetCsma_TxStart(devs, 2);

        NetCsma_TxCollisionSense(devs, 2);

        NetCsma_EndOfOneRound(devs, 2);
    }
}
