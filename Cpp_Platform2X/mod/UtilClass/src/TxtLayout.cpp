
#include "Everything_UtilClass.hpp"

int TxtLayout::Init(void)
{
    struct TxtLayout_Node_t *node;
    if (ct.bufSize == 0)
        ct.bufSize = 512;

    for (u32 i=0; i<ct.colNum; i++) {
        node = (struct TxtLayout_Node_t *)MM_ALLOC(ct.bufSize + sizeof(struct TxtLayout_Node_t));
        //DLLIST_INSERT_LAST(&head, node);
        BLOCKCLR(node);
        node->buf[0] = 0;
        vec.push_back(node);
    }

    ct.smartBuf = (char *)MM_ALLOC(ct.bufSize);
    ct.smartBuf[0] = 0;
    ct.headStr = (char *)MM_ALLOC(ct.bufSize);
    ct.headStr[0] = 0;
    return 0;
}

int TxtLayout::Deinit(void)
{
#if 0
    struct TxtLayout_Node_t *curr, *prev;

    DLLIST_WHILE_START(&head, curr, struct TxtLayout_Node_t) {
        prev = curr;
        DLLIST_WHILE_NEXT(curr, struct TxtLayout_Node_t);
        MM_FREE(prev);
    }
#else
    u32 i;

    for (i=0; i<vec.size(); i++) {
        MM_FREE(vec[i]);
    }
    vec.clear();

    if (ct.smartBuf) {
        MM_FREE(ct.smartBuf);
        ct.smartBuf = NULL;
    }
    if (ct.headStr) {
        MM_FREE(ct.headStr);
        ct.headStr = NULL;
    }
#endif
    return 0;
}

void TxtLayout::Print(void)
{
    u32 strLen;
    u32 i,j;

    for (i=0; i<vec.size(); i++) {
        strLen = strlen(Buf(i));
        printf("%s", Buf(i));
        if (strLen < ct.colWidth) {
            //print padding
            strLen = ct.colWidth - strLen;
            for (j=0; j<strLen; j++) {
                printf(" ");
            }
        }
    }
    printf("\n");
}

// If the width is 20, the max printed string is "19" !!
void TxtLayout::SmartPrint(void)
{
    char *p;
    u32 printLen;
    u32 i;
    int firstRun = 1;
    int loopCheck = 1;
    u32 deftWidth = ct.colWidth;
    u32 currWidth;

    while (loopCheck)
    {
        loopCheck = 0;
        for (i=0; i<vec.size(); i++) {
            if (vec[i]->width)
                currWidth = vec[i]->width;
            else
                currWidth = deftWidth;
            if (firstRun) {
                vec[i]->remain = strlen(Buf(i));
                vec[i]->curr = 0;
                if (i == 0)
                    printf("%s", ct.headStr);
            } else {
                if (i == 0)
                    UtilClass_PrintSpace(strlen(ct.headStr));
            }
            printLen = vec[i]->remain;
            if (printLen) {
                if (printLen > currWidth - 1) {
                    loopCheck++;
                    printLen = currWidth - 1;
                    p = ct.smartBuf;
                    memcpy(ct.smartBuf, &(vec[i]->buf[vec[i]->curr]), printLen);
                } else {
                    p = (char *)&(vec[i]->buf[vec[i]->curr]);
                }
                vec[i]->remain -= printLen;
                vec[i]->curr += printLen;
                printf("%s", p);
                vec[i]->buf[0] = 0;
            }

            if (printLen < currWidth) {
                UtilClass_PrintSpace(currWidth - printLen);
            }
        }
        printf("\n");
        firstRun = 0;
    }
}

char *TxtLayout::NextLineBuf(u32 index)
{
    char *pCurr;
    char *p = (char *)vec[index]->buf;
    u32 strLen = strlen(p);

    if (strLen == 0)
        return p;

    pCurr = &p[strLen];

    while (1)
    {
        if (strLen <= ct.colWidth - 1) {
            memset(pCurr, ' ', ct.colWidth-strLen-1);
            pCurr += (ct.colWidth-strLen-1);
            break;
        } else {
            strLen -= (ct.colWidth - 1);
            pCurr += (ct.colWidth - 1);
        }
    }
    return pCurr;
};

void TxtLayout_Demo(void)
{
    class TxtLayout layout;
    layout.ct.colNum = 2;
    layout.ct.colWidth = 25;
    layout.Init();
    strcpy(layout.ct.headStr, "head]]] ");

    sprintf(layout.Buf(0), "num=%d,width=%d", layout.ct.colNum, layout.ct.colWidth);
    sprintf(layout.Buf(1), "sssssssss");
    layout.SmartPrint();
    sprintf(layout.Buf(0), "123ttttttttttttttttttttttttttttsss");
    sprintf(layout.Buf(1), "456");
    layout.SmartPrint();
    sprintf(layout.Buf(0), "abc");
    sprintf(layout.Buf(1), "def");
    layout.SmartPrint();
}