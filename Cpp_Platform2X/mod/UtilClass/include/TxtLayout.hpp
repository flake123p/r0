

#ifndef _TXT_LAYOUT_HPP_INCLUDED_

#include "Everything_Lib_Mgr.hpp"
#include <vector>

struct TxtLayout_Node_t {
    //DLList_Head_t entry;
    u32 curr;
    u32 remain;
    u32 width;
    u8 buf[];
};

struct TxtLayout_t {
    u32 colNum;
    u32 colWidth;
    u32 bufSize;
    char *smartBuf;
    char *headStr;
};

class TxtLayout {
public:
    struct TxtLayout_t ct;
    std::vector<struct TxtLayout_Node_t *> vec;
    DLList_Head_t head;

    TxtLayout(void){DLLIST_HEAD_RESET(&head);BLOCKCLR(&ct);vec.clear();};
    ~TxtLayout(void){Deinit();};
    int Init(void);
    int Deinit(void);
    char *Buf(u32 index){return (char *)vec[index]->buf;};
    char *NextLineBuf(u32 index);

    void Print(void);
    void SmartPrint(void);
};

void TxtLayout_Demo(void);

#define _TXT_LAYOUT_HPP_INCLUDED_
#endif//_TXT_LAYOUT_HPP_INCLUDED_



