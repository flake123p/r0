

#ifndef _EVERYTHING_TEST_MOD3_HPP_INCLUDED_


#include "Everything_Lib_Mgr.hpp"

#ifndef TCHAR
#define TCHAR char
#endif

#ifndef _T
#define _T(a) a
#endif

#ifndef _tcscmp
#define _tcscmp strcmp
#endif

#ifndef _tcschr
#define _tcschr strchr
#endif

#include "XGetopt.h"


#define _EVERYTHING_TEST_MOD3_HPP_INCLUDED_
#endif//_EVERYTHING_TEST_MOD3_HPP_INCLUDED_



