
#include "Everything_App.hpp"

#define TOTAL_RUN (1000)
#define TOTAL_PKT (12)

u32 a_tx_ctr; //seqno, will be 0 to TOTAL_PKT
u32 b_rx_ctr;
u32 b_rx_q[10];
u32 b_rx_q_flag[10];

u32 ch_pkt_success[2];
u32 ch_pkt_hdr_seqno[2];
u32 ch_pkt_payload[250][2];

double single_avg;
double dup_avg;
double intv_avg;

// return true for success
int success(u32 success_rate)
{
    u32 rand = (u32)LibUtil_GetRand() % 100;
    //printf("rand=%d, sr=%d\n", rand, success_rate);
    if (rand < success_rate)
        return 1;
    else
        return 0;
}

void channel__a_tx_to_b(u32 ch_idx, u32 success_rate)
{
    ch_pkt_success[ch_idx] = 0;
    if (success(success_rate) == 0)
        return;

    ch_pkt_success[ch_idx] = 1;
    if (a_tx_ctr < TOTAL_PKT) {
        ch_pkt_hdr_seqno[ch_idx] = a_tx_ctr;
        //printf("a seq=%d, ctr=%d\n", ch_pkt_hdr_seqno[ch_idx], a_tx_ctr);
        a_tx_ctr++;
    }
}

void channel__rx_on_b(u32 ch_idx)
{
    if (ch_pkt_success[ch_idx]) {
        if (ch_pkt_hdr_seqno[ch_idx] == b_rx_ctr) {
            //printf("b seq=%d, ctr=%d\n", ch_pkt_hdr_seqno[ch_idx], b_rx_ctr);
            b_rx_ctr++;
        }
    }
}

int test_single_channel(u32 success_rate)
{
    u32 curr_cmpl_rounds = 0; // 1 round = 1ms, with 4 + 1000 bytes, no error = 1Mbps
    u32 accu_cmpl_rounds = 0;
    u32 total_run_i = 0;

    LibUtil_InitRand();

    for (total_run_i = 0; total_run_i < TOTAL_RUN; total_run_i++) {
        curr_cmpl_rounds = 0;
        a_tx_ctr = 0;
        b_rx_ctr = 0;
        while (1) {
            curr_cmpl_rounds++;
            channel__a_tx_to_b(0, success_rate);
            channel__rx_on_b(0);
            if (b_rx_ctr == TOTAL_PKT) {
                accu_cmpl_rounds += curr_cmpl_rounds;
                break;
            }
            //DND(curr_cmpl_rounds);
            BASIC_ASSERT(curr_cmpl_rounds < (TOTAL_PKT*100));
        }
        //printf("curr_cmpl_rounds = %d\n", curr_cmpl_rounds);
    }

    {
        double avg = (double)accu_cmpl_rounds;
        avg = avg / TOTAL_RUN;
        //printf("avg rounds = %f, rate = %f Kbps\n", avg, 1000000/avg);
        printf("avg rounds = %f\n", avg);

        single_avg = avg;
    }

    return 0;
}

void channel_dup__a_tx_to_b(u32 success_rate)
{
    u32 a_tx_ctr_temp = a_tx_ctr;
    u32 tx_ok = 0;
    u32 ch_idx;

    for (ch_idx = 0; ch_idx < 2; ch_idx++) {
        ch_pkt_success[ch_idx] = 0;
        if (success(success_rate)) {
            ch_pkt_success[ch_idx] = 1;
            if (a_tx_ctr_temp < TOTAL_PKT) {
                ch_pkt_hdr_seqno[ch_idx] = a_tx_ctr_temp;
                //printf("a seq=%d, ctr=%d\n", ch_pkt_hdr_seqno[ch_idx], a_tx_ctr);
                tx_ok = 1;
            }
        }
    }

    if (tx_ok)
        a_tx_ctr++;
}

void channel_dup__rx_on_b(void)
{
    u32 ch_idx;
    for (ch_idx = 0; ch_idx < 2; ch_idx++) {
        if (ch_pkt_success[ch_idx]) {
            if (ch_pkt_hdr_seqno[ch_idx] == b_rx_ctr) {
                //printf("b seq=%d, ctr=%d\n", ch_pkt_hdr_seqno[ch_idx], b_rx_ctr);
                b_rx_ctr++;
                return;
            }
        }
    }
}

int test_two_channel_dup(u32 success_rate)
{
    u32 curr_cmpl_rounds = 0; // 1 round = 1ms, with 4 + 1000 bytes, no error = 1Mbps
    u32 accu_cmpl_rounds = 0;
    u32 total_run_i = 0;

    LibUtil_InitRand();

    for (total_run_i = 0; total_run_i < TOTAL_RUN; total_run_i++) {
        curr_cmpl_rounds = 0;
        a_tx_ctr = 0;
        b_rx_ctr = 0;
        while (1) {
            curr_cmpl_rounds++;
            channel_dup__a_tx_to_b(success_rate);
            channel_dup__rx_on_b();
            if (b_rx_ctr == TOTAL_PKT) {
                accu_cmpl_rounds += curr_cmpl_rounds;
                break;
            }
            //DND(curr_cmpl_rounds);
            BASIC_ASSERT(curr_cmpl_rounds < (TOTAL_PKT*100));
        }
        //printf("curr_cmpl_rounds = %d\n", curr_cmpl_rounds);
    }

    {
        double avg = (double)accu_cmpl_rounds;
        avg = avg / TOTAL_RUN;
        //printf("avg rounds = %f, rate = %f Kbps\n", avg, 1000000/avg);
        printf("avg rounds = %f\n", avg);

        dup_avg = avg;
    }

    return 0;
}

u32 a_tx_ctr_i0; //seqno, will be 0 to TOTAL_PKT
u32 a_tx_ctr_i1;
u32 b_rx_ctr_i0;
u32 b_rx_ctr_i1;

void channel_interleaved__a_tx_to_b(u32 success_rate)
{
    ch_pkt_success[0] = 0;
    if (success(success_rate)) {
        ch_pkt_success[0] = 1;
        if (a_tx_ctr_i0 < TOTAL_PKT - 1) {
            ch_pkt_hdr_seqno[0] = a_tx_ctr_i0;
            //printf("a seq=%d, ctr=%d\n", ch_pkt_hdr_seqno[ch_idx], a_tx_ctr);
            a_tx_ctr_i0+=2;
        }
    }

    ch_pkt_success[1] = 0;
    if (success(success_rate)) {
        ch_pkt_success[1] = 1;
        if (a_tx_ctr_i1 < TOTAL_PKT) {
            ch_pkt_hdr_seqno[1] = a_tx_ctr_i1;
            //printf("a seq=%d, ctr=%d\n", ch_pkt_hdr_seqno[ch_idx], a_tx_ctr);
            a_tx_ctr_i1+=2;
        }
    }
}

void channel_interleaved__rx_on_b(void)
{
    if (ch_pkt_success[0]) {
        if (ch_pkt_hdr_seqno[0] == b_rx_ctr_i0) {
            //printf("b seq=%d, ctr=%d\n", ch_pkt_hdr_seqno[ch_idx], b_rx_ctr);
            b_rx_ctr_i0+=2;
        }
    }

    if (ch_pkt_success[1]) {
        if (ch_pkt_hdr_seqno[1] == b_rx_ctr_i1) {
            //printf("b seq=%d, ctr=%d\n", ch_pkt_hdr_seqno[ch_idx], b_rx_ctr);
            b_rx_ctr_i1+=2;
        }
    }
}

int test_two_channel_interleaved(u32 success_rate)
{
    u32 curr_cmpl_rounds = 0; // 1 round = 1ms, with 4 + 1000 bytes, no error = 1Mbps
    u32 accu_cmpl_rounds = 0;
    u32 total_run_i = 0;

    LibUtil_InitRand();

    for (total_run_i = 0; total_run_i < TOTAL_RUN; total_run_i++) {
        curr_cmpl_rounds = 0;
        a_tx_ctr_i0 = 0;
        a_tx_ctr_i1 = 1;
        b_rx_ctr_i0 = 0;
        b_rx_ctr_i1 = 1;
        while (1) {
            curr_cmpl_rounds++;
            channel_interleaved__a_tx_to_b(success_rate);
            channel_interleaved__rx_on_b();
            if ((b_rx_ctr_i0 == TOTAL_PKT) && (b_rx_ctr_i1 == TOTAL_PKT+1)) {
                accu_cmpl_rounds += curr_cmpl_rounds;
                break;
            }
            //DND(curr_cmpl_rounds);
            BASIC_ASSERT(curr_cmpl_rounds < (TOTAL_PKT*100));
        }
        //printf("curr_cmpl_rounds = %d\n", curr_cmpl_rounds);
    }

    {
        double avg = (double)accu_cmpl_rounds;
        avg = avg / TOTAL_RUN;
        //printf("avg rounds = %f, rate = %f Kbps\n", avg, 1000000/avg);
        printf("avg rounds = %f\n", avg);

        intv_avg = avg;
    }

    return 0;
}


int main(int argc, char *argv[])
{
    test_single_channel(100);
    test_single_channel(10);

    test_two_channel_dup(2);

    test_two_channel_interleaved(2);

    //test_two_channel_interleaved(100);

    printf("dup  rate = %f\n", dup_avg/single_avg);
    printf("intv rate = %f\n", intv_avg/single_avg);
    printf("intv is faster in = %f\n", dup_avg/single_avg - intv_avg/single_avg);

    return 0;
}

