
#include "Everything_App.hpp"

void SimChSimple32Test(void)
{
    class SimChSimple32 ch;

    ch.ct.dataRetainCycles = 10;
    BASIC_ASSERT(ch.ct.power == 0);

    ch.Tx(0x65, 0);
    //DUMPNX(ch.ct.data32);
    BASIC_ASSERT(ch.ct.data32 == 0x65);
    BASIC_ASSERT(ch.ct.power == 1);
    
    ch.CarrierSense(9);
    //DUMPNX(ch.ct.data32);
    BASIC_ASSERT(ch.ct.data32 == 0x65);
    BASIC_ASSERT(ch.ct.power == 1);

    ch.CarrierSense(10);
    //DUMPNX(ch.ct.data32);
    BASIC_ASSERT(ch.ct.data32 == 0);
    BASIC_ASSERT(ch.ct.power == 0);

    ch.Tx(0x0F, 11);
    ch.Tx(0xF0, 12);
    BASIC_ASSERT(ch.ct.data32 == 0xFF);

    ch.CarrierSense(21);
    BASIC_ASSERT(ch.ct.data32 == 0xFF);
    ch.CarrierSense(22);
    BASIC_ASSERT(ch.ct.data32 == 0);

    printf("ALL PASS!!\n");

    A_CSMA_Demo();
}

/*
CSMA/CD
Preamble <1 byte 0xA5>
    LEN <1 byte>
    DATA <LEN byte>
end_symbol <2 byte 0 nothing>

Pre TX probe:
waiting 1 byte end_symbol(no power in channel)
*/
SimEngine_Desc A_SimDb;
class SimTime3 A_TimeDb;
class SimChSimple32 A_Ch;
SimTime3_Desc Aar,Abr,Acr,Aat,Abt,Act;
u32 AstopFlag = BIT_0|BIT_1;//|BIT_2;
class TxtLayout Alayout;
/*
a,b send at beginning begin
c recv at beginning
*/
#define A_PREAMBLE 0xA5
#define A_TX_EN     BIT_0
#define A_RX_EN     BIT_1
#define A_DISABLE   BIT_31
typedef struct {
    u32 TxCtrl;
    u32 TxState;
    u32 RxCtrl;
    u32 RxState;
    u32 txlen_done;
    u32 txlen;
    u32 rxlen_target;
    u32 rxlen;
    u8 txbuf[10];
    u8 rxbuf[10];
    class SimChSimple32 *pCh;
} Ainst_t;
class Ainst {
public:
    Ainst_t ct;
    Ainst(void){memset(&ct,0,sizeof(Ainst_t));};
    ~Ainst(void){};
};
class Ainst Ainsta, Ainstb, Ainstc;
u32 A_SimTime3_DemoCBxx(SimTime3_Desc *pDesc)
{
    class SimTime3 *timeDb = (class SimTime3 *)pDesc->timeDb;
    class Ainst *pInst = (class Ainst *)pDesc->hdlA;
    class SimChSimple32 *pCh = (class SimChSimple32 *)pInst->ct.pCh;
    u32 *pCtrl = (u32 *)pDesc->hdlB;
    u32 layoutIdx = (u32)pDesc->hdlD;

    if (FLG_CHK(*pCtrl, A_DISABLE))
        return 0;

    //printf("ss currTime = %lu\n", (unsigned long)timeDb->currTime);

    if (FLG_CHK(*pCtrl, A_TX_EN))
    {
        pDesc->remain_time = 1;
        switch (pInst->ct.TxState)
        {
            case 0: //
                pCh->CarrierSense((u32)timeDb->currTime);
                if (pCh->ct.power == 0) {
                    pInst->ct.TxState = 1;
                    sprintf(Alayout.Buf(layoutIdx), "tx-corr-1st OK");
                } else {
                    sprintf(Alayout.Buf(layoutIdx), "tx-corr-1st NG");
                }
                break;
            case 1: //
                pCh->CarrierSense((u32)timeDb->currTime);
                if (pCh->ct.power == 0) {
                    pInst->ct.TxState = 2;
                    pCh->Tx(A_PREAMBLE, (u32)timeDb->currTime);
                    sprintf(Alayout.Buf(layoutIdx), "tx-preamble");
                } else {
                    pInst->ct.TxState = 0;
                    sprintf(Alayout.Buf(layoutIdx), "tx-corr-2nd NG");
                }
                break;
            case 2: //
                if (pInst->ct.txlen_done == pInst->ct.txlen) {
                    pDesc->remain_time = 0;
                    //printf("TX len = %d, DONE\n", pInst->ct.txlen);
                    sprintf(Alayout.Buf(layoutIdx), "tx-done");
                    break;
                }
                pCh->CarrierSense((u32)timeDb->currTime);
                if (pCh->ct.power == 0) {
                    pCh->Tx(pInst->ct.txbuf[pInst->ct.txlen_done], (u32)timeDb->currTime);
                    pInst->ct.txlen_done++;
                    sprintf(Alayout.Buf(layoutIdx), "tx-ing OK");
                } else {
                    pInst->ct.txlen_done = 0;
                    pInst->ct.TxState = 0;
                    sprintf(Alayout.Buf(layoutIdx), "tx-ing NG");
                }
                break;
        }
        //printf("TX len = %d\n", pInst->ct.txlen);
    }

    if (FLG_CHK(*pCtrl, A_RX_EN))
    {
        //printf("RX len = %d\n", pInst->ct.rxlen);
        pCh->CarrierSense((u32)timeDb->currTime);

        //DUMPD(timeDb->currTime);
        //DUMPD(pCh->ct.power);
        //DUMPNX(pCh->ct.data32);

        switch (pInst->ct.RxState)
        {
            case 0: //
                pCh->CarrierSense((u32)timeDb->currTime);
                if (pCh->ct.power == 1 && pCh->ct.data32 == 0xA5) {
                    pInst->ct.RxState = 1;
                    
                    sprintf(Alayout.Buf(layoutIdx), "rx-corr OK");
                } else {
                    sprintf(Alayout.Buf(layoutIdx), "rx-corr NG");
                }
                break;
            case 1: //
                pCh->CarrierSense((u32)timeDb->currTime);
                if (pCh->ct.power == 1) {
                    pInst->ct.RxState = 2;
                    pInst->ct.rxlen_target = pCh->ct.data32;
                    sprintf(Alayout.Buf(layoutIdx), "rx-len OK");
                } else {
                    pInst->ct.RxState = 0;
                    sprintf(Alayout.Buf(layoutIdx), "rx-len NG");
                }
                break;
            case 2: //
                pCh->CarrierSense((u32)timeDb->currTime);
                if (pCh->ct.power == 1) {
                    pInst->ct.rxbuf[pInst->ct.rxlen] = pCh->ct.data32;
                    pInst->ct.rxlen++;
                    if (pInst->ct.rxlen == pInst->ct.rxlen_target) {
                        //UtilClass_PrintSpace(70);
                        //ARRAYDUMPX2(pInst->ct.rxbuf, pInst->ct.rxlen);
                        // new round
                        sprintf(Alayout.Buf(layoutIdx), "rx-data done : ");
                        {
                            char *p = Alayout.Buf(layoutIdx);
                            u32 i;
                            p += strlen(p);
                            for (i=0; i<pInst->ct.rxlen; i++) {
                                    sprintf(p, "%02X ", pInst->ct.rxbuf[i]);
                                    p+=3;
                            }
                        }
                        pInst->ct.RxState = 0;
                        pInst->ct.rxlen = 0;
                    } else {
                        sprintf(Alayout.Buf(layoutIdx), "rx-data-ing");
                    }
                } else {
                    pInst->ct.RxState = 0;
                    sprintf(Alayout.Buf(layoutIdx), "rx-data NG");
                }
                break;
        }

        if (timeDb->currTime >= 20)
        {
        }
        else
        {
            pDesc->remain_time = 1;
        }
    }
    return 0;
}

u32 A_EntryCB(SimTime3_Desc *pDesc)
{
    class SimTime3 *timeDb = (class SimTime3 *)pDesc->timeDb;
    sprintf(Alayout.ct.headStr, "%04u : ", (u32)timeDb->currTime);
    return 0;
}

u32 A_LeaveCB(SimTime3_Desc *pDesc)
{//PRLOC
    Alayout.SmartPrint();
    return 0;
}

SimTime3_Desc Aentry, Aleave;

/*
    timeDb
        desc ar
            - inst a
                - ch a
            - inst a (t/r control)
            - layout-index 0
        desc at
            - inst a
                - ch a
            - inst a (t/r control)
            - layout-index 0
        desc br
            - inst b
                - ch a
            - inst b (t/r control)
            - layout-index 1
        desc bt
            - inst b
                - ch a
            - inst b (t/r control)
            - layout-index 1
        desc cr
            - inst c
                - ch a
            - inst c (t/r control)
            - layout-index 2
        desc ct
            - inst c
                - ch a
            - inst c (t/r control)
            - layout-index 2

    SimDb : useless

    Simplify : structure copy
*/
void A_CSMA_Demo(void)
{
    Alayout.ct.colNum = 3;
    Alayout.ct.colWidth = 25;
    Alayout.Init();
    Alayout.vec[0]->width = 20;
    A_TimeDb.ClearDesc(&Aentry);
    A_TimeDb.ClearDesc(&Aleave);
    Aentry.cb = (SimeTime3_CB_t)A_EntryCB;
    Aleave.cb = (SimeTime3_CB_t)A_LeaveCB;
    A_TimeDb.SetEntry(&Aentry);
    A_TimeDb.SetLeave(&Aleave);
//    Aar.hdlC = &Alayout;
//    Abr.hdlC = &Alayout;
//    Acr.hdlC = &Alayout;
//    Aat.hdlC = &Alayout;
//    Abt.hdlC = &Alayout;
//    Act.hdlC = &Alayout;
    Aar.hdlD = (void *)0;
    Abr.hdlD = (void *)1;
    Acr.hdlD = (void *)2;
    Aat.hdlD = (void *)0;
    Abt.hdlD = (void *)1;
    Act.hdlD = (void *)2;
    //strcpy(Alayout.ct.headStr, "head]]] ");

    //no use for now
    A_SimDb.timeDb = (class SimTime3*)&A_TimeDb;

    A_Ch.ct.dataRetainCycles = 1;
    Ainsta.ct.pCh = &A_Ch;
    Ainstb.ct.pCh = &A_Ch;
    Ainstc.ct.pCh = &A_Ch;

    A_TimeDb.ClearDesc(&Aar);
    //Aar.remain_time = 1;
    Aar.cb = (SimeTime3_CB_t)A_SimTime3_DemoCBxx;
    Aar.hdlA = &Ainsta;
    Aar.hdlB = &Ainsta.ct.RxCtrl;
    Ainsta.ct.RxCtrl = A_DISABLE;
    
    A_TimeDb.ClearDesc(&Aat);
    //Aat.remain_time = 1;
    Aat.cb = (SimeTime3_CB_t)A_SimTime3_DemoCBxx;
    Aat.hdlA = &Ainsta;
    Ainsta.ct.txlen = 3;
    Ainsta.ct.txbuf[0] = 2;
    Ainsta.ct.txbuf[1] = 0x11;
    Ainsta.ct.txbuf[2] = 0x22;
    Aat.hdlB = &Ainsta.ct.TxCtrl;
    Ainsta.ct.TxCtrl = A_TX_EN;
    
    A_TimeDb.ClearDesc(&Abr);
    //Abr.remain_time = 1;
    Abr.cb = (SimeTime3_CB_t)A_SimTime3_DemoCBxx;
    Abr.hdlA = &Ainstb;
    Abr.hdlB = &Ainstb.ct.RxCtrl;
    Ainstb.ct.RxCtrl = A_DISABLE;
    
    A_TimeDb.ClearDesc(&Abt);
    //Abt.remain_time = 1;
    Abt.cb = (SimeTime3_CB_t)A_SimTime3_DemoCBxx;
    Abt.hdlA = &Ainstb;
    Ainstb.ct.txlen = 4;
    Ainstb.ct.txbuf[0] = 3;
    Ainstb.ct.txbuf[1] = 0xAA;
    Ainstb.ct.txbuf[2] = 0xBB;
    Ainstb.ct.txbuf[3] = 0xCC;
    Abt.hdlB = &Ainstb.ct.TxCtrl;
    Ainstb.ct.TxCtrl = A_TX_EN;
    
    A_TimeDb.ClearDesc(&Acr);
    //Acr.remain_time = 1;
    Acr.cb = (SimeTime3_CB_t)A_SimTime3_DemoCBxx;
    Acr.hdlA = &Ainstc;
    Acr.hdlB = &Ainstc.ct.RxCtrl;
    Ainstc.ct.RxCtrl = A_RX_EN;

    A_TimeDb.ClearDesc(&Act);
    //Act.remain_time = 1;
    Act.cb = (SimeTime3_CB_t)A_SimTime3_DemoCBxx;
    Act.hdlA = &Ainstc;
    Act.hdlB = &Ainstc.ct.TxCtrl;
    Ainstc.ct.TxCtrl = A_DISABLE;

    A_TimeDb.InsertHead(&Aat);
    A_TimeDb.InsertHead(&Abt);
    A_TimeDb.InsertHead(&Act);
    A_TimeDb.InsertTail(&Aar);
    A_TimeDb.InsertTail(&Abr);
    A_TimeDb.InsertTail(&Acr);

    //timeDb.DumpAll();
    A_TimeDb.Run();
    Alayout.Deinit();
}
