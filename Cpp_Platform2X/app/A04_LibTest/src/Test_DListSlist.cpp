#include "Everything_App.hpp"

typedef struct {
    DLList_Entry_t entry;
    u32 val;
} DLListTest_t;

typedef struct {
    SLList_Entry_t entry;
    u32 val;
} SLListTest_t;

void DListTest(void)
{
    u32 i;
    DLList_Head_t headinst = DLLIST_HEAD_INIT(&headinst);
    DLList_Head_t *head = &headinst;
    //DLListTest_t *prev;
    DLListTest_t *curr;
    //DLListTest_t *next;
    DLListTest_t node[32];

    PRLOC;

    printf("head test ...\n");
    BASIC_ASSERT(head->head == NULL);
    BASIC_ASSERT(head->tail_or_self == &headinst);
    head->head = (void *)0xFFFFFFFF;
    head->tail_or_self = (void *)0xFFFFFFFF;
    DLLIST_HEAD_RESET(head);
    BASIC_ASSERT(head->head == NULL);
    BASIC_ASSERT(head->tail_or_self == &headinst);

    printf("insert first test ...\n");
    for (i=0; i<32; i++) {
        node[i].val = 0x00000001 << i;
        DLLIST_INSERT_FIRST(head, &node[i]);
    }
    i = 0;
    DLLIST_FOREACH(head,curr,DLListTest_t) {
        BASIC_ASSERT(curr->val == (u32)0x80000000 >> i);
        i++;
    }
    BASIC_ASSERT(i == 32);
    printf("insert first test ... while test ...\n");
    i = 0;
    DLLIST_WHILE_START(head,curr,DLListTest_t) {
        BASIC_ASSERT(curr->val == (u32)0x80000000 >> i);
        i++;
        DLLIST_WHILE_NEXT(curr,DLListTest_t);
    }
    BASIC_ASSERT(i == 32);


    printf("insert last test ...\n");
    DLLIST_HEAD_RESET(head);
    for (i=0; i<32; i++) {
        node[i].val = 0x00000001 << i;
        DLLIST_INSERT_LAST(head, &node[i]);
    }
    i = 0;
    DLLIST_FOREACH(head,curr,DLListTest_t) {
        BASIC_ASSERT(curr->val == (u32)0x00000001 << i);
        i++;
    }
    BASIC_ASSERT(i == 32);

    printf("insert after test ...\n");
    // after empty head
    // after un-empty head
    // after the node is the last
    // after the node is not the last
    DLLIST_HEAD_RESET(head);
    DLLIST_INSERT_AFTER(head,head,&node[1]);        //head->[1]
    DLLIST_INSERT_AFTER(head,head,&node[0]);        //head->[0]->[1]
    DLLIST_INSERT_AFTER(head,&node[1],&node[3]);    //head->[0]->[1]->[3]
    DLLIST_INSERT_AFTER(head,&node[1],&node[2]);    //head->[0]->[1]->[2]->[3]
    i = 0;
    DLLIST_FOREACH(head,curr,DLListTest_t) {
        BASIC_ASSERT(curr->val == (u32)0x00000001 << i);
        i++;
    }
    BASIC_ASSERT(i == 4);

    printf("remove first test ...\n");
    // remove list with 1 node
    // remove list with more mode
    //printf("DLLIST_IS_NOT_EMPTY(head) = %d\n", DLLIST_IS_NOT_EMPTY(head));
    DLLIST_HEAD_RESET(head);
    //printf("DLLIST_IS_NOT_EMPTY(head) = %d\n", DLLIST_IS_NOT_EMPTY(head));
    DLLIST_INSERT_LAST(head, &node[0]);
    i = 0;
    DLLIST_FOREACH(head,curr,DLListTest_t) {
        i++;
    }
    BASIC_ASSERT(i == 1);
    BASIC_ASSERT(DLLIST_IS_NOT_EMPTY(head) == 1);
    DLLIST_REMOVE_FIRST_SAFELY(head);
    i = 0;
    DLLIST_FOREACH(head,curr,DLListTest_t) {
        i++;
    }
    BASIC_ASSERT(i == 0);
    BASIC_ASSERT(DLLIST_IS_NOT_EMPTY(head) == 0);
    // next
    DLLIST_INSERT_LAST(head, &node[0]);
    DLLIST_INSERT_LAST(head, &node[1]);
    DLLIST_INSERT_FIRST(head, &node[2]);
    DLLIST_REMOVE_FIRST_SAFELY(head);
    i = 0;
    DLLIST_FOREACH(head,curr,DLListTest_t) {
        BASIC_ASSERT(curr->val == (u32)0x00000001 << i);
        i++;
    }
    BASIC_ASSERT(i == 2);

    printf("remove last test ...\n");
    DLLIST_HEAD_RESET(head);
    // remove list with 1 node
    // remove list with more mode
    DLLIST_INSERT_LAST(head, &node[0]);
    i = 0;
    DLLIST_FOREACH(head,curr,DLListTest_t) {
        i++;
    }
    BASIC_ASSERT(i == 1);
    BASIC_ASSERT(DLLIST_IS_NOT_EMPTY(head) == 1);
    DLLIST_REMOVE_LAST_SAFELY(head);
    i = 0;
    DLLIST_FOREACH(head,curr,DLListTest_t) {
        i++;
    }
    BASIC_ASSERT(i == 0);
    BASIC_ASSERT(DLLIST_IS_NOT_EMPTY(head) == 0);
    // next
    DLLIST_INSERT_LAST(head, &node[0]);
    DLLIST_INSERT_LAST(head, &node[1]);
    DLLIST_INSERT_LAST(head, &node[2]);
    DLLIST_REMOVE_LAST_SAFELY(head);
    i = 0;
    DLLIST_FOREACH(head,curr,DLListTest_t) {
        BASIC_ASSERT(curr->val == (u32)0x00000001 << i);
        i++;
    }
    BASIC_ASSERT(i == 2);

    printf("remove node test ...\n");
    // remove the list with single node
    DLLIST_HEAD_RESET(head);
    DLLIST_INSERT_LAST(head, &node[0]);
    DLLIST_REMOVE_NODE_SAFELY(head, &node[0]);
    i = 0;
    DLLIST_FOREACH(head,curr,DLListTest_t) {
        i++;
    }
    BASIC_ASSERT(i == 0);
    BASIC_ASSERT(DLLIST_IS_EMPTY(head) == 1);
    // remove 1st node with next node
    DLLIST_HEAD_RESET(head);
    DLLIST_INSERT_LAST(head, &node[0]);
    DLLIST_INSERT_LAST(head, &node[1]);
    DLLIST_REMOVE_NODE_SAFELY(head, &node[0]);
    i = 0;
    DLLIST_FOREACH(head,curr,DLListTest_t) {
        BASIC_ASSERT(curr->val == (u32)0x00000001 << 1);
        i++;
    }
    BASIC_ASSERT(i == 1);
    BASIC_ASSERT(DLLIST_IS_EMPTY(head) == 0);
    // remove last node 
    DLLIST_HEAD_RESET(head);
    DLLIST_INSERT_LAST(head, &node[0]);
    DLLIST_INSERT_LAST(head, &node[1]);
    DLLIST_REMOVE_NODE_SAFELY(head, &node[1]);
    i = 0;
    DLLIST_FOREACH(head,curr,DLListTest_t) {
        BASIC_ASSERT(curr->val == (u32)0x00000001 << 0);
        i++;
    }
    BASIC_ASSERT(i == 1);
    BASIC_ASSERT(DLLIST_IS_EMPTY(head) == 0);
    // remove node not first or last
    DLLIST_HEAD_RESET(head);
    DLLIST_INSERT_LAST(head, &node[0]);
    DLLIST_INSERT_LAST(head, &node[2]);
    DLLIST_INSERT_LAST(head, &node[1]);
    DLLIST_REMOVE_NODE_SAFELY(head, &node[2]);
    i = 0;
    DLLIST_FOREACH(head,curr,DLListTest_t) {
        BASIC_ASSERT(curr->val == (u32)0x00000001 << i);
        i++;
    }
    BASIC_ASSERT(i == 2);
    BASIC_ASSERT(DLLIST_IS_EMPTY(head) == 0);
}

void SListTest(void)
{
    u32 i;
    SLList_Head_t headinst = SLLIST_HEAD_INIT(&headinst);
    SLList_Head_t *head = &headinst;
    SLListTest_t *curr;
    SLListTest_t node[32];

    PRLOC;

    printf("head test ...\n");
    BASIC_ASSERT(head->head == NULL);
    BASIC_ASSERT(head->tail_or_self == &headinst);
    head->head = (void *)0xFFFFFFFF;
    head->tail_or_self = (void *)0xFFFFFFFF;
    SLLIST_HEAD_RESET(head);
    BASIC_ASSERT(head->head == NULL);
    BASIC_ASSERT(head->tail_or_self == &headinst);

    printf("insert first test ...\n");
    for (i=0; i<32; i++) {
        node[i].val = 0x00000001 << i;
        SLLIST_INSERT_FIRST(head, &node[i]);
    }
    i = 0;
    SLLIST_FOREACH(head,curr,SLListTest_t) {
        BASIC_ASSERT(curr->val == (u32)0x80000000 >> i);
        i++;
    }
    BASIC_ASSERT(i == 32);
    printf("insert first test ... while test ...\n");
    i = 0;
    SLLIST_WHILE_START(head,curr,SLListTest_t) {
        BASIC_ASSERT(curr->val == (u32)0x80000000 >> i);
        i++;
        SLLIST_WHILE_NEXT(curr,SLListTest_t);
    }
    BASIC_ASSERT(i == 32);


    printf("insert last test ...\n");
    SLLIST_HEAD_RESET(head);
    for (i=0; i<32; i++) {
        node[i].val = 0x00000001 << i;
        SLLIST_INSERT_LAST(head, &node[i]);
    }
    i = 0;
    SLLIST_FOREACH(head,curr,SLListTest_t) {
        BASIC_ASSERT(curr->val == (u32)0x00000001 << i);
        i++;
    }
    BASIC_ASSERT(i == 32);

    printf("insert after test ...\n");
    // after empty head
    // after un-empty head
    // after the node is the last
    // after the node is not the last
    SLLIST_HEAD_RESET(head);
    SLLIST_INSERT_AFTER(head,head,&node[1]);        //head->[1]
    SLLIST_INSERT_AFTER(head,head,&node[0]);        //head->[0]->[1]
    SLLIST_INSERT_AFTER(head,&node[1],&node[3]);    //head->[0]->[1]->[3]
    SLLIST_INSERT_AFTER(head,&node[1],&node[2]);    //head->[0]->[1]->[2]->[3]
    i = 0;
    SLLIST_FOREACH(head,curr,SLListTest_t) {
        BASIC_ASSERT(curr->val == (u32)0x00000001 << i);
        i++;
    }
    BASIC_ASSERT(i == 4);

    printf("remove first test ...\n");
    // remove list with 1 node
    // remove list with more mode
    //printf("SLLIST_IS_NOT_EMPTY(head) = %d\n", SLLIST_IS_NOT_EMPTY(head));
    SLLIST_HEAD_RESET(head);
    //printf("SLLIST_IS_NOT_EMPTY(head) = %d\n", SLLIST_IS_NOT_EMPTY(head));
    SLLIST_INSERT_LAST(head, &node[0]);
    i = 0;
    SLLIST_FOREACH(head,curr,SLListTest_t) {
        i++;
    }
    BASIC_ASSERT(i == 1);
    BASIC_ASSERT(SLLIST_IS_NOT_EMPTY(head) == 1);
    SLLIST_REMOVE_FIRST_SAFELY(head);
    i = 0;
    SLLIST_FOREACH(head,curr,SLListTest_t) {
        i++;
    }
    BASIC_ASSERT(i == 0);
    BASIC_ASSERT(SLLIST_IS_NOT_EMPTY(head) == 0);
    // next
    SLLIST_INSERT_LAST(head, &node[0]);
    SLLIST_INSERT_LAST(head, &node[1]);
    SLLIST_INSERT_FIRST(head, &node[2]);
    SLLIST_REMOVE_FIRST_SAFELY(head);
    i = 0;
    SLLIST_FOREACH(head,curr,SLListTest_t) {
        BASIC_ASSERT(curr->val == (u32)0x00000001 << i);
        i++;
    }
    BASIC_ASSERT(i == 2);

    printf("remove last test ...\n");
    SLLIST_HEAD_RESET(head);
    // remove list with 1 node
    // remove list with more mode
    SLLIST_INSERT_LAST(head, &node[0]);
    i = 0;
    SLLIST_FOREACH(head,curr,SLListTest_t) {
        i++;
    }
    BASIC_ASSERT(i == 1);
    BASIC_ASSERT(SLLIST_IS_NOT_EMPTY(head) == 1);
    SLLIST_REMOVE_LAST_SAFELY(head);
    i = 0;
    SLLIST_FOREACH(head,curr,SLListTest_t) {
        i++;
    }
    BASIC_ASSERT(i == 0);
    BASIC_ASSERT(SLLIST_IS_NOT_EMPTY(head) == 0);
    // next
    SLLIST_INSERT_LAST(head, &node[0]);
    SLLIST_INSERT_LAST(head, &node[1]);
    SLLIST_INSERT_LAST(head, &node[2]);
    SLLIST_REMOVE_LAST_SAFELY(head);
    i = 0;
    SLLIST_FOREACH(head,curr,SLListTest_t) {
        BASIC_ASSERT(curr->val == (u32)0x00000001 << i);
        i++;
    }
    BASIC_ASSERT(i == 2);

    printf("remove node test ...\n");
    // remove the list with single node
    SLLIST_HEAD_RESET(head);
    SLLIST_INSERT_LAST(head, &node[0]);
    SLLIST_REMOVE_NODE_SAFELY(head, &node[0]);
    i = 0;
    SLLIST_FOREACH(head,curr,SLListTest_t) {
        i++;
    }
    BASIC_ASSERT(i == 0);
    BASIC_ASSERT(SLLIST_IS_EMPTY(head) == 1);
    // remove 1st node with next node
    SLLIST_HEAD_RESET(head);
    SLLIST_INSERT_LAST(head, &node[0]);
    SLLIST_INSERT_LAST(head, &node[1]);
    SLLIST_REMOVE_NODE_SAFELY(head, &node[0]);
    i = 0;
    SLLIST_FOREACH(head,curr,SLListTest_t) {
        BASIC_ASSERT(curr->val == (u32)0x00000001 << 1);
        i++;
    }
    BASIC_ASSERT(i == 1);
    BASIC_ASSERT(SLLIST_IS_EMPTY(head) == 0);
    // remove last node 
    SLLIST_HEAD_RESET(head);
    SLLIST_INSERT_LAST(head, &node[0]);
    SLLIST_INSERT_LAST(head, &node[1]);
    SLLIST_REMOVE_NODE_SAFELY(head, &node[1]);
    i = 0;
    SLLIST_FOREACH(head,curr,SLListTest_t) {
        BASIC_ASSERT(curr->val == (u32)0x00000001 << 0);
        i++;
    }
    BASIC_ASSERT(i == 1);
    BASIC_ASSERT(SLLIST_IS_EMPTY(head) == 0);
    // remove node not first or last
    SLLIST_HEAD_RESET(head);
    SLLIST_INSERT_LAST(head, &node[0]);
    SLLIST_INSERT_LAST(head, &node[2]);
    SLLIST_INSERT_LAST(head, &node[1]);
    SLLIST_REMOVE_NODE_SAFELY(head, &node[2]);
    i = 0;
    SLLIST_FOREACH(head,curr,SLListTest_t) {
        BASIC_ASSERT(curr->val == (u32)0x00000001 << i);
        i++;
    }
    BASIC_ASSERT(i == 2);
    BASIC_ASSERT(SLLIST_IS_EMPTY(head) == 0);
}

void DListTest_FreeAll(void)
{
    u32 i;
    DLList_Head_t headinst = DLLIST_HEAD_INIT(&headinst);
    DLList_Head_t *head = &headinst;
    //DLListTest_t *prev;
    DLListTest_t *curr;
    //DLListTest_t *next;
    //DLListTest_t node[32];

    PRLOC;

    MM_INIT();

    printf("insert first test ...\n");
    for (i=0; i<32; i++) {
        curr = (DLListTest_t *)MM_ALLOC(sizeof(DLListTest_t));
        //printf("curr = %p\n", curr);
        curr->val = 0x00000001 << i;
        DLLIST_INSERT_FIRST(head, curr);
    }
    DLLIST_FREE_ALL(head);

    MM_UNINIT();
}

void SListTest_FreeAll(void)
{
    u32 i;
    SLList_Head_t headinst = SLLIST_HEAD_INIT(&headinst);
    SLList_Head_t *head = &headinst;
    //DLListTest_t *prev;
    SLListTest_t *curr;
    //DLListTest_t *next;
    //DLListTest_t node[32];

    PRLOC;

    MM_INIT();

    printf("insert first test ...\n");
    for (i=0; i<32; i++) {
        curr = (SLListTest_t *)MM_ALLOC(sizeof(SLListTest_t));
        //printf("curr = %p\n", curr);
        curr->val = 0x00000001 << i;
        SLLIST_INSERT_FIRST(head, curr);
    }
    SLLIST_FREE_ALL(head);

    MM_UNINIT();
}

void DListSListTest(void)
{
    PRLOC;
    DListTest();
    SListTest();
    DListTest_FreeAll();
    SListTest_FreeAll();
    printf("%s() test done ....\n", __func__);
}