
#include "Everything_App.hpp"
/*
Test category:
    1. init, uninit
    2. single function
*/
void SeqnoBasicFactory_Test()
{
    pv("Test Start!\n");
    MM_INIT();
    {
        u32 num;
        class SeqnoBasicFactory fctr;
        BASIC_ASSERT(fctr.nextSeqno == 0);
        BASIC_ASSERT(fctr.seqnoIncrement == 1);

        //NewSeqno() test
        num = fctr.NewSeqno();
        BASIC_ASSERT(num == 0);
        num = fctr.NewSeqno();
        BASIC_ASSERT(num == 1);
        BASIC_ASSERT(fctr.nextSeqno == 2);
        BASIC_ASSERT(fctr.seqnoIncrement == 1);

        fctr.nextSeqno = 1;
        fctr.seqnoIncrement = 2;
        num = fctr.NewSeqno();
        BASIC_ASSERT(num == 1);
        num = fctr.NewSeqno();
        BASIC_ASSERT(num == 3);

        {
            struct SeqnoNode *node;
            fctr.Reset(1, 2);
            // create 1, 3, 5, 7, 9
            node = fctr.NewNodesOnTheLast(3);
            BASIC_ASSERT(node->seqno == 1);
            BASIC_ASSERT(!DLLIST_IS_EMPTY(fctr.pHead));
            node = fctr.NewNodesOnTheLast(2);
            BASIC_ASSERT(node->seqno == 7);

            node = fctr.FindSeqno(3, 0);
            BASIC_ASSERT(node != NULL);
            BASIC_ASSERT(node->seqno == 3);

            // remove 3, result is 1 5 7 9
            node = fctr.FindSeqno(3, 1);
            BASIC_ASSERT(node != NULL);
            BASIC_ASSERT(node->seqno == 3);
            MM_FREE(node);
            node = fctr.FindSeqno(3, 1);
            BASIC_ASSERT(node == NULL);

            {
                u32 i = 0, ans[] = {1,5,7,9};

                for(; i<LENGTH_OF_ARRAY(ans); i++)
                {
                    node = fctr.GetFirst(0);
                    BASIC_ASSERT(node != NULL);
                    BASIC_ASSERT(node->seqno == ans[i]);
                    node = fctr.GetFirst(1);
                    BASIC_ASSERT(node != NULL);
                    BASIC_ASSERT(node->seqno == ans[i]);
                    MM_FREE(node);
                }
            }
            BASIC_ASSERT(DLLIST_IS_EMPTY(fctr.pHead));

            node = fctr.NewNode(999);
            BASIC_ASSERT(node != NULL);
            BASIC_ASSERT(node->seqno == 999);
            MM_FREE(node);

            {
                // 1. create 2 4 6
                // 2. add 1 3 999
                u32 i = 0, ans[] = {1,2,3,4,6,999};
                fctr.ReleaseList();
                node = fctr.NewNode(4);
                fctr.InsertNodeWithSeqnoSearch(node);
                node = fctr.NewNode(2);
                fctr.InsertNodeWithSeqnoSearch(node);
                node = fctr.NewNode(6);
                fctr.InsertNodeWithSeqnoSearch(node);
                node = fctr.NewNode(1);
                fctr.InsertNodeWithSeqnoSearch(node);
                node = fctr.NewNode(3);
                fctr.InsertNodeWithSeqnoSearch(node);
                node = fctr.NewNode(999);
                fctr.InsertNodeWithSeqnoSearch(node);
                //verify
                for(; i<LENGTH_OF_ARRAY(ans); i++)
                {
                    node = fctr.GetFirst(0);
                    BASIC_ASSERT(node != NULL);
                    BASIC_ASSERT(node->seqno == ans[i]);
                    //DND(node->seqno);
                    node = fctr.GetFirst(1);
                    BASIC_ASSERT(node != NULL);
                    BASIC_ASSERT(node->seqno == ans[i]);
                    MM_FREE(node);
                }
            }

            //In bottom
            fctr.ReleaseList();
            BASIC_ASSERT(DLLIST_IS_EMPTY(fctr.pHead));
        }
    }
    MM_UNINIT();
    pv("Test Done!\n");
}

void SeqnoRxCentral_Test()
{
    struct SeqnoNode *node;

    pv("Test Start!\n");

    // init test
    MM_INIT();
    {
        class SeqnoRxCentral rxc;

        BASIC_ASSERT(DLLIST_IS_EMPTY(rxc.pHead));

        // uninit test
        node = rxc.NewNode(1);
        rxc.InsertNodeWithSeqnoSearch(node);
    }
    MM_UNINIT();

    MM_INIT();
    {
        class SeqnoRxCentral rxc;
        rxc.RxReset(0, 0);
        BASIC_ASSERT(!rxc.RxIsDone());
        rxc.RxPushNewSeqno(0);
        BASIC_ASSERT(rxc.RxIsDone());

        rxc.RxReset(0, 1);
        BASIC_ASSERT(!rxc.RxIsDone());
        rxc.RxPushNewSeqno(0);
        rxc.RxPushNewSeqno(1);
        BASIC_ASSERT(rxc.RxIsDone());

        rxc.RxReset(0, 1);
        BASIC_ASSERT(!rxc.RxIsDone());
        rxc.RxPushNewSeqno(1);
        rxc.RxPushNewSeqno(0);
        BASIC_ASSERT(rxc.RxIsDone());

        rxc.RxReset(0, 2);
        BASIC_ASSERT(!rxc.RxIsDone());
        rxc.RxPushNewSeqno(1);
        rxc.RxPushNewSeqno(2);
        rxc.RxPushNewSeqno(0);
        BASIC_ASSERT(rxc.RxIsDone());

        // node
        rxc.RxReset(0, 2);
        BASIC_ASSERT(!rxc.RxIsDone());
        rxc.RxPushNewNode(rxc.NewNode(1));
        rxc.RxPushNewNode(rxc.NewNode(2));
        rxc.RxPushNewNode(rxc.NewNode(0));
        BASIC_ASSERT(rxc.RxIsDone());
        // hybrid
        rxc.RxReset(0, 2);
        BASIC_ASSERT(!rxc.RxIsDone());
        rxc.RxPushNewSeqno(1);
        rxc.RxPushNewNode(rxc.NewNode(2));
        rxc.RxPushNewNode(rxc.NewNode(0));
        BASIC_ASSERT(rxc.RxIsDone());
        rxc.RxReset(0, 2);
        BASIC_ASSERT(!rxc.RxIsDone());
        rxc.RxPushNewNode(rxc.NewNode(1));
        rxc.RxPushNewSeqno(2);
        rxc.RxPushNewNode(rxc.NewNode(0));
        BASIC_ASSERT(rxc.RxIsDone());
        rxc.RxReset(0, 2);
        BASIC_ASSERT(!rxc.RxIsDone());
        rxc.RxPushNewNode(rxc.NewNode(1));
        rxc.RxPushNewNode(rxc.NewNode(2));
        rxc.RxPushNewSeqno(0);
        BASIC_ASSERT(rxc.RxIsDone());

        rxc.RxReset(0, 4);
        BASIC_ASSERT(!rxc.RxIsDone());
        rxc.RxPushNewSeqno(4);
        rxc.RxPushNewNode(rxc.NewNode(1));
        //rxc.Dump();
        rxc.RxPushNewSeqno(3);
        rxc.RxPushNewNode(rxc.NewNode(2));
        //rxc.Dump();
        rxc.RxPushNewSeqno(0);
        BASIC_ASSERT(rxc.RxIsDone());
    }
    MM_UNINIT();
    pv("Test Done!\n");
}

void NetExp_Test()
{
    pv("Test Start!\n");
    MM_INIT();
    {
        class NetExp exp;
        exp.Reset(5); // 0,1,2,3,4, sum = 10，avg = 2
        exp.RunAll();
        BASIC_ASSERT((double)exp.CalcAverage()*5 == 10.0);
        exp.resultAry[0] = 89.0;
        //DNF(exp.CalcSum()/10);
        //DNF(exp.CalcAverage());
        BASIC_ASSERT(exp.CalcSum() == 99.0);
        BASIC_ASSERT((double)exp.CalcAverage()*5 == 99.0);
    }
    MM_UNINIT();

    MM_INIT();
    {
        class NetExpDemo : public NetExp {
            public:
            virtual double ReturnResult(){return CalcSum();};
        };
        class NetExp outter;
        class NetExpDemo inner; // change to "return sum"" from "return avg"
        outter.Reset(10);
        inner.Reset(5); // 0,1,2,3,4, sum = 10，avg = 2
        outter.innerExp = &inner;
        outter.RunAll();
        //DNF(outter.CalcSum());
        //DNF(outter.CalcAverage());
        BASIC_ASSERT(outter.CalcSum() == 100.0);
        BASIC_ASSERT((double)outter.CalcAverage()*10 == 100.0);
    }
    MM_UNINIT();

    MM_INIT();
    {
        class NetExp_SimpleRate sr;
        sr.Reset(3);
        sr.RateReset(10, 2);
        BASIC_ASSERT(sr.error_rate_start == 10);
        BASIC_ASSERT(sr.error_rate_increment == 2);
        BASIC_ASSERT(sr.error_rate_curr == 10);
        sr.RunAll();
        BASIC_ASSERT(sr.error_rate_start == 10);
        BASIC_ASSERT(sr.error_rate_increment == 2);
        BASIC_ASSERT(sr.error_rate_curr == 16);
        sr.RunAll();
        BASIC_ASSERT(sr.error_rate_start == 10);
        BASIC_ASSERT(sr.error_rate_increment == 2);
        BASIC_ASSERT(sr.error_rate_curr == 16);
        sr.RateReset(10, 1);
        sr.RunAll();
        BASIC_ASSERT(sr.error_rate_start == 10);
        BASIC_ASSERT(sr.error_rate_increment == 1);
        BASIC_ASSERT(sr.error_rate_curr == 13);
        sr.RateReset(10);
        sr.RunAll();
        BASIC_ASSERT(sr.error_rate_start == 10);
        BASIC_ASSERT(sr.error_rate_increment == 0);
        BASIC_ASSERT(sr.error_rate_curr == 10);
    }
    MM_UNINIT();

    MM_INIT();
    {
        class NetExp_SinglePath single;
        LibUtil_InitRand();
        single.Init(4, 5, 0);
        single.Run();
        BASIC_ASSERT(single.CalcSum() == 20.0);
        BASIC_ASSERT((double)single.CalcAverage()*5 == 20.0);

        single.Init(10, 3, 0);
        single.Run();
        //DNF(single.CalcSum());
        BASIC_ASSERT(single.CalcSum() == 3*10.0);
        BASIC_ASSERT((double)single.CalcAverage()*3 == 30.0);

        single.Init(10, 1, 0);
        single.Run();
        BASIC_ASSERT(single.CalcAverage() >= 9.99 && single.CalcAverage() <= 10.01);
        single.Init(10, 1000, 50);
        single.Run();
        //DNF(single.CalcAverage());
        BASIC_ASSERT(single.CalcAverage() >= 18 && single.CalcAverage() <= 22);
    }
    MM_UNINIT();

    // error rate test
    MM_INIT();
    {
        u32 i, accu;
        class NetExp_DualPath dual;
        LibUtil_InitRand();
        dual.Init(4, 5, 0);
        for (accu = i = 0; i < 50000; i++) {
            if (dual.IsSuccess())
                accu++;
        }
        //DND(accu);
        BASIC_ASSERT(accu == 50000);
        for (accu = i = 0; i < 50000; i++) {
            if (dual.IsError())
                accu++;
        }
        //DND(accu);
        BASIC_ASSERT(accu == 0);

        dual.Init(4, 5, 50);
        for (accu = i = 0; i < 50000; i++) {
            if (dual.IsSuccess())
                accu++;
        }
        //DND(accu);
        BASIC_ASSERT(accu > 24700 && accu < 25300);
        for (accu = i = 0; i < 50000; i++) {
            if (dual.IsError())
                accu++;
        }
        //DND(accu);
        BASIC_ASSERT(accu > 24700 && accu < 25300);

        dual.Init(4, 5, 90);
        for (accu = i = 0; i < 1000000; i++) {
            if (dual.IsSuccess())
                accu++;
        }
        //DND(accu);
        BASIC_ASSERT(accu > 100000-2000 && accu < 100000+2000);

        dual.Init(4, 5, 99);
        for (accu = i = 0; i < 1000000; i++) {
            if (dual.IsSuccess())
                accu++;
        }
        //DND(accu);
        BASIC_ASSERT(accu > 10000-500 && accu < 10000+500);
    }

    MM_INIT();
    {
        class NetExp_DualPath dual;
        LibUtil_InitRand();
        dual.Init(4, 5, 0);
        dual.Run();
        BASIC_ASSERT(dual.CalcSum() == 10.0);
        BASIC_ASSERT((double)dual.CalcAverage()*5 == 10.0);

        dual.Init(12, 1, 0);
        dual.Run();
        //DNF(dual.CalcAverage());
        BASIC_ASSERT((u32)dual.CalcAverage() == 6);

        dual.Init(12, 10000, 50);
        dual.Run();
        //DNF(dual.CalcAverage());
    }
    MM_UNINIT();

    MM_INIT();
    {
        class NetExp_DualPath_Dup dup;
        LibUtil_InitRand();
        dup.Init(6, 2, 0);
        dup.Run();
        BASIC_ASSERT((u32)dup.CalcSum() == 12);
        BASIC_ASSERT((u32)dup.CalcAverage() == 6);

        dup.Init(12, 1000, 50);
        dup.Run();
        DNF(dup.CalcAverage());
    }
    MM_UNINIT();

    MM_INIT();
    {
        class NetExp_DualPath_DupEx dup;
        LibUtil_InitRand();
        dup.Init(12, 1000, 50);
        dup.Run();
        DNF(dup.CalcAverage());
    }
    MM_UNINIT();

    MM_INIT();
    {
        class NetExp outter;
        class NetExp_DualPath_DupEx dup;

        outter.Reset(1000);
        outter.innerExp = &dup;
        dup.Init(12, 1, 50);
        LibUtil_InitRand();
        outter.RunAll();
        DNF(outter.resultAry[0]);
        DNF(outter.resultAry[1]);
        DNF(outter.resultAry[2]);
        DNF(outter.CalcAverage());
    }
    MM_UNINIT();

    pv("Test Done!\n");
}

void NetUtilTest_00()
{
    SeqnoBasicFactory_Test();

    SeqnoRxCentral_Test();

    NetExp_Test();
}
