
#include "Everything_App.hpp"

struct seqnode {
    DLList_Entry_t entry;
    u32 seqno;
    u32 done;
};

class ArbiterSeqno {
public:
    u32 maxSeqno;
    u32 txSeqno;
    u32 txSeqnoDone;
    u32 expectRxSeqno;
    DLList_Head_t txHead;
    DLList_Head_t rxHead;

    void Reset() {
        txSeqno=0;
        txSeqnoDone=0;
        expectRxSeqno=0;
        DLLIST_FREE_ALL(&txHead);
        DLLIST_FREE_ALL(&rxHead);
        DLLIST_HEAD_RESET(&txHead);
        DLLIST_HEAD_RESET(&rxHead);
    };
    ArbiterSeqno(u32 inMaxSeqno = 0) {
        maxSeqno=inMaxSeqno;
        txSeqno=0;
        txSeqnoDone=0;
        expectRxSeqno=0;
        DLLIST_HEAD_RESET(&txHead);
        DLLIST_HEAD_RESET(&rxHead);
    };
    ~ArbiterSeqno(){
        DLLIST_FREE_ALL(&txHead);
        DLLIST_FREE_ALL(&rxHead);
    };
    int TxDone(u32 seqno) {
        struct seqnode *curr;
        struct seqnode *matched;
        DLLIST_FOREACH (&txHead, curr, struct seqnode) {
            if (seqno == curr->seqno) {
                curr->done = 1;
                break;
            }
        }
        //recycle
        DLLIST_WHILE_START (&txHead, curr, struct seqnode) {
            if (curr->done) {
                if (curr->seqno == txSeqnoDone) {
                    matched = curr;
                    DLLIST_WHILE_NEXT(curr, struct seqnode);
                    DLLIST_REMOVE_NODE(&txHead, matched);
                    txSeqnoDone++;
                    MM_FREE(matched);
                    continue;
                }
            } else {
                break;
            }
        }
        return 0;
    };
    u32 GetNewTxSeq(void) {
        u32 seqno = txSeqno;
        if (!IsTxEmpty()) {
            struct seqnode *node;
            node = (struct seqnode *)MM_ALLOC(sizeof(struct seqnode));
            node->seqno = seqno;
            node->done = 0;
            DLLIST_INSERT_LAST(&txHead, node);
            txSeqno++;
        }
        return seqno;
    };
    /*
        Put seqno, if not sequential = buffered, return expectRxSeqno;
    */
    u32 PutRxSeq(u32 seqno) {
        struct seqnode *curr;
        //struct seqnode *matched;
        int found = 0;
        int recycle = 0;

        if (IsRxDone())
            return expectRxSeqno;

        if (seqno == expectRxSeqno) {
            expectRxSeqno++;
            found = 1;
        }

        //recursive recycle
        do {
            recycle = 0;
            //recycle
            DLLIST_FOREACH (&rxHead, curr, struct seqnode) {
                if (curr->seqno == seqno) {
                    found = 1;
                }
                if (curr->seqno == expectRxSeqno) {
                    DLLIST_REMOVE_NODE(&rxHead, curr);
                    expectRxSeqno++;
                    MM_FREE(curr);
                    recycle = 1;
                    break;
                }
            }
        } while (recycle);

        if (!found) {
            //or insert to list
            struct seqnode *node;
            node = (struct seqnode *)MM_ALLOC(sizeof(struct seqnode));
            node->seqno = seqno;
            node->done = 1;
            DLLIST_INSERT_LAST(&rxHead, node);
        }

        return 0;
    };
    int IsTxEmpty(void) {
        return (maxSeqno == txSeqno);
    };
    int IsTxDone(void) {
        return (maxSeqno == txSeqnoDone);
    };
    int IsRxDone(void) {
        return (maxSeqno == expectRxSeqno);
    };
    void Dump() {
        struct seqnode *curr;
        DND(maxSeqno);
        DND(txSeqno);
        DND(txSeqnoDone);
        DND(expectRxSeqno);
        DND(IsTxEmpty());
        DND(IsTxDone());
        DND(IsRxDone());
        printf("tx q = ");
        DLLIST_FOREACH (&txHead, curr, struct seqnode) {
            printf("%d(%d) ", curr->seqno, curr->done);
        }
        printf("\n");
        printf("rx q = ");
        DLLIST_FOREACH (&rxHead, curr, struct seqnode) {
            printf("%d(%d) ", curr->seqno, curr->done);
        }
        printf("\n");
    };
};

void PathAggregationTestStart(void)
{
    class ArbiterSeqno arb(4);
    
    //arb.Dump();
    arb.GetNewTxSeq();
    arb.GetNewTxSeq();
    arb.TxDone(1);
    arb.GetNewTxSeq();
    arb.GetNewTxSeq();
    arb.GetNewTxSeq();

    arb.PutRxSeq(3);
    arb.PutRxSeq(1);
    arb.PutRxSeq(2);
    arb.PutRxSeq(2);
    //arb.PutRxSeq(0);
    arb.Dump();
}

#define TOTAL_RUN ( 2000 )
#define TOTAL_PKT ( 4 )

u32 tx_work_seqno[2];
u32 tx_success[2];
u32 cmpl_rounds[TOTAL_RUN];


double single_avg;
double dup_avg;
double intv_avg;

void reset_test()
{
    tx_success[0] = 0;
    tx_success[1] = 0;
}

// return true for success
int success(u32 success_rate)
{
    u32 rand = (u32)LibUtil_GetRand() % 100;
    //printf("rand=%d, sr=%d\n", rand, success_rate);
    if (rand < success_rate)
        return 1;
    else
        return 0;
}

// return if success
int tx_from_a_to_b(u32 ch_idx, u32 success_rate, u32 seqno)
{
    tx_success[ch_idx] = 0;
    if (success(success_rate) == 0)
        return 0;

    tx_success[ch_idx] = 1;
    tx_work_seqno[ch_idx] = seqno;
    return 1;
}

// return if rx ok
int rx_on_b(u32 ch_idx, u32 &seqno)
{
    if (tx_success[ch_idx]) {
        seqno = tx_work_seqno[ch_idx];
        return 1;
    }
    return 0;
}

void show_statistics(double &in_avg, double &in_jitter, double &in_min, double &in_max)
{
    double min = 10000000.0;
    double max = 0.0;
    double avg;
    double curr = 0.0;
    double jitter_accu = 0.0;
    int i;

    for (i = 0; i < TOTAL_RUN; i++)
    {
        curr += (double)cmpl_rounds[i];
    }
    avg = curr / TOTAL_RUN;

    for (i = 0; i < TOTAL_RUN; i++)
    {
        curr = (double)cmpl_rounds[i];
        // min & max
        if (curr < min)
            min = curr;
        if (curr > max)
            max = curr;
        // jitter
        if (curr >= avg) {
            curr = curr - avg;
        } else {
            curr = avg - curr;
        }
        jitter_accu = jitter_accu + curr;
    }
    in_avg = avg;
    in_jitter = jitter_accu / TOTAL_RUN;

    printf("avg = %11f, jitter = %11f, min = %11f, max = %11f\n", avg, in_jitter, min, max);
}

int test_single_channel(u32 success_rate)
{
    class ArbiterSeqno arb(TOTAL_PKT);
    u32 curr_cmpl_rounds = 0; // 1 round = 1ms, with 4 + 1000 bytes, no error = 1Mbps
    u32 total_run_i = 0;
    u32 tx_seq[2];
    u32 rx_seq[2];
    u32 error_count[2];
    u32 success_rate_backup = success_rate;

    LibUtil_InitRand();

    for (total_run_i = 0; total_run_i < TOTAL_RUN; total_run_i++) {
        curr_cmpl_rounds = 0;
        arb.Reset();
        reset_test();
        tx_seq[0] = arb.GetNewTxSeq();
        error_count[0] = 0;
        while (1) {
            curr_cmpl_rounds++;
            if (tx_from_a_to_b(0, success_rate, tx_seq[0])) {
            /*
            PRLOC
            DND(total_run_i);
            DND(curr_cmpl_rounds);
            DND(tx_seq[0]);
            DND(error_count);
            */
                arb.TxDone(tx_seq[0]);
                tx_seq[0] = arb.GetNewTxSeq();
                success_rate = success_rate_backup;
                error_count[0] = 0;
            } else {
                error_count[0]++;
            }
            if (rx_on_b(0, rx_seq[0])) {
                arb.PutRxSeq(rx_seq[0]);
            }
            if (arb.IsRxDone()) {
                cmpl_rounds[total_run_i] = curr_cmpl_rounds;
                break;
            }
            if (error_count[0] >= 1000) {
                success_rate = 100;
            }
        }
        //printf("curr_cmpl_rounds = %d\n", curr_cmpl_rounds);
    }

    return 0;
}

int test_tow_channel_dup(u32 success_rate)
{
    class ArbiterSeqno arb(TOTAL_PKT);
    u32 curr_cmpl_rounds = 0; // 1 round = 1ms, with 4 + 1000 bytes, no error = 1Mbps
    u32 total_run_i = 0;
    u32 tx_seq[2];
    u32 rx_seq[2];
    u32 error_count[2];
    u32 success_rate_curr[2];
    int is_any_tx_done;

    LibUtil_InitRand();

    for (total_run_i = 0; total_run_i < TOTAL_RUN; total_run_i++) {
        curr_cmpl_rounds = 0;
        arb.Reset();
        reset_test();
        tx_seq[0] = arb.GetNewTxSeq();
        tx_seq[1] = tx_seq[0];
        error_count[0] = 0;
        error_count[1] = 0;
        success_rate_curr[0] = success_rate;
        success_rate_curr[1] = success_rate;
        while (1) {
            curr_cmpl_rounds++;
            is_any_tx_done = 0;
            // tx
            if (tx_from_a_to_b(0, success_rate_curr[0], tx_seq[0])) {
                arb.TxDone(tx_seq[0]);
                is_any_tx_done = 1;
            } else {
                error_count[0]++;
            }
            if (tx_from_a_to_b(1, success_rate_curr[1], tx_seq[1])) {
                arb.TxDone(tx_seq[1]);
                is_any_tx_done = 1;
            } else {
                error_count[1]++;
            }
            if (is_any_tx_done) {
                tx_seq[0] = arb.GetNewTxSeq();
                tx_seq[1] = tx_seq[0];
                error_count[0] = 0;
                error_count[1] = 0;
            }
            // rx
            if (rx_on_b(0, rx_seq[0])) {
                arb.PutRxSeq(rx_seq[0]);
            }
            if (rx_on_b(1, rx_seq[1])) {
                arb.PutRxSeq(rx_seq[1]);
            }
            if (arb.IsRxDone()) {
                cmpl_rounds[total_run_i] = curr_cmpl_rounds;
                break;
            }
            if (error_count[0] >= 1000) {
                success_rate_curr[0] = 100;
            }
            if (error_count[1] >= 1000) {
                success_rate_curr[1] = 100;
            }
        }
        //printf("curr_cmpl_rounds = %d\n", curr_cmpl_rounds);
    }

    //arb.Dump();

    return 0;
}

int test_tow_channel_dup2(u32 success_rate)
{
    class ArbiterSeqno arb(TOTAL_PKT);
    u32 curr_cmpl_rounds = 0; // 1 round = 1ms, with 4 + 1000 bytes, no error = 1Mbps
    u32 total_run_i = 0;
    u32 tx_seq[2];
    u32 rx_seq[2];
    u32 error_count[2];
    u32 success_rate_curr[2];

    LibUtil_InitRand();

    for (total_run_i = 0; total_run_i < TOTAL_RUN; total_run_i++) {
        curr_cmpl_rounds = 0;
        arb.Reset();
        reset_test();
        // intv2 = pre-interleaved
        for (int i = 0; i < TOTAL_PKT; i++) {
            arb.GetNewTxSeq();
        }
        tx_seq[0] = 0;
        tx_seq[1] = 0;

        error_count[0] = 0;
        error_count[1] = 0;
        success_rate_curr[0] = success_rate;
        success_rate_curr[1] = success_rate;
        while (1) {
            curr_cmpl_rounds++;
            // tx
            if (tx_from_a_to_b(0, success_rate_curr[0], tx_seq[0])) {
                arb.TxDone(tx_seq[0]);
                error_count[0] = 0;
                tx_seq[0]++;
            } else {
                error_count[0]++;
            }
            if (tx_from_a_to_b(1, success_rate_curr[1], tx_seq[1])) {
                arb.TxDone(tx_seq[1]);
                error_count[1] = 0;
                tx_seq[1]++;
            } else {
                error_count[1]++;
            }
            // rx
            if (rx_on_b(0, rx_seq[0])) {
                arb.PutRxSeq(rx_seq[0]);
            }
            if (rx_on_b(1, rx_seq[1])) {
                arb.PutRxSeq(rx_seq[1]);
            }
            if (arb.IsRxDone()) {
                cmpl_rounds[total_run_i] = curr_cmpl_rounds;
                break;
            }
            if (error_count[0] >= 1000) {
                success_rate_curr[0] = 100;
            }
            if (error_count[1] >= 1000) {
                success_rate_curr[1] = 100;
            }
        }
        //printf("curr_cmpl_rounds = %d\n", curr_cmpl_rounds);
    }

    //arb.Dump();

    return 0;
}

int test_tow_channel_intv(u32 success_rate)
{
    class ArbiterSeqno arb(TOTAL_PKT);
    u32 curr_cmpl_rounds = 0; // 1 round = 1ms, with 4 + 1000 bytes, no error = 1Mbps
    u32 total_run_i = 0;
    u32 tx_seq[2];
    u32 rx_seq[2];
    u32 error_count[2];
    u32 success_rate_curr[2];
    int do_flag[2];

    LibUtil_InitRand();

    for (total_run_i = 0; total_run_i < TOTAL_RUN; total_run_i++) {
        curr_cmpl_rounds = 0;
        arb.Reset();
        reset_test();
        tx_seq[0] = arb.GetNewTxSeq();
        tx_seq[1] = arb.GetNewTxSeq();
        error_count[0] = 0;
        error_count[1] = 0;
        success_rate_curr[0] = success_rate;
        success_rate_curr[1] = success_rate;
        do_flag[0] = 1;
        do_flag[1] = 1;
        while (1) {
            curr_cmpl_rounds++;
            // tx
            #define CHANNEL 0
            if (do_flag[CHANNEL] && tx_from_a_to_b(CHANNEL, success_rate_curr[CHANNEL], tx_seq[CHANNEL])) {
                arb.TxDone(tx_seq[CHANNEL]);
                if (arb.IsTxEmpty()) {
                    do_flag[CHANNEL] = 0;
                } else {
                    tx_seq[CHANNEL] = arb.GetNewTxSeq();
                }
                error_count[CHANNEL] = 0;
            } else {
                error_count[CHANNEL]++;
            }
            #undef CHANNEL
            #define CHANNEL 1
            if (do_flag[CHANNEL] && tx_from_a_to_b(CHANNEL, success_rate_curr[CHANNEL], tx_seq[CHANNEL])) {
                arb.TxDone(tx_seq[CHANNEL]);
                if (arb.IsTxEmpty()) {
                    do_flag[CHANNEL] = 0;
                } else {
                    tx_seq[CHANNEL] = arb.GetNewTxSeq();
                }
                error_count[CHANNEL] = 0;
            } else {
                error_count[CHANNEL]++;
            }
            #undef CHANNEL
            // rx
            if (rx_on_b(0, rx_seq[0])) {
                arb.PutRxSeq(rx_seq[0]);
            }
            if (rx_on_b(1, rx_seq[1])) {
                arb.PutRxSeq(rx_seq[1]);
            }
            if (arb.IsRxDone()) {
                cmpl_rounds[total_run_i] = curr_cmpl_rounds;
                break;
            }
            if (error_count[0] >= 1000) {
                success_rate_curr[0] = 100;
            }
            if (error_count[1] >= 1000) {
                success_rate_curr[1] = 100;
            }
        }
        //printf("curr_cmpl_rounds = %d\n", curr_cmpl_rounds);
    }

    //arb.Dump();

    return 0;
}

int test_tow_channel_intv_dup(u32 success_rate)
{
    class ArbiterSeqno arb(TOTAL_PKT);
    u32 curr_cmpl_rounds = 0; // 1 round = 1ms, with 4 + 1000 bytes, no error = 1Mbps
    u32 total_run_i = 0;
    u32 tx_seq[2];
    u32 rx_seq[2];
    u32 error_count[2];
    u32 success_rate_curr[2];
    int do_flag[2];

    LibUtil_InitRand();

    for (total_run_i = 0; total_run_i < TOTAL_RUN; total_run_i++) {
        curr_cmpl_rounds = 0;
        arb.Reset();
        reset_test();
        tx_seq[0] = arb.GetNewTxSeq();
        tx_seq[1] = arb.GetNewTxSeq();
        error_count[0] = 0;
        error_count[1] = 0;
        success_rate_curr[0] = success_rate;
        success_rate_curr[1] = success_rate;
        do_flag[0] = 1;
        do_flag[1] = 1;
        while (1) {
            curr_cmpl_rounds++;
            // tx
            #define CHANNEL 0
            if (do_flag[CHANNEL] && tx_from_a_to_b(CHANNEL, success_rate_curr[CHANNEL], tx_seq[CHANNEL])) {
                arb.TxDone(tx_seq[CHANNEL]);
                if (arb.IsTxEmpty()) {
                    tx_seq[CHANNEL] = tx_seq[1];
                } else {
                    tx_seq[CHANNEL] = arb.GetNewTxSeq();
                }
                error_count[CHANNEL] = 0;
            } else {
                error_count[CHANNEL]++;
            }
            #undef CHANNEL
            #define CHANNEL 1
            if (do_flag[CHANNEL] && tx_from_a_to_b(CHANNEL, success_rate_curr[CHANNEL], tx_seq[CHANNEL])) {
                arb.TxDone(tx_seq[CHANNEL]);
                if (arb.IsTxEmpty()) {
                    tx_seq[CHANNEL] = tx_seq[0];
                } else {
                    tx_seq[CHANNEL] = arb.GetNewTxSeq();
                }
                error_count[CHANNEL] = 0;
            } else {
                error_count[CHANNEL]++;
            }
            #undef CHANNEL
            // rx
            if (rx_on_b(0, rx_seq[0])) {
                arb.PutRxSeq(rx_seq[0]);
            }
            if (rx_on_b(1, rx_seq[1])) {
                arb.PutRxSeq(rx_seq[1]);
            }
            if (arb.IsRxDone()) {
                cmpl_rounds[total_run_i] = curr_cmpl_rounds;
                break;
            }
            if (error_count[0] >= 1000) {
                success_rate_curr[0] = 100;
            }
            if (error_count[1] >= 1000) {
                success_rate_curr[1] = 100;
            }
        }
        //printf("curr_cmpl_rounds = %d\n", curr_cmpl_rounds);
    }

    //arb.Dump();

    return 0;
}

int test_tow_channel_intv2(u32 success_rate)
{
    class ArbiterSeqno arb(TOTAL_PKT);
    u32 curr_cmpl_rounds = 0; // 1 round = 1ms, with 4 + 1000 bytes, no error = 1Mbps
    u32 total_run_i = 0;
    u32 tx_seq[2];
    u32 rx_seq[2];
    u32 error_count[2];
    u32 success_rate_curr[2];
    int do_flag[2];
    int ch_idx;

    LibUtil_InitRand();

    for (total_run_i = 0; total_run_i < TOTAL_RUN; total_run_i++) {
        curr_cmpl_rounds = 0;
        arb.Reset();
        reset_test();

        // intv2 = pre-interleaved
        for (int i = 0; i < TOTAL_PKT; i++) {
            arb.GetNewTxSeq();
        }
        tx_seq[0] = 0;
        tx_seq[1] = 1;

        error_count[0] = 0;
        error_count[1] = 0;
        success_rate_curr[0] = success_rate;
        success_rate_curr[1] = success_rate;
        do_flag[0] = 1;
        do_flag[1] = 1;
        while (1) {
            curr_cmpl_rounds++;
            // tx
            for (ch_idx = 0; ch_idx < 2; ch_idx++) {
                if (!do_flag[ch_idx])
                    continue;
                if (tx_from_a_to_b(ch_idx, success_rate_curr[ch_idx], tx_seq[ch_idx])) {
                    arb.TxDone(tx_seq[ch_idx]);
                    tx_seq[ch_idx] += 2;
                    if (tx_seq[ch_idx] >= TOTAL_PKT) {
                        // not help others
                        do_flag[ch_idx] = 0;
                    }
                    error_count[ch_idx] = 0;
                } else {
                    error_count[ch_idx]++;
                }
            }
            // rx
            for (ch_idx = 0; ch_idx < 2; ch_idx++) {
                if (rx_on_b(ch_idx, rx_seq[ch_idx])) {
                    arb.PutRxSeq(rx_seq[ch_idx]);
                }
            }
            if (arb.IsRxDone()) {
                cmpl_rounds[total_run_i] = curr_cmpl_rounds;
                break;
            }
            if (error_count[0] >= 1000) {
                success_rate_curr[0] = 100;
            }
            if (error_count[1] >= 1000) {
                success_rate_curr[1] = 100;
            }
        }
        //printf("curr_cmpl_rounds = %d\n", curr_cmpl_rounds);
    }

    //arb.Dump();

    return 0;
}

int test_tow_channel_intv_dup2(u32 success_rate)
{
    class ArbiterSeqno arb(TOTAL_PKT);
    u32 curr_cmpl_rounds = 0; // 1 round = 1ms, with 4 + 1000 bytes, no error = 1Mbps
    u32 total_run_i = 0;
    u32 tx_seq[2];
    u32 rx_seq[2];
    u32 error_count[2];
    u32 success_rate_curr[2];
    int do_flag[2];
    int ch_idx;
    int do_help[2];

    LibUtil_InitRand();

    for (total_run_i = 0; total_run_i < TOTAL_RUN; total_run_i++) {
        curr_cmpl_rounds = 0;
        arb.Reset();
        reset_test();

        // intv2 = pre-interleaved
        for (int i = 0; i < TOTAL_PKT; i++) {
            arb.GetNewTxSeq();
        }
        tx_seq[0] = 0;
        tx_seq[1] = 1;

        error_count[0] = 0;
        error_count[1] = 0;
        success_rate_curr[0] = success_rate;
        success_rate_curr[1] = success_rate;
        do_flag[0] = 1;
        do_flag[1] = 1;
        do_help[0] = 0;
        do_help[1] = 0;
        while (1) {
            curr_cmpl_rounds++;
            // tx
            for (ch_idx = 0; ch_idx < 2; ch_idx++) {
                if (!do_flag[ch_idx])
                    continue;
                if (tx_from_a_to_b(ch_idx, success_rate_curr[ch_idx], tx_seq[ch_idx])) {
                    arb.TxDone(tx_seq[ch_idx]);
                    if (do_help[ch_idx]) {
                        if (tx_seq[ch_idx] < 2)
                            do_flag[ch_idx] = 0;
                        else
                            tx_seq[ch_idx] -= 2;
                    } else {
                        tx_seq[ch_idx] += 2;
                    }
                    if (tx_seq[ch_idx] >= TOTAL_PKT) {
                        // help others
                        //do_flag[ch_idx] = 0;
                        tx_seq[ch_idx] = TOTAL_PKT - 1 - ch_idx;
                        do_help[ch_idx] = 1;
                    }
                    error_count[ch_idx] = 0;
                } else {
                    error_count[ch_idx]++;
                }
            }
            // rx
            for (ch_idx = 0; ch_idx < 2; ch_idx++) {
                if (rx_on_b(ch_idx, rx_seq[ch_idx])) {
                    arb.PutRxSeq(rx_seq[ch_idx]);
                }
            }
            if (arb.IsRxDone()) {
                cmpl_rounds[total_run_i] = curr_cmpl_rounds;
                break;
            }
            if (error_count[0] >= 1000) {
                success_rate_curr[0] = 100;
            }
            if (error_count[1] >= 1000) {
                success_rate_curr[1] = 100;
            }
        }
        //printf("curr_cmpl_rounds = %d\n", curr_cmpl_rounds);
    }

    //arb.Dump();

    return 0;
}

void PathAggregationTest(void)
{
    double avg, jitter, min, max;
    u32 rate;

    PRLOC

    MM_INIT();

    //PathAggregationTestStart();
    test_single_channel(100);
    printf("1ch 100%%, ");
    show_statistics(avg, jitter, min, max);

    test_single_channel(75);
    printf("1ch  75%%, ");
    show_statistics(avg, jitter, min, max);

    test_single_channel(50);
    printf("1ch  50%%, ");
    show_statistics(avg, jitter, min, max);

    test_tow_channel_dup(50);
    printf("dup  50%%, ");
    show_statistics(avg, jitter, min, max);

    test_tow_channel_intv(100);
    printf("itv 100%%, ");
    show_statistics(avg, jitter, min, max);

    test_tow_channel_intv(50);
    printf("itv  50%%, ");
    show_statistics(avg, jitter, min, max);

    test_tow_channel_intv_dup(50);
    printf("i+d  50%%, ");
    show_statistics(avg, jitter, min, max);

    test_tow_channel_dup2(50);
    printf("dp2  50%%, ");
    show_statistics(avg, jitter, min, max);

    test_tow_channel_intv2(50);
    printf("it2  50%%, ");
    show_statistics(avg, jitter, min, max);

    printf("\n");

    test_tow_channel_intv_dup2(50);
    printf("id2  50%%, ");
    show_statistics(avg, jitter, min, max);

    printf("\n");

    rate = 10;

    test_single_channel(rate);
    printf("1ch  %d%%, ", rate);
    show_statistics(avg, jitter, min, max);

    test_tow_channel_dup(rate);
    printf("dup  %d%%, ", rate);
    show_statistics(avg, jitter, min, max);

    test_tow_channel_intv(rate);
    printf("itv  %d%%, ", rate);
    show_statistics(avg, jitter, min, max);

    test_tow_channel_intv_dup(rate);
    printf("i+d  %d%%, ", rate);
    show_statistics(avg, jitter, min, max);

    test_tow_channel_dup2(rate);
    printf("dp2  %d%%, ", rate);
    show_statistics(avg, jitter, min, max);

    test_tow_channel_intv2(rate);
    printf("it2  %d%%, ", rate);
    show_statistics(avg, jitter, min, max);

    MM_UNINIT();
}

