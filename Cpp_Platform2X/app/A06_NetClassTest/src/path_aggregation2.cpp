
#include "Everything_App.hpp"

void SingleTest_Simple(void)
{
    class NetExp outter;
    class NetExp_SinglePath exp;

    outter.Reset(1000);
    outter.innerExp = &exp;
    exp.Init(4, 5, 0);
    outter.RunAll();
    //DNF(outter.CalcAverage());
    printf("%f\n", outter.CalcAverage());

    exp.Init(4, 5, 25);
    outter.RunAll();
    //DNF(outter.CalcAverage());
    printf("%f\n", outter.CalcAverage());

    exp.Init(4, 5, 50);
    outter.RunAll();
    //DNF(outter.CalcAverage());
    printf("%f\n", outter.CalcAverage());

    exp.Init(4, 5, 75);
    outter.RunAll();
    //DNF(outter.CalcAverage());
    printf("%f\n", outter.CalcAverage());
}

void SingleTest(u32 pktNum)
{
    class NetExp outter;
    class NetExp_SinglePath exp;
    u32 rate;
    LibFileIoClass outFile("out.txt", "w+b");

    outFile.FileOpen();

    outter.Reset(10000);
    outter.innerExp = &exp;

    for(rate = 0; rate < 96; rate += 1) {
        exp.Init(pktNum, 20, rate);
        outter.RunAll();
        //DNF(outter.CalcAverage());
        printf("%f\t%d%%\t%s() - %d\n", outter.CalcAverage(), rate, __func__, pktNum);
        fprintf(outFile.fp, "%f\n", outter.CalcAverage());
    }
}

void InterleavedTest(u32 pktNum)
{
    class NetExp outter;
    class NetExp_DualPath exp;
    u32 rate;
    LibFileIoClass outFile("out_Intv.txt", "w+b");

    outFile.FileOpen();

    outter.Reset(10000);
    outter.innerExp = &exp;

    for(rate = 0; rate < 96; rate += 1) {
        exp.Init(pktNum, 20, rate);
        outter.RunAll();
        //DNF(outter.CalcAverage());
        printf("%f\t%d%%\t%s() - %d\n", outter.CalcAverage(), rate, __func__, pktNum);
        fprintf(outFile.fp, "%f\n", outter.CalcAverage());
    }
}

void DupTest(u32 pktNum)
{
    class NetExp outter;
    class NetExp_DualPath_Dup exp;
    u32 rate;
    LibFileIoClass outFile("out_Dup.txt", "w+b");

    outFile.FileOpen();

    outter.Reset(10000);
    outter.innerExp = &exp;

    for(rate = 0; rate < 96; rate += 1) {
        exp.Init(pktNum, 20, rate);
        outter.RunAll();
        //DNF(outter.CalcAverage());
        printf("%f\t%d%%\t%s() - %d\n", outter.CalcAverage(), rate, __func__, pktNum);
        fprintf(outFile.fp, "%f\n", outter.CalcAverage());
    }
}

void DupExTest(u32 pktNum)
{
    class NetExp outter;
    class NetExp_DualPath_DupEx exp;
    u32 rate;
    LibFileIoClass outFile("out_DupEx.txt", "w+b");

    outFile.FileOpen();

    outter.Reset(10000);
    outter.innerExp = &exp;

    for(rate = 0; rate < 96; rate += 1) {
        exp.Init(pktNum, 20, rate);
        outter.RunAll();
        //DNF(outter.CalcAverage());
        printf("%f\t%d%%\t%s() - %d\n", outter.CalcAverage(), rate, __func__, pktNum);
        fprintf(outFile.fp, "%f\n", outter.CalcAverage());
    }
}

void InterleavedTest(u32 pktNum, const char *outputFile)
{
    class NetExp outter;
    class NetExp_DualPath exp;
    u32 rate;
    LibFileIoClass outFile(outputFile, "w+b");

    outFile.FileOpen();

    outter.Reset(10000);
    outter.innerExp = &exp;

    for(rate = 0; rate < 96; rate += 1) {
        exp.Init(pktNum, 20, rate);
        outter.RunAll();
        //DNF(outter.CalcAverage());
        printf("%f\t%d%%\t%s() - %d\n", outter.CalcAverage(), rate, __func__, pktNum);
        fprintf(outFile.fp, "%f\n", outter.CalcAverage());
    }
}

void DupExTest2(u32 pktNum, const char *outputFile)
{
    class NetExp outter;
    class NetExp_DualPath_DupEx exp;
    u32 rate;
    LibFileIoClass outFile(outputFile, "w+b");

    outFile.FileOpen();

    outter.Reset(10000);
    outter.innerExp = &exp;

    for(rate = 0; rate < 96; rate += 1) {
        exp.Init(pktNum, 20, rate);
        outter.RunAll();
        //DNF(outter.CalcAverage());
        printf("%f\t%d%%\t%s() - %d\n", outter.CalcAverage(), rate, __func__, pktNum);
        fprintf(outFile.fp, "%f\n", outter.CalcAverage());
    }
}

class TestDynamicRate {
public:
    void Run(u32 pktNum, class NetExp_SinglePath *exp)
    {
        class NetExp outter;

        outter.Reset(10000);
        outter.innerExp = exp;
        exp->Init(pktNum, 20, 0, 5);
        outter.RunAll();
        printf("%f\t%s() - %d\n", outter.CalcAverage(), __func__, pktNum);
    }
};

class TestDynamicRate50 {
public:
    void Run(u32 pktNum, class NetExp_SinglePath *exp)
    {
        class NetExp outter;

        outter.Reset(20000);
        outter.innerExp = exp;
        exp->Init(pktNum, 10, 50, 5);
        outter.RunAll();
        printf("%f\t%s() - %d\n", outter.CalcAverage(), __func__, pktNum);
    }
};

class TestDynamicRate70 {
public:
    void Run(u32 pktNum, class NetExp_SinglePath *exp)
    {
        class NetExp outter;

        outter.Reset(20000);
        outter.innerExp = exp;
        exp->Init(pktNum, 6, 70, 5);
        outter.RunAll();
        printf("%f\t%s() - %d\n", outter.CalcAverage(), __func__, pktNum);
    }
};

class TestDynamicRate90 {
public:
    void Run(u32 pktNum, class NetExp_SinglePath *exp)
    {
        class NetExp outter;

        outter.Reset(20000);
        outter.innerExp = exp;
        exp->Init(pktNum, 6, 90, 1);
        outter.RunAll();
        printf("%f\t%s() - %d\n", outter.CalcAverage(), __func__, pktNum);
    }
};

void HybridTest(u32 pktNum, const char *fileName)
{
    class NetExp outter;
    class NetExp_DualPath_Hybrid exp;
    u32 rate;
    //LibFileIoClass outFile("out_Hybrid.txt", "w+b");
    LibFileIoClass outFile(fileName, "w+b");

    outFile.FileOpen();

    outter.Reset(10000);
    outter.innerExp = &exp;

    for(rate = 0; rate < 96; rate += 1) {
        exp.Init(pktNum, 20, rate);

        exp.threshold = 9999;
        switch (pktNum) {
            case 2:
                exp.threshold = 66;
                break;
            case 3:
                exp.threshold = 76;
                break;
            case 6:
                break;
            case 12:
                break;
            case 24:
                break;
            default:
                BASIC_ASSERT(0);
                break;
        }
        outter.RunAll();
        //DNF(outter.CalcAverage());
        printf("%f\t%d%%\t%s() - %d\n", outter.CalcAverage(), rate, __func__, pktNum);
        fprintf(outFile.fp, "%f\n", outter.CalcAverage());
    }
}

void HybridExTest(u32 pktNum, const char *fileName)
{
    class NetExp outter;
    class NetExp_DualPath_HybridEx exp;
    u32 rate;
    //LibFileIoClass outFile("out_Hybrid.txt", "w+b");
    LibFileIoClass outFile(fileName, "w+b");

    outFile.FileOpen();

    outter.Reset(10000);
    outter.innerExp = &exp;

    for(rate = 0; rate < 96; rate += 1) {
        exp.Init(pktNum, 20, rate);

        exp.threshold = 9999;
        switch (pktNum) {
            case 2:
                exp.threshold = 49;
                break;
            case 3:
                exp.threshold = 44;
                break;
            case 6:
                exp.threshold = 61;
                break;
            case 12:
                exp.threshold = 69;
                break;
            case 24:
                exp.threshold = 76;
                break;
            default:
                BASIC_ASSERT(0);
                break;
        }
        outter.RunAll();
        //DNF(outter.CalcAverage());
        printf("%f\t%d%%\t%s() - %d\n", outter.CalcAverage(), rate, __func__, pktNum);
        fprintf(outFile.fp, "%f\n", outter.CalcAverage());
    }
}

void PathAggregationTest2()
{
    u32 pktAry[] = {2,3,6,12,24};
    u32 pktNum = 0;
    MM_INIT();
    LibUtil_InitRand();

    REMOVE_UNUSED_WRANING(pktNum);
    REMOVE_UNUSED_WRANING(pktAry[0]);

    //DupExTest(1);
    SingleTest(1);
    //InterleavedTest(1);
#if 0
    {
        char baseDup2[] = "D2_0000.txt";
        char baseIntv[] = "IT_0000.txt";
        for (u32 i = 256; i <= 1024; i=i*2) {
            baseDup2[3] = '0' + i / 1000;
            baseDup2[4] = '0' + (i%1000) / 100;
            baseDup2[5] = '0' + (i%100) / 10;
            baseDup2[6] = '0' + i % 10;
            baseIntv[3] = '0' + i / 1000;
            baseIntv[4] = '0' + (i%1000) / 100;
            baseIntv[5] = '0' + (i%100) / 10;
            baseIntv[6] = '0' + i % 10;
            if (i != 256) {
                InterleavedTest(i, baseIntv);
            }
            DupExTest2(i, baseDup2);
        }
    }
#endif
#if 0
    {
        HybridTest(2, "out_Hybrid2.txt");
        HybridExTest(2, "out_HybridEx2.txt");
    }
#endif
#if 0
    {
        //HybridTest(2, "out_Hybrid2.txt");
        HybridTest(3, "out_Hybrid3.txt");
        HybridTest(6, "out_Hybrid6.txt");
        HybridTest(12, "out_Hybrid12.txt");
        HybridTest(24, "out_Hybrid24.txt");
        //HybridExTest(2, "out_HybridEx2.txt");
        HybridExTest(3, "out_HybridEx3.txt");
        HybridExTest(6, "out_HybridEx6.txt");
        HybridExTest(12, "out_HybridEx12.txt");
        HybridExTest(24, "out_HybridEx24.txt");
    }
#endif
#if 0
    {
        class TestDynamicRate90 test;
        class NetExp_SinglePath sin;
        class NetExp_DualPath intv;
        class NetExp_DualPath_Dup dup;
        class NetExp_DualPath_DupEx dup2;
        class NetExp_DualPath_Hybrid hybrid;
        class NetExp_DualPath_HybridEx hybridEx;
        u32 pktAry[] = {1};

        for (int i = 0; i < ARRAY_AND_SIZE(pktAry); i++) {
            pktNum = pktAry[i];
            DND(pktNum);

            hybrid.threshold = 9999;
            switch (pktNum) {
                case 2:
                    hybrid.threshold = 66;
                    hybridEx.threshold = 49;
                    break;
                case 3:
                    hybrid.threshold = 76;
                    hybridEx.threshold = 44;
                    break;
                case 6:
                    hybridEx.threshold = 61;
                    break;
                case 12:
                    hybridEx.threshold = 69;
                    break;
                case 24:
                    hybridEx.threshold = 76;
                    break;
                default:
                    BASIC_ASSERT(0);
                    break;
            }

            test.Run(pktNum, &sin);
            test.Run(pktNum, &dup);
            test.Run(pktNum, &intv);
            test.Run(pktNum, &dup2);
            test.Run(pktNum, &hybrid);
            test.Run(pktNum, &hybridEx);
        }
    }
#endif
    MM_UNINIT();
}
