#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/types_c.h>

#define BASIC_ASSERT(a) if(a){;}else{printf(" Assertion failed: in %s(), line %d\n",__FUNCTION__,__LINE__);while(1){;}}

class Printer {
public:
    enum {
        PR_BUF_SIZE = 256,
    };
    char pr_buf[PR_BUF_SIZE];
    int print_enable;
    Printer() : print_enable(0) {};

    //log(fmt, ...) printf(("[%d] %s(): " fmt), __LINE__, __FUNCTION__, ##__VA_ARGS__)
    int pr(const char* format, ...) {
        if (print_enable == 0)
            return 0;
        va_list vl;
        va_start(vl, format);
        int ret = vprintf(format, vl);
        va_end(vl);
        return ret;
    }
    int snpr(const char* format, ...) {
        va_list vl;
        va_start(vl, format);
        int ret = vsnprintf(pr_buf, PR_BUF_SIZE, format, vl);
        va_end(vl);
        return ret;
    }
};

#ifdef _USING_DUMP_
class ImgConcator : public Printer {
#else
class ImgConcator_STUB : public Printer {
#endif
public:
    int max_row;
    int h;
    int w;
    int mat_type;
    double font_factor;
    int y_ctr_step;
    std::vector<cv::Mat> mat_vec;
    std::vector<int> row_vec;
    cv::Mat blank;

#ifdef _USING_DUMP_
    ImgConcator() : max_row(0), h(0), w(0), mat_type(-1), font_factor(1.0), y_ctr_step(20) {};
#endif
    void import_impl(cv::Mat &in, int row) {
        if (h == 0) {
            h = in.rows;
        }
        if (w == 0) {
            w = in.cols;
        }
        if (mat_type == -1) {
            mat_type = in.type();
        }
        BASIC_ASSERT(h == in.rows);
        BASIC_ASSERT(w == in.cols);
        BASIC_ASSERT(mat_type == in.type());
        mat_vec.push_back(std::move(in));
        row_vec.push_back(row);
        if (row+1 > max_row) {
            max_row = row+1;
        }
    };
    void import(cv::Mat &in, int row) {
        cv::Mat _new = in.clone();
        import_impl(_new, row);
    };
    void import(const cv::Mat &in, int row) {
        cv::Mat _new = in.clone();
        import_impl(_new, row);
    };
    int calc_max_col() {
        int ret = 0;
        int *ctr = (int *)calloc(max_row, sizeof(int));
        BASIC_ASSERT(ctr != NULL);
        for (std::vector<int>::iterator it = row_vec.begin() ; it != row_vec.end(); ++it) {
            BASIC_ASSERT(*it < max_row);
            ctr[*it]++;
        }
        for (int i = 0; i < max_row; i++) {
            if (ctr[i] > ret) {
                ret = ctr[i];
            }
        }
        free(ctr);
        return ret;
    };
    void hconcat_simple(cv::Mat &dst, cv::Mat &new_img) {
        if (dst.total() == 0) {
            dst = new_img.clone();
        } else {
            cv::hconcat(dst, new_img, dst);
        }
    };
    void vconcat_simple(cv::Mat &dst, cv::Mat &new_img) {
        if (dst.total() == 0) {
            dst = new_img.clone();
        } else {
            cv::vconcat(dst, new_img, dst);
        }
    };
    void vec_points_to_blank(std::vector<cv::Point> &in_vec, int point_color=0) {
        int total = in_vec.size();
        for (int i = 0; i < total; i++) {
            cv::Point &p = in_vec[i];
            blank.at<unsigned char>(p.y, p.x) = point_color;
        }
    }
    void vec_points_to_blank(const std::vector<cv::Point> &in_vec, int point_color=0) {
        int total = in_vec.size();
        for (int i = 0; i < total; i++) {
            const cv::Point &p = in_vec[i];
            blank.at<unsigned char>(p.y, p.x) = point_color;
        }
    }
    void vec_points_init(std::vector<cv::Point> &in_vec, int point_color=0, int bg_color=255) {
        string_img_init(bg_color);
        vec_points_to_blank(in_vec, point_color);
    }
    void vec_points_dump(int row_position, std::vector<cv::Point> &in_vec, int point_color=0, int bg_color=255) {
        vec_points_init(in_vec, point_color, bg_color);
        import(blank, row_position);
    }
    void string_img_init(int color = 255) {
        BASIC_ASSERT(h != 0);
        BASIC_ASSERT(w != 0);
        blank = cv::Mat(h, w, mat_type);
        blank.setTo(cv::Scalar(color)); //background color
    }
    void string_img_put_text(int y, int text_color = 0) {
        static int curr_w = 0;
        static double fontScale = 0.4;
        static int thickness = 1;

        if (curr_w == 0) {
            curr_w = w;
            if (w > 160) {
                fontScale += ((double)w - 160) / (256 - 160) * 0.6;
                thickness = 2;
                y_ctr_step = 25;
            }
        }

        pr("fontScale = %f\n", fontScale);
        cv::putText(blank, pr_buf, cv::Point(20, y), cv::FONT_HERSHEY_SIMPLEX, fontScale*font_factor, cv::Scalar(text_color), thickness);
    }

#define VSNPRINT_TO_BUF() \
    va_list vl; \
    va_start(vl, format); \
    vsnprintf(pr_buf, PR_BUF_SIZE, format, vl); \
    va_end(vl);

    void string_img_put_text(int *y_ctr, const char* format, ...) {
        VSNPRINT_TO_BUF();
        string_img_put_text(*y_ctr);
        *y_ctr += y_ctr_step;
    }
    void string_img_put_text(int y, const char* format, ...) {
        VSNPRINT_TO_BUF();
        string_img_put_text(y);
    }
    void string_img_put_text_color(int color, int *y_ctr, const char* format, ...) {
        VSNPRINT_TO_BUF();
        string_img_put_text(*y_ctr, color);
        *y_ctr += y_ctr_step;
    }
    void string_img_put_text_color(int color, int y, const char* format, ...) {
        VSNPRINT_TO_BUF();
        string_img_put_text(y, color);
    }
    void string_img_import(int which_row) {
        import(blank, which_row);
    };
    void dump_to_file(int error_dump = 0) {
        int num;
        int max_col;
        cv::Mat out;

        if (error_dump) {
            string_img_init(180);
            string_img_put_text(h/2, "error %d", error_dump);
            string_img_import(max_row);
        }

        if (mat_vec.empty()) {
            return;
        }

        num = mat_vec.size();
        max_col = calc_max_col();

        //printf("max_col = %d, num = %d, max_row = %d\n", max_col, num, max_row);
        cv::Mat blank = cv::Mat(mat_vec[0].rows, mat_vec[0].cols, mat_vec[0].type());
        blank.setTo(cv::Scalar(127));
        for (int i = 0; i < max_row; i++) {
            cv::Mat cur;
            int ctr = 0;
            for (int j = 0; j < num; j++) {
                if (row_vec[j] == i) {
                    hconcat_simple(cur, mat_vec[j]);
                    ctr++;
                }
            }
            //padding with blank
            for (; ctr < max_col; ctr++) {
                hconcat_simple(cur, blank);
            }
            vconcat_simple(out, cur);
        }
        cv::imwrite("_img_dump.png", out);
    };

    void contours_to_img_dump(Contours_2D &in) {
        string_img_init(0);
        cv::drawContours(blank, in, -1, cv::Scalar(255), 1);
    }
    
    void contours_all_dump(Contours_2D &in, int &target_row, const char *name) {
        string_img_init(180);
        string_img_put_text(h/2, "%s", name);
        string_img_import(target_row);

        int i = 0;
        int curr_row = target_row;
        for (const auto& contour : in) {
            string_img_init(0);

            //cv::drawContours(blank, std::vector<std::vector<cv::Point>>{contour}, -1, cv::Scalar(255), 1);
            vec_points_to_blank(contour, 255);
            
            string_img_put_text_color(255, h-16, "Contour %d %d", i, contour.size());
            string_img_import(curr_row);
            // update target_row for caller
            if (target_row == curr_row) {
                target_row++;
            }
            // check if needs new row
            if ((i % 8) == 7) {
                curr_row++;
            }
            i++;
        }
    }

    void create_cross_points(std::vector<cv::Point> &out, cv::Point &center) {
        out.push_back(center);
        int x = center.x;
        int y = center.y;
        if (x - 1 >= 0) { out.push_back(cv::Point(x - 1, y)); }
        if (x - 2 >= 0) { out.push_back(cv::Point(x - 2, y)); }
        if (x - 3 >= 0) { out.push_back(cv::Point(x - 3, y)); }
        if (x + 1 < w)  { out.push_back(cv::Point(x + 1, y)); }
        if (x + 2 < w)  { out.push_back(cv::Point(x + 2, y)); }
        if (x + 3 < w)  { out.push_back(cv::Point(x + 3, y)); }
        if (y - 1 >= 0) { out.push_back(cv::Point(x, y - 1)); }
        if (y - 2 >= 0) { out.push_back(cv::Point(x, y - 2)); }
        if (y - 3 >= 0) { out.push_back(cv::Point(x, y - 3)); }
        if (y + 1 < h)  { out.push_back(cv::Point(x, y + 1)); }
        if (y + 2 < h)  { out.push_back(cv::Point(x, y + 2)); }
        if (y + 3 < h)  { out.push_back(cv::Point(x, y + 3)); }
    };
};

#ifdef _USING_DUMP_
class ImgConcator_STUB : public Printer {
#else
class ImgConcator : public Printer {
#endif
public:
    int max_row;
    int h;
    int w;
    int mat_type;
    double font_factor;
    int y_ctr_step;
    std::vector<cv::Mat> mat_vec;
    std::vector<int> row_vec;
    cv::Mat blank;

    void import_impl(...) {};
    void import(...) {};
    int calc_max_col() {return 0;};
    void hconcat_simple(...) {};
    void vconcat_simple(...) {};
    void vec_points_to_blank(...) { };
    void vec_points_init(...) {};
    void vec_points_dump(...) {};
    void string_img_init(...) {};
    void string_img_put_text(...) {};
    void string_img_put_text_color(...) {};
    void string_img_import(...) {};
    void dump_to_file(...) {};
    void contours_to_img_dump(...) {};
    void contours_all_dump(...) {};
    void create_cross_points(...) {};
};

int main()
{
    // path is from build/
    cv::Mat image = cv::imread("../../../DIP/Images/lena256.bmp", cv::IMREAD_GRAYSCALE);

    ImgConcator dump;

    dump.import(image, 0);
    dump.import(image, 0);
    dump.import(image, 0);
    dump.import(image, 2);

    int y = 30;
    dump.font_factor = 1;
    dump.string_img_init(255);
    dump.string_img_put_text(&y, "11111111");
    dump.string_img_put_text(&y, "22222222");
    dump.string_img_put_text(&y, "33333333");
    dump.string_img_put_text(&y, "44444444");
    dump.string_img_import(1);

    dump.dump_to_file(222);

    return 0;
}