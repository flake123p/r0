#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "sys.h"
#include "sys_internal.h"
//#include "pupil.h"
//#include "linked_list.h"

//Linux platform
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <assert.h>

int g_LibIPC_EventCtr = 0;

typedef struct {
	pthread_cond_t cond;
	pthread_mutex_t mutex;
	int flag;
}EVENT_HANDLE_LINUX_t;
int LibIPC_Event_Create(OUT EVENT_HANDLE_t *eventHdlPtr)
{
    int retVal = 0;
	EVENT_HANDLE_LINUX_t *eHdl = (EVENT_HANDLE_LINUX_t *)malloc(sizeof(EVENT_HANDLE_LINUX_t));
	if (eHdl == NULL)
		return -1;

	retVal = pthread_cond_init(&eHdl->cond, NULL);
	if(retVal) return retVal;
	retVal = pthread_mutex_init(&eHdl->mutex, NULL);
	if(retVal) return retVal;

	eHdl->flag = 0;

	*eventHdlPtr = eHdl;

	g_LibIPC_EventCtr++;

	return 0;
}

int LibIPC_Event_Destroy(EVENT_HANDLE_t eventHdl)
{
	EVENT_HANDLE_LINUX_t *eHdl = (EVENT_HANDLE_LINUX_t *)eventHdl;

	int retVal = pthread_mutex_destroy(&eHdl->mutex);
	RETURN_IF(retVal);

	retVal = pthread_cond_destroy(&eHdl->cond);
	RETURN_IF(retVal);

	free(eventHdl);

	g_LibIPC_EventCtr--;

	return 0;
}

int LibIPC_Event_Set(EVENT_HANDLE_t eventHdl)
{
	EVENT_HANDLE_LINUX_t *eHdl = (EVENT_HANDLE_LINUX_t *)eventHdl;
	//PRINT_FUNC;
	pthread_mutex_lock(&eHdl->mutex);
	pthread_cond_signal(&eHdl->cond);
	eHdl->flag = 1;
	pthread_mutex_unlock(&eHdl->mutex);

	return 0;
}

int LibIPC_Event_Wait(EVENT_HANDLE_t eventHdl)
{
	EVENT_HANDLE_LINUX_t *eHdl = (EVENT_HANDLE_LINUX_t *)eventHdl;
	//PRINT_FUNC;
	pthread_mutex_lock(&eHdl->mutex);
	if (eHdl->flag == 1) {
		eHdl->flag = 0;
	} else {
		pthread_cond_wait(&eHdl->cond, &eHdl->mutex);
	}
	pthread_mutex_unlock(&eHdl->mutex);

	return 0;
}

int LibIPC_Event_GetCounter(void)
{
	return g_LibIPC_EventCtr;
}

int g_LibIPC_MutexCtr = 0;

typedef struct {
	pthread_mutex_t mutex;
}MUTEX_HANDLE_LINUX_t;
int LibIPC_Mutex_Create(OUT MUTEX_HANDLE_t *mutexHdlPtr)
{
	MUTEX_HANDLE_LINUX_t *linuxMutexHdl = (MUTEX_HANDLE_LINUX_t *)malloc(sizeof(MUTEX_HANDLE_LINUX_t));

	int retVal = pthread_mutex_init(&linuxMutexHdl->mutex, NULL);

	*mutexHdlPtr = linuxMutexHdl;

	g_LibIPC_MutexCtr++;

	return retVal;
}

int LibIPC_Mutex_Destroy(MUTEX_HANDLE_t mutexHdl)
{
	int ret;

	MUTEX_HANDLE_LINUX_t *linuxMutexHdl = (MUTEX_HANDLE_LINUX_t *)mutexHdl;

	ret = pthread_mutex_destroy(&linuxMutexHdl->mutex);

	free(mutexHdl);

	g_LibIPC_MutexCtr--;

	return ret;
}

int LibIPC_Mutex_Lock(MUTEX_HANDLE_t mutexHdl)
{
	MUTEX_HANDLE_LINUX_t *linuxMutexHdl = (MUTEX_HANDLE_LINUX_t *)mutexHdl;

	return pthread_mutex_lock(&linuxMutexHdl->mutex);
}

int LibIPC_Mutex_Unlock(MUTEX_HANDLE_t mutexHdl)
{
	MUTEX_HANDLE_LINUX_t *linuxMutexHdl = (MUTEX_HANDLE_LINUX_t *)mutexHdl;

	return pthread_mutex_unlock(&linuxMutexHdl->mutex);
}

int LibIPC_Mutex_GetCounter(void)
{
	return g_LibIPC_MutexCtr;
}

int g_LibThread_Ctr;

typedef struct {
	pthread_t thread;
	uint32_t priority;
}THREAD_HANDLE_LINUX_t;
int LibThread_NewHandle(OUT THREAD_HANDLE_t *threadHdlPtr, uint32_t priority /* = TPRI_DEFAULT */)
{
	THREAD_HANDLE_LINUX_t *linuxThreadHdl = (THREAD_HANDLE_LINUX_t *)malloc(sizeof(THREAD_HANDLE_LINUX_t));
	if (linuxThreadHdl == NULL)
		return -1;

	linuxThreadHdl->priority = priority;
	*threadHdlPtr = linuxThreadHdl;
	g_LibThread_Ctr++;
	return 0;
}

int LibThread_Create(THREAD_HANDLE_t threadHdl, ThreadEntryFunc entry, void *arg /* = NULL */)
{
	THREAD_HANDLE_LINUX_t *linuxThreadHdl = (THREAD_HANDLE_LINUX_t *)threadHdl;

	int retVal = pthread_create(&linuxThreadHdl->thread, NULL, entry, arg);

	return retVal;
}

int LibThread_WaitThread(THREAD_HANDLE_t threadHdl)
{
	THREAD_HANDLE_LINUX_t *linuxThreadHdl = (THREAD_HANDLE_LINUX_t *)threadHdl;

	/* wait for the thread to finish */
	int retVal = pthread_join(linuxThreadHdl->thread, NULL);

	return retVal;
}

int LibThread_DestroyHandle(THREAD_HANDLE_t threadHdl)
{
	free(threadHdl);
	g_LibThread_Ctr--;
	return 0;
}

int LibThread_GetCounter(void)
{
	return g_LibThread_Ctr;
}