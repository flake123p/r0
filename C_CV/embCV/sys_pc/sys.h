#ifndef _SYS_H_INCLUDED_
#define _SYS_H_INCLUDED_

#include <stdint.h>
#include <stdio.h>  // flake: remove this
#include "linked_list.h"
#include "macros.h"
#include "type.h"

uint8_t *get_image(uint32_t size);
int release_image(uint8_t *ptr);
void *get_isr_msg(uint32_t size);
int release_isr_msg(void *ptr);
void *get_task_msg(uint32_t size);
int release_task_msg(void *ptr);
void *get_cmd_req_msg(uint32_t size);
int release_cmd_req_msg(void *ptr);
void *get_cmd_res_msg(uint32_t size);
int release_cmd_res_msg(void *ptr);
void *get_evt_msg(uint32_t size);
int release_evt_msg(void *ptr);

void *dummy_msg_alloc(uint32_t size);
int dummy_msg_free(void *ptr);
void *dummy_msg_alloc_tracer(uint32_t size, const char *file_str, int line);
#define DUMMY_ALLOC(size) dummy_msg_alloc_tracer(size,__FILE__,__LINE__)
#define DUMMY_FREE dummy_msg_free

#define USE_INLINE_TRACER ( 1 )

#if ( USE_INLINE_TRACER )
#include "libmem.h"
#define MEM_ALLOC MM_ALLOC
#define MEM_FREE MM_FREE
#define GET_IMG(size) (uint8_t *)MM_ALLOC(size);BASIC_ASSERT((size)!=0)
#define RELEASE_IMG(p) MM_FREE(p)
#define GET_ISR_MSG(size) DUMMY_ALLOC(size)
#define RELEASE_ISR_MSG(p) DUMMY_FREE(p)
#define GET_TSK_MSG(size) DUMMY_ALLOC(size)
#define RELEASE_TSK_MSG(p) DUMMY_FREE(p)
#define GET_CMD_REQ_MSG(size) DUMMY_ALLOC(size)
#define RELEASE_CMD_REQ_MSG(p) DUMMY_FREE(p)
#define GET_CMD_RES_MSG(size) DUMMY_ALLOC(size)
#define RELEASE_CMD_RES_MSG(p) DUMMY_FREE(p)
#define GET_EVT_MSG(size) DUMMY_ALLOC(size)
#define RELEASE_EVT_MSG(p) DUMMY_FREE(p)
#else
#define MEM_ALLOC mem_alloc
#define MEM_FREE mem_free
#define GET_IMG(size) get_image(size)
#define RELEASE_IMG(p) release_image((uint8_t *)p)
#define GET_ISR_MSG(size) get_isr_msg(size)
#define RELEASE_ISR_MSG(p) release_isr_msg(p)
#define GET_TSK_MSG(size) get_task_msg(size)
#define RELEASE_TSK_MSG(p) release_task_msg(p)
#define GET_CMD_REQ_MSG(size) get_cmd_req_msg(size)
#define RELEASE_CMD_REQ_MSG(p) release_cmd_req_msg(p)
#define GET_CMD_RES_MSG(size) get_cmd_res_msg(size)
#define RELEASE_CMD_RES_MSG(p) release_cmd_res_msg(p)
#define GET_EVT_MSG(size) get_evt_msg(size)
#define RELEASE_EVT_MSG(p) release_evt_msg(p)
#endif

void mem_init();
void mem_uninit();
void *mem_alloc(size_t size);
int mem_free(void *ptr);

void sys_print_control(int en);
void sys_print(const char *format, ...);

void sys_assert(int expression, const char *file, int line);
//TODO: disable compiling switch
#define BASIC_ASSERT(a) sys_assert(a, __FILE__, __LINE__)
//NOTE: Prepare caller_file & caller_line before use this
#define CALLER_ASSERT3(a) if(a){;}else{printf(" CALLER asserted: in %s(), line %ld\n",caller_file,caller_line);BASIC_ASSERT(0);}
#define CALLER_PARA3 , const char * caller_file, unsigned long caller_line
#define CALLER_ARG3 ,__FUNCTION__,__LINE__

#define ASSERT_IF(retVal) if(retVal){BASIC_ASSERT(0);};
#define ASSERT_CHK(a, b)  a=b;ASSERT_IF(a)
#define RETURN_IF(retVal) if(retVal){return (retVal);}
#define RETURN_CHK(a, b)  a=b;RETURN_IF(a)
#define RETURN_WHEN(condition, retVal) if(condition){return (retVal);}

#define BASIC_PRINT(fmt, ...) printf(fmt, ##__VA_ARGS__)

#define BASIC_TEST_CASE(test_func) \
    do { \
        printf("[%s] START\n", #test_func); \
        mem_init(); \
        test_func(); \
        mem_uninit(); \
        printf("[%s] PASS\n", #test_func); \
    } while(0);



typedef void *(*ThreadEntryFunc)(void *);

typedef void * msg_task_handle_t; 

// USE MACRO PLEASE
int msg_task_new_handle(OUT msg_task_handle_t *hdl_ptr, uint32_t priority);
int msg_task_destory_handle(msg_task_handle_t hdl);
int msg_task_create(msg_task_handle_t hdl, ThreadEntryFunc cb, void *arg);
int msg_task_wait(msg_task_handle_t hdl);

int msg_task_new_handle_no_os(OUT msg_task_handle_t *hdl_ptr, uint32_t priority);
int msg_task_destory_handle_no_os(msg_task_handle_t hdl);

#ifdef _NO_OS_
#define MSGTASK_NEW_HANDLE(hdl_ptr,priority) msg_task_new_handle_no_os(hdl_ptr,priority)
#define MSGTASK_DESTROY_HANDLE(hdl) msg_task_destory_handle_no_os(hdl)
#define MSGTASK_CRREATE(hdl,cb,arg)
#define MSGTASK_WAIT(hdl)

#else // #ifdef _NO_OS_
#define MSGTASK_NEW_HANDLE(hdl_ptr,priority) msg_task_new_handle(hdl_ptr,priority)
#define MSGTASK_DESTROY_HANDLE(hdl) msg_task_destory_handle(hdl)
#define MSGTASK_CRREATE(hdl,cb,arg) msg_task_create(hdl,cb,arg)
#define MSGTASK_WAIT(hdl) msg_task_wait(hdl)

#endif // #ifdef _NO_OS_



// USE MACRO PLEASE
void msg_send(msg_task_handle_t hdl, void *msg);
void *msg_receive(msg_task_handle_t hdl);
void msg_wait(msg_task_handle_t hdl);
void *msg_wait_and_receive(msg_task_handle_t hdl);

void msg_send_no_os(msg_task_handle_t hdl, void *msg);
void *msg_receive_no_os(msg_task_handle_t hdl);

#ifdef _NO_OS_
#define MSGTASK_MSG_SEND(hdl,msg) msg_send_no_os(hdl,msg) // Please implement MSGTASK_MSG_SEND on upper layer
#define MSGTASK_MSG_RECV(hdl) msg_receive_no_os(hdl)
#define MSGTASK_MSG_WAIT(hdl)
#define MSGTASK_MSG_WAIT_n_RECV(hdl) msg_receive_no_os(hdl)

#else // #ifdef _NO_OS_
#define MSGTASK_MSG_SEND(hdl,msg) msg_send(hdl,msg)
#define MSGTASK_MSG_RECV(hdl) msg_receive(hdl)
#define MSGTASK_MSG_WAIT(hdl) msg_wait(hdl)
#define MSGTASK_MSG_WAIT_n_RECV(hdl) msg_wait_and_receive(hdl)

#endif // #ifdef _NO_OS_

typedef void (*state_machine_t)(void);
void register_no_os_sm(state_machine_t sm);

// cmd req src = tool
// cmd req dst = task_lo
// cmd res src = task_lo
// cmd res dst = tool
// evt src     = task_hi
// evt dst     = tool
int cmd_internal_interface_config(msg_task_handle_t task_hi_hdl, msg_task_handle_t task_lo_hdl);
int cmd_external_interface_config(msg_task_handle_t tool_cmd_hdl, msg_task_handle_t tool_evt_hdl);
int cmd_req_send(void *msg);
int cmd_res_send(void *msg);
int event_send(void *msg);

#define PRLOC prloc(__FILE__, __LINE__);
void prloc(const char *file, int line);

// int sys_memlock_init();
// int sys_memlock_uninit();
// int sys_memlock_get();
// int sys_memlock_release();
//#define LOCK_MEM sys_memlock_get()
//#define UNLOCK_MEM sys_memlock_release()
#define LOCK_MEM
#define UNLOCK_MEM

int sys_magic_number(void);

void sys_internal_counter_clear_check(void);

#define REMOVE_UNUSED_WRANING(a) (a=a)

#endif//_SYS_H_INCLUDED_
