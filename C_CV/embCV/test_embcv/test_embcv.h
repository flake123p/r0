#ifndef _TEST_EMB_CV_H_INCLUDED_
#define _TEST_EMB_CV_H_INCLUDED_

#include "sys.h"

int test_embcv();

#include <opencv2/opencv.hpp>
#include "embcv_api.h"

void test_embcv_mat_to_cv(_Mat src, cv::Mat &dst_cv);
cv::Mat test_embcv_mat_to_cv_create(_Mat src);
void test_embcv_mat_from_cv(cv::Mat &src_cv, _Mat dst);
void test_embcv_mat_from_cv_create(cv::Mat &src_cv, _Mat dst);
void test_embcv_mat_compare_cv(cv::Mat &cv_mat, _Mat my_mat CALLER_PARA3);
int test_embcv_mat_compare_cv2(cv::Mat &cv_mat, _Mat my_mat CALLER_PARA3);
void test_embcv_mat_compare_cv_trunc(FLOAT_T tolerance, cv::Mat &cv_mat, _Mat my_mat CALLER_PARA3);
#define TE_MAT_COMPARE_CV(a,b) test_embcv_mat_compare_cv(a,b CALLER_ARG3)
#define TE_MAT_COMPARE_CV2(a,b) test_embcv_mat_compare_cv2(a,b CALLER_ARG3)
#define TE_MAT_COMPARE_CV_TRUNC(tol,a,b) test_embcv_mat_compare_cv_trunc(tol,a,b CALLER_ARG3)
void test_embcv_mat_dump(_Mat my_mat, const char *log);
void test_embcv_mat_dump_cv(cv::Mat &cv_mat, const char *log);
#define TE_MAT_DUMP(a) test_embcv_mat_dump(a, #a)
#define TE_MAT_DUMP_CV(a) test_embcv_mat_dump_cv(a, #a)
void test_embcv_mat_fill_random_int(_Mat src, int min, int max);
void test_embcv_mat_fill_random_int_cv(cv::Mat &src, int min, int max);
void test_embcv_mat_fill_random_float(_Mat src, FLOAT_T min, FLOAT_T max);
void test_embcv_mat_fill_random_float_cv(cv::Mat &src, FLOAT_T min, FLOAT_T max);
int test_embcv_mat_cv_type_get();
void test_embcv_asymm_diag_fill_cv(cv::Mat &src, cv::Mat &dst);

int test_embcv_float_compare(FLOAT_T tolerance, FLOAT_T a, FLOAT_T b CALLER_PARA3);
#define TE_FLOAT_COMPARE(tol,a,b) test_embcv_float_compare(tol,a,b CALLER_ARG3)

int test_embcv_is_file_exist(const char *file_with_path);

#if 0
#define BASIC_TEST_CASE(test_func) \
    do { \
        printf("[%s] START\n", #test_func); \
        mem_init(); \
        test_func(); \
        mem_uninit(); \
        printf("[%s] PASS\n", #test_func); \
    } while(0);
#endif

int test_embcv_svd();
int test_embcv_imgproc();

#endif//_TEST_EMB_CV_H_INCLUDED_