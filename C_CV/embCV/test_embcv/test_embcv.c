#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <assert.h>

#include <unistd.h>
#include "test_embcv.h"

#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
//using namespace cv;

int test_embcv()
{
    MM_INIT();
    //test_embcv_svd_backsub();
    MM_UNINIT();
    return 0;
}

void test_embcv_mat_print(_Mat src)
{
    if (!src->data) {
        BASIC_PRINT("matrix is empty\n");
    }
    
    BASIC_PRINT("matrix(%dx%d):\n", src->rows, src->cols);
    for (int i=0; i < src->rows; i++) {
        const FLOAT_T* dptr = (src->data + i*src->cols);
        for (int j=0; j < src->cols; j++) {
            BASIC_PRINT("%.16g ", dptr[j]);
        }
        BASIC_PRINT("\n");
    }
}

void test_embcv_mat_create(_Mat dst, int height, int width)
{
    dst->rows = height > 0 ? height : 0;
    dst->cols = width > 0 ? width : 0;
    dst->is_able_to_free = 1;
    dst->data = (FLOAT_T*) MEM_ALLOC(sizeof(FLOAT_T)*dst->rows*dst->cols);
    BASIC_ASSERT(dst->data != NULL);
}

void test_embcv_mat_to_cv(_Mat src, cv::Mat &dst_cv)
{
    for (int i=0; i < src->rows; i++) {
        for (int j=0; j < src->cols; j++) {
            dst_cv.at<FLOAT_T>(i,j) = src->data[i*src->cols+j];
        }
    }
}

cv::Mat test_embcv_mat_to_cv_create(_Mat src)
{
    int type;
    if (sizeof(FLOAT_T) == sizeof(double)) {
        type = CV_64F;
    } else {
        type = CV_32F;
    }
    return cv::Mat(src->rows, src->cols, type, src->data);
}

void test_embcv_mat_from_cv(cv::Mat &src_cv, _Mat dst)
{
    for (int i=0; i < src_cv.rows; i++) {
        for (int j=0; j < src_cv.cols; j++) {
            dst->data[i*src_cv.cols+j] = src_cv.at<FLOAT_T>(i,j);
        }
    }
}

void test_embcv_mat_from_cv_create(cv::Mat &src_cv, _Mat dst)
{
    test_embcv_mat_create(dst, src_cv.rows, src_cv.cols);
    test_embcv_mat_from_cv(src_cv, dst);
}

void test_embcv_mat_compare_cv(cv::Mat &cv_mat, _Mat my_mat CALLER_PARA3)
{
    CALLER_ASSERT3(cv_mat.cols == my_mat->cols);
    CALLER_ASSERT3(cv_mat.rows == my_mat->rows);
    for (int i=0; i < cv_mat.rows; i++) {
        for (int j=0; j < cv_mat.cols; j++) {
            // printf("xxxxxxxxxxxxxxx: i=%d, j=%d, cv_value=%.16f, my_value=%.16f, diff=%.16f\n",
            //     i,
            //     j,
            //     cv_mat.at<FLOAT_T>(i,j),
            //     my_mat->data[i*cv_mat.cols+j],
            //     my_mat->data[i*cv_mat.cols+j] - cv_mat.at<FLOAT_T>(i,j)
            // );
            if ( my_mat->data[i*cv_mat.cols+j] != cv_mat.at<FLOAT_T>(i,j) ) {
                printf("Matrix Compare Failed1: i=%d, j=%d, cv_value=%.16f, my_value=%.16f, diff=%.16f\n",
                    i,
                    j,
                    cv_mat.at<FLOAT_T>(i,j),
                    my_mat->data[i*cv_mat.cols+j],
                    my_mat->data[i*cv_mat.cols+j] - cv_mat.at<FLOAT_T>(i,j)
                );
                TE_MAT_DUMP(my_mat);
                TE_MAT_DUMP_CV(cv_mat);
                CALLER_ASSERT3(0);
            }
        }
    }
}

int test_embcv_mat_compare_cv2(cv::Mat &cv_mat, _Mat my_mat CALLER_PARA3)
{
    CALLER_ASSERT3(cv_mat.cols == my_mat->cols);
    CALLER_ASSERT3(cv_mat.rows == my_mat->rows);
    for (int i=0; i < cv_mat.rows; i++) {
        for (int j=0; j < cv_mat.cols; j++) {
            if ( my_mat->data[i*cv_mat.cols+j] != cv_mat.at<FLOAT_T>(i,j) ) {
                printf("Matrix Compare Failed2: i=%d, j=%d, cv_value=%.16f, my_value=%.16f, diff=%.16f\n",
                    i,
                    j,
                    cv_mat.at<FLOAT_T>(i,j),
                    my_mat->data[i*cv_mat.cols+j],
                    my_mat->data[i*cv_mat.cols+j] - cv_mat.at<FLOAT_T>(i,j)
                );
                TE_MAT_DUMP(my_mat);
                TE_MAT_DUMP_CV(cv_mat);
                return 1;
            }
        }
    }
    return 0;
}

void test_embcv_mat_compare_cv_trunc(FLOAT_T tolerance, cv::Mat &cv_mat, _Mat my_mat CALLER_PARA3)
{
    CALLER_ASSERT3(cv_mat.cols == my_mat->cols);
    CALLER_ASSERT3(cv_mat.rows == my_mat->rows);
    for (int i=0; i < cv_mat.rows; i++) {
        for (int j=0; j < cv_mat.cols; j++) {
            if ( my_mat->data[i*cv_mat.cols+j] != cv_mat.at<FLOAT_T>(i,j) ) {
                if (fabs(my_mat->data[i*cv_mat.cols+j] - cv_mat.at<FLOAT_T>(i,j)) <= tolerance) {
                    continue;
                }
                printf("Matrix Compare Failed1: i=%d, j=%d, cv_value=%.16f, my_value=%.16f, diff=%.16f (tol=%f)\n",
                    i,
                    j,
                    cv_mat.at<FLOAT_T>(i,j),
                    my_mat->data[i*cv_mat.cols+j],
                    my_mat->data[i*cv_mat.cols+j] - cv_mat.at<FLOAT_T>(i,j),
                    tolerance
                );
                TE_MAT_DUMP(my_mat);
                TE_MAT_DUMP_CV(cv_mat);
                CALLER_ASSERT3(0);
            }
        }
    }
}

void test_embcv_mat_dump(_Mat my_mat, const char *log)
{
    printf("[cv::Mat Dump] %s :\n", log);
    test_embcv_mat_print(my_mat);
}

void test_embcv_mat_dump_cv(cv::Mat &cv_mat, const char *log)
{
    printf("[cv::Mat Dump] %s :\n", log);
    std::cout << "CV matrix(" << cv_mat.rows << "x" << cv_mat.cols << ")" << std::endl;
    std::cout << cv_mat << std::endl;
}

void test_embcv_mat_fill_random_int(_Mat src, int min, int max)
{
    BASIC_ASSERT(max >= min);

    for (int i = 0; i < src->rows*src->cols; i++) {
        src->data[i] = (FLOAT_T)((rand() % (max - min + 1)) + min);
    }
}

void test_embcv_mat_fill_random_int_cv(cv::Mat &src, int min, int max)
{
    BASIC_ASSERT(max >= min);

    for (int i=0; i < src.rows; i++) {
        for (int j=0; j < src.cols; j++) {
            src.at<FLOAT_T>(i,j) = (FLOAT_T)((rand() % (max - min + 1)) + min);
        }
    }
}

void test_embcv_mat_fill_random_float(_Mat src, FLOAT_T min, FLOAT_T max)
{
    FLOAT_T a = max - min;

    BASIC_ASSERT(max >= min);
    for (int i = 0; i < src->rows*src->cols; i++) {
        src->data[i] = (FLOAT_T)rand()/(FLOAT_T)(RAND_MAX/a) + min;
    }
}

void test_embcv_mat_fill_random_float_cv(cv::Mat &src, FLOAT_T min, FLOAT_T max)
{
    FLOAT_T a = max - min;

    BASIC_ASSERT(max >= min);
    for (int i=0; i < src.rows; i++) {
        for (int j=0; j < src.cols; j++) {
            src.at<FLOAT_T>(i,j) = (FLOAT_T)rand()/(FLOAT_T)(RAND_MAX/a) + min;
        }
    }
}

int test_embcv_mat_cv_type_get()
{
    if (sizeof(FLOAT_T) == sizeof(double)) {
        return CV_64F;
    } else if (sizeof(FLOAT_T) == sizeof(float))
        return CV_32F;
    else {
        BASIC_ASSERT(0);
        return 0;
    }
}

void test_embcv_asymm_diag_fill_cv(cv::Mat &src, cv::Mat &dst)
{
    // if (dst.rows != src.rows) {
    //     TE_MAT_DUMP_CV(src);
    //     TE_MAT_DUMP_CV(dst);
    //     BASIC_ASSERT(0);
    // }

    for (int i = 0; i < dst.rows; i++) {
        for (int j = 0; j < dst.cols; j++) {
            if (i == j) {
                if (i < src.rows)
                    dst.at<FLOAT_T>(i,j) = src.at<FLOAT_T>(i,0);
                else
                    dst.at<FLOAT_T>(i,j) = 0;
            } else {
                dst.at<FLOAT_T>(i,j) = 0;
            }
        }        
    }

    return;
}

int test_embcv_float_compare(FLOAT_T tolerance, FLOAT_T a, FLOAT_T b CALLER_PARA3)
{
    if ( a != b ) {
        if (fabs(a - b) <= tolerance) {
            return 0;
        }
        printf("Float Compare Failed1: a=%.16f, b=%.16f, diff=%.16f (tol=%f)\n",
            a,
            b,
            a-b,
            tolerance
        );
        CALLER_ASSERT3(0);
    }
    return 0;
}

int test_embcv_is_file_exist(const char *file_with_path)
{
    FILE * pFile;
    pFile = fopen (file_with_path,"r");
    if (pFile!=NULL)
    {
        fclose (pFile);
        return 1;
    }
    return 0;
}