#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <assert.h>
#include <unistd.h>

#include <iostream>
#include <opencv2/opencv.hpp>

#include "embcv_api.h"
#include "test_embcv.h"


using namespace std;
//using namespace cv;


void test_embcv_svd_backsub()
{
    //FLOAT_T tolerance = 0.0;
    int int_min = 0;
    int int_max = 6;
    //FLOAT_T float_min = -1.0; //-10000000.0;
    //FLOAT_T float_max = 1.0; //10000000.0;
    int m, n;

    int b_cols = 0;

    for (m = 1; m < 10; m++) {
        for (n = 1; n < 10; n++) {
            for (b_cols = 1; b_cols < 6; b_cols++) {
                int w_len = n < m ? n : m;
                cv::Mat U, W, VT, C;
                cv::Mat A = cv::Mat(m, n, test_embcv_mat_cv_type_get());
                cv::Mat W_Diag = cv::Mat(w_len, w_len, test_embcv_mat_cv_type_get());

                Mat_t_dclr(a); Mat_t_dclr(w); Mat_t_dclr(u); Mat_t_dclr(vt); Mat_t_dclr(b); Mat_t_dclr(x);
                embcv_mat_buf_create(&a, m, n);

                // int test
                test_embcv_mat_fill_random_int(&a, int_min, int_max);
                test_embcv_mat_to_cv(&a, A);
                TE_MAT_COMPARE_CV(A, &a);

                //matrixF_SVDecomp(&a, &w, &u, &vt);
                cv::SVD::compute(A, W, U, VT);
                // TE_MAT_DUMP(&a);
                // TE_MAT_DUMP_CV(A);
                // TE_MAT_DUMP_CV(W);
                // TE_MAT_DUMP_CV(U);
                // TE_MAT_DUMP_CV(VT);
                embcv_mat_buf_create(&u, U.rows, U.cols);
                embcv_mat_buf_create(&w, W.rows, W.cols);
                embcv_mat_buf_create(&vt, VT.rows, VT.cols);
                test_embcv_mat_from_cv(U, &u);
                test_embcv_mat_from_cv(W, &w);
                test_embcv_mat_from_cv(VT, &vt);

                embcv_mat_buf_create(&b, m, b_cols);
                for (int i = 0; i < b.rows * b.cols; i++) {
                    b.data[i] = 100.0 + i;
                }
                cv::Mat B( m, b_cols, test_embcv_mat_cv_type_get(), b.data );
                cv::Mat X;
                cv::SVBackSubst(W, U, VT, B, X);
                //TE_MAT_DUMP_CV(X);
                
                _SVD_backSubst(&w, &u, &vt, &b, &x);
                //TE_MAT_DUMP(&x);

                TE_MAT_COMPARE_CV(X, &x);

                embcv_mat_buf_release(&a);
                embcv_mat_buf_release(&w);
                embcv_mat_buf_release(&u);
                embcv_mat_buf_release(&vt);
                embcv_mat_buf_release(&b);
                embcv_mat_buf_release(&x);
            }
        }
    }
}

void test_embcv_svd_compute()
{
    FLOAT_T tolerance = 0.0;
    int int_min = -10000000;
    int int_max = 10000000;
    FLOAT_T float_min = -1.0; //-10000000.0;
    FLOAT_T float_max = 1.0; //10000000.0;
    int m, n;

    for (m = 1; m < 10; m++) {
        for (n = 1; n < 10; n++) {
            int w_len = n < m ? n : m;
            cv::Mat U, W, VT, C;
            cv::Mat A = cv::Mat(m, n, test_embcv_mat_cv_type_get());
            cv::Mat W_Diag = cv::Mat(w_len, w_len, test_embcv_mat_cv_type_get());

            Mat_t_dclr(a); Mat_t_dclr(w); Mat_t_dclr(w_diag); Mat_t_dclr(u); Mat_t_dclr(vt);
            Mat_t_dclr(temp); Mat_t_dclr(temp2);
            embcv_mat_buf_create(&a, m, n);
            embcv_mat_buf_create(&w_diag, w_len, w_len);
            embcv_mat_buf_create(&temp, m, w_len);
            embcv_mat_buf_create(&temp2, m, n);

            // int test
            test_embcv_mat_fill_random_int(&a, int_min, int_max);
            test_embcv_mat_to_cv(&a, A);
            TE_MAT_COMPARE_CV(A, &a);

            _SVD_compute(&a, &w, &u, &vt);
            cv::SVD::compute(A, W, U, VT);

            TE_MAT_COMPARE_CV_TRUNC(tolerance, W, &w);
            TE_MAT_COMPARE_CV_TRUNC(tolerance, U, &u);
            TE_MAT_COMPARE_CV_TRUNC(tolerance, VT, &vt);

            test_embcv_asymm_diag_fill_cv(W, W_Diag);
            // TE_MAT_DUMP_CV(W);
            // TE_MAT_DUMP_CV(W_Diag);
            // TE_MAT_DUMP_CV(U);
            // TE_MAT_DUMP_CV(VT);
            embcv_mat_asymm_diag_fill(&w, &w_diag);
            // TE_MAT_DUMP(&w);
            // TE_MAT_DUMP(&w_diag);

            C = U * W_Diag * VT;
            embcv_mat_mul(&u, &w_diag, &temp);
            embcv_mat_mul(&temp, &vt, &temp2);
            TE_MAT_COMPARE_CV_TRUNC(tolerance, C, &temp2);
            TE_MAT_COMPARE_CV_TRUNC(1.0/1000000.0, C, &a); // have truncation error in default
            embcv_mat_buf_release(&w);
            embcv_mat_buf_release(&u);
            embcv_mat_buf_release(&vt);

            // float test
            test_embcv_mat_fill_random_float(&a, float_min, float_max);
            test_embcv_mat_to_cv(&a, A);
            TE_MAT_COMPARE_CV(A, &a);

            _SVD_compute(&a, &w, &u, &vt);
            cv::SVD::compute(A, W, U, VT);

            TE_MAT_COMPARE_CV_TRUNC(tolerance, W, &w);
            TE_MAT_COMPARE_CV_TRUNC(tolerance, U, &u);
            TE_MAT_COMPARE_CV_TRUNC(tolerance, VT, &vt);

            test_embcv_asymm_diag_fill_cv(W, W_Diag);
            embcv_mat_asymm_diag_fill(&w, &w_diag);

            C = U * W_Diag * VT;
            embcv_mat_mul(&u, &w_diag, &temp);
            embcv_mat_mul(&temp, &vt, &temp2);
            TE_MAT_COMPARE_CV_TRUNC(tolerance, C, &temp2);
            TE_MAT_COMPARE_CV_TRUNC(1.0/1000000.0, C, &a); // have truncation error in default


            embcv_mat_buf_release(&a);
            embcv_mat_buf_release(&w);
            embcv_mat_buf_release(&w_diag);
            embcv_mat_buf_release(&u);
            embcv_mat_buf_release(&vt);
            embcv_mat_buf_release(&temp);
            embcv_mat_buf_release(&temp2);
        }
    }
}

int test_embcv_svd()
{
    BASIC_TEST_CASE(test_embcv_svd_backsub);
    BASIC_TEST_CASE(test_embcv_svd_compute);

    return 0;
}