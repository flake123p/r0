#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <assert.h>
#include <unistd.h>

#include <iostream>
#include <opencv2/opencv.hpp>

#include "embcv_api.h"
#include "test_embcv.h"

using namespace std;
using namespace cv;

void test_embcv_imgproc_fitEllipse_impl(_RotatedRect_t *output, _Point_I *input, size_t input_len)
{
    RotatedRect ret_cv;

    vector<Point2f>pts;

    for (size_t i = 0; i < input_len; i++) {
        pts.push_back(Point2f(input[i].x, input[i].y));    
    }
    ret_cv = fitEllipse(pts);
    // printf("center = %f / %f, size = %f / %f, angle = %f\n",
    //     ret_cv.center.x,
    //     ret_cv.center.y,
    //     ret_cv.size.width,
    //     ret_cv.size.height,
    //     ret_cv.angle);

    _RotatedRect_t my_ret;
    fitEllipse_generic(input, input_len, &my_ret);
    // printf("center = %f / %f, size = %f / %f, angle = %f\n",
    //     my_ret.x,
    //     my_ret.y,
    //     my_ret.width,
    //     my_ret.height,
    //     my_ret.angle);

    *output = my_ret;
}

void test_embcv_imgproc_fitEllipse()
{
    _RotatedRect_t gold_out_1 = {
        98.968727,
        44.976013,
        36.015713,
        92.059372,
        90.901993
    };
    _Point_I gold_in_1[] = {
        {100, 27},
        {145, 45},
        {129, 59},
        {99, 63},
        {53, 45},
        {72, 30}
    };
    _RotatedRect_t gold_out_2 = {
        217.370895,
        77.822464,
        78.901093,
        133.113312,
        179.930923
    };
    _Point_I gold_in_2[] = {
        {183, 45},
        {242, 26},
        {253, 49},
        {256, 91},
        {233, 139},
        {189, 124},
        {178, 74}
    };
    _RotatedRect_t gold_out_3 = {
        367.232178,
        78.163750,
        79.443207,
        132.380798,
        134.748199
    };
    _Point_I gold_in_3[] = {
        {315, 39},
        {327, 27},
        {373, 33},
        {385, 41},
        {421, 95},
        {414, 125},
        {401, 132},
        {381, 131}
    };

    //FLOAT_T tol = 1000000.0;
    FLOAT_T tol = 50.0 / 1000000.0;
    _RotatedRect_t result;
    _RotatedRect_t *gold_out;
    test_embcv_imgproc_fitEllipse_impl(&result, ARRAY_AND_SIZE(gold_in_1));
    gold_out = &gold_out_1;
    TE_FLOAT_COMPARE(tol, gold_out->x, result.x);
    TE_FLOAT_COMPARE(tol, gold_out->y, result.y);
    TE_FLOAT_COMPARE(tol, gold_out->width, result.width);
    TE_FLOAT_COMPARE(tol, gold_out->height, result.height);
    TE_FLOAT_COMPARE(tol, gold_out->angle, result.angle);

    test_embcv_imgproc_fitEllipse_impl(&result, ARRAY_AND_SIZE(gold_in_2));
    gold_out = &gold_out_2;
    TE_FLOAT_COMPARE(tol, gold_out->x, result.x);
    TE_FLOAT_COMPARE(tol, gold_out->y, result.y);
    TE_FLOAT_COMPARE(tol, gold_out->width, result.width);
    TE_FLOAT_COMPARE(tol, gold_out->height, result.height);
    TE_FLOAT_COMPARE(tol, gold_out->angle, result.angle);

    test_embcv_imgproc_fitEllipse_impl(&result, ARRAY_AND_SIZE(gold_in_3));
    gold_out = &gold_out_3;
    TE_FLOAT_COMPARE(tol, gold_out->x, result.x);
    TE_FLOAT_COMPARE(tol, gold_out->y, result.y);
    TE_FLOAT_COMPARE(tol, gold_out->width, result.width);
    TE_FLOAT_COMPARE(tol, gold_out->height, result.height);
    TE_FLOAT_COMPARE(tol, gold_out->angle, result.angle);
}

int test_embcv_imgproc()
{
    printf("[%s] start (%d)\n", __func__, sys_magic_number());

    // BASIC_TEST_CASE(test_imgproc_resize);
    // BASIC_TEST_CASE(test_imgproc_broder);
    // BASIC_TEST_CASE(test_imgproc_point_list);
    // BASIC_TEST_CASE(test_imgproc_contour_list);
    // BASIC_TEST_CASE(test_imgproc_hierarchy_list);
    // BASIC_TEST_CASE(test_imgproc_findcontour);
    // BASIC_TEST_CASE(test_embcv_imgproc_fitEllipse);
    
    BASIC_TEST_CASE(test_embcv_imgproc_fitEllipse);

    // mem_init();
    // // test_imgproc_resize();
    // // test_imgproc_broder();
    // // test_imgproc_point_list();
    // // test_imgproc_contour_list();
    // // test_imgproc_hierarchy_list();
    // test_imgproc_findcontour();
    // mem_uninit();

    // test_matrix_svd();

    return 0;
}