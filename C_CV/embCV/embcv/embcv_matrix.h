#ifndef _EMB_CV_MATRIX_H_INCLUDED_
#define _EMB_CV_MATRIX_H_INCLUDED_

#include "embcv_api.h"

uint64_t embcv_rng_create(uint32_t _state);
uint32_t embcv_rng_next(uint64_t *src);

/* MATH functions */
FLOAT_T embcv_hypot(FLOAT_T a, FLOAT_T b);
int embcv_givens(FLOAT_T *A, FLOAT_T *B, int n, FLOAT_T c, FLOAT_T s);
void embcv_get_ofs(int32_t i, FLOAT_T eps, _Point_F *out);

void embcv_mat_buf_create(_Mat dst, int height, int width);
void embcv_mat_buf_create_from_data(_Mat dst, int height, int width, FLOAT_T* data_ptr);
void embcv_mat_buf_release(_Mat src);
void embcv_mat_transpose(_Mat src, _Mat dst);
void embcv_mat_copy_to(_Mat src, _Mat dst);
void embcv_mat_clone_alloc(_Mat src, _Mat dst);
void embcv_mat_asymm_diag_fill(_Mat src, _Mat dst);
void embcv_mat_mul(_Mat a, _Mat b, _Mat dst);

#endif//_EMB_CV_MATRIX_H_INCLUDED_