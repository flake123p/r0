#include <stdlib.h>
#include <stdint.h>
#include <float.h>

#include <math.h> // sqrt ...

#include "embcv_api.h"

uint64_t embcv_rng_create(uint32_t _state)
{
    return _state ? _state : 0xffffffff;
}

uint32_t embcv_rng_next(uint64_t *src)
{
    *src = (uint64_t)(unsigned)*src* /*CV_RNG_COEFF*/ 4164903690U + (unsigned)(*src >> 32);
    return (unsigned)*src;
}


FLOAT_T embcv_hypot(FLOAT_T a, FLOAT_T b)
{
    // return sqrt(a*a + b*b);
    a = ABS(a);
    b = ABS(b);
    if( a > b ) {
        b /= a;
        return (FLOAT_T)( a * sqrt(1 + b*b));
    }
    if( b > 0 ) {
        a /= b;
        return (FLOAT_T)( b * sqrt(1 + a*a));
    }
    return 0;
}

int embcv_givens(FLOAT_T *A, FLOAT_T *B, int n, FLOAT_T c, FLOAT_T s)
{
    if (sizeof(FLOAT_T) == 4) {
        if (n < 4)
            return 0;
    }

    int k = 0;
    // v_float32 c4 = vx_setall_f32(c), s4 = vx_setall_f32(s);
    for(; k < n; k++) {
        FLOAT_T a0 = A[k], b0 = B[k];
        FLOAT_T t0 = (a0 * c) + (b0 * s);
        FLOAT_T t1 = (b0 * c) - (a0 * s);

        A[k] = t0;
        B[k] = t1;
    }

    return k;
}

void embcv_get_ofs(int32_t i, FLOAT_T eps, _Point_F *out)
{
    out->x = ((i & 1)*2 - 1)*eps;
    out->y = ((i & 2) - 1)*eps;
}

void embcv_mat_buf_create(_Mat dst, int height, int width)
{
    dst->rows = height > 0 ? height : 0;
    dst->cols = width > 0 ? width : 0;
    dst->is_able_to_free = 1;
    if (dst->data == NULL) {
        dst->data = (FLOAT_T*) MEM_ALLOC(sizeof(FLOAT_T)*dst->rows*dst->cols);
    }
    BASIC_ASSERT(dst->data != NULL);
}

void embcv_mat_buf_create_from_data(_Mat dst, int height, int width, FLOAT_T* data_ptr)
{
    dst->rows = height > 0 ? height : 0;
    dst->cols = width > 0 ? width : 0;
    
    if (data_ptr) {
        dst->is_able_to_free = 0;
        dst->data = data_ptr;
    }
    else {
        BASIC_ASSERT(0); // Flake: no need now
        dst->is_able_to_free = 1;
        dst->data = (FLOAT_T*) MEM_ALLOC(sizeof(FLOAT_T)*dst->rows*dst->cols);
        BASIC_ASSERT(dst->data != NULL);
    }
}

void embcv_mat_buf_release(_Mat src)
{
    src->rows = 0;
    src->cols = 0;
    if (src->is_able_to_free)
    {
        MEM_FREE(src->data);
        src->is_able_to_free = 0;
        src->data = NULL;
    }
}

void embcv_mat_transpose(_Mat src, _Mat dst)
{
    if (dst->cols*dst->rows != src->cols*src->rows) {
        BASIC_ASSERT(0);
        return;
    }
    
    if (dst->rows != src->cols)
        dst->rows = src->cols;
    
    if (dst->cols != src->rows)
        dst->cols = src->rows;
    
    size_t dst_step;
    for (int i=0; i < dst->rows; i++) {
        dst_step = i*dst->cols;
        for (int j=0; j < dst->cols; j++) {
            dst->data[dst_step + j] = src->data[j*src->cols + i];
        }
    }

    return;
}

/// @brief copy src data to dst, will change shape
void embcv_mat_copy_to(_Mat src, _Mat dst)
{
    if (dst->rows*dst->cols < src->rows*src->cols) {
        BASIC_PRINT("matrix copy fail, total size different.\n");
        return;
    }
    
    if (dst->rows != src->rows)
        dst->rows = src->rows;
    
    if (dst->cols != src->cols)
        dst->cols = src->cols;

    for (int i=0; i<src->rows; i++) {
        const FLOAT_T* src_ptr = (src->data + i*src->cols);
              FLOAT_T* dst_ptr = (dst->data + i*dst->cols);
        for (int j=0; j < src->cols; j++)
            dst_ptr[j] = src_ptr[j];
    }

    return;
}

void embcv_mat_clone_alloc(_Mat src, _Mat dst)
{
    embcv_mat_buf_create(dst, src->rows, src->cols);
    
    embcv_mat_copy_to(src, dst);
}

void embcv_mat_asymm_diag_fill(_Mat src, _Mat dst)
{
    //BASIC_ASSERT(dst->rows == src->rows);

    for (int i = 0; i < dst->rows; i++) {
        for (int j = 0; j < dst->cols; j++) {
            const int idx = i * dst->cols + j;
            if (i == j) {
                if (i < src->rows)
                    dst->data[idx] = src->data[i];
                else
                    dst->data[idx] = 0;
            } else {
                dst->data[idx] = 0;
            }
        }        
    }

    return;
}

/// @brief dst = A * B
void embcv_mat_mul(_Mat a, _Mat b, _Mat dst)
{
    if (a->cols != b->rows || dst->rows != a->rows || dst->cols != b->cols) {
        BASIC_ASSERT(0);
        return;
    }

    FLOAT_T temp;
    int m = dst->rows, nn = a->cols, n = dst->cols;
    int i, j, k;
    for (i=0; i < m; i++) {
        for (j=0; j < n; j++) {
            temp = 0.0;
            for (k=0; k < nn; k++) {
                temp += a->data[i*a->cols+k] * b->data[k*b->cols+j];
            }
            dst->data[i*n+j] = temp;
        }
    }
    
    return;
}