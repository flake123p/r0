#ifndef _EMB_CV_API_H_INCLUDED_
#define _EMB_CV_API_H_INCLUDED_

#include "sys.h" // type.h & ...
// make sure temp exists

typedef struct {
    int height;
    int width;
} _Size_t;

typedef struct {
    FLOAT_T* data;
    union {
        struct {
            int rows;
            int cols;
        };
        _Size_t size;
    };
    int is_able_to_free;
} _Mat_t;

typedef _Mat_t* _Mat;
typedef _Mat_t* _InputArray;
typedef _Mat_t* _OutputArray;
#define Mat_dclr(a) _Mat_t __##a; __##a.data=NULL; _Mat a = &__##a
#define Mat_t_dclr(inst) _Mat_t inst; inst.data=NULL

typedef struct {
    union {
        struct {
            FLOAT_T x;
            FLOAT_T y;
        } center;
        struct {
            FLOAT_T x;
            FLOAT_T y;
        };
    };
    union {
        struct {
            FLOAT_T width;
            FLOAT_T height;
        } size;
        struct {
            FLOAT_T width;
            FLOAT_T height;
        };
    };
    FLOAT_T angle;
} _RotatedRect_t;
typedef _RotatedRect_t* _RotatedRect;

typedef struct {
    int32_t x;
    int32_t y;
} _Point_I;

typedef struct {
    FLOAT_T x;
    FLOAT_T y;
} _Point_F;

// EmbCV API's, keep these at bottom
#include "embcv_matrix.h"

void _SVD_backSubst( _InputArray _w, _InputArray _u, _InputArray _vt,
                     _InputArray _rhs, _OutputArray _dst );
void _SVD_compute(_InputArray A, _InputArray w, _InputArray U, _InputArray Vt);
bool _solve( _InputArray _src, _InputArray _src2arg, _OutputArray _dst, int method);

int _fitEllipseNoDirect(_Point_I *points, int n, _RotatedRect out_box);
int fitEllipse_generic(_Point_I *points, int n, _RotatedRect out_box);

#endif//_EMB_CV_API_H_INCLUDED_