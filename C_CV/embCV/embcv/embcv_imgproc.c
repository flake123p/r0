#include <stdlib.h>
#include <stdint.h>
#include <float.h>
#include <math.h>
#include <string.h> //memset

#include "embcv_internal.h"
#include "embcv_api.h"

//static RotatedRect _fitEllipseNoDirect( InputArray _points )
int _fitEllipseNoDirect(_Point_I *points, int n, _RotatedRect out_box)
{
    // CV_INSTRUMENT_REGION();

    // Mat points = _points.getMat();
    // int i, n = points.checkVector(2);
    // int depth = points.depth();
    // CV_Assert( n >= 0 && (depth == CV_32F || depth == CV_32S));

    // RotatedRect box;

    // if( n < 5 )
    //     CV_Error( CV_StsBadSize, "There should be at least 5 points to fit the ellipse" );
    
    int i;
    BASIC_ASSERT(n >= 5);

    // New fitellipse algorithm, contributed by Dr. Daniel Weiss
    _Point_F c = {0., 0.};
    FLOAT_T gfp[5] = {0}, rp[5] = {0}, t, vd[25]={0}, wd[5]={0};
    const FLOAT_T min_eps = 1e-8;
    //bool is_float = depth == CV_32F;

    _AutoBuffer(FLOAT_T, _Ad, n*12+n);
    FLOAT_T *Ad = _Ad, *ud = Ad + n*5, *bd = ud + n*5;
    _Point_F* ptsf_copy = (_Point_F*)(bd + n);

    // first fit for parameters A - E
    Mat_dclr(A); Mat_dclr(b); Mat_dclr(x); Mat_dclr(u); Mat_dclr(vt); Mat_dclr(w);
    Mat_create_from_data( A, n, 5, CV_64F, Ad );
    Mat_create_from_data( b, n, 1, CV_64F, bd );
    Mat_create_from_data( x, 5, 1, CV_64F, gfp );
    Mat_create_from_data( u, n, 1, CV_64F, ud );
    Mat_create_from_data( vt, 5, 5, CV_64F, vd );
    Mat_create_from_data( w, 5, 1, CV_64F, wd );

    {
    for (int i=0; i < n; i++) {
        //_Point_F p = {(FLOAT_T)points[i].x, (FLOAT_T)points[i].y };
        _Point_F p;
        p.x = (FLOAT_T)points[i].x;
        p.y = (FLOAT_T)points[i].y;
        ptsf_copy[i] = p;
        c.x += p.x;
        c.y += p.y;
    }
    }
    c.x /= n;
    c.y /= n;

    FLOAT_T s = 0;
    for( i = 0; i < n; i++ )
    {
        _Point_F p = ptsf_copy[i];
        p.x -= c.x;
        p.y -= c.y;
        s = s + ABS(p.x) + ABS(p.y);
    }
    //printf("abmy, s = %f\n", s); //same
    FLOAT_T scale = 100.0/(s > FLT_EPSILON ? s : FLT_EPSILON);

    for( i = 0; i < n; i++ )
    {
        _Point_F p = ptsf_copy[i];
        p.x -= c.x;
        p.y -= c.y;
        FLOAT_T px = p.x*scale;
        FLOAT_T py = p.y*scale;

        bd[i] = 10000.0; // 1.0?
        Ad[i*5] = -px * px; // A - C signs inverted as proposed by APP
        Ad[i*5 + 1] = -py * py;
        Ad[i*5 + 2] = -px * py;
        Ad[i*5 + 3] = px;
        Ad[i*5 + 4] = py;
    }

    _SVD_compute(A, w, u, vt);

    if(wd[0]*FLT_EPSILON > wd[4]) {
        FLOAT_T eps = (FLOAT_T)(s/((FLOAT_T)n*2)*1e-3);
        for( i = 0; i < n; i++ )
        {
            _Point_F ofs;
            _Point_F p;
            embcv_get_ofs(i, eps, &ofs);
            p.x = ptsf_copy[i].x + ofs.x;
            p.x = ptsf_copy[i].y + ofs.y;
            ptsf_copy[i] = p;
        }

        for( i = 0; i < n; i++ )
        {
            _Point_F p = ptsf_copy[i];
            p.x -= c.x;
            p.y -= c.y;
            FLOAT_T px = p.x*scale;
            FLOAT_T py = p.y*scale;
            bd[i] = 10000.0; // 1.0?
            Ad[i*5] = -px * px; // A - C signs inverted as proposed by APP
            Ad[i*5 + 1] = -py * py;
            Ad[i*5 + 2] = -px * py;
            Ad[i*5 + 3] = px;
            Ad[i*5 + 4] = py;
        }
        _SVD_compute(A, w, u, vt);
    }
    //memset(gfp, 0, 5*sizeof(FLOAT_T));
    _SVD_backSubst(w, u, vt, b, x);
    //printf("my backsub, gfp = \n%.16f\n%.16f\n%.16f\n%.16f\n%.16f\n", gfp[0], gfp[1], gfp[2], gfp[3], gfp[4]);  //same

    // now use general-form parameters A - E to find the ellipse center:
    // differentiate general form wrt x/y to get two equations for cx and cy
    Mat_create_from_data( A, 2, 2, CV_64F, Ad );
    Mat_create_from_data( b, 2, 1, CV_64F, bd );
    Mat_create_from_data( x, 2, 1, CV_64F, rp );
    Ad[0] = 2 * gfp[0];
    Ad[1] = Ad[2] = gfp[2];
    Ad[3] = 2 * gfp[1];
    bd[0] = gfp[3];
    bd[1] = gfp[4];
    //memset(rp, 0, 2*sizeof(FLOAT_T));
    _solve( A, b, x, _DECOMP_SVD );
    //printf("abmy1, rp = \n%.16f\n%.16f\n", rp[0], rp[1]);

    // re-fit for parameters A - C with those center coordinates
    Mat_create_from_data( A, n, 3, CV_64F, Ad );
    Mat_create_from_data( b, n, 1, CV_64F, bd );
    Mat_create_from_data( x, 3, 1, CV_64F, gfp );
    for( i = 0; i < n; i++ )
    {
        _Point_F p = ptsf_copy[i];
        p.x -= c.x;
        p.y -= c.y;
        FLOAT_T px = p.x*scale;
        FLOAT_T py = p.y*scale;
        bd[i] = 1.0;
        Ad[i * 3] = (px - rp[0]) * (px - rp[0]);
        Ad[i * 3 + 1] = (py - rp[1]) * (py - rp[1]);
        Ad[i * 3 + 2] = (px - rp[0]) * (py - rp[1]);
        //printf("mycv2, Ad = \n%.16f\n%.16f\n%.16f\n", Ad[i * 3], Ad[i * 3 + 1], Ad[i * 3 + 2]);
    }
    //memset(gfp, 0, 3*sizeof(FLOAT_T));
    _solve(A, b, x, _DECOMP_SVD );
    //printf("abmy2, gfp = \n%.16f\n%.16f\n%.16f\n", gfp[0], gfp[1], gfp[2]);

    // store angle and radii
    rp[4] = -0.5 * atan2(gfp[2], gfp[1] - gfp[0]); // convert from APP angle usage
    if( ABS(gfp[2]) > min_eps )
        t = gfp[2]/sin(-2.0 * rp[4]);
    else // ellipse is rotated by an integer multiple of pi/2
        t = gfp[1] - gfp[0];
    rp[2] = ABS(gfp[0] + gfp[1] - t);
    if( rp[2] > min_eps )
        rp[2] = sqrt(2.0 / rp[2]);
    rp[3] = ABS(gfp[0] + gfp[1] + t);
    if( rp[3] > min_eps )
        rp[3] = sqrt(2.0 / rp[3]);

    out_box->center.x = (FLOAT_T)(rp[0]/scale) + c.x;
    out_box->center.y = (FLOAT_T)(rp[1]/scale) + c.y;
    out_box->size.width = (FLOAT_T)(rp[2]*2/scale);
    out_box->size.height = (FLOAT_T)(rp[3]*2/scale);
    if( out_box->size.width > out_box->size.height )
    {
        FLOAT_T temp;
        SWAP_BY_TEMP( out_box->size.width, out_box->size.height );
        out_box->angle = (FLOAT_T)(90 + rp[4]*180/_CV_PI);
    }
    if( out_box->angle < -180 )
        out_box->angle += 360;
    if( out_box->angle > 360 )
        out_box->angle -= 360;

    _AutoBufferFree(_Ad);
    //return box;
    return 0;
}
