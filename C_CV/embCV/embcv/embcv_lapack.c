#include <stdlib.h>
#include <stdint.h>
#include <float.h>

#include <math.h> // sqrt ...

#include "embcv_internal.h"
#include "embcv_api.h"


#define T1 FLOAT_T
#define T2 FLOAT_T
#define T3 FLOAT_T
static void
MatrAXPY( int m, int n, const T1* x, int dx,
         const T2* a, int inca, T3* y, int dy )
{
    int i;
    for( i = 0; i < m; i++, x += dx, y += dy )
    {
        T2 s = a[i*inca];
        int j = 0;
         #if CV_ENABLE_UNROLLED
        for(; j <= n - 4; j += 4 )
        {
            T3 t0 = (T3)(y[j]   + s*x[j]);
            T3 t1 = (T3)(y[j+1] + s*x[j+1]);
            y[j]   = t0;
            y[j+1] = t1;
            t0 = (T3)(y[j+2] + s*x[j+2]);
            t1 = (T3)(y[j+3] + s*x[j+3]);
            y[j+2] = t0;
            y[j+3] = t1;
        }
        #endif
        for( ; j < n; j++ )
            y[j] = (T3)(y[j] + s*x[j]);
    }
}
#undef T1
#undef T2
#undef T3

#define T FLOAT_T
static void SVBkSbImpl_( int m, int n, const T* w, int incw,
       const T* u, int ldu, bool uT,
       const T* v, int ldv, bool vT,
       const T* b, int ldb, int nb,
       T* x, int ldx, FLOAT_T* buffer, T eps )
{
    FLOAT_T threshold = 0;
    int udelta0 = uT ? ldu : 1, udelta1 = uT ? 1 : ldu;
    int vdelta0 = vT ? ldv : 1, vdelta1 = vT ? 1 : ldv;
    int i, j, nm = MIN(m, n);

    if( !b )
        nb = m;

    for( i = 0; i < n; i++ )
        for( j = 0; j < nb; j++ )
            x[i*ldx + j] = 0;

    for( i = 0; i < nm; i++ )
        threshold += w[i*incw];
    threshold *= eps;

    // v * inv(w) * uT * b
    for( i = 0; i < nm; i++, u += udelta0, v += vdelta0 )
    {
        FLOAT_T wi = w[i*incw];
        if( (FLOAT_T)ABS(wi) <= threshold )
            continue;
        wi = 1/wi;

        if( nb == 1 )
        {
            FLOAT_T s = 0;
            if( b )
                for( j = 0; j < m; j++ )
                    s += u[j*udelta1]*b[j*ldb];
            else
                s = u[0];
            s *= wi;

            for( j = 0; j < n; j++ )
                x[j*ldx] = (T)(x[j*ldx] + s*v[j*vdelta1]);
        }
        else
        {
            if( b )
            {
                for( j = 0; j < nb; j++ )
                    buffer[j] = 0;
                MatrAXPY( m, nb, b, ldb, u, udelta1, buffer, 0 );
                for( j = 0; j < nb; j++ )
                    buffer[j] *= wi;
            }
            else
            {
                for( j = 0; j < nb; j++ )
                    buffer[j] = u[j*udelta1]*wi;
            }
            MatrAXPY( n, nb, buffer, 0, v, vdelta1, x, ldx );
        }
    }
}
#undef T

static void
SVBkSb( int m, int n, const FLOAT_T* w, size_t wstep,
        const FLOAT_T* u, size_t ustep, bool uT,
        const FLOAT_T* v, size_t vstep, bool vT,
        const FLOAT_T* b, size_t bstep, int nb,
        FLOAT_T* x, size_t xstep, uchar* buffer )
{
    SVBkSbImpl_(m, n, w, wstep ? (int)(wstep/sizeof(w[0])) : 1,
                u, (int)(ustep/sizeof(u[0])), uT,
                v, (int)(vstep/sizeof(v[0])), vT,
                b, (int)(bstep/sizeof(b[0])), nb,
                x, (int)(xstep/sizeof(x[0])),
                (FLOAT_T*)Mat_alignPtr(buffer, sizeof(FLOAT_T)), (FLOAT_T)(DBL_EPSILON*2) );
}

void _SVD_backSubst( _InputArray _w, _InputArray _u, _InputArray _vt,
                     _InputArray _rhs, _OutputArray _dst )
{
    _Mat w = Mat_getMat(_w), u = Mat_getMat(_u), vt = Mat_getMat(_vt), rhs = Mat_getMat(_rhs);
    int type = Mat_type(w), esz = Mat_elemSize(w);
    int m = u->rows, n = vt->cols, nb = rhs->data ? rhs->cols : m, nm = MIN(m, n);
    size_t wstep = w->rows == 1 ? (size_t)esz : w->cols == 1 ? (size_t)Mat_step(w) : (size_t)Mat_step(w) + esz;
    _AutoBuffer(uchar, buffer, (nb*sizeof(double) + 16));
    CV_Assert( Mat_type(w) == Mat_type(u) && Mat_type(u) == Mat_type(vt) && u->data && vt->data && w->data );
    CV_Assert( u->cols >= nm && vt->rows >= nm &&
               (Mat_size_cmp(w, nm, 1) || Mat_size_cmp(w, 1, nm) || Mat_size_cmp(w, vt->rows, u->cols)) );
    CV_Assert( rhs->data == 0 || (Mat_type(rhs) == type && rhs->rows == m) );

    Mat_create( _dst, n, nb, type );
    _Mat dst = Mat_getMat(_dst);
    // if( type == CV_32F )
    //     SVBkSb(m, n, w.ptr<float>(), wstep, u.ptr<float>(), u.step, false,
    //            vt.ptr<float>(), vt.step, true, rhs.ptr<float>(), rhs.step, nb,
    //            dst.ptr<float>(), dst.step, buffer.data());
    // else if( type == CV_64F )
    //     SVBkSb(m, n, w.ptr<double>(), wstep, u.ptr<double>(), u.step, false,
    //            vt.ptr<double>(), vt.step, true, rhs.ptr<double>(), rhs.step, nb,
    //            dst.ptr<double>(), dst.step, buffer.data());
    // else
    //     CV_Error( CV_StsUnsupportedFormat, "" );
    SVBkSb(m, n, w->data, wstep, u->data, Mat_step(u), false,
            vt->data, Mat_step(vt), true, rhs->data, Mat_step(rhs), nb,
            dst->data, Mat_step(dst), buffer);

    _AutoBufferFree(buffer);
}

static void _JacobiSVDImpl(FLOAT_T *At, size_t astep, FLOAT_T *sigma, FLOAT_T *Vt, size_t vstep, int m, int n, int n1, FLOAT_T minval, FLOAT_T eps)
{
    int i, j, k, iter, max_iter = MAX(m, 30);
    FLOAT_T *wBuf = (FLOAT_T*) MEM_ALLOC(sizeof(FLOAT_T) * n);
    BASIC_ASSERT(wBuf != NULL);
    FLOAT_T c, s, sd, temp;
    astep /= sizeof(At[0]);
    vstep /= sizeof(Vt[0]);

    for(i=0; i < n; i++)
    {
        for (k=0, sd=0; k < m; k++)
        {
            FLOAT_T temp = At[i*astep + k];
            sd += (FLOAT_T) temp * temp;
        }
        wBuf[i] = sd;

        if (Vt)
        {
            for (k=0; k < n; k++)
                Vt[i*vstep + k] = 0;
            Vt[i*vstep + i] = 1;
        }
    }

    for (iter=0; iter < max_iter; iter++)
    {
        int changed = 0;
        for (i=0; i < n-1; i++)
        {
            for (j=i+1; j < n; j++)
            {
                FLOAT_T *Ai = At + i*astep, *Aj = At + j*astep;
                FLOAT_T a = wBuf[i], b = wBuf[j], p=0;

                for (k=0; k < m; k++)
                    p += Ai[k] * Aj[k];
                
                if (ABS(p) <= eps*sqrt(a*b))
                    continue;
                
                p *= 2;
                FLOAT_T beta = a-b, gamma = embcv_hypot(p, beta);
                if (beta < 0)
                {
                    FLOAT_T delta = (gamma - beta) * 0.5;
                    s = (FLOAT_T)sqrt(delta/gamma);
                    c = (FLOAT_T)(p / (gamma * s * 2));
                }
                else
                {
                    c = (FLOAT_T)sqrt((gamma + beta)/(gamma * 2));
                    s = (FLOAT_T)(p / (gamma * c * 2));
                }

                a = b = 0;
                for (k=0; k < m; k++)
                {
                    FLOAT_T t0 =  c*Ai[k] + s*Aj[k];
                    FLOAT_T t1 = -s*Ai[k] + c*Aj[k];
                    Ai[k] = t0; Aj[k] = t1;

                    a += (double)t0*t0;
                    b += (double)t1*t1;
                }
                wBuf[i] = a; wBuf[j] = b;
                changed = 1;

                if (Vt)
                {
                    FLOAT_T *Vi = Vt + i*vstep, *Vj = Vt + j*vstep;
                    k = embcv_givens(Vi, Vj, n, c, s);
                    for (; k < n; k++)
                    {
                        FLOAT_T t0 =  c*Vi[k] + s*Vj[k];
                        FLOAT_T t1 = -s*Vi[k] + c*Vj[k];
                        Vi[k] = t0; Vj[k] = t1;
                    }
                }

            }
        }
        if (!changed)
            break;
    }

    for (i=0; i < n; i++)
    {
        for (k=0, sd=0; k < m; k++)
        {
            FLOAT_T temp = At[i*astep + k];
            sd += temp * temp;
        }
        wBuf[i] = (FLOAT_T)sqrt(sd);
    }

    for (i=0; i < n-1; i++)
    {
        j = i;
        for (k=i+1; k < n; k++)
        {
            if (wBuf[j] < wBuf[k]) j = k;
        }
        if (i != j)
        {
            SWAP_BY_TEMP(wBuf[i], wBuf[j]);
            if (Vt)
            {
                for (k=0; k < m; k++) {
                    SWAP_BY_TEMP(At[i*astep + k], At[j*astep + k]);
                }

                for (k=0; k < n; k++) {
                    SWAP_BY_TEMP(Vt[i*vstep + k], Vt[j*vstep + k]);
                }
            }
        }
    }

    for (i=0; i < n; i++)
    {
        sigma[i] = wBuf[i];
    }
    if (!Vt)
        return;
    
    uint64_t rng = embcv_rng_create(0x12345678);
    for (i=0; i < n1; i++)
    {
        sd = (i < n) ? wBuf[i] : 0;

        for (int ii=0; ii < 100 && sd <= minval; ii++)
        {
            // if we got a zero singular value, then in order to get the corresponding left singular vector
            // we generate a random vector, project it to the previously computed left singular vectors,
            // subtract the projection and normalize the difference.
            const FLOAT_T val0 = (FLOAT_T) (1.0 / m);
            for (k=0; k < m; k++)
            {
                FLOAT_T val = ((embcv_rng_next(&rng) & 256) != 0) ? val0 : -val0;
                At[i*astep + k] = val;
            }

            for (iter=0; iter < 2; iter++)
            {
                for (j=0; j < i; j++)
                {
                    sd =0;
                    for (k=0; k < m; k++)
                        sd += At[i*astep + k] * At[j*astep + k];
                    FLOAT_T asum = 0.0;
                    for (k=0; k < m; k++)
                    {
                        FLOAT_T temp = At[i*astep + k] - sd*At[j*astep + k];
                        At[i*astep + k] = temp;
                        asum += ABS(temp);
                    }
                    asum = asum > eps*100 ? 1/asum : 0;
                    for (k=0; k < m; k++) 
                        At[i*astep + k] *= asum;
                }
            }
            sd = 0;
            for (k=0; k < m; k++)
            {
                FLOAT_T temp = At[i*astep + k];
                sd += (temp * temp);
            }
            sd = (FLOAT_T)sqrt(sd);
        }

        s = (FLOAT_T)(sd > minval ? 1/sd : 0.0);
        for (k=0; k < m; k++)
            At[i*astep + k] *= s;
    }
    
    MEM_FREE(wBuf);

    return;
}

static void _JacobiSVD(FLOAT_T* At, size_t astep, FLOAT_T* W, FLOAT_T* Vt, size_t vstep, int m, int n, int n1)
{
    // default n1 = -1;
    if (sizeof(FLOAT_T) == 4) {
        _JacobiSVDImpl( At, astep, W, Vt, vstep, m, n, !Vt ? 0 : n1 < 0 ? n : n1, FLT_MIN, FLT_EPSILON*2);
    } else if (sizeof(FLOAT_T) == 8) {
        _JacobiSVDImpl( At, astep, W, Vt, vstep, m, n, !Vt ? 0 : n1 < 0 ? n : n1, DBL_MIN, DBL_EPSILON*10);
    }
}

// from OpenCV _SVDcompute()
void _SVD_compute(_InputArray A, _InputArray w, _InputArray U, _InputArray Vt)
{
    /// @brief A = u * w * vT
    // flags == 4, SVD_FULL_UV
    // flags == 2, SVD_NO_UV
    // flags == 1, SVD_MODIFY_A
    int m = A->rows, n = A->cols, urows;
    int compute_uv = (U != NULL || Vt != NULL) ? 1:0;

    int transpose_A = 0;
    if( m < n )
    {
        SWAP_BY_TYPE(int, m, n);
        transpose_A = 1;
    }

    urows = n; // full_uv ? m : n; because flags always 0, so urow always = n
    size_t esz = sizeof(FLOAT_T), astep = m * esz, vstep = n * esz;
    uint8_t *buffer = (uint8_t*) MEM_ALLOC(urows*astep + n*esz + n*vstep);
    BASIC_ASSERT(buffer != NULL);
    
    Mat_t_dclr(temp_a); Mat_t_dclr(temp_u); Mat_t_dclr(temp_w); Mat_t_dclr(temp_vt);
    embcv_mat_buf_create_from_data(&temp_a, n, m, (FLOAT_T*)buffer);
    embcv_mat_buf_create_from_data(&temp_u, n, m, (FLOAT_T*)buffer);
    embcv_mat_buf_create_from_data(&temp_w, n, 1, (FLOAT_T*)(buffer+n*astep));

    // printf("ab --   %d(0x%x)\n", urows*astep + n*esz + n*vstep, urows*astep + n*esz + n*vstep);
    // printf("ab 00 u %p\n", (FLOAT_T*)buffer);
    // printf("ab 11   %p\n", (FLOAT_T*)buffer+n*astep);
    // printf("ab 22 w %p\n", (FLOAT_T*)(buffer+n*astep));
    
    if (compute_uv) {
        embcv_mat_buf_create_from_data(&temp_vt, n, n, (FLOAT_T*)(buffer+n*astep+n*esz));
    }

    // printf("ab 33 v %p\n", temp_vt.data);
    // printf("ab 33 v %p\n", (FLOAT_T*)(buffer+n*astep+n*esz));

    if (!transpose_A) {
        embcv_mat_transpose(A, &temp_a);
    } else {
        embcv_mat_copy_to(A, &temp_a);
    }

    // PRLOC
    // print_matrixF(temp_a);

    // std::cout << 
    //     " u step : " << temp_a.cols*(sizeof(FLOAT_T)) <<
    //     " v step : " << temp_vt.cols*(sizeof(FLOAT_T)) <<
    //     " m : " << m <<
    //     " n : " << n << 
    //     " com_uv : " << compute_uv << 
    //     " urows : " << n <<
    //      std::endl;

    _JacobiSVD((FLOAT_T*)temp_a.data, temp_a.cols*(sizeof(FLOAT_T)), (FLOAT_T*)temp_w.data, (FLOAT_T*)temp_vt.data, temp_vt.cols*(sizeof(FLOAT_T)), m, n, compute_uv ? n : 0);

    // printf(" U U U U U U U U U U\n");
    // print_matrixF(temp_u);
    // printf(" W W W W W W W W W W\n");
    // print_matrixF(temp_w);
    // printf(" V V V V V V V V V V\n");
    // print_matrixF(temp_vt);

    embcv_mat_buf_create(w, temp_w.rows, temp_w.cols);
    embcv_mat_copy_to(&temp_w, w);

    if (compute_uv)
    {
        if (!transpose_A)
        {
            if (U != NULL)
            {
                embcv_mat_buf_create(U, temp_u.cols, temp_u.rows);
                embcv_mat_transpose(&temp_u, U);
            }
            if (Vt != NULL)
            {
                embcv_mat_buf_create(Vt, temp_vt.rows, temp_vt.cols);
                embcv_mat_copy_to(&temp_vt, Vt);
            }
        }
        else
        {
            if (U != NULL)
            {
                embcv_mat_buf_create(U, temp_vt.cols, temp_vt.rows);
                embcv_mat_transpose(&temp_vt, U);
            }
            if (Vt != NULL)
            {
                embcv_mat_buf_create(Vt, temp_u.rows, temp_u.cols);
                embcv_mat_copy_to(&temp_u, Vt);
            }
        }
    }
    // release
    MEM_FREE(buffer);
}

bool _solve( _InputArray _src, _InputArray _src2arg, _OutputArray _dst, int method )
{
    //CV_INSTRUMENT_REGION();

    bool result = true;
    _Mat src = Mat_getMat(_src), _src2 = Mat_getMat(_src2arg);
    int type = Mat_type(src);
    bool is_normal = (method & _DECOMP_NORMAL) != 0;

    CV_Assert( type == Mat_type(_src2) && (type == CV_32F || type == CV_64F) );

    method &= ~_DECOMP_NORMAL;
    // CV_Check(method, method == _DECOMP_LU || method == _DECOMP_SVD || method == _DECOMP_EIG ||
    //                  method == _DECOMP_CHOLESKY || method == _DECOMP_QR,
    //          "Unsupported method, see #DecompTypes");
    CV_Assert( (method != _DECOMP_LU && method != _DECOMP_CHOLESKY) ||
        is_normal || src->rows == src->cols );

    // check case of a single equation and small matrix
    if( (method == _DECOMP_LU || method == _DECOMP_CHOLESKY) && !is_normal &&
        src->rows <= 3 && src->rows == src->cols && _src2->cols == 1 )
    {
        BASIC_ASSERT(0);
#if 0
        _dst.create( src.cols, _src2.cols, src.type() );
        Mat dst = _dst.getMat();

        #define bf(y) ((float*)(bdata + y*src2step))[0]
        #define bd(y) ((double*)(bdata + y*src2step))[0]

        const uchar* srcdata = src.ptr();
        const uchar* bdata = _src2.ptr();
        uchar* dstdata = dst.ptr();
        size_t srcstep = src.step;
        size_t src2step = _src2.step;
        size_t dststep = dst.step;

        if( src.rows == 2 )
        {
            if( type == CV_32FC1 )
            {
                double d = det2(Sf);
                if( d != 0. )
                {
                    double t;
                    d = 1./d;
                    t = (float)(((double)bf(0)*Sf(1,1) - (double)bf(1)*Sf(0,1))*d);
                    Df(1,0) = (float)(((double)bf(1)*Sf(0,0) - (double)bf(0)*Sf(1,0))*d);
                    Df(0,0) = (float)t;
                }
                else
                    result = false;
            }
            else
            {
                double d = det2(Sd);
                if( d != 0. )
                {
                    double t;
                    d = 1./d;
                    t = (bd(0)*Sd(1,1) - bd(1)*Sd(0,1))*d;
                    Dd(1,0) = (bd(1)*Sd(0,0) - bd(0)*Sd(1,0))*d;
                    Dd(0,0) = t;
                }
                else
                    result = false;
            }
        }
        else if( src.rows == 3 )
        {
            if( type == CV_32FC1 )
            {
                double d = det3(Sf);
                if( d != 0. )
                {
                    float t[3];
                    d = 1./d;

                    t[0] = (float)(d*
                           (bf(0)*((double)Sf(1,1)*Sf(2,2) - (double)Sf(1,2)*Sf(2,1)) -
                            Sf(0,1)*((double)bf(1)*Sf(2,2) - (double)Sf(1,2)*bf(2)) +
                            Sf(0,2)*((double)bf(1)*Sf(2,1) - (double)Sf(1,1)*bf(2))));

                    t[1] = (float)(d*
                           (Sf(0,0)*(double)(bf(1)*Sf(2,2) - (double)Sf(1,2)*bf(2)) -
                            bf(0)*((double)Sf(1,0)*Sf(2,2) - (double)Sf(1,2)*Sf(2,0)) +
                            Sf(0,2)*((double)Sf(1,0)*bf(2) - (double)bf(1)*Sf(2,0))));

                    t[2] = (float)(d*
                           (Sf(0,0)*((double)Sf(1,1)*bf(2) - (double)bf(1)*Sf(2,1)) -
                            Sf(0,1)*((double)Sf(1,0)*bf(2) - (double)bf(1)*Sf(2,0)) +
                            bf(0)*((double)Sf(1,0)*Sf(2,1) - (double)Sf(1,1)*Sf(2,0))));

                    Df(0,0) = t[0];
                    Df(1,0) = t[1];
                    Df(2,0) = t[2];
                }
                else
                    result = false;
            }
            else
            {
                double d = det3(Sd);
                if( d != 0. )
                {
                    double t[9];

                    d = 1./d;

                    t[0] = ((Sd(1,1) * Sd(2,2) - Sd(1,2) * Sd(2,1))*bd(0) +
                            (Sd(0,2) * Sd(2,1) - Sd(0,1) * Sd(2,2))*bd(1) +
                            (Sd(0,1) * Sd(1,2) - Sd(0,2) * Sd(1,1))*bd(2))*d;

                    t[1] = ((Sd(1,2) * Sd(2,0) - Sd(1,0) * Sd(2,2))*bd(0) +
                            (Sd(0,0) * Sd(2,2) - Sd(0,2) * Sd(2,0))*bd(1) +
                            (Sd(0,2) * Sd(1,0) - Sd(0,0) * Sd(1,2))*bd(2))*d;

                    t[2] = ((Sd(1,0) * Sd(2,1) - Sd(1,1) * Sd(2,0))*bd(0) +
                            (Sd(0,1) * Sd(2,0) - Sd(0,0) * Sd(2,1))*bd(1) +
                            (Sd(0,0) * Sd(1,1) - Sd(0,1) * Sd(1,0))*bd(2))*d;

                    Dd(0,0) = t[0];
                    Dd(1,0) = t[1];
                    Dd(2,0) = t[2];
                }
                else
                    result = false;
            }
        }
        else
        {
            CV_Assert( src.rows == 1 );

            if( type == CV_32FC1 )
            {
                double d = Sf(0,0);
                if( d != 0. )
                    Df(0,0) = (float)(bf(0)/d);
                else
                    result = false;
            }
            else
            {
                double d = Sd(0,0);
                if( d != 0. )
                    Dd(0,0) = (bd(0)/d);
                else
                    result = false;
            }
        }
        return result;
#endif
    }

    int m = src->rows, m_ = m, n = src->cols, nb = _src2->cols;
    //size_t esz = CV_ELEM_SIZE(type), bufsize = 0;
    size_t esz = sizeof(FLOAT_T), bufsize = 0;
    // size_t vstep1 = Mat_alignSize(n*esz, 16);
    // size_t astep1 = method == _DECOMP_SVD && !is_normal ? Mat_alignSize(m*esz, 16) : vstep1;
    size_t vstep = n*esz;
    size_t astep = m*esz;
    // printf("v1 = %u\n", vstep1);
    // printf("v2 = %u\n", vstep);
    // printf("a1 = %u\n", astep1);
    // printf("a2 = %u\n", astep);
    //AutoBuffer<uchar> buffer;

    Mat_dclr(src2);
    embcv_mat_clone_alloc(_src2, src2);

    Mat_create(_dst, src->cols, src2->cols, Mat_type(src) );
    _Mat dst = Mat_getMat(_dst);

    if( m < n ) {
        BASIC_ASSERT(0);
        CV_Error(CV_StsBadArg, "The function can not solve under-determined linear systems" );
    }

    if( m == n )
        is_normal = false;
    else if( is_normal )
    {
        m_ = n;
        if( method == _DECOMP_SVD )
            method = _DECOMP_EIG;
    }

    size_t asize = astep*(method == _DECOMP_SVD || is_normal ? n : m);
    bufsize += asize + 32;

    if( is_normal )
        bufsize += n*nb*esz;
    if( method == _DECOMP_SVD || method == _DECOMP_EIG )
        bufsize += n*5*esz + n*vstep + nb*sizeof(double) + 32;

    //buffer.allocate(bufsize);
    _AutoBuffer(uchar, buffer, bufsize);
    uchar* ptr = (uchar *)Mat_alignPtr(buffer, 16);

    //Mat a(m_, n, type, ptr, astep);
    Mat_dclr(a);
    //Mat_create_from_data(a, m_, n, type, (FLOAT_T *)ptr);

    if( is_normal ) {
        BASIC_ASSERT(0); // not implement
        // mulTransposed(src, a, true);
    } else if( method != _DECOMP_SVD ) {
        BASIC_ASSERT(0); // not implement
        // src.copyTo(a);
    } else {
        //a = Mat(n, m_, type, ptr, astep);
        Mat_create_from_data(a, n, m_, type, (FLOAT_T *)ptr);
        embcv_mat_transpose(src, a);
    }
    ptr += asize;

    if( !is_normal )
    {
        if( method == _DECOMP_LU || method == _DECOMP_CHOLESKY ) {
            BASIC_ASSERT(0); // not implement
            //src2.copyTo(dst);
        }
    }
    else
    {
        BASIC_ASSERT(0); // not implement
#if 0
        // a'*b
        if( method == DECOMP_LU || method == DECOMP_CHOLESKY )
            gemm( src, src2, 1, Mat(), 0, dst, GEMM_1_T );
        else
        {
            Mat tmp(n, nb, type, ptr);
            ptr += n*nb*esz;
            gemm( src, src2, 1, Mat(), 0, tmp, GEMM_1_T );
            src2 = tmp;
        }
#endif
    }

    if( method == _DECOMP_LU )
    {
        BASIC_ASSERT(0); // not implement
#if 0
        if( type == CV_32F )
            result = hal::LU32f(a.ptr<float>(), a.step, n, dst.ptr<float>(), dst.step, nb) != 0;
        else
            result = hal::LU64f(a.ptr<double>(), a.step, n, dst.ptr<double>(), dst.step, nb) != 0;
#endif
    }
    else if( method == _DECOMP_CHOLESKY )
    {
        BASIC_ASSERT(0); // not implement
#if 0
        if( type == CV_32F )
            result = hal::Cholesky32f(a.ptr<float>(), a.step, n, dst.ptr<float>(), dst.step, nb);
        else
            result = hal::Cholesky64f(a.ptr<double>(), a.step, n, dst.ptr<double>(), dst.step, nb);
#endif
    }
    else if( method == _DECOMP_QR )
    {
        BASIC_ASSERT(0); // not implement
#if 0
        Mat rhsMat;
        if( is_normal || m == n )
        {
            src2.copyTo(dst);
            rhsMat = dst;
        }
        else
        {
            rhsMat = Mat(m, nb, type);
            src2.copyTo(rhsMat);
        }

        if( type == CV_32F )
            result = hal::QR32f(a.ptr<float>(), a.step, a.rows, a.cols, rhsMat.cols, rhsMat.ptr<float>(), rhsMat.step, NULL) != 0;
        else
            result = hal::QR64f(a.ptr<double>(), a.step, a.rows, a.cols, rhsMat.cols, rhsMat.ptr<double>(), rhsMat.step, NULL) != 0;

        if (rhsMat.rows != dst.rows)
            rhsMat.rowRange(0, dst.rows).copyTo(dst);
#endif
    }
    else
    {
        ptr = (uchar *)Mat_alignPtr(ptr, 16);
        Mat_dclr(v); Mat_dclr(w); Mat_dclr(u);
        Mat_create_from_data(v, n, n, type, (FLOAT_T *)ptr);
        Mat_create_from_data(w, n, 1, type, (FLOAT_T *)(ptr + vstep*n));
        //Mat v(n, n, type, ptr, vstep), w(n, 1, type, ptr + vstep*n), u;
        ptr += n*(vstep + esz);

        if( method == _DECOMP_EIG )
        {
            BASIC_ASSERT(0); // not implement
#if 0
            if( type == CV_32F )
                Jacobi(a.ptr<float>(), a.step, w.ptr<float>(), v.ptr<float>(), v.step, n, ptr);
            else
                Jacobi(a.ptr<double>(), a.step, w.ptr<double>(), v.ptr<double>(), v.step, n, ptr);
            u = v;
#endif
        }
        else
        {
            if( type == CV_32F ) {
                BASIC_ASSERT(0); // not implement
                //JacobiSVD(a.ptr<float>(), a.step, w.ptr<float>(), v.ptr<float>(), v.step, m_, n);
            } else {
                //JacobiSVD(a.ptr<double>(), a.step, w.ptr<double>(), v.ptr<double>(), v.step, m_, n);
                _JacobiSVD(a->data, astep, w->data, v->data, vstep, m_, n, -1);
            }
            u = a;
        }

        if( type == CV_32F )
        {
            BASIC_ASSERT(0); // not implement
#if 0
            SVBkSb(m_, n, w.ptr<float>(), 0, u.ptr<float>(), u.step, true,
                   v.ptr<float>(), v.step, true, src2.ptr<float>(),
                   src2.step, nb, dst.ptr<float>(), dst.step, ptr);
#endif
        }
        else
        {
            SVBkSb(m_, n, w->data, 0, u->data, sizeof(FLOAT_T)*u->cols, true,
                   v->data, sizeof(FLOAT_T)*v->cols, true, src2->data,
                   sizeof(FLOAT_T)*src2->cols, nb, dst->data, sizeof(FLOAT_T)*dst->cols, ptr);
        }
        result = true;
    }

    if( !result ) {
        BASIC_ASSERT(0); // not implement
        //dst = Scalar(0);
    }
    embcv_mat_buf_release(src2);
    _AutoBufferFree(buffer);
    return result;
}