#include <stdlib.h>
#include <stdint.h>
#include <float.h>

#include <vector>
#include <opencv2/opencv.hpp>

#include "embcv_api.h"


int fitEllipse_generic(_Point_I *points, int n, _RotatedRect out_box)
{
#ifdef _USE_MY_FIT_ELLIPSE_
    // if (n == 5)
    //     return fitEllipse_Direct(points, n);
    // else
        return _fitEllipseNoDirect(points, n, out_box);
#else
    BASIC_ASSERT(0);
    std::vector<cv::Point> pts;
    for (int i=0; i < n; i++) {
        pts.push_back(cv::Point(points[i].x, points[i].y));
    }
    cv::RotatedRect box = cv::fitEllipse( pts );
    out_box->x = box.center.x;
    out_box->y = box.center.y;
    out_box->width = box.size.width;
    out_box->height = box.size.height;
    out_box->angle = box.angle;
#endif
    return 0;
}