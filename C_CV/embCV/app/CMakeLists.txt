cmake_minimum_required(VERSION 3.6)
set (CMAKE_CXX_STANDARD 14)

project(main)

add_compile_options ( -D_USE_MY_FIT_ELLIPSE_ )

#file(GLOB_RECURSE CFILES "${CMAKE_SOURCE_DIR}/*.c") SET_SOURCE_FILES_PROPERTIES(${CFILES} PROPERTIES LANGUAGE CXX )
file(GLOB main_SRC CONFIGURE_DEPENDS "*.c" "*.cpp" "*.cxx" "*.cc")
SET_SOURCE_FILES_PROPERTIES(${main_SRC} PROPERTIES LANGUAGE CXX )
add_executable(a ${main_SRC})

set(CMAKE_CXX_FLAGS "-O0 -Wall -Wextra")


# add the binary tree to the search path for include files
# so that we will find TutorialConfig.h
target_include_directories(
    a PUBLIC
    "${PROJECT_BINARY_DIR}"
    "${PROJECT_SOURCE_DIR}/../test_embcv"
)

add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../test_embcv ${CMAKE_CURRENT_BINARY_DIR}/test_embcv)
target_link_libraries(a PUBLIC test_embcv m)