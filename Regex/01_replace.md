# Reserve a, b, c, 0, 1, 2, -
https://stackoverflow.com/questions/13726429/regex-string-replace

**sed**
echo abcdefg-0123456 | sed -e "s/[^a-c0-2-]//g"
-> abc-012

**python**
import re
result = re.sub(r"[^a-c0-2-]", "", string)


# Replace a start a to null

**sed**
echo abca | sed -e "s/^a//"
-> bca

echo babca | sed -e "s/^a//"
-> babca

echo babca | sed -e "s/a//"
-> bbca

echo babca | sed -e "s/a//g"
-> bbc

**python**
import re
result = re.sub(r"a", "", "babca", count=1)

