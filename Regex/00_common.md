
# Matching two words in the same line (Note: this is in order) 
https://stackoverflow.com/questions/5607774/ack-regex-matching-two-words-in-order-in-the-same-line

Keyword:
    .*

e.g.:
    aaa.*bbb
    cpu.*count
    linux.*LD_LIBRARY_PATH.*path
    grep --include=\*.{py,md,c,cpp,h,hpp} -Irnw -ie "linux.*path"


==================================================================================
# Or 
Keyword:
    |

e.g.:
    cpu|count
    cpu.*(list|count)


==================================================================================
# Whole Word 
https://stackoverflow.com/questions/1751301/regex-match-entire-words-only

Keyword:
    \bXXX\b

e.g.:
    \bcpu\b.*\bcount\b


==================================================================================
# Set/Combination
Keyword:
    []

cpu or gpu:
    [cg]pu