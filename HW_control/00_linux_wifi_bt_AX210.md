# No bluetooth (Still not working)

##　AX210 Bluetooth adapter not shwowing up under Linux
https://community.intel.com/t5/Wireless/AX210-Bluetooth-adapter-not-shwowing-up-under-Linux/td-p/1519930

https://www.reddit.com/r/archlinux/comments/zweejv/how_to_fix_error_bluetooth_service_was_skipped/?rdt=50981
->
    Try these:

        Run sudo systemctl enable --now bluetooth.service

        Disable fastboot in the UEFI Settings.

        Switch to LTS kernel


##　Missing CAP_NET_ADMIN permission.
https://forum.qt.io/topic/114503/missing-cap_net_admin-permission/2

1:
> sudo vim /etc/dbus-1/system.d/bluetooth.conf

2: 
Paste these & replace your name
  <policy user="yourUserName">
    <allow own="org.bluez"/>
    <allow send_destination="org.bluez"/>
    <allow send_interface="org.bluez.Agent1"/>
    <allow send_interface="org.bluez.MediaEndpoint1"/>
    <allow send_interface="org.bluez.MediaPlayer1"/>
    <allow send_interface="org.bluez.Profile1"/>
    <allow send_interface="org.bluez.GattCharacteristic1"/>
    <allow send_interface="org.bluez.GattDescriptor1"/>
    <allow send_interface="org.bluez.LEAdvertisement1"/>
    <allow send_interface="org.freedesktop.DBus.ObjectManager"/>
    <allow send_interface="org.freedesktop.DBus.Properties"/>
  </policy>


3:
reboot