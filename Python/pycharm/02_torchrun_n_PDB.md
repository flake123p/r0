# 0: Make sure default interpreter in project setting is chosen correctly!!
Then proceed following steps.
[20240521]

# 1: Add this to "script" in "Run/Debug Configurations":

    <some_path>/torchrun

# 2: Add this to "Script parameters":

    --nproc_per_node=1 <your_python_file> ...

# 3: Reference:

[PYCHARM Run Configuration:]

Script:
/local10T/flake/envs/buda/bin/torchrun

Script Parameters:
--nproc_per_node 1 example_chat_fixed.py --ckpt_dir /local10T/flake/llama2/llama-2-7b-chat/ --tokenizer_path /local10T/flake/llama2/tokenizer.model --max_seq_len 512 --max_batch_size 6

Environment Variables (For Hijack Configurations):
PYTHONUNBUFFERED=1;LD_PRELOAD=../lib/buda/lib/libcuda.so.1:../lib/buda/lib/libcudart.so.11.0:../lib/buda/lib/libcublas.so.11:../lib/buda/lib/libcublasLt.so.11::../lib/buda/lib/libbudartpp.so;LD_LIBRARY_PATH=../lib/buda/lib:../lib/cmd/lib