#
# Add this to Script Parameters:
#
#     -a=7
#
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("-a", help="arg a")
parser.add_argument("-b", help="arg b")
#
# Flag without argument: https://stackoverflow.com/questions/5262702/argparse-module-how-to-add-option-without-any-argument
#
#   use action='store_true', 'store_false' or 'store_const'
#
parser.add_argument("-c", help="arg c", action='store_true')

args = parser.parse_args()

print("arg a:", args.a)
print("arg b:", args.b)
print("arg c:", args.c)