
import logging # default: stderr

# Configure logging to write to a file
logging.basicConfig(filename='app.log', level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# Log some messages
logging.info("This is an info message")
logging.warning("This is a warning message")
logging.error("This is an error message")