'''
https://stackoverflow.com/questions/2266646/how-to-disable-logging-on-the-standard-error-stream
''' 

import logging # default: stderr

logger: logging.Logger = logging.getLogger(name='dev')

logger.disabled = True

logger.debug('debug message')
logger.info('info message')
logger.warning('warning message')
logger.error('error message')
logger.critical('critical message')
# logger.exception('exception message')
# logger.fatal('fatal message')
