
import logging # default: stderr

# Create logger
logger = logging.getLogger("my_logger")
logger.setLevel(logging.DEBUG)

# Create a file handler
file_handler = logging.FileHandler("app.log")
file_handler.setLevel(logging.INFO)  # Only logs INFO and above

# Create a formatter and set it for the handler
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
file_handler.setFormatter(formatter)

# Add the file handler to the logger
logger.addHandler(file_handler)

# Log messages
logger.debug("This will not be logged (DEBUG < INFO)")
logger.info("This is an info message")
logger.warning("This is a warning message")
logger.error("This is an error message")