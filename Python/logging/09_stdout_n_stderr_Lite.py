import logging
import sys

logout = logging.getLogger("logout")
logerr = logging.getLogger("logerr")

stdout_handler = logging.StreamHandler(sys.stdout)
stderr_handler = logging.StreamHandler(sys.stderr)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
stdout_handler.setFormatter(formatter)
stderr_handler.setFormatter(formatter)

logout.addHandler(stdout_handler)
logerr.addHandler(stderr_handler)

logout.critical("This is a critical message AAA")
logerr.critical("This is a critical message BBB")