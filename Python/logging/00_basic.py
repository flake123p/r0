'''
Tutorial:

Doc:
https://docs.python.org/3/library/logging.html
''' 

import logging # default: stderr

print('========= default: =========')
logging.debug('debug message') # not shown in default
logging.info('info message') # not shown in default
logging.warning('warning message')
logging.error('error message')
logging.critical('critical message')

logging.disable(logging.WARNING)
print('========= disable WARNING =========')
logging.debug('debug message') # not shown
logging.info('info message') # not shown
logging.warning('warning message') # not shown
logging.error('error message')
logging.critical('critical message')

logging.disable(logging.NOTSET) 
print('========= enable WARNING =========')
logging.debug('debug message')
logging.info('info message')
logging.warning('warning message')
logging.error('error message')
logging.critical('critical message')

print('========= dev_logger + handler =========')
dev_logger: logging.Logger = logging.getLogger(name='dev')
#dev_logger.setLevel(logging.DEBUG)

handler: logging.StreamHandler = logging.StreamHandler() # StreamHandler() = stderr
dev_logger.addHandler(handler)

dev_logger.debug('debug message')
dev_logger.info('info message')
dev_logger.warning('warning message')
dev_logger.error('error message')
dev_logger.critical('critical message')

dev_logger.removeHandler(handler)

print('========= dev_logger + setLevel =========')
dev_logger.setLevel(logging.INFO)
dev_logger.debug('debug message')  # not shown
dev_logger.info('info message')
dev_logger.warning('warning message')
dev_logger.error('error message')
dev_logger.critical('critical message')

print('========= dev_logger + formatter =========')
import sys
formatter: logging.Formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler: logging.StreamHandler = logging.StreamHandler(sys.stderr) 
handler.setFormatter(formatter)

dev_logger = logging.getLogger('dev')
dev_logger.addHandler(handler)
dev_logger.debug('debug message')
dev_logger.info('info message')
dev_logger.warning('warning message')
dev_logger.error('error message')
dev_logger.critical('critical message')

print('========= root.handlers[0] =========')
formatter: logging.Formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
root = logging.getLogger()
hdlr = root.handlers[0]
hdlr.setFormatter(formatter)
dev_logger.critical('critical message')

print('========= basicConfig =========')
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    #datefmt='%m-%d %H:%M',
                    )
logging.debug('debug message')
logging.info('info message')
logging.warning('warning message')
logging.error('error message')
logging.critical('critical message')