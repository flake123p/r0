#
# Behavior: When app.log reaches 1MB, it will rotate and keep up to 3 old log files (app.log.1, app.log.2, etc.).
#

import logging
from logging.handlers import RotatingFileHandler

# Create a rotating file handler (max size: 1MB, keeps 3 backup files)
handler = RotatingFileHandler("app.log", maxBytes=1_000_000, backupCount=3)
handler.setLevel(logging.INFO)
handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))

# Create logger and add handler
logger = logging.getLogger("rotating_logger")
logger.setLevel(logging.INFO)
logger.addHandler(handler)

# Log messages
for i in range(100):
    logger.info(f"Log entry {i}")