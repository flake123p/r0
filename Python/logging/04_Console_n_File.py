import logging

# Create a logger
logger = logging.getLogger("my_logger")
logger.setLevel(logging.DEBUG)  # Set the minimum logging level

# Create a file handler to write logs to a file
file_handler = logging.FileHandler("app.log")
file_handler.setLevel(logging.INFO)  # Logs INFO and above to the file

# Create a console handler to print logs to the console
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)  # Logs DEBUG and above to the console

# Create a formatter for both handlers
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

# Attach the formatter to both handlers
file_handler.setFormatter(formatter)
console_handler.setFormatter(formatter)

# Add handlers to the logger
logger.addHandler(file_handler)
logger.addHandler(console_handler)

# Example log messages
logger.debug("This is a debug message (console only)")
logger.info("This is an info message (both file and console)")
logger.warning("This is a warning message (both file and console)")
logger.error("This is an error message (both file and console)")
logger.critical("This is a critical message (both file and console)")