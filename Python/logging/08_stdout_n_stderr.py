import logging
import sys

# Create a logger
logger = logging.getLogger("my_logger")
logger.setLevel(logging.DEBUG)  # Log everything (DEBUG and above)

# Create a handler for stdout (INFO and below)
stdout_handler = logging.StreamHandler(sys.stdout)
stdout_handler.setLevel(logging.DEBUG)  # Capture DEBUG and INFO logs

# Create a handler for stderr (WARNING and above)
stderr_handler = logging.StreamHandler(sys.stderr)
stderr_handler.setLevel(logging.WARNING)  # Capture WARNING and above logs

# Create a formatter and set it for both handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
stdout_handler.setFormatter(formatter)
stderr_handler.setFormatter(formatter)

# Add both handlers to the logger
logger.addHandler(stdout_handler)
logger.addHandler(stderr_handler)

# Log messages
logger.debug("This is a debug message (stdout)")
logger.info("This is an info message (stdout)")
logger.warning("This is a warning message (stderr & stdout)")
logger.error("This is an error message (stderr & stdout)")
logger.critical("This is a critical message (stderr & stdout)")