#
# filemode = 'w' will overwrite the file instead of appending to it.
#
import logging

# Configure logging to overwrite the file
logging.basicConfig(
    filename='app.log', 
    filemode='w',  # 'w' mode overwrites the file on each run
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s'
)

# Log messages
logging.info("This is an info message")
logging.warning("This is a warning message")
logging.error("This is an error message")