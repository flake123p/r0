# Link
https://stackoverflow.com/questions/4673373/logging-within-pytest-tests

# Solution: --log-cli-level=DEBUG

e.g.:
    pytest --log-cli-level=DEBUG

    pytest --log-cli-level=DEBUG $1 | tee log.log