#
# mode = 'w' will overwrite the file instead of appending to it.
#
import logging

# Create a logger
logger = logging.getLogger("my_logger")
logger.setLevel(logging.DEBUG)

# Create a file handler that overwrites the file
file_handler = logging.FileHandler("app.log", mode="w")  # 'w' mode overwrites the file
file_handler.setLevel(logging.INFO)  # Log level for file

# Create a formatter and set it for the file handler
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
file_handler.setFormatter(formatter)

# Add the handler to the logger
logger.addHandler(file_handler)

# Log messages
logger.info("This is an info message")
logger.warning("This is a warning message")
logger.error("This is an error message")