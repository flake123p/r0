import logging

logger = logging.getLogger("my_logger")

file_handler = logging.FileHandler("app.log", mode="w")  # 'w' mode overwrites the file

console_handler = logging.StreamHandler()

logger.addHandler(file_handler)
logger.addHandler(console_handler)

logger.debug("This is a debug message (console only)")
logger.info("This is an info message (both file and console)")
logger.warning("This is a warning message (both file and console)")
logger.error("This is an error message (both file and console)")
logger.critical("This is a critical message (both file and console)")