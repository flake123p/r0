import re

'''
re.search()

This function searches for the first occurrence of a pattern anywhere in the string.

'''
pattern = r"world"
string = "hello world"

search = re.search(pattern, string)
if search:
    print("Found:", search.group(), search.start(), search.end())  # Output: Found: world 6 11
else:
    print("Not found")