import re

'''
re.compile()

You can compile a regex for better performance, especially if you reuse it..
'''
pattern = re.compile(r"\d+")  # Pre-compile the pattern

matches = pattern.findall("123 apples and 456 oranges")
print(matches)  # Output: ['123', '456']