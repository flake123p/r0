import re

#
# Match a Word Character Repeated 2 to 4 Times
#
pattern = r"\w{2,4}"  # Matches words with 2 to 4 characters
string = "Hi there! My code is 1234xyz."

matches = re.findall(pattern, string)
print(matches)  # Output: ['Hi', 'ther', 'My', 'code', '1234', 'xyz']


#
# Match Exactly 3 to 5 Digits
#
pattern = r"\d{3,5}"  # Matches numbers with 3 to 5 digits
string = "Phone numbers: 123, 45678, and 123456."

matches = re.findall(pattern, string)
print(matches)  # Output: ['123', '45678', '12345']


#
# Match Repeated Characters
#
pattern = r"a{2,5}"  # Matches 2 to 5 'a' characters in a row
string = "a aa aaa aaaa aaaaa aaaaaa"

matches = re.findall(pattern, string)
print(matches)  
# Output:   ['aa', 'aaa', 'aaaa', 'aaaaa']
# Real:     ['aa', 'aaa', 'aaaa', 'aaaaa', 'aaaaa']


#
# Match a Range of Letters or Symbols
#
pattern = r"[A-Z]{1,3}"  # Matches 1 to 3 uppercase letters
string = "A AB ABC ABCD"

matches = re.findall(pattern, string)
print(matches)  
# Output:   ['A', 'AB', 'ABC']
# Real:     ['A', 'AB', 'ABC', 'ABC', 'D']


#
# Match Words with Specific Lengths
#
pattern = r"\b\w{4,7}\b"  # Matches words with 4 to 7 characters
string = "Hello world! Regex is awesome for patterns."

matches = re.findall(pattern, string)
print(matches)  # Output: ['Hello', 'world', 'awesome', 'patterns']


#
# Match Repeated Groups
#
pattern = r"(ab){2,4}"  # Matches 'ab' repeated 2 to 4 times
string = "ab abab abab abababab"

matches = re.findall(pattern, string)
print(matches)
# Output:   ['abab', 'abab', 'ababab']
# Real:     ['ab', 'ab', 'ab']


#
# Validate a String of Specific Length
#
# ^ ensures the pattern starts at the beginning.
# $ ensures the pattern ends at the end.
# Matches strings with exactly 4, 5, or 6 digits.
#
pattern = r"^\d{4,6}$"  # Matches strings with 4 to 6 digits ONLY
string1 = "1234"
string2 = "1234567"

print(bool(re.match(pattern, string1)))  # Output: True
print(bool(re.match(pattern, string2)))  # Output: False


#
# Match Specific Patterns in Text
#
# [aeiou] matches vowels.
# {2,3} matches sequences of 2 to 3 vowels.
#
pattern = r"[aeiou]{2,3}"  # Matches 2 to 3 consecutive vowels
string = "beautiful queue zoo."

matches = re.findall(pattern, string)
print(matches)  # Output: ['eau', 'ueu', 'oo']


#
# Match Hexadecimal Numbers
#
pattern = r"[A-Fa-f0-9]{3,6}"  # Matches 3 to 6 hexadecimal digits
string = "Colors: #FFF, #A1B2C3, and #123456. (0x123 0x456X)"

matches = re.findall(pattern, string)
print(matches)  # Output: ['FFF', 'A1B2C3', '123456', '123', '456']


#
# Match Dates in a Specific Format
#
pattern = r"\b\d{2,4}-\d{1,2}-\d{1,2}\b"  # Matches dates like YYYY-MM-DD or YY-M-D
string = "Valid dates: 2023-02-17, 23-2-7, 1999-12-31."

matches = re.findall(pattern, string)
print(matches)  # Output: ['2023-02-17', '23-2-7', '1999-12-31']