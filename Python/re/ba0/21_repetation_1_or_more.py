import re

#
# Match Letters Repeated 1 or More Times
#
pattern = r"a+"  # Matches 'a' repeated 1 or more times
string = "a aa aaa b aaaa"

matches = re.findall(pattern, string)
print(matches)  # Output: ['a', 'aa', 'aaa', 'aaaa']


#
# Match Digits Repeated 1 or More Times
#
pattern = r"\d+"  # Matches one or more digits
string = "123 abc 456 78"

matches = re.findall(pattern, string)
print(matches)  # Output: ['123', '456', '78']


#
# Match Words
#
pattern = r"\w+"  # Matches words (letters, numbers, or underscores)
string = "Hello world_123!"

matches = re.findall(pattern, string)
print(matches)  # Output: ['Hello', 'world_123']


#
# Match a Group Repeated 1 or More Times
#
pattern = r"(ab)+"  # Matches 'ab' repeated 1 or more times
string = "abab ab abababa"

matches = re.findall(pattern, string)
print(matches)  # Output: ['ab', 'ab', 'ab']


#
# Match Repeated Symbols
#
pattern = r"\$+"  # Matches '$' repeated 1 or more times
string = "$ $$ $$$$ $$"

matches = re.findall(pattern, string)
print(matches)  # Output: ['$', '$$', '$$$$', '$$']


#
# Match Any Character Repeated 1 or More Times
#
pattern = r".+"  # Matches any character 1 or more times (greedy)
string = "Hello world!"

matches = re.findall(pattern, string)
print(matches)  # Output: ['Hello world!']


#
# Match Vowels Repeated 1 or More Times
#
pattern = r"[aeiou]+"  # Matches 1 or more vowels
string = "beautiful queue zoo"

matches = re.findall(pattern, string)
print(matches)  # Output: ['eau', 'i', 'u', 'ueue', 'oo']


#
# Match Specific Patterns
#
pattern = r"[A-Z]+"  # Matches 1 or more uppercase letters
string = "Python IS Fun"

matches = re.findall(pattern, string)
print(matches)  # Output: ['P', 'IS', 'F']


#
# Match Continuous Whitespace
#
pattern = r"\s+"  # Matches 1 or more whitespace characters
string = "Hello   World"

matches = re.findall(pattern, string)
print(matches)  # Output: ['   ']


#
# Match Repeated Non-Digits
#
pattern = r"\D+"  # Matches 1 or more non-digit characters
string = "123abc456def"

matches = re.findall(pattern, string)
print(matches)  # Output: ['abc', 'def']


#
# Match Words with Optional Parts
#
pattern = r"colou+r"  # Matches 'colour', or 'colouur'
string = "color colour colouur colouuur"

matches = re.findall(pattern, string)
print(matches)  # Output: ['colour', 'colouur', 'colouuur']


#
# Match HTML Tags
#
# .+? matches characters inside HTML tags (stops at the first > due to non-greedy ?).
#
pattern = r"<.+?>"  # Matches 1 or more characters inside HTML tags (non-greedy)
string = "<div>Hello</div><p>World</p>"

matches = re.findall(pattern, string)
print(matches)  # Output: ['<div>', '</div>', '<p>', '</p>']


#
# Match Dates or Numbers
#
pattern = r"\d+-\d+"  # Matches numbers separated by a hyphen
string = "2023-02, 1999-12, and 123-456"

matches = re.findall(pattern, string)
print(matches)  # Output: ['2023-02', '1999-12', '123-456']


#
# Match Floating Ponint Numbers
#
pattern = r"\d+\.\d+"  # Matches floating ponint numbers
string = "3.14, 0.99, and 42.0"

matches = re.findall(pattern, string)
print(matches)  # Output: ['3.14', '0.99', '42.0']


#
# Match Continuous Letters in Words
#
pattern = r"[a-zA-Z]+"  # Matches 1 or more letters (case-insensitive)
string = "Regex101 is great!"

matches = re.findall(pattern, string)
print(matches)  # Output: ['Regex', 'is', 'great']