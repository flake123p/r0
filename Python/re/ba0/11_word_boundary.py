import re
'''
In Python's re module, \b represents a word boundary.

A word boundary is a position where a word starts or ends.

It ensures that the match happens only at the beginning or 
end of a word and not in the middle of a word.
'''

#
# Match a Word at Its Boundary
#
pattern = r"\bcat\b"  # Match the word "cat" as a whole word
string = "The cat is on the catalog."

matches = re.findall(pattern, string)
print(matches)  # Output: ['cat']


#
# Replace
#
pattern = r"\bcat\b"  # Match the word "cat" as a whole word
string = "The cat is on the catalog."

repl = re.sub(pattern, "dog", string)
print(repl)  # Output: ['cat']


#
# Match Words Starting with a Specific Pattern
#
# \bdog matches "dog" and "dogfish" because they both start with "dog" at a word boundary
#
pattern = r"\bdog"  # Match words starting with "dog"
string = "The dog chased the dogfish."

matches = re.findall(pattern, string)
print(matches)  # Output: ['dog', 'dog']


#
# Match Words Ending with a Specific Pattern
#
pattern = r"fish\b"  # Match words ending with "fish"
string = "The catfish swims with a fish."

matches = re.findall(pattern, string)
print(matches)  # Output: ['fish', 'fish']


#
# Match Words Without Word Boundaries
#
pattern = r"cat"  # Match "cat" anywhere
string = "The cat is on the catalog."

matches = re.findall(pattern, string)
print(matches)  # Output: ['cat', 'cat']


#
# Match Words Using Negated Word Boundaries
#
pattern = r"\Bcat"  # Match "cat" not at the beginning of a word
string = "The cat is in the catalog."

matches = re.findall(pattern, string)
print(matches)  # Output: []


#
# Replace Words at Word Boundaries
#
pattern = r"\bfox\b"  # Match the whole word "fox"
string = "The quick brown fox jumps over the lazy fox."
replacement = "wolf"

result = re.sub(pattern, replacement, string)
print(result)  # Output: "The quick brown wolf jumps over the lazy wolf."


#
# Find Words with Specific Patterns
#
pattern = r"\b[a-zA-Z]{3}\b"  # Match words with exactly 3 letters
string = "The fox ran fast to the den."

matches = re.findall(pattern, string)
print(matches)  # Output: ['The', 'fox', 'ran', 'the', 'den']


#
# Count Words in a String
#
pattern = r"\b\w+\b"  # Match whole words
string = "Count the number of words in this sentence."

matches = re.findall(pattern, string)
print(len(matches))  # Output: 8


#
# Match Words Starting with a Capital Letter
#
pattern = r"\b[A-Z][a-z]*\b"  # Match words starting with a capital letter
string = "Alice went to Wonderland with Bob and Charlie."

matches = re.findall(pattern, string)
print(matches)  # Output: ['Alice', 'Wonderland', 'Bob', 'Charlie']