import re

#
# Match 0 or 1 Character Non-Greedily
#
string = "ab aaab b acb aabb"
# Matches 'b' preceded by 0 or 1 'a' (non-greedy 0 or 1)

print('case 1', re.findall(r"ab", string))    # Output: ['ab', 'ab', 'ab']
print('case 2', re.findall(r"a.b", string))   # Output: ['aab', 'acb', 'aab']
print('case 3', re.findall(r"a*b", string))   # Output: ['ab', 'aaab', 'b', 'b', 'aab', 'b']
print('case 4', re.findall(r"a.*b", string))  # Output: ['ab aaab b acb aabb'] (ALL)
print('case 5', re.findall(r"a+b", string))   # Output: ['ab', 'aaab', 'aab']
print('case 6', re.findall(r"a.+b", string))  # Output: ['ab aaab b acb aabb'] (ALL)
print('case 7', re.findall(r"a?b", string))   # Output: ['ab', 'ab', 'b', 'b', 'ab', 'b']
print('case 8', re.findall(r"a??b", string))  # Output: ['ab', 'ab', 'b', 'b', 'ab', 'b']
print('case 9', re.findall(r"a.?b", string))  # Output: ['ab', 'aab', 'acb', 'aab']
print('case A', re.findall(r"a.??b", string)) # Output: ['ab', 'aab', 'acb', 'aab']
print('case B', re.findall(r"a.*?b", string)) # Output: ['ab', 'aaab', 'acb', 'aab']


#
# Match Numbers Non-Greedily 0 or 1
#
pattern = r"\d??\w"  # Matches a word character optionally preceded by 1 digit (non-greedy)
string = "123abc"

matches = re.findall(pattern, string)
print(matches)  # Output: ['1', '2', '3', 'a', 'b', 'c']


#
# Match Numbers Non-Greedily
#
pattern = r"\d?\w"
string = "123abc"

matches = re.findall(pattern, string)
print(matches)  # Output: ['12', '3a', 'b', 'c']


#
# Match Numbers Greedily
#
pattern = r"\d*\w"
string = "123abc"

matches = re.findall(pattern, string)
print(matches)  # Output: ['123a', 'b', 'c']


#
# Match Numbers Greedily 1 or more
#
pattern = r"\d+\w"
string = "123abc"

matches = re.findall(pattern, string)
print(matches)  # Output: ['123a']


#
# Match Smallest Patterns in a Sentence (Non-Greedy 0 1)
#
pattern = r"a??\w"  # Matches a word character optionally preceded by 1 'a' (non-greedy)
string = "abc aaa"

matches = re.findall(pattern, string)
print(matches)  # Output: ['a', 'b', 'c', 'a', 'a', 'a']


#
# Match Smallest Patterns in a Sentence (Non-Greedy)
#
pattern = r"a?\w"
string = "abc aaa"

matches = re.findall(pattern, string)
print(matches)  # Output: ['ab', 'c', 'aa', 'a']


#
# Match Optional Symbols Non-Greedily 0 1
#
pattern = r"\$??\d"  # Matches a digit optionally preceded by '$' (non-greedy)
string = "$1 $2 3 $4"

matches = re.findall(pattern, string)
print(matches)  # Output: ['$1', '$2', '3', '$4']


#
# Match Optional Symbols Non-Greedily
#
pattern = r"\$?\d"  # Matches a digit optionally preceded by '$' (non-greedy)
string = "$1 $2 3 $4"

matches = re.findall(pattern, string)
print(matches)  # Output: ['$1', '$2', '3', '$4']