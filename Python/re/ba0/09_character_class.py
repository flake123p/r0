import re
'''
In Python's re module, square brackets [] are used to define a character class, 
which matches any one character within the brackets. 

You can use ranges, single characters, or combinations of both.
'''
#
# Match Any Character in the Brackets
#
pattern = r"[aeiou]"  # Match any vowel
string = "hello world"

matches = re.findall(pattern, string)
print(matches)  # Output: ['e', 'o', 'o']


#
# Match Characters in a Range
#
pattern = r"[a-d]"  # Match any lowercase letter from 'a' to 'd'
string = "abcdeABCDE"

matches = re.findall(pattern, string)
print(matches)  # Output: ['a', 'b', 'c', 'd']


#
# Match Digits
#
pattern = r"[0-9]"  # Match any digit
string = "Phone: 123-456-7890"

matches = re.findall(pattern, string)
print(matches)  # Output: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']


#
# Combine Multiple Ranges
#
pattern = r"[a-zA-Z0-9]"  # Match any letter or digit
string = "Hello123!"

matches = re.findall(pattern, string)
print(matches)  # Output: ['H', 'e', 'l', 'l', 'o', '1', '2', '3']


#
# Match Special Characters
#
pattern = r"[!@#$%^&*]"  # Match any special character in the class
string = "Hello@World!$"

matches = re.findall(pattern, string)
print(matches)  # Output: ['@', '!', '$']


#
# Negation Using ^
#
pattern = r"[^aeiou]"  # Match any character that is NOT a vowel
string = "hello world"

matches = re.findall(pattern, string)
print(matches)  # Output: ['h', 'l', 'l', ' ', 'w', 'r', 'l', 'd']


#
# Match a Specific Set of Characters
#
pattern = r"[abc]"  # Match 'a', 'b', or 'c'
string = "abcdef"

matches = re.findall(pattern, string)
print(matches)  # Output: ['a', 'b', 'c']


#
# Match Word Boundaries
#
pattern = r"[\s]"  # Match any whitespace character
string = "Hello World!"

matches = re.findall(pattern, string)
print(matches)  # Output: [' ']


#
# Match a Literal Hyphen
#
pattern = r"[a-z\-]"  # Match any lowercase letter or a hyphen
string = "a-z abc-def"

matches = re.findall(pattern, string)
print(matches)  # Output: ['a', '-', 'z', 'a', 'b', 'c', '-', 'd', 'e', 'f']


#
# Match Repeated Characters
#
pattern = r"[a-z]+"  # Match one or more lowercase letters
string = "hello123world"

matches = re.findall(pattern, string)
print(matches)  # Output: ['hello', 'world']


#
# Match Uppercase and Digits
#
pattern = r"[A-Z0-9]+"  # Match one or more uppercase letters or digits
string = "PYTHON123java456"

matches = re.findall(pattern, string)
print(matches)  # Output: ['PYTHON123', '456']


#
# Match Hexadecimal Characters (Not very useful)
#
pattern = r"[0-9A-Fa-f]"  # Match any hex character
string = "Hex: 1A3F"

matches = re.findall(pattern, string)
print(matches)  # Output: ['e', '1', 'A', '3', 'F']


#
# Match Alphanumeric and Underscore
#
# [\w] is equivalent to [a-zA-Z0-9_].
#
pattern = r"[\w]"  # Matches alphanumeric characters and underscore
string = "Python_3.9"

matches = re.findall(pattern, string)
print(matches)  # Output: ['P', 'y', 't', 'h', 'o', 'n', '_', '3', '9']


#
# Match Any Character (Custom Class)
#
pattern = r"[a-zA-Z0-9!?]"  # Match letters, digits, or '!', '?'
string = "Hello123!?"

matches = re.findall(pattern, string)
print(matches)  # Output: ['H', 'e', 'l', 'l', 'o', '1', '2', '3', '!', '?']