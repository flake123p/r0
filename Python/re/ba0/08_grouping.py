import re
'''
Regular Parentheses (()): 
    Used for grouping and capturing.

Non-Capturing Groups ((?:...)): 
    Used for grouping without capturing.

Named Groups ((?P<name>...)): 
    Allow assigning meaningful names to groups.
'''
#
# Grouping for Capturing Substrings
#
pattern = r"(ab)c"  # Group 'ab' together
string = "abc ac abd ab"

matches = re.findall(pattern, string)
print(matches)  # Output: ['ab']


#
# Applying Quantifiers to Groups
#
pattern = r"(abc)+"  # Match 'abc' repeated one or more times
string = "abcabc xyzabc"

matches = re.findall(pattern, string)
print(matches)  # Output: ['abc', 'abc']


#
# Nested Groups
#
pattern = r"(a(bc))"  # Group 'a(bc)', with a nested group '(bc)'
string = "abc abcd"

matches = re.findall(pattern, string)
print(matches)  # Output: [('abc', 'bc'), ('abc', 'bc')]


#
# Nested Groups Part II
#
pattern = r"(a(bc)d)"
string = "abcd"

match = re.search(pattern, string)
if match:
    print(match.group(0))  # Output: abcd
    print(match.group(1))  # Output: abcd (outer group)
    print(match.group(2))  # Output: bc (inner group)


#
# Non-Capturing Groups (?:...)
#
pattern = r"(?:abc)+"  # Match 'abc' repeated one or more times, but do not capture
string = "abcabc xyzabc"

matches = re.findall(pattern, string)
print(matches)  # Output: ['abcabc', 'abc']


#
# Non-Capturing Groups II (?:...)
#
pattern = r"(?:abc)+"
string = "abcabcabc"

match = re.search(pattern, string)
if match:
    print(match.group(0))  # Output: abcabcabc


#
# Extracting Multiple Groups
#
pattern = r"(\w+):(\d+)"  # Capture word and number
string = "apple:5 orange:10 banana:3"

matches = re.findall(pattern, string)
print(matches)  # Output: [('apple', '5'), ('orange', '10'), ('banana', '3')]


#
# Extracting Without Multiple Groups
#
pattern = r"\w+:\d+"  # Capture word and number
string = "apple:5 orange:10 banana:3"

matches = re.findall(pattern, string)
print(matches)  # Output: ['apple:5', 'orange:10', 'banana:3']


#
# Group Referencing in Substitution
#
pattern = r"(\w+) (\w+)"  # Capture two words
string = "John Doe"

result = re.sub(pattern, r"\2, \1", string)
print(result)  # Output: "Doe, John"


#
# Conditional Matching with Groups
#
pattern = r"(\d+)?[a-z]+"  # Optional group for digits, followed by letters
string = "123abc abc 88 99x"

matches = re.findall(pattern, string)
print(matches)  # Output: ['123', '', '99']


#
# Repeating Groups
#
pattern = r"(ab)+"  # Match one or more 'ab'
string = "abababc ad abd"

matches = re.findall(pattern, string)
print(matches)  # Output: ['ab', 'ab']


#
# Using Groups with re.match
#
pattern = r"(\w+) (world)"  # Match '(word) world'
string = "hello world"

match = re.match(pattern, string)
if match:
    print(match.group(0))  # Output: "hello world" (entire match)
    print(match.group(1))  # Output: "hello" (first group)
    print(match.group(2))  # Output: "world" (second group)


#
# Splitting Strings Using Groups
#
pattern = r"(\d+)"  # Capture numbers
string = "apple123orange456banana"

result = re.split(pattern, string)
print(result)  # Output: ['apple', '123', 'orange', '456', 'banana']


#
# Combining Alternation and Groups (OR)
#
pattern = r"(cat|dog|fish)"  # Match 'cat', 'dog', or 'fish'
string = "I have a cat, a dog, and a fish."

matches = re.findall(pattern, string)
print(matches)  # Output: ['cat', 'dog', 'fish']


#
# Grouping for Alternation
#
pattern = r"I like (cats|dogs|birds)"
string = "I like dogs"

match = re.search(pattern, string)
if match:
    print(match.group(0))  # Output: I like dogs
    print(match.group(1))  # Output: dogs


#
# Using Group Names: ?P<word>
#
pattern = r"(?P<word>\w+):(?P<number>\d+)"  # Named groups
string = "apple:5 orange:10 banana:3"

matches = re.finditer(pattern, string)
for match in matches:
    print(match.group("word"), match.group("number"))
# apple 5
# orange 10
# banana 3

#
# Optional Groups
#
pattern = r"foo(bar)?"
string = "foobar foo"

matches = re.findall(pattern, string)
print(matches)  # Output: ['bar', '']


#
# Match and Capture Multiple Groups
#
pattern = r"(\d+)-(\d+)-(\d+)"
string = "2025-02-17"

match = re.search(pattern, string)
if match:
    print(match.group(1))  # Output: 2025 (first group)
    print(match.group(2))  # Output: 02 (second group)
    print(match.group(3))  # Output: 17 (third group)