import re

#
# \? - just stands for character: ?
#
pattern = r"\d+\?\d+"  # Matches decimal numbers
string = "3.14, 0?99, and 42.0"

matches = re.findall(pattern, string)
print(matches)  # ['0?99']


#
# Match HTML Tags (Non-Greedy)
#
# Greedy .* would match everything between the first < and the last >.
# Non-greedy .*? matches the smallest possible portion (e.g., each tag separately).
#
pattern = r"<.*?>"  # Non-greedy: Matches the smallest possible substring
string = "<div>Hello</div><p>World</p>"

matches = re.findall(pattern, string)
print(matches)  # Output: ['<div>', '</div>', '<p>', '</p>']


#
# Match HTML Tags (Greedy)
#
pattern = r"<.*>"  # Greedy
string = "<div>Hello</div><p>World</p>"

matches = re.findall(pattern, string)
print(matches)  # Output: ['<div>Hello</div><p>World</p>']


#
# Match the First Word in Quotes
#
pattern = r'"(.*?)"'  # Non-greedy: Matches the smallest substring in quotes
string = '"first" and "second" and "third"'

matches = re.findall(pattern, string)
print(matches)  # Output: ['first', 'second', 'third']


#
# Match the First Word in Quotes (Greedy)
#
pattern = r'"(.*)"'  # Greedy (.*) would match the entire string between the first " and the last ".
string = '"first" and "second" and "third"'

matches = re.findall(pattern, string)
print(matches)  # Output: ['first" and "second" and "third']


#
# Match the First Section of a String
#
pattern = r"start.*?end"  # Non-greedy: Matches the shortest portion between 'start' and 'end'
string = "start123end456start789end"

matches = re.findall(pattern, string)
print(matches)  # Output: ['start123end', 'start789end']


#
# Match the First Section of a String (Greedy)
#
pattern = r"start.*end"
string = "start123end456start789end"

matches = re.findall(pattern, string)
print(matches)  # Output: ['start123end456start789end']


#
# Match Smallest Repeated Patterns
#
pattern = r"a.*?b"  # Non-greedy: Matches the smallest portion from 'a' to 'b'
string = "a123b456ab789bc"

matches = re.findall(pattern, string)
print(matches)  # Output: ['a123b', 'ab']


#
# Match Smallest Repeated Patterns (Greedy)
#
pattern = r"a.*b"
string = "a123b456ab789bc"

matches = re.findall(pattern, string)
print(matches)  # Output: ['a123b456ab789b']


#
# Match Numbers Before a Comma
#
pattern = r"\d+?,"
string = "123, 456, 789,"

matches = re.findall(pattern, string)
print(matches)  # Output: ['123,', '456,', '789,']


#
# Match Numbers Before a Comma (Greedy, but same result)
#
pattern = r"\d+,"
string = "123, 456, 789,"

matches = re.findall(pattern, string)
print(matches)  # Output: ['123,', '456,', '789,']


#
# Match Smallest Substring Between Two Characters
#
pattern = r"\[.*?\]"  # Non-greedy: Matches the shortest portion inside square brackets
string = "[abc][def][ghi]"

matches = re.findall(pattern, string)
print(matches)  # Output: ['[abc]', '[def]', '[ghi]']


#
# Match Smallest Substring Between Two Characters (Greedy)
#
pattern = r"\[.*\]"
string = "[abc][def][ghi]"

matches = re.findall(pattern, string)
print(matches)  # Output: ['[abc][def][ghi]']


#
# Extract the First Part of a URL
#
pattern = r"https://.*?/"  # Non-greedy: Matches up to the first `/`
string = "https://example.com/path/to/page"

matches = re.findall(pattern, string)
print(matches)  # Output: ['https://example.com/']


#
# Extract the First Part of a URL (Greedy)
#
pattern = r"https://.*/"
string = "https://example.com/path/to/page"

matches = re.findall(pattern, string)
print(matches)  # Output: ['https://example.com/path/to/']


#
# Match First Sentence in a Paragraph
#
pattern = r".*?\."  # Non-greedy: Matches the shortest text up to the first period
string = "This is the first sentence. This is the second sentence."

matches = re.findall(pattern, string)
print(matches)  # Output: ['This is the first sentence.', ' This is the second sentence.']


#
# Match First Sentence in a Paragraph
#
pattern = r".*\."  # Greedy
string = "This is the first sentence. This is the second sentence."

matches = re.findall(pattern, string)
print(matches)  # Output: ['This is the first sentence. This is the second sentence.']


#
# Match Repeated Symbols Non-Greedily
#
pattern = r"\$+?"  # Non-greedy: Matches the smallest number of '$'s
string = "$$$ $$ $"

matches = re.findall(pattern, string)
print(matches)  # Output: ['$', '$', '$', '$', '$', '$']


#
# Match Repeated Symbols Non-Greedily
#
pattern = r"\$+"  # Greedy
string = "$$$ $$ $"

matches = re.findall(pattern, string)
print(matches)  # Output: ['$$$', '$$', '$']


#
# Match Optional Text Non-Greedily
#
pattern = r"\(.*?\)"  # Non-greedy: Matches the shortest text inside parentheses
string = "This is (example 1) and (example 2)!"

matches = re.findall(pattern, string)
print(matches)  # Output: ['(example 1)', '(example 2)']


#
# Match Optional Text Non-Greedily (Greedy)
#
pattern = r"\(.*\)"
string = "This is (example 1) and (example 2)!"

matches = re.findall(pattern, string)
print(matches)  # Output: ['(example 1) and (example 2)']