import re

#
# Match a Letter Repeated 0 or More Times
#
pattern = r"a*"  # Matches 'a' repeated 0 or more times (resulting in empty matches)
string = "aaa b aaaa c"

matches = re.findall(pattern, string)
print(matches)  
# Output: ['aaa', '', '', '', 'aaaa', '', '', '']


#
# Match Digits Repeated 0 or More Times
#
pattern = r"\d*"  # Matches 0 or more digits
string = "123 abc 456"

matches = re.findall(pattern, string)
print(matches)  
# Output: ['123', '', '', '', '', '', '456', '']


#
# Match Optional Words
#
pattern = r"cat*"  # Matches 'cat', 'catt', 'cattt', or just 'c'
string = "cat catt catt cattttt cc"

matches = re.findall(pattern, string)
print(matches)  
# Output: ['cat', 'catt', 'catt', 'cattttt']


#
# Match Optional Groups
#
pattern = r"(ab)*"  # Matches 0 or more repetitions of 'ab'
string = "abab abab abc ababab"

matches = re.findall(pattern, string)
print(matches)  
# Output: ['ab', '', 'ab', '', 'ab', '', 'ab', '']


#
# Match Any Character Repeated 0 or More Times
#
pattern = r".*"  # Matches any character 0 or more times (greedy match)
string = "hello world!"

matches = re.findall(pattern, string)
print(matches)  
# Output: ['hello world!', '']


#
# Match Leading Whitespace (or None)
#
pattern = r"\s*"  # Matches 0 or more whitespace characters
string = "   Hello  World"

matches = re.findall(pattern, string)
print(matches)  
# Output: ['   ', '', '', '', '', '', '  ', '', '', '', '', '', '']


#
# Match Repeated Digits or No Digits
#
pattern = r"\d*"  # Matches 0 or more digits
string = "Age: 25, Height: , Weight: 70"

matches = re.findall(pattern, string)
print(matches)  
# Output: ['', '', '', '', '', '25', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '70', '']


#
# Match Specific Patterns
#
pattern = r"[a-z]*"  # Matches lowercase letters repeated 0 or more times
string = "abc 123 xyz!"

matches = re.findall(pattern, string)
print(matches)  
# Output: ['abc', '', '', '', '', '', 'xyz', '', '']


#
# Match Repeated Symbols
#
pattern = r"\$*"  # Matches '$' repeated 0 or more times
string = "Price: $$$ Free: $ Only: $$"

matches = re.findall(pattern, string)
print(matches)
# Output: ['', '', '', '', '', '', '', '$$$', '', '', '', '', '', '', '', '$', '', '', '', '', '', '', '', '$$', '']


#
# Match HTML Tags or None
#
pattern = r"<.*?>"  # Matches HTML tags (non-greedy)
string = "<div>Hello</div><p>World</p>"

matches = re.findall(pattern, string)
print(matches)  # Output: ['<div>', '</div>', '<p>', '</p>']


#
# Match Empty Strings or Sequences
#
pattern = r"x*"  # Matches 0 or more 'x' characters
string = "xx xy y"

matches = re.findall(pattern, string)
print(matches)
# Output: ['xx', '', 'x', '', '', '', '']


#
# Match Words with Optional Letters
#
pattern = r"colou*r"  # Matches 'color', or 'colour'
string = "color, colour, colr"

matches = re.findall(pattern, string)
print(matches)  # Output: ['color', 'colour']


#
# Match IP Address Parts
#
# .* allows the dot to appear after the digits or not at all.
#
pattern = r"\d{1,3}\.*"  # Matches 1-3 digits followed by 0 or more dots
string = "192.168.0.1"

matches = re.findall(pattern, string)
print(matches)  # Output: ['192.', '168.', '0.', '1']


#
# Match Empty Strings Between Words
#
pattern = r"\w*\s*"  # Matches words followed by 0 or more spaces
string = "Hello   world!"

matches = re.findall(pattern, string)
print(matches)
# Output: ['Hello   ', 'world', '', '']
