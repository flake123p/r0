import re

#
# [ ]
#
pattern = r"\[a+\]"
string = "a [a] [aa] [abc] [gag]"

matches = re.findall(pattern, string)
print(matches)  # Output: ['[a]', '[aa]']


#
# ( )
#
pattern = r"\(a+\)"
string = "a (a) (aa) (abc) (gag)"

matches = re.findall(pattern, string)
print(matches)  # Output: ['(a)', '(aa)']


#
# '(', ')' with capture: ()
#
pattern = r"\((a+)\)"
string = "a (a) (aa) (abc) (gag)"

matches = re.findall(pattern, string)
print(matches)  # Output: ['a', 'aa']
