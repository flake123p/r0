import re

#
# *
#
pattern = r"colou*r"  # Matches 'color', or 'colour'
string = "color, colour, colouur, colouuur"

matches = re.findall(pattern, string)
print(matches)  # Output: ['color', 'colour', 'colouur', 'colouuur']


#
# ?
#
pattern = r"colou?r"
string = "color, colour, colouur, colouuur"

matches = re.findall(pattern, string)
print(matches)  # Output: ['color', 'colour']
