import re

'''
You can use flags to modify the behavior of regex. Common flags include:

re.IGNORECASE (re.I) - Makes the pattern case-insensitive.
re.DOTALL (re.S) - Makes . match newline characters too.
re.MULTILINE (re.M) - Allows ^ and $ to match at the start and end of each line.
'''

pattern = r"hello"
string = "Hello world"

match = re.search(pattern, string, re.IGNORECASE)
if match:
    print("Matched:", match.group())  # Output: Matched: Hello