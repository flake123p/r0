import re

'''
re.findall()

This function returns all matches of a pattern as a list.
'''
pattern = r"\d+"  # Matches one or more digits
string = "The numbers are 123 and 456."

matches = re.findall(pattern, string)
print("Matches:", matches)  # Output: Matches: ['123', '456']
