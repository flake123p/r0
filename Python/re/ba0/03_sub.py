import re

'''
re.sub()

This function is used to replace matches with a new string.
'''
pattern = r"\d+"  # Matches digits
string = "The price is 100 to 150 dollars."

result = re.sub(pattern, "200", string)
print(result)  # Output: The price is 200 to 200 dollars.


#
# Reserve a, b, c, 0, 1, 2, -
#
pattern = r"[^a-c0-2-]"
string = "abcdefg-0123456"

result = re.sub(pattern, "", string)
print(result)  # Output: abc-012


#
# Replace 'a' to '', but only once (count=1)
#
pattern = r"a"
string = "babca"

result = re.sub(pattern, "", string, count=1)
print(result)  # Output: abc-012