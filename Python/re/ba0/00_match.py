import re

'''
re.match()

This function tries to match a pattern at the "BEGINNING" of a string.

'''
pattern = r"hello"
string = "hello world"

match = re.match(pattern, string)
if match:
    print("Matched:", match.group(), match.start(), match.end())  # Output: Matched: hello 0 5
else:
    print("No match")

#
# not match if the pattern is in middle of a string
#
pattern = r"world"
string = "hello world"

match = re.match(pattern, string)
if match:
    print("Matched:", match.group())
else:
    print("No match")  # Output: No match


#
# Check if a pattern in middle of string using "in". (You can use re.search() too!)
#
pattern = "world"
string = "hello world"

match = True if pattern in string else False
if match:
    print("Matched:", match)  # Output: Matched: True
else:
    print("No match")