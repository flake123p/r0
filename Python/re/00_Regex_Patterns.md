```
Here are some common patterns:

.   - Matches any character except a newline.
^   - Matches the beginning of a string.
$   - Matches the end of a string. (e.g.: mys list_r | grep .py$)
\.  - Matches a . character.
\^  - Matches a ^ character.
\$  - Matches a $ character.
\(  - Matches a ( character.
\)  - Matches a ) character.
\d  - Matches a digit (0-9).
\D  - Matches a non-digit.
\w  - Matches a word character (letters, digits, underscore).
\W  - Matches a non-word character.
\s  - Matches a whitespace character.
\S  - Matches a non-whitespace character.
*   - Matches 0 or more repetitions.
+   - Matches 1 or more repetitions.
{n,m} - Matches n to m repetitions.
?   - Matches 0 or 1 repetitions.
      non-greedy, it matches as little as possible (lazy match).
.*  - Greedy.     r"<.*>" would match everything between the first < and the last >.
        - "<div>Hello</div><p>World</p>" to ['<div>', '</div>', '<p>', '</p>']
.*? - Non-greedy. r"<.*?>" matches the smallest possible portion (e.g., each tag separately).
        - "<div>Hello</div><p>World</p>" to ['<div>Hello</div><p>World</p>']
??  - Non-greedy 0 or 1.
      The ?? quantifier in regular expressions makes the preceding quantifier
      non-greedy, but only matches 0 or 1 repetition of the preceding element.
      It is the non-greedy version of ?.

\1  - 1st group.
\2  - 2nd group.
\w+   - word
(\w+) - word grouping
\d+   - number
(\d+) - number grouping

[]     - Character class
[a-z]  - Matches a to z (any lowercase letter).
[a-z]+ - Matches a to z 1 or more repetitions.
[^]    - Negation
[^abc] - Match any character that is NOT a and NOT b and NOT c (not anyone of a b c). 
[\-]   - Matches a Literal Hyphen
[\w]   - is equivalent to [a-zA-Z0-9_].
[\s]   - Matches whitespace characters.


https://stackoverflow.com/questions/16944357/carets-in-regular-expressions
^ :
[^abc]  -> not a, b or c
[ab^cd] -> a, b, ^ (character), c or d
\^      -> a ^ character
Anywhere else -> start of string or line.

^[b-d]t$ ->
            Start of line
            b/c/d character
            t character
            End of line

\b      - Word Boundary
\bcat\b - Match the word "cat" as a whole word
\bcat   - Match words starting with "cat"
cat\b"  - Match words ending with "cat"
\b\w+\b - Match whole words
\b[a-zA-Z]{3}\b - Match words with exactly 3 letters
\b[A-Z][a-z]*\b - Match words starting with a capital letter
```