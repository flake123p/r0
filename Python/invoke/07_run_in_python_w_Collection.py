from invoke import task, Context, Collection
import tasks

ctx = Context()

# Group tasks together
namespace = Collection(tasks.hello, tasks.list_files)

# Manually invoke tasks
ctx = Context()
namespace["list_files"](ctx)   # Calls list_files()
namespace["hello"](ctx)   # Calls hello()