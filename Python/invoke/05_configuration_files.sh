#!/bin/bash

#
# pip install invoke
#

# You can create an invoke.yaml or ~/.invoke.yml file to store default configurations.

export INVOKE_YAML=invoke.yaml  # macOS/Linux
# set INVOKE_YAML=invoke.yaml  # Windows

invoke greet
invoke greet -f invoke.yaml
invoke greet -c invoke.yaml