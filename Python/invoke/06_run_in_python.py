from invoke import task, Context
import tasks

ctx = Context()

tasks.greet(ctx, name="Bob")  # Output: Hello, Bob!