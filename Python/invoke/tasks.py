from invoke import task
import os

@task
def hello(c):
    print("Hello, World!")

@task
def check(c):
    if os.path.getmtime("file1.txt") > os.path.getmtime("file2.txt"):
        print("file1.txt is newer")
    else:
        print("file2.txt is newer")

@task
def list_files(c):  # Use it by "list-files", NOT UNDERSCORE !!!
    c.run("ls -l")  # On Windows, use "dir"

@task
def greet(c, name="World"):
    print(f"Hello, {name}!")

@task
def clean(c):
    print("Cleaning up...")

@task
def build(c):
    print("Building project...")

@task(pre=[clean, build])
def deploy(c):
    print("Deploying project...")
