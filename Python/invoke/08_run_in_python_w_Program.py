from invoke import task, Collection, Program

@task
def greet2(c, name="World"):
    print(f"Hello, {name} 222!")

ns = Collection(greet2)

if __name__ == "__main__":
    program = Program(namespace=ns)
    program.run()