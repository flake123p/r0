import pandas as pd
import datetime

def pickle_from(file):
    import pickle
    with open(file, 'rb') as my_file:
        return pickle.load(my_file)

startDate = datetime.datetime(2019, 5, 31)
endDate = datetime.datetime(2021, 1, 30)

d = pickle_from('META.pkl')
print(d)

print('Keys = ', d.keys())

print(startDate)
print(endDate)

print('d.index[0] = ', d.index[0])
