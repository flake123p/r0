import yaml

names_yaml = """
- 'eric'
- 'justin'
- 'mary-kate'
"""

names = yaml.safe_load(names_yaml)

with open('names.yaml', 'w') as file:
    yaml.dump(names, file)

print(open('names.yaml').read())


d = {
    'a' : 10,
    'b' : [20, 40],
    'c' : {
        'a.py' : ['GCC', 'GDB'],
        'b.py' : ['CLANG'],
    },
}

with open('names2.yaml', 'w') as file:
    yaml.dump(d, file)

#
# pip install ruamel.yaml
#
with open('names3.yaml', 'w') as file:
    import ruamel.yaml
    yaml = ruamel.yaml.YAML()
    yaml.indent(mapping=2, sequence=4, offset=2)
    yaml.dump(d, file)

#
# List of dict
#
lod = list()
lod.append({'A' : 111})
lod.append({'B' : 222})
lod.append({'C' : 333})
with open('names4.yaml', 'w') as file:
    d = {'ALL' : lod}
    yaml.dump(d, file)