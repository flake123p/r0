import yaml

with open('config.yaml', 'r') as file:
    yd = yaml.safe_load(file)

def pr(name):
    print(f'\n{name}:')
    print(yd[name])

pr('dep')
print(type(yd['dep']))

pr('dep1')
print(type(yd['dep1']))

pr('dep2')
print(type(yd['dep2']))

pr('dep3')
print(type(yd['dep3']))

pr('dep4')
print(type(yd['dep4']))

pr('dep5')
print(type(yd['dep5']))

pr('dep6')
print(type(yd['dep6']))
print('len =', len(yd['dep6']))