import yaml

with open('config.yaml', 'r') as file:
    prime_service = yaml.safe_load(file)

print('type = ', type(prime_service))
print(prime_service['prime_numbers'][0])
print(prime_service['rest']['url'])

def pr(name):
    print(f'\n{name}:')
    print(prime_service[name])

pr('gen.py')
pr('static_targets')
pr('dynamic_targets')
pr('dep')
pr('root')
pr('int_or_str')
print(type(prime_service['int_or_str']))
pr('int_or_str2')
print(type(prime_service['int_or_str2']))

pr('ONE_LINE')
print(prime_service['ONE_LINE']['AA'])