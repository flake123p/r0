'''
https://python.land/data-processing/python-yaml

PyYAML safe_load() vs load()

You will encounter many examples of PyYAML usage where load() is used instead of safe_load(). 
I intentionally didn’t tell you about the load() function until now. 
Since most people have a job to do and tend to copy-paste some example code quickly, 
I wanted them to use the safest method of parsing YAML with Python.

However, if you’re curious about the difference between these two, 
here’s the summary: load() is a dangerously powerful function, just like pickle, if you know that function. 
Both are insecure methods because they allow an attacker to execute arbitrary code. 

PyYAML’s load function allows you to serialize and deserialize complete Python objects and even execute Python code, 
including calls to the os.system library, which can execute any command on your system.

In recent PyYAML versions, the load() function is deprecated and will issue a big fat warning when you use it in an insecure way.

If you’re parsing regular YAML files, like 99% of us do, you should always use safe_load(), 
since it only contains a subset of the load function. All the scary, arbitrary code execution type of stuff is stripped out.
'''