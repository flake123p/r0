'''
https://stackoverflow.com/questions/16782112/can-pyyaml-dump-dict-items-in-non-alphabetical-order

If you upgrade PyYAML to 5.1 version, now, it supports dump without sorting the keys like this:

    yaml.dump(data, sort_keys=False)
'''