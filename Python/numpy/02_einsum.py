#
# https://numpy.org/devdocs/user/absolute_beginners.html
#
# einsum : https://blog.csdn.net/LoseInVain/article/details/81143966
# einsum more : https://ajcr.net/Basic-guide-to-einsum/
#

# GOAL:
'''
    succesive_distances_gaze = np.einsum(
        "ij,ij->i", undistorted_3d[:-1, :3], undistorted_3d[1:, :3]
    )
    succesive_distances_ref = np.einsum(
        "ij,ij->i", undistorted_3d[:-1, 3:], undistorted_3d[1:, 3:]
    )

    locations_image[:, ::2] *= width
    locations_image[:, 1::2] = (1.0 - locations_image[:, 1::2]) * height
    locations_image.shape = -1, 2
'''
import numpy as np

a = np.array([1, 2, 3, 4, 5, 6])
print(a.shape)
print(a)

row_vector = a[np.newaxis, :]
print(row_vector.shape)
print(row_vector)

col_vector = a[:, np.newaxis]
print(col_vector.shape)
print(col_vector)

# Indexing and slicing
print('Indexing and slicing')
data = np.array([10, 20, 30])
print(data[1])
print(data[0:0])
print(data[0:1])
print(data[0:2])
print(data[0:3])
print('start by \':\'')
print(data[:])
print(data[:1])
print(data[:2])
print(data[:3])
print(data[:-1])
print(data[:-2])
print(data[:-3])
print('end by \':\'')
print(data[0:])
print(data[1:])
print(data[2:])
print(data[3:])
print(data[-3:])
print(data[-2:])
print(data[-1:])

print('2D array')
a = np.array([[1 , 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]])
print(a)
print('row 0:-1, column :3]')
x = a[0:-1, :3]
print(x)
print('row 1:, column :3]')
y = a[1:, :3]
print(y)
s = np.einsum(
        "ij,ij->ij", x, y
    )
print(s)
s = np.einsum(
        "ij,ij->i", x, y
    )
print(s)

print('double \'::\', = step')
s = a[:, ::]
print(s)
s = a[:, ::1]
print(s)
s = a[:, ::2]
print(s)
s = a[:, ::3]
print(s)
s = a[:, 1::2]
print(s)