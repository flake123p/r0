import numpy as np

x = np.arange(5, 10, dtype = np.float32)
print(x)                             # [5. 6. 7. 8. 9.]
print(np.argwhere(x == 777))         # []
print(np.argwhere(x == 7))           # [[2]]
print(np.argwhere(x == 7)[0])        # [2]
print(np.argwhere(x == 7)[0][0])     # 2
print(tuple(np.argwhere(x == 7)[0])) # (2,)





