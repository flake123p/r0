# -*- coding: utf-8 -*-
import math
import numpy as np

a = np.array([1., 2., 3., 4.], dtype = float)
print('org     a =', a)

print('pow 2   a =', np.power(a, 2))

print('pow 0.5 a =', np.power(a, 0.5))

print('sqrt    a =', np.sqrt(a))

print('rsqrt   a =', 1.0/np.sqrt(a))

print('exp     a =', np.exp(a))

print('e pow   a =', np.power(math.e, a))