# -*- coding: utf-8 -*-

import numpy as np

a = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
print(a.shape)
# output: (12,)

a.shape = -1
print(a.shape)
# output: (12,)

a.shape = 6,2
print(a.shape)
# output: (6, 2)

a.shape = -1,2
print(a.shape)
# output: (6, 2)

a.shape = -1,3
print(a.shape)
# output: (4, 3)

a.shape = -1,2,3
print(a.shape)
# output: (2, 2, 3)
