
import numpy as np

A = np.array([(1.1, 3.3), (5.5, 7.7)], dtype = np.float64)

print(A)
print(type(A))
print(A.dtype)

np.save("f64_local", A)
B = np.load("f64_local.npy")

print(B)
print(type(B))
print(B.dtype)


A = np.array([(1.1, 3.3), (5.5, 7.7)], dtype = np.float32)
print(A)
print(type(A))
print(A.dtype)
np.save("f32_local", A)