
# https://stackoverflow.com/questions/13567345/how-to-calculate-the-sum-of-all-columns-of-a-2d-numpy-array-efficiently

import numpy as np

a = np.arange(12).reshape(4,3)
print(a)

b = a.sum(axis=0)
print(b)

c = a.sum(axis=1)
print(c)