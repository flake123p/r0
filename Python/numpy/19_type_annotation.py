import numpy as np
import numpy.typing as npt

def Len(x : npt.NDArray):
    return x.size


x = np.arange(10, dtype = np.float32)

print(Len(x)) # 10 10
