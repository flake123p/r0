# -*- coding: utf-8 -*-

import numpy as np

#
# input is radius, just line java: Math.sin(45), just like C: sin(45)
#
print('np.sin(45) =', np.sin(45)) 

#
# Degree to radius
#
print('np.sin(45) =', np.sin(45* 3.1415926 / 180)) 

#
# Degree to radius, Part 2
#
print('np.sin(45) =', np.sin(45 * np.pi / 180)) 

#
# Degree to radius, Part 3
#
print('np.sin(45) =', np.sin(np.radians(45))) 