import datetime as dt
import numpy as np
import time
import numba

#
# datetime is pointer
#
length = 3
dates = []
t = dt.datetime(2008, 1, 1)
for i in range(length):
    dates.append(t + dt.timedelta(days=i))

a = dates
b = dates
print('a =', a)
print('b =', b)
print('change in b')
b[2] = dt.datetime(2999, 1, 1)
print('a =', a)
print('b =', b)
print('pop in b')
b.pop()
print('a =', a)
print('b =', b)
'''
a = [datetime.datetime(2008, 1, 1, 0, 0), datetime.datetime(2008, 1, 2, 0, 0), datetime.datetime(2008, 1, 3, 0, 0)]
b = [datetime.datetime(2008, 1, 1, 0, 0), datetime.datetime(2008, 1, 2, 0, 0), datetime.datetime(2008, 1, 3, 0, 0)]
change in b
a = [datetime.datetime(2008, 1, 1, 0, 0), datetime.datetime(2008, 1, 2, 0, 0), datetime.datetime(2999, 1, 1, 0, 0)]
b = [datetime.datetime(2008, 1, 1, 0, 0), datetime.datetime(2008, 1, 2, 0, 0), datetime.datetime(2999, 1, 1, 0, 0)]
pop in b
a = [datetime.datetime(2008, 1, 1, 0, 0), datetime.datetime(2008, 1, 2, 0, 0)]
b = [datetime.datetime(2008, 1, 1, 0, 0), datetime.datetime(2008, 1, 2, 0, 0)]
'''

print('\nNumpy Case:')
x = np.arange(5, 10, dtype = np.float32)
a = x
b = x
print('a =', a)
print('b =', b)
print('change in b')
b[2] = 2999
print('a =', a)
print('b =', b)
print('append in b')
b = np.append(b, 777)
print('a =', a)
print('b =', b)
'''
Numpy Case:
a = [5. 6. 7. 8. 9.]
b = [5. 6. 7. 8. 9.]
change in b
a = [   5.    6. 2999.    8.    9.]
b = [   5.    6. 2999.    8.    9.]
append in b
a = [   5.    6. 2999.    8.    9.]
b = [   5.    6. 2999.    8.    9.  777.]
'''