# -*- coding: utf-8 -*-
import math
import numpy as np


print('np.binary_repr(3) =', np.binary_repr(3))
print('np.binary_repr(-3, width=5) =', np.binary_repr(-3, width=5))

print(float.hex(0.25))
print(float.hex(0.33))

import torch
a = torch.tensor([0.33], dtype=torch.float32)
print(float.hex(a.item()))
b = torch.tensor([0.33], dtype=torch.float32)
print(type(float.hex(a.item())))

