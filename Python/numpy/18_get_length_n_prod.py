import numpy as np

x = np.arange(10, dtype = np.float32)
y = np.arange(10, dtype = np.float64)

print(x.size, np.prod(x.shape)) # 10 10
print(y.size, np.prod(y.shape)) # 10 10