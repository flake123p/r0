# -*- coding: utf-8 -*-
import math
import numpy as np

a = np.array([1., 2., 10., np.e], dtype = float)
print('org     a =', a)

print('log     a =', np.log(a), '   !!!!! numpy\'s log() is nature log !!!!!')

print('log10   a =', np.log10(a))

print('log2    a =', np.log2(a))
