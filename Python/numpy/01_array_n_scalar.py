#
# https://stackabuse.com/pythons-classmethod-and-staticmethod-explained/
#
import numpy as np

# fix random
np.random.seed(0)

f16 = np.float16(0.1)
s32 = np.int32(3)
print('f16 = ', f16, ', dtype = ', f16.dtype)
print('s32 = ', s32, ', dtype = ', s32.dtype)
print('f16 is np f16 = ', type(f16) == np.float16)
print('s32 is np s32 = ', type(s32) == np.int32)

a = np.array([1, 2, 3, 4])     # 1D
b = np.array([(2.5, 1, 3, 4.5), (5, 6, 7, 8)], dtype = np.float32)  # 2D
c = np.array([[(2.5, 1, 3, 4.5), (5, 6, 7, 8)], [(2.5, 1, 3, 4.5), (5, 6, 7, 8)]], dtype = np.float32)  # 3D

print('b size = ', b.size)
print('b shape = ', b.shape)
print('b dims = ', len(b.shape))

print('====== a ======')
print(a)
print(a.tolist())
print(a.flatten().tolist())
print(a.ravel().tolist())
print('====== b ======')
print(b)
print(b.tolist())
print(b.flatten().tolist())
print(b.ravel().tolist())
print('====== c ======')
print(c)
print(c.tolist())
print(c.flatten().tolist())
print(c.ravel().tolist())

x = np.zeros((2, 3))               # 建立一個2x3全為0的陣列
print('x = \n', x)
x = np.ones((2, 3, 4))             # 建立一個2x3x4全為1的陣列
print('x = \n', x)
x = np.arange(1, 10, 2)            # 建立一個由1開始，不超過10，間隔值為2的均勻數值陣列
print('x = \n', x)
x = np.linspace(0, 10, 5)          # 建立一個0到10之間，均勻的5個數值陣列
print('x = \n', x)
x = np.full((3,2), 8)              # 建立一個3x2全為8的陣列
print('x = \n', x)
x = np.eye(2)                      # 建立一個5x5的單位矩陣
print('x = \n', x)
x = np.random.random((2,3))        # 建立一個2x3的隨機值矩陣
print('x = \n', x)

x = np.zeros((2, 3)) 
x[0][0] = 1
x[0][1] = 2
x[0][2] = 3
x[1][0] = 4
x[1][1] = 5
x[1][2] = 6
y = np.zeros((2, 3))
x[:1] = y[:1]  # x[0][0~2] = y  ??????????????????????????????????????
#x[1:] = y[1:]  # x[1][0~2] = y
print('x = \n', x)

#
# Fast reserve array - https://stackoverflow.com/questions/3491802/what-is-the-preferred-way-to-preallocate-numpy-arrays
#
x = np.empty(10, dtype = np.float64)
print('empty 10, x = \n', x)
# >>> timeit("np.empty(1000000)",number=1000, globals=globals())
# 0.033749611208094166

# >>> timeit("np.zeros(1000000)",number=1000, globals=globals())
# 0.03421245135849915

# >>> timeit("np.arange(0,1000000,1)",number=1000, globals=globals())
# 1.2212416112155324

# >>> timeit("np.ones(1000000)",number=1000, globals=globals())
# 2.2877375495381145

# >>> timeit("np.linspace(0,1000000,1000000)",number=1000, globals=globals())
# 3.0824269766860652