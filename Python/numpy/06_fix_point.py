# -*- coding: utf-8 -*-

import numpy as np

print("\n ====== DEMO 1: uint32 & truncation ======")
a = np.uint32(1.4)
b = np.uint32(1.5)
print(a, b)
# output: 1 1

print("\n ====== DEMO 2: don't assign np variable directly ======")
print('type of a before assign :', type(a))
# output: type of a before assign : <class 'numpy.uint32'>
a = 0xFFFFFFFF
print('type of a after  assign :', type(a))
# output: type of a after  assign : <class 'int'>

print("\n ====== DEMO 3: 32bit muliplication will OVERFLOW !!! ======")
a = np.uint32(0xFFFFFFFF)
b = np.uint32(2)
c = np.multiply(a, b)
d = np.uint64(np.multiply(a, b))
print(a, b, c, d)
# output: 4294967295 2 4294967294 4294967294

print("\n ====== DEMO 4: fix demo 3, assign any variable to uint64 ======")
a = np.uint32(0xFFFFFFFF)
b = np.uint64(2)
c = np.multiply(a, b)
print(a, b, c)
# output: 4294967295 2 8589934590

print("\n ====== DEMO 5: float32 x float32 v.s. float32 x float64 ======")
a = np.float32(1.4)
b32 = np.float32(1.5)
b64 = np.float64(1.5)
print("32x32, 1.4 x 1.5 =", np.multiply(a, b32))
# output: 2.1
print("32x64, 1.4 x 1.5 =", np.multiply(a, b64)) 
# output: 2.099999964237213

#
# Ref:
# https://numpy.org/doc/stable/reference/generated/numpy.bitwise_and.html
#
print("\n ====== DEMO 6: bitwise operation ======")
a = np.uint32(0x12345678)
print(hex(a))
# output: 0x12345678
a = np.bitwise_and(a, 0xFFFF0000)
print(hex(a))
# output: 0x12340000
a = np.bitwise_or(a, 0x00000056)
print(hex(a))
# output: 0x12340056
a = np.right_shift(a, 4)
print(hex(a))
# output: 0x1234005
a = np.left_shift(a, 4)
print(hex(a))
# output: 0x12340050


