
import numpy as np

# x = np.zeros((2, 3))
# x[1,2] = 9.0
# print('x = \n', x)

# y = x.transpose()
# print('y = \n', y)

# z = np.zeros((2, 3))
# print('array_equal(y, z)= \n', np.array_equal(y, z))

def inplace_transpose_symmetric(target, row, col):
    temp = 0
    for r in range(row):
        for c in range(r+1, col):
            srcIdx = r * col + c
            dstIdx = c * col + r
            temp = target[dstIdx]
            target[dstIdx] = target[srcIdx]
            target[srcIdx] = temp

def inplace_transpose_horizontal(target, row, col):
            # loop_ctr = 0
            done = 0
            main_loop = 1
            c_iter = 0

            while main_loop == 1:
                temp_odd = 0
                temp_even = 0
                init_r = 0
                # init_c = (row * loop_ctr) + 1
                init_c = 1 + c_iter

                temp_odd = target[init_c]; # first

                r = init_r
                c = init_c

                ctr = 0

                while True:
                    if ctr % 2 == 0:
                        # even run
                        dstIdx = c * row + r
                        # print('c = ', c)
                        # print('r = ', r)
                        # print('row = ', row)
                        # print('col = ', col)
                        # print('dstIdx = ', dstIdx)
                        temp_even = target[dstIdx]
                        target[dstIdx] = temp_odd
                    else:
                        # old run
                        dstIdx = c * row + r
                        temp_odd = target[dstIdx]
                        target[dstIdx] = temp_even
                    # update
                    r = dstIdx // col
                    c = dstIdx % col
                    done += 1

                    if done == ( row * col ) - 2:
                        main_loop = 0
                        break

                    if r == init_r and c == init_c:
                        break
                    ctr += 1

                # loop_ctr += 1
                if c_iter:
                    c_iter += row
                else:
                    c_iter = row

def inplace_transpose_vertical(target, row, col):
    temp = 0
    for r in range(row):
        for c in range(r+1, col):
            srcIdx = r * col + c
            dstIdx = c * col + r
            temp = target[dstIdx]
            target[dstIdx] = target[srcIdx]
            target[srcIdx] = temp

def inplace_transpose(target, row, col):
    if row == 1 or col == 1:
        pass
    elif row == col:
        inplace_transpose_symmetric(target, row, col)
    elif row < col:
        inplace_transpose_horizontal(target, row, col)
    else:
        inplace_transpose_vertical(target, row, col)

def square_verify():
    for r in range(4000, 40000):
        c = r

        golden = np.arange(c * r) # 0, 1, 2, ..., c*r-1
        target = np.arange(c * r)

        golden = np.reshape(golden, [r, c])
        golden = golden.transpose()

        inplace_transpose(target, r, c)
        target = np.reshape(target, [c, r])
        # print(golden)
        # print(target)
        equ = np.array_equal(golden, target)
        print('square_verify equ = ', equ, r, c)
        if (equ == False):
            exit()

def horizontal_verify():
    for r in range(1, 4):
        for c in range(r + 1, 20):
            golden = np.arange(c * r) # 0, 1, 2, ..., c*r-1
            target = np.arange(c * r)

            golden = np.reshape(golden, [r, c])
            golden = golden.transpose()

            inplace_transpose(target, r, c)
            target = np.reshape(target, [c, r])
            # print(golden)
            # print(target)
            equ = np.array_equal(golden, target)
            print('square_verify equ = ', equ, r, c)
            # if (equ == False):
            #     exit()

if __name__ == '__main__':
    # square_verify() # 4000
    horizontal_verify()
