import numpy as np

np_f32_eps = np.finfo(np.float32).eps

# a = np.float32(0)

# print(a, type(a), np_f32_eps)

a = np.float32(0.5 + np_f32_eps)
b = np.float32(0.5 + np_f32_eps)
c = a + b

print(f'{c:.50f}')

devi = np_f32_eps * c

print(f'{devi:.50f}')

print(f'{np_f32_eps:.50f}')

print(f'{np_f32_eps*2:.50f}')