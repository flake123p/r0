# -*- coding: utf-8 -*-
import math
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import time

if 0:
    A = torch.tensor([[1., 3.],[5., 7.]])
    print(A.storage())
    B = torch.tensor([[2., 4.],[6., 8.]])
    print(B.storage())
    C = torch.matmul(A,B)
    print(C.storage())
    print(C)

if 1:
    torch.manual_seed(0)
    loops = 20
    input1 = torch.randn(6000*8000, 1)*100
    input2 = torch.randn(8000*6000, 1)*100
    input1 = input1.to(torch.float32).cuda()
    input2 = input2.to(torch.float32).cuda()
    total_s = time.time_ns()

    print('input1 conti =', input1.is_contiguous())
    print('input1 shape =', input1.shape)
    if 1:
        input1 = torch.t(input1)
        print('input1 conti =', input1.is_contiguous())
        print('input1 shape =', input1.shape)
    print('')

    print('input2 conti =', input2.is_contiguous())
    print('input2 shape =', input2.shape)
    if 0:
        input2 = torch.t(input2)
        print('input2 conti =', input2.is_contiguous())
        print('input2 shape =', input2.shape)
    print('')

    matmal_s = time.time_ns()
    for i in range(loops):
        output = torch.matmul(input1, input2)
    output = output.to('cpu')
    print('output conti =', output.is_contiguous())
    end = time.time_ns()
    print('output shape =', output.shape)
    print(output)

    print((end - matmal_s) / (1000000*loops), 'ms of matmul')
    print((end - total_s)  / (1000000*loops), 'ms of total')