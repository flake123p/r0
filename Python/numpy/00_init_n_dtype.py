
import numpy as np

A = np.array([(1, 3), (5, 7)], dtype = np.float64)

print(A)
print(type(A))
print(A.dtype)
'''
[[1. 3.]
 [5. 7.]]
<class 'numpy.ndarray'>
float64
'''

#
# Init empty
#
x = np.zeros((2, 3), dtype = np.float32)  # Tensor 2x3 in float32
print(x.dtype)
'''
Common:
https://numpy.org/devdocs/reference/arrays.scalars.html#numpy.int8
numpy.int8
numpy.int16
numpy.int32
numpy.int64
numpy.uint8
numpy.uint16 
numpy.uint32 
numpy.uint64
numpy.float16
numpy.float32
numpy.float64
'''