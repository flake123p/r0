#
# in brief : please use slicing
#
import numpy as np

# Creating numpy array
arr = np.array([1,2,3,4,5,6,7])

# Removing the FIRST element using delete method
#INDEX of FIRST element is 0.
arr = np.delete(arr, 0)

print(arr)


#
# delete() in not what I want !!! SAD!
#
arr2 = np.array([[1,2],[3,4],[5,6],[7,8]])
arr2 = np.delete(arr2, 0)
print(arr2)

#
# slicing is what I want !!! Noice~~~ ... maybe ...
#
arr2 = np.array([[1,2],[3,4],[5,6],[7,8]])
arr2 = arr2[1:]
print(arr2)
