
import numpy as np

a = [
    [0.9, 3.4, 102.3],
    [0.9, 3.4, 102.3],
    [0.9, 3.4, 102.3],
    [0.9, 3.4, 102.3],
    [0.9, 3.4, 102.9],
]

# to np array
arr = np.array(a)
x=arr[:,0]
y=arr[:,1]
z=arr[:,2]

# sum, these 2 are the same
print(sum(arr))
print([sum(x),sum(y),sum(z)])

def mse(a):
    m = a.mean()
    r = 0
    for i in a:
        r = r + np.square(i-m)
    return r

print('========= mse x,y,z =========')
print(mse(x))
print(mse(y))
print(mse(z))