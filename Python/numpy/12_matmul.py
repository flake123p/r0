# -*- coding: utf-8 -*-
import math
import numpy as np

A = np.array([(1, 3), (5, 7)], dtype = float)
B = np.array([(2, 4), (6, 8)], dtype = float)
C = np.matmul(A,B)
print(C)




if 0:
    input1 = np.float32(np.arange(6*2).reshape(6, 2))
    input2 = np.float32(np.arange(3*6).reshape(6, 3))
    print('input1 conti =', input1.data.contiguous, 'shape =', input1.shape, 'dtype =', input1.dtype)
    print('input2 conti =', input2.data.contiguous, 'shape =', input2.shape, 'dtype =', input2.dtype)


    # total_s = time.time_ns()
    # matmal_s = time.time_ns()
    output = np.matmul(input1.T, input2)

    # end = time.time_ns()

    print('output conti =', output.data.contiguous, 'shape =', output.shape, 'dtype =', input2.dtype)

    # print((end - matmal_s) / 1000000, 'ms of matmul')
    # print((end - total_s) / 1000000, 'ms of total')