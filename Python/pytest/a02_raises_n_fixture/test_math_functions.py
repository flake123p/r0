import pytest

#
# raises() :
#
#   https://doc.pytest.org/en/latest/how-to/assert.html#assertraises
#
def test_divide_by_zero():
    with pytest.raises(ZeroDivisionError):
        1 / 0

def myfunc():
    raise ValueError("Exception 123 raised")

if 1:
    def test_match():
        with pytest.raises(ValueError, match=r".* 123 .*"):
            myfunc()

#
# fixture : share resource
#

def add(a, b):
    return a + b

def subtract(a, b):
    return a - b

# @pytest.fixture
def sample_data():
    return {"a": 2, "b": 3}

def test_without_fixture():
    assert add(sample_data()["a"], sample_data()["b"]) == 5
    assert subtract(sample_data()["a"], sample_data()["b"]) == -1

#
# Fixture = Resource Share, Prepare & Clean Up
#
@pytest.fixture
def sample_data2():
    return {"a": 2, "b": 3}

def test_with_fixture(sample_data2):
    assert add(sample_data2["a"], sample_data2["b"]) == 5
    assert subtract(sample_data2["a"], sample_data2["b"]) == -1

class create_connection:
    def __init__(self):
        print('Connection Created')

    def query(self, msg):
        print('Query:', msg)
        return 0

    def close(self):
        print('Connection Closed')

#
# With yield : pytest will go till StopIteration !!
#
@pytest.fixture
def db_connection():
    connection = create_connection()
    yield connection
    connection.close()

def test_query(db_connection):
    assert db_connection.query("SELECT * FROM table") is not None

#
# Without yield and fixture
#
def db_connection2(msg):
    connection = create_connection()
    ret = connection.query(msg)
    connection.close()
    return ret

def test_query2():
    assert db_connection2("SELECT * FROM table") is not None