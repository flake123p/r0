# import pytest

def add(x, y):
    return x + y

def test_add():
    assert add(1, 2) == 3, "aa"
    assert add(-1, 1) == 0, "bb"
    assert add(-1, -1) == -1, "cc"


#
# 1. pip install pytest
#
# 2. pytest 00_basic.py
#