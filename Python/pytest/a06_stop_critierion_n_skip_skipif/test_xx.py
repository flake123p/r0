import pytest

def some_version():
    return 3.3

def test_mul_1():
    assert True

@pytest.mark.skip('my skip message')
def test_mul_2():
    assert False

@pytest.mark.skipif(some_version() >= 3.0, reason=f'skip because of some version ({some_version()})')
def test_mul_3():
    assert False

def test_mul_4():
    assert 2 > 3

def test_mul_5():
    assert False

def test_mul_6():
    assert False

def test_mul_7():
    assert False