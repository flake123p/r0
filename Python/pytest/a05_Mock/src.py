from datetime import datetime

def get_current_month() -> int:
    today = datetime.now()

    month = today.month
    if month < 0:
        raise Exception("Wrong month")

    return today.month


import time

def sleep_for_a_while(seconds: int) -> int:
    time.sleep(seconds)
    return seconds


#
#
#
# def external_api():
#     return 1

# def is_positive(n):
#     if n > 0:
#         return True
#     else:
#         return False