# Ref: https://minglunwu.com/notes/2023/pytest_101_2.html/

from unittest.mock import patch
from src import get_current_month
from datetime import datetime
import pytest

#
# Replace return value
#
def test_get_current_month():
    from unittest.mock import patch
    with patch("src.datetime") as mock_datetime: 

        mock_datetime.now.return_value = datetime(2023, 3, 1, 0, 0, 0)

        month = get_current_month()
        assert month == 3



#
# Replace function
#
from src import sleep_for_a_while

#
# too long
#
# def test_sleep_for_a_while():
#     response = sleep_for_a_while(2)
#     assert response==2


def test_sleep_for_a_while_mock(): # patch time.sleep() to None
    with patch("src.time.sleep"): 
        response = sleep_for_a_while(2)
        assert response==2

def test_sleep_for_a_while_replace(): # patch time.sleep() to time.sleep(0.1)
    import time
    with patch("src.time.sleep", new_callable=time.sleep(0.1)):
        response = sleep_for_a_while(20)
        assert response==20



#
# Replace my function
#
from src2 import is_external_positive

def test_ext_posi_0():
    assert is_external_positive() == True

def test_ext_posi_1():
    with patch("src2.external_api") as external_stub:
        external_stub.return_value = 1
        assert is_external_positive() == True

def test_ext_posi_2():
    with patch("src2.external_api") as external_stub:
        external_stub.return_value = -1
        assert is_external_positive() == False

#
# pytest Mock
#
from pytest_mock import MockFixture

def test_ext_posi__PytestMock(mocker: MockFixture):
    mock_api_2 = mocker.patch("src2.external_api", return_value=999)
    assert is_external_positive() == True

def test_ext_posi__PytestMock2(mocker: MockFixture):
    mock_api_2 = mocker.patch("src2.external_api", return_value=-999)
    assert is_external_positive() == False