#
# https://stackoverflow.com/questions/24617397/how-do-i-print-to-console-in-pytest
#
#   pytest --capture=no     # show print statements in console
#   pytest -s               # equivalent to previous command
#

pytest -s
