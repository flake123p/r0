import pytest
from MyMath import *

@pytest.mark.parametrize("a, b, expected", [
    (6, 3, 2),
    (10, 2, 5),
    (5, 2, 2.5),
])
def test_divide(a, b, expected):
    assert divide(a, b) == expected

#
# Unmark to reach 100% coverage
#
# @pytest.mark.parametrize("a, b", [
#     (5, 0),
#     (-3, 0)
# ])
# def test_divide_by_zero(a, b):
#     with pytest.raises(ValueError, match="Cannot divide by zero!"):
#         divide(a, b)