import pytest
import sys
sys.path.append('../a04_0_coverage_n_ini_addopts/')
import MyMath

@pytest.mark.parametrize("a, b, expected", [
    (6, 3, 2),
    (10, 2, 5),
    (5, 2, 2.5),
])
def test_divide(a, b, expected):
    assert MyMath.divide(a, b) == expected

#
# Unmark to reach 100% coverage
#
@pytest.mark.parametrize("a, b", [
    (5, 0),
    (-3, 0)
])
def test_divide_by_zero(a, b):
    with pytest.raises(ValueError, match="Cannot divide by zero!"):
        MyMath.divide(a, b)