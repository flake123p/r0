import pytest
# test_math_functions.py
from math_functions import add, subtract

@pytest.mark.math1
def test_add():
    assert add(2, 3) == 5
    assert add(-1, 1) == 0
    assert add(0, 0) == 0
    # assert add(0, 1) == 0 # Flake: this is error

@pytest.mark.math2
def test_subtract():
    assert subtract(5, 3) == 2
    assert subtract(0, 3) == -3
    assert subtract(10, 10) == 0
