
# 1. A working example:
Project Structure:
```
r0/Python/_my_pkgs/
r0/Python/_my_pkgs/MyScripts/
r0/Python/_my_pkgs/MyScripts/__init__.py
r0/Python/_my_pkgs/MyScripts/my_hello_world.py
r0/Python/_my_pkgs/setup.py
```


# 2. setup.py (Keyword: entry_points)
```python
import setuptools

if __name__ == "__main__":
    setuptools.setup(     
        name="MyPkgs",
        version="0.0.1",
        python_requires=">=3.9",
        packages=setuptools.find_packages(),

        entry_points = {
            'console_scripts': [
                'my_hello_world = MyScripts.my_hello_world:console_main',
                'my_goodbye     = MyScripts.my_hello_world:console_main',
            ],              
        },
    )
```
Command Name = my_hello_world & my_goodbye


# 3. my_hello_world.py (Keyword: console_main)
```python
import sys

def my_hello_world(to_someone : str = 'None'):
    print('My Hello World to', to_someone)

def my_bye_bye():
    print('Bye~~')

def console_main():
    # print("Raw arguments:", sys.argv)
    if sys.argv[0].split('/')[-1] == 'my_goodbye':
        my_bye_bye()
    else:
        if len(sys.argv) > 1:
            my_hello_world(sys.argv[1])
        else:
            my_hello_world()
```

