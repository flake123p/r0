
```markdown
my_packages/
├── my_mod_A/
│   ├── __init__.py
│   ├── moduleA.py
│   ├── submodule/
│   │   ├── __init__.py
│   │   ├── module_sub.py
├── my_mod_B/
│   ├── __init__.py
│   ├── moduleB.py
├── tests/
│   ├── __init__.py
│   ├── test_module.py
├── setup.py
```
 