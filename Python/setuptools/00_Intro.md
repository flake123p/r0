From ChatGPT

# 1. Install setuptools
If you don’t have setuptools installed, you can install it using pip:

    pip install setuptools


# 2. Create a Project Directory
```bash

my_project/
├── my_package/
│   ├── __init__.py
│   └── module.py
├── setup.py
└── README.md
```


# 3. Write setup.py
setup.py defines the package configuration:

```python

from setuptools import setup, find_packages

setup(
    name="my_package",  # Package name
    version="0.1.0",  # Version number
    author="Your Name",  # Author name
    author_email="your.email@example.com",  # Author email
    description="A simple example package",  # Short description
    long_description=open("README.md").read(),  # Read README.md for long description
    long_description_content_type="text/markdown",
    url="https://github.com/yourname/my_package",  # Project URL
    packages=find_packages(),  # Automatically find all Python packages
    classifiers=[  # Metadata tags
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",  # Minimum required Python version
    install_requires=[  # Dependencies
        "requests",
        "numpy",
    ],
)
```


# 3.5 Simple version
```python

from setuptools import setup, find_packages

setup(
    name="my_package",
    version="0.1.0",
    author="Your Name",
    packages=find_packages(),
    python_requires=">=3.6",
)
```


# 4. Install the Package
Run the following command in the directory containing setup.py:
```bash
pip install .
```
This will install my_package in your local Python environment.

For development mode installation (which allows changes to be immediately reflected), use:

```bash
pip install -e .
```


# 5. Build the Package
To prepare for publishing to PyPI (Python Package Index), first build the package:
```bash
pip install wheel
```
python setup.py sdist bdist_wheel
This generates .tar.gz and .whl files in the dist/ directory.


# 6. Upload to PyPI
First, install twine:
```bash
pip install twine
```

Then upload the package:
```bash
twine upload dist/*
```

You will be prompted to enter your PyPI username and password. After uploading, you can install the package using:
```bash
pip install my_package
```

---

This is a basic guide to using setuptools. There are also advanced features such as:
1. Specifying entry points for creating executable commands
2. Using setup.cfg for configuration
3. Replacing setup.py with pyproject.toml
Let me know if you need more details! 😊