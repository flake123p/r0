from ctypes import *

i = c_int(42)

print(i)
# c_int(42)

print(i.value)
# 42

i.value = -99

print(i)
# c_int(-99)

print(i.value)
# -99