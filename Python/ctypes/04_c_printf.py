import ctypes
from ctypes.util import find_library

libc = ctypes.CDLL(find_library("c")) # 'libc.so.6'
libc.printf.argtypes = [ctypes.c_char_p]
libc.printf(b"Hello, %s %d!\n", b"World", 99)

print(b"Hello, %s %d!\n", b"World", 99)
