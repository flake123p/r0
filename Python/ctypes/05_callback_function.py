import ctypes

def py_callback(n):
    print('PY Callback: n =', n)

_cb = ctypes.CFUNCTYPE(None, ctypes.c_int)
py_cb = _cb(py_callback)


my = ctypes.CDLL('./lib_src/lib_local/libmylib.so')
c_container = my.my_container
c_container.argtypes = [ctypes.c_void_p, ctypes.c_int]
c_container.restype = ctypes.c_int

c_cb = my.my_callback
c_cb.argtypes = [ctypes.c_int]
c_cb.restype = ctypes.c_int

print('====== demo 1 : Python -> C -> Python CB')
for i in range(3):
    c_container(py_cb, i)

print('====== demo 2 : Python -> C -> C CB')
for i in range(3):
    c_container(c_cb, i)