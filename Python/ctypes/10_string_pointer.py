from ctypes import *

s = b"Hello, World"

c_s = c_char_p(s)

print(c_s.value)
# b'Hello, World'

print(c_s)
# c_char_p(140494369176272)

c_s.value = b"Hi, there"

print(c_s.value)
# b'Hi, there'

print(c_s)
# c_char_p(140494369176752)

print(s)
# b'Hello, World'