from ctypes import *

my = CDLL('./lib_src/lib_local/libmylib.so')
my.my_string_get.argtypes = []
my.my_string_get.restype = c_void_p
my.my_string_free.argtypes = c_void_p,
my.my_string_free.restype = None

ptr = my.my_string_get()
print(ptr)
print(hex(ptr))
print(cast(ptr, c_char_p).value)
my.my_string_free(ptr)