# 1. cd lib_src
# 2. mkdir
# 3. cmake ..
# 4. make
# 5. make install

mkdir build -p
cd build
rm * -rf
cmake ..
make -j10
make install