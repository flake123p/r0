#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "mylib.h"

int32_t my_increase(int32_t x)
{
    return x + 1;
}

char* my_string(char *str)
{
    printf("%s\n", str);
    str[0] = 'A';
    return str;
}

int my_callback(int n)
{
    printf("C Callback: n = %d\n", n);
    return n;
}

int my_container(int (*cb)(int), int n)
{
    printf("C Container:\n");
    return cb(n);
}

char *my_string_get(void)
{
    char *buf = "Hello World";
    char *new_buf = strdup(buf);
    printf("allocated address: %p\n", new_buf);
    return new_buf;
}

void my_string_free(char *ptr)
{
    printf("freeing address: %p\n", ptr);
    free(ptr);
}