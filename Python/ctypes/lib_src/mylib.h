#include <stdint.h>

int32_t my_increase(int32_t x);
char* my_string(char *str);
int my_callback(int n);
int my_container(int (*cb)(int), int n);
char *my_string_get(void);
void my_string_free(char *ptr);