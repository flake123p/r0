from ctypes import *

i = c_int()

f = c_float()

s = create_string_buffer(b'\000' * 32)

print(i.value, f.value, repr(s.value))
# 0 0.0 b''

libc = CDLL("libc.so.6")

libc.sscanf(b"1 3.14 Hello", b"%d %f %s", byref(i), byref(f), s)
# 3

print(i.value, f.value, repr(s.value))
# 1 3.140000104904175 b'Hello'