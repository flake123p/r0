#
# c_int c_bool c_double
# 

import ctypes

print(type(ctypes.c_int))
print(type(ctypes.c_int8))
print(type(ctypes.c_int32))
print(type(ctypes.c_bool))
print(type(ctypes.c_double))

from multiprocessing import (
    Value,
)

timebase = Value(ctypes.c_double, 0.123)
print(timebase)
print(type(timebase))