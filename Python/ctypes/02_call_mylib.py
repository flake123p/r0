from ctypes import *

my = CDLL('./lib_src/lib_local/libmylib.so')

print('====== Demo 1 , int call ======')
inc = my.my_increase
inc.argtypes = [c_int32]
inc.restype = c_int32
ret = inc(33)
print('ret =', ret)
print(type(ret))


print('====== Demo 2 , c string call ======')
mys = my.my_string
mys.argtypes = [POINTER(c_char)]
mys.restype = c_char_p
res = mys(b"hello ctypes")

print(res)
print('to python string :', res.decode()) # "utf-8" is default
print('to python string :', res.decode("utf-8"))


print('====== Demo 3 , py string to c string ======')
py_string = res.decode()

print(bytes(py_string, 'utf-8'))
print(py_string.encode('utf-8'))


print('====== Demo 4 , with bytearray ======')
x = bytearray('111', 'utf-8')
print(x)
res = mys(bytes(x))
print(res)