from ctypes.util import find_library

print(find_library("m")) # 'libm.so.6'

print(find_library("c")) # 'libc.so.6'

print(find_library("bz2")) # 'libbz2.so.1.0'