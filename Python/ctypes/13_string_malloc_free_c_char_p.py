from ctypes import *

class c_char_p_sub(c_char_p):
    pass

my = CDLL('./lib_src/lib_local/libmylib.so')
my.my_string_get.argtypes = []
my.my_string_get.restype = c_char_p_sub
my.my_string_free.argtypes = c_char_p_sub,
my.my_string_free.restype = None

ptr = my.my_string_get()
print(ptr)
print(ptr.value)
my.my_string_free(ptr)