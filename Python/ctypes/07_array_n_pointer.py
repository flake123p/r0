from ctypes import *

i = c_int(42)

p = pointer(i)

print(p)
# <__main__.LP_c_int object at 0x7f260cdff268>

print(p.contents)
# c_int(42)

i = c_int(99)

p.contents = i

print( p.contents, 'pointer to i') # Pointer to i
# c_int(99)

print(i, 'i itself')
# c_int(99)

p[0] = 22 # Change i by pointer array indexing

print(i, 'i itself')
# c_int(22)