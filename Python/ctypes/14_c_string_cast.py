from ctypes import *


s = b"hello\x00world"  # create a string containing null bytes

sz = len(s)
print(sz)
# 11

p = c_char_p(s)  # obtain a pointer of various types
# by default it is interpreted as null-terminated
print(p.value)
# b'hello'

#
# various methods of explicitly specifying the full length
#
p2 = cast(p, POINTER(c_char))
print(p2[:sz])
# b'hello\x00world

print(string_at(p, size=sz))
# b'hello\x00world

address = cast(p, c_void_p).value
print((c_char * sz).from_address(address).raw)
# b'hello\x00world