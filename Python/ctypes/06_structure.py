from ctypes import *

class Point(Structure):
    __fields__ = [
        ("x", c_int),
        ("y", c_int),
    ]

    def __str__(self):
        return "x={0.x}, y={0.y}".format(self)

class Rect(Structure):
    __fields__ = [
        ("upperleft", Point),
        ("lowerright", Point),
    ]

    def __str__(self):
        return "upperleft:[{0.upperleft}], lowerright：[{0.lowerright}]".format(self)

point = Point(x=10, y=20)
print("point1:", point)

rect = Rect(upperleft=Point(x=1, y=2), lowerright=Point(x=3, y=4))
print("rect1:", rect)