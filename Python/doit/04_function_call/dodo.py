
def hello():
    print("Hello from Python!")

def task_hello():
    return {
        'actions': [hello],
        'verbosity': 2,
    }
