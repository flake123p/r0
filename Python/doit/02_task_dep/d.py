
def task_second():
    return {
        'actions': ['echo "Second task after first!"'],
        'task_dep': ['first']
    }

def task_first():
    return {
        'actions': ['echo "First task done!"'],
        'file_dep': [],  # No file dependencies
        'targets': ['first_done.txt']
    }