
def hello():
    print("Hello from Python!")

def task_hello():
    return {
        'actions': ['echo "Hello, World!"']
    }

def task_check_output_txt():
    return {
        'actions': ['echo 123>output.log'],
        'file_dep': ['d.py'],
        'targets': ['output.log']
    }