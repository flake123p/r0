import sys
sys.path.append('MyUtils/')
import ast_op, mys_op, fs

def mys_init():
    total = list()

    with open('setup_mys.log', 'w') as log:
        folder = 'MyUtils'
        my_ut_python_files = fs.list_files(folder, '.py')
        # print(my_ut_python_files)
        for f in my_ut_python_files:
            f = fs.join(folder, f)
            func_list = ast_op.ast_parse_file(f)
            prev_line = ast_op.ast_get_prev_line(f, func_list)
            entries = mys_op.mys_gen_entries(func_list, prev_line)
            # print(entries)
            
            total += entries
        
        folder = 'MyUtils/prj'
        my_ut_python_files = fs.list_files(folder, '.py')
        # print(my_ut_python_files)
        for f in my_ut_python_files:
            f = fs.join(folder, f)
            func_list = ast_op.ast_parse_file(f)
            prev_line = ast_op.ast_get_prev_line(f, func_list)
            entries = mys_op.mys_gen_entries(func_list, prev_line)
            # print(entries)
            
            total += entries
        
        # print(total)
        for t in total:
            log.write(t + '\n')
    
    with open('MyScripts/mys_auto.py', 'w') as agf: # auto generated file
        agf.write('from MyUtils import *\n\n')

        agf.write('entries_auto = {\n')
        for t in total:
            agf.write('    ' + t + '\n')
        agf.write('}\n')

if __name__ == "__main__":
    mys_init()


