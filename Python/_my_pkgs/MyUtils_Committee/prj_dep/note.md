# prj.dep.yaml
```yml
#
# Implicit Target: ALL
#
Deps:
  "123":
    # Timestamp Prerequisites
    # :Optional[str, list[str]]
    TS_Prereq : null

    # Target Prerequisites (Execute Targets First)
    # :Optional[str, list[str]]
    TGT_Prereq : null

    # Console Action (Will run before Python Action)
    # :str
    Console_Action : echo "echo 123" from prj dep
    # Python Action (Will run after Concole Action)
    # :str
    Python_Action : null
    TGT_Action : null
    Group_Action : null

    # :str
    Console_FalseAction : echo "echo false 123" from prj dep
    # :str
    Python_FalseAction : null
    TGT_FalseAction : null
    Group_FalseAction : null

    # :str
    Group: AAA
    # :Optional[str, list[str]]
    Group_Prereq : null
```

# v2.0 Draft

## logging log

## Syntax
```yml
# Location of dep yamls, implicit default value: current folder ./
Dep_Folders : null

# Location of dep yamls, using prj target as a path
Dep_Prj_Folders : null

# Is target a file or folder, default: false.
# :bool
Phony : true

# When doing execute "ALL_FORCE", prevent it from execution this target dep check. Default: false
# :bool
Block_The_ALL_FORCE_Execution : false

# Register Self to Targets, default: ALL
# :Optional[str, list[str]]
TG_Register : ALL
```