import os
import json
import MyUtils as ut
from MyTester.Tester import Tester

class GitUtils:
    def __init__(self, ws : str):
        assert isinstance(ws, str)
        assert ut.exists(ws) == True
        self.ws = ws

    def flush(self):
        if __debug__:
            print('[GitUtils] : flush()')
        ut.run_cmd('cd ' + self.ws + ' ; git stash ; git stash drop')

    def flush_to_latest(self):
        if __debug__:
            print('[GitUtils] : flush_to_latest()')
        ut.run_cmd('cd ' + self.ws + ' ; git stash ; git stash drop ; git checkout main ; git pull')

    def check_is_modified(self, output_of_git_show : list[str], posible_extension : list[str]):
        find_word = False
        for each_line in output_of_git_show:
            seach_line = each_line.split()
            for each_word in seach_line:
                if(each_word == '+++' or each_word == '---'):
                    find_word = True
                elif(find_word == True):
                    log_word = each_word.split('/')[-1]
                    log_word_extension = log_word.rsplit('.', 1)[-1]
                    if log_word_extension in posible_extension:
                        # self.add_log('Do test by : ' + each_line)
                        return True # build_this_commit
                    if log_word == 'CMakeLists.txt':
                        # self.add_log('Do test by : ' + each_line)
                        return True # build_this_commit
                    find_word = False
        return False

    def check_is_modified_cpp_python(self, commit_hash : str):
        output_of_git_show = ut.run_cmd_to_buf(['git', '-C', self.ws, 'show', commit_hash])

        ret = self.check_is_modified(
            output_of_git_show, 
            posible_extension=['c', 'cc', 'cxx', 'cpp', 'h', 'hpp', 'py'])
        
        if __debug__:
            print('[GitUtils] : check_is_modified_cpp_python(), ret =', ret)
        return ret
    
    def git_checkout(self, commit, checkout_log1 = None, checkout_log2 = '/dev/null'):
        if __debug__:
            print('[GitUtils] : git_checkout(), ', checkout_log1, ',', checkout_log2)
        cmd = 'cd ' + self.ws + '; git checkout ' + commit
        if checkout_log1 != None:
            cmd += '>' + checkout_log1 + ' 2>' + checkout_log2
        ut.run_cmd(cmd)

    def git_log_dump(self, dump_file):
        if __debug__:
            print('[GitUtils] : git_log_dump()')
        ut.run_cmd('cd ' + self.ws + ' && git log>' + dump_file)
    
    def git_commit_push(self, dst_folder : str, commit_msg : str):
        if __debug__:
            print(f'[GitUtils] : git_commit_push(), dst = {dst_folder}, msg = {commit_msg}')
        cmd = f'cd {dst_folder} ; git add . ; git commit -m {commit_msg} ; git fetch origin ; git merge -m MERGE ; git push'
        ut.run_cmd(cmd)
    
    def git_log_to_dlist(self, git_log_path):
        if __debug__:
            print('[GitUtils] : git_log_to_dlist()')
        dlist = []
        capture_log = False
        with open(git_log_path, 'r') as f:
            cd = dict()
            for line in f:
                s = line.split()
                if len(s) == 0:
                    continue
                if s[0] == 'commit' and len(s) >= 2:
                    if len(cd) != 0:
                        dlist.append(cd)
                        cd = dict()
                    cd['commit'] = s[1]
                    cd['log'] = 'NULL'
                elif s[0] == 'Author:' and len(s) >= 2:
                    cd['Author'] = line
                elif s[0] == 'Date:' and len(s) >= 2:
                    cd['Date'] = line
                    capture_log = True
                    continue
                elif capture_log:
                    capture_log = False
                    cd['log'] = line
        if len(cd) != 0:
            dlist.append(cd)
        #
        # Add revision
        #
        total = len(dlist)
        curRev = 0
        curIdx = total - 1
        for _ in range(total):
            dlist[curIdx]["Rev"] = curRev
            curRev += 1
            curIdx -= 1

        return dlist

class GitTester(GitUtils):
    def __init__(self, ws, db, PARAM) -> None:
        if __debug__:
            ut.SHOW(ws)
            ut.SHOW(db)
        super().__init__(ws)
        self.db = db
        self.PARAM = PARAM

        assert ut.exists(ws) == True, f'ws doesn\'t exist : {ws}'
        
        ut.mkdir(db)

        TESTED_FILE   = ut.join(self.db, PARAM['TESTED_COMMIT'])
        GIT_LOG_FILE  = ut.join(self.db, "01_git_log.txt")
        GIT_LOG_JSON  = ut.join(self.db, "02_git_log_json.txt")
        UNTESTED_FILE = ut.join(self.db, "03_untested_commits.txt")

        #
        # 1. Get all git log (via cmd 'git log')
        #
        if __debug__:
            print('\nGet all git log ...')
        self.flush_to_latest()
        self.git_log_dump(GIT_LOG_FILE)

        #
        # 2. Covert git log to json with rev. num
        #
        all_commits = self.git_log_to_dlist(GIT_LOG_FILE)
        ut.json_dump(all_commits, GIT_LOG_JSON)

        #
        # 3. Load tested commits
        #
        if ut.exists(TESTED_FILE):
            tested = ut.json_load(TESTED_FILE)
        else:
            assert len(all_commits) > 0
            tested = [all_commits[0]]
            ut.json_dump(tested, TESTED_FILE)
        
        #
        # 4. Extract untested commits.
        #
        _untested = self.extract_untested(all_commits, tested)
        _untested.reverse()

        #
        # 5. Filter out TEST_REPORT commits.
        #
        self.untested = []
        for u in _untested:
            s = u['log'].split()
            if len(s) > 0:
                if s[0] == '[TEST_REPORT]':
                    continue
            self.untested.append(u)
        ut.json_dump(self.untested, UNTESTED_FILE)

    def extract_untested(self, all, tested):
        untested = []
        if len(tested):
            firstTested = tested[0]["commit"]
            
            self.CUTList = []
            for d in all:
                if d["commit"] == firstTested:
                    break
                else:
                    untested.append(d)
        return untested

    def run_all_tests(self, tester : Tester) -> None:
        if len(self.untested) == 0:
            if __debug__:
                print()
                print('No commits for test ... Exit!')
                print()
            return

        #
        # Get latest report, append it. Save it if necessary.
        #
        if __debug__:
            print('\nGet latest report ...')
        self.flush_to_latest()
        
        if ut.exists(self.PARAM['FINAL_REPORT']): # pragma: no cover
            try:
                self.latest_report = ut.json_load(self.PARAM['FINAL_REPORT'])
            except json.JSONDecodeError as err:
                print('FINAL_REPORT format error:')
                print('Exception -', err.__class__.__name__, ':', err)
                self.latest_report = []
        else: # pragma: no cover
            self.latest_report = []

        #
        # Run all tests in every untested commits
        #
        for curr_commit in self.untested:
            self.run_single(curr_commit, tester)

        if __debug__:
            print('\nAll tests are completed ...')
        self.flush_to_latest()
        self.commit_report_to_ws_folder()

    def run_single(self, target_commit, tester : Tester):
        #
        # Prepare
        #
        LOG_FOLDER = ut.join(self.PARAM['LOG_FOLDER'], '{:06d}'.format(target_commit['Rev']))

        if __debug__:
            print('\nSTART TEST:')
            ut.SHOW(LOG_FOLDER)
            print('target_commit =')
            print(target_commit)
            
        is_skip = False
        is_pass = False
        
        ut.mkdir(LOG_FOLDER)
        ut.json_dump(target_commit, ut.join(LOG_FOLDER, 'commit.txt'))

        # 1. Create empty single report
        curReport = dict()
        curReport['Job'] = target_commit
        curReport['Result'] = ''

        # 2. Skip check
        file_changed = self.check_is_modified_cpp_python(commit_hash=target_commit["commit"])

        # 3. Start Tester and save to single report (if not skipped)
        if file_changed:
            self.flush()
            self.git_checkout(
                target_commit["commit"],
                ut.join(LOG_FOLDER, self.PARAM['GIT_CHECKOUT_LOG_1']),
                ut.join(LOG_FOLDER, self.PARAM['GIT_CHECKOUT_LOG_2']))
            is_pass = tester.start_tests(LOG_FOLDER)
            curReport['Result'] = tester.test_result
        else:
            is_skip = True
            curReport['Result'] = '[SKIPPED]'
        ut.json_dump(curReport, ut.join(LOG_FOLDER, 'result.txt'))
        if __debug__:
            ut.SHOW(RESULT := curReport['Result'])
            print()
        
        #
        # 4. Append reports at the top of the file.
        #
        self.latest_report.reverse()
        self.latest_report.append(curReport)
        self.latest_report.reverse()
        ut.json_dump(self.latest_report, self.PARAM['FINAL_REPORT_LOCAL'])

        if self.PARAM['AUTO_UPDATE_TESTED']:
            ut.json_dump([target_commit], self.PARAM['TESTED_COMMIT'])

        return is_skip, is_pass

    def commit_report_to_ws_folder(self):
        if self.PARAM['AUTO_COMMIT_TEST_REPORT']:
            if __debug__:
                print('\nCommit report to WS ...')

            # self.flush_to_latest()

            ut.mkdir(self.PARAM['FINAL_REPORT_DIR'])
            
            cmd = 'cp ' + self.PARAM['FINAL_REPORT_LOCAL'] + ' ' + self.PARAM['FINAL_REPORT_DIR']
            ut.run_cmd(cmd)

            self.git_commit_push(self.PARAM['FINAL_REPORT_DIR'], '[TEST_REPORT]')
