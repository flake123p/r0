from abc import ABC, abstractmethod
import os
import json
import MyUtils as ut
from typing_extensions import Self

class TesterException(Exception):
    pass

class Tester():
    def __init__(self, db=None, auto_tagging=False, verbose=False):
        self.db = db # db = default log folder
        if self.db != None:
            ut.mkdir(db)
        self.run_log = None
        self.f = None
        self.test_result = [] # list of string
        self.is_failed = False
        self.log_folder = None
        self.auto_tagging = auto_tagging
        self.auto_tagging_width = 3 # zero padding decimal
        self.auto_tagging_index = 0
        self.verbose = verbose
    
    def ImportConfig(self, another: Self, tag_idx=None) -> Self:
        self.db = another.db
        self.auto_tagging = another.auto_tagging
        self.auto_tagging_width = another.auto_tagging_width
        if tag_idx == None:
            self.auto_tagging_index = another.auto_tagging_index
        else:
            self.auto_tagging_index = tag_idx
        self.verbose = another.verbose
        return self
    
    def ImportUserHandle(self, hdl):
        self.hdl = hdl
        
    def __del__(self):
        if self.f != None:
            self.f.close()
        
    def add_log(self, _str):
        assert self.f != None
        self.f.write('  ' + _str + '\n')

    def run_cmd(self, cmd):
        self.add_log('[CMD] ' + cmd)
        # print('[CMD] ' + cmd)
        return ut.run_cmd(cmd)

    # @abstractmethod
    def run_tests_impl(self) -> bool: # return is_pass
        return False

    def start_tests(self, log_folder: str = None, time_prof: bool = False, fail_exit: bool = False) -> bool:
        self.is_failed = False
        self.test_result = [] # list of string
        if log_folder == None:
            self.log_folder = self.db
        else:
            self.log_folder = log_folder
        assert isinstance(self.log_folder, str) == True 
        ut.mkdir(self.log_folder)
        self.run_log = ut.join(self.log_folder, 'Tester_RunLog_local.log')
        self.f = open(self.run_log, "w")
        self.add_log('[CMD] mkdir -p ' + self.log_folder)
        
        if time_prof:
            p = ut.PM()
        ret = self.run_tests_impl()
        if time_prof:
            p.show()
        
        if fail_exit and ret == False:
            raise TesterException('[Failed] Test Failed')
        return ret
    
    #
    # Call another's start_tests(), and append results.
    #
    def start_tests_agent(
        self, 
        another: Self,
        log_folder: str = None,
        time_prof: bool = False,
        fail_exit: bool = False
    ) -> bool:
        is_pass = another.start_tests(log_folder, time_prof)

        self.test_result += another.test_result
        if another.is_failed == True:
            self.is_failed = True

        if fail_exit and is_pass == False:
            raise TesterException('[Failed] Test Failed')
        return is_pass

    def append_test_result(self, is_pass, result):
        if is_pass == False:
            self.is_failed = True
            curr_result = '[FAILED] ' + result
        else:
            curr_result = '[PASS]   ' + result
        self.test_result.append(curr_result)
        print(curr_result)

    def pass_if_pattern_number_correct(self, file, pattern, num):
        return ut.Check_NumOfString(file, pattern) == num

    def pass_if_no_python_error(self, file):
        return ut.Check_IsAnyPythonError(file) == False

    def pass_if_no_failed(self, file):
        return ut.Check_IsAnyFailed(file) == False
       
    def pass_if_no_string(self, file):
        return ut.Check_IsAnyString(file) == False

    def pass_if_no_stderr_error(self, file):
        return ut.Check_IsAnyStderrError(file) == False
    
    def gen_test_log_file_name(self, prefix) -> tuple[str, str]:
        file1 = prefix + '.txt'
        file2 = prefix + '_Err' + '.txt'
        if self.auto_tagging:
            file1 = f'{self.auto_tagging_index:0{self.auto_tagging_width}}_' + file1
            file2 = f'{self.auto_tagging_index:0{self.auto_tagging_width}}_' + file2
            self.auto_tagging_index += 1
        return file1, file2
    
    def gen_redirect_string(self, file1, file2) -> str:
        if self.verbose:
            return f' 2> {file2} | tee {file1}'
        else:
            return f' > {file1} 2> {file2}'

    def test_template(
        self,
        test_name: str,
        command: str,
        pass_func_list: list = [],
        pass_func_list2: list = []
    ) -> bool:
        if __debug__:
            print('\nSTARTING TEST : ' + test_name + ' ...')

        file1, file2 = self.gen_test_log_file_name(test_name)

        file1 = ut.join(self.log_folder, file1)
        file2 = ut.join(self.log_folder, file2)

        cmd = command + self.gen_redirect_string(file1, file2)
        self.run_cmd(cmd)

        is_pass = True
        for func in pass_func_list:
            if not func(file1):
                is_pass = False

        if is_pass:
            for func in pass_func_list2:
                if not func(file2):
                    is_pass = False

        self.append_test_result(is_pass, test_name)

        return is_pass

    def test_template_pattern_num_match(
        self,
        test_name: str,
        command: str,
        pattern: str,
        num: int = 1,
        pass_func_list: list = [],
        pass_func_list2: list = []
    ) -> bool:
        pat_x_n = lambda file: self.pass_if_pattern_number_correct(
            file, pattern, num)
        func_list1 = [pat_x_n] + pass_func_list
        return self.test_template(
            test_name,
            command,
            pass_func_list=func_list1,
            pass_func_list2=pass_func_list2
        )

class TesterMP(Tester):
    def ImportMP(self, another: Self, TesterClass: Tester = Tester, tag_func = None) -> Self:
        self.mp_prepare(another.mp_num, another.split_num, TesterClass, tag_func=tag_func)
        return self

    def mp_prepare(self, mp_num, split_num, TesterClass: Tester = Tester, tag_func = None):
        self.mp_num = mp_num
        self.split_num = split_num
        self.tester_list:list[Tester] = list()

        for pid in range(self.split_num):
            t = TesterClass(self.db)
            t.ImportConfig(self)
            if tag_func != None:
                t.auto_tagging_index = tag_func(self.auto_tagging_index, pid)
            else:
                t.auto_tagging_index = self.auto_tagging_index + (100 * pid)
            self.tester_list.append(t)
    
    def mp_pid_entry(pid: int, tester: Tester, log_folder: str) -> bool:  # pragma: no cover
        tester.ImportUserHandle(pid)
        return [tester.start_tests(log_folder), tester.test_result]

    def run_tests_impl(self):
        from multiprocessing import Pool

        inputs = list()
        for pid in range(self.split_num):
            inputs.append((pid, self.tester_list[pid], self.log_folder))

        with Pool(processes=self.mp_num) as pool:
            results = pool.starmap(TesterMP.mp_pid_entry, inputs)
        
        for i in range(self.split_num):
            self.test_result += results[i][1]
        
        del self.tester_list[:]

        # print(results)
        for r in results:
            if r[0] == False:
                return False

        return True
