from MyUtils import str_op
from typing import TypedDict

class LcovFile(TypedDict):
    file_name : str
    lines_cov : int
    lines_total : int

class LcovFolder(TypedDict):
    folder_name: str
    file_records: list[LcovFile]
    lines_cov : int
    lines_total : int

class LcovSummary(TypedDict):
    folder_records: list[LcovFolder]
    max_file_len : int
    max_file_name : str
    sum_of_lines_cov : int
    sum_of_lines_total : int

def lcov_summary_list_file(file, file_exclude_list = [], folder_exclude_list = []) -> LcovSummary:
    '''
    0 - Scan 'Reading tracefile'
    1 - Scan 'Lines' 'Functions' 'Branches'
    2 - Scan 'Filename'
    3 - Scan '===' x n
    4 - Skip one line of folder name, e.g.: '[/home/user/]'
    5 - Start Extracting, e.g.: aaaa/aaaa.cpp 87.7% 487
    6 - Stop till: '===' x n
    7 - Save 'Total' and finalize.

    ['Reading', 'tracefile', 'xxx.info']
    ['Lines', 'vvvvtions', 'Branches']
    ['Filename', 'Rate', 'Num', 'Rate', 'Num', 'Rate', 'Num']
    ['================================================================================']
    ['[/home/user/]']
    ['aaaa/aaaa.cpp', '86.7%', '487', '69.4%', '49', '-', '0']
    ['aaaa/aaaa.h', '72.5%', '69', '76.9%', '13', '-', '0']
        ...
    ['================================================================================']
    ['Total:', '57.5%', '5797', '64.2%', '590', '-', '0']
    '''

    _summary = LcovSummary()
    _summary['folder_records'] = list()
    _summary['max_file_len'] = 0
    _summary['max_file_name'] = ''
    _summary['sum_of_lines_cov'] = 0
    _summary['sum_of_lines_total'] = 0


    def save_record(curr_splited_line : list[str]):
        assert len(curr_splited_line) == 7, f'curr_splited_line = {curr_splited_line}'

        #
        # Exclusion
        #
        if curr_splited_line[0] in file_exclude_list:
            return
        folder_name = curr_splited_line[0].split('/')[0]
        if folder_name in folder_exclude_list:
            return

        #
        # Update max length
        #
        if len(curr_splited_line[0]) > _summary['max_file_len']:
            _summary['max_file_len'] = len(curr_splited_line[0])
            _summary['max_file_name'] = curr_splited_line[0]
        
        #
        # 1. create file record
        #
        lines_total = int(curr_splited_line[2])
        percent_str = curr_splited_line[1].split('%')[0]
        lines_cov = int(float(percent_str) * lines_total / 100.0)
        # print(folder_name, lines_cov, lines_total)
        record : LcovFile = {
            'name' : curr_splited_line[0],
            'lines_cov' : lines_cov,
            'lines_total' : lines_total
        }

        #
        # 2. find folder record or create new one
        #
        curr_record = None
        for mgr in _summary['folder_records']:
            if mgr['folder_name'] == folder_name:
                curr_record = mgr
                break
        if curr_record == None:
            curr_record = LcovFolder()
            curr_record['folder_name'] = folder_name
            curr_record['file_records'] = list()
            curr_record['lines_cov'] = 0
            curr_record['lines_total'] = 0
            _summary['folder_records'].append(curr_record)

        #
        # 3. add to folder record
        #
        curr_record['file_records'].append(record)
        curr_record['lines_cov'] += lines_cov
        curr_record['lines_total'] += lines_total
        _summary['sum_of_lines_cov'] += lines_cov
        _summary['sum_of_lines_total'] += lines_total

        # end of save_record()

    with open(file) as f:
        state = 0
        for l in f:
            l = str_op.str_replace(l, ['|'], ' ')
            l = l.split()
            # print(l, state)

            if state == 0:
                assert len(l) >= 3, f'l = {l}'
                assert l[0] == 'Reading' and l[1] == 'tracefile', f'l = {l}'
                if l[0] == 'Reading' and l[1] == 'tracefile':
                    state = 1
            elif state == 1:
                assert len(l) == 3, f'l = {l}'
                assert l[0] == 'Lines' and l[1] == 'Functions' and l[2] == 'Branches', f'l = {l}'
                if l[0] == 'Lines' and l[1] == 'Functions' and l[2] == 'Branches':
                    state = 2
            elif state == 2:
                assert len(l) == 7, f'l = {l}'
                assert l[0] == 'Filename', f'l = {l}'
                if l[0] == 'Filename':
                    state = 3
            elif state == 3:
                assert len(l) == 1, f'l = {l}'
                assert l[0][0] == '=', f'l = {l}'
                if l[0][0] == '=':
                    state = 4
            elif state == 4:
                assert len(l) > 0, f'l = {l}'
                assert l[0][0] == '[' and l[0][-1] == ']', f'l = {l}'
                if l[0][0] == '[' and l[0][-1] == ']':
                    state = 5
            elif state == 5:
                assert len(l) == 7 or len(l) == 1, f'l = {l}'
                if len(l) == 7:
                    save_record(curr_splited_line=l)
                elif len(l) == 1:
                    assert l[0][0] == '=', f'l = {l}'
                    state = 6
                else: # pragma: no cover
                    assert 0
            elif state == 6:
                assert len(l) == 7, f'l = {l}'
                assert l[0] == 'Total:'
                state = 7
            else: # pragma: no cover
                assert 0, f'\'Total:\' should be the last line, curr state = {state}'

    # print(misc.json_dump_str(result))
    # print(max_file_name)

    return _summary

def lcov_records_dump(summary : LcovSummary):
    w = 0
    l = 0
    for r in summary['folder_records']:
        w = max(w, len(r['folder_name']))
        l = max(l, len(str(r['lines_total'])))
    for r in summary['folder_records']:
        percent = r['lines_cov'] / r ['lines_total'] * 100
        name = r['folder_name']
        total = r['lines_total']
        print(f'{name:{w}}, Lines_Total = {total:{l}}, Coverage = {percent:>6.2f} %')

# mys python_cov
def python_coverage_scan_total(file : str) -> int:
    ret = -1
    scanning_TOTAL = False
    with open(file) as f:
        for l in f:
            if scanning_TOTAL == False:
                if '---------- coverage:' in l:
                    scanning_TOTAL = True
            else:
                if 'TOTAL' in l and '%' in l:
                    l = str_op.str_replace(l, ['%'], '')
                    s = l.split()
                    assert len(s) == 4
                    ret = int(s[3])
                    break

    return ret