
import os
from pathlib import Path
import shutil
from typing import Union

class MyUtFsException(Exception):
    pass

def FsExc(func):
    def wrapper(*args, **kwargs):
        try :
            return func(*args, **kwargs)
        except Exception as e:  # pragma: no cover
            raise MyUtFsException from e
    return wrapper

class PathMgr():
    def __init__(self, dir:str = None):
        if dir == None:
            self.dir = pwd()
        else:
            self.dir = dir

    def Join(self, path: str):
        return join(self.dir, path)
    
    def __call__(self, path: str):
        return join(self.dir, path)

# for pdb mostly
@FsExc
def currdir() -> str:
    import sys
    ret = dirname(sys.argv[0])
    if ret == '':  # pragma: no cover
        ret = pwd()
    return ret

@FsExc
def find_file(
    currdir: str, 
    filename: str,
    walk_to_root=False, 
    walk_from_root=False
) -> str:
    assert walk_from_root == False # TODO

    loc = None
    curr_folder = currdir
    while(True):
        full_path = join(curr_folder, filename)
        if exists(full_path):
            loc = curr_folder
            break

        if walk_to_root:
            prev_folder = dirname(curr_folder) # use dirname() to walk to root folder
            if prev_folder == curr_folder: # in root
                break
            curr_folder = prev_folder
            # next round
        else:
            break

    return loc

# def find_file_to_root(currdir: str, filename: str) -> str:
#     loc = None
#     curr_folder = currdir
#     while(True):
#         full_path = join(curr_folder, filename)
#         if exists(full_path):
#             loc = curr_folder
#             break
#         prev_folder = dirname(curr_folder) # use dirname() to walk to root folder
#         if prev_folder == curr_folder:
#             break
#         curr_folder = prev_folder

#     return loc

# mys
@FsExc
def dirname(path: str) -> str:
    return os.path.dirname(path)

# mys
def basename(path: str) -> str:
    return os.path.basename(path)

# mys - print
def pwd() -> str:
    return os.getcwd()

# mys
def join(pre: str, post: str) -> str:
    return os.path.join(pre, post)

# mys
def exists(path: str) -> bool:
    return os.path.exists(path)

def exists_check(d: dict[str, bool], fail_exc=False) -> bool:
    for k, v in d.items():
        ex = exists(k)
        if ex != v:
            if fail_exc:
                raise ValueError(f'exists_check failed: key:{k}, golden:{v}, target:{ex}')
            else:
                return False
    return True

# mys
def all_exists(path: Union[str, list[str]]) -> bool:
    if isinstance(path, str):
        p_list = [path]
    else:
        p_list = path
    
    for p in p_list:
        if exists(p) == False:
            return False
    return True

# mys
def all_not_exists(path: Union[str, list[str]]) -> bool:
    if isinstance(path, str):
        p_list = [path]
    else:
        p_list = path
    
    for p in p_list:
        if exists(p) == True:
            return False
    return True

# mys
def mkdir(path: str, exist_ok: bool = True) -> None:
    Path(path).mkdir(parents=True, exist_ok=exist_ok)

# mys
def rmdir(path: str) -> bool:
    if exists(path):
        shutil.rmtree(path)
        return True
    else:
        return False

# mys
def file_create(path: str, create_folder: bool = False) -> None:
    dir = os.path.dirname(path)
    # print('dir =', dir)
    if exists(dir) == False and create_folder:
        mkdir(dir)
    with open(path, 'w'):
        pass

# mys
def file_remove(path: Union[str, list[str]]) -> bool:
    def __file_remove(path: str) -> bool:
        if exists(path):
            os.remove(path)
            return True # remove success
        else:
            return False
    if isinstance(path, str):
        return __file_remove(path)
    elif isinstance(path, list):
        ret = False
        for p in path:
            if __file_remove(p):
                ret = True
    return ret

# mys - print_list
def list_dir(folder_name: str) -> list:
    if folder_name == None:
        folder_name = '.'
    return os.listdir(folder_name)

# mys - print_list
def list_files(folder_name: str = None, extension_allowed: str = None) -> list:
    if folder_name == None:
        folder_name = '.'

    if extension_allowed != None:
        extension_list = extension_allowed.split()

    files = []
    for name in os.listdir(folder_name):
        path = os.path.join(folder_name, name)
        if os.path.isfile(path):
            # print(path)
            if extension_allowed != None:
                filename, file_extension = os.path.splitext(name)
                if file_extension in extension_list:
                    files.append(name)    
            else:
                files.append(name)
    return files

# mys - -
def list_dump(input: list[str]):
    for i in input:
        print(i)

# mys - print_list
def ls() -> list: # pragma: no cover
    ret = list()
    items = list_dir('.')
    for i in items:
        ret.append(join(pwd(), i))
    return ret

#
# List recursively
#
# mys - print_list
def list_r(folder_name: str = None) -> list:
    if folder_name == None:
        folder_name = '.'
    
    ret = []
    # i = 0
    for root, subdirs, files in os.walk(folder_name):
        # print(i)
        # print(root)
        # print(subdirs)
        # print(files)
        for f in files:
            fname = join(root, f)
            # print(fname)
            ret.append(fname)
        #     fname = join(root, f)
        #     print(fname)
        #     files.append(fname)
        # i += 1
    return ret

# mys
def clean() -> None:
    import os
    return os.system('./clean.sh')

# mys
def clean_r(folder_name: str = None) -> None:
    import os
    if folder_name == None:
        folder_name = '.'
    
    files = list_r(folder_name)
    
    for f in files:
        fname = basename(f)
        dname = dirname(f)
        # print(dname, fname)
        if fname == 'clean.sh':
            os.system(f'cd {dname} ; ./clean.sh')

#
# Return if deps are newer
#
# mys - - arg2 str_split
def newer(target: str, deps: list[str] = None) -> bool:
    import os
    if exists(target) == False:
        return True
    if deps == None:
        return True
    if len(deps) == 0:
        return True
    target_ts = os.path.getmtime(target)
    # print(target_ts)
    for d in deps:
        if exists(d) == False:
            return True
        d_ts = os.path.getmtime(d)
        if d_ts > target_ts:
            return True

    return False

def import_mod(dir: str, mod_name_without_py_extension: str):
    """
    Usage:
        mod = import_mod('mod', 'mod_name_without_py_extension)
        mod.your_function_name()
    """
    import sys
    import importlib
    sys.path.append(dir)
    ret = importlib.import_module(mod_name_without_py_extension)
    return ret

# mys
def home() -> str:
    from pathlib import Path
    return str(Path.home())

def file_copy(src: str, dst: str) -> None:
    '''
        https://stackoverflow.com/questions/123198/how-do-i-copy-a-file

        You can use one of the copy functions from the shutil package:

        Function            preserves     supports          accepts     copies other
                            permissions   directory dest.   file obj    metadata  
        ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
        shutil.copy              ✔             ✔                 ☐           ☐
        shutil.copy2             ✔             ✔                 ☐           ✔
        shutil.copyfile          ☐             ☐                 ☐           ☐
        shutil.copyfileobj       ☐             ☐                 ✔           ☐
    '''
    import shutil
    shutil.copy(src, dst)  # dst can be a folder; use shutil.copy2() to preserve timestamp
