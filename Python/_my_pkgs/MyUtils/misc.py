'''
Use cases:

import sys
sys.path.append('../')
import util

Prof = util.ProfilingManager
pf = util.ProfilingFunc

from util import *

import sys
sys.path.append('../../')
import util
'''
import sys
import inspect

# def MY_ASSERT(condi):
#     if __debug__:
#         if condi == False:
#             from inspect import getframeinfo, stack
#             caller = getframeinfo(stack()[1][0])
#             print('[FAILED] ASSERT at {} : line {}.'.format(caller.filename, caller.lineno))
#             sys.exit(1)

def DISP(var, var_name = 'N/A', max_prints=None):
    ctr = 0
    if isinstance(var, list):
        print(var_name + ' = ( list:len:', len(var), ')')
        for v in var:
            print(v)
            ctr += 1
            if max_prints != None:
                if ctr >= max_prints:
                    break
    elif isinstance(var, dict):
        print(var_name + ' = ( dict:len:', len(var), ')')
        for v in var:
            print(v, ':', var[v])
            ctr += 1
            if max_prints != None:
                if ctr >= max_prints:
                    break
    else:
        # print(f"Variable name: {var_name}")
        print(var_name + ' =', var)
        # print('type =', type(var))

def SHOW(var, max_prints=None):
    current_frame = inspect.currentframe()
    try:
        frame_locals = current_frame.f_back.f_locals
        var_name = [name for name, value in frame_locals.items() if value is var][0]
        DISP(var, var_name, max_prints)
    finally:
        del current_frame

def try_catch(func):
    try:
        func()
    except BaseException as err:
        print('Exception -', err.__class__.__name__, ':', err)


import importlib.util, sys
def import_abs_pkg(name, path):
    spec = importlib.util.spec_from_file_location(name, path+name+'/__init__.py')
    pkg = importlib.util.module_from_spec(spec)
    sys.modules[name] = pkg
    spec.loader.exec_module(pkg)
    return pkg

def import_abs_mod(name, path):
    spec = importlib.util.spec_from_file_location(name, path+name+'.py')
    mod = importlib.util.module_from_spec(spec)
    sys.modules[name] = mod
    spec.loader.exec_module(mod)
    return mod

import json

def json_dump_str(data, indent=2) -> str:
    return json.dumps(data, indent=indent)

def json_dump(data, path, indent=2):
    with open(path, "w") as f:
        json.dump(data, f, indent=indent)

def json_load(path):
    with open(path, "r") as f:
        return json.load(f)

def dict_key_value_exist(d: dict, key):
    if key in d:
        if d[key] != None:
            return True
    return False

def dict_key_value_extract(d: dict, key):
    if key in d:
        if d[key] != None:
            return d[key]
    return None

def list_containerize(value):
    if value != None:
        if isinstance(value, list):
            return value
        else:
            return [value]
    return None

def dict_string_list_value_extract(d: dict, key):
    ret = dict_key_value_extract(d, key)
    ret = list_containerize(ret)
    return ret

def dict_return_reversed(input: dict):
    l = list()
    for k, v in input.items():
        l.append([k, v])
    ret = dict()
    for i in reversed(l):
        ret[i[0]] = i[1]
    return ret

def dict_merge(merged, another):
    if another == None:
        return merged
    for key, value in another.items():
        if key in merged:
            raise KeyError('Duplicate key found: %s' % key)
        merged[key] = value
    return merged
