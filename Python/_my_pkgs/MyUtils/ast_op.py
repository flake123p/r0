import ast
from typing import TypedDict
from MyUtils import misc

# Define a type for function argument parameters
class ArgPara(TypedDict):
    name: str
    anno: str

# Define a type for function parameters
class FuncPara(TypedDict):
    name: str
    lineno: int
    ret_anno: str
    args: list[ArgPara]

# Parse a source string and extract function information
def ast_parse_string(source: str) -> list[FuncPara]:
    node = ast.parse(source)  # Parse the source code into an AST

    functions = []
    for item in ast.walk(node):  # Walk through all nodes in the AST
        if isinstance(item, ast.FunctionDef):  # Check if it's a function definition
            skip_class_method = False
            fp = FuncPara()
            fp['name'] = item.name  # Function name
            fp['lineno'] = item.lineno  # Line number where function is defined
            fp['args'] = []
            for arg in item.args.args:  # Iterate over function arguments
                a = ArgPara()
                if arg.arg == 'self':
                    skip_class_method = True
                    break
                a['name'] = arg.arg  # Argument name
                a['anno'] = (
                    ast.unparse(arg.annotation) if arg.annotation else None
                )  # Get annotation as a string
                fp['args'].append(a)
            fp['ret_anno'] = (
                ast.unparse(item.returns) if item.returns else None
            )  # Return type annotation
            if not skip_class_method:
                functions.append(fp)

    if 1:  # Ascending sorting
        def myFunc(e):
            return e['lineno']
        descent_order = False
        functions.sort(reverse=descent_order, key=myFunc)  # Sort functions by line number

    return functions

# Parse a file and extract function information
def ast_parse_file(file: str) -> list[FuncPara]:
    file = open(file)
    source = file.read()  # Read the file content
    file.close()

    return ast_parse_string(source)  # Parse the file content

# Get the previous line of each function definition
def ast_get_prev_line(file: str, func_para_list: list[FuncPara]) -> dict[str, str]:
    ret = dict()

    max = len(func_para_list)
    if max == 0:
        return ret

    fi = max - 1
    target_line = func_para_list[fi]['lineno'] - 1
    if target_line == 0:
        return ret

    # Open the file and read all lines
    with open(file) as fr:
        lines = fr.readlines()
        for i in reversed(range(len(lines))):
            if target_line == i + 1:
                # Matched the target line
                if lines[i][0] == '@':  # Skip decorator lines
                    # print('skip decorator:', lines[i])
                    target_line -= 1
                    if target_line == 0:
                        return misc.dict_return_reversed(ret)
                else:
                    # print('add dict key value =', func_para_list[fi]['name'], '->', lines[i].replace('\n', ''))
                    ret[func_para_list[fi]['name']] = lines[i].replace('\n', '')  # Store the previous line
                    if fi == 0:
                        return misc.dict_return_reversed(ret)
                    else:
                        fi -= 1
                        target_line = func_para_list[fi]['lineno'] - 1
                        if target_line == 0:
                            return misc.dict_return_reversed(ret)

    assert 0, f'Unknown error, file={file}, fi={fi}, max={max}'  # pragma: no cover
