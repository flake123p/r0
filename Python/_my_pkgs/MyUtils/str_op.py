from typing_extensions import Self
import re
from MyUtils import fs

class StrFile():
    def __init__(self):
        self.name = 'dummy_StrFile_local.log'
        self.file = fs.join(fs.pwd(), self.name)

    def __del__(self):
        fs.file_remove(self.file)

    def use_linux_tmp(self) -> Self:
        self.file = fs.join('/tmp', self.name)
        return self
    
    def str_to_file(self, string: str) -> str:
        with open(self.file, "w") as fw:
            fw.write(string)
        return self.file

def str_split(string : str, separators : list[str] = []):
    assert isinstance(separators, list) == True
    if len(separators) == 0:
        return string.split()
    
    delimiters = r"["
    for s in separators:
        delimiters += s
    delimiters += "]"
    # print(delimiters)

    return re.split(delimiters, string)

#
# Return new replaced string
# mys - - arg2 str_split
def str_replace(string : str, from_patterns : list[str], to_pattern : str) -> str:
    assert isinstance(from_patterns, list) == True

    ret = string

    for f in from_patterns:
        ret = ret.replace(f, to_pattern)
    
    return ret

# def str_concat(s1 : str, s2 : str):
#     return s1 + s2

def str_replace_last(string: str, from_pattern: str, to_pattern: str) -> str:
    last = string.rfind(from_pattern)
    if last == -1:
        return string
    return string[:last] + to_pattern + string[last + len(from_pattern):]