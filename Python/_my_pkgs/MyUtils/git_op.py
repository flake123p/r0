from MyUtils import run

# mys
def git_show_toplevel(verbose : int = 0) -> str:
    cmd = 'git rev-parse --show-toplevel'
    if verbose:
        print('cmd =', cmd)
    buf = run.run_cmd_to_buf(cmd)
    assert len(buf) == 2
    assert len(buf[0]) > 0
    assert buf[1] == ''
    return buf[0]

# mys
def git_show_url(verbose : int = 0) -> str:
    cmd = 'git remote get-url origin'
    if verbose:
        print('cmd =', cmd)
    buf = run.run_cmd_to_buf(cmd)
    assert len(buf) == 2
    assert len(buf[0]) > 0
    assert buf[1] == ''
    return buf[0]

#
# Git String (gits) section
#
# mys
def gits_flush() -> str:
    cmd = 'git stash ; git stash drop'
    return cmd

# mys
def gits_flush_to_latest() -> str:
    cmd = 'git stash ; git stash drop ; git checkout main ; git pull'
    return cmd