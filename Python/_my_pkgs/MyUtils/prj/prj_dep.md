# prj.dep.yaml
```yml
#
# Implicit Target: ALL
#
Deps:
  "123":
    # Timestamp Prerequisites
    # :Union[str, list[str]]
    TS_Prereq : null

    # Target Prerequisites (Execute Targets First)
    # :Union[str, list[str]]
    TGT_Prereq : null

    # Console Action (Will run before Python Action)
    # :str
    Console_Action : echo "echo 123" from prj dep
    # Python Action (Will run after Concole Action)
    # :str
    Python_Action : null
    # :Union[str, list[str]]
    Python_ActionArgs : null

    TGT_Action : null
    Group_Action : null

    # :str
    Console_FalseAction : echo "echo false 123" from prj dep
    # :str
    Python_FalseAction : null
    # :Union[str, list[str]]
    Python_FalseActionArgs : null

    TGT_FalseAction : null
    Group_FalseAction : null

    # :str
    Group: AAA
    # :Union[str, list[str]]
    Group_Prereq : null
```

# Python_Action input/output
input/ouput dict:
```yml
ActionType: True # or False
args: [abc, def] # List of String
TargetParam: null # Dictionary of currnt target parameters.
StopPrjDep: True # or False, stops prj_dep target chain.
StopPrjDepMsg: some message # string
UserHandle: null # Accepts any type and supports result chaining.
```

============================================================================
# v2.0 Draft
----------------------------------------------------------------------------

prj_var


## Syntax
```yml
# Location of dep yamls, implicit default value: current folder ./
Dep_Folders : null

# Location of dep yamls, using prj target as a path
Dep_Prj_Folders : null

# Is target a file or folder, default: false.
# :bool
Phony : true

# When doing execute "ALL_FORCE", prevent it from execution this target dep check. Default: false
# :bool
Block_The_ALL_FORCE_Execution : false

# Register Self to Targets, default: ALL
# :Optional[str, list[str]]
TG_Register : ALL
```