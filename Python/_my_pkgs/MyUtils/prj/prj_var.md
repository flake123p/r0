# Main

String replacer for prj package

draft 1
```yml
Vars:
    # default system variable for argv[0], ...
    # $0, $1, $2

    # number is illegal
    "123" : null

    # replace $a123
    a123 : 1.log

DynamicVars:
    # repalce $b123
    b123 : location_of_python_module $1 $a123

    b456 : location_of_python_module $1 $b123 # ok, because $b123 is evaluated ahead.
```

draft 2
```yml

Vars:
    abc: # replace $abc
        SimpleValue: 1.log
        ConsoleFunction: ls
        PythonFunction: mod/gen.py
        PythonFunctionArgs:
            - abc
            - True
            - ${0}
            - ${1}
            - ${2}

    
    def: 2.log

    defg: 3.log

    ghi: ${abc} ${def}

    jkl: $$120abc${def}g


```
