# class RunMgr
run_mgr.py

Generalize script:

```yml
RunScripts:
    test00:
        Action0: echo 123>log00.txt
        Action1: python is_failed.py log00.txt
        Action2: null
        RetPythonModule: parse_report.py log00.txt
    
    test01:
        Action0: python sanity/run_test.py run log01
        RetPythonModule: python sanity/run_test.py parse log01

RunConfigs:
    Stop_At_One_False: true
    RunningGroup: # String or List of String or ALL
```
