from typing import TypedDict, Optional, Dict, Any
from typing_extensions import Self
import yaml
from MyUtils import git_op, fs, misc

# def prj_show_root() -> str:
#     top_level = git_op.git_show_toplevel()
#     base = fs.basename(top_level)
#     if base == 'r2': # pragma no cover
#         return fs.join(top_level, 'cpp/projects')
#     return top_level

class PrjException(Exception):
    pass

class PrjDict(TypedDict):
    Namespace: str
    Real_Static: dict[str, str]
    Real_Dynamic: dict[str, list[str]]
    Pseudo_Static: dict[str, str]
    Pseudo_Dynamic: dict[str, list[str]]
    Gen_Log: int

class AutoDict(TypedDict):
    Namespace: str
    Real_Static: dict[str, str]
    Real_Dynamic: dict[str, str]
    Pseudo_Static: dict[str, str]
    Pseudo_Dynamic: dict[str, str]

# class AutoPara(TypedDict):
#     Namespace: str
#     Real_Static: dict
#     Real_Dynamic: dict
#     External_Params: dict[str, Self]
#     External_Depth: int # root only
#     Loc_List: list # root only
class PrjBase():
    def __init__(self):
        self.path = None
        self.conf_file = None
        self.auto_file = None
        self.confd = None
        self.autod = None

    def evaluate_path(self, path: str):
        if path == None: # pragma no cover
            self.path = fs.currdir()
        else:
            self.path = path
        
        assert fs.exists(self.path) == True, f'Failed: Invalid path: {self.path}'

    def auto_yml_dep_check(self) -> bool: # Return if needs to gen
        assert self.conf_file != None
        assert self.auto_file != None
        auto_yml_exist = fs.exists(self.auto_file)
        if auto_yml_exist:
            auto_yml_newer = fs.newer(self.conf_file, [self.auto_file])
            if auto_yml_newer:
                return False
        return True

    def load_conf_yml(self):
        assert self.conf_file != None
        with open(self.conf_file, 'r') as fr:
            self.confd = yaml.safe_load(fr)

    def load_auto_yml(self):
        assert self.auto_file != None
        with open(self.auto_file, 'r') as fr:
            self.autod = yaml.safe_load(fr)

class Prj(PrjBase):
    def __init__(self, path: str = None, enable_parse=True):
        super().__init__()
        self.evaluate_path(path)
        self.conf_yml = '.prj.yaml'
        self.auto_yml = '.prj/auto_gen.yaml'
        self.confd:PrjDict = None
        self.autod:AutoDict = None
        self.conf_loc = None
        self.force_gen = False
        '''
        1. if autod == None, find conf_loc (.prj.yaml)
        2. if conf_loc == None, use self.path to find prj yaml. (Walk from current path toward root.)
        3. if prj yaml was found, check auto yml exists.
        4. if prj yaml wasn't found, return error, self.conf_loc is None forever
        4. if auto yml exists but older, re-gen auto yml.
        5. if auto yml doesn't exist, re-gen auto yml.
        6. read auto yaml to self.autod
        '''
        if enable_parse:
            self.parse()
       
    # def auto_yml_dep_check(self) -> bool: # Return if needs to gen
    #     assert self.path != None
    #     assert self.conf_loc != None
    #     assert self.conf_file != None
    #     assert self.auto_file != None
    #     auto_yml_exist = fs.exists(self.auto_file)
    #     if auto_yml_exist:
    #         auto_yml_newer = fs.newer(self.conf_file, [self.auto_file])
    #         if auto_yml_newer:
    #             return False
    #     return True
    
    def yml_load_n_integrity_check(self): # Return if pass the check
        assert self.path != None
        assert self.conf_loc != None
        assert self.conf_file != None
        assert self.auto_file != None

        self.load_conf_yml()
        self.load_auto_yml()

        ns = self.confd['Namespace']
        if ns != self.autod['Namespace']:
            return False
        
        if not misc.dict_key_value_exist(self.autod, 'Real_Static'):
            return False

        if ns not in self.autod['Real_Static']:
            return False

        if self.autod['Real_Static'][ns] != self.conf_loc:
            return False
        
        return True
    
    def load_conf_yml(self):
        assert self.conf_loc != None
        super().load_conf_yml()
        assert 'Namespace' in self.confd, f'Failed: Can\'t find "Namespace" key in conf_file: {self.conf_file}'

    def load_auto_yml(self):
        assert self.conf_loc != None
        super().load_auto_yml()
        assert 'Namespace' in self.autod, f'Failed: Can\'t find "Namespace" key in conf_file: {self.auto_file}'
    
    def auto_yml_gen(self):
        assert self.path != None
        assert self.conf_loc != None
        assert self.conf_file != None
        assert self.auto_file != None

        self.load_conf_yml()
        assert self.confd != None
        
        autod = AutoDict()
        ns = self.confd['Namespace']

        autod['Namespace'] = ns

        #
        # Real_Static
        #
        key = 'Real_Static'
        autod[key] = dict()
        autod[key][ns] = self.conf_loc
        if misc.dict_key_value_exist(self.confd, key):
            for k, v in self.confd[key].items():
                # Existance check
                path = fs.join(self.conf_loc, v)
                assert fs.exists(path) == True, f'Failed: Static_Target: {k} - it\'s path doesn\'t exist : {path}'
                autod[key][k] = path
        
        #
        # Pseudo_Static
        #
        key = 'Pseudo_Static'
        if misc.dict_key_value_exist(self.confd, key):
            autod[key] = dict()
            for k, v in self.confd[key].items():
                autod[key][k] = fs.join(self.conf_loc, v)
        
        def dynamic_eval(key: str, check_existance: bool):
            if misc.dict_key_value_exist(self.confd, key):
                autod[key] = dict()
                for k, v in self.confd[key].items():
                    dyn_py = fs.join(self.conf_loc, k)
                    assert fs.exists(dyn_py) == True, f'Failed: {key}: {k} - dynamic python file doesn\'t exist: {dyn_py}'
                    dir = fs.dirname(dyn_py)
                    base = fs.basename(dyn_py)
                    assert len(base) >= 4 # file name 1 + extension 3 (.py)
                    assert base[-3:] == '.py', f'Failed: {key}: {k} - not python extension(.py): {dyn_py}'
                    mod_name = base[:-3]
                    mod = fs.import_mod(dir, mod_name)
                    assert isinstance(v, list) == True
                    for target in v:
                        target_value = mod.eval(target)
                        # print(k, target, target_value)
                        if check_existance:
                            assert fs.exists(target_value) == True, f'Failed: {key}: {k} - target doesn\'t exist: {target_value}'
                        autod[key][target] = target_value
        
        dynamic_eval('Real_Dynamic', check_existance=True)
        dynamic_eval('Pseudo_Dynamic', check_existance=False)

        self.autod = autod
        #
        # Output
        #
        auto_yaml_dir = fs.dirname(self.auto_file)
        fs.mkdir(auto_yaml_dir)
        with open(self.auto_file, 'w') as fw:
            #
            # Clean
            #
            # if 'External_Depth' in local_para:
            #     del local_para['External_Depth']
            # if 'Loc_List' in local_para:
            #     del local_para['Loc_List']
            yaml.dump(self.autod, fw, sort_keys=False)
    
    def auto_yml_load(self) -> None: # Check if auto_yml is newer, otherwise generate it.
        needs_to_gen = self.auto_yml_dep_check()
        if needs_to_gen == False:
            assert fs.exists(fs.join(self.conf_loc, self.conf_yml)) == True
            assert fs.exists(fs.join(self.conf_loc, self.auto_yml)) == True
            is_pass = self.yml_load_n_integrity_check()
            if is_pass == False:
                needs_to_gen= True

        if needs_to_gen or self.force_gen:
            self.auto_yml_gen()
            if self.force_gen == True:
                self.force_gen = False
        else:
            assert self.confd != None
            assert self.autod != None

    def parse(self) -> Self:
        self.conf_loc = fs.find_file(self.path, self.conf_yml, walk_to_root=True)
        assert self.conf_loc != None, f'Failed: Can\'t find "{self.conf_yml}" in path: {self.path}'
        
        self.conf_file = fs.join(self.conf_loc, self.conf_yml)
        self.auto_file = fs.join(self.conf_loc, self.auto_yml)

        self.auto_yml_load()
        assert self.confd != None
        assert self.autod != None
        assert self.confd['Namespace'] == self.autod['Namespace']
        return self
    
    def get_namespace(self, force_gen=False):
        if force_gen:
            self.force_gen = force_gen
            self.parse()
        return self.autod['Namespace']
    
    def get_root(self, force_gen=False):
        ns = self.get_namespace(force_gen=force_gen)
        return self.get_target(ns, fail_exit=True)
    
    def get_target(self, name, force_gen=False, fail_exit=False):
        if force_gen:
            self.force_gen = force_gen
            self.parse()
        keys = ['Real_Static', 'Pseudo_Static', 'Real_Dynamic', 'Pseudo_Dynamic']
        for k in keys:
            if k in self.autod:
                if name in self.autod[k]:
                    return self.autod[k][name]
        
        if fail_exit:
            raise PrjException(f"Failed : Can't find target: {name}")

        return None
    
    def sys_path_append(self, target_name: str, force_gen=False):
        target_path = self.get_target(target_name, force_gen=force_gen, fail_exit=True)
        assert target_path != None
        import sys
        sys.path.append(target_path)

# mys
def prj_parse():
    p = Prj(path=fs.pwd(), enable_parse = True)

# mys
def prj_ns() -> str:
    p = Prj(path=fs.pwd(), enable_parse = True)
    return p.get_namespace()

# mys
def prj_target() -> str:
    p = Prj(path=fs.pwd(), enable_parse = True)
    return p.get_target()

# mys
def prj_root() -> str:
    p = Prj(path=fs.pwd(), enable_parse = True)
    return p.get_root()

# mys
def prj_force():
    p = Prj(path=fs.pwd(), enable_parse = False)
    p.force_gen = True
    p.parse()

# mys
def prj_list_conf():
    p = Prj(path=fs.pwd(), enable_parse = True)
    import sys
    import json
    json.dump(p.confd, sys.stdout, indent=2)
    print()

# mys
def prj_list_auto():
    p = Prj(path=fs.pwd(), enable_parse = True)
    import sys
    import json
    json.dump(p.autod, sys.stdout, indent=2)
    print()