from typing import TypedDict, Optional, Dict, Any
from typing_extensions import Self
import yaml
import logging

from MyUtils import fs, misc, run, str_op
from .prj import *

class PrjDepException(Exception):
    pass

class PrjDepUnit(TypedDict):
    TS_Prereq: list[str]
    TGT_Prereq: list[str]
    Python_Action: str
    Console_Action: str
    # Target chain
    TGT_Action: list[str]
    # Grouping
    Group_Prereq: list[str]
    Group: list[str]
    Group_Action: list[str]
    # False Actions
    Console_FalseAction: str
    Python_FalseAction: str
    TGT_FalseAction: list[str]
    Group_FalseAction: list[str]
   

class PrjDepConfig(TypedDict):
    Dep_PrjWise: dict[str, PrjDepUnit]
    Dep_Local: dict[str, PrjDepUnit]
    Dep_Dynamic: dict[str : list[str]] # Generator : [Args List]

class PrjDepAuto(TypedDict):
    Dep_PrjWise: dict[str, PrjDepUnit] # Join prj root path
    Dep_Local: dict[str, PrjDepUnit] # Same
    Dep_Dynamic: dict[str, PrjDepUnit] # Gen for generator python file

class PrjDepPyActDict(TypedDict):
    ActionType: bool
    args: list[str]
    TargetParam: dict[str, PrjDepUnit]
    StopPrjDep: bool
    StopPrjDepMsg: str
    UserHandle: Any

class PrjDep(PrjBase):
    def __init__(self, path: str = None, logger_file: str = 'prj_dep.log', logger:logging.Logger = None):
        super().__init__()
        self.evaluate_path(path)
        self.conf_yml = '.prj.dep.yaml'
        self.target_db = None
        self.job_list: list[str] = []
        self.group_list: list[str] = []

        if logger == None:
            logger: logging.Logger = logging.getLogger(name=logger_file)
            logger.disabled = True
        self.logger = logger
        self.logger_file = logger_file

        self.group_level = 0
        self.target_level = 0
        self.init_pyactd()
        
    def init_pyactd(self):
        self.pyactd: PrjDepPyActDict = {
            'ActionType': True,
            'args': [],
            'TargetParam': {},
            'StopPrjDep': False,
            'StopPrjDepMsg': '',
            'UserHandle': None
        }

    def group_deco(func):
        def wrapper(*args, **kwargs):
            self: Self = args[0]
            self.group_level += 1
            ret = func(*args, **kwargs)
            self.group_level -= 1
            return ret
        return wrapper
    
    def target_deco(func):
        def wrapper(*args, **kwargs):
            self: Self = args[0]
            self.target_level += 1
            ret = func(*args, **kwargs)
            self.target_level -= 1
            return ret
        return wrapper

    def get_target_dict(self, tname) -> dict[str, dict[str, Any]]:
        assert self.target_db != None
        tdict = self.target_db[tname]
        assert tdict != None
        return tdict
    
    @group_deco
    def dep_group(self, group_id: str):
        if group_id in self.group_list:
            self.logger.debug(f"[dep_group L:{self.group_level}] group_id: " + group_id + " already in group_list")
            return
        else:
            self.logger.debug(f"[dep_group L:{self.group_level}] group_id: " + group_id + " added to group_list")
            self.group_list.append(group_id)
        assert self.target_db != None

        for tname, parad in self.target_db.items():
            if tgt_gid_list := misc.dict_string_list_value_extract(parad, 'Group'):
                for tgt_gid in tgt_gid_list:
                    if tgt_gid == group_id:
                        self.dep_act(tname)
    
    @target_deco
    def dep_act(self, tname: str):
        if tname in self.job_list:
            self.logger.debug(f"[dep_act L:{self.target_level}] tname: " + tname + " already in job_list")
            return
        else:
            self.logger.debug(f"[dep_act L:{self.target_level}] tname: " + tname + " added to job_list")
            self.job_list.append(tname)
        
        tdict = self.get_target_dict(tname)

        #
        # Group prerequisites check
        #
        if gp_pre := misc.dict_string_list_value_extract(tdict, 'Group_Prereq'):
            assert isinstance(gp_pre, list)
            for gid in gp_pre:
                self.dep_group(gid)

        #
        # Target prerequisites check
        #
        if tgt_pre := misc.dict_string_list_value_extract(tdict, 'TGT_Prereq'):
            assert isinstance(tgt_pre, list)
            for t in tgt_pre:
                self.dep_act(t)
    
    def dep_act_pyact(self, path: str):
        assert fs.exists(path) == True, f"Python action file not found: {path}"
        assert path.endswith('.py') == True, f"Python action file must end with .py: {path}"
        path = str_op.str_replace_last(path, '.py', '')
        basename = fs.basename(path)
        dirname = fs.dirname(path)
        mod = fs.import_mod(dirname, basename)
        mod.prj_dep_py_act(self.pyactd)
        if self.pyactd['StopPrjDep']:
            raise PrjDepException("StopPrjDep")

    def dep_act_start(self):
        assert self.target_db != None
        self.logger.debug("[dep_act_start] job_list: " + str(self.job_list))

        while len(self.job_list) > 0:
            self.logger.debug("job_list before pop: " + str(self.job_list))
            tname = self.job_list.pop()
            self.logger.debug("poped tname: " + tname)
            parad = self.target_db[tname]
            assert parad != None

            ts_pre = misc.dict_string_list_value_extract(parad, 'TS_Prereq')

            action_required = False
            if fs.newer(tname, ts_pre):
                action_required = True

            if action_required:
                if v := misc.dict_key_value_extract(parad, 'Console_Action'):
                    self.logger.debug("[parse_dep_act_startdeco] Console_Action, Target: " + tname)
                    run.run_cmd(v)
                if v := misc.dict_key_value_extract(parad, 'Python_Action'):
                    self.logger.debug("[parse_dep_act_startdeco] Python_Action, Target: " + tname)
                    self.pyactd['ActionType'] = True
                    self.pyactd['args'] = misc.dict_string_list_value_extract(parad, 'Python_ActionArgs')
                    self.pyactd['TargetParam'] = parad
                    self.pyactd['StopPrjDep'] = False
                    self.dep_act_pyact(v)
                if v := misc.dict_string_list_value_extract(parad, 'TGT_Action'):
                    for tgt in v:
                        self.dep_act(tgt)
                if v := misc.dict_string_list_value_extract(parad, 'Group_Action'):
                    for tgt in v:
                        self.dep_group(tgt)
            else:
                if v := misc.dict_key_value_extract(parad, 'Console_FalseAction'):
                    self.logger.debug("[parse_dep_act_startdeco] Console_FalseAction, Target: " + tname)
                    run.run_cmd(v)
                if v := misc.dict_key_value_extract(parad, 'Python_FalseAction'):
                    self.logger.debug("[parse_dep_act_startdeco] Python_FalseAction, Target: " + tname)
                    self.pyactd['ActionType'] = False
                    self.pyactd['args'] = misc.dict_string_list_value_extract(parad, 'Python_FalseActionArgs')
                    self.pyactd['TargetParam'] = parad
                    self.pyactd['StopPrjDep'] = False
                    self.dep_act_pyact(v)
                if v := misc.dict_string_list_value_extract(parad, 'TGT_FalseAction'):
                    for tgt in v:
                        self.dep_act(tgt)
                if v := misc.dict_string_list_value_extract(parad, 'Group_FalseAction'):
                    for tgt in v:
                        self.dep_group(tgt)
    
    def load_conf_inc_recursive(self, inc_history: list[str]):
        assert self.confd == None
        self.conf_file = fs.join(self.path, self.conf_yml)
        self.load_conf_yml()

        if inc_list := misc.dict_string_list_value_extract(self.confd, 'Include'):
            for inc in inc_list:
                if inc in inc_history:
                    return
                else:
                    inc_history.append(inc)
                inc_dep = PrjDep(self.path, logger=self.logger)
                inc_dep.conf_yml = inc
                inc_dep.load_conf_inc_recursive(inc_history)
                del inc_dep
    
    def load_conf_dict_includes(self, inc_history: list[str]):
        assert self.confd != None
        self.logger.debug("[load_conf_dict_includes] inc_history: " + misc.json_dump_str(inc_history))
        for inc in inc_history:
            self.logger.debug("[load_conf_dict_includes] inc: " + inc)
            if inc != self.conf_yml:
                inc_dep = PrjDep(self.path, logger=self.logger)
                inc_dep.conf_yml = inc
                inc_dep.conf_file = fs.join(self.path, inc_dep.conf_yml)
                inc_dep.load_conf_yml()
                # self.logger.debug("[load_conf_dict_includes] inc_dep: " + misc.json_dump_str(inc_dep.confd))
                misc.dict_merge(self.confd['Deps'], inc_dep.confd['Deps'])
                del inc_dep
    
    def load_conf_dict(self):
        if self.confd == None:
            self.logger.debug("[load_conf_dict] start")
            inc_history = [self.conf_yml]
            self.load_conf_inc_recursive(inc_history)
            self.logger.debug("[load_conf_dict] inc_history: " + misc.json_dump_str(inc_history))

            self.load_conf_dict_includes(inc_history)
            

    def parse_deco(func):
        def wrapper(*args, **kwargs):
            self: Self = args[0]
            
            enable_logger = kwargs.get('enable_logger', False)

            if enable_logger:
                self.logger.disabled = False
                file_handler = logging.FileHandler(self.logger_file, mode="w")  # 'w' mode overwrites the file
                self.logger.addHandler(file_handler)
                self.logger.setLevel(logging.DEBUG)
                self.logger.debug("[parse_deco] enable_logger = True")
                self.logger.debug("[parse_deco] job_list: " + str(self.job_list))
                self.logger.debug("[parse_deco] group_list: " + str(self.group_list))
                # self.logger.debug("[parse_deco] inc_history: " + misc.json_dump_str(inc_history))
                # self.logger.debug("[parse_deco] target_db: \n" + misc.json_dump_str(self.target_db))
            else:
                self.logger.disabled = True

            self.load_conf_dict()
            assert self.confd != None
            # print(self.confd)

            self.job_list = []
            self.group_list = []

            self.target_db = self.confd['Deps']
            self.logger.debug("[parse_deco] target_db: \n" + misc.json_dump_str(self.target_db))

            self.init_pyactd()
            ret = func(*args, **kwargs)

            self.logger.disabled = True
            return ret
        return wrapper

    @parse_deco
    def parse(self, tname: str = None, enable_logger=False) -> Self:
        self.logger.debug("[parse] start, target_name = " + str(tname))
        if tname == None:
            tname = 'ALL'
            k_list = list(self.target_db.keys())
            for k in k_list:
                self.dep_act(k)
        else:
            self.dep_act(tname)
        
        self.dep_act_start()
        return self
    
    @parse_deco
    def parse_group(self, group_id: str = None, enable_logger=False) -> Self:
        self.logger.debug("[parse_group] start")
        self.dep_group(group_id)

        self.dep_act_start()

        self.logger.disabled = True
        return self

# mys
def prj_dep(tname: str = None):
    PrjDep(fs.pwd()).parse(tname)

# mys
def prj_depg(group: str = None):
    PrjDep(fs.pwd()).parse_group(group)

# mys
def prj_dep_ls():
    pd = PrjDep(fs.pwd())
    # pd.load_conf_dict_recursive([])
    print(misc.json_dump_str(pd.confd))