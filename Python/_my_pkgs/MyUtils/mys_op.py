from MyUtils import str_op, ast_op

'''
Input 1:
  {
    "name": "greet",
    "lineno": 3,
    "args": [
      {
        "name": "name",
        "anno": "str"
      }
    ],
    "ret_anno": "None"
  },

Input 2:
# mys [OP_NAME] [Print_OP]

Output:
OP_NAME : list[func, Return_Print_OP, return, arg1, arg2, ...]

greet : [greet, print, None, str]

Default OP:
-

List of Print_OP:
print (default)
print_none
'''

def mys_gen_entries(funcs : list[ast_op.FuncPara], prev_lines : dict[str : str]) -> list[str]:
    entries = list()
    for f in funcs:
        if f['name'] in prev_lines:
            pl = prev_lines[f['name']]
            # print(pl)
            s = pl.split()
            print(s)
            if len(s) < 2:
                continue
            if s[0] != '#' or s[1] != 'mys':
                continue

            ret_anno = f['ret_anno']
            op_name = None
            print_op = 'print'

            #
            # Positional arguments
            #
            if len(s) >= 3:
                op_name = s[2]
            if len(s) >= 4:
                if s[3] == '-':
                    print_op = 'print'
                else:
                    print_op = s[3]

            #
            # Argument annotation overwrite
            #
            arg_ow = dict()
            if len(s) > 4:
                arg_str = s[4:]
                arg_str_len = len(arg_str)
                assert arg_str_len % 2 == 0, f'arg_str_len is not even number: {arg_str_len}'
                for i in range(0, len(arg_str), 2):
                    assert len(arg_str[i]) == 4, f'arg_str[i] is not arg1 ~ arg9: {arg_str[i]}, i = {i}'
                    assert arg_str[i][0:3] == 'arg', f'first 3 is not "arg"'
                    index = int(arg_str[i][3])
                    arg_ow[index] = f'"{arg_str[i + 1]}"'
            
            if op_name == None or op_name == '-':
                op_name = f['name']
            
            entry = f'"{op_name}" : [' + f['name'] + f', "{print_op}", {ret_anno}'
            i = 1 # start from arg1
            for a in f['args']:
                anno = a['anno']
                if anno == None:
                    anno = 'None'
                # overwrite
                if i in arg_ow:
                    anno = arg_ow[i]
                entry = entry + ', ' + anno
                i += 1
            entry += '],'
            # print('entry =', entry)
            entries.append(entry)
    return entries