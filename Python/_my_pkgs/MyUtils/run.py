
def run_cmd_to_buf(cmd):
    if isinstance(cmd, str):
        cmd = cmd.split()
    else:
        pass
    import subprocess
    return subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')

def run_cmd(cmd) -> int:
    import os
    return os.system(cmd)

# TODO
# Experimental, must use run_bash(), but has timing issue
def tee_stdout_n_stderr(file1, file2) -> str: # pragma no cover
    return f' 1> >(tee {file1} ) 2> >(tee {file2} >&2 )'

# TODO
def run_bash(cmd : str) -> int: # pragma no cover
    #
    # Main:
    # https://stackoverflow.com/questions/21822054/how-to-force-os-system-to-use-bash-instead-of-shell
    #
    # Wait:
    # https://stackoverflow.com/questions/2270081/bash-script-calls-another-bash-script-and-waits-for-it-to-complete-before-procee
    #
    bash_cmd = f'GREPDB="{cmd}"; /bin/bash -c "$GREPDB; "'

    import os
    return os.system(bash_cmd)

    # https://stackoverflow.com/questions/8246520/does-pythons-os-system-wait-for-an-end-of-the-process
    #
    # os.system: On Mac it waits the end of subprocess, but on Linux it does not.
    #
    # import subprocess
    # subprocess.run(bash_cmd)
