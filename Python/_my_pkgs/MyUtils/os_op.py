import time

def time_sleep_sec(seconds : float) -> None:  
    time.sleep(seconds)

def time_curr_sec() -> float:
    return time.time()