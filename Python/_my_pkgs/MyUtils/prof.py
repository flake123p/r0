import time

'''
Use case:

prof = Prof()
# Your func
prof.show('MY datetime np with numpy f64')
'''
class ProfilingManager:
    def __init__(self):
        self.start = time.time()

    def show(self, prefix : str = 'N/A', w = 50):
        elapsed = time.time() - self.start
        print(f'{prefix:{w}}' + 'Elapsed Time = {:.6f} ms'.format(elapsed * 1000))

PM = ProfilingManager

'''
Use case:

prof(lambda : [indexing_n_sum(dates, dates, val)    for _ in N], 'datetime with list')
prof(lambda : [indexing_n_sum(dates, dates, val_np) for _ in N], 'datetime with np_f64')
'''
def ProfilingFunc(func, prefix = '', w = 50):
    p = ProfilingManager()
    func()
    p.show(prefix, w)

def Profilling(enable=True, prefix='', w=50):
    def decorator(func):
        def wrapper(*args, **kwargs):
            p = ProfilingManager() if enable else None
            ret = func(*args, **kwargs)
            p.show() if enable else None
            return ret
        return wrapper
    return decorator

#
# New Function for testing between installed package and absolute path (dynamic)importing
#
# def NewFunc():
#     print('NewFunc executed!')
#     pass