from typing import Union, Optional, Tuple, Type, TypedDict

# mys is_any_failed
def Check_IsAnyFailed(file: str) -> bool:
    with open(file, 'r') as f:
        for l in f:
            s = l.split()
            if len(s):
                if s[0] == '[FAILED]':
                    return True
                elif s[0] == 'FAILED':
                    return True
    return False

# mys total_cov
def Check_TotalCoverage(file: str) -> Optional[str]:
    with open(file, 'r') as f:
        for l in f:
            s = l.split()
            if len(s):
                if s[0] == 'TOTAL' and len(s) == 4:
                    return s[3]
    return None

# mys is_any_string
def Check_IsAnyString(file: str) -> bool:
    with open(file, 'r') as f:
        for l in f:
            return True
    return False

# mys is_any_python_error
def Check_IsAnyPythonError(file: str) -> bool:
    with open(file, 'r') as f:
        for l in f:
            if 'Errno' in l:
                return True
            if 'Traceback' in l:
                return True
            if 'SyntaxError' in l:
                return True
    return False

# mys num_of_100
def Check_NumOf100Percent(file: str) -> int:
    ctr = 0
    with open(file, 'r') as f:
        for l in f:
            s = l.split()
            if len(s):
                if s[0] == '[100%]':
                    ctr += 1
    return ctr

# mys num_of_string
def Check_NumOfString(file: str, pattern: str) -> int:
    ctr = 0
    with open(file, 'r') as f:
        for l in f:
            if pattern in l:
                ctr += 1
    return ctr

# mys is_any_stderr_error
def Check_IsAnyStderrError(file: str) -> bool:
    with open(file, 'r') as f:
        for l in f:
            if 'Segmentation fault' in l:
                return True
            if 'core dumped' in l:
                return True
    return False

def Check_LineNum(file: str) -> int:
    with open(file, 'r') as f:
        lines = f.read().splitlines()
        return len(lines)

def Check_OneLineCompare(file: str, pattern: str) -> bool:
    with open(file, 'r') as f:
        lines = f.read().splitlines()
        if len(lines) != 1:
            return False
        if lines[0] == pattern:
            return True
    return False

def Check_WholeFileCompare(file: str, pattern: str) -> bool:
    with open(file, 'r') as f:
        fr = f.read()
        if fr == pattern:
            return True
    return False