import setuptools
from setup_mys import *

if __name__ == "__main__":

    with open('setup.log', 'w') as f:
        pkgs = setuptools.find_packages()
        # pkgs=['MyTester', 'MyScripts', 'MyUtils'],
        print('Installed pkgs =', pkgs)
        f.write('Installed pkgs =' + str(pkgs) + '\n\n')
        mys_init()

    setuptools.setup(     
        name="MyPkgs",
        version="0.0.1",
        python_requires=">=3.9",
        packages=setuptools.find_packages(),
        # packages=['MyTester', 'MyScripts', 'MyUtils'],

        entry_points = {
            'console_scripts': [
                'my_hello_world = MyScripts.my_hello_world:console_main',
                'my_goodbye     = MyScripts.my_hello_world:console_main',
                'mys            = MyScripts.mys:console_main'
            ],              
        },
    )