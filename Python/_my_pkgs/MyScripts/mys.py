import argparse
import MyUtils
import sys
from MyScripts.mys_auto import *

PRINT_OP_LIST = [
    'print', 
    'print_none', 
    'print_list',
    'print_json',
]

def read_as_str_list(path : str) -> list:
    ret = list()
    with open(path) as f:
        for l in f:
            l = MyUtils.str_replace(l, ['\n'], '')
            ret.append(str(l))
    return ret

# func_table = {
#     'pyconv' : MyUtils.python_coverage_scan_total
# }

def arg_to_types(str_arg, type):
    if type == "str_split":
        return str_split(str_arg)
    elif type == bool:
        return bool(int(str_arg))
    elif type == None:
        return str(str_arg)
    elif type == list[str]:
        return read_as_str_list(str_arg)
    elif type == Union[str, list[str]]:
        return str(str_arg)
    else:
        return type(str_arg)

def console_main():
    argc = len(sys.argv)

    desc = 'Commands:\n'
    for k in entries_auto:
        desc += ('  ' + k + '\n')
    
    parser = argparse.ArgumentParser(epilog=desc, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("op", help="Operation")
    parser.add_argument("arg1", nargs="?", help="[Optional] 1st argment for operation", default=None)
    parser.add_argument("arg2", nargs="?", help="[Optional] 2nd argment for operation", default=None)
    parser.add_argument("arg3", nargs="?", help="[Optional] 3rd argment for operation", default=None)
    parser.add_argument("arg4", nargs="?", help="[Optional] 4th argment for operation", default=None)
    parser.add_argument("arg5", nargs="?", help="[Optional] 5th argment for operation", default=None)
    parser.add_argument("arg6", nargs="?", help="[Optional] 6th argment for operation", default=None)
    args = parser.parse_args()

    # print(f"{args.op} {args.arg1} {args.arg2}")
    

    try:
        if args.op == 'pycov':
            ret = MyUtils.python_coverage_scan_total(args.arg1)
            print(ret)
            # sys.exit(ret)
        elif args.op in entries_auto:
            en = entries_auto[args.op]
            
            # print('len en = ', len(en), argc)

            assert len(en) >= 3 and len(en) <= 9, f'en = {en}'
            assert argc >= 2 and len(en) <= 8, f'argc = {argc}'

            a1 = arg_to_types(args.arg1, en[3]) if argc >= 3 else None
            a2 = arg_to_types(args.arg2, en[4]) if argc >= 4 else None
            a3 = arg_to_types(args.arg3, en[5]) if argc >= 5 else None
            a4 = arg_to_types(args.arg4, en[6]) if argc >= 6 else None
            a5 = arg_to_types(args.arg5, en[7]) if argc >= 7 else None
            a6 = arg_to_types(args.arg6, en[8]) if argc >= 8 else None

            if len(en) == 3:
                ret = en[0]()
            elif len(en) == 4:
                ret = en[0](a1)
            elif len(en) == 5:
                ret = en[0](a1, a2)
            elif len(en) == 6:
                ret = en[0](a1, a2, a3)
            elif len(en) == 7:
                ret = en[0](a1, a2, a3, a4)
            elif len(en) == 8:
                ret = en[0](a1, a2, a3, a4, a5)
            elif len(en) == 9:
                ret = en[0](a1, a2, a3, a4, a5, a6)
            
            print_en = True
            print_op = en[1]
            assert (print_op in PRINT_OP_LIST) == True, f'print_op = {print_op}'
            ret_anno = en[2]
            if print_op == "print_none":
                print_en = False
            if ret_anno == None: # return annotation
                print_en = False
            
            if print_en:
                if print_op == 'print_list':
                    for r in ret:
                        print(r)
                elif print_op == 'print_json':
                    print(MyUtils.json_dump_str(ret))
                else:
                    print(ret)
        else:
            assert 0, f'OP: {args.op} doesn\'t supported.'
    except BaseException as err:
        print('Exception -', err.__class__.__name__, ':', err)
    

if __name__ == '__main__':
    console_main()