import sys

def my_hello_world(to_someone : str = 'None'):
    print('My Hello World to', to_someone)

def my_bye_bye():
    print('Bye~~')

def console_main():
    # print("Raw arguments:", sys.argv)
    if sys.argv[0].split('/')[-1] == 'my_goodbye':
        my_bye_bye()
    else:
        if len(sys.argv) > 1:
            my_hello_world(sys.argv[1])
        else:
            my_hello_world()