import pytest
import sys
sys.path.append('../MyUtils/')

def test_prof():
    import prof
    print() # new line for pytest -s
    p = prof.ProfilingManager()
    p.show('PROF DEMO')
    prof.ProfilingFunc(lambda : [_ for _ in [3]], 'PROF DEMO2') 

    @prof.Profilling()
    def a():
        print('PROF DEMO3')
    a()

    @prof.Profilling(enable=False)
    def b():
        print('PROF DEMO4')
    b()

def test_check():
    DUMMY_FILE = 'dummy.log'
    import check, io
    with open(DUMMY_FILE, 'w') as f:
        f.write('abc def\n')
        f.write('123 456\n')
        f.write('[FAILED]\n')
    assert check.Check_IsAnyFailed(DUMMY_FILE) == True
    with open(DUMMY_FILE, 'w') as f:
        f.write('abc def\n')
        f.write('FAILED abc\n')
    assert check.Check_IsAnyFailed(DUMMY_FILE) == True
    with open(DUMMY_FILE, 'w') as f:
        f.write('abc def\n')
        f.write('FAILED...\n')
    assert check.Check_IsAnyFailed(DUMMY_FILE) == False
    assert check.Check_TotalCoverage(DUMMY_FILE) == None

    with open(DUMMY_FILE, 'w') as f:
        f.write('abc def\n')
        f.write('FAILED...\n')
        f.write('TOTAL       112     56    50%\n')
    assert check.Check_TotalCoverage(DUMMY_FILE) == '50%'
    
    with open(DUMMY_FILE, 'w') as f:
        f.write('abc def\n')
    assert check.Check_IsAnyString(DUMMY_FILE) == True

    with open(DUMMY_FILE, 'w') as f:
        pass
    assert check.Check_IsAnyString(DUMMY_FILE) == False
    
    assert check.Check_IsAnyFailed('pytest.ini') == False

    #
    # Python Error
    #
    with open(DUMMY_FILE, 'w') as f:
        f.write('abc def\n')
    assert check.Check_IsAnyPythonError(DUMMY_FILE) == False

    with open(DUMMY_FILE, 'w') as f:
        f.write('abc def [Errno]\n')
    assert check.Check_IsAnyPythonError(DUMMY_FILE) == True

    with open(DUMMY_FILE, 'w') as f:
        f.write('abc def [Traceback]\n')
    assert check.Check_IsAnyPythonError(DUMMY_FILE) == True

    with open(DUMMY_FILE, 'w') as f:
        f.write('abc def [SyntaxError]\n')
    assert check.Check_IsAnyPythonError(DUMMY_FILE) == True
    
    #
    # 100%
    #
    with open(DUMMY_FILE, 'w') as f:
        f.write('abc def [Traceback]\n')
    assert check.Check_NumOf100Percent(DUMMY_FILE) == 0
    with open(DUMMY_FILE, 'w') as f:
        f.write('abc def [Traceback]\n')
        f.write('[100%]\n') # count
        f.write('[100%]a\n') # doesn't count
        f.write('[100%] a\n') # count
    assert check.Check_NumOf100Percent(DUMMY_FILE) == 2
    
    #
    # String Pattern
    #
    with open(DUMMY_FILE, 'w') as f:
        f.write('abc def [Traceback]\n')
        f.write('[100%] Built target\n') # count
        f.write('[100%] Built targeta\n') # count
        f.write('[100%] Built target a\n') # count
    assert check.Check_NumOfString(DUMMY_FILE, '[100%] Built target') == 3
    
    assert check.Check_NumOfString('pytest.ini', '[100%] Built target') == 0



def test_misc():
    print() # new line for pytest -s
    from misc import DISP, SHOW, try_catch, import_abs_pkg, import_abs_mod
    x = 3
    DISP(x)
    x = dict()
    x['a'] = 10
    x['b'] = 20
    DISP(x, 'xx', 1)
    x = [10, 20]
    SHOW(x, 1)

    def func():
        raise BaseException('HA HA HA')
    
    with pytest.raises(BaseException, match=r".* HA .*"):
        func()

    try_catch(func)

def test_misc2():
    print() # new line for pytest -s
    from misc import import_abs_mod
    prof = import_abs_mod('prof', '../MyUtils/')
    p = prof.ProfilingManager()
    p.show('PROF DEMO A')

def test_misc3():
    print() # new line for pytest -s
    from misc import import_abs_pkg
    ut = import_abs_pkg('MyUtils', '../')
    p = ut.ProfilingManager()
    p.show('PROF DEMO B')

def test_misc4_json():
    import misc
    x = {'a' : 123}
    misc.json_dump(x, 'dummy.log')
    y = misc.json_load('dummy.log')
    assert x == y

def test_misc5_json_dumps():
    import misc
    dumps = misc.json_dump_str(['aa', 'bb'])
    assert len(dumps) == 18
    assert dumps == '[\n  "aa",\n  "bb"\n]'
    dumps = misc.json_dump_str(['aa', 'bb'], indent=4)
    assert len(dumps) == 22
    assert dumps == '[\n    "aa",\n    "bb"\n]'
    

def test_run():
    # print() # new line for pytest -s
    import run
    run.run_cmd('echo')
    run.run_cmd('echo 123')

    buf = run.run_cmd_to_buf(['echo', '123'])
    assert len(buf) == 2
    assert buf[0] == '123'
    assert buf[-1] == ''

    buf = run.run_cmd_to_buf('echo 123')
    assert len(buf) == 2
    assert buf[0] == '123'
    assert buf[-1] == ''

if __name__ == '__main__': # pragma: no cover
    import fs
    fs.mkdir('GHOST')
    fs.file_create('GHOST/1.txt')
    # fs.rmdir('GHOST')
    fs.file_remove('GHOST/1.txt')
    # fs.file_remove('GHOST/1.txt')
    print('hello')

    import misc
    x = {'a' : 123}
    misc.json_dump(x, 'dummy.log')
    y = misc.json_load('dummy.log')
    assert x == y

    if 1:
        import cov
        import str_op
        s = cov.lcov_summary_list_file('testdata/mod/cov/00_lcov_list.txt')

        print(misc.json_dump_str(s))

        cov.lcov_records_dump(s)

        # print(str_op.str_split("apple;orange,banana|grape:melon", [';', ',', '|', ':']))

        # delimiters = r"[;,|:"

        # delimiters += 'a]'

        # print(delimiters)