import pytest
import sys
sys.path.append('../MyUtils/')

def test__PrjDep():
    import fs
    import prj

    pd = prj.PrjDep(path=fs.join(fs.pwd(), 'testdata/mod/prj_dep/00_basic'))

    assert fs.exists('testdata/log.123.log') == False
    pd.parse('123')
    assert fs.exists('testdata/log.123.log') == True
    fs.file_remove('testdata/log.123.log')

    assert fs.exists('testdata/log.abc.log') == False
    pd.parse('abc')
    assert fs.exists('testdata/log.abc.log') == True
    fs.file_remove('testdata/log.abc.log')

    assert fs.exists('testdata/log.456.log') == False
    assert fs.exists('testdata/log.abc.log') == False
    pd.parse('456')
    assert fs.exists('testdata/log.456.log') == True
    assert fs.exists('testdata/log.abc.log') == True
    fs.file_remove('testdata/log.456.log')
    fs.file_remove('testdata/log.abc.log')

    # assert fs.exists('testdata/log.pyact.log') == False
    # pd.parse('pyact')
    # assert fs.exists('testdata/log.pyact.log') == True
    # fs.file_remove('testdata/log.pyact.log')

    #
    # TS_Prereq
    #
    f1 = 'testdata/log.def.log'
    f2 = 'testdata/log.def.false.log'
    assert fs.exists(f1) == False
    assert fs.exists(f2) == False
    pd.parse(f1)
    assert fs.exists(f1) == True
    assert fs.exists(f2) == False
    pd.parse(f1)
    assert fs.exists(f2) == True
    fs.file_remove(f1)
    fs.file_remove(f2)
    # def
    assert fs.exists(f1) == False
    assert fs.exists(f2) == False
    pd.parse('def')
    assert fs.exists(f1) == True
    assert fs.exists(f2) == False
    pd.parse('def')
    assert fs.exists(f2) == True
    fs.file_remove(f1)
    fs.file_remove(f2)

    #
    # Group
    #
    fxxx = 'testdata/log.xxx.log'
    fyyy = 'testdata/log.yyy.log'
    fzzz = 'testdata/log.zzz.log'
    assert fs.exists(fxxx) == False
    assert fs.exists(fyyy) == False
    assert fs.exists(fzzz) == False
    pd.parse('G00')
    assert fs.exists(fxxx) == True
    assert fs.exists(fyyy) == True
    assert fs.exists(fzzz) == False
    fs.file_remove(fxxx)
    fs.file_remove(fyyy)

    pd.parse('G01')
    assert fs.exists(fxxx) == False
    assert fs.exists(fyyy) == True
    assert fs.exists(fzzz) == True
    fs.file_remove(fyyy)
    fs.file_remove(fzzz)

    pd.parse('GALL')
    assert fs.exists(fxxx) == True
    assert fs.exists(fyyy) == True
    assert fs.exists(fzzz) == True
    fs.file_remove(fxxx)
    fs.file_remove(fyyy)
    fs.file_remove(fzzz)

    # target recursive test
    ftr0 = 'testdata/log.tr0.log'
    ftr1 = 'testdata/log.tr1.log'
    ftr2 = 'testdata/log.tr2.log'
    assert fs.exists(ftr0) == False
    assert fs.exists(ftr1) == False
    assert fs.exists(ftr2) == False
    pd.parse('TR0')
    assert fs.exists(ftr0) == True
    assert fs.exists(ftr1) == True
    assert fs.exists(ftr2) == True
    fs.file_remove(ftr0)
    fs.file_remove(ftr1)
    fs.file_remove(ftr2)
    #
    assert fs.exists(ftr0) == False
    assert fs.exists(ftr1) == False
    assert fs.exists(ftr2) == False
    pd.parse('TR1')
    assert fs.exists(ftr0) == True
    assert fs.exists(ftr1) == True
    assert fs.exists(ftr2) == True
    fs.file_remove(ftr0)
    fs.file_remove(ftr1)
    fs.file_remove(ftr2)
    #
    assert fs.exists(ftr0) == False
    assert fs.exists(ftr1) == False
    assert fs.exists(ftr2) == False
    pd.parse('TR2')
    assert fs.exists(ftr0) == True
    assert fs.exists(ftr1) == True
    assert fs.exists(ftr2) == True
    fs.file_remove(ftr0)
    fs.file_remove(ftr1)
    fs.file_remove(ftr2)

    # group recursive test
    fgr0 = 'testdata/log.gr0.log'
    fgr1 = 'testdata/log.gr1.log'
    fgr2 = 'testdata/log.gr2.log'
    assert fs.exists(fgr0) == False
    assert fs.exists(fgr1) == False
    assert fs.exists(fgr2) == False
    pd.parse('GR0')
    assert fs.exists(fgr0) == True
    assert fs.exists(fgr1) == True
    assert fs.exists(fgr2) == True
    fs.file_remove(fgr0)
    fs.file_remove(fgr1)
    fs.file_remove(fgr2)

    assert fs.exists(fgr0) == False
    assert fs.exists(fgr1) == False
    assert fs.exists(fgr2) == False
    pd.parse('GR1')
    assert fs.exists(fgr0) == False
    assert fs.exists(fgr1) == True
    assert fs.exists(fgr2) == False
    fs.file_remove(fgr1)

    assert fs.exists(fgr0) == False
    assert fs.exists(fgr1) == False
    assert fs.exists(fgr2) == False
    pd.parse('GR2')
    assert fs.exists(fgr0) == True
    assert fs.exists(fgr1) == True
    assert fs.exists(fgr2) == True
    fs.file_remove(fgr0)
    fs.file_remove(fgr1)
    fs.file_remove(fgr2)

def test__PrjDep_Include():
    import fs
    import prj

    pd = prj.PrjDep(
        path=fs.join(fs.pwd(), 'testdata/mod/prj_dep/01_include'),
        logger_file=fs.join(fs.pwd(), 'log.prj_dep.01_include.log'))

    faaa = 'testdata/log.aaa.log'
    fbbb = 'testdata/log.bbb.log'
    fccc = 'testdata/log.ccc.log'
    assert fs.exists(faaa) == False
    assert fs.exists(fbbb) == False
    assert fs.exists(fccc) == False
    pd.parse('aaa', enable_logger=False)
    assert fs.exists(faaa) == True
    assert fs.exists(fbbb) == True
    assert fs.exists(fccc) == True
    fs.file_remove(faaa)
    fs.file_remove(fbbb)
    fs.file_remove(fccc)

    assert fs.exists(faaa) == False
    assert fs.exists(fbbb) == False
    assert fs.exists(fccc) == False
    pd.parse_group('G00', enable_logger=False)
    assert fs.exists(faaa) == True
    assert fs.exists(fbbb) == True
    assert fs.exists(fccc) == True
    fs.file_remove(faaa)
    fs.file_remove(fbbb)
    fs.file_remove(fccc)

    assert fs.exists(faaa) == False
    assert fs.exists(fbbb) == False
    assert fs.exists(fccc) == False
    pd.parse('G00')
    assert fs.exists(faaa) == True
    assert fs.exists(fbbb) == True
    assert fs.exists(fccc) == True
    fs.file_remove(faaa)
    fs.file_remove(fbbb)
    fs.file_remove(fccc)

def test__PrjDep_Include_Duplicate():
    import fs
    import prj

    pd = prj.PrjDep(
        path=fs.join(fs.pwd(), 'testdata/mod/prj_dep/02_include_duplicate_key'),
        logger_file=fs.join(fs.pwd(), 'log.prj_dep.02_include_duplicate_key.log'))
    
    with pytest.raises(KeyError, match="Duplicate key found: aaa"):
        pd.parse('aaa')

def test__PrjDep_Include_Recursive():
    import fs
    import prj

    pd = prj.PrjDep(
        path=fs.join(fs.pwd(), 'testdata/mod/prj_dep/03_include_recursive'),
        logger_file=fs.join(fs.pwd(), 'log.prj_dep.03_include_recursive.log'))
    
    pd.parse('ccc', enable_logger=False)

def test__PrjDep_TargetAct():
    import fs
    import prj

    pd = prj.PrjDep(
        path=fs.join(fs.pwd(), 'testdata/mod/prj_dep/04_tgt_act'),
        logger_file=fs.join(fs.pwd(), 'log.prj_dep.04_tgt_act.log'))
    
    faaa = 'testdata/log.04_tgt_act.aaa.log'
    fbbb = 'testdata/log.04_tgt_act.bbb.log'
    fccc = 'testdata/log.04_tgt_act.ccc.log'

    assert fs.all_not_exists([faaa, fbbb, fccc]) == True
    pd.parse(faaa, enable_logger=False)
    assert fs.all_exists([faaa, fbbb]) == True
    assert fs.exists(fccc) == False
    pd.parse(faaa, enable_logger=False)
    assert fs.exists(fccc) == True
    fs.file_remove([faaa, fbbb, fccc])


def test__PrjDep_GroupAct():
    import fs
    import prj

    pd = prj.PrjDep(
        path=fs.join(fs.pwd(), 'testdata/mod/prj_dep/05_group_act'),
        logger_file=fs.join(fs.pwd(), 'log.prj_dep.05_group_act.log'))
    
    faa = 'testdata/log.05_group_act.aa.log'
    fb0 = 'testdata/log.05_group_act.b0.log'
    fb1 = 'testdata/log.05_group_act.b1.log'
    fc0 = 'testdata/log.05_group_act.c0.log'
    fc1 = 'testdata/log.05_group_act.c1.log'
    fd0 = 'testdata/log.05_group_act.d0.log'
    assert fs.all_not_exists([faa, fb0, fb1, fc0, fc1, fd0]) == True

    pd.parse(faa, enable_logger=False)
    assert fs.exists(faa) == True
    assert fs.exists(fb0) == True
    assert fs.exists(fb1) == True
    pd.parse(faa, enable_logger=False)
    assert fs.exists(fc0) == True
    assert fs.exists(fc1) == True
    assert fs.exists(fd0) == True

    fs.file_remove([faa, fb0, fb1, fc0, fc1, fd0])
    assert fs.all_not_exists([faa, fb0, fb1, fc0, fc1, fd0]) == True

    fs.exists_check({faa : False}, fail_exc=True)

def test__PrjDep_PyAct():
    import fs
    import prj
    import check

    pd = prj.PrjDep(
        path=fs.join(fs.pwd(), 'testdata/mod/prj_dep/06_py_act'),
        logger_file=fs.join(fs.pwd(), 'log.prj_dep.06_py_act.log'))
    
    f0 = 'testdata/mod/prj_dep/06_py_act/log.pyact.log'
    f1 = 'testdata/mod/prj_dep/06_py_act/log.pyact1.log'

    assert fs.all_not_exists([f0, f1]) == True

    pd.parse('pyact0', enable_logger=False)
    assert fs.exists(f0) == True
    assert check.Check_WholeFileCompare(f0, 'xxx') == True
    fs.file_remove(f0)

    pd.parse('pyact1')
    assert fs.exists(f1) == True
    assert check.Check_WholeFileCompare(f1, '1\nyes\nhello world p1\nTrue') == True
    fs.file_remove(f1)

def test__PrjDep_PyAct_UserHandle():
    pass
"""
TODO:

stoppin chain
false act

TGT_Action

"""
if __name__ == '__main__': # pragma: no cover
    ...
    import fs
    import prj
    import check

    pd = prj.PrjDep(
        path=fs.join(fs.pwd(), 'testdata/mod/prj_dep/06_py_act'),
        logger_file=fs.join(fs.pwd(), 'log.prj_dep.06_py_act.log'))

    pd.parse('pyactA0', enable_logger=False)
