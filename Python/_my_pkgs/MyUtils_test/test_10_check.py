import pytest
import sys
sys.path.append('../MyUtils/')

def test__Check_IsAnyStderrError():
    import check
    assert check.Check_IsAnyStderrError('testdata/mod/check/stderr_0_no_err.txt') == False
    assert check.Check_IsAnyStderrError('testdata/mod/check/stderr_1_seg_A.txt') == True
    assert check.Check_IsAnyStderrError('testdata/mod/check/stderr_1_seg_B.txt') == True
    assert check.Check_IsAnyStderrError('testdata/mod/check/stderr_1_seg_C.txt') == True

def test__Check_LineNum():
    import check
    assert check.Check_LineNum('testdata/mod/check/line_num_A.txt') == 1
    assert check.Check_LineNum('testdata/mod/check/line_num_B.txt') == 4
    assert check.Check_LineNum('testdata/mod/check/line_num_C.txt') == 9
    assert check.Check_LineNum('testdata/mod/check/line_num_D.txt') == 12
    assert check.Check_LineNum('testdata/mod/check/line_num_E.txt') == 10
    assert check.Check_LineNum('testdata/mod/check/line_num_F.txt') == 1
    assert check.Check_LineNum('testdata/mod/check/line_num_G.txt') == 0

def test__Check_OneLineCompare():
    import check
    assert check.Check_OneLineCompare('testdata/mod/check/one_line_cmp_A.txt', 'ABC') == False # multi-lines
    assert check.Check_OneLineCompare('testdata/mod/check/one_line_cmp_B.txt', 'ABC') == False # Pattern does not match
    assert check.Check_OneLineCompare('testdata/mod/check/one_line_cmp_C.txt', 'ABC') == True # Pattern matches (wo new line)
    assert check.Check_OneLineCompare('testdata/mod/check/one_line_cmp_D.txt', 'ABC') == True # Pattern matches (w new line)

def test__Check_WholeFileCompare():
    import check
    assert check.Check_WholeFileCompare('testdata/mod/check/one_line_cmp_A.txt', 'ABC\nABC\nABC') == True
    assert check.Check_WholeFileCompare('testdata/mod/check/one_line_cmp_A.txt', 'ABC\nABC\nABC\n') == False
    assert check.Check_WholeFileCompare('testdata/mod/check/one_line_cmp_B.txt', 'ABCC') == True
    assert check.Check_WholeFileCompare('testdata/mod/check/one_line_cmp_D.txt', 'ABC\n') == True
    assert check.Check_WholeFileCompare('testdata/mod/check/one_line_cmp_D.txt', 'ABC') == False

if __name__ == '__main__': # pragma: no cover
    ...

    