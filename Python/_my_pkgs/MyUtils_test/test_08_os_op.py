import pytest
import sys
sys.path.append('../MyUtils/')

def test__sleep_sec():
    import os_op
    t = os_op.time_curr_sec()
    os_op.time_sleep_sec(0.1)
    t = os_op.time_curr_sec() - t
    # 10% tolerance
    assert t >= 0.09

if __name__ == '__main__': # pragma: no cover
    ...
    import os_op
    t = os_op.time_curr_sec()
    print("Start :", t, type(t))
    os_op.time_sleep_sec(0.1)
    t = os_op.time_curr_sec() - t
    print("End :", t)
    # 10% tolerance
    assert t >= 0.09
    