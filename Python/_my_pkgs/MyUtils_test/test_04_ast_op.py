import pytest
import sys
sys.path.append('../MyUtils/')
sys.path.append('../')

golden = [
  {
    "name": "greet",
    "lineno": 3,
    "args": [
      {
        "name": "name",
        "anno": "str"
      }
    ],
    "ret_anno": "None"
  },
  {
    "name": "add",
    "lineno": 7,
    "args": [
      {
        "name": "a",
        "anno": "int"
      },
      {
        "name": "b",
        "anno": "int"
      }
    ],
    "ret_anno": "int"
  },
  {
    "name": "no_args",
    "lineno": 14,
    "args": [],
    "ret_anno": "str"
  }
]

golden2 = [
  {
    "name": "greet",
    "lineno": 1,
    "args": [
      {
        "name": "name",
        "anno": "str"
      }
    ],
    "ret_anno": "None"
  }
]

def test_ast_op():
    import ast_op
    ret = ast_op.ast_parse_file('testdata/mod/ast_op/02_example.py')
    assert ret == golden

    ret = ast_op.ast_get_prev_line('testdata/mod/ast_op/02_example.py', ret)
    prev_line_golden = {
        'greet'   : '# mys',
        'add'     : '# mys - print',
        'no_args' : '# mys',
    }
    assert ret == prev_line_golden

def test_ast_op2():
    import ast_op
    ret = ast_op.ast_parse_file('testdata/mod/ast_op/03_example.py')
    assert ret == golden2

    ret = ast_op.ast_get_prev_line('testdata/mod/ast_op/03_example.py', ret)
    prev_line_golden = {
    }
    assert ret == prev_line_golden

def test_ast_op3():
    import ast_op
    ret = ast_op.ast_parse_file('testdata/mod/ast_op/04_example.py')
    ret = ast_op.ast_get_prev_line('testdata/mod/ast_op/04_example.py', ret)
    prev_line_golden = {
    }
    assert ret == prev_line_golden

def test_ast_op4():
    import ast_op
    ret = ast_op.ast_parse_file('testdata/mod/ast_op/05_example.py')
    ret = ast_op.ast_get_prev_line('testdata/mod/ast_op/05_example.py', ret)
    prev_line_golden = {
        'greet2'   : '# mys',
    }
    assert ret == prev_line_golden
  
def test_ast_op5_decorator():
    import ast_op, fs
    file = '01_example.py'
    dir = 'testdata/mod/ast_op'
    ret = ast_op.ast_parse_file(fs.join(dir, file))
    ret = ast_op.ast_get_prev_line(fs.join(dir, file), ret)
    prev_line_golden = {'foo2': '#mys', 'foo3': '#mys'}
    assert ret == prev_line_golden

def test_ast_op_SkipClassMethod():
    import ast_op, fs
    file = '07_example.py'
    dir = 'testdata/mod/ast_op'
    ret = ast_op.ast_parse_file(fs.join(dir, file))
    # import misc
    # print(misc.json_dump_str(ret))
    ret = ast_op.ast_get_prev_line(fs.join(dir, file), ret)
    # print(ret)
    golden = {'FsExc': '', 'wrapper': 'def FsExc(func):', 'currdir': '', 'find_file': '', 'dirname': '# mys', 'basename': '# mys', 'pwd': '# mys - print', 'join': '# mys', 'join2': '# mys'}
    assert ret == golden

if __name__ == '__main__': # pragma: no cover
    ...
    import ast_op, fs

    # file = '../MyUtils/prof.py'

    # ret = ast_op.ast_parse_file(file)

    # import misc
    # print(misc.json_dump_str(ret))

    # print(ret == golden)

    # ret = ast_op.ast_get_prev_line(file, ret)
    # for k in ret:
    #     print(k, ret[k])

    file = '07_example.py'
    dir = 'testdata/mod/ast_op'
    ret = ast_op.ast_parse_file(fs.join(dir, file))

    import misc
    print(misc.json_dump_str(ret))

    ret = ast_op.ast_get_prev_line(fs.join(dir, file), ret)
    print(ret)

    golden = {'FsExc': '', 'wrapper': 'def FsExc(func):', 'currdir': '', 'find_file': '', 'dirname': '# mys', 'basename': '# mys', 'pwd': '# mys - print', 'join': '# mys', 'join2': '# mys'}
    assert ret == golden