import pytest
import sys
sys.path.append('../MyUtils/')

def TODO_test__tee_stdout_n_stderr(): # pragma: no cover
    LET_TEST_PASS = True
    import run
    import fs
    fs.file_remove('1.log')
    fs.file_remove('2.log')
    ret = run.run_bash('xxxabcxxx' + run.tee_stdout_n_stderr('1.log', '2.log'))
    if LET_TEST_PASS:
        import time
        time.sleep(1)
    assert fs.exists('1.log') == True
    assert fs.exists('2.log') == True

if __name__ == '__main__': # pragma: no cover
    ...
    LET_TEST_PASS = True
    import run
    import fs
    fs.file_remove('1.log')
    fs.file_remove('2.log')
    ret = run.run_bash('xxxabcxxx' + run.tee_stdout_n_stderr('1.log', '2.log'))
    if LET_TEST_PASS:
        import time
        time.sleep(1)
    assert fs.exists('1.log') == True
    assert fs.exists('2.log') == True

    