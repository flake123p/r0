import pytest
import sys
sys.path.append('../MyUtils/')

def test__str_split():
    import str_op
    ret = str_op.str_split("aa;bb,cc|dd:ee", [';', ',', '|', ':'])
    assert len(ret) == 5
    assert ret[0] == 'aa'
    assert ret[1] == 'bb'
    assert ret[2] == 'cc'
    assert ret[3] == 'dd'
    assert ret[4] == 'ee'
    ret = str_op.str_split("aa;bb,cc|dd:ee", [';,|:'])
    assert len(ret) == 5
    assert ret[0] == 'aa'
    assert ret[1] == 'bb'
    assert ret[2] == 'cc'
    assert ret[3] == 'dd'
    assert ret[4] == 'ee'
    ret = str_op.str_split("aa bb cc dd ee")
    assert len(ret) == 5
    assert ret[0] == 'aa'
    assert ret[1] == 'bb'
    assert ret[2] == 'cc'
    assert ret[3] == 'dd'
    assert ret[4] == 'ee'

def test__str_replace():
    import str_op
    ret = str_op.str_replace("aa;bb,cc|dd:ee", [';', ',', '|', ':'], ' ')
    assert ret == 'aa bb cc dd ee'

def test__StrFile():
    import str_op
    import fs
    import check
    def run():
        sf = str_op.StrFile()
        assert fs.exists(sf.file) == False
        sf.str_to_file('ABC')
        assert fs.exists(sf.file) == True
        assert check.Check_OneLineCompare(sf.file, 'ABC') == True

    run()

    # delete check
    sf = str_op.StrFile()
    assert fs.exists(sf.file) == False
    del sf

    # Temp check
    sf = str_op.StrFile().use_linux_tmp()
    assert fs.exists(sf.file) == False
    sf.str_to_file('ABC')
    assert fs.exists(sf.file) == True
    assert check.Check_OneLineCompare(sf.file, 'ABC') == True

def test__str_replace_last():
    import str_op
    assert str_op.str_replace_last('abbc', 'b', 'B') == 'abBc'
    assert str_op.str_replace_last('a.py.b.py', '.py', '') == 'a.py.b'