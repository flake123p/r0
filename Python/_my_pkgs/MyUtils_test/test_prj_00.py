import pytest
import sys
sys.path.append('../MyUtils/')

# def test__prj_show_root():
    # import prj
    # root = prj.prj_show_root()
    # import fs
    # root_basename = fs.basename(root)
    # assert root_basename == 'r0'

def test__Prj_sys_path_append():
    #
    # AssertionError: Failed: Can't find ".prj.yaml" in path: /home/user0/ws/envs/sche/bin
    #
    if 1:
        import fs
        import prj

        # Clear For Re-Gen
        fs.rmdir(fs.join(fs.pwd(), '.prj/'))

        # Init
        p = prj.Prj(fs.pwd())

        # Import
        p.sys_path_append('PYLIB')
        import hello_prj

        # print(hello_prj.hello_prj())
        assert hello_prj.hello_prj() == 'HELLO PRJ'
        assert p.get_root() == fs.pwd()

        # Init with existed auto file
        del p
        p = prj.Prj(fs.pwd())

        # Force Re-Gen
        assert p.get_root(force_gen=True) == fs.pwd()
        assert p.get_target('TEST_PRJ', force_gen=True) == fs.pwd()

        assert p.get_target('GHOST') == None

        with pytest.raises(prj.PrjException, match="Failed : Can't find target: GHOST"):
            p.get_target('GHOST', fail_exit=True)

        # fs.rmdir(fs.join(fs.pwd(), '.prj/'))
        # assert p.get_root() == fs.pwd()

        # print('ABXX')
        # print(p.get_root())
        # print('ABXX')

def test__a_explicit_path():
    import fs
    import prj
    #
    # a_src
    #
    path = fs.join(fs.pwd(), 'testdata/mod/prj/a_explicit_path/a_src')
    p = prj.Prj(path)
    assert p.get_root() == path
    assert p.get_namespace() == 'a_src'
    assert p.get_target('CC') == fs.join(path, 'mod/libcc')

    #
    # b_user
    #
    # TODO: Implement

def test__b_walk_2_root():
    import fs
    import prj

    root_path = fs.join(fs.pwd(), 'testdata/mod/prj/b_walk_2_root')
    path = fs.join(root_path, 'inside')
    
    p = prj.Prj(path)
    
    assert p.get_root() == root_path
    assert p.get_namespace() == 'bw2r'
    assert p.get_target('AA') == fs.join(root_path, 'mod/libaa')

def test__c_integrity():
    import fs
    import prj

    pm = fs.PathMgr(fs.join(fs.pwd(), 'testdata/mod/prj/c_integrity'))
    
    targets = [
        '.prj/auto_gen.int0.yaml',
        '.prj/auto_gen.int1.yaml',
        '.prj/auto_gen.int2.yaml',
        '.prj/auto_gen.int3.yaml',
        '.prj/auto_gen.int4.yaml',
        '.prj/auto_gen.int5.yaml'
    ]
    for t in targets:
        fs.file_copy(pm(t), pm('.prj/auto_gen.yaml'))
        p = prj.Prj(pm.dir)
        assert p.get_namespace() == 'c_int', f'Current Target = {t}'
        fs.file_remove(pm('.prj/auto_gen.yaml'))

if __name__ == '__main__': # pragma: no cover
    ...
    import fs
    import prj

    pd = prj.PrjDep(path=fs.join(fs.pwd(), 'testdata/mod/prj_dep'))
    # pd.parse('123')

    # pd.parse('abc')

    # pd.parse('1.log')

    pd.parse()
