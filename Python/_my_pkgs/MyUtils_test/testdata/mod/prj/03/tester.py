import sys
import os
import pytest

def CurrDir() -> str:
    import sys
    import os
    ret = os.path.dirname(sys.argv[0])
    if ret == '':
        ret = os.getcwd()
    return ret

cd = CurrDir()

if __name__ == '__main__': # pragma: no cover
    sys.path.append(os.path.join(cd, '../../../../MyUtils'))
    import prj
    
    p = prj.Prj()
    print('NS =', p.get_namespace(force_gen=False))

    print('PYLIB =', p.get_target('PYLIB'))

    p.sys_path_append('PYLIB')
    import hello
    print(hello.hello_prj())

    # print('P02 =', p.get_target('P02'))

    # print('AA =', p.get_target('AA'))

    # print('CC =', p.get_target('CC'))

    raise Exception('ABC')
    