import sys
import os
import pytest

def CurrDir() -> str:
    import sys
    import os
    ret = os.path.dirname(sys.argv[0])
    if ret == '':
        ret = os.getcwd()
    return ret

cd = CurrDir()


if __name__ == '__main__': # pragma: no cover
    sys.path.append(os.path.join(cd, '../../../../../MyUtils'))
    import prj
    ret = prj.prj_namespace(force_gen=False)
    print(ret)
    
    ret = prj.prj_target('P01')
    print(ret)