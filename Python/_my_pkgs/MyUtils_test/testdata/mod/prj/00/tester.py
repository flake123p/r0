import sys
import os
import pytest

def CurrDir() -> str:
    import sys
    import os
    ret = os.path.dirname(sys.argv[0])
    if ret == '':
        ret = os.getcwd()
    return ret

cd = CurrDir()

# print('argv[0]  =', sys.argv[0])
# print('__file__ =', __file__)

# sys.path.append('/home/user0/ws/r0/Python/_my_pkgs/MyUtils') # this is for pdb


if __name__ == '__main__': # pragma: no cover
    sys.path.append(os.path.join(cd, '../../../../MyUtils'))
    import prj
    # root = prj.prj_show_root()
    # print(root)
    ret = prj.prj_namespace(force_gen=True)
    print('NS =', ret)
    
    ret = prj.prj_target('P01')
    print('P01 = ', ret)

    ret = prj.prj_target('P04')
    print('P04 = ', ret)