
#
# Evaluate target value from target name
#
def eval(target):
    if target == 'BASH':
        return '/usr/bin/bash'
    if target == 'ZSH':
        return '/usr/share/zsh'