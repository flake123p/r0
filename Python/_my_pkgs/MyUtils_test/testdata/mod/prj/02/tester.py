import sys
import os
import pytest

def CurrDir() -> str:
    import sys
    import os
    ret = os.path.dirname(sys.argv[0])
    if ret == '':
        ret = os.getcwd()
    return ret

cd = CurrDir()

# print('argv[0]  =', sys.argv[0])
# print('__file__ =', __file__)

# sys.path.append('/home/user0/ws/r0/Python/_my_pkgs/MyUtils') # this is for pdb


if __name__ == '__main__': # pragma: no cover
    sys.path.append(os.path.join(cd, '../../../../MyUtils'))
    import prj
    
    p = prj.Prj()
    print('NS =', p.get_namespace(force_gen=True))

    print('P01 =', p.get_target('P01'))

    print('P02 =', p.get_target('P02'))

    print('AA =', p.get_target('AA'))

    print('CC =', p.get_target('CC'))
    