from MyUtils import misc
from MyUtils.prj import *

def prj_dep_py_act(io: PrjDepPyActDict):
    args = misc.dict_key_value_extract(io, 'args')
    assert args != None
    assert len(args) == 2

    if io['UserHandle'] == None:
        io['UserHandle'] = []

    io['UserHandle'].append(args[1])
    
    file_name = args[0]
    with open(file_name, 'w') as f:
        f.write(str(io['UserHandle']))
