from MyUtils import misc
from MyUtils.prj import *

def prj_dep_py_act(io: PrjDepPyActDict):
    args = misc.dict_key_value_extract(io, 'args')
    if args and len(args) > 1:
        # print(args)
        # assert 0
        file_name = args[0]
        with open(file_name, 'w') as f:
            for a in args[1:]:
                f.write(str(a) + '\n')
            f.write(f'{io["ActionType"]}')
    else:
        with open('testdata/mod/prj_dep/06_py_act/log.pyact.log', 'w') as f:
            f.write('xxx')