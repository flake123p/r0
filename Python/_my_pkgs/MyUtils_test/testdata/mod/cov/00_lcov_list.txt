Reading tracefile xxx.info
                                          |Lines       |Functions  |Branches    
Filename                                  |Rate     Num|Rate    Num|Rate     Num
================================================================================
[/home/user/]
aaaa/aaaa.cpp                             |86.7%    487|69.4%    49|    -      0
aaaa/aaaa.h                               |72.5%     69|76.9%    13|    -      0
aaaa/aaaa_ddd.cpp                         |93.9%    494|92.4%   172|    -      0
aaaa/aaaa_mmm.cpp                         |65.8%    395|53.8%    39|    -      0
aaaa_vvvv/aaaa_bbbbbb.cpp                 |79.7%    148| 100%     3|    -      0
aaaa_vvvv/aaaa_vvvv.cpp                   |77.4%     31|66.7%     3|    -      0
aaaa_vvvv/aaaa_vvvv.h                     |40.0%     10|50.0%     2|    -      0
aaaa_vvvv/aaaa_vvvv_44444.cpp             | 0.0%    251| 0.0%    15|    -      0
aaaa_vvvv/aaaa_vvvv_mmmm_sssssss.cpp      | 0.0%     11| 0.0%     1|    -      0
aaaa_vvvv/aaaa_vvvv_333333_44444.cpp      | 0.0%     39| 0.0%     2|    -      0
aaaa_vvvv/aaaa_qqqqqqq.cpp                | 0.0%     14| 0.0%     1|    -      0
aaaa_vvvv/aaaa_555555.cpp                 |49.6%    353|58.8%    17|    -      0
aaaa_vvvv/aaaa_ssssss.cpp                 | 100%     42| 100%     1|    -      0
aaaa_vvvv/aaaa_zzzzz.cpp                  |38.7%    119| 100%     1|    -      0
bb_0000000/bb_0000000.cpp                 |85.9%    362|64.3%    14|    -      0
bb_0000000/bb_0000000_qqq.cpp             | 0.0%     82| 0.0%     3|    -      0
bb_0000000/bb_0000000_qqq_222222.cpp      | 0.0%    508| 0.0%     3|    -      0
bb_0000000/bb_0000000_qqq_333333_44444.cpp| 0.0%    115| 0.0%     1|    -      0
bb_1111/bb_1111.cpp                       | 100%    130| 100%     3|    -      0
bb_1111_op_222222/bb_1111_op_222222.cpp   |31.7%    857|15.9%   107|    -      0
bb_tt_cmn/bb_ccc.cpp                      |98.1%     54| 100%    10|    -      0
bb_tt_cmn/bb_tt_cccccc.cpp                |86.9%     61|88.9%     9|    -      0
bb_tt_ii/bb_tt.h                          | 0.0%      2|    -     0|    -      0
bb_tt_ss/bb_tt.cpp                        |86.8%    106|82.6%    23|    -      0
cc/cc.cpp                                 |47.2%     36|50.0%    10|    -      0
cc/cc_all888888.cpp                       |71.0%     31|50.0%     2|    -      0
cc/cc_eeeee.cpp                           |47.8%     69|40.0%     5|    -      0
cc/cc_888888.cpp                          |74.3%     70|80.0%    10|    -      0
cc/cc_777_777777.h                        | 100%     25| 100%     1|    -      0
cc/cc_xxxxxx.cpp                          |94.2%    206|81.8%    22|    -      0
cc/cc_ff.cpp                              |67.0%    212|57.1%     7|    -      0
cc/cc_9999999.cpp                         |67.9%     56|75.0%     8|    -      0
cc/cc_6666.cpp                            |82.1%    240|79.2%    24|    -      0
cc/cc_send.cpp                            |89.3%    112|77.8%     9|    -      0
================================================================================
                                    Total:|57.5%   5797|64.2%   590|    -      0
