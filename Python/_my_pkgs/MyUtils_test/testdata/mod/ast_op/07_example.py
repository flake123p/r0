
import os
from pathlib import Path
import shutil

class MyUtFsException(Exception):
    pass

def FsExc(func):
    def wrapper(*args, **kwargs):
        try :
            return func(*args, **kwargs)
        except Exception as e:
            raise MyUtFsException from e
    return wrapper

class PathMgr():
    def __init__(self, dir:str = None):
        if dir == None:
            self.dir = pwd()
        else:
            self.dir = dir

    def Join(self, path: str):
        return join(self.dir, path)

def currdir() -> str:
    import sys
    ret = dirname(sys.argv[0])
    if ret == '':
        ret = pwd()
    return ret

def find_file(
    currdir: str, 
    filename: str,
    walk_to_root=False, 
    walk_from_root=False
) -> str:
    assert walk_from_root == False # TODO

    loc = None
    curr_folder = currdir
    while(True):
        full_path = join(curr_folder, filename)
        if exists(full_path):
            loc = curr_folder
            break

        if walk_to_root:
            prev_folder = dirname(curr_folder) # use dirname() to walk to root folder
            if prev_folder == curr_folder: # in root
                break
            curr_folder = prev_folder
            # next round
        else:
            break

    return loc

# mys
@FsExc
def dirname(path: str) -> str:
    return os.path.dirname(path)

# mys
def basename(path: str) -> str:
    return os.path.basename(path)

# mys - print
@FsExc
@FsExc
def pwd() -> str:
    return os.getcwd()

# mys
def join(pre: str, post: str) -> str:
    return os.path.join(pre, post)

# mys
def join2(pre: str, post: str) -> str:
    return os.path.join(pre, post)