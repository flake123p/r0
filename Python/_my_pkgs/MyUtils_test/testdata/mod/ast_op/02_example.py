
# mys
def greet(name: str) -> None:
    print(f"Hello, {name}")

# mys - print
def add(
    a: int,
    b: int
) -> int:
    return a + b

# mys
def no_args() -> str:
    return "No arguments here"

''' Output:
[
  {
    "name": "greet",
    "lineno": 3,
    "args": [
      {
        "name": "name",
        "anno": "str"
      }
    ],
    "ret_anno": "None"
  },
  {
    "name": "add",
    "lineno": 7,
    "args": [
      {
        "name": "a",
        "anno": "int"
      },
      {
        "name": "b",
        "anno": "int"
      }
    ],
    "ret_anno": "int"
  },
  {
    "name": "no_args",
    "lineno": 14,
    "args": [],
    "ret_anno": "str"
  }
]
'''