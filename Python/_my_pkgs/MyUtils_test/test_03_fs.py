import pytest
import sys
sys.path.append('../MyUtils/')

def test_fs():
    import fs
    print()
    print('pwd =', fs.pwd())
    print('join =', fs.join('a/', 'b/'))

    assert fs.exists('pytest.ini') == True
    assert fs.exists('pytest.iniABCDEF') == False

    fs.mkdir('GHOST')
    fs.file_create('GHOST/1.txt')
    fs.file_remove('GHOST/1.txt')
    fs.file_create('GHOST/1.txt')
    fs.rmdir('GHOST')

    fs.file_create('GHOST/1.txt', True)
    fs.rmdir('GHOST')

    assert 'testdata' in fs.list_dir('.')
    assert 'testdata' in fs.list_dir(None)
    assert 'cov.sh' in fs.list_files('.')
    assert 'cov.sh' in fs.list_files(None)
    assert 'cov.sh' not in fs.list_files('.', '.py')
    assert 'test_03_fs.py' in fs.list_files('.', '.py')
    assert 'test_03_fs.py' in fs.list_files('.', '.py .sh')
    assert 'cov.sh' in fs.list_files('.', '.py .sh')
    assert fs.dirname('abc/def') == 'abc'

    fs.list_dump(['abc'])

def test_fs_remove_file_twice():
    import fs
    fs.mkdir('GHOST')
    fs.file_create('GHOST/1.txt')
    assert fs.file_remove('GHOST/1.txt') == True
    assert fs.file_remove('GHOST/1.txt') == False
    fs.rmdir('GHOST')

def test_fs_remove_folder_twice():
    import fs
    fs.mkdir('GHOST')
    assert fs.rmdir('GHOST') == True
    assert fs.rmdir('GHOST') == False

def test_fs_clean():
    import fs
    fs.list_r()
    fs.clean()
    fs.clean_r()

def test_fs_newer():
    import fs
    assert fs.newer('xxxabcxxx') == True
    assert fs.newer('cov.sh') == True
    assert fs.newer('cov.sh', []) == True
    assert fs.newer('cov.sh', ['xxxabcxxx']) == True
    assert fs.newer('cov.sh', ['xxxabcxxx', 'test_fs_newer_local.log']) == True
    assert fs.newer('cov.sh', ['test_fs_newer_local.log']) == True
    fs.file_create('test_fs_newer_local.log')
    assert fs.newer('cov.sh', ['test_fs_newer_local.log']) == True
    import time
    time.sleep(0.001)
    import os
    os.system('touch cov.sh')
    assert fs.newer('cov.sh', ['test_fs_newer_local.log']) == False

    fs.file_remove('test_fs_newer_local.log')
    # print(fs.newer('cov.sh', ['xxxabcxxx', 'log.log']))

def test_fs_import_mod():
    import fs
    m = fs.import_mod('testdata/mod/fs', mod_name_without_py_extension='import_mod00')
    print(m.hello())

def test_PathMgr():
    import fs
    
    pm = fs.PathMgr()
    assert fs.pwd() == pm.dir
    del pm

    pm = fs.PathMgr('/abc')
    assert pm.Join('def') == fs.join('/abc', 'def')

def test_home():
    import fs
    assert isinstance(fs.home(), str) == True

def test_currdir():
    import fs
    assert isinstance(fs.currdir(), str) == True
    assert fs.currdir() != ''

def test_exists_check():
    import fs
    assert fs.exists_check({'pytest.ini' : True}) == True
    assert fs.exists_check({'pytest.iniABCDEF' : True}) == False
    with pytest.raises(ValueError, match='exists_check failed: key:pytest.iniABCDEF, golden:True, target:False'):
        fs.exists_check({'pytest.iniABCDEF' : True}, fail_exc=True)

def test_all_exists():
    import fs
    assert fs.all_exists('pytest.ini') == True
    assert fs.all_exists(['pytest.ini']) == True
    assert fs.all_exists(['pytest.ini', 'pytest.ini']) == True
    assert fs.all_exists(['pytest.ini', 'pytest.iniABCDEF']) == False
    assert fs.all_exists(['pytest.iniABCDEF', 'pytest.iniABCDEF']) == False

def test_all_not_exists():
    import fs
    assert fs.all_not_exists('pytest.ini') == False
    assert fs.all_not_exists('pytest.iniABCDEF') == True
    assert fs.all_not_exists(['pytest.iniABCDEF']) == True
    assert fs.all_not_exists(['pytest.iniABCDEF', 'pytest.iniABCDEF']) == True
    assert fs.all_not_exists(['pytest.ini', 'pytest.iniABCDEF']) == False
    assert fs.all_not_exists(['pytest.ini', 'pytest.ini']) == False

def test_file_copy():
    import fs
    import check
    ft0 = 'testdata/mod/fs/t0.txt'
    ft1 = 'testdata/mod/fs/t1.txt'
    ft2 = 'testdata/mod/fs/t2.txt'
    assert fs.exists_check({ft0 : True, ft1 : True, ft2 : False})
    assert check.Check_WholeFileCompare(ft0, 't0')
    assert check.Check_WholeFileCompare(ft1, 't1')
    # To dir
    fs.mkdir('testdata/mod/fs/copy/')
    fs.file_copy(ft0, 'testdata/mod/fs/copy/')
    assert fs.exists('testdata/mod/fs/copy/t0.txt')
    assert check.Check_WholeFileCompare('testdata/mod/fs/copy/t0.txt', 't0')
    fs.rmdir('testdata/mod/fs/copy/')
    # To another
    fs.file_copy(ft0, ft2)
    assert fs.exists(ft2)
    assert check.Check_WholeFileCompare(ft2, 't0')
    fs.file_remove(ft2)

if __name__ == '__main__': # pragma: no cover
    import fs
    import check
    ft0 = 'testdata/mod/fs/t0.txt'
    ft1 = 'testdata/mod/fs/t1.txt'
    ft2 = 'testdata/mod/fs/t2.txt'
    assert fs.exists_check({ft0 : True, ft1 : True, ft2 : False})
    assert check.Check_WholeFileCompare(ft0, 't0')
    assert check.Check_WholeFileCompare(ft1, 't1')
    fs.mkdir('testdata/mod/fs/copy/')
    fs.file_copy(ft0, 'testdata/mod/fs/copy/')
    # import os
    # print(os.path.isdir('testdata/mod/fs/copy/'))
    # print(os.path.isdir('testdata/mod/fs/copy'))