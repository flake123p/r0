import pytest
import sys
sys.path.append('../MyUtils/')

def test__dict_merge():
    import misc
    assert misc.dict_merge({}, {}) == {}
    assert misc.dict_merge({}, {'a': 1}) == {'a': 1}
    assert misc.dict_merge({'a': 1}, {'b': 2}) == {'a': 1, 'b': 2}
    assert misc.dict_merge({'a': 1}, None) == {'a': 1}
    with pytest.raises(KeyError):
        misc.dict_merge({'a': 1}, {'a': 2})

if __name__ == '__main__': # pragma: no cover
    ...

    