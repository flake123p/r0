import pytest
import sys
sys.path.append('../MyUtils/')

def test__mys_gen_entries():
    import ast_op, mys_op

    file = 'testdata/mod/ast_op/02_example.py'
    funcs = ast_op.ast_parse_file(file)
    import misc
    prev_lines = ast_op.ast_get_prev_line(file, funcs)
    entries = mys_op.mys_gen_entries(funcs=funcs, prev_lines=prev_lines)
    assert entries == ['"greet" : [greet, "print", None, str],', '"add" : [add, "print", int, int, int],', '"no_args" : [no_args, "print", str],']

    file = 'testdata/mod/ast_op/04_example.py'
    funcs = ast_op.ast_parse_file(file)
    import misc
    prev_lines = ast_op.ast_get_prev_line(file, funcs)
    entries = mys_op.mys_gen_entries(funcs=funcs, prev_lines=prev_lines)
    assert entries == []

    file = 'testdata/mod/ast_op/05_example.py'
    funcs = ast_op.ast_parse_file(file)
    import misc
    prev_lines = ast_op.ast_get_prev_line(file, funcs)
    entries = mys_op.mys_gen_entries(funcs=funcs, prev_lines=prev_lines)
    assert entries == ['"greet2" : [greet2, "print", None, None],']

    file = 'testdata/mod/ast_op/06_example.py'
    funcs = ast_op.ast_parse_file(file)
    import misc
    prev_lines = ast_op.ast_get_prev_line(file, funcs)
    entries = mys_op.mys_gen_entries(funcs=funcs, prev_lines=prev_lines)
    assert entries == ['"bar" : [foo, "print", None, str],', '"a3" : [a3, "print", None, "str_split"],']

if __name__ == '__main__': # pragma: no cover
    ...
    import ast_op, mys_op
    file = 'testdata/mod/ast_op/06_example.py'
    funcs = ast_op.ast_parse_file(file)

    import misc
    print(misc.json_dump_str(funcs))

    prev_lines = ast_op.ast_get_prev_line(file, funcs)
    for k in prev_lines:
        print(k, prev_lines[k])
    
    
    entries = mys_op.mys_gen_entries(funcs=funcs, prev_lines=prev_lines)
    print(entries)

    # print(entries == ['"greet2" : [greet2, "print", None, None],'])