
rm .coverage -rf
rm htmlcov -rf
rm *.log -rf
rm .pytest_cache -rf
rm __pycache__ -rf