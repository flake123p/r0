import pytest
import sys
sys.path.append('../MyUtils/')

def test__lcov_summary_list_file():
    import cov
    summary = cov.lcov_summary_list_file('testdata/mod/cov/00_lcov_list.txt')
    assert summary['max_file_name'] == 'bb_0000000/bb_0000000_qqq_333333_44444.cpp'
    assert summary['sum_of_lines_cov'] == 3327
    assert summary['sum_of_lines_total'] == 5797
    assert summary['folder_records'][0]['folder_name'] == 'aaaa'
    assert summary['folder_records'][1]['folder_name'] == 'aaaa_vvvv'
    assert summary['folder_records'][2]['folder_name'] == 'bb_0000000'
    assert summary['folder_records'][3]['folder_name'] == 'bb_1111'
    assert summary['folder_records'][4]['folder_name'] == 'bb_1111_op_222222'
    assert summary['folder_records'][5]['folder_name'] == 'bb_tt_cmn'
    assert summary['folder_records'][6]['folder_name'] == 'bb_tt_ii'
    assert summary['folder_records'][7]['folder_name'] == 'bb_tt_ss'
    assert summary['folder_records'][8]['folder_name'] == 'cc'

    filter = ['bb_0000000/bb_0000000_qqq_333333_44444.cpp']
    summary = cov.lcov_summary_list_file('testdata/mod/cov/00_lcov_list.txt', filter)
    assert summary['max_file_name'] == 'bb_1111_op_222222/bb_1111_op_222222.cpp'
    assert summary['sum_of_lines_total'] == 5797 - 115

    folder_filter = ['bb_0000000']
    summary = cov.lcov_summary_list_file('testdata/mod/cov/00_lcov_list.txt', folder_exclude_list=folder_filter)
    assert summary['max_file_name'] == 'bb_1111_op_222222/bb_1111_op_222222.cpp'
    assert summary['sum_of_lines_total'] == 5797 - 115 - 508 - 82 - 362

    cov.lcov_records_dump(summary)

def test__python_coverage_scan_total():
    import cov
    total = cov.python_coverage_scan_total('testdata/mod/cov/01_python_coverage.txt')
    assert total == 100