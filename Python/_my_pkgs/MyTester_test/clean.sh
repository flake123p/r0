
rm .coverage -rf
rm htmlcov -rf
rm log.log -rf
rm .pytest_cache -rf
rm __pycache__ -rf
rm *local -rf