import pytest
import os
import sys
sys.path.append('../MyTester/')
from GitUtils import *
from Tester import *

#
# Shell Example:
# 
#   while true; do date -R; python main.py; date -R; sleep 60; done
#

DB = ut.join(ut.pwd(), 'TEST_01_local/')
WS = ut.join(ut.pwd(), '../../../../r2/')

PARAM = dict()
# DB
FINAL_REPORT_NAME = 'report.json'
PARAM['TESTED_COMMIT']      = ut.join(DB, '00_tested_commits.txt')
PARAM['LOG_FOLDER']         = ut.join(DB, 'logs')
PARAM['FINAL_REPORT_LOCAL'] = ut.join(DB, FINAL_REPORT_NAME)
# WS
PARAM['FINAL_REPORT_DIR']   = ut.join(WS, 'autotest/database')
PARAM['FINAL_REPORT']       = ut.join(PARAM['FINAL_REPORT_DIR'], FINAL_REPORT_NAME)
# Other
PARAM['AUTO_UPDATE_TESTED'] = True # True False
PARAM['AUTO_COMMIT_TEST_REPORT'] = True # True False
PARAM['GIT_CHECKOUT_LOG_1'] = 'checkout_log_1.log'
PARAM['GIT_CHECKOUT_LOG_2'] = 'checkout_log_2.log'

class R2Tester(Tester):
    def run_tests_impl(self):
        is_pass = True

        if self.test_template(
            test_name='T0',
            command=f'cd {WS}/autotest/t00 ; ./00_test.sh',
            pass_func_list=[self.pass_if_no_failed]) == False:
            is_pass = False
        
        if self.test_template(
            test_name='T1',
            command=f'cd {WS}/autotest/t00 ; ./01_test.sh',
            pass_func_list=[self.pass_if_no_failed]) == False:
            is_pass = False
        
        if self.test_template(
            test_name='T2',
            command=f'cd {WS}/autotest/t00 ; ./02_test.sh',
            pass_func_list=[self.pass_if_no_failed]) == False:
            is_pass = False
        
        return is_pass
        
def test_GitUtils_00():
    with pytest.raises(AssertionError, match=''):
        _ = GitTester('GHOST', 'GHOST', PARAM=PARAM)

def test_GitUtils_01_create_empty_TESTED_COMMIT():
    gt = GitTester(ws=WS, db=DB, PARAM=PARAM)
    gt.run_all_tests(R2Tester(DB))

def test_GitUtils_02():
    gt = GitTester(ws=WS, db=DB, PARAM=PARAM)
    # injection
    gt.untested = [
        {
            "commit": "99da40b22299d9f6c03d7e74138a8a34aa03f9fb",
            "log": "    update\n",
            "Author": "Author: Flake <you@example.com>\n",
            "Date": "Date:   Thu Feb 6 19:57:43 2025 +0800\n",
            "Rev": 242
        },
    ]
    gt.run_all_tests(R2Tester(DB))

def test_GitUtils_03():
    # injection
    tested = ut.json_load('test_01_local/00_tested_commits.txt')
    tested[0]['commit'] = 'd86a5b8979003aeade830c85a73f5c4d5fadbe53'
    ut.json_dump(tested, 'test_01_local/00_tested_commits.txt')
    gt = GitTester(ws=WS, db=DB, PARAM=PARAM)
    gt.untested = gt.untested[0:11]
    gt.run_all_tests(R2Tester(DB))

def test_GitUtils_99_PKG_Import():
    print() # new line for pytest -s
    _ = ut.import_abs_pkg('MyTester', '../')

'''
TODO:
    data prepare
'''

if __name__ == '__main__': # pragma: no cover
    # tested = ut.json_load('test_01_local/00_tested_commits.txt')
    # tested[0]['commit'] = '99da40b22299d9f6c03d7e74138a8a34aa03f9fb'
    # ut.json_dump(tested, 'test_01_local/00_tested_commits.txt')

    gt = GitTester(ws=WS, db=DB, PARAM=PARAM)

    gt.run_all_tests(R2Tester(DB))
