
# pkg_name=MyUtils

# # lib_loc=$(python -c "import site; print(site.getsitepackages()[0])")
# # lib_loc=$lib_loc/$pkg_name

# # abs source files
# lib_loc=../MyUtils

# echo lib_loc = $lib_loc

# coverage run --source=$lib_loc,. 00_basic.py

# coverage report -m

./clean.sh

pytest $1 | tee log.log

coverage html