import pytest
import os
import sys
sys.path.append('../MyTester/')
sys.path.append('../MyUtils/')
from check import *

TEST_FOLDER = os.path.join(os.getcwd(), 'TEST_00_local')
TEST_FOLDER_DUMMY = os.path.join(TEST_FOLDER, 'dummy_local')

def test_00_user_handle_test():
    import Tester
    class Tester00(Tester.Tester):
        def run_tests_impl(self):
            assert self.hdl == 100
            pass

    tt = Tester00(TEST_FOLDER)
    tt.ImportUserHandle(hdl=100)
    tt.start_tests()
    tt.start_tests(TEST_FOLDER_DUMMY)

def test_01_basic_io_test():
    import Tester
    class Tester00(Tester.Tester):
        def run_tests_impl(self):
            self.test_template(
                test_name='dummy01',
                command='echo 123')
            pass

    tt = Tester00(TEST_FOLDER)
    tt.start_tests(TEST_FOLDER_DUMMY)

    with open(os.path.join(TEST_FOLDER_DUMMY, 'dummy01.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == '123'
    
    with open(os.path.join(TEST_FOLDER_DUMMY, 'dummy01_Err.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 0

def test_02_basic_pass_test():
    import Tester
    class Tester00(Tester.Tester):
        def run_tests_impl(self):
            ret = self.test_template(
                test_name='dummy02',
                command='echo 456',
                pass_func_list=[self.pass_if_no_failed, self.pass_if_no_python_error],
                pass_func_list2=[self.pass_if_no_string, self.pass_if_no_stderr_error])
            return ret

    tt = Tester00(TEST_FOLDER)
    is_pass = tt.start_tests(TEST_FOLDER_DUMMY)
    assert is_pass == True
    assert tt.is_failed == False
    assert len(tt.test_result) == 1
    assert tt.test_result[0] == '[PASS]   dummy02'

    with open(os.path.join(TEST_FOLDER_DUMMY, 'dummy02.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == '456'
    
    with open(os.path.join(TEST_FOLDER_DUMMY, 'dummy02_Err.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 0

def test_03_basic_error_test():
    import Tester
    class Tester00(Tester.Tester):
        def run_tests_impl(self):
            ret = self.test_template(
                test_name='dummy03',
                command='xxxxdfdbfdwerfwfgdsgbvxcbvddffdsgwefwefefdsfgsdgff',
                pass_func_list=[self.pass_if_no_failed, self.pass_if_no_python_error],
                pass_func_list2=[self.pass_if_no_string])
            return ret

    tt = Tester00(TEST_FOLDER)
    is_pass = tt.start_tests(TEST_FOLDER_DUMMY)
    assert is_pass == False
    assert tt.is_failed == True
    assert len(tt.test_result) == 1
    assert tt.test_result[0] == '[FAILED] dummy03'

    with open(os.path.join(TEST_FOLDER_DUMMY, 'dummy03.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 0
    
    with open(os.path.join(TEST_FOLDER_DUMMY, 'dummy03_Err.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == 'sh: 1: xxxxdfdbfdwerfwfgdsgbvxcbvddffdsgwefwefefdsfgsdgff: not found'

def test_04_error():
    import Tester
    class Tester00(Tester.Tester):
        def run_tests_impl(self):
            ret = self.test_template(
                test_name='dummy04',
                command='echo 123',
                pass_func_list=[self.pass_if_no_string])
            return ret

    tt = Tester00(TEST_FOLDER)
    is_pass = tt.start_tests(TEST_FOLDER_DUMMY)
    assert is_pass == False
    assert tt.is_failed == True

def test_05_pattern_num():
    import Tester
    class Tester00(Tester.Tester):
        def run_tests_impl(self):
            pat_100_x_2 = lambda file : self.pass_if_pattern_number_correct(
                file, 'abc 100%', 2)
            ret = self.test_template(
                test_name='dummy05',
                command='printf "abc 100%%\nabc 100%%\n"',
                pass_func_list=[pat_100_x_2])
            return ret

    tt = Tester00(TEST_FOLDER)
    is_pass = tt.start_tests(TEST_FOLDER_DUMMY)
    assert is_pass == True
    assert tt.is_failed == False

def test_06_pattern_num():
    import Tester
    class Tester00(Tester.Tester):
        def run_tests_impl(self):
            ret = self.test_template_pattern_num_match(
                test_name='dummy06',
                command='printf "abc 100%%\nabc 100%%\n"',
                pattern='abc 100%',
                num=2)
            return ret

    tt = Tester00(TEST_FOLDER)
    is_pass = tt.start_tests(TEST_FOLDER_DUMMY)
    assert is_pass == True
    assert tt.is_failed == False

def test_07():
    import Tester
    class Tester00(Tester.Tester):
        def run_tests_impl(self):
            self.test_template(
                test_name='dummy07_A',
                command='echo 123')
            self.test_template(
                test_name='dummy07_B',
                command='echo 123')

    tt = Tester00(TEST_FOLDER)
    tt.auto_tagging = True
    tt.start_tests(TEST_FOLDER_DUMMY)

    with open(os.path.join(TEST_FOLDER_DUMMY, '000_dummy07_A.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == '123'
    with open(os.path.join(TEST_FOLDER_DUMMY, '000_dummy07_A_Err.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 0
    
    with open(os.path.join(TEST_FOLDER_DUMMY, '001_dummy07_B.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == '123'
    with open(os.path.join(TEST_FOLDER_DUMMY, '001_dummy07_B_Err.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 0

def test_08():
    import Tester
    class Tester00(Tester.Tester):
        def run_tests_impl(self):
            self.test_template(
                test_name='dummy08_A',
                command='echo 123 dummy08_A')
            self.test_template(
                test_name='dummy08_B',
                command='./testdata/test_00_Tester/test_08.sh')
            return True

    tt = Tester00(TEST_FOLDER, auto_tagging=True, verbose=True)
    tt.auto_tagging_index = 10
    tt.auto_tagging_width = 4
    tt.start_tests(TEST_FOLDER_DUMMY)

    with open(os.path.join(TEST_FOLDER_DUMMY, '0010_dummy08_A.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == '123 dummy08_A'
    with open(os.path.join(TEST_FOLDER_DUMMY, '0010_dummy08_A_Err.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 0
    
    with open(os.path.join(TEST_FOLDER_DUMMY, '0011_dummy08_B.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == '456 dummy08_B'
    with open(os.path.join(TEST_FOLDER_DUMMY, '0011_dummy08_B_Err.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == './testdata/test_00_Tester/test_08.sh: 1: xxabcxx: not found'

def test_09_ImportConfig():
    import Tester
    class Tester00(Tester.Tester):
        def run_tests_impl(self): # pragma: no cover
            ...

    tt1 = Tester00(TEST_FOLDER, auto_tagging=True, verbose=True)
    tt1.auto_tagging_index = 10
    tt1.auto_tagging_width = 4
    
    tt2 = Tester00(TEST_FOLDER)
    tt2.ImportConfig(tt1)
    assert tt2.auto_tagging == True
    assert tt2.verbose == True
    assert tt2.auto_tagging_width == 4
    assert tt2.auto_tagging_index == 10
    tt2.ImportConfig(tt1, tag_idx=999)
    assert tt2.auto_tagging == True
    assert tt2.verbose == True
    assert tt2.auto_tagging_width == 4
    assert tt2.auto_tagging_index == 999

TEST_NAME = 0
ECHO_PATTERN = 1

Table_Test10 = [
    ['dummy10', '000'],
    ['dummy11', '111'],
    ['dummy12', '222'],
]

SLEEP_CONTRAL = 'sleep 1;'
def entry(i : int, tag_index : int, tag_width) -> bool: # pragma: no cover
    import Tester
    if i == 0:
        class Tester00(Tester.Tester):
            def run_tests_impl(self):
                ret = self.test_template(
                    test_name       = Table_Test10[i][TEST_NAME],
                    command         = f'{SLEEP_CONTRAL} echo ' + Table_Test10[i][ECHO_PATTERN],
                    pass_func_list  = [self.pass_if_no_failed],
                    pass_func_list2 = [self.pass_if_no_string, self.pass_if_no_python_error])
                return ret
        tt = Tester00(TEST_FOLDER, auto_tagging=True, verbose=True)
        tt.auto_tagging_index = tag_index
        tt.auto_tagging_width = tag_width
        return tt.start_tests(TEST_FOLDER_DUMMY)
    elif i == 1:
        class Tester01(Tester.Tester):
            def run_tests_impl(self):
                ret = self.test_template(
                    test_name       = Table_Test10[i][TEST_NAME],
                    command         = f'{SLEEP_CONTRAL} echo ' + Table_Test10[i][ECHO_PATTERN],
                    pass_func_list  = [self.pass_if_no_failed],
                    pass_func_list2 = [self.pass_if_no_string, self.pass_if_no_python_error])
                return ret
        tt = Tester01(TEST_FOLDER, auto_tagging=True, verbose=True)
        tt.auto_tagging_index = tag_index
        tt.auto_tagging_width = tag_width
        return tt.start_tests(TEST_FOLDER_DUMMY)
    elif i == 2:
        class Tester02(Tester.Tester):
            def run_tests_impl(self):
                ret = self.test_template(
                    test_name       = Table_Test10[i][TEST_NAME],
                    command         = f'{SLEEP_CONTRAL} echo ' + Table_Test10[i][ECHO_PATTERN],
                    pass_func_list  = [self.pass_if_no_failed],
                    pass_func_list2 = [self.pass_if_no_string, self.pass_if_no_python_error])
                return ret
        tt = Tester02(TEST_FOLDER, auto_tagging=True, verbose=True)
        tt.auto_tagging_index = tag_index
        tt.auto_tagging_width = tag_width
        return tt.start_tests(TEST_FOLDER_DUMMY)
    
    return False

def test_10_Multiprocess():
    global SLEEP_CONTRAL
    SLEEP_CONTRAL = ''
    import Tester
    class T00(Tester.Tester):
        def run_tests_impl(self):
            from multiprocessing import Pool
            inputs = [
                (0, self.auto_tagging_index+ 0, self.auto_tagging_width),
                (1, self.auto_tagging_index+10, self.auto_tagging_width),
                (2, self.auto_tagging_index+20, self.auto_tagging_width)
            ]
            self.auto_tagging_index += 30
            with Pool() as pool:
                results = pool.starmap(entry, inputs)
            # print(results)
            if False in results:
                return False # pragma: no cover
            else:
                return True

    tt = T00(TEST_FOLDER)
    tt.auto_tagging_width = 4
    tt.auto_tagging_index = 100
    assert True == tt.start_tests(TEST_FOLDER_DUMMY) 

    with open(os.path.join(TEST_FOLDER_DUMMY, '0100_' + Table_Test10[0][TEST_NAME] + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == Table_Test10[0][ECHO_PATTERN]
    with open(os.path.join(TEST_FOLDER_DUMMY, '0100_' + Table_Test10[0][TEST_NAME] + '_Err.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 0

    with open(os.path.join(TEST_FOLDER_DUMMY, '0110_' + Table_Test10[1][TEST_NAME] + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == Table_Test10[1][ECHO_PATTERN]
    with open(os.path.join(TEST_FOLDER_DUMMY, '0110_' + Table_Test10[1][TEST_NAME] + '_Err.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 0

    with open(os.path.join(TEST_FOLDER_DUMMY, '0120_' + Table_Test10[2][TEST_NAME] + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == Table_Test10[2][ECHO_PATTERN]
    with open(os.path.join(TEST_FOLDER_DUMMY, '0120_' + Table_Test10[2][TEST_NAME] + '_Err.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 0

Table_Test11 = [
    ['dummy11a', 'aaa'],
    ['dummy11b', 'bbb'],
    ['dummy11c', 'ccc'],
]
import Tester
class T0(Tester.Tester):
    def run_tests_impl(self): # pragma: no cover
        i = 0
        ret = self.test_template(
            test_name       = Table_Test11[i][TEST_NAME],
            command         = f'{SLEEP_CONTRAL} echo ' + Table_Test11[i][ECHO_PATTERN],
            pass_func_list  = [self.pass_if_no_failed],
            pass_func_list2 = [self.pass_if_no_string, self.pass_if_no_python_error])
        return ret
    
class T1(Tester.Tester):
    def run_tests_impl(self): # pragma: no cover
        i = 1
        ret = self.test_template(
            test_name       = Table_Test11[i][TEST_NAME],
            command         = f'{SLEEP_CONTRAL} echo ' + Table_Test11[i][ECHO_PATTERN],
            pass_func_list  = [self.pass_if_no_failed],
            pass_func_list2 = [self.pass_if_no_string, self.pass_if_no_python_error])
        return ret

class T2(Tester.Tester):
    def run_tests_impl(self): # pragma: no cover
        i = 2
        ret = self.test_template(
            test_name       = Table_Test11[i][TEST_NAME],
            command         = f'{SLEEP_CONTRAL} echo ' + Table_Test11[i][ECHO_PATTERN],
            pass_func_list  = [self.pass_if_no_failed],
            pass_func_list2 = [self.pass_if_no_string, self.pass_if_no_python_error])
        return ret

def entry2(pid : int, tester : Tester.Tester, log_folder : str) -> bool: # pragma: no cover
    return tester.start_tests(log_folder)

def test_11_Multiprocess_entry2():
    global SLEEP_CONTRAL
    SLEEP_CONTRAL = ''
    
    tt0 = T0(TEST_FOLDER, auto_tagging=True, verbose=True)
    tt0.auto_tagging_index = 2100
    tt0.auto_tagging_width = 4

    tt1 = T1(TEST_FOLDER, auto_tagging=True, verbose=True)
    tt1.auto_tagging_index = 2200
    tt1.auto_tagging_width = 4

    tt2 = T2(TEST_FOLDER, auto_tagging=True, verbose=True)
    tt2.auto_tagging_index = 2300
    tt2.auto_tagging_width = 4

    class TesterMgr(Tester.Tester):
        def run_tests_impl(self):
            from multiprocessing import Pool
            inputs = [
                (0, tt0, self.log_folder),
                (1, tt1, self.log_folder),
                (2, tt2, self.log_folder)
            ]
            with Pool() as pool:
                results = pool.starmap(entry2, inputs)
            # print(results)
            if False in results:
                return False # pragma: no cover
            else:
                return True

    ttmgr = TesterMgr(TEST_FOLDER)
    assert True == ttmgr.start_tests(TEST_FOLDER_DUMMY)

    with open(os.path.join(TEST_FOLDER_DUMMY, '2100_' + Table_Test11[0][TEST_NAME] + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == Table_Test11[0][ECHO_PATTERN]
    with open(os.path.join(TEST_FOLDER_DUMMY, '2100_' + Table_Test11[0][TEST_NAME] + '_Err.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 0

    with open(os.path.join(TEST_FOLDER_DUMMY, '2200_' + Table_Test11[1][TEST_NAME] + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == Table_Test11[1][ECHO_PATTERN]
    with open(os.path.join(TEST_FOLDER_DUMMY, '2200_' + Table_Test11[1][TEST_NAME] + '_Err.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 0

    with open(os.path.join(TEST_FOLDER_DUMMY, '2300_' + Table_Test11[2][TEST_NAME] + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == Table_Test11[2][ECHO_PATTERN]
    with open(os.path.join(TEST_FOLDER_DUMMY, '2300_' + Table_Test11[2][TEST_NAME] + '_Err.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 0

class T2_Err(Tester.Tester):
    def run_tests_impl(self): # pragma: no cover
        i = 2
        ret = self.test_template(
            test_name       = Table_Test11[i][TEST_NAME],
            command         = f'{SLEEP_CONTRAL} echo ccc; xxxabcxxx',
            pass_func_list  = [self.pass_if_no_failed],
            pass_func_list2 = [self.pass_if_no_string, self.pass_if_no_python_error])
        return ret

def test_12_Multiprocess_entry2_error():
    global SLEEP_CONTRAL
    SLEEP_CONTRAL = ''
    
    tt0 = T0(TEST_FOLDER, auto_tagging=True, verbose=True)
    tt0.auto_tagging_index = 3100
    tt0.auto_tagging_width = 4

    tt1 = T1(TEST_FOLDER, auto_tagging=True, verbose=True)
    tt1.auto_tagging_index = 3200
    tt1.auto_tagging_width = 4

    tt2 = T2_Err(TEST_FOLDER, auto_tagging=True, verbose=True)
    tt2.auto_tagging_index = 3300
    tt2.auto_tagging_width = 4

    class TesterMgr(Tester.Tester):
        def run_tests_impl(self):
            from multiprocessing import Pool
            inputs = [
                (0, tt0, self.log_folder),
                (1, tt1, self.log_folder),
                (2, tt2, self.log_folder)
            ]
            with Pool() as pool:
                results = pool.starmap(entry2, inputs)
            # print(results)
            if False in results:
                return False
            else:
                return True # pragma: no cover

    ttmgr = TesterMgr(TEST_FOLDER)
    assert False == ttmgr.start_tests(TEST_FOLDER_DUMMY)

    with open(os.path.join(TEST_FOLDER_DUMMY, '3100_' + Table_Test11[0][TEST_NAME] + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == Table_Test11[0][ECHO_PATTERN]
    with open(os.path.join(TEST_FOLDER_DUMMY, '3100_' + Table_Test11[0][TEST_NAME] + '_Err.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 0

    with open(os.path.join(TEST_FOLDER_DUMMY, '3200_' + Table_Test11[1][TEST_NAME] + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == Table_Test11[1][ECHO_PATTERN]
    with open(os.path.join(TEST_FOLDER_DUMMY, '3200_' + Table_Test11[1][TEST_NAME] + '_Err.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 0

    with open(os.path.join(TEST_FOLDER_DUMMY, '3300_' + Table_Test11[2][TEST_NAME] + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 0
    with open(os.path.join(TEST_FOLDER_DUMMY, '3300_' + Table_Test11[2][TEST_NAME] + '_Err.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == 'sh: 1: xxxabcxxx: not found'

class T13(Tester.Tester):
    def run_tests_impl(self): # pragma: no cover
        ret = self.test_template(
            test_name       = 'T13',
            command         = f'echo T13',
            pass_func_list  = [self.pass_if_no_failed],
            pass_func_list2 = [self.pass_if_no_string, self.pass_if_no_python_error])
        return ret
    
def test_13_TesterMP_00():  
    mp_mgr = Tester.TesterMP(TEST_FOLDER, auto_tagging=True)
    mp_mgr.auto_tagging_index = 4000
    mp_mgr.mp_prepare(2, 4, T13)
    mp_mgr.start_tests(TEST_FOLDER_DUMMY)
    with open(os.path.join(TEST_FOLDER_DUMMY, '4000_' + 'T13' + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == 'T13'
    with open(os.path.join(TEST_FOLDER_DUMMY, '4100_' + 'T13' + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == 'T13'
    with open(os.path.join(TEST_FOLDER_DUMMY, '4200_' + 'T13' + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == 'T13'
    with open(os.path.join(TEST_FOLDER_DUMMY, '4300_' + 'T13' + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == 'T13'

class T14(Tester.Tester):
    def run_tests_impl(self): # pragma: no cover
        ret = self.test_template(
            test_name       = 'T14',
            command         = f'xxxabcxxx',
            pass_func_list  = [self.pass_if_no_failed],
            pass_func_list2 = [self.pass_if_no_string, self.pass_if_no_python_error])
        return ret
    
def test_14_TesterMP_01(): 
    def tag_func(curr_idx, pid):
        return curr_idx + (pid * 10)
    mp_mgr = Tester.TesterMP(TEST_FOLDER, auto_tagging=True)
    mp_mgr.auto_tagging_index = 5000
    mp_mgr.mp_prepare(2, 4, T14, tag_func=tag_func)
    mp_mgr.start_tests(TEST_FOLDER_DUMMY)
    with open(os.path.join(TEST_FOLDER_DUMMY, '5000_' + 'T14' + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 0
    with open(os.path.join(TEST_FOLDER_DUMMY, '5010_' + 'T14' + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 0
    with open(os.path.join(TEST_FOLDER_DUMMY, '5020_' + 'T14' + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 0
    with open(os.path.join(TEST_FOLDER_DUMMY, '5030_' + 'T14' + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 0

    with open(os.path.join(TEST_FOLDER_DUMMY, '5000_' + 'T14_Err' + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == 'sh: 1: xxxabcxxx: not found'
    with open(os.path.join(TEST_FOLDER_DUMMY, '5010_' + 'T14_Err' + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == 'sh: 1: xxxabcxxx: not found'
    with open(os.path.join(TEST_FOLDER_DUMMY, '5020_' + 'T14_Err' + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == 'sh: 1: xxxabcxxx: not found'
    with open(os.path.join(TEST_FOLDER_DUMMY, '5030_' + 'T14_Err' + '.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == 'sh: 1: xxxabcxxx: not found'

def test_15_time_prof():
    import Tester
    tt = Tester.Tester(TEST_FOLDER)
    tt.start_tests(time_prof=True)

def test_16_start_tests_agent_pass():
    import Tester
    class Tester00(Tester.Tester):
        def run_tests_impl(self):
            ret = self.test_template(
                test_name='dummy16',
                command='echo 456',
                pass_func_list=[self.pass_if_no_failed, self.pass_if_no_python_error],
                pass_func_list2=[self.pass_if_no_string, self.pass_if_no_stderr_error])
            return ret
    class TesterAll(Tester.Tester):
        def run_tests_impl(self):
            tt = Tester00()
            is_pass = self.start_tests_agent(tt, self.log_folder)
            return is_pass

    tt = TesterAll()
    is_pass = tt.start_tests(TEST_FOLDER_DUMMY)
    assert is_pass == True
    assert tt.is_failed == False
    assert len(tt.test_result) == 1
    assert tt.test_result[0] == '[PASS]   dummy16'

    with open(os.path.join(TEST_FOLDER_DUMMY, 'dummy16.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == '456'
    
    with open(os.path.join(TEST_FOLDER_DUMMY, 'dummy16_Err.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 0

def test_17_start_tests_agent_failed():
    import Tester
    class Tester17a(Tester.Tester):
        def run_tests_impl(self):
            ret = self.test_template(
                test_name='dummy17a',
                command='echo 456',
                pass_func_list=[self.pass_if_no_failed, self.pass_if_no_python_error],
                pass_func_list2=[self.pass_if_no_string, self.pass_if_no_stderr_error])
            return ret
    class Tester17b(Tester.Tester):
        def run_tests_impl(self):
            ret = self.test_template(
                test_name='dummy17b',
                command='xxxxdfdbfdwerfwfgdsgbvxcbvddffdsgwefwefefdsfgsdgff',
                pass_func_list=[self.pass_if_no_failed, self.pass_if_no_python_error],
                pass_func_list2=[self.pass_if_no_string])
            return ret
    class TesterAll(Tester.Tester):
        def run_tests_impl(self):
            try:
                self.start_tests_agent(Tester17a(), self.log_folder, fail_exit=True)

                self.start_tests_agent(Tester17b(), self.log_folder, fail_exit=True)

            except Tester.TesterException as err:
                print(f'TesterException : {err}')
                return False

            assert 0 # pragma: no cover
            return True # pragma: no cover

    tt = TesterAll()
    is_pass = tt.start_tests(TEST_FOLDER_DUMMY)
    assert is_pass == False
    assert tt.is_failed == True
    assert tt.test_result == ['[PASS]   dummy17a', '[FAILED] dummy17b']

    with open(os.path.join(TEST_FOLDER_DUMMY, 'dummy17a.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == '456'

    assert Check_LineNum(os.path.join(TEST_FOLDER_DUMMY, 'dummy17a_Err.txt')) == 0

    assert Check_LineNum(os.path.join(TEST_FOLDER_DUMMY, 'dummy17b.txt')) == 0
    
    with open(os.path.join(TEST_FOLDER_DUMMY, 'dummy17b_Err.txt')) as f:
        lines = f.read().splitlines()
        assert len(lines) == 1
        assert lines[0] == 'sh: 1: xxxxdfdbfdwerfwfgdsgbvxcbvddffdsgwefwefefdsfgsdgff: not found'

if __name__ == '__main__':  # pragma: no cover
    import MyUtils
    import Tester
    ...
