
https://docs.python.org/3/library/venv.html

# Create venv
python -m venv /path/to/new/virtual/environment


# Activate(Linux-bash)
source <venv>/bin/activate


# Deactivate
deactivate