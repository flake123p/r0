# Download Pycharm Commuity Edition
https://www.jetbrains.com/pycharm/download/?section=linux

tar zxf ...
cd pycharm.../bin
bash pycharm.sh


# Setting virtaul env to pycharm
https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html#python_create_virtual_env

1: Ctrl+Alt+S
2: Select Project on Left-Side-Bar
3: Select Python Interpreter
4: Hit "Add Interpreter"
5: Hit "Add Local Interpreter"
6: Select Virtualenv Environment on sidebar
7: Choose Existing
8: Set the Location to pycharm