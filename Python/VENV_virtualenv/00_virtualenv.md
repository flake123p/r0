# Install venv:
pip3 install --upgrade pip
pip3 install virtualenv
//p27
pip install virtualenv

# Create venv
virtualenv ENV_NAME
virtualenv --python=/opt/python-3.6/bin/python ENV_NAME
//p27
~/anaconda3/envs/py27/bin/python -m virtualenv myenv

# Activate(Linux)
source ./venv/bin/activate

//p27 (In addition, export LD_LIBRARY_PATH)
export LD_LIBRARY_PATH=/home/pupil/anaconda3/envs/py27/lib:$LD_LIBRARY_PATH

# Deactivate
deactivate