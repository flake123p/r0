# Create with spyder
> conda create -n py39_15 python=3.9.15 virtualenv spyder

// remove env:
> conda remove -n <ENV_NAME> --all


# Copy ~/anaconda3/envs/py39_15s folder to seed folder
> cp ~/anaconda3/envs/py39_15s/ path/to/your/seed/folder -r


# leave anaconda
> conda deactivte


# Create env with seed python
> cd envs/
> ../seeds/py39_15s/bin/python -m virtualenv myenv
> ../seeds/py39_15s/bin/python -m virtualenv meta_l2


# Activate your env
> source <path_to_your_env>/bin/activate


# Install spyder
https://docs.spyder-ide.org/current/installation.html
> pip install spyder numpy scipy pandas matplotlib sympy cython


# Copy your python packages to your env
> copy to <path_to_your_env>/lib/python3.9/site-packages


# Add shortcut to your .bashrc
> vim ~/.bashrc

//Add this:
if true ; then
    source <path_to_your_env>/bin/activate
else
    ...
fi


# Install torch 2.0.1
// CU11.x
pip3 install torch==2.0.1 torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118

// CU12.x
pip3 install torch==2.0.1 torchvision torchaudio


# Common packages:
pip install numpy scipy numpy scipy pandas matplotlib sympy cython


# USER Defined site-packages
1: Add *.pth to .../site-packages
2: Content:

<path_1>/site-packages
<path_2>/site-packages


=======================================================================
# SymLink conda pkgs
=======================================================================
cp ~/anaconda3/pkgs/* to <your_env_pkgs_folder>

ln -s ../source/*.bar .


Example:

    <to ln folder!!>

    ln -s ../conda/*/lib/* . -f

Export:

    export LD_LIBRARY_PATH=~/ws/VENV/pkgs/conda_ln:$LD_LIBRARY_PATH