# Install venv:
pip3 install --upgrade pip
pip3 install virtualenvwrapper


# Create venv
https://virtualenvwrapper.readthedocs.io/en/latest/

$ pip install virtualenvwrapper
...
$ export WORKON_HOME=~/Envs
$ mkdir -p $WORKON_HOME
$ source ~/.local/bin/virtualenvwrapper.sh
$ mkvirtualenv env1

mkvirtualenv -p python2.7 ENV_NAME
