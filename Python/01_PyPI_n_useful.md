
# run
https://pypi.org/project/run/

Make sure only one instance of the program is running ...

Run until a specified timespec, if the process runs longer, it will be killed:
>>> import run
>>> run.until('23m42s')
>>> ...

You can also choose to limit on the amount of CPU-time being consumed (default is wall clock time):
>>> run.until('42s', 'cpu')
>>> ...


# timeit