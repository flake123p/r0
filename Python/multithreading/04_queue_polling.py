import time
import threading
import queue

class Worker(threading.Thread):
    def __init__(self, queue, num):
        threading.Thread.__init__(self)
        self.queue = queue
        self.num = num

    def run(self):
        while True:
            time.sleep(0.1)
            print('........polling')
            while self.queue.qsize() > 0:
                msg = self.queue.get()
                print("Worker %d: %s" % (self.num, msg))
                if msg == 'leave':
                    return

my_queue = queue.Queue()

my_worker1 = Worker(my_queue, 1)

my_worker1.start()

my_queue.put("Data 000")
my_queue.put("Data 001")
my_queue.put("Data 002")
time.sleep(1)
my_queue.put("leave")

my_worker1.join()

print("Done.")