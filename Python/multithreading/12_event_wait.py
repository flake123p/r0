import threading
import time

def wait_for_event(event):
    print("Thread is waiting for the event to be set")
    event.wait()  # Wait until the event is set
    print("Event has been set, continuing execution...")

event = threading.Event()
thread = threading.Thread(target=wait_for_event, args=(event,))
thread.start()

time.sleep(2)  # Simulate some work being done

event.set()  # Set the event

thread.join()