import time
import threading
import queue

class Worker(threading.Thread):
    def __init__(self, num, li):
        threading.Thread.__init__(self)
        self.num = num
        self.li = li

    def run(self):
        for _ in range(2000000):
            self.li[0] += 1

x = [0]

my_worker1 = Worker(1, x)
my_worker2 = Worker(2, x)

my_worker1.start()
my_worker2.start()

my_worker1.join()
my_worker2.join()

print("Done. x =", x) # not 4,000,000, because of race condition