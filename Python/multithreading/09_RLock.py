import time
import threading
import queue

class Worker(threading.Thread):
    def __init__(self, num, li, lock : threading.RLock):
        threading.Thread.__init__(self)
        self.num = num
        self.li = li
        self.lock = lock

    def run(self):
        for _ in range(2000000):
            self.lock.acquire()             # just lock twice ...
            self.lock.acquire()
            self.li[0] += 1
            self.lock.release()
            self.lock.release()

x = [0]

lock = threading.RLock()
my_worker1 = Worker(1, x, lock)
my_worker2 = Worker(2, x, lock)

my_worker1.start()
my_worker2.start()

my_worker1.join()
my_worker2.join()

print("Done. x =", x)