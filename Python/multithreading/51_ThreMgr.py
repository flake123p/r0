import time
import threading
import queue


class ThreMgr():
    def __init__(self):
        self.queue = queue.Queue()
        self.event = threading.Event()
        self.lock = threading.Lock()
    
    def send_msg(self, msg):
        self.queue.put(msg)
        self.event.set()
    
    def wait_msg(self):
        self.event.wait()
        self.event.clear()
    
    def recv_msg(self):
        if self.queue.qsize() > 0:
            return self.queue.get()
        return None

class ThrePoolMgr():
    def __init__(self, num : int):
        self.num = num
        self.pool = []
        for _ in range(self.num):
            self.pool.append(ThreMgr())
    
    def mgr(self, index : int) -> ThreMgr:
        return self.pool[index]

class ThreInst(threading.Thread):
    def __init__(self, num, pool : ThrePoolMgr):
        threading.Thread.__init__(self)
        self.num = num
        self.pool = pool

    def run(self):
        mgr = self.pool.mgr(self.num)
        while True:
            mgr.wait_msg()
            while (msg := mgr.recv_msg()) != None:
                # send to next
                print(self.num, 'get msg:', msg)
                if self.num != 2:
                    self.pool.mgr(self.num + 1).send_msg(msg)
                if msg == 'leave':
                    return

class ThreInstMgr():
    def __init__(self, pool : ThrePoolMgr):
        self.pool = pool
        self.insts : list[ThreInst] = []
        for i in range(self.pool.num):
            self.insts.append(ThreInst(i, self.pool))
    
    def start(self):
        for i in range(self.pool.num):
            self.insts[i].start()
    
    def join(self):
        for i in range(self.pool.num):
            self.insts[i].join()

if __name__ == '__main__':
    pool = ThrePoolMgr(3)
    insts = ThreInstMgr(pool)
    insts.start()

    pool.mgr(0).send_msg('msg 0')
    pool.mgr(0).send_msg('msg 1')
    time.sleep(1)
    pool.mgr(0).send_msg('msg 2')
    time.sleep(1)
    pool.mgr(0).send_msg('leave')

    insts.join()

    print("Done.")