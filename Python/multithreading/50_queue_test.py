import time
import threading
import queue

class Worker(threading.Thread):
    def __init__(self, queue, num):
        threading.Thread.__init__(self)
        self.queue = queue
        self.num = num

    def run(self):
        while True:
            while self.queue.qsize() > 0:
                msg = self.queue.get()
                # print("Worker %d: %s" % (self.num, msg))
                if msg == 'leave':
                    print(msg)
                    return
                else:
                    if msg == "abddfsdfxcvwsdgfwefsdfsdgvsdfwgsdvxcverfg":
                        pass
                    else:
                        print('[FAILED]')

my_queue = queue.Queue()

my_worker1 = Worker(my_queue, 1)

my_worker1.start()

for _ in range(1000 * 1000 * 3):
        my_queue.put("abddfsdfxcvwsdgfwefsdfsdgvsdfwgsdvxcverfg")

my_queue.put("leave")

my_worker1.join()

print("Done.") # Test Passed: queue is thread safe