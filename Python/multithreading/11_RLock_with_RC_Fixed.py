#
# https://stackoverflow.com/questions/16567958/when-and-how-to-use-pythons-rlock
#
import time
import threading
import queue

class X:
    def __init__(self):
        self.a = 0
        self.b = 0
        self.lock = threading.RLock()

    def changeA(self):
        with self.lock:
            self.a = self.a + 1

    def changeB(self):
        with self.lock:
            self.b = self.b + self.a

    def changeAandB(self):
        # you can use chanceA and changeB thread-safe!
        with self.lock:
            self.changeA() # a usual lock would block at here
            self.changeB()

class Worker(threading.Thread):
  def __init__(self, num, li : list[X]):
    threading.Thread.__init__(self)
    self.num = num
    self.li = li

  def run(self):
    for _ in range(1000*1000):
      self.li[0].changeAandB()


x = [X()]

my_worker1 = Worker(1, x)
my_worker2 = Worker(2, x)

my_worker1.start()
my_worker2.start()

my_worker1.join()
my_worker2.join()

print("Done.", x[0].a, x[0].b)