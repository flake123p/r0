import time
import threading
import queue



class Worker(threading.Thread):
    def __init__(self, num, li, sem : threading.Semaphore):
        threading.Thread.__init__(self)
        self.num = num
        self.li = li
        self.sem = sem

    def run(self):
        for _ in range(1000):
            self.sem.acquire()
            for x in range(1000):
                self.li[0] += 1
            self.sem.release()

x = [0]

sem = threading.Semaphore(2)                            # 2 threads allowed
my_worker1 = Worker(1, x, sem)
my_worker2 = Worker(2, x, sem)
my_worker3 = Worker(3, x, sem)

my_worker1.start()
my_worker2.start()
my_worker3.start()

my_worker1.join()
my_worker2.join()
my_worker3.join()

print("Done. x =", x) # Done. x = [2430461], not 3000000