import time
import threading
import queue

class Worker(threading.Thread):
    def __init__(self, queue, num, evt : threading.Event):
        threading.Thread.__init__(self)
        self.queue = queue
        self.num = num
        self.evt = evt

    def run(self):
        while True:
            time.sleep(0.1)
            print('........polling')
            self.evt.wait()
            self.evt.clear()
            while self.queue.qsize() > 0:
                msg = self.queue.get()
                print("Worker %d: %s" % (self.num, msg))
                if msg == 'leave':
                    return

my_queue = queue.Queue()
event = threading.Event()

my_worker1 = Worker(my_queue, 1, event)

my_worker1.start()

my_queue.put("Data 000")
my_queue.put("Data 001")
my_queue.put("Data 002")
event.set()
time.sleep(1)
my_queue.put("leave")
event.set()

my_worker1.join()

print("Done.")