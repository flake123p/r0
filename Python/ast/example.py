
# mys
def greet(name: str) -> None:
    print(f"Hello, {name}")

# mys - print
def add(
    a: int,
    b: int
) -> int:
    return a + b

# mys
def no_args() -> str:
    return "No arguments here"
