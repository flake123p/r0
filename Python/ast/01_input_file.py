import ast

# Parse the code into an Abstract Syntax Tree (AST)
file = open('example.py')
data = file.read()
file.close()

tree = ast.parse(data)

# Function to extract function names, arguments, annotations, and line numbers
def extract_function_info_with_line_numbers(node):
    functions = []
    for item in ast.walk(node):
        if isinstance(item, ast.FunctionDef):  # Check if it's a function definition
            func_name = item.name
            func_lineno = item.lineno  # Function definition line number
            args = []
            for arg in item.args.args:
                arg_name = arg.arg
                annotation = (
                    ast.unparse(arg.annotation) if arg.annotation else None
                )  # Get annotation as a string
                arg_lineno = arg.lineno  # Argument line number
                args.append((arg_name, annotation, arg_lineno))
            return_annotation = (
                ast.unparse(item.returns) if item.returns else None
            )  # Return type annotation
            functions.append((func_name, func_lineno, args, return_annotation))
    return functions

# Extract function names, arguments, annotations, and line numbers
function_info = extract_function_info_with_line_numbers(tree)

# Print results
for func_name, func_lineno, args, return_annotation in function_info:
    print(f"Function: {func_name}, Line: {func_lineno}")
    for arg_name, annotation, arg_lineno in args:
        print(f"  Argument: {arg_name}, Annotation: {annotation}, Line: {arg_lineno}")
    print(f"  Return Annotation: {return_annotation}")
    print()