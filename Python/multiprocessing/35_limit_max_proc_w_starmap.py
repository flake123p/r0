import multiprocessing
from multiprocessing import Pool, Value, Manager

multiprocessing.set_start_method('fork') # for Windows...

class CLA():
    def __init__(self, var = 7):
        self.var = var

def run(pid : int, x : CLA):
    import time
    time.sleep(1)
    print(x.var)
    return x.var + 1

if __name__ == '__main__':
    import MyUtils as ut

    p = ut.PM()

    inputs = [(1, a:=CLA()), (2, a:=CLA(77)), (3, a:=CLA(777))]

    with Pool(processes=2) as pool:
        results = pool.starmap(run, inputs)

    print(results)

    p.show() # 2 Max processes for 3 job = 2 sec elapsed
