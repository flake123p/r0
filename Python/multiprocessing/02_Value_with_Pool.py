from multiprocessing import Pool, Value


def add_one(v):
    v.value += 1


num = Value('d', 0.0)


with Pool(4) as pool:
    for _ in range(20):
        pool.apply_async(add_one, args=(num, ), error_callback=lambda e: print(e))
    pool.close()
    pool.join()


print(num.value)

'''
Synchronized objects should only be shared between processes through inheritance
Synchronized objects should only be shared between processes through inheritance
Synchronized objects should only be shared between processes through inheritance
Synchronized objects should only be shared between processes through inheritance
Synchronized objects should only be shared between processes through inheritance
Synchronized objects should only be shared between processes through inheritance
Synchronized objects should only be shared between processes through inheritance
Synchronized objects should only be shared between processes through inheritance
Synchronized objects should only be shared between processes through inheritance
...
'''