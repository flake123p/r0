import multiprocessing
from multiprocessing import Pool, Value

multiprocessing.set_start_method('fork') # for Windows...

# def add_one(v):
#     v.value += 1
def add_one():          # fork version
    num.value += 1

num = Value('d', 0.0)


with Pool(4) as pool:
    for _ in range(100):
        pool.apply_async(add_one, error_callback=lambda e: print(e))
    pool.close()
    pool.join()


print(num.value)
