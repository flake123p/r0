import multiprocessing
from multiprocessing import Pool, Value, Manager

multiprocessing.set_start_method('fork') # for Windows...

def square(n):
    return n * n

if __name__ == '__main__':
    numbers = [1, 2, 3, 4, 5]
    with Pool() as pool:
        results = pool.map(square, numbers)
    print(results)  # Output: [1, 4, 9, 16, 25]
