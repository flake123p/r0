import multiprocessing
from multiprocessing import Pool, Value, Manager, Process, Queue

multiprocessing.set_start_method('fork') # for Windows...

def square(n, q):
    q.put(n * n)

if __name__ == '__main__':
    numbers = [1, 2, 3, 4, 5]
    queue = Queue()
    processes = []

    for n in numbers:
        p = Process(target=square, args=(n, queue))
        processes.append(p)
        p.start()

    for p in processes:
        p.join()

    results = [queue.get() for _ in numbers]
    print(results)  # Output: [1, 4, 9, 16, 25]