import multiprocessing
from multiprocessing import Pool, Value, Manager, Process, Manager

multiprocessing.set_start_method('fork') # for Windows...

def square(n, result_list):
    result_list.append(n * n)

if __name__ == '__main__':
    numbers = [1, 2, 3, 4, 5]
    with Manager() as manager:
        results = manager.list()
        processes = []

        for n in numbers:
            p = Process(target=square, args=(n, results))
            processes.append(p)
            p.start()

        for p in processes:
            p.join()

        print(list(results))  # Output: [1, 4, 9, 16, 25]