import multiprocessing
from multiprocessing import Pool, Value, Manager

multiprocessing.set_start_method('fork') # for Windows...

def add(x, y):
    print(x + y)
    import time
    time.sleep(1)
    return {f'{x}' : x + y}

if __name__ == '__main__':
    inputs = [(1, 2), (3, 4), (5, 6)]
    with Pool() as pool:
        results = pool.starmap(add, inputs)
    print(results)  # Output: [3, 7, 11]


'''
    If you need to pass multiple arguments to a function, use Pool.starmap:
'''