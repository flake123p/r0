import multiprocessing
from multiprocessing import Pool, Value, Manager

multiprocessing.set_start_method('fork') # for Windows...

def run(i : str):
    import os
    return os.system(f'./07_sleep_echo.sh {i}')

with Pool(3) as pool:
    for i in range(3):
        pool.apply_async(run, args=(f'{i}', ), error_callback=lambda e: print(e))
    pool.close()
    pool.join()
