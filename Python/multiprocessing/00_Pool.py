#
# https://myapollo.com.tw/blog/python-multiprocessing/
#
from time import sleep, time
from multiprocessing import Pool


def send_mail(username, coupon_code, extra_sentence):
    print(username, coupon_code, extra_sentence or '')
    sleep(1)  # 模擬寄信需要 1 秒的時間


s_time = time()
with Pool(4) as pool:
    for idx in range(20):
        pool.apply_async(
            send_mail,
            (f'user{idx:0>2}', f'vipcode{idx:0>2}'),
            {'extra_sentence': f'{idx:0>4}'}
        )
    pool.close()
    pool.join()

print('花費時間：', time() - s_time)