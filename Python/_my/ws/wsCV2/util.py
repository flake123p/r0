import os

class ImgFileMgr():
    def __init__(self, basePath, inFolder, outFolder):
        self.basePath = basePath
        self.inFolder = inFolder
        self.outFolder = outFolder

        inFileList = os.listdir(basePath + inFolder)
        self.inFileList = sorted(inFileList)

        #for filename in inFileList:
        #    print(filename)

    def YeildFullName(self):
        for f in self.inFileList:
            i = self.basePath + self.inFolder + f
            o = self.basePath + self.outFolder + f
            yield i,o