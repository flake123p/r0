import cv2
import numpy as np

def CornerHarris(input, output=None):
    filename = input
    img = cv2.imread(filename)
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    gray = np.float32(gray)
    dst = cv2.cornerHarris(gray,14,27,0.14)

    #result is dilated for marking the corners, not important
    dst = cv2.dilate(dst,None)

    # Threshold for an optimal value, it may vary depending on the image.
    img[dst>0.01*dst.max()]=[0,0,255]

    if output:
        cv2.imwrite(output, img)
    else:
        while True:
            cv2.imshow('dst', img)
            key = cv2.waitKey(1)
            if key == 27: #ESC
                break
        cv2.destroyAllWindows()


def detect_pupil(filename):
    img = cv2.imread(filename)
    inv = cv2.bitwise_not(img)
    thresh = cv2.cvtColor(inv, cv2.COLOR_BGR2GRAY)
    kernel = np.ones((2,2),np.uint8)
    erosion = cv2.erode(thresh,kernel,iterations = 1)
    ret,thresh1 = cv2.threshold(erosion,220,255,cv2.THRESH_BINARY)
    return thresh1
    cnts, hierarchy = cv2.findContours(thresh1, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    print('len(cnts) = ' + str(len(cnts)))
    if len(cnts) != 0:
        c = max(cnts, key = cv2.contourArea)
        print('len(c) = ' + str(len(c)))
        (x,y),radius = cv2.minEnclosingCircle(c)
        
        center = (int(x),int(y))
        radius = int(radius)
        print('radius = ' + str(radius) + ' x,y = ' + str(int(x)) + ',' + str(int(y)))
        cv2.circle(img,center,radius,(255,0,0),2)
    return img