import numpy as np


class MoveAvg:
    def __init__(self, interval=5):
        self.interval = interval
        self.cnt = 0
        self.li_x = []
        self.li_y = []

    def input(self, xy):
        if self.cnt == self.interval:
            self.li_x.pop(0)
            self.li_y.pop(0)
        else:
            self.cnt += 1
        self.li_x.append(xy[0])
        self.li_y.append(xy[1])
        #print(self.li_y)

    def output(self):
        return [sum(self.li_x)/self.cnt, sum(self.li_y)/self.cnt]

    def find_peak_index(self):
        avg = self.output()
        diff_li = []
        for i in range(self.cnt):
            diff = abs(self.li_x[i] - avg[0]) + abs(self.li_y[i] - avg[1])
            diff_li.append(diff)
        return diff_li.index(max(diff_li))

    def output_filter_1_peak(self):
        if self.cnt < 2:
            return self.output()
        else:
            idx = self.find_peak_index()
            return [(sum(self.li_x)-self.li_x[idx])/(self.cnt-1), (sum(self.li_y)-self.li_y[idx])/(self.cnt-1)]


ma = MoveAvg()
ma.input([0.1, 1.0])
ma.input([0.1, 1.0])
ma.input([0.1, 1.0])
ma.input([0.1, 5.0])
ma.input([0.1, 1.0])
ma.input([0.1, 2.0])
ma.input([0.1, 2.0])
ma.input([0.1, 3.0])
ma.input([0.1, 3.0])
#ma.find_peak_index()
print(ma.output())
print(ma.output_filter_1_peak())