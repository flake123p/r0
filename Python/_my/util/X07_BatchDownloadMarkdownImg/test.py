'''
	%d for integer
	%g for floating-point
	%s for string

Batch Image Download:
    1. scan & save all file name
    2. open file
        2.1. download image & rename it on specific folder
        2.2. new file with new image path
    3. close file
    4. replace old file with new one

'''
import urllib.request
import os

dirname = "dut"
for name in os.listdir(dirname):
    path = os.path.join(dirname, name)
    if os.path.isfile(path):
        #print(path)
        split999 = os.path.split(path)
        #print('split999       = ', split999)
        #print('split999[0]    = ', split999[0])
        #print('split999[1]    = ', split999[1])
        pureName = split999[1].split(".md")
        #print('pureName       = ', pureName)
        purePath = os.path.join(os.getcwd(), dirname)
        purePath = os.path.join(purePath, pureName[0])
        print('purePath       = ', purePath)
        if not os.path.exists(purePath):
            os.mkdir(purePath)
#
# 1
#
dutdir = "dut"
dutpath = os.path.join(os.getcwd(), dutdir)
nameAry = [];
nameNum = 0;
for name in os.listdir(dutpath):
    fileWithPath = os.path.join(dutpath, name)
    if os.path.isfile(fileWithPath):
        nameNum = nameNum + 1;
        string = name.split(".md")
        nameAry.append(string[0])

print('nameNum    = ', nameNum)
#print('nameAry    = ', nameAry)

#
# 2
#
gDirName = ""
gDirWithPath = ""
gImageIndex = 0 # str(gImageIndex).zfill(4)

def download_image(inImg, outImg):
        r = urllib.request.urlopen(inImg)
        #Save to file
        with open(outImg, 'b+w') as f:
            f.write(r.read())

def is_imgur(each_line):
	posi = -1
	for i in range(len(each_line)-6):
		#if state == 0 :
		j = i
		if each_line[j] == ord('i'):
			j = j + 1
			if each_line[j] == ord('.'):
				j = j + 1
				if each_line[j] == ord('i'):
					j = j + 1
					if each_line[j] == ord('m'):
						j = j + 1
						if each_line[j] == ord('g'):
							j = j + 1
							if each_line[j] == ord('u'):
								j = j + 1
								if each_line[j] == ord('r'):
									j = j + 1
									return i
	return posi

def replace_new_image(each_line, posi, newImgPathShort):
	#print('type(each_line)    = ', type(each_line))
	new_line = each_line
	xxx = bytearray(b'\x00')
	lastBrace = 0
	for i in range(0, posi):
		xxx.append(each_line[i])
	for i in range(0, len(newImgPathShort)):
		xxx.append(ord(newImgPathShort[i]))
	for i in range(posi, len(each_line)):
		if each_line[i] == ord(')'):
			lastBrace = i
			break;
	for i in range(lastBrace, len(each_line)):
		xxx.append(each_line[i])
	return xxx

def single_line_process(each_line):
	global gDirName
	global gDirWithPath
	global gImageIndex
	posi = is_imgur(each_line)
	if posi == -1:
		return each_line
	#print(each_line)
	imgPath = ""
	posi = posi - 8;
	i = posi
	while True:
		ch = chr(each_line[i])
		if ch == ')':
			break;
		imgPath = imgPath + ch
		i = i +1;
	print(imgPath)
	newImgPath = gDirWithPath + "/" + str(gImageIndex).zfill(4) + ".png"
	newImgPathShort = gDirName + "/" + str(gImageIndex).zfill(4) + ".png"
	print(newImgPathShort)
	download_image(imgPath, newImgPath)
	gImageIndex = gImageIndex + 1
	each_line = replace_new_image(each_line, posi, newImgPathShort)
	return each_line

def single_file_process(fileWithPath) :
	fin = open(fileWithPath, "rb")
	fout = open(fileWithPath+".md", "wb")
	for each_line in fin:
		each_line = single_line_process(each_line)
		fout.write(each_line)
	fin.close()
	fout.close()
	os.remove(fileWithPath)
	os.rename(fileWithPath+".md", fileWithPath)

for name in nameAry :
	gDirName = name
	fileWithPath = os.path.join(dutpath, name) + ".md"
	gDirWithPath = os.path.join(dutpath, name)
	print(fileWithPath)
	print(gDirWithPath)
	gImageIndex = 0
	single_file_process(fileWithPath)
