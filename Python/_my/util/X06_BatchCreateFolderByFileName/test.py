'''
	%d for integer
	%g for floating-point
	%s for string

Batch folder creation:
    1. change current folder to dut
    2. scan & save all file name
    3. divide file name from the extension => pure name
    4. use pure name to create all folders

'''
import os

dirname = "dut"
for name in os.listdir(dirname):
    path = os.path.join(dirname, name)
    if os.path.isfile(path):
        #print(path)
        split999 = os.path.split(path)
        #print('split999       = ', split999)
        #print('split999[0]    = ', split999[0])
        #print('split999[1]    = ', split999[1])
        pureName = split999[1].split(".md")
        #print('pureName       = ', pureName)
        purePath = os.path.join(os.getcwd(), dirname)
        purePath = os.path.join(purePath, pureName[0])
        print('purePath       = ', purePath)
        if not os.path.exists(purePath):
            os.mkdir(purePath)