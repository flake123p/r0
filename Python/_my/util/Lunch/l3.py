#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
from sys import exit
from copy import copy
import array

nui_names = [
    (2, '葉志翔', 'Flake'),
    (1, '昱銘'),
    (1, '川'),
    (1, 'Lien'),
    (1, '陳爾皓'),
]

if 0:
    # 2024
    nui_namesLt = [
        'FLAKE',
        'KIWI',
        'LIEN',
        'IVER',
        'ALAN',
        'SEAN',
        'GENE',
        'GARY',
        'ALICE',
        'TINSLEY',
        'JACK',
        'YANJUN',
        'YUMING',
        'Yuanda',
        'Erhao'
    ]
else:
    # 2024
    nui_namesLt = [
        'FLAKE',
        'KIWI',
        'LIEN',
        'IVER',
        'ALAN',
        'SEAN',
        'GENE',
        'GARY',
        'COLIN',
        'ALICE',
        'TINSLEY',
        'JACK',
        'YANJUN',
        'YUMING',
        'Yuanda',
        'Erhao'
    ]

if 0:
    date_strs = [ 
        (1, '星期一'), 
        (2, '星期二'), 
        (3, '星期三'), 
        (4, '星期四'), 
        (5, '星期五'), 
        (6, '星期六'),
        (7, '星期日')
    ]
else:
    date_strs = [ 
        (1, '週一）'), 
        (2, '週二）'), 
        (3, '週三）'), 
        (4, '週四）'), 
        (5, '週五）'), 
        (6, '週六）'),
        (7, '週日）')
    ]

class BanDon :
    def __init__(self):
        print('ban don init')
        self.date = []
        self.currDateIdx = -1
        self.currDataId = 0
        self.records = []
        # self.reset_flags()
    
    # def reset_flags(self):
    #     self.flags = ['	' for i in nui_namesLt]

    def date_parser(self, line):
        s = line.split()
        if len(s) != 2:
            s = line.split('（')
            if len(s) != 2:
                return False
        for d in date_strs:
            if d[1] == s[1]:
                self.date.append(line)
                self.currDateIdx += 1
                self.currDataId = d[0]
                print(self.date)
                return True
        return False
    
    def match_nui_names(self, ls):
        if len(ls) < 2:
            return 0
        inner = ls[1:]
        for n in nui_names:
            match_str_num = n[0]
            if len(inner) <= match_str_num:
                break
            ctr = 0
            for i in range(match_str_num):
                nameIdx = i+1
                if n[nameIdx] == inner[i]:
                    ctr += 1
            if ctr == match_str_num:
                return match_str_num
        return 0

    def find_name(self, ct3):
        exist = False
        for i in range(len(nui_namesLt)):
            if ct3[0].upper() == nui_namesLt[i]:
                exist = True
                if self.currDateIdx < 0:
                    print('[FAILED] Unknown Date, idx =', self.currDateIdx)
                tup = (self.currDateIdx, self.currDataId, i, ct3[0], ct3[1], ct3[2])
                self.records.append(tup)
                # self.flags[i] = ct3[2]
                break
        return exist

    def context_parser(self, ct):
        if ct[0] == '訂便當囉':
            return
        if len(ct) != 3:
            print('[WARING] Weired String(1):', ct)
            return
        if self.find_name(ct) is False:
            print('[WARING] Weired String(2):', ct)
            return
        
    def line_parser(self, line):
        if self.date_parser(line):
            return
        ls = line.split()
        mch = self.match_nui_names(ls)
        if mch:
            context = ls[mch+1:]
            self.context_parser(context)
            #print(ls[mch+1:])
    
    def run(self, file_name):
        with open(file_name, 'r') as fin:
            for each_line in fin:
                if len(each_line) == 0:
                    continue
                if each_line[0] == '\n':
                    continue
                if len(each_line) < 6:
                    print('[FAILED] Unknow string:', each_line)
                    exit()
                self.line_parser(each_line.split('\n')[0])
                # print(each_line)
                # print(len(each_line))
    
    def dump(self):
        for r in self.records:
            print(self.date[r[0]], r[0], r[1], nui_namesLt[r[2]], r[3], r[4], r[5])
        
    def post_proc(self):
        cate_records = []
        curr_list = []
        curr_idx = 0
        for r in self.records:
            if curr_idx != r[0]:
                curr_idx = r[0]
                cate_records.append(curr_list.copy())
                curr_list.clear()
            curr_list.append(copy(r))
        if len(curr_list) != 0:
            cate_records.append(curr_list.copy())
        
        print('[cate_records] SSSSSSSSSSSSSSSSSSSSSSS')
        for cr in cate_records:
            print(cr)
        print('[cate_records] EEEEEEEEEEEEEEEEEEEEEEE')
            
        curId = 0
        week_dates = []
        week_flags = []
        for cr in cate_records:
            if len(cr) == 0:
                continue
            if cr[0][1] < curId:
                # next week flush
                for wd in week_dates:
                    print(wd)
                for wf in week_flags:
                    print(wf)
                week_dates.clear()
                week_flags.clear()
            curId = cr[0][1]
            # get flags in one day
            flags = ['\t' for i in nui_namesLt]
            for r in cr:
                flags[r[2]] = r[5] + '\t'
            x = ''
            for i in flags:
                x += i
            week_dates.append(self.date[cr[0][0]])
            week_flags.append(x)
        # last data flush
        for wd in week_dates:
            print(wd)
        for wf in week_flags:
            print(wf)
        week_dates.clear()
        week_flags.clear()

bd = BanDon()
bd.run('a.txt')
bd.dump()
bd.post_proc()
exit()
