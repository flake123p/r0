#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys

nui_names = [
    'FLAKE',
    'KIWI',
    'LIEN',
    'IVER',
    'ALAN',
    'SEAN',
    'GENE',
    'ADAM',
    'ANDY',
    'GARY',
    'COLIN',
    'TINSLEY'
]
# test = '滷大雞腿飯'
# print(test.upper() +'\t110')
# test = 'flaKE'
# print(test.upper() +'\t110')

'''
    0 = finding date
    1 = finding names
'''
state = 0 
date = ''
order = dict()

def import_order(index, price):
    global order
    order[index] = price

def export_order():
    global order
    print('[EXPORT ORDER] Date = ', date)
    temp = ''
    for i in range(len(nui_names)):
        if i in order:
            #print(i)
            temp = temp + order[i] + '\t'
        else:
            temp = temp + '\t'
    order = dict()
    print('>>>>>>>>>')
    print(temp)
    print('<<<<<<<<<')
    pass

with open('a.txt', 'r') as fin:
    for each_line in fin:
        #print(len(each_line))
        if each_line[0] == '\n':
            #print('new line')
            continue
        no_new_line = each_line.split('\n')
        if len(no_new_line) == 0:
            continue
        strs = no_new_line[0].split()
        if len(strs) == 0:
            continue
        
        #print(no_new_line[0])
        #print(len(each_line), len(strs), strs[0])
        
        if strs[0] == '訂便當囉':
            if len(strs) != 3 and len(strs) != 4:
                print('[Unknown Data String]: ', no_new_line[0], ' Exit!!!')
                sys.exit(1)

            if state == 0:
                state = 1
            elif state == 1: 
                export_order()
            else:
                print('[State is not 0 or 1] Exit!!!')
                sys.exit(1)
                
            date = strs[2]
            print('=====================================================================')
            print('[Current Date] = ', date)
            print('=====================================================================')
            continue
        
        if strs[0].upper() not in nui_names:
            print('[Unknown String]: ', no_new_line[0])
            continue
        
        if state != 1:
            print('[Skip No Date String]: ', no_new_line[0])
            continue
        
        if len(strs) != 3 and len(strs) != 4:
            print('[String Len Is Not 3 or 4]: ', no_new_line, " >>>>>>>>>>>>>>>>>> Maybe Format Error <<<<<<<<<<<<<<<<<<")
            continue
            
        i = nui_names.index(strs[0].upper())
        print(f'[{i:2d}] >> ', no_new_line[0])
        
        if len(strs) == 3:
            import_order(i, strs[2])
        elif len(strs) == 4:
            import_order(i, strs[3])
        else:
            print('[String Len Is Not 3 or 4]: LOGICAL ERROR, Exit!!! ... ', len(strs))
            sys.exit(1)

print('state = ', state)
print('date  = ', date)
print('order = ', order)
export_order()
