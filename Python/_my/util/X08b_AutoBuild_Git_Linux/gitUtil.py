import os
import json

class GitUntestedExtractor:
    def __init__(self, ws, db) -> None:
        self.ws = ws
        self.db = db
        self.git_log_file       = self.db + "00_git_log.txt"
        self.git_log_json       = self.db + "01_git_log_json.txt"
        self.tested_commit_file = self.db + "02_tested_commits.txt" # json
        self.local_commits_all  = self.db + "z_commits_all_local.txt"
        self.local_commits_ut   = self.db + "z_commits_ut_local.txt"
        
        cmd = 'cd ' + self.ws + ' && git log>' + self.git_log_file

        os.system(cmd)
        # print(cmd)
        # exit()
        self.git_log = []
        self.tested_commit = [] # json
        self.CUTList = []
        
        self.run()

    def import_tested_commit_2_dlist(self) -> None:
        with open(self.tested_commit_file, 'r') as f:
            self.tested_commit = json.load(f)
        
    def import_git_log_2_dlist(self) -> None:
        self.git_log = []
        capture_log = False
        with open(self.git_log_file, 'r') as f:
            cd = dict()
            for line in f:
                s = line.split()
                if len(s) == 0:
                    continue
                if s[0] == 'commit' and len(s) >= 2:
                    if len(cd) != 0:
                        self.git_log.append(cd)
                        cd = dict()
                    cd['commit'] = s[1]
                    cd['log'] = 'NULL'
                elif s[0] == 'Author:' and len(s) >= 2:
                    cd['Author'] = line
                elif s[0] == 'Date:' and len(s) >= 2:
                    cd['Date'] = line
                    capture_log = True
                    continue
                elif capture_log:
                    capture_log = False
                    cd['log'] = line
        if len(cd) != 0:
            self.git_log.append(cd)
        #
        # Add revision
        #
        total = len(self.git_log)
        curRev = 0
        curIdx = total - 1
        for _ in range(total):
            self.git_log[curIdx]["Rev"] = curRev
            curRev += 1
            curIdx -= 1
        #
        # Export meta file
        #
        f = open(self.git_log_json, "w")
        json.dump(self.git_log, f, indent=2)
        f.close()

    def dump(self) -> None:
        print(self.git_log_file)
        #print(self.git_log)
        for d in self.git_log:
            print(d)
    
    def gen_compare_metaData(self):
        with open(self.local_commits_all, 'w') as f:
            for d in self.git_log:
                f.write(d["commit"] + '\n')
        if len(self.tested_commit):
            firstTested = self.tested_commit[0]["commit"]
            
            self.CUTList = []
            for d in self.git_log:
                curCommit = d["commit"]
                if curCommit == firstTested:
                    break
                self.CUTList.append(d)
                
            with open(self.local_commits_ut, 'w') as f:
                json.dump(self.CUTList, f, indent=2)

    def run(self):
        self.import_git_log_2_dlist()
        self.import_tested_commit_2_dlist()
        self.gen_compare_metaData()
        
        # print('>>> git_log <<<')
        # for d in self.git_log:
        #     print(d["commit"])
        
        # print('>>> tested_commit <<<')
        # for d in self.tested_commit:
        #     print(d["commit"])
            
        # print('>>> Commits Under Test <<<')
        # for elem in self.CUTList:
        #     print(elem)

class GitTestJobMaker:
    def __init__(self, db, commits_under_test_list):
        self.CUTList = []
        for cut in commits_under_test_list:
            s = cut['log'].split()
            if len(s) > 0:
                if s[0] == '[TEST_REPORT]':
                    continue
            self.CUTList.append(cut)
        self.CUTList.reverse()
        
        #
        # LOG
        #
        self.local_job_maker = db + "z_maker_cut_lists_local.txt"
        with open(self.local_job_maker, 'w') as f:
            json.dump(self.CUTList, f, indent=2)

class GitTestJobRunner:
    def __init__(self, db, commits_under_test_list, tester) -> None:
        self.jobs = commits_under_test_list
        self.tester = tester
        self.db = db
        self.report = self.db + 'report.txt'
        self.run()
    
    def run_single(self, job):
        test_log = self.tester.run(job)
        #print('Single test log:', test_log)
        #
        # Merge test_log & commit_log to report
        #
        curReport = dict()
        curReport['Commit'] = job
        curReport['Result'] = test_log
        
        f = open(self.report, "r")
        allReport = json.load(f)
        f.close()
        f = open(self.report, "w")
        allReport.reverse()
        allReport.append(curReport)
        allReport.reverse()
        json.dump(allReport, f, indent=2)
        f.close()
        
        with open(self.db + "02_tested_commits.txt" , 'w') as f:
            json.dump([job], f, indent=2)
        
    def run(self):
        for job in self.jobs:
            self.run_single(job)

class GitTestReporter:
    def __init__(self):
        pass