from abc import ABC, abstractmethod
                   
class Tester(ABC):
    def __init__(self, ws, db):
        self.ws = ws
        self.db = db
        self.run_log = 'database/z0_TesterRunLog_local.txt'
        self.f = open(self.run_log, "w")
        self.test_result = [] # list of string
        self.is_failed = False
        self.is_skipped = False
        
    def __del__(self):
        self.f.close()
        
    def add_log(self, _str):
        self.f.write('  ' + _str + '\n')
        
    def run_cmd_to_buf(self, cmd):
        import subprocess
        return subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')

    # return build_this_commit
    def git_filter_c_cpp_python(self, git_folder, commit):
        # find .c .cpp .h .hpp file
        result = self.run_cmd_to_buf(['git', '-C', git_folder, 'show', commit])
        find_word = False
        for each_line in result:
            seach_line = each_line.split()
            # print(seach_line)
            for each_word in seach_line:
                if(each_word == '+++' or each_word == '---'):
                    find_word = True
                elif(find_word == True):
                    log_word = each_word.split('/')[-1]
                    log_word_extension = log_word.rsplit('.', 1)[-1]
                    if log_word_extension in ['c', 'cc', 'cxx', 'cpp', 'h', 'hpp', 'py']:
                        self.add_log('Do test by : ' + each_line)
                        return True #build_this_commit
                    if log_word == 'CMakeLists.txt':
                        self.add_log('Do test by : ' + each_line)
                        return True #build_this_commit
                    find_word = False
        return False
    
    @abstractmethod
    def run_tests(self, job) ->  list():
        return list() # or do_something, 

    def run(self, job) -> list():
        self.f.write('[Tester run()]: Rev = ' + str(job['Rev']) + ', Commit = ' + job['commit'] + '\n')
        self.is_failed = False
        self.is_skipped = False
        self.test_result = [] # list of string
        isCriticalFileChanged = self.git_filter_c_cpp_python(self.ws, job['commit'])
        if isCriticalFileChanged:
            self.test_result = self.run_tests(job)
        else:
            self.append_skip_result("[SKIPPED]")
        self.add_log('is_failed  = ' + str(self.is_failed))
        self.add_log('is_skipped = ' + str(self.is_skipped))
        self.add_log('test_result[] = ')
        for r in self.test_result:
            self.add_log('  ' + r)
        return self.test_result

    def append_fail_result(self, _str):
        #self.add_log(_str)
        self.test_result.append(_str)
        self.is_failed = True

    def append_skip_result(self, _str):
        #self.add_log(_str)
        self.test_result.append(_str)
        self.is_skipped = True


