# *****************************************************************************
# Copyright (C) 2022. MegaChips Corporation. All rights reserved.
# This software and the information contained herein ("Software")
# are PROPRIETARY and CONFIDENTIAL to MegaChips,and are being
# provided solely under the terms and conditions ofa MegaChips
# software license agreement or non-disclosure agreement.Otherwise,
# you have no rights to use or access the Software in any manner.
# No license under any patent, copyright, trade secret or other
# intellectual property right to the Software is granted to or
# conferred upon you by disclosure or delivery of the Software,
# either expressly, by implication, inducement, estoppel or otherwise.
# All or a portion of the Software are not allowed to be distributed,
# copied, or reproduced in any manner, electronic or otherwise,
# without MegaChips' prior written consent.Further, unless otherwise
# agreed in wrtiting by MegaChips, you may not use or utilize
# the Software as or for critical components in life support devices
# or systems.
#
# 1-1-1 Miyahara Yodogawa-ku
# Osaka 532-0003
# *****************************************************************************

# *****************************************************************************
# Imports
# *****************************************************************************
import cv2
import numpy as np
import onnxruntime
import quadric
import time

if __name__ == '__main__':
    import signal


# *****************************************************************************
# Defines
# *****************************************************************************
D_FOCUS_DISTANCE = 2.4  # mm # need the correct value
D_PIXEL_PITCH = 2.74 * (10**(-3))  # mm # need the correct value
D_IRIS_LENGTH = 11.7  # mm
D_VIDEO_WIDTH = 640
D_VIDEO_HEIGHT = 480
D_RIGHT_ANGLE = 90
D_CAMERA_MAX_HORIZONTAL_ANGLE = 60  # C310n HD WEBCAM
D_CAMERA_MAX_VERTICAL_ANGLE = 60  # C310n HD WEBCAM
D_CAMERA_CENTER_AXSIS = (D_VIDEO_WIDTH/2, D_VIDEO_HEIGHT/2)
D_CAMERA_HORIZONTAL_PIXEL_RESOLUTION = D_VIDEO_WIDTH / D_CAMERA_MAX_HORIZONTAL_ANGLE
D_CAMERA_VERTICAL_PIXEL_RESOLUTION = D_VIDEO_HEIGHT / D_CAMERA_MAX_VERTICAL_ANGLE

# *****************************************************************************
# Class
# *****************************************************************************


class EyeSensor():
    # *************************************************************************
    # Constracter/Destracter
    # *************************************************************************
    def __init__(self, root_path="."):
        ###
        # Valiable Initialization
        ###
        self.root_path = root_path
        proc_path = self.root_path + "../Resource/proc"
        model_path = self.root_path + "../Resource/model"
        
        self.Distance = 0.0
        self.L_CameraAngle = []  # [0] : h angle, [1] : v angle
        self.L_EyeAngle = []  # [0] : h angle, [1] : v angle
        self.L_EyeRadian = []  # [0] : h radian, [1] : v radian
        self.L_FaceBox = []  # [0] : box width, [1] : box height
        self.L_Iris = []
        self.IsFrameUpdated = False

        ###
        # Quadric initialization
        ###
        self.device_manager = quadric.DeviceManager()
        self.device = self.device_manager.get_device(quadric.DeviceConfiguration(num_cores=16))

        ###
        # Quadric kernel pre-postprocess initialization
        ###
        # Resize 480x640 to 240x320
        resize_480x640_to_240x320_path = proc_path + "/resize_480x640_to_240x320"
        self.resize_480x640_to_240x320_kernel = quadric.get_kernel(resize_480x640_to_240x320_path + "/resize_480x640_to_240x320_epu")
        self.resize_480x640_to_240x320_input_shape = (1, 1, 480, 640 * 3)
        self.resize_480x640_to_240x320_input_dtype = np.dtype(np.uint8)
        self.resize_480x640_to_240x320_input = self.device.allocate(self.resize_480x640_to_240x320_input_shape, self.resize_480x640_to_240x320_input_dtype)
        self.resize_480x640_to_240x320_output_shape = (1, 3, 240, 320)
        self.resize_480x640_to_240x320_output_dtype = np.dtype(np.int8)
        self.resize_480x640_to_240x320_output = self.device.allocate(self.resize_480x640_to_240x320_output_shape, self.resize_480x640_to_240x320_output_dtype)

        # Crop Resize 480x640 to 224x224
        cr_480x640_to_224x224_path = proc_path + "/crop_resize_480x640_to_224x224"
        self.cr_480x640_to_224x224_kernel = quadric.get_kernel(cr_480x640_to_224x224_path + "/crop_resize_480x640_to_224x224_epu")
        self.cr_480x640_to_224x224_input_shape = (1, 1, 480, 640*3)
        self.cr_480x640_to_224x224_input_dtype = np.dtype(np.uint8)
        self.cr_480x640_to_224x224_input = self.device.allocate(self.cr_480x640_to_224x224_input_shape, self.cr_480x640_to_224x224_input_dtype)
        self.cr_480x640_to_224x224_box_shape = (1, 1, 4, 1)
        self.cr_480x640_to_224x224_box_dtype = np.dtype(np.int32)
        self.cr_480x640_to_224x224_box = self.device.allocate(self.cr_480x640_to_224x224_box_shape, self.cr_480x640_to_224x224_box_dtype)
        self.cr_480x640_to_224x224_class_count_shape = (1, 1, 1, 1)
        self.cr_480x640_to_224x224_class_count_dtype = np.dtype(np.int32)
        self.cr_480x640_to_224x224_class_count = self.device.allocate(self.cr_480x640_to_224x224_class_count_shape, self.cr_480x640_to_224x224_class_count_dtype)
        self.cr_480x640_to_224x224_box_id_shape = (1, 1, 1, 1)
        self.cr_480x640_to_224x224_box_id_dtype = np.dtype(np.int16)
        self.cr_480x640_to_224x224_box_id = self.device.allocate(self.cr_480x640_to_224x224_box_id_shape, self.cr_480x640_to_224x224_box_id_dtype)
        self.cr_480x640_to_224x224_output_shape = (1, 3, 224, 224)
        self.cr_480x640_to_224x224_output_dtype = np.dtype(np.int8)
        self.cr_480x640_to_224x224_output = self.device.allocate(self.cr_480x640_to_224x224_output_shape, self.cr_480x640_to_224x224_output_dtype)

        # Crop Resize 480x640 to 64x64
        cr_480x640_to_64x64_path = proc_path + "/crop_resize_480x640_to_64x64"
        self.cr_480x640_to_64x64_kernel = quadric.get_kernel(cr_480x640_to_64x64_path + "/crop_resize_480x640_to_64x64_epu")
        self.cr_480x640_to_64x64_input_shape = (1, 1, 480, 640*3)
        self.cr_480x640_to_64x64_input_dtype = np.dtype(np.uint8)
        self.cr_480x640_to_64x64_input = self.device.allocate(self.cr_480x640_to_64x64_input_shape, self.cr_480x640_to_64x64_input_dtype)
        self.cr_480x640_to_64x64_box_shape = (1, 1, 4, 1)
        self.cr_480x640_to_64x64_box_dtype = np.dtype(np.int32)
        self.cr_480x640_to_64x64_box = self.device.allocate(self.cr_480x640_to_64x64_box_shape, self.cr_480x640_to_64x64_box_dtype)
        self.cr_480x640_to_64x64_class_count_shape = (1, 1, 1, 1)
        self.cr_480x640_to_64x64_class_count_dtype = np.dtype(np.int32)
        self.cr_480x640_to_64x64_class_count = self.device.allocate(self.cr_480x640_to_64x64_class_count_shape, self.cr_480x640_to_64x64_class_count_dtype)
        self.cr_480x640_to_64x64_box_id_shape = (1, 1, 1, 1)
        self.cr_480x640_to_64x64_box_id_dtype = np.dtype(np.int16)
        self.cr_480x640_to_64x64_box_id = self.device.allocate(self.cr_480x640_to_64x64_box_id_shape, self.cr_480x640_to_64x64_box_id_dtype)
        self.cr_480x640_to_64x64_output_shape = (1, 3, 64, 64)
        self.cr_480x640_to_64x64_output_dtype = np.dtype(np.int8)
        self.cr_480x640_to_64x64_output = self.device.allocate(self.cr_480x640_to_64x64_output_shape, self.cr_480x640_to_64x64_output_dtype)

        # NMS
        nms_path = proc_path + "/nms"
        self.nms_kernel = quadric.get_kernel(nms_path + '/nms_epu')
        self.nms_input_shape = (1, 1, 6, 4420)
        self.nms_input_dtype = np.dtype(np.int32)
        self.nms_boxes_shape = (1, 1, 4, 160)
        self.nms_boxes_dype = np.dtype(np.int32)
        self.nms_confidences_shape = (1, 1, 160, 1)
        self.nms_confidences_dtype = np.dtype(np.int16)  # FP 16 with 9 Frac bits.
        self.nms_class_count_shape = (1, 1, 1, 1)
        self.nms_class_count_dtype = np.dtype(np.int32)
        self.nms_box_id_shape = (1, 1, 160, 1)
        self.nms_box_id_dtype = np.dtype(np.int16)
        self.nms_input = self.device.allocate(self.nms_input_shape, self.nms_input_dtype)
        self.nms_boxes = self.device.allocate(self.nms_boxes_shape, self.nms_boxes_dype)
        self.nms_confidences = self.device.allocate(self.nms_confidences_shape, self.nms_confidences_dtype)
        self.nms_class_count = self.device.allocate(self.nms_class_count_shape, self.nms_class_count_dtype)
        self.nms_box_id = self.device.allocate(self.nms_box_id_shape, self.nms_box_id_dtype)
        self.nms_box_score = int(0.7 * (1 << 14))
        self.nms_confidences_score = int(0.5 * (1 << 14))

        ###
        # Quadric kernel model initialization
        ###
        # FaceDetection model

        ultraface_path = model_path + "/ultraface"
        self.ultraface_1 = quadric.get_kernel(ultraface_path + '/face_detection_epu')
        self.ultraface_2 = quadric.get_kernel(ultraface_path + '/face_detection_2_epu')
        self.ultraface_weights = self.device.allocate_and_copy_ndarray(np.fromfile(ultraface_path + '/face_detection_epu.qo.tensor0.bin', dtype=np.uint8))
        self.ultraface_input_shape = (1, 3, 240, 320)
        self.ultraface_input_dtype = np.dtype(np.int8)
        self.ultraface_output_shape = (1, 6, 1, 4420)
        self.ultraface_output_dtype = np.dtype(np.int32)
        self.ultraface_input = self.device.allocate(self.ultraface_input_shape, self.ultraface_input_dtype)
        self.ultraface_output = self.device.allocate(self.ultraface_output_shape, self.ultraface_output_dtype)

        # Gaze model

        gaze_path = model_path + "/xgaze"
        self.gaze_kernel = quadric.get_kernel(gaze_path + '/xgaze_epu')
        self.gaze_weights = self.device.allocate_and_copy_ndarray(np.fromfile(gaze_path + '/const_tensor_data.bin', dtype=np.int8))
        self.gaze_input_shape = (1, 3, 224, 224)
        self.gaze_input_dtype = np.dtype(np.int32)
        self.gaze_output_shape = (1, 2)
        self.gaze_output_dtype = np.dtype(np.int32)
        self.gaze_input = self.device.allocate(self.gaze_input_shape, self.gaze_input_dtype)
        self.gaze_output = self.device.allocate(self.gaze_output_shape, self.gaze_output_dtype)

        # FaceLandmark model
        # Refer : MCC's original resnet18
        landmark_path = model_path + "/landmark"
        self.landmark_kernel = quadric.get_kernel(landmark_path + '/landmark_epu')
        self.landmark_weights = self.device.allocate_and_copy_ndarray(np.fromfile(landmark_path + '/const_tensor_data.bin', dtype=np.int8))
        self.landmark_input_shape = (1, 3, 224, 224)
        self.landmark_input_dtype = np.dtype(np.int32)
        self.landmark_output_shape = (1, 136)
        self.landmark_output_dtype = np.dtype(np.int32)
        self.landmark_input = self.device.allocate(self.landmark_input_shape, self.landmark_input_dtype)
        self.landmark_output = self.device.allocate(self.landmark_output_shape, self.landmark_output_dtype)

        ###
        # ONNX model initialization
        ###
        # Iris model

        self.IrisModel = onnxruntime.InferenceSession(self.root_path + '../Resource/model/iris_landmark.onnx', providers=['CPUExecutionProvider'])
        self.IrisModel_InName = self.IrisModel.get_inputs()[0].name    # 'input_1'
        self.IrisModel_OutName1 = self.IrisModel.get_outputs()[0].name   # 'output_eyes_contours_and_brows'
        self.IrisModel_OutName2 = self.IrisModel.get_outputs()[1].name   # 'output_iris'

    # *************************************************************************
    # Estimation [I/F for External]
    # *************************************************************************

    def Estimation(self, src_img):
        time_st1 = time.perf_counter()

        ###
        # Re-initialize status and variables
        ###
        self.L_Iris = []
        self.IsFrameUpdated = False


        ###
        # Face detection
        ###
        # PreProcess : Resize
        self.device.copy_ndarray_to_device(src_img.reshape(self.resize_480x640_to_240x320_input_shape), self.resize_480x640_to_240x320_input)
        self.device.load_kernel(self.resize_480x640_to_240x320_kernel)
        self.device.run_kernel("resizeImage", (self.resize_480x640_to_240x320_input, self.resize_480x640_to_240x320_output))

        # AI Process : FaceDetection
        self.device.load_kernel(self.ultraface_1)
        self.device.run_kernel("infer", (self.ultraface_weights, self.resize_480x640_to_240x320_output, self.ultraface_output))
        self.device.load_kernel(self.ultraface_2)
        self.device.run_kernel("infer", (self.ultraface_weights, self.resize_480x640_to_240x320_output, self.ultraface_output))
        # data = self.device.copy_ndarray_from_device(self.ultraface_output, self.ultraface_output_shape, self.ultraface_output_dtype)


        ###
        # Gaze estimation and Landmark detection
        ###
        # PreProcess : NMS from face landmark
        self.device.load_kernel(self.nms_kernel)
        self.device.run_kernel("filter_boxes", (self.ultraface_output, self.nms_boxes, self.nms_confidences,
                               self.nms_class_count, self.nms_box_id, self.nms_box_score, self.nms_confidences_score))
        nms_labels = self.device.copy_ndarray_from_device(self.nms_class_count, self.nms_class_count_shape, self.nms_class_count_dtype).squeeze()
        nms_boxes = self.device.copy_ndarray_from_device(self.nms_boxes, self.nms_boxes_shape, self.nms_boxes_dype)
        nms_box_ids = self.device.copy_ndarray_from_device(self.nms_box_id, self.nms_box_id_shape, self.nms_box_id_dtype).squeeze()
        if False is nms_labels.any():  # Face didn't detected
            return None

        # The CPU of the PC equipped with Devkit has a high performance CPU(Genuine Intel 11th i7), 
        # and Crop & Resize processing is faster than Quadric Kernel.
        # This time, we don't use Quadric kernel for Crop & Resize processing because speed is important.
        """
        # PreProcess : Crop and resize
        face_box = nms_boxes.squeeze().transpose()
        face_box = face_box[nms_box_ids[0]]
        self.device.copy_ndarray_to_device(src_img.reshape(self.cr_480x640_to_224x224_input_shape), self.cr_480x640_to_224x224_input)
        self.device.copy_ndarray_to_device(face_box.reshape(self.cr_480x640_to_224x224_box_shape), self.cr_480x640_to_224x224_box)
        self.device.copy_ndarray_to_device(np.array([1]).reshape(self.cr_480x640_to_224x224_class_count_shape), self.cr_480x640_to_224x224_class_count)
        self.device.copy_ndarray_to_device(np.array([0]).reshape(self.cr_480x640_to_224x224_box_id_shape), self.cr_480x640_to_224x224_box_id)
        self.device.load_kernel(self.cr_480x640_to_224x224_kernel)
        self.device.run_kernel("cropGrayscaleResize", (self.cr_480x640_to_224x224_input, self.cr_480x640_to_224x224_box, self.cr_480x640_to_224x224_class_count, self.cr_480x640_to_224x224_box_id, self.cr_480x640_to_224x224_output))
        face_img = self.device.copy_ndarray_from_device(self.cr_480x640_to_224x224_output, self.cr_480x640_to_224x224_output_shape, self.cr_480x640_to_224x224_output_dtype).squeeze()
        
        # PreProcess : Calc face box axis(Use Iris detection)
        face_box_float = face_box.astype(np.float32) / (1 << 14) 
        face_box_float = (face_box_float * [src_img.shape[1], src_img.shape[0], src_img.shape[1], src_img.shape[0]]).astype(np.int32)
        face_box_width = face_box_float[2] - face_box_float[0]
        face_box_height = face_box_float[3] - face_box_float[1]
        maximum = max(face_box_width, face_box_height)
        dx = int((maximum - face_box_width)/2)
        dy = int((maximum - face_box_height)/2)
        upper_x = face_box_float[0] - dx
        upper_y = face_box_float[1] - dy
        lower_x = face_box_float[2] + dx
        lower_y = face_box_float[3] + dy
        face_box = [upper_x, upper_y, lower_x-upper_x, lower_y-upper_y]  
        
        # PreProcess : Normalize etc
        tmp_img = face_img[np.newaxis, :, :, :]
        tmp_img = (tmp_img - 127.5) / 128
        tmp_img = (tmp_img * (1 << 24)).astype(np.int32)
        """

        # PreProcess : Calculate face box axis(Use Iris detection)
        face_box = nms_boxes.squeeze().transpose()
        face_box = face_box[nms_box_ids[0]]
        face_box_float = face_box.astype(np.float32) / (1 << 14)
        face_box_float = (face_box_float * [src_img.shape[1], src_img.shape[0], src_img.shape[1], src_img.shape[0]]).astype(np.int32)
        face_box_width = face_box_float[2] - face_box_float[0]
        face_box_height = face_box_float[3] - face_box_float[1]
        maximum = max(face_box_width, face_box_height)
        dx = int((maximum - face_box_width)/2)
        dy = int((maximum - face_box_height)/2)
        upper_x = face_box_float[0] - dx
        upper_y = face_box_float[1] - dy
        lower_x = face_box_float[2] + dx
        lower_y = face_box_float[3] + dy
        face_box = [upper_x, upper_y, lower_x-upper_x, lower_y-upper_y]
        face_img = src_img[face_box[1]:face_box[1]+face_box[3], face_box[0]:face_box[0]+face_box[2], :]
        if 0 == face_img.size:
            return None

        # PreProcess : Normalize etc
        tmp_img = cv2.resize(face_img, (224, 224))
        tmp_img = tmp_img.transpose([2, 0, 1])
        tmp_img = (tmp_img - 127.5) / 128
        tmp_img = (tmp_img * (1 << 24)).astype(np.int32)

        # AI Process : Gaze estimation
        self.device.copy_ndarray_to_device(tmp_img.flatten(), self.gaze_input)
        self.device.load_kernel(self.gaze_kernel)
        self.device.run_kernel("xgaze_int8_opset11_devkit_8MB_13GBps_13GBps", (self.gaze_input, self.gaze_output, self.gaze_weights))
        gaze_result = self.device.copy_ndarray_from_device(self.gaze_output, self.gaze_output_shape, self.gaze_output_dtype)
        gaze_result = gaze_result[0] / (1 << 21)
        gaze_radian = np.array([gaze_result[1], gaze_result[0]])  # H : gaze_result[1], V : gaze_result[0]
        gaze_degree = [gaze_radian[0] * 180.0/np.pi, gaze_radian[1] * 180.0/np.pi]
        gaze_degree[1] = gaze_degree[1] * (-1)
        
        # Other Algorithm
        # degree(gaze direction) may from landmark(Face detection) -> IRIS -> CAM angle
        # Face detection, landmark, eye box, IRIS, 2 iris & ... rotate angle
        
        
        
        # AI Process : Landmark detection
        self.device.copy_ndarray_to_device(tmp_img.flatten(), self.landmark_input)
        self.device.load_kernel(self.landmark_kernel)
        self.device.run_kernel("landmark_int8_opset11_devkit_8MB_13GBps_13GBps", (self.landmark_input, self.landmark_output, self.landmark_weights))
        land_result = self.device.copy_ndarray_from_device(self.landmark_output, self.landmark_output_shape, self.landmark_output_dtype)[0]
        land_result = (land_result / (1 << 21)).astype(np.float32)
        land_result = (land_result + 0.5) * 224

        time_en1 = time.perf_counter()


        ###
        # Iris detection
        ###
        # PreProcess : Select eye point from landmark
        eye_boxs = []
        rate_x = (face_box[2] / 224)
        rate_y = (face_box[3] / 224)
        left_eye_ux = int(land_result[60] * rate_x) + int(face_box[0])
        left_eye_uy = int(land_result[53] * rate_y) + int(face_box[1])
        left_eye_lx = int(land_result[52] * rate_x) + int(face_box[0])
        left_eye_ly = int(land_result[61] * rate_y) + int(face_box[1])
        right_eye_ux = int(land_result[34] * rate_x) + int(face_box[0])
        right_eye_uy = int(land_result[36] * rate_y) + int(face_box[1])
        right_eye_lx = int(land_result[60] * rate_x) + int(face_box[0])
        right_eye_ly = int(land_result[61] * rate_y) + int(face_box[1])
        eye_boxs.append([left_eye_ux, left_eye_uy, left_eye_lx, left_eye_ly])
        eye_boxs.append([right_eye_ux, right_eye_uy, right_eye_lx, right_eye_ly])

        # The CPU of the PC equipped with Devkit has a high performance CPU(Genuine Intel 11th i7), 
        # and Crop & Resize processing is faster than Quadric Kernel.
        # This time, we don't use Quadric kernel for Crop & Resize processing because speed is important.
        """ 
        iris_output = []
        for eye_box in eye_boxs:
            #!!! PreProcess : Crop and resize
            eye_box_k = np.array(eye_box).astype(np.float32)
            base_img_size = np.array([640, 480, 640, 480]).astype(np.float32)
            eye_box_k = eye_box_k / base_img_size
            eye_box_k = (eye_box_k * (1 << 14)).astype(np.int32)
            self.device.copy_ndarray_to_device(src_img.reshape(self.cr_480x640_to_64x64_input_shape), self.cr_480x640_to_64x64_input)
            self.device.copy_ndarray_to_device(np.array(eye_box_k).reshape(self.cr_480x640_to_64x64_box_shape), self.cr_480x640_to_64x64_box)
            self.device.copy_ndarray_to_device(np.array([1]).reshape(self.cr_480x640_to_64x64_class_count_shape), self.cr_480x640_to_64x64_class_count)
            self.device.copy_ndarray_to_device(np.array([0]).reshape(self.cr_480x640_to_64x64_box_id_shape), self.cr_480x640_to_64x64_box_id)
            self.device.load_kernel(self.cr_480x640_to_64x64_kernel)
            self.device.run_kernel("cropGrayscaleResize", (self.cr_480x640_to_64x64_input, self.cr_480x640_to_64x64_box, self.cr_480x640_to_64x64_class_count, self.cr_480x640_to_64x64_box_id, self.cr_480x640_to_64x64_output))
            eye_img = self.device.copy_ndarray_from_device(self.cr_480x640_to_64x64_output, self.cr_480x640_to_64x64_output_shape, self.cr_480x640_to_64x64_output_dtype).squeeze()
            eye_img = np.transpose(eye_img, [1,2,0])
            eye_img = (eye_img / 127.5) #- 0.1
            eye_img = eye_img.astype('float32') 
        """
        
        iris_output = []
        for eye_box in eye_boxs:
            
            # PreProcess : Crop and resize
            eye_img = src_img[eye_box[1]:eye_box[3], eye_box[0]:eye_box[2], :]
            if 0 == eye_img.size:
                return None

            iris_img = cv2.resize(eye_img, (64, 64))
            iris_img = cv2.cvtColor(iris_img, cv2.COLOR_RGB2BGR)
            iris_img = iris_img[np.newaxis, :, :, :]
            iris_img = (iris_img / 127.5)  # - 0.1
            iris_img = iris_img.astype('float32')

            # AI Process : Iris detection
            iris_result = self.IrisModel.run([self.IrisModel_OutName1, self.IrisModel_OutName2], {self.IrisModel_InName: iris_img})
            iris = iris_result[1][0]
            iris_list = []
            for index in range(5):
                point_x = int(iris[index * 3] * ((eye_box[2] - eye_box[0]) / 64))
                point_y = int(iris[index * 3 + 1] * ((eye_box[3] - eye_box[1]) / 64))
                point_x += eye_box[0]
                point_y += eye_box[1]
                iris_list.append((int(point_x), int(point_y)))

            iris_output.append(iris_list)

        if np.all(np.array(iris_output) == 0):  # Eye didn't detected
            return None


        ###
        # Output
        ###
        self.L_EyeRadian = gaze_radian
        self.L_EyeAngle = gaze_degree
        self.L_FaceBox = face_box
        self.L_Iris = iris_output
        self.L_CameraAngle = self.CalculateCameraAngle(self.L_Iris)
        self.Distance = self.CalculateDistance(self.L_Iris)
        self.IsFrameUpdated = True

        return time_en1 - time_st1

    # *************************************************************************
    # Estimation-Co-functions [Internal Only]
    # *************************************************************************
    def CalculateCameraAngle(self, iris):
        if len(iris) == 0:
            return 0.0, 0.0

        # Calculate the average axisi of both eyes
        X_LeftEyeCenter, Y_LeftEyeCenter = iris[0][0]  # Left eye center
        X_RightEyeCenter, Y_RightEyeCenter = iris[1][0]  # Right eye center
        X_AveEyeCenter = (X_LeftEyeCenter + X_RightEyeCenter) / 2
        Y_AveEyeCenter = (Y_LeftEyeCenter + Y_RightEyeCenter) / 2

        # Calculate camera angle
        H_CameraInteriorAngle = (X_AveEyeCenter - D_CAMERA_CENTER_AXSIS[0]) / D_CAMERA_HORIZONTAL_PIXEL_RESOLUTION
        if H_CameraInteriorAngle < 0:
            H_CameraAngle = D_RIGHT_ANGLE - abs(H_CameraInteriorAngle)
        else:
            H_CameraAngle = D_RIGHT_ANGLE + H_CameraInteriorAngle

        V_CameraInteriorAngle = (Y_AveEyeCenter - D_CAMERA_CENTER_AXSIS[1]) / D_CAMERA_VERTICAL_PIXEL_RESOLUTION
        if V_CameraInteriorAngle < 0:
            V_CameraAngle = D_RIGHT_ANGLE + abs(V_CameraInteriorAngle)
        else:
            V_CameraAngle = D_RIGHT_ANGLE - V_CameraInteriorAngle

        return [H_CameraAngle, V_CameraAngle]

    def GetIrisWidth(self, iris_l, iris_r):
        #L_CenterX, L_CenterY = iris_l[0]
        L_RightX,  L_RightY = iris_l[1]
        L_TopX,    L_TopY = iris_l[2]
        L_LeftX,   L_LeftY = iris_l[3]
        L_BottomX, L_BottomY = iris_l[4]
        L_Width = (abs(L_LeftX - L_RightX)**2 + abs(L_LeftY - L_RightY)**2)**(1/2)
        L_Height = (abs(L_BottomX - L_TopX)**2 + abs(L_BottomY - L_TopY)**2)**(1/2)
        L_Length = (L_Width + L_Height) / 2

        #R_CenterX, R_CenterY = iris_r[0]
        R_RightX,  R_RightY = iris_r[1]
        R_TopX,    R_TopY = iris_r[2]
        R_LeftX,   R_LeftY = iris_r[3]
        R_BottomX, R_BottomY = iris_r[4]
        R_Width = (abs(R_LeftX - R_RightX)**2 + abs(R_LeftY - R_RightY)**2)**(1/2)
        R_Height = (abs(R_BottomX - R_TopX)**2 + abs(R_BottomY - R_TopY)**2)**(1/2)
        R_Length = (R_Width + R_Height) / 2

        return L_Length, R_Length

    def CalculateDistance(self, iris):
        if len(iris) == 0:
            return 0.0

        L_ImageIrisLength, R_ImageIrisLength = self.GetIrisWidth(iris[0], iris[1])
        if L_ImageIrisLength == 0 or R_ImageIrisLength == 0:
            return 0.0
        
        L_BaseDistance = D_IRIS_LENGTH * (D_FOCUS_DISTANCE / (L_ImageIrisLength * D_PIXEL_PITCH))
        R_BaseDistance = D_IRIS_LENGTH * (D_FOCUS_DISTANCE / (R_ImageIrisLength * D_PIXEL_PITCH))

        L_Distance = L_BaseDistance
        R_Distance = R_BaseDistance

        # Get average
        Distance = (L_Distance + R_Distance) / 2
        #Distance = Distance * D_FOCUS_DISTANCE_CORRECTION

        return Distance

    # *************************************************************************
    # Drawing [I/F for External]
    # *************************************************************************
    def Drawing(self, src_img):
        eye_imgs = None

        ###
        # Parameter check
        ###
        if len(src_img) == 0:
            return "No image", src_img, eye_imgs

        if self.IsFrameUpdated == True:
            ###
            # Draw Iris
            ###
            eye_imgs = []
            for iris in self.L_Iris:
                center, radius = cv2.minEnclosingCircle(np.array(iris))
                center = (int(center[0]), int(center[1]))
                radius = int(radius)

                radiusx3 = radius * 4
                eye_img = src_img[center[1]-radiusx3:center[1]+radiusx3, center[0]-radiusx3:center[0]+radiusx3, :]
                if (0 != eye_img.shape[0]) and (0 != eye_img.shape[1]):
                    eye_imgs.append(cv2.resize(eye_img, (160, 120)))
                else:
                    eye_imgs = None
                    break

                cv2.circle(src_img, center, radius, (0, 0, 255), 2)
                for point in iris:
                    cv2.circle(src_img, (point[0], point[1]), 1, (255, 255, 255), 2)
            
            ###
            # Draw Face box
            ###
            [top_left_col, top_left_row, box_width, box_height] = self.L_FaceBox
            cv2.rectangle(src_img,
                          (int(top_left_col), int(top_left_row)),
                          (int(top_left_col+box_width), int(top_left_row+box_height)),
                          color=(0, 0, 255),
                          thickness=2)

            ###
            # Draw Gaze
            ###
            (h, w) = src_img.shape[:2]
            length = w / 2.0
            x_center = top_left_col + (box_width / 2)
            y_center = top_left_row + (box_height / 2)
            dx = -length * np.sin(self.L_EyeRadian[0]) * np.cos(self.L_EyeRadian[1])
            dy = -length * np.sin(self.L_EyeRadian[1])  # * np.cos(self.L_EyeRadian[0])
            pos_c = (x_center, y_center)
            pos_xy = [pos_c[0] + dx, pos_c[1] + dy]
            cv2.arrowedLine(src_img,
                            tuple(np.round(pos_c).astype(np.int32)),
                            tuple(np.round(pos_xy).astype(int)),
                            (0, 0, 255), 2, cv2.LINE_AA, tipLength=0.18)

            Res = "Eye Detected"
        else:
            Res = "No Eyes"
        return Res, src_img, eye_imgs

    # *************************************************************************
    # Sub-Functions [I/F for External]
    # *************************************************************************

    def GetEyeSensingResult(self):
        return self.Distance, self.L_CameraAngle, self.L_EyeAngle


# *****************************************************************************
# Signal interrupt
# *****************************************************************************
def KeyboardInterruptHandler(SignalValue, Handler):
    if (SignalValue == signal.SIGINT):
        capture.release()
        cv2.destroyAllWindows()
        print("Pressed Ctrl-C")
        exit(2)


# *****************************************************************************
# Main Routine
# *****************************************************************************
if __name__ == '__main__':
    print(__file__)
    print("Script Started.")

    # Set signal interrupt
    signal.signal(signal.SIGINT, KeyboardInterruptHandler)

    # Open camera
    capture = cv2.VideoCapture(0)
    if capture.isOpened() != True:
        print("Could not open camera device")
        exit(1)

    cv2.namedWindow("frame", cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty("frame", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

    # Make instance
    Eye = EyeSensor("./Resource/")                  # ------------> EyeSensor()

    # Face Pose estimation routine
    frame_shown = False
    while True:
        
        # Get an image from camera
        ret, src_img = capture.read()
        if (cv2.waitKey(1) & 0xFF == ord('q')) or not ret:
            print("Pressed q key")
            break
        if frame_shown and cv2.getWindowProperty('frame', cv2.WND_PROP_VISIBLE) == 0:
            print("Video is not there")
            break

        # AI estimation
        elasped_time = Eye.Estimation(src_img)     # ------------> Estimation()

        # Face landmark drawing
        res_text, res_img, _ = Eye.Drawing(src_img)
        dis, cam, eye = Eye.GetEyeSensingResult()

        # Show image
        if None is elasped_time:
            txt1 = f"Fps : Unknow"
        else:
            txt1 = f"Fps : {1 / elasped_time:.02f}"
        txt2 = f"Dis : {dis:.02f}"
        txt3 = f"Cam : H {cam[0]:.02f} V {cam[1]:.02f}"
        txt4 = f"Eye : H {eye[0]:.02f} V {eye[1]:.02f}"
        res_img = cv2.flip(src_img, 1)
        cv2.putText(res_img, res_text, (8, 24), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
        cv2.putText(res_img, txt1, (8, 48), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
        cv2.putText(res_img, txt2, (8, 72), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
        cv2.putText(res_img, txt3, (8, 96), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
        cv2.putText(res_img, txt4, (8, 120), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
        cv2.imshow('frame', res_img)
        frame_shown = True

    # Finish application
    capture.release()
    cv2.destroyAllWindows()
    print("Script finished successfully.")
    exit(0)
