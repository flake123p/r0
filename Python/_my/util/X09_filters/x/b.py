# *****************************************************************************
# Copyright (C) 2022. MegaChips Corporation. All rights reserved.
# This software and the information contained herein ("Software")
# are PROPRIETARY and CONFIDENTIAL to MegaChips,and are being
# provided solely under the terms and conditions ofa MegaChips
# software license agreement or non-disclosure agreement.Otherwise,
# you have no rights to use or access the Software in any manner.
# No license under any patent, copyright, trade secret or other
# intellectual property right to the Software is granted to or
# conferred upon you by disclosure or delivery of the Software,
# either expressly, by implication, inducement, estoppel or otherwise.
# All or a portion of the Software are not allowed to be distributed,
# copied, or reproduced in any manner, electronic or otherwise,
# without MegaChips' prior written consent.Further, unless otherwise
# agreed in wrtiting by MegaChips, you may not use or utilize
# the Software as or for critical components in life support devices
# or systems.
# 
# 1-1-1 Miyahara Yodogawa-ku
# Osaka 532-0003
# *****************************************************************************

# *****************************************************************************
# Imports
# *****************************************************************************
import time
import math
import signal
import cv2
from logging import getLogger  # noqa: E402

from eyesensor_kernel import EyeSensor

# *****************************************************************************
# Defines
# *****************************************************************************
D_CAM_DEVICE_NUM = 0

D_IRIS_PATH = "Iris/"

PRINT_FPS_ENABLE = False
PRINT_DBG_ENABLE = False

# *****************************************************************************
# MCC Eye Tracking Class
# *****************************************************************************
class MccEyeTracker():
    # *************************************************************************
    # Constracter/Destracter
    # *************************************************************************
    def __init__(self, root_path="./"):
        # Valiable Initialization
        self.RootPath  = root_path
        self.IsStarted = False
        self.IsReady   = False
        self.IsVideoUpdated = False
        self.OutputVideo = []
        self.OutputEyeVideo = []
        self.AppFps      = 0.0
        self.EstFps      = 0.0
        self.FaceLandGazeFps = 0.0
        self.PrintEn     = False

        # Instances
        print("Video AI prediction initializing...")
        self.Eye = EyeSensor(root_path=self.RootPath + "Resource/")

    # *************************************************************************
    # Run MCC Holistic
    # *************************************************************************
    def Run(self):
        # Open camera
        capture = cv2.VideoCapture(D_CAM_DEVICE_NUM)
        if capture.isOpened() != True:
            print("Could not open camera device")
            exit(1)
        print("Video AI prediction started")

        # Pose estimation routine
        self.IsStarted = True
        self.IsReady   = False
        frame_shown    = False
        while(self.IsStarted == True):
            time_at_first = time.time()

            # Get an image from camera
            ret, src_img = capture.read()
            if __name__ == '__main__':
                if (cv2.waitKey(1) & 0xFF == ord('q')) or not ret:
                    print("Pressed q key")
                    break
                if frame_shown and cv2.getWindowProperty('frame', cv2.WND_PROP_VISIBLE) == 0:
                    print("Video is not there")
                    break
            time_at_video_received = time.time()

            # Estimate Landmark from image
            face_land_gaze_time = self.Eye.Estimation(src_img)
            time_at_estimated = time.time()

            # Draw Landmarks into image
            res_text_eye, res_img, eye_imgs = self.Eye.Drawing(src_img)
            time_at_drawn = time.time()

            # Calculate FPS
            app_total_time  = time_at_drawn - time_at_first
            estimation_time = time_at_estimated - time_at_video_received
            #overhead_time   = (time_at_video_received - time_at_first) + (time_at_drawn - time_at_estimated)
            if None is face_land_gaze_time:
                app_total_time = 0
                estimation_time = 0
                face_land_gaze_time = 0
            
            if app_total_time > 0.0:
                application_fps = 1.0 / app_total_time
            else:
                application_fps = 0.0
                
            if estimation_time > 0.0:
                estimation_fps  = 1.0 / estimation_time
            else:
                estimation_fps  = 0.0
                
            if face_land_gaze_time > 0.0:
                face_land_gaze_fps  = 1.0 / face_land_gaze_time
            else:
                face_land_gaze_fps  = 0.0
                
            self.AppFps = application_fps
            self.EstFps = estimation_fps
            self.FaceLandGazeFps = face_land_gaze_fps

            # Show image
            res_img = cv2.flip(src_img, 1)

            if __name__ == '__main__':
                if PRINT_FPS_ENABLE == True:
                    display_text    = 'AppFPS: {:.1f}, EstFPS: {:.1f}'.format(application_fps, estimation_fps)
                    cv2.putText(res_img, display_text, (8, 20), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 255, 0), 2, cv2.LINE_AA)
                if PRINT_DBG_ENABLE == True:
                    # putText:  img, text, org=(x,y), fontFace, fontScale, color, thickness, lineType)
                    cv2.putText(res_img, "Eye: " + res_text_eye, (8, 50),  cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 0, 255), 2, cv2.LINE_AA)
                    #DbgStr = 'VIDEO:{:.1f}    Cap:{:.1f}    Pred:{:.1f}    Out:{:.1f}'.format(time_at_first, time_at_video_received-time_at_first, time_at_estimated-time_at_video_received, time_at_drawn-time_at_estimated)
                    #print(DbgStr)
                    Distance, CamAngle, EyeAngle = self.Eye.GetEyeSensingResult()
                    DbgStr = 'D({:.02f}mm) AngC({:.02f},{:.02f}) AngE({:.02f},{:.02f}))'.format(Distance, CamAngle[0], CamAngle[1], EyeAngle[0], EyeAngle[1])
                    cv2.putText(res_img, DbgStr, (8, 80),  cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 0, 255), 2, cv2.LINE_AA)

            # Display result image
            if __name__ == '__main__':
                cv2.imshow('frame', res_img)
            else:
                rgb_img = cv2.cvtColor(res_img, cv2.COLOR_BGR2RGB)
                self.OutputVideo = rgb_img.copy()
                
                if None is not eye_imgs:
                    eye_imgs[0] = cv2.flip(eye_imgs[0], 1)
                    eye_imgs[1] = cv2.flip(eye_imgs[1], 1)
                    eye_imgs[0] = cv2.cvtColor(eye_imgs[0], cv2.COLOR_BGR2RGB)
                    eye_imgs[1] = cv2.cvtColor(eye_imgs[1], cv2.COLOR_BGR2RGB)
                    self.OutputEyeVideo = eye_imgs.copy()
                else:
                    self.OutputEyeVideo = None
                
                self.IsVideoUpdated = True
                if self.IsReady == False:
                    self.IsReady = True

            frame_shown = True

        # Finish application
        capture.release()
        cv2.destroyAllWindows()
        self.IsReady = False
        print("Video AI prediction stopped")

    # *************************************************************************
    # Stop MCC Holistic
    # *************************************************************************
    def Stop(self):
        self.IsStarted = False

    # *************************************************************************
    # Is ready?
    # *************************************************************************
    def IsEyeTrackingReady(self):
        return self.IsReady

    # *************************************************************************
    # Is updated?
    # *************************************************************************
    def IsUpdated(self):
        Ret = self.IsVideoUpdated
        if self.IsVideoUpdated == True:
            self.IsVideoUpdated = False
        return Ret

    # *************************************************************************
    # Get Output Video
    # *************************************************************************
    def GetOutputVideo(self):
        return self.OutputVideo, self.OutputEyeVideo
    
    # *************************************************************************
    # Get FPS
    # *************************************************************************
    def GetFps(self):
        return self.AppFps, self.EstFps, self.FaceLandGazeFps

    # *************************************************************************
    # Get Pointer Position
    # *************************************************************************
    def GetPointerPosition(self):
        # Get current info
        Distance, CamAngle, EyeAngle = self.Eye.GetEyeSensingResult()
        
        #----------------------------------------
        # Calcurate Horizontal
        if CamAngle[0] < 90:
            if EyeAngle[0] < 0:
                 Dig_E = abs(EyeAngle[0])
                 Dig_C = 180 - CamAngle[0]
                 Dig_G = 180 - (Dig_E + Dig_C)
                 Tan_G = math.tan(math.radians(Dig_G))
                 Len_H = Distance * (1/Tan_G)
                 Positive = False
                 DbgStrH = '@@@1: {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}'.format(
                                Len_H, Distance, Dig_E, Dig_C, Dig_G, Tan_G, 0.0)
            else:
                 Dig_E = EyeAngle[0]
                 Dig_C = CamAngle[0]
                 Dig_G = 180 - (Dig_E + Dig_C)
                 Tan_G = math.tan(math.radians(Dig_G))
                 Tan_C = math.tan(math.radians(Dig_C))
                 Len_H = Distance * (1/Tan_G + 1/Tan_C)
                 Positive = True
                 DbgStrH = '@@@2: {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}'.format(
                            Len_H, Distance, Dig_E, Dig_C, Dig_G, Tan_G, Tan_C)
        else:
            if EyeAngle[0] < 0:
                 Dig_E = abs(EyeAngle[0])
                 Dig_C = 180 - CamAngle[0]
                 Dig_G = 180 - (Dig_E + Dig_C)
                 Tan_G = math.tan(math.radians(Dig_G))
                 Tan_C = math.tan(math.radians(Dig_C))
                 Len_H = Distance * (1/Tan_G + 1/Tan_C)
                 Positive = False
                 DbgStrH = '@@@3: {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}'.format(
                                Len_H, Distance, Dig_E, Dig_C, Dig_G, Tan_G, Tan_C)
            else:
                 Dig_E = EyeAngle[0]
                 Dig_C = CamAngle[0]
                 Dig_G = 180 - (Dig_E + Dig_C)
                 Tan_G = math.tan(math.radians(Dig_G))
                 Len_H = Distance * (1/Tan_G)
                 Positive = True
                 DbgStrH = '@@@4: {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}'.format(
                                Len_H, Distance, Dig_E, Dig_C, Dig_G, Tan_G, 0.0)
        # Convert MAP from camera to monitor
        if Positive == True:
            #PosX_mm = 130.8 + Len_H
            PosX_mm = 130.8 + Len_H
        else:
            #PosX_mm = 130.8 - Len_H
            PosX_mm = 130.8 - Len_H
            
        #----------------------------------------
        # Calcurate Vertical
        if CamAngle[1] < 90:
            if EyeAngle[1] < 0:
                 Dig_E = abs(EyeAngle[1])
                 Dig_C = 180 - CamAngle[1]
                 Dig_G = 180 - (Dig_E + Dig_C)
                 Tan_G = math.tan(math.radians(Dig_G))
                 Len_V = Distance * (1/Tan_G)
                 Positive = False
                 DbgStrV = '###1: {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}'.format(
                                Len_V, Distance, Dig_E, Dig_C, Dig_G, Tan_G, 0.0)
            else:
                 Dig_E = EyeAngle[1]
                 Dig_C = CamAngle[1]
                 Dig_G = 180 - (Dig_E + Dig_C)
                 Tan_G = math.tan(math.radians(Dig_G))
                 Tan_C = math.tan(math.radians(Dig_C))
                 Len_V = Distance * (1/Tan_G + 1/Tan_C)
                 Positive = True
                 DbgStrV = '###2: {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}'.format(
                                Len_V, Distance, Dig_E, Dig_C, Dig_G, Tan_G, Tan_C)
        else:
            if EyeAngle[1] < 0:
                 Dig_E = abs(EyeAngle[1])
                 Dig_C = 180 - CamAngle[1]
                 Dig_G = 180 - (Dig_E + Dig_C)
                 Tan_G = math.tan(math.radians(Dig_G))
                 Tan_C = math.tan(math.radians(Dig_C))
                 Len_V = Distance * (1/Tan_G + 1/Tan_C)
                 Positive = False
                 DbgStrV = '###3: {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}'.format(
                                Len_V, Distance, Dig_E, Dig_C, Dig_G, Tan_G, Tan_C)
            else:
                 Dig_E = EyeAngle[1]
                 Dig_C = CamAngle[1]
                 Dig_G = 180 - (Dig_E + Dig_C)
                 Tan_G = math.tan(math.radians(Dig_G))
                 Len_V = Distance * (1/Tan_G)
                 Positive = True
                 DbgStrV = '###4: {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}, {:.02f}'.format(
                                Len_V, Distance, Dig_E, Dig_C, Dig_G, Tan_G, 0.0)
        # Convert MAP from camera to monitor
        if Positive == True:
            PosY_mm = Len_V - 11.1
        else:
            PosY_mm = (Len_V + 11.1) * (-1)

        if self.PrintEn == True:
            print(DbgStrH)
            print(DbgStrV)
            self.PrintEn = False

        #print('{:.02f}, {:.02f}'.format(PosX_mm, PosY_mm))
        return PosX_mm, PosY_mm

# *****************************************************************************
# Signal interrupt
# *****************************************************************************
def KeyboardInterruptHandler(SignalValue, Handler):
    if (SignalValue == signal.SIGINT):
        MVH.Stop()
        print("Pressed Ctrl-C")

# *****************************************************************************
# Main Routine
# *****************************************************************************
if __name__ == '__main__':
    print(__file__)
    print("Script Started.")
    # Start logger
    logger = getLogger(__name__)

    # Set signal interrupt
    signal.signal(signal.SIGINT, KeyboardInterruptHandler)

    # Make instance
    MVH = MccEyeTracker(root_path="./")

    # Run MCC Holistic
    MVH.Run()

    if (MVH.IsRunning() == True):
        print("!!! Unexpected situation !!!")

    logger.info('Script finished successfully.')
    print("Script finished successfully.")
    exit(0)

# End of File