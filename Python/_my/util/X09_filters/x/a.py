# 1. cam angle (plot)
# 2. gaze degree
# 3. projection (plot)
# 4. calibration (plot)

# fix size:
# monitor
# iris length

# *****************************************************************************
# Copyright (C) 2022. MegaChips Corporation. All rights reserved.
# This software and the information contained herein ("Software")
# are PROPRIETARY and CONFIDENTIAL to MegaChips,and are being
# provided solely under the terms and conditions ofa MegaChips
# software license agreement or non-disclosure agreement.Otherwise,
# you have no rights to use or access the Software in any manner.
# No license under any patent, copyright, trade secret or other
# intellectual property right to the Software is granted to or
# conferred upon you by disclosure or delivery of the Software,
# either expressly, by implication, inducement, estoppel or otherwise.
# All or a portion of the Software are not allowed to be distributed,
# copied, or reproduced in any manner, electronic or otherwise,
# without MegaChips' prior written consent.Further, unless otherwise
# agreed in wrtiting by MegaChips, you may not use or utilize
# the Software as or for critical components in life support devices
# or systems.
#
# 1-1-1 Miyahara Yodogawa-ku
# Osaka 532-0003
# *****************************************************************************

# *****************************************************************************
# Imports
# *****************************************************************************
import signal
import threading
import tkinter as tk
from tkinter import font
from PIL import Image, ImageTk

from mcc_eye_tracker import MccEyeTracker

# *****************************************************************************
# Defines
# *****************************************************************************
D_TITLE = "MCC Eye Tracker Devkit"
D_VERSION = "V0.02"

D_WINDOW_SIZE_W = 1280
D_WINDOW_SIZE_H = 720
D_WINDOW_SIZE = str(D_WINDOW_SIZE_W) + "x" + str(D_WINDOW_SIZE_H)
D_POINTER_SIZE = 50
D_PIXEL_PER_LENGTH_W_RATE = D_WINDOW_SIZE_W / 208.0  # ONKYO LPM10M01-BL
D_PIXEL_PER_LENGTH_H_RATE = D_WINDOW_SIZE_H / 117.0  # ONKYO LPM10M01-BL
D_FILTER_CNT_MAX = 25

D_ROOT_PATH = "./"
D_FIG_LOGO = D_ROOT_PATH + "Resource/fig/mcc_logo.png"
D_FIG_POINTER_R = D_ROOT_PATH + "Resource/fig/pointer_r.png"
D_FIG_POINTER_B = D_ROOT_PATH + "Resource/fig/pointer_b.png"
D_FIG_POINTER_G = D_ROOT_PATH + "Resource/fig/pointer_g.png"
D_FIG_POINTER_GRY = D_ROOT_PATH + "Resource/fig/pointer_gray.png"
D_FIG_INITIALIZE = D_ROOT_PATH + "Resource/fig/init.png"
D_FIG_CALIBRATION = D_ROOT_PATH + "Resource/fig/calibration.png"
D_FIG_QUIT = D_ROOT_PATH + "Resource/fig/quit.png"
D_FIG_NO_VIDEO = D_ROOT_PATH + "Resource/fig/no_image_320x240.png"
D_FIG_NO_EYE_VIDEO = D_ROOT_PATH + "Resource/fig/no_image_160x120.png"

D_GUI_ROUTINE_INTERVAL = 30	 # msec

D_STATE_INITIAL = 0
D_STATE_INITIAL_TH = 1
D_STATE_IDLE = 2
D_STATE_CALIBRATION = 3
D_STATE_TRACKING = 4
D_STATE_QUIT = 5
D_STATE_END = 100

D_GUI_BUTTON_X_OFFSET = 200
D_GUI_BUTTON_X_OFFSET = 200

# *****************************************************************************
# Gloabal Function
# *****************************************************************************
def ConvFloat2Int(float_val):
    temp_int = int(float_val)
    temp_float = float_val - temp_int
    if temp_float >= 0.5:
        out_int = temp_int + 1
    else:
        out_int = temp_int
    return out_int

# *****************************************************************************
# Demo GUI Class
# *****************************************************************************
class DemoGui(tk.Frame):
    # *************************************************************************
    # Constracter/Destracter
    # *************************************************************************
    def __init__(self, Window=None):
        self.State = D_STATE_INITIAL
        self.BasePos_X = int((D_WINDOW_SIZE_W/2) - (D_POINTER_SIZE/2))
        self.BasePos_Y = int((D_WINDOW_SIZE_H/2) - (D_POINTER_SIZE/2))
        self.CurrPos_X = self.BasePos_X
        self.CurrPos_Y = self.BasePos_Y
        self.CalibPos_X = 0
        self.CalibPos_Y = 0
        self.CalibRate_X = 0
        self.CalibRate_Y = 0
        self.FilterCnt = 0
        self.FilterBuff_X = []
        self.FilterBuff_Y = []
        # Create window
        super().__init__(Window)
        self.WindowWidth = D_WINDOW_SIZE_W
        self.WindowHeight = D_WINDOW_SIZE_H
        self.Window = Window
        self.Window.title(D_TITLE + " " + D_VERSION)
        self.Window.geometry(D_WINDOW_SIZE)
        # Canvas Settings
        self.Canvas = tk.Canvas(self.Window, bg="white", width=D_WINDOW_SIZE_W, height=D_WINDOW_SIZE_H)
        #self.Canvas.place(x=0, y=0)
        self.Canvas.place(x=0, y=0, relwidth=1.0, relheight=1.0)
        # Image set to canvas
        PosX = 0
        PosY = 0
        self.LogoTk,  self.LogoCanvas = self.CreateImageCanvas(self.Canvas, D_FIG_LOGO,        PosX, PosY,  tk.NW, Tag="Logo")
        PosX = 10
        PosY = self.WindowHeight - 130
        self.InitTk,  self.InitCanvas = self.CreateImageCanvas(
            self.Canvas, D_FIG_INITIALIZE,  PosX, PosY, tk.NW, Tag="Init",    Callback=self.Event_Initialize)
        PosX = 280
        PosY = self.WindowHeight - 130
        self.CalibTk, self.CalibCanvas = self.CreateImageCanvas(
            self.Canvas, D_FIG_CALIBRATION, PosX, PosY, tk.NW, Tag="Calib",   Callback=self.Event_Calibration)
        PosX = self.WindowWidth - 180
        PosY = self.WindowHeight - 130
        self.QuitTk,  self.QuitCanvas = self.CreateImageCanvas(
            self.Canvas, D_FIG_QUIT,        PosX, PosY, tk.NW, Tag="Quit",    Callback=self.Event_Quit)
        PosX = self.WindowWidth - 400
        PosY = 45
        self.VideoTk, self.VideoCanvas = self.CreateImageCanvas(self.Canvas, D_FIG_NO_VIDEO,    PosX, PosY, tk.NW, Tag="Video")
        PosX = self.WindowWidth - 400
        PosY = 300
        self.LeftEyeVideoTk, self.LeftEyeVideoCanvas = self.CreateImageCanvas(
            self.Canvas, D_FIG_NO_EYE_VIDEO,    PosX, PosY, tk.NW, Tag="LeftEyeVideo")
        PosX = self.WindowWidth - 238
        PosY = 300
        self.RightEyeVideoTk, self.RightEyeVideoCanvas = self.CreateImageCanvas(
            self.Canvas, D_FIG_NO_EYE_VIDEO,    PosX, PosY, tk.NW, Tag="RightEyeVideo")
        self.PointerTk, self.PointerCanvas = self.CreateImageCanvas(
            self.Canvas, D_FIG_POINTER_R,  self.CurrPos_X, self.CurrPos_Y, tk.NW, Tag="Pointer", Callback=self.Event_Calibration)
        self.PointerBTk, _ = self.CreateImageCanvas(self.Canvas, D_FIG_POINTER_B, self.CurrPos_X, self.CurrPos_Y, tk.NW, Tag="")
        self.PointerGTk, _ = self.CreateImageCanvas(self.Canvas, D_FIG_POINTER_G, self.CurrPos_X, self.CurrPos_Y, tk.NW, Tag="")
        self.PointerGRYTk, _ = self.CreateImageCanvas(self.Canvas, D_FIG_POINTER_GRY, self.CurrPos_X, self.CurrPos_Y, tk.NW, Tag="")
        self.Canvas.itemconfig(self.PointerCanvas, image=self.PointerGRYTk, anchor=tk.NW)
        PosX = 0
        PosY = 35
        self.LineIdTop = self.CreateLineCanvas(self.Canvas, PosX, PosY, self.WindowWidth)
        PosX = 0
        PosY = self.WindowHeight - 15
        self.LineIdBottom = self.CreateLineCanvas(self.Canvas, PosX, PosY, self.WindowWidth)

        Str = 'Distance(mm)\t\t: 0.0\n'
        Str += 'Pointer Location(x, y)\t: (0, 0)\n'
        Str += 'FPS\t\t\t\n'
        Str += '  facedet, landmark, gaze\t: 0.0\n'
        Str += '  + iris\t\t\t: 0.0\n'
        self.StrCanvas = self.CreateTextCanvas(self.Canvas, Str, 10, 45, tk.NW)

        self.Window.bind('<Configure>', self.Event_WindowSizeChange)
        # Initialize Instance
        self.MET = MccEyeTracker(root_path=D_ROOT_PATH)
        # Thread start
        self.VideoThread = None
        self.StartVideoThread()
        # GUI routine
        self.after(D_GUI_ROUTINE_INTERVAL, self.GuiRoutine)
    #
    # Create Image Canvas
    #

    def CreateImageCanvas(self, Canvas, FigPath, Xpos, Ypos, Anc, Tag="", Callback=None):
        # Open Image Files
        Img = Image.open(FigPath)
        ImgTk = ImageTk.PhotoImage(Img)
        ImageCanvas = None
        if Tag != "":
            ImageCanvas = self.Canvas.create_image(Xpos, Ypos, image=ImgTk,  anchor=Anc,  tags=Tag)
            if Callback != None:
                Canvas.tag_bind(Tag, "<ButtonPress-1>", Callback)
        return ImgTk, ImageCanvas
    #
    # Create Text Canvas
    #
    def CreateTextCanvas(self, Canvas, TextStr, Xpos, Ypos, Anc, Tag="", Callback=None):
        TxtFont = font.Font(family="Helvetica", size=20, weight="bold")
        TextCanvas = Canvas.create_text(Xpos, Ypos, text=TextStr, font=TxtFont, fill="blue", anchor=Anc)
        return TextCanvas
    #
    # Create Line Canvas
    #
    def CreateLineCanvas(self, Canvas, Xpos, Ypos, Length):
        LineId = Canvas.create_line(Xpos, Ypos, (Length - Xpos), Ypos, width=5, fill="#2471C6", state=tk.DISABLED)
        return LineId

    # --------------------------------------------------------------------------
    # GUI Routine
    # --------------------------------------------------------------------------
    def GuiRoutine(self):
        if self.State == D_STATE_INITIAL:
            self.State = self.Proc_Initialize()
        elif self.State == D_STATE_INITIAL_TH:
            self.State = self.Proc_InitialThread()
        elif self.State == D_STATE_IDLE:
            self.State = self.Proc_Idle()
        elif self.State == D_STATE_CALIBRATION:
            self.State = self.Proc_Calibration()
        elif self.State == D_STATE_TRACKING:
            self.State = self.Proc_Tracking()
        elif self.State == D_STATE_QUIT:
            self.State = self.Proc_Quit()
        else:
            print('Invalid State = {:d}'.format(self.State))
            self.State = self.Proc_Quit()

        if self.State <= D_STATE_QUIT:
            self.after(D_GUI_ROUTINE_INTERVAL, self.GuiRoutine)

    # *************************************************************************
    # Event Function
    # *************************************************************************
    #
    # Event: Initialize
    #
    def Event_Initialize(self, event):
        self.State = D_STATE_INITIAL
    #
    # Event: Calibration
    #
    def Event_Calibration(self, event):
        if self.State == D_STATE_IDLE:
            self.State = D_STATE_CALIBRATION
    #
    # Event: Quit
    #
    def Event_Quit(self, event):
        self.State = D_STATE_QUIT
        # self.UpdatePointer()
    #
    # Event: Window size change
    #
    def Event_WindowSizeChange(self, event):
        self.Window.update()
        Width = max(self.Canvas.winfo_width(), D_WINDOW_SIZE_W)
        Height = max(self.Canvas.winfo_height(), D_WINDOW_SIZE_H)
        print('{:d}, {:d}'.format(Width, Height))
        if self.WindowWidth != Width or self.WindowHeight != Height:
            self.WindowWidth = Width
            self.WindowHeight = Height
            self.Canvas.config(width=Width, height=Height)
            PosX = 0
            PosY = 0
            self.Canvas.moveto("Logo", PosX, PosY)
            PosX = 0
            PosY = 35
            self.Canvas.moveto("Line", PosX, PosY)
            PosX = 10
            PosY = self.WindowHeight - 130
            self.Canvas.moveto("Init", PosX, PosY)
            PosX = 280
            PosY = self.WindowHeight - 130
            self.Canvas.moveto("Calib", PosX, PosY)
            PosX = self.WindowWidth - 180
            PosY = self.WindowHeight - 130
            self.Canvas.moveto("Quit", PosX, PosY)
            PosX = self.WindowWidth - 400
            PosY = 45
            self.Canvas.moveto("Video", PosX, PosY)
            PosX = self.WindowWidth - 400
            PosY = 300
            self.Canvas.moveto("LeftEyeVideo", PosX, PosY)
            PosX = self.WindowWidth - 238
            PosY = 300
            self.Canvas.moveto("RightEyeVideo", PosX, PosY)
            PosX = 0
            PosY = 35
            self.Canvas.coords(self.LineIdTop, PosX, PosY, (self.WindowWidth - PosX), PosY)
            PosX = 0
            PosY = self.WindowHeight - 15
            self.Canvas.coords(self.LineIdBottom, PosX, PosY, (self.WindowWidth - PosX), PosY)
            self.BasePos_X = int((self.WindowWidth/2) - (D_POINTER_SIZE/2))
            self.BasePos_Y = int((self.WindowHeight/2) - (D_POINTER_SIZE/2))
        self.State = D_STATE_INITIAL

    # *************************************************************************
    # Processing Function
    # *************************************************************************
    #
    # Proc: Initialize
    #
    def Proc_Initialize(self):
        self.CurrPos_X = self.BasePos_X
        self.CurrPos_Y = self.BasePos_Y
        self.CalibPos_X = 0
        self.CalibPos_Y = 0
        self.CalibRate_X = 0
        self.CalibRate_Y = 0
        self.FilterCnt = 0
        self.FilterBuff_X = []
        self.FilterBuff_Y = []
        self.Canvas.moveto("Pointer", self.CurrPos_X, self.CurrPos_Y)
        self.Canvas.itemconfig(self.PointerCanvas, image=self.PointerGRYTk, anchor=tk.NW)
        return D_STATE_INITIAL_TH
    #
    # Proc: Initialize Thread (Just wait for video thread ready)
    #
    def Proc_InitialThread(self):
        if self.MET.IsEyeTrackingReady() == False:
            return D_STATE_INITIAL_TH
        self.Canvas.itemconfig(self.PointerCanvas, image=self.PointerBTk, anchor=tk.NW)
        return D_STATE_IDLE
    #
    # Proc: Idle
    #
    def Proc_Idle(self):
        if self.MET.IsUpdated() == True:
            self.UpdateVideo()
        return D_STATE_IDLE
    #
    # Proc: Calibration
    #
    def Proc_Calibration(self):
        # Wait for video update
        if self.MET.IsUpdated() == True:
            self.UpdateVideo()
        else:
            return D_STATE_CALIBRATION

        # Filtering Info
        PosX_mm, PosY_mm = self.MET.GetPointerPosition()
        AvgX, AvgY = self.AverageFilter(PosX_mm, PosY_mm)
        if AvgX == 0 or AvgY == 0:
            self.Canvas.itemconfig(self.PointerCanvas, image=self.PointerGTk, anchor=tk.NW)
            return D_STATE_CALIBRATION

        # X: Calibration
        PosX_pix = AvgX * D_PIXEL_PER_LENGTH_W_RATE
        PosX_pix = ConvFloat2Int(PosX_pix)
        #self.CalibRate_X = (self.BasePos_X / PosX_pix)
        PosX_tmp = self.RoundOffBorder(PosX_pix, self.WindowWidth, D_POINTER_SIZE)
        #PosX_tmp = self.RoundOffBorder(PosX_pix * self.CalibRate_X, self.WindowWidth, D_POINTER_SIZE)
        self.CalibPos_X = self.BasePos_X - ConvFloat2Int(PosX_tmp)

        # Y: Calibration
        PosY_pix = AvgY * D_PIXEL_PER_LENGTH_H_RATE
        PosY_pix = ConvFloat2Int(PosY_pix)
        self.CalibRate_Y = (self.BasePos_Y / PosY_pix)
        #PosY_tmp = self.RoundOffBorder(PosY_pix, self.WindowHeight, D_POINTER_SIZE)
        #PosY_tmp = self.RoundOffBorder(PosY_pix * self.CalibRate_Y, self.WindowHeight, D_POINTER_SIZE)
        #self.CalibPos_Y = self.BasePos_Y - ConvFloat2Int(PosY_tmp)

        self.Canvas.itemconfig(self.PointerCanvas, image=self.PointerTk, anchor=tk.NW)

        print('Calib_X:{:d} Calib_Y:{:d}'.format(self.CalibPos_X, self.CalibPos_Y))
        return D_STATE_TRACKING
    #
    # Proc: Tracking
    #
    def Proc_Tracking(self):
        if self.MET.IsUpdated() == True:
            self.UpdateVideo()
            self.UpdatePointer()
        return D_STATE_TRACKING
    #
    # Proc: Quit
    #
    def Proc_Quit(self):
        self.QuitApplication()
        return D_STATE_END

    # --------------------------------------------------------------------------
    # Processing Sub-Functions
    # --------------------------------------------------------------------------
    #
    # Update Pointer
    #
    def __trim_x_pos_for_1280x720(self, pos):
        if pos < self.BasePos_X:  # Left the center of the display
            pos = pos + ((self.BasePos_X - pos) * 0.45)
        elif pos > self.BasePos_X:  # Right from the center of the display
            pos = pos - ((pos - self.BasePos_X) * 0.71)
        else:
            pass
        return pos

    def __trim_y_pos_1280x720(self, pos):
        if pos < self.BasePos_Y:  # Above the center of the display
            limit_y_from_display_center = self.BasePos_Y - 242
            rate = (self.BasePos_Y - pos) / limit_y_from_display_center
            pos = pos - (self.BasePos_Y * rate)
        elif pos > self.BasePos_Y:  # Down from the center of the display
            limit_y_from_display_center = 480 - self.BasePos_Y
            rate = (pos - self.BasePos_Y) / limit_y_from_display_center
            pos = pos + (self.BasePos_Y * rate)
        else:
            pass
        return pos

    def UpdatePointer(self):
        # Get current info
        #self.MET.PrintEn = True
        PosX_mm, PosY_mm = self.MET.GetPointerPosition()
        AvgX, AvgY = self.AverageFilter(PosX_mm, PosY_mm)  # PosX_mm, PosY_mm
        if AvgX == 0 or AvgY == 0:
            return

        # X: Convert mm to pixel
        PosX_pix = AvgX * D_PIXEL_PER_LENGTH_W_RATE
        PosX_pix = ConvFloat2Int(PosX_pix) + self.CalibPos_X
        #PosX_pix = ConvFloat2Int(PosX_pix * self.CalibRate_X)
        PosX_pix = self.__trim_x_pos_for_1280x720(PosX_pix)
        self.CurrPos_X = self.RoundOffBorder(PosX_pix, self.WindowWidth, D_POINTER_SIZE)

        # Y: Convert mm to pixel
        PosY_pix = AvgY * D_PIXEL_PER_LENGTH_H_RATE
        #PosY_pix = ConvFloat2Int(PosY_pix) + self.CalibPos_Y
        PosY_pix = ConvFloat2Int(PosY_pix * self.CalibRate_Y)
        PosY_pix = self.__trim_y_pos_1280x720(PosY_pix)
        self.CurrPos_Y = self.RoundOffBorder(PosY_pix, self.WindowHeight, D_POINTER_SIZE)

        # Move Pointer
        self.Canvas.moveto("Pointer", self.CurrPos_X, self.CurrPos_Y)

    #
    # Filter functions
    #
    def AverageFilter(self, PosX_mm, PosY_mm):
        temp = 0
        length = len(self.FilterBuff_X)
        if length >= D_FILTER_CNT_MAX:
            for i in range(length):
                temp += self.FilterBuff_X[i]
            AvgX = temp / length
            self.FilterBuff_X.pop(0)
            self.FilterBuff_X.append(PosX_mm)
        else:
            AvgX = 0
            self.FilterBuff_X.append(PosX_mm)

        temp = 0
        length = len(self.FilterBuff_Y)
        if length >= D_FILTER_CNT_MAX:
            for i in range(length):
                temp += self.FilterBuff_Y[i]
            AvgY = temp / length
            self.FilterBuff_Y.pop(0)
            self.FilterBuff_Y.append(PosY_mm)
        else:
            AvgY = 0
            self.FilterBuff_Y.append(PosY_mm)

        return AvgX, AvgY
    #
    # Round off function
    #
    def RoundOffBorder(self, Pos, BorderSize, BoxSize):
        if Pos < 0:
            OutPos = 0 - (BoxSize/2)  # 0
        elif Pos > (BorderSize - BoxSize):
            OutPos = BorderSize - (BoxSize/2)
        else:
            OutPos = Pos
        return OutPos

    #
    # Update Video Area
    #
    def UpdateVideo(self):
        # Video updating
        src_img, eye_imgs = self.MET.GetOutputVideo()
        if len(src_img) != 0:
            PilImg = Image.fromarray(src_img)
            #PilImg = PilImg.resize((160, 120))
            PilImg = PilImg.resize((320, 240))

            self.CamImgTk = ImageTk.PhotoImage(PilImg)
            self.Canvas.itemconfig(self.VideoCanvas, image=self.CamImgTk, anchor=tk.NW)

        if None is not eye_imgs:
            PilLeftEyeImg = Image.fromarray(eye_imgs[0])
            PilLeftEyeImg = PilLeftEyeImg.resize((160, 120))
            self.CamLeftEyeImgTk = ImageTk.PhotoImage(PilLeftEyeImg)
            self.Canvas.itemconfig(self.LeftEyeVideoCanvas, image=self.CamLeftEyeImgTk, anchor=tk.NW)

            PilRightEyeImg = Image.fromarray(eye_imgs[1])
            PilRightEyeImg = PilRightEyeImg.resize((160, 120))
            self.CamRightEyeImgTk = ImageTk.PhotoImage(PilRightEyeImg)
            self.Canvas.itemconfig(self.RightEyeVideoCanvas, image=self.CamRightEyeImgTk, anchor=tk.NW)

        # Infomation updating
        AppFps, EstFps, FaceGazeLandFps = self.MET.GetFps()
        #InfoStr = 'AppFPS={:.1f}, EstFPS={:.1f}'.format(AppFps, EstFps)
        Distance, _, _ = self.MET.Eye.GetEyeSensingResult()
        Str = f'Distance(mm)\t\t: {Distance:.02f}\n'
        Str += f'Pointer Location(x, y)\t: ({int(self.CurrPos_X)}, {int(self.CurrPos_Y)})\n'
        Str += f'FPS\t\t\t\n'
        Str += f'  facedet, landmark, gaze\t: {FaceGazeLandFps:.1f}\n'
        Str += f'  + iris\t\t\t: {EstFps:.1f}\n'

        self.Canvas.itemconfig(self.StrCanvas, text=Str, anchor=tk.NW)

    # *************************************************************************
    # Start Thread
    # *************************************************************************
    def StartVideoThread(self):
        self.VideoThread = threading.Thread(target=self.MET.Run)
        self.VideoThread.start()
        print("Video thread started.")

    # *************************************************************************
    # Stop Thread
    # *************************************************************************
    def StopVideoThread(self):
        self.MET.Stop()
        print("Video thread stopping...")
        self.VideoThread.join()
        print("Video thread stopped.")

    # *************************************************************************
    # Quit GUI application
    # *************************************************************************
    def QuitApplication(self):
        self.StopVideoThread()
        self.Window.quit()

# *****************************************************************************
# Close Button Click
# *****************************************************************************
def CloseButtonClick():
    Gui.QuitApplication()
    print("Click close button")

# *****************************************************************************
# Signal interrupt
# *****************************************************************************
def KeyboardInterruptHandler(SignalValue, Handler):
    if (SignalValue == signal.SIGINT):
        Gui.QuitApplication()
        print("Pressed Ctrl-C")


# *****************************************************************************
# Main Routine
# *****************************************************************************
if __name__ == '__main__':
    print(__file__)
    print("Script Started.")

    # Set signal interrupt
    signal.signal(signal.SIGINT, KeyboardInterruptHandler)

    # Run Demo Application
    RootWindow = tk.Tk()
    Gui = DemoGui(Window=RootWindow)

    RootWindow.protocol("WM_DELETE_WINDOW", CloseButtonClick)
    Gui.mainloop()

    print("Script finished successfully.")
    exit(0)

# End of File
