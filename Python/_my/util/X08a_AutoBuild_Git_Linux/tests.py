import os

def run_cmd_to_buf(cmd):
    import subprocess
    return subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')

# return build_this_commit
def git_filter_c_cpp_python(git_folder, commit):
    # find .c .cpp .h .hpp file
    result = run_cmd_to_buf(['git', '-C', git_folder, 'show', commit])
    find_word = False
    for each_line in result:
        seach_line = each_line.split()
        # print(seach_line)
        for each_word in seach_line:
            if(each_word == '+++' or each_word == '---'):
                find_word = True
            elif(find_word == True):
                log_word = each_word.split('/')[-1]
                log_word_extension = log_word.rsplit('.', 1)[-1]
                if log_word_extension in ['c', 'cpp', 'h', 'hpp', 'py'] or log_word == 'CMakeLists.txt':
                    return True #build_this_commit
                find_word = False
    return False

# def app_test_sanity(ws, db):
#     cmd = 'cd ' + ws + 'app_test_sanity'
#     cmd = cmd + ' && rm build/ -rf && mkdir build/ && '
#     cmd = cmd + 'cd build/ && cmake .. && make -j2'
#     os.system(cmd)
#     pass

class MyTests(object):
    def __init__(self, ws, db, commit):
        self.ws = ws
        self.db = db
        self.cm = commit
        self.make_opt = 'make -j2'
        self.enable_email = False
        self.test_result = [] # list of string
        self.is_failed = False
        self.is_skipped = False

    def send_email(self):
        return #Flake: has bug now
        import smtplib

        TO = 'flake.yeh@nui-ai.com'
        SUBJECT = 'TEST MAIL'
        TEXT = 'Here is a message from python.\n\n'

        # Gmail Sign In
        gmail_sender = 'nui.ai.com@gmail.com'
        gmail_passwd = 'Nui90270757'

        print ('email SMTP config ... 1')
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        print ('email SMTP config ... 2')
        server.login(gmail_sender, gmail_passwd)

        TEXT = TEXT + " ====== [[ TEST SUMMARY ]] ====== " + self.cm + "\n"
        for i in self.test_result:
            TEXT = TEXT + i + '\n'

        BODY = '\r\n'.join(['To: %s' % TO,
                            'From: %s' % gmail_sender,
                            'Subject: %s' % SUBJECT,
                            '', TEXT])
        
        try:
            print ('email sending ...')
            server.sendmail(gmail_sender, [TO], BODY)
            print ('email sent')
        except:
            print ('email sending error')

        server.quit()
        pass

    def append_fail_result(self, _str):
        print(_str)
        self.test_result.append(_str)
        self.is_failed = True

    def append_skip_result(self, _str):
        print(_str)
        self.test_result.append(_str)
        self.is_skipped = True

    def dump_test_result(self):
        print("\n ====== [[ SINGLE COMMIT TEST RESULT ]] ====== ", self.cm)
        for i in self.test_result:
            print(i)
        print("")
        if self.is_failed and self.enable_email:
            self.send_email()
        
    def cmake_log_string(self):
        return self.db + 'test_' + self.tf + '_cmake.txt'

    def make_log_string(self):
        return self.db + 'test_' + self.tf + '_make.txt'
    
    def run_log_string(self):
        return self.db + 'test_' + self.tf + '_run.txt'
    
    def prepare_test_data(self, test_folder, cmake_option=''):
        self.tf = test_folder
        self.cur_folder = self.ws + self.tf

        # cmake & make 
        build_folder = self.ws + self.tf + '/build/'

        self.build_folder = build_folder

        # remove build/
        # mkdir build/
        self.build_folder_cmd = \
            'rm ' + build_folder + ' -rf && ' + \
            'mkdir ' + build_folder
        
        # cd build/ && cmake ..>cmake_app_demo_cpp.txt
        self.cmake_cmd = \
            'cd ' + build_folder + ' && cmake .. ' + cmake_option + '>' + self.cmake_log_string()
        
        # cd build/ && make -j4>make_app_demo_cpp.txt
        self.make_cmd = \
            'cd ' + build_folder + f' && {self.make_opt}>' + self.make_log_string() + ' 2>&1'
        
        # cd build/ && ./a>run_app_demo_cpp.txt
        self.run_cmd = \
            'cd ' + build_folder + ' && ./a>' + self.run_log_string()

    def parse_cmake_log(self):
        print('[CMAKE LOG] ', self.cmake_log_string())
        return True

    def parse_make_log(self):
        print('[MAKE LOG] ', self.make_log_string())
        with open(self.make_log_string(), 'r') as f:
            for each_line in f:
                words_in_line = each_line.split()
                #print(words_in_line)
                if 'warning:' in words_in_line:
                    return False
                if len(words_in_line) >= 2:
                    if words_in_line[0] == "[100%]" and words_in_line[1] == "Built":
                        return True
        return False

    def parse_run_log(self):
        print('[RUN LOG] ', self.run_log_string())
        with open(self.run_log_string(), 'r') as f:
            for each_line in f:
                words_in_line = each_line.split()
                #print(words_in_line)
                if len(words_in_line) >= 1:
                    if words_in_line[0] == "[FAILED]":
                        return False
        return True

    # return needs_clean_run
    def test_start_speed_run(self):
        #print("find build folder exists: ", self.build_folder)
        if os.path.exists(self.build_folder):
            print("speed run start")
            os.system(self.cmake_cmd)
            cmake_succeed = self.parse_cmake_log()
            if not cmake_succeed:
                return True # needs_clean_run
            
            os.system(self.make_cmd)
            make_succeed = self.parse_make_log()
            if not make_succeed:
                return True # needs_clean_run
            
            os.system(self.run_cmd)
            run_succeed = self.parse_run_log()
            if not run_succeed:
                return True # needs_clean_run
            
            # All Pass
            _str = "[PASS]   " + self.tf + " (speed run)"
            self.append_fail_result(_str)
            return False # needs_clean_run
        else:
            return True # needs_clean_run

    def test_start(self):
        # needs_clean_run
        if self.test_start_speed_run():
            print("clean run start")
            print('[BUILD FOLDER] ', self.build_folder_cmd)
            os.system(self.build_folder_cmd)
            print('[CMAKE CMD] ', self.cmake_cmd)

            os.system(self.cmake_cmd)
            cmake_succeed = self.parse_cmake_log()
            if not cmake_succeed:
                _str = "[FAILED] " + self.tf + " ( CMAKE FAILED )"
                self.append_fail_result(_str)
                return
            
            print("[start make] :")
            print(self.make_cmd)
            os.system(self.make_cmd)
            make_succeed = self.parse_make_log()
            if not make_succeed:
                _str = "[FAILED] " + self.tf + " ( MAKE FAILED )"
                self.append_fail_result(_str)
                return
            
            os.system(self.run_cmd)
            run_succeed = self.parse_run_log()
            if not run_succeed:
                _str = "[FAILED] " + self.tf + " ( RUN FAILED )"
                self.append_fail_result(_str)
                return

            # All Pass
            _str = "[PASS]   " + self.tf
            self.append_fail_result(_str)

    def clear_test_logs(self):
        os.system('rm ' + self.db + 'test*.txt -f')
        pass

    def go(self):
        print('start_tests')
        # init
        self.clear_test_logs()

        if git_filter_c_cpp_python(self.ws, self.cm):
            # start tests one by one
            self.prepare_test_data('app_demo_cpp')
            self.test_start()

            # self.prepare_test_data('app_opencv')
            # self.test_start()

            # self.prepare_test_data('app_d2d_test_inte')
            # self.test_start()
        
        else:
            self.append_skip_result("[SKIPPED]")

        self.dump_test_result()
