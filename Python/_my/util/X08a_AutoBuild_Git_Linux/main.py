import os
import json
import tests

class GitTests(object):
    def __init__(self, ws, db):
        self.ws = ws
        self.db = db
        self.git_file = '00_git_log.txt'
        self.tested_file = '01_tested_commit.txt'
        self.all_tested_file = '02_tested_all.txt'
        self.summary = list()
        self.curr_log = dict()
    
    def get_git_file(self):
        return self.db + self.git_file

    def get_tested_file(self):
        return self.db + self.tested_file

    def get_all_tested_file(self):
        return self.db + self.all_tested_file

    def proc_00_gen_git_log(self):
        cmd = 'cd ' + self.ws + ' && git pull'
        os.system(cmd)
        cmd = 'cd ' + self.ws + ' && git log>' + self.get_git_file()
        os.system(cmd)
    
    # return commit string
    def proc_01_get_tested_commit(self):
        _file = self.get_tested_file()
        f = open(_file)
        rd = json.load(f)
        f.close()
        #print('rd = ', rd)
        #print('len(rd) = ', len(rd))
        if len(rd) == 0:
            print("[FAILED] please check tested file:", _file)
            exit()
            return ""
        else:
            return rd["commit"]

    # return commit string list (reversed from git log)
    def proc_02_get_all_untested_commits(self, latest_tested):
        out = []
        with open(self.get_git_file(), 'r') as f:
            for each_line in f:
                words_in_line = each_line.split()
                if len(words_in_line) == 2:
                    if words_in_line[0] == 'commit':
                        #print(words_in_line[1])
                        if words_in_line[1] == latest_tested:
                            break
                        else:
                            out.append(words_in_line[1])
        out.reverse()
        return out

    def proc_031_update_db(self, cur_commit):
        # find commit and gen dict
        parse_state = 0
        cdict = dict()
        with open(self.get_git_file(), 'r') as f:
            for each_line in f:
                words_in_line = each_line.split()
                if len(words_in_line) >= 1 and words_in_line[0] == 'Merge:':
                    continue
                if parse_state == 0:
                    if len(words_in_line) == 2:
                        if words_in_line[0] == 'commit' and words_in_line[1] == cur_commit:
                            cdict['commit'] = cur_commit
                            parse_state = 1
                elif parse_state == 1:
                    cdict['A'] = each_line
                    parse_state = 2
                elif parse_state == 2:
                    cdict['D'] = each_line
                    break
    
        cdict['Result'] = self.curr_log

        # update 01_tested_commit.txt
        f = open(self.get_tested_file(), "w")
        json.dump(cdict, f, indent=2)
        f.close()

        # append 02_tested_all.txt
        f = open(self.get_all_tested_file())
        tested_list = json.load(f)
        f.close()

        tested_list.insert(0, cdict)

        f = open(self.get_all_tested_file(), "w")
        json.dump(tested_list, f, indent=2)
        f.close()

    def proc_032_move_log(self, cur_commit):
        log_folder = self.db + 'log/' + cur_commit
        os.system('mkdir -p ' + log_folder)
        os.system('mv -f ' + self.db + 'test*.txt ' +  log_folder)

    def proc_03_checkout_loop(self, untested_commits):
        import copy
        for nearest_commit in untested_commits:
            os.system('cd ' + ws + ' && git checkout ' +  nearest_commit)
            print('[GIT] to nearest:', nearest_commit)
            single_test = tests.MyTests(ws, db, nearest_commit)
            single_test.go()
            # export & make single test log
            self.curr_log = dict()
            self.curr_log['commit'] = nearest_commit
            self.curr_log['log'] = copy.deepcopy(single_test.test_result)
           
            self.summary.append(self.curr_log)
            print('[GIT] checkout main')
            os.system('cd ' + ws + ' && git checkout main')

            # update 01_tested_commit.txt
            # update 02_tested_all.txt
            self.proc_031_update_db(nearest_commit)
            self.proc_032_move_log(nearest_commit)

            #exit()
        self.proc_dump_summry()

    def proc_dump_summry(self):
        print('\n====== [[ SUMMARY ]] ======')
        for i in self.summary:
            print('\ncommit:', i['commit'])
            log_list = i['log']
            for j in log_list:
                print('   ', j)

    def go(self):
        self.proc_00_gen_git_log()

        latest_tested = self.proc_01_get_tested_commit()

        print('latest_tested = ', latest_tested)

        untested_commits = self.proc_02_get_all_untested_commits(latest_tested)

        print('untested_commits = ', untested_commits)

        self.proc_03_checkout_loop(untested_commits)

'''
TODO:
start without commit (using current state)
    log folder = db/log/current

Add option?

curr test folder
    app_d2d_prof
    app_d2d_test_unit
    app_d2d_test_inte
    app_d2d_sanity

'''


ws = '/home/pupil/temp/p1/pupil_p1/'
db = '/home/pupil/sda/ws/r0/Python/X08_AutoBuild_Git_Linux/test_database/'

gt = GitTests(ws, db)
gt.go()

exit()
