# -*- coding: utf-8 -*-
import math
import numpy as np


print('np.binary_repr(3) =', np.binary_repr(3))
print('np.binary_repr(-3, width=5) =', np.binary_repr(-3, width=5))

print(float.hex(0.25))
print(float.hex(0.3))

# 0x3FD3333333333333
#  0x1.3333333333333p-2