#
# https://stackoverflow.com/questions/5787277/python-underscore-as-a-function-parameter
#
# It stores the result of the last expression you evaluated in your interactive interpreter 
# and is used for convenience
#

#
# demo 1
#
x = 10
8 * 9
#print(_) # NameError: name '_' is not defined, flake: only work in "interactive interpreter"

#
# demo 2
#
for _ in range(5):
    print('demo 2')
print(_) # _ exists now


#
# demo 3
#
def foo(_=None):
    return (3,33)

ret,_=foo()
print('demo', ret, _)