
import subprocess

class PipeHelper(object):
    def __init__(self, cmd_list):
        self.cmds = cmd_list
        self.result = ''
        self.proc = None
    
    def run_cmds(self):
        old_proc = None
        new_proc = None
        cmd_num = len(self.cmds)

        if cmd_num == 0:
            return self.result
        elif cmd_num == 1:
            self.result = subprocess.run(self.cmds[0], stdout=subprocess.PIPE).stdout.decode('utf-8')
        else:
            for i in range(cmd_num):
                #print(i)
                if i == 0:
                    new_proc = subprocess.Popen(self.cmds[i], stdout=subprocess.PIPE)
                    #print(new_proc)
                elif i != cmd_num - 1:
                    new_proc = subprocess.Popen(self.cmds[i], stdin=old_proc.stdout, stdout=subprocess.PIPE)
                    #print(new_proc)
                else:
                    self.result = subprocess.run(self.cmds[i], stdin=old_proc.stdout, stdout=subprocess.PIPE).stdout.decode('utf-8')
                    #print(result)
                old_proc = new_proc
        return self.result
    
    def popen_cmds(self):
        old_proc = None
        new_proc = None
        cmd_num = len(self.cmds)
        if cmd_num == 0:
            return self.proc
        for i in range(cmd_num):
            #print(i)
            if i == 0:
                new_proc = subprocess.Popen(self.cmds[i], stdout=subprocess.PIPE)
                #print(new_proc)
            elif i != cmd_num - 1:
                new_proc = subprocess.Popen(self.cmds[i], stdin=old_proc.stdout, stdout=subprocess.PIPE)
                #print(new_proc)
            else:
                new_proc = subprocess.Popen(self.cmds[i], stdin=old_proc.stdout, stdout=subprocess.PIPE)
            old_proc = new_proc
            self.proc = new_proc
        return self.proc
    
    def wait(self):
        if self.proc:
            self.proc.wait()

    def popen(self):
        return self.popen_cmds()

    def run(self):
        return self.run_cmds()
    
    def run_with_split(self):
        return self.run_cmds().split('\n')

