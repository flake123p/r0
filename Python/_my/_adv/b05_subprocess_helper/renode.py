
import util
import time

def run_cmd_to_buf(cmd):
    import subprocess
    return subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')

cmd_start_renode = util.PipeHelper([
    ['renode', '/opt/renode/scripts/prof3.resc']
])

cmd_kill_renode = util.PipeHelper([
    ['ps', 'aux'],
    ['grep', 'renode'],
    ['awk', '{print $2}'],
    ['xargs', 'kill', '-9']
])

cmd_check_renode = util.PipeHelper([
    ['ps', 'aux'],
    ['grep', 'Renode.exe']
])

cmd_gdb_renode = util.PipeHelper([
    ['riscv32-unknown-elf-gdb', '-x', 'gdbat', '-batch', '-quiet']
])

cmd_start_renode.popen()

max_loop = 5
for i in range(max_loop):
    time.sleep(6)
    if '/opt/renode/bin/Renode.exe' in cmd_check_renode.run():
        print('Found renode, i =', i)
        break
    else:
        print('Keep detect renode, i =', i)
    
    if i == max_loop - 1:
        print('Failed 111')
        exit()

print("start gdb")
print(cmd_gdb_renode.run())
#run_cmd_to_buf(['riscv32-unknown-elf-gdb', '-x', 'gdbat', '-batch', '-quiet'])
print("end gdb")

cmd_kill_renode.run()

cmd_start_renode.wait()