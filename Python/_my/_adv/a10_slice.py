#
#
# 

import numpy as np

a = np.array([0, 10, 20, 30, 40, 50, 60, 70, 80, 90])
print(a[:3])    # 0 1 2
print(a[1:3])   # 1 2
print(a[-3:])   # 7 8 9
print(a[3::])   # 3 ~ 9
print(a[3::2])  # 3 5 7 9
print(a[3::5])  # 3 8
print(a[...])   # all
print(a)        # all

#
# slice 2d array --> ONLY in numpy!!!!!!!!!!!!!!1
#
#slice = arr[:2,:2]

import numpy as np
f = np.arange(0.0, 15.0, 2.5)
print(f)