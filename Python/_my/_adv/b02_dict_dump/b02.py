#
#   json - dict dump
#   https://www.geeksforgeeks.org/write-a-dictionary-to-a-file-in-python/
#   https://stackoverflow.com/questions/36965507/writing-a-dictionary-to-a-text-file
#

def dict_dump_json(ex_dict, file='dict_out_json.txt'):
    import json
    with open(file, 'w') as fp:
        fp.write(json.dumps(ex_dict))  # use `json.loads` to do the reverse

def dict_load_json(file='dict_out_json.txt'):
    import json
    with open(file, 'r') as fp:
        dic = json.loads(fp.read())
        return dic

def dict_dump(ex_dict, file='dict_out.txt'):
    with open(file, 'w') as fp:
        print(ex_dict, file=fp)

def dict_load(file='dict_out.txt'):
    with open(file, 'r') as fp:
        content = fp.read()
        dic = eval(content)
        return dic

# Not fully tested
def dict_dump2(ex_dict, file='dict_out.txt'):
    with open(file, 'w') as fp:
        print('{', file=fp)
        for k, v in ex_dict.items():
            if type(v) is str:
                v = '\"' + v + '\"'
            print('    \"' + k + '\": ', v, ',', file=fp)
        print('}', file=fp)

# as requested in comment
exDict = {
    'k0': 123, 
    'k1': 'str123',
    'k2': [0.23, 0.76],
    'k3': {'aa': 11, 'bb': 22},
}

print("========= dump - json version =========")
print("========= dump - json version =========")
dict_dump_json(exDict)
print(dict_load_json())

print("========= dump =========")
print("========= dump =========")
dict_dump(exDict)
print(dict_load())

print("========= dump 2 =========")
print("========= dump 2 =========")
dict_dump2(exDict)
print(dict_load())
