#
# https://www.tutorialspoint.com/What-does-double-star-and-star-do-for-parameters-in-Python
#


def l1():
    def l3():
        nonlocal window_size
        window_size = window_size + 200
        print(window_size)

    def l2():
        nonlocal window_size
        window_size = window_size + 30
        l3()

    window_size = 4
    l2()

l1()

var1 = 900
def fun():
    var1 = 10
 
    def gun():
        global var1
        #nonlocal var1 #use fun().var1
        var1 = var1 + 10
        print(var1)
 
    gun()
    print(var1)
fun()