#
# operator:
#   https://docs.python.org/zh-tw/3/library/operator.html
#
from operator import add, mod, mul, neg, sub, truediv, floordiv

#two input lists 
list1 = [1, 2, 3, 7] 
list2 = [3, 6, 9] 

print(' ========= demo 1 : map & add =========')
result = list(map(add, list1, list2))  

print ("Resultant list : ", result)

print(' ========= demo 2 : map type =========')
x = map(add, list1, list2)
print(type(x), x)
for i in x:
    print(type(i), i)

print(' ========= demo 3 : my map =========')
def myAdd(a, b):
    return a + b

result = list(map(myAdd, list1, list2))  

print ("Resultant list : ", result)

print(' ========= demo 4 : mul =========')
print ("Resultant list : ", list(map(mul, list1, list2))  )

print(' ========= demo 5 : sub =========')
print ("Resultant list : ", list(map(sub, list1, list2))  )

print(' ========= demo 6 : truediv =========')
print ("Resultant list : ", list(map(truediv, list1, list2))  )

print(' ========= demo 7 : floordiv =========')
print ("Resultant list : ", list(map(floordiv, list1, list2))  )

print(' ========= demo 8 : 2d list function =========')
x = [[1,1,1],[1,1,2],[1,2,3]]
def sum2d(a):
    temp = a[0]
    for i in range(1,len(a)):
        temp = list(map(add, temp, a[i]))
    return temp
s = sum2d(x)
print(s)
s = [sum(x[:][0]), sum(x[:][1]), sum(x[:][2])]
print(s)
#
# lambda & map: 
# https://stackoverflow.com/questions/17595590/correct-style-for-element-wise-operations-on-lists-without-numpy-python
#
