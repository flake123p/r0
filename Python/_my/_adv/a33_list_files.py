
def run_cmd_to_buf(cmd):
    import subprocess
    return subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')

def remake_file_paths(maxdepth, old_path):
    temp = old_path.split('/')
    new_path = temp[-1]

    if len(new_path) == 0:
        return ''

    idx = -2
    while abs(idx) < len(temp):
        if maxdepth <= 1:
            break

        new_path = temp[idx] + '/' + new_path

        maxdepth = maxdepth - 1
        idx =  idx - 1

    if maxdepth > 1:
        new_path = '/' + new_path

    #print('new', new_path)
    return new_path


def list_files_to_file(folder, out_file, folder_depth = 1):
    list_command = ['find', folder, '-maxdepth', '1']

    with open(out_file, 'w') as f:
        for each_line in run_cmd_to_buf(list_command):
            if len(each_line) != 0:
                #print(each_line)
                temp = remake_file_paths(folder_depth, each_line)
                if len(temp):
                    f.write("\t\"" + remake_file_paths(folder_depth, each_line) + "\",\n")


def list_files_to_file_in_order(folder, out_file, folder_depth = 1):
    list_command = ['find', folder, '-maxdepth', '1']

    # with open(out_file, 'w') as f:
    #     for each_line in run_cmd_to_buf(list_command):
    #         if len(each_line) != 0:
    #             #print(each_line)
    #             temp = remake_file_paths(folder_depth, each_line)
    #             if len(temp):
    #                 f.write("\t\"" + remake_file_paths(folder_depth, each_line) + "\",\n")

    _dict = dict()
    for each_line in run_cmd_to_buf(list_command):
        
        if len(each_line) != 0:
            #print(each_line)
            temp = remake_file_paths(folder_depth, each_line)
            if len(temp):
                fnumber = temp.replace(".jpg", "").replace(".png", "")
                #print(fnumber)
                _dict[temp] = fnumber
    _dict = sorted(_dict.items(), key=lambda x: x[1], reverse=False)
    #print(_dict)

    with open(out_file, 'w') as f:
        for i in _dict:
            f.write("\t\"" + i[0] + "\",\n")


def list_files_to_file_in_order__jpg2png(folder, out_file, folder_depth = 1):
    list_command = ['find', folder, '-maxdepth', '1']

    # with open(out_file, 'w') as f:
    #     for each_line in run_cmd_to_buf(list_command):
    #         if len(each_line) != 0:
    #             #print(each_line)
    #             temp = remake_file_paths(folder_depth, each_line)
    #             if len(temp):
    #                 f.write("\t\"" + remake_file_paths(folder_depth, each_line) + "\",\n")

    _dict = dict()
    for each_line in run_cmd_to_buf(list_command):
        
        if len(each_line) != 0:
            #print(each_line)
            temp = remake_file_paths(folder_depth, each_line)
            if len(temp):
                fnumber = temp.replace(".jpg", "").replace(".png", "")
                #print(fnumber)
                _dict[temp] = fnumber
    _dict = sorted(_dict.items(), key=lambda x: x[1], reverse=False)
    #print(_dict)

    with open(out_file, 'w') as f:
        for i in _dict:
            f.write("\t\"" + i[0].replace(".jpg", ".png") + "\",\n")

def list_files_to_file_in_order__num(folder, out_file, folder_depth = 1):
    list_command = ['find', folder, '-maxdepth', '1']

    # with open(out_file, 'w') as f:
    #     for each_line in run_cmd_to_buf(list_command):
    #         if len(each_line) != 0:
    #             #print(each_line)
    #             temp = remake_file_paths(folder_depth, each_line)
    #             if len(temp):
    #                 f.write("\t\"" + remake_file_paths(folder_depth, each_line) + "\",\n")

    _dict = dict()
    for each_line in run_cmd_to_buf(list_command):
        
        if len(each_line) != 0:
            #print(each_line)
            temp = remake_file_paths(folder_depth, each_line)
            if len(temp):
                fnumber = temp.replace(".jpg", "").replace(".png", "")
                #print(fnumber)
                _dict[temp] = fnumber
    _dict = sorted(_dict.items(), key=lambda x: x[1], reverse=False)
    #print(_dict)

    with open(out_file, 'w') as f:
        for i in _dict:
            f.write("\t" + i[0].replace(".jpg", "").replace(".png", "") + ",\n")


def the_batch(base):
    folder = base + '0/'
    out_file = base + 'files.txt'
    list_files_to_file_in_order(folder, out_file, 1)
    out_file = base + 'filespng.txt'
    list_files_to_file_in_order__jpg2png(folder, out_file, 1)
    out_file = base + 'filesnum.txt'
    list_files_to_file_in_order__num(folder, out_file, 1)

if 0:
    folder = '/home/pupil'
    out_file = "a33.out.log"
    list_files_to_file(folder, out_file, 2)
else:
    the_batch('/home/pupil/temp/p1/pupil_p1/pupil_data/pieces/func2/data/data_func2a/')
    the_batch('/home/pupil/temp/p1/pupil_p1/pupil_data/pieces/func2/data/data_func2b/')
    the_batch('/home/pupil/temp/p1/pupil_p1/pupil_data/pieces/func2/data/data_func2c/')
