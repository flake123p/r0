import timeit

class MyExc(Exception):
    pass

def my_decorator(func):
    def wrapper(*args, **kwargs):
        try :
            func(*args, **kwargs)
        except Exception as e:
            # raise MyExc from e  # This is OK.
            # raise MyExc() from e  # This is OK.
            raise MyExc('Something wrong inside me') from e
    return wrapper


@my_decorator  # Applying the decorator
def foo():
    print("Hello, World!")
    1/0

foo()
