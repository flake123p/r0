import logging
myLogger = logging.getLogger(__name__)
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)10s() ] %(message)s"
logging.basicConfig(format=FORMAT)
myLogger.setLevel(logging.DEBUG)

myLogger.debug('your message 1') 

myLogger.debug('your message 2') 

def foo():
    myLogger.debug('your message foo()') 

foo()