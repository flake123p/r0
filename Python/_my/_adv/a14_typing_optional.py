'''
typing.Optional
Optional type.

Optional[X] is equivalent to X | None (or Union[X, None]).

Note that this is not the same concept as an optional argument, which is one that has a default. 
An optional argument with a default does not require the Optional qualifier on its type annotation just because it is optional.
'''
from typing import Optional

# def foo(arg: int = 0) -> None:
# or
def foo(arg: Optional[int] = None) -> None:
    print(type(arg))
    print(arg)
    #or
    #print(a) ==> None
    return arg

foo()
foo({'a': 1234})