#
# https://stackoverflow.com/questions/1006289/how-to-find-out-the-number-of-cpus-using-python
#
import os
print('os.cpu_count() =', os.cpu_count()) # python cpu count number


import multiprocessing
print('multiprocessing.cpu_count() =', multiprocessing.cpu_count())


import psutil
threads_count = psutil.cpu_count() / psutil.cpu_count(logical=False)
print('psutil.cpu_count()              =', psutil.cpu_count())
print('psutil.cpu_count(logical=False) =', psutil.cpu_count(logical=False))
print('threads per core                =', threads_count)

