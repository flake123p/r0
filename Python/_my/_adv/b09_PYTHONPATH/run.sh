# In Linux, we can use the following command in the terminal to set the path:

#     export PYTHONPATH=’path/to/directory’

# In the Windows system :

#     SET PYTHONPATH=”path/to/directory”

export PYTHONPATH='../../../'

python b09_PYTHONPATH.py