import timeit

class A():
    def __init__(self):
        self.val = 0

    def parse_deco(func):
        def wrapper(*args, **kwargs):
            self = args[0]
            self.val += 1
            enable = kwargs.get('enable', True)
            print('A.parse_deco() start, enable =', enable)

            func(*args, **kwargs)

            print('A.parse_deco() end')
        return wrapper
    
    @parse_deco
    def parse(self, enable=True):
        print('A.parse()')
    
    @parse_deco
    def parse_group(self, enable=True):
        print('A.parse_group()')



a = A()
a.parse()
a.parse_group(enable=False)
print('a.val =', a.val)