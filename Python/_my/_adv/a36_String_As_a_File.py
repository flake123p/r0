import io

with io.StringIO() as f:
    f.write('abcdef')
    print('gh', file=f)
    f.seek(0)
    print(f.read())

with io.StringIO() as f:
    f.write('abc def\n')
    f.write('123 456\n')
    f.seek(0)
    for l in f:
        print(l.split())
    
    print('type of f:', type(f))

with open('a35_cpu_count.py', 'r') as f:
    print('type of f:', type(f))