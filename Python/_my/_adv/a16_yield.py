'''
    for gaze_pos in gaze_positions:
        gaze_datum = {
            "topic": topic,
            "norm_pos": gaze_pos,
            "confidence": np.mean([p["confidence"] for p in pupil_match]),
            "timestamp": np.mean([p["timestamp"] for p in pupil_match]),
            "base_data": pupil_match,
        }
        yield gaze_datum
'''

# https://ithelp.ithome.com.tw/m/articles/10258195

#
# need a lot of ram or ... when the number is too big
#
print(' ========= demo 1: this will use a lot of RAM ========= ')
powers = [x**2 for x in range(10)]
for x in powers:
    print(x)


def yield_test(n):
    print("start n =", n)
    for i in range(n):
        print("i =", i)
        yield i*i
    print("end")
    return 9999 #no one get this?

print(' ========= demo 2: create yield iterator ========= ')
tests = yield_test(5)
print('type(tests) = ', type(tests))

#
# https://stackoverflow.com/questions/6416538/how-to-check-if-an-object-is-a-generator-object-in-python
#
import types
#types.GeneratorType

#if type(tests) == types.GeneratorType:  # this is old compare
if isinstance(tests, types.GeneratorType):
    print('tests is a generator')
    listOfTests = list(tests)
    print('type(listOfTests) = ', type(listOfTests))
    print('listOfTests = ')
    print(listOfTests)
else:
    print('tests is NOT a generator')

print(' ========= for-loop once yield iterator ========= ')
for test in tests:
    print('test =', test)
    break
print(' ========= for-loop all yield iterator ========= ')
for test in tests:
    print('test =', test)
    pass

#
# https://medium.com/citycoddee/python%E9%80%B2%E9%9A%8E%E6%8A%80%E5%B7%A7-6-%E8%BF%AD%E4%BB%A3%E9%82%A3%E4%BB%B6%E5%B0%8F%E4%BA%8B-%E6%B7%B1%E5%85%A5%E4%BA%86%E8%A7%A3-iteration-iterable-iterator-iter-getitem-next-fac5b4542cf4
#
print(' ========= demo: end of next() =========')
#iterator = iter(iterable)
tests = yield_test(5)
try:
    while True:
        print('get next()')
        item = next(tests)
        #do_stuff(item)
except StopIteration:
    print('except StopIteration')
    pass
finally:
    print('finally')
    #del iterator

#
# yeild from: https://zh-blog.logan.tw/2019/03/30/python3-intro-to-yield-from-expr/
#
print(' ========= demo: yield from (inherit) =========')
def example3():
    for i in range(3):
        yield i
    return 'end'

def example4():
    x = yield from example3()
    print('example4: x:', x)

ctr = 0
for i in example4():
    print('i:', i, ', ctr =', ctr)
    ctr = ctr + 1