
from enum import IntEnum, auto, Enum

print('============ demo 1 ============')
print('============ demo 1 ============')
class InputType(Enum):
    TRACK = 'track'
    ALBUM = 'album'
    TRAP  = 'track'
    RED   = (255,0,0)
    CYAN  = tuple(reversed((0, 255, 255)))
class InputType2(Enum):
    TRACK = 'track'
print(InputType.TRACK)
print(InputType2.TRACK)
print(InputType.TRACK == InputType.TRACK)
print(InputType.TRACK == InputType2.TRACK)
print('trap :', InputType.TRACK == InputType.TRAP)
print('============ demo 1.5 ============')
print('============ demo 1.5 ============')
print('RED =', InputType.RED)  # output:InputType.RED
print('RED.name =', InputType.RED.name)  # output:RED
print('RED.value =', InputType.RED.value)  # output:(255,0,0)
print('RED._value_ =', InputType.RED._value_)  # output:(255,0,0)
print('CYAN =', InputType.CYAN)  # output:InputType.RED
print('CYAN.name =', InputType.CYAN.name)  # output:RED
print('CYAN.value =', InputType.CYAN.value)  # output:(255,0,0)
print('CYAN._value_ =', InputType.CYAN._value_)  # output:(255,0,0)

print('============ demo 2 ============')
print('============ demo 2 ============')
class InputType3(Enum):
    TRACK = auto()
    ALBUM = auto()
    TRAP  = auto()

class InputType4(Enum):
    TRACK = auto()
print(InputType3.TRACK)
print(InputType4.TRACK)
print(InputType3.TRACK == InputType3.TRACK)
print(InputType3.TRACK == InputType4.TRACK)
print('trap :', InputType3.TRACK == InputType3.TRAP)

print('============ demo 3 ============')
print('============ demo 3 ============')
class Mac(IntEnum):
    AIR = 6
    PRO = 7
class Android(IntEnum):
    MARSHMALLOW = 6
    NOUGAT = 7
    TEST = auto()
print(Mac.AIR == Android.MARSHMALLOW)
print(type(Android.MARSHMALLOW._value_))
print(Android.MARSHMALLOW._value_)
print(type(Android.TEST._value_)) # = NOUGAT + 1 = 8
print(Android.TEST._value_) # = NOUGAT + 1 = 8

print('============ demo 4, for_name ============')
print('============ demo 4, for_name ============')
import enum
class DetectorMode(enum.Enum):
    blocking = 'TwoSphereModel'
    asynchronous = 'TwoSphereModelAsync'

    @classmethod
    def from_name(cls, mode_name: str):
        return {mode.name: mode for mode in cls}[mode_name]

    @classmethod
    def all_name(cls):
        print('all_name() dump :')
        for mode in cls:
            print('mode =', mode)
        return {mode.name: mode for mode in cls}

print(DetectorMode.from_name('blocking'))
print(DetectorMode.from_name('asynchronous'))

zz = DetectorMode.all_name()
print(zz)

print('============ demo 5, name & value ============')
print('============ demo 5, name & value ============')
zzz = DetectorMode.asynchronous
print('name =', zzz.name)
print('value =', zzz.value)
