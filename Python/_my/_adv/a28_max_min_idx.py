# https://www.delftstack.com/zh-tw/howto/python/get-index-of-min-and-max-value-from-list-in-python/

list1 = [10, 12, 13, 0, 14, 3]

tmp = max(list1)
index = list1.index(tmp)

print(tmp)
print(index)

tmp = min(list1)
index = list1.index(tmp)

print(tmp)
print(index)