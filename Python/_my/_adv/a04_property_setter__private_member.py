#
# https://www.maxlist.xyz/2019/12/25/python-property/
#
import MyUtils as ut

class Bank_acount:
    print('====== demo 1 ======')
    axx = ...
    print('axx = ', axx, '. This code block was always executed.')

    def __init__(self):
        self.__password = '0000'

    @property
    def password(self):
        print('(Someone get password...)')
        return self.__password

    @password.setter
    def password(self, value):
        print('(Someone set password...)')
        self.__password = value

    @password.deleter
    def password(self):
        del self.__password
        print('(Someone delete password...)')
    
    print('====== demo 2 ======')
    ayy = ...
    print('ayy = ', ayy, '. This code block was always executed.')

print('====== demo 3 ======')
andy = Bank_acount()
# print(andy.__password) #AttributeError: 'Bank_acount' object has no attribute '__password'
ut.try_catch(lambda : print(andy.__password))

print('====== demo 2 ======')
print('print password')
print(andy.password)

print('====== demo 3 ======')
andy.password = '1111'
print('print password')
print(andy.password)

print('====== demo 4 ======')
del andy.password

print('====== demo 5 ======')
print('print password') 
# print(andy.password) #AttributeError: 'Bank_acount' object has no attribute '_Bank_acount__password'
def func():
    print(andy.password)
ut.try_catch(func)



#
# __your_var as private : (two underscores)
#       https://stackoverflow.com/questions/1641219/does-python-have-private-variables-in-classes
#
class AA:
    def __init__(self):
        self.__x = 3                    # You can't access me !!!!!!
        self._y = 4
    
    def _foo(self):
        print('This is _foo()')
    
    def __bar(self):
        print('This is __bar()')        # You can't access __bar !!!!!!
    
    @property
    def bar(self):
        return self.__bar               # You can't access bar with XXX.bar() !!!!!!

aa = AA()
ut.try_catch(lambda : print('aa.__x =', aa.__x))
ut.try_catch(lambda : print('aa._y =', aa._y))

aa._foo()

ut.try_catch(lambda : aa.__bar())

aa.bar()