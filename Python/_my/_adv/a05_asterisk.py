#
# https://www.tutorialspoint.com/What-does-double-star-and-star-do-for-parameters-in-Python
#

# an argument with single asterisk (star) prefixed to it helps in 
# receiving variable number of argument from calling environment
print(' ========= demo 1 ========= ')
def function(*arg):
    print (type(arg)) # <class 'tuple'>
    for i in arg:
      print (i)

function(1,2,7)

# Argument with double asterisks (stars) is used in function definition when variable 
# number of keyword arguments have to be passed to a function
print(' ========= demo 2 ========= ')
def foo(**arg):
    print(type(arg)) # <class 'dict'>
    for i in arg:
        print (i, ' - ', arg[i])
    for key, value in arg.items():
        print("The value of {} is {}".format(key, value))

foo(a=22, b=33, c = 77)

# In Python 3, it is possible to define a variable with asterisk in assignment 
# statement to enable extended unpacking of iterables.
print(' ========= demo 3 ========= ')
a,*b,c=[1,2,3,4]
print('type(a) = ', type(a), 'a = ', a) # 1
print('type(b) = ', type(b), 'b = ', b) # [2, 3]
print('type(c) = ', type(c), 'c = ', c) # 4

print(' ========= demo 4 ========= ')
print('extended unpacking of iterables 111')
x = [3,4]
function(1, x)
print('extended unpacking of iterables 222')
function(1, *x)

#
# Bare asterisk in function arguments?
# https://stackoverflow.com/questions/14301967/bare-asterisk-in-function-arguments
#   https://stackoverflow.com/questions/2965271/forced-naming-of-parameters-in-python/14298976#14298976
#   https://docs.python.org/3/reference/compound_stmts.html#function-definitions
#
# Bare * is used to force the caller to use named arguments - so you cannot define a function 
# with * as an argument when you have no following keyword arguments.
#
print(' ========= demo 5 ========= ')
def foo(pos, *, forcenamed ):
    print(pos, forcenamed)

foo(pos=10, forcenamed=20)
foo(10, forcenamed=20)
# basically you always have to give the value!
#foo(10) #TypeError: foo() missing 1 required keyword-only argument: 'forcenamed'