#
# 'python -O [python_file]' to disable debug/assert mode
#

print('__debug__ =', __debug__)

if __debug__:
    print('in debug mode')

assert 0, "PLEASE run me with 'python -O' !"

print('You should see this line.')