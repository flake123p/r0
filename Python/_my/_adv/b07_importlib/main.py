import sys
import importlib


if __name__ == '__main__':
    folder = 'the_folder'
    func = 'the_func'
    sys.path.append(folder)
    mod = importlib.import_module(func)
    mod.run()
