#
# https://stackoverflow.com/questions/52874575/subprocess-echo-environment-var-outputs-environment-var
#
import os
import subprocess
from subprocess import Popen
from os import environ

env = dict(os.environ)
env['LD_LIBRARY_PATH'] = '/opt/java/jre/lib/i386/:/opt/java/jre/lib/amd64/'
echo_arg = os.environ['LD_LIBRARY_PATH']
args = ['python', 'a34x.py']
#p = Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=env, shell=True).stdout

#p = subprocess.run(args, stdout=subprocess.PIPE, env=env).stdout.decode('utf-8').split('\n')
p = subprocess.run(args, stderr=subprocess.PIPE, env=env).stderr.decode('utf-8').split('\n')

print(p)
