#
# https://ithelp.ithome.com.tw/articles/10218710
#

#
# python lambda:
#   1. one line
#   2. syntax: lambda inputs : outputs
#

print('====== demo 1 : exection x N ======')
def x():
    print('111')
    print('222')
lam = lambda : [x() for i in range(3)]
ret = lam()
print('return of lam is:', ret)

print('====== demo 2 ======')
students = [
['Chris', 'B', 10],
['Amy', 'C', 8],
['Amy2', 'A', 9],
['Amy1', 'C', 99],
['Bob', 'B', 12]
]
student_sort_1st = sorted(students)
student_sort_2nd = sorted(students, key = lambda s : s[1])
student_sort_3rd = sorted(students, key = lambda i : i[2])

print(student_sort_1st)
print(student_sort_2nd)
print(student_sort_3rd)

#
# with for loop & without argument
#
test = lambda : [i for i in range(10)]
print(test())


# Syntax: lambda arguments : expression
#
# take "any number" of arguments, but can "only have one" expression.
#
# https://www.w3schools.com/python/python_lambda.asp

# The power of lambda is better shown when you use them as an anonymous function inside another function.

print("========= lambda POWER =========")
print("========= lambda POWER =========")
def myfunc(n):
    return lambda a : a * n

mydoubler = myfunc(2)
mytripler = myfunc(3)
print(mydoubler(11))
print(mytripler(11))