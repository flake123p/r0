#
# https://www.studytonight.com/python-howtos/elementwise-addition-of-two-lists-in-python
#

#a = [1, 2]
a = [1, 2, 4, 7] 
b = [3, 6, 9] 

print(' ========= demo 1 : sum() =========')
print('sum(a) =', sum(a))

print(' ========= demo 2 : zip() =========')
z = zip(a, b)
print('z =', z)
print('type(z) =', type(z))
for i in z:
    print('type(i) =', type(i), ' i =', i)

print(' ========= demo 3 : zip() & asterisk =========')
def foo(*arg):
    print (type(arg)) # <class 'tuple'>
    for i in arg:
      print (i)
print('print foo(z)')
foo(z)
print('print foo(*z)')
foo(*z)

print(' ========= demo 4 : zip() & asterisk =========')
z = zip(*z)
for i in z:
    print('type(i) =', type(i), ' i =', i)

print(' ========= demo 5 : zip() & sum() =========')
def listSum(a, b):
    result = [sum(i) for i in zip(a, b)]  
    return result

result = listSum(a,b)
print ("Resultant list :", result)