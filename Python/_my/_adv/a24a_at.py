#
# @ Symbol
#
# https://builtin.com/software-engineering-perspectives/python-symbol
#

'''
Common Decorators :
@property
@classmethod
@staticmethod
    ...
'''

print(' ========= demo 1: no decorator =========')

def divide(x, y):
    return x / y

#print(divide(10,0)) # will cause ZeroDivisionError

print(' ========= demo 2: with decorator =========')
def guard_zero(operate):
    def inner(x, y):
        if y == 0:
            print("Cannot divide by 0.")
            return 9999
        return operate(x, y)
    return inner

# usage 1:
#divide = guard_zero(divide)

# usage 2:
@guard_zero
def divide2(x, y):
    return x / y

print(divide2(10,0))

#
# Matrix Multiplication
#
# Since Python 3.5, it’s been possible to use @ to multiply matrices.
#
print(' ========= demo 3: Matrix Multiplication =========')
class Matrix(list):
    def __matmul__(self, B):
        A = self
        return Matrix([[sum(A[i][k] * B[k][j] for k in range(len(B)))
                    for j in range(len(B[0])) ] for i in range(len(A))])
A = Matrix([[1, 2],[3, 4]])
B = Matrix([[5, 6],[7, 8]])
print(A @ B)