use_pyqt6 = False

if use_pyqt6:
    # import PyQt6 as pqt
    from PyQt6.QtWidgets import (QApplication, QWidget,
    QVBoxLayout, QComboBox, QLineEdit, QPushButton, QLabel)
else:
    # import PySide6 as pqt
    from PySide6.QtWidgets import (QApplication, QWidget,
    QVBoxLayout, QComboBox, QLineEdit, QPushButton, QLabel)

# from .test import (
#     foo,
#     abxx,
#     ModXX,
# )

# # from .same_name import *  # might ambiguous

# from . import same_name  # better practice