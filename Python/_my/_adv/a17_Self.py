
from typing_extensions import Self

class A:
    def __init__(self, val = 0):
        self.val = val
    
    def Import(self, another : Self) -> Self:
        self.val = another.val
        return self


a = A(10)
b = A().Import(a)

print(f'b.val = {b.val}')
