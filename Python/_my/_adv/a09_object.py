#
# https://python-course.eu/oop/the-abc-of-abstract-base-classes.php
# 

'''
In Python 3, apart from compatibility between Python 2 and 3, no reason. In Python 2, many reasons.
'''

class ClassicSpam: 
    pass

print(ClassicSpam.__bases__)

class ClassicSpam2(int): 
    pass

print(ClassicSpam2.__bases__)
print(ClassicSpam2.__bases__[0].__bases__)
print(ClassicSpam2.__bases__[0].__bases__[0].__bases__)