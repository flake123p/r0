print('============ demo 1 ============')
print('============ demo 1 ============')
x = '123'
print(x)
print(repr(x))
print(x.__repr__)
print(x.__repr__())


print('============ demo 2 ============')
print('============ demo 2 ============')
class Coo:
    x = 10
x = Coo()
print(x)
print(repr(x))
print(x.__repr__)
print(x.__repr__())

print('============ demo 3 ============')
print('============ demo 3 ============')
class Coo:
    x = 10
x = Coo()
print(x.x)
print(repr(x.x))
print(x.x.__repr__)
print(x.x.__repr__())

print('============ demo 4 ============')
print('============ demo 4 ============')
class Coo:
    x = 10
    def __repr__(self):
        rep = 'Coo, x = ' + str(self.x)
        return rep
x = Coo()
print(x)
print(repr(x))
print(x.__repr__)
print(x.__repr__())