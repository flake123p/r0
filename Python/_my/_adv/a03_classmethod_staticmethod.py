#
# https://stackabuse.com/pythons-classmethod-and-staticmethod-explained/
#

#
# classmethod : like factory
#
# staticmethod : just static, e.g.: like c++ static method
#
class Student(object):
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

    #@classmethod #don't use classmethod, because dump() doesn't return class Student
    def dump(self):
        print('first name = ', self.first_name)
        print('last  name = ', self.last_name)

    @classmethod
    def from_string(cls, name_str):
        first_name, last_name = map(str, name_str.split(' '))
        student = cls(first_name, last_name)
        return student

    @staticmethod
    def is_full_name(name_str):
        names = name_str.split(' ')
        return len(names) > 1

    @classmethod
    def selection_label(cls) -> str:
        return "Screen Marker (Bad Example)"

    def selection_label2() -> str:
        return "Screen Marker2 (Better use staticmethod)"

print('====== demo 1 ======')
scott = Student.from_string('Scott Robinson')
scott.dump()

print('====== demo 2 ======')
ret = scott.is_full_name('AA')
print('ret 1 = ', ret)
ret = Student.is_full_name('AA BB')
print('ret 2 = ', ret)

print('====== demo 3 ======')
wtf = Student.selection_label()
print('wtf =')
print(type(wtf))
print(wtf)
wtf = Student.selection_label2()
print('wtf =')
print(type(wtf))
print(wtf)