#
# https://python-course.eu/oop/the-abc-of-abstract-base-classes.php
# 

from abc import ABC, abstractmethod
 
class AbstractClassExample(ABC):
 
    def __init__(self, value):
        self.value = value
        super().__init__()
    
    @abstractmethod
    def do_something(self):
        pass # or do_something, you can put code here

class DoAdd42(AbstractClassExample):

    def do_something(self):
        return 42 + self.value

x = DoAdd42(4)
print(DoAdd42(1).do_something())