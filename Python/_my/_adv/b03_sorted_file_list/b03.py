#
# https://stackoverflow.com/questions/3207219/how-do-i-list-all-files-of-a-directory
#

from os import listdir
from os.path import isfile, join

from decimal import Decimal

mypath = 'data'
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
print('onlyfiles = ', onlyfiles)

#
# split before sort
#
pre_sort_dict = dict()
for f in onlyfiles:
    spl = f.split('.')
    rspl = f.rsplit('.', 1)
    print(spl, rspl, )
    print(float(rspl[0]))
    pre_sort_dict[str(f)] = float(rspl[0])


#print(pre_sort_dict)

sorted_list = sorted(pre_sort_dict.items(), key=lambda x: x[1], reverse=False)
print(sorted_list)

#
# final version
#
def sorted_float_file_list(mypath):
    from os import listdir
    from os.path import isfile, join
    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    pre_sort_dict = dict()
    for f in onlyfiles:
        rspl = f.rsplit('.', 1)
        pre_sort_dict[str(f)] = float(rspl[0])
    return sorted(pre_sort_dict.items(), key=lambda x: x[1], reverse=False)

print(sorted_float_file_list('data'))