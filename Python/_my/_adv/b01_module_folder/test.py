#
# https://www.tutorialspoint.com/What-does-double-star-and-star-do-for-parameters-in-Python
#


from mod.test import bar

bar()

import mod

mod.foo() # OK
#mod.bar() # NG, AttributeError: module 'mod' has no attribute 'bar'

#abxx() #NG, must use mode.abxx()
from mod import *
abxx()

m = mod.ModXX()


#
# Same funciont name test
#
mod.same_name.foo() # inner same_name foo() OK, good

import same_name
from same_name import *

same_name.foo() # outer same_name foo() OK, good
mod.same_name.foo() # inner same_name foo() OK, good


#
# Ambiguous test
#
foo() # Ambiguous !!!