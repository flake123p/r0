#
# https://www.tutorialspoint.com/What-does-double-star-and-star-do-for-parameters-in-Python
#


def foo():
    print('mod foo')

def bar():
    print('mod bar')

def abxx():
    print('mod abxx')

class ModXX:
    def __init__(self, aa=95):
        self.a = aa
        print('base init', self.a)

    def do(self):
        print('base do')