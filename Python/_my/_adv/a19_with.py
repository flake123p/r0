
import time

class Timer:
    def __init__(self):
        self._times = []

    def __enter__(self, *args, **kwargs):
        self.t0 = time.perf_counter()

    def __exit__(self, *args, **kwargs):
        self._times.append(time.perf_counter() - self.t0)
        del self.t0

    def result(self):
        import pandas as pd

        return pd.Series(self._times).describe()


t = Timer()
for i in range(10):
    with t:
        print('hello world')
print(t.result())