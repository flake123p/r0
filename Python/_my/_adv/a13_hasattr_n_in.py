class Coordinate:
    x = 10
    y = -5
    z = 0

    def foo(self):
        self.no = 999
        print(self.no)
 
point1 = Coordinate() 
print(hasattr(point1, 'x'))
print(hasattr(point1, 'y'))
print(hasattr(point1, 'z'))
print(hasattr(point1, 'no'))  # not exsit

point1.foo()

print(hasattr(point1, 'no'))  # exsit

print('=== dict with hasattr ===')
#
# dict with hasattr ==> bad idea
#
dic = {'age':18, 'len':30, 99:999}
print(hasattr(dic, 'age')) # dont use hasattr, use "in"
print('age' in dic)
print(99 in dic)
print(999 in dic) # use dict.values()
print(999 in dic.values())