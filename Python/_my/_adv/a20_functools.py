import time

class Timer:
    def __init__(self):
        self._times = []

    def __enter__(self, *args, **kwargs):
        self.t0 = time.perf_counter()

    def __exit__(self, *args, **kwargs):
        self._times.append(time.perf_counter() - self.t0)
        del self.t0

    def result(self):
        import pandas as pd

        return pd.Series(self._times).describe()


def foo(x):
    def go(x):
        x2 = x * x
        x3 = x2 * x
        temp = 0.014*x3 + 0.9*x2 + 0.5*x + 0.34
        temp = temp / x3
        temp = temp * x2
        temp = temp / x
        return temp
    for i in range(100000):
        x = go(x)
    return x

t = Timer()
for i in range(5):
    with t:
        print(foo(9527))
print(t.result())

import functools
@functools.lru_cache
def bar(x):
    def go(x):
        x2 = x * x
        x3 = x2 * x
        temp = 0.014*x3 + 0.9*x2 + 0.5*x + 0.34
        temp = temp / (x3+1)
        temp = temp * x2
        temp = temp / (x+1)
        return temp
    for i in range(100000):
        x = go(x)
    return x

t = Timer()
for i in range(5):
    with t:
        #print(bar(i+9527)) #can't speed up
        print(bar(9527))
print(t.result())