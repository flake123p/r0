import numpy as np

corr = [
    {'Name': 'Amy', 'Age': [7,2], 'Grade': 98},
    {'Name': 'Bob', 'Age': [5,5], 'Grade': 49},
    {'Name': 'Chris', 'Age': [6,8], 'Grade': 90},
]

lo_not_unpacking = np.array(
            [(e["Age"], e["Grade"]) for e in corr]
        )

# enable extended unpacking of iterables. Notice the asterisk symbol!!!!!!!!!!!11
lo_do_unpacking = np.array(
            [(*e["Age"], e["Grade"]) for e in corr]
        )

print('Not unpacking')
print(lo_not_unpacking)
print('Do unpacking')
print(lo_do_unpacking)