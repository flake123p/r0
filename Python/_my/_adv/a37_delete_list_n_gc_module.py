#
# https://stackoverflow.com/questions/14465279/delete-all-objects-in-a-list
#

a = 1
b = 2

l = [a, b]

print('l:', l)

del l[:]
print('l:', l)

l.clear()
print('l:', l)

print('a:', a) # not working
print('b:', b) # not working