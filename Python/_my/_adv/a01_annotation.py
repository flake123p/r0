#
# https://stackoverflow.com/questions/14379753/what-does-mean-in-python-function-definitions
# 


#
# parameter annotations
#
def f(x) -> int:
    return int(x)

x = 1.3
y = f(x)
print('====== demo 1 ======')
print(y)
print(type(y))


#
# have a python data structure
#
rd = {
    'type':float,
    'units':'Joules',
    'docstring':'Given mass and velocity returns kinetic energy in Joules'
}
def f2()->rd:
    pass

print('====== demo 2 ======')
print(f2.__annotations__['return']['type'])
print(f2.__annotations__['return']['docstring'])

#
# combination
#
rd3 = {
    'type':int,
    'docstring':'combination xx'}
def f3(x) -> rd3:
    return int(x)

x = 1.3
y = f3(x)
print('====== demo 3 ======')
print(y)
print(type(y))
print(f3.__annotations__['return']['type'])
print(f3.__annotations__['return']['docstring'])