
def my_decorator(func):
    def wrapper(*args, **kwargs):
        print("Before the function runs")
        func(*args, **kwargs)
        print("After the function runs")
    return wrapper

@my_decorator  # Applying the decorator
def say_hello(s):
    print("Hello, World!", s)

say_hello('John')


def my_hijact(func):
    def wrapper():
        print("Before the function runs")
        print("After the function runs")
    return wrapper

@my_hijact  # Applying the decorator
def say_hello2():
    print("Hello, World 2!")

say_hello2()

#
#
#
def repeat(n):
    def decorator(func):
        def wrapper(*args, **kwargs):
            for _ in range(n):
                func(*args, **kwargs)  # Forward all arguments
        return wrapper
    return decorator

@repeat(2)
def add(a, b=10):
    print(a + b)

add(5, b=20)