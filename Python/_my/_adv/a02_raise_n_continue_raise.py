#
# https://docs.python.org/3/tutorial/errors.html
#

#
# https://www.geeksforgeeks.org/try-except-else-and-finally-in-python/
#
'''
try:
       # Some Code.... 
except:
       # optional block
       # Handling of exception (if required)
else:
       # execute if no exception
finally:
      # Some code .....(always executed)
'''

class ValueError(BaseException):
    pass

class CorrelationError(ValueError):
    pass

def bar(b):
    if b > 10:
        raise KeyError("b is too big")

def foo(a, b, continue_raise):
    if a > 10:
        raise CorrelationError("a is too big")

    try:
        bar(b)
        return b
    except KeyError as err:
        if continue_raise:
            print('aaa err = ', err)
            print('continue_raise = ', continue_raise)
            raise
        else:
            print('bbb err = ', err)
            print('continue_raise = ', continue_raise)

def main(a, b, continue_raise):
    try:
        ret = foo(a, b, continue_raise)
        print('111 ret = ', ret)
    except KeyError as err:
        print('222 err = ', err)
    else:
        print('333 ret = ', ret)
    finally:
        print('444 finally')

def main_w_CorrelationError(a, b, continue_raise):
    try:
        ret = foo(a, b, continue_raise)
        print('AAA ret = ', ret)
    except KeyError as err:
        print('BBB err = ', err)
    except CorrelationError as err:
        print('CCC err = ', err)
    else:
        print('DDD ret = ', ret)
    finally:
        print('EEE finally')

print('====== demo 1 ======')
main(6, 8, 1)
'''
====== demo 1 ======
111 ret =  8
333 ret =  8
444 finally
'''

print('====== demo 2 ======')
#
# This demo will stop all demonstrations
#
# main(16, 18, 1)
'''
====== demo 2 ======
444 finally
Traceback (most recent call last):
  File "/home/user0/ws/r0/Python/_my/_adv/a02_raise.py", line 68, in <module>
    main(16, 18, 1)
  File "/home/user0/ws/r0/Python/_my/_adv/a02_raise.py", line 33, in main
    ret = foo(a, b, continue_raise)
  File "/home/user0/ws/r0/Python/_my/_adv/a02_raise.py", line 17, in foo
    raise CorrelationError("a is too big")
__main__.CorrelationError: a is too big
'''
#
# This is ok
#
main_w_CorrelationError(16, 18, 1)

print('====== demo 3 ======')
main(6, 18, 0)

print('====== demo 4 ======')
main(6, 18, 1)