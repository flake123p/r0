import torch
from py754 import *

t = torch.tensor([1.3], dtype=torch.bfloat16)

wh = Py754_Torch(t)

print(t)
print(t.dtype)
print(t[0].item(), wh.str(t[0]), float.hex(t[0].item()))


t = torch.tensor([200.7], dtype=torch.bfloat16)
print(t[0].item(), wh.str(t[0]), float.hex(t[0].item()))

t = torch.tensor([9.18e-41], dtype=torch.bfloat16)
print(t[0].item(), wh.str(t[0]), float.hex(t[0].item()))


