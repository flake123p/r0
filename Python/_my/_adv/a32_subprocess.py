#
# https://stackoverflow.com/questions/4760215/running-shell-command-and-capturing-the-output
#
def run_cmd_to_buf(cmd):
    import subprocess
    return subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')

import subprocess
result = subprocess.run(['ls', '-l'], stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')

for each_line in result:
    seach_line = each_line.split()
    print(seach_line)
    for each_word in seach_line:
        #print(each_word)
        pass

for each_line in run_cmd_to_buf(['ls']):
    seach_line = each_line.split()
    print(seach_line)
    for each_word in seach_line:
        #print(each_word)
        pass


'''
subprocess + -exec + grep

https://stackoverflow.com/questions/42415042/issue-running-find-command-in-python-with-subprocess



His example:

proc = subprocess.Popen(['/usr/bin/find', '/path/to/dir', '-type', 'f',
                         '-name', '*.gradle', '-exec', 'grep', 'KEYWORD',
                         '{}', '/dev/null', ';'],
                         stdout=PIPE, stderr=PIPE)



My example:

self.cmd = ["find", "LOC", "-type", "f", "-exec", "grep", "KEY", '{}', '/dev/null', ';']


When Find Nothing:
            print(len(cmd.ret))
            print(cmd.ret)
Output:
            1
            ['']
'''