#
# https://docs.python.org/3/library/csv.html
#

import csv

with open('eggs.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        #print(row['first_name'], row['last_name'])
        print(row)