#
# https://stackoverflow.com/questions/772124/what-does-the-ellipsis-object-do
# 

import numpy as np

#
# section 1
#
class foo():
    ... # replace "pass"

x = foo()
print(x)
print(type(x))

#
# section 2
#
a = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
print(a[...])   # all
print(a)        # all

#
# section 3
#
x = ...
print(x)
print(type(x))

if x is ...:
    print('x is ...')


import numpy as np
a = np.array([1, 2, 3, 4])
a[...] = a * 2
print(a[...])