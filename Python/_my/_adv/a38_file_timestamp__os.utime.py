#
# https://www.geeksforgeeks.org/python-os-utime-method/
#

import os
 
# Path
path = 'a38_file_timestamp__os.utime.py'
 
# Print current access and modification time
print("Current access time:", os.stat(path).st_atime)
print("Current modification time:", os.stat(path).st_mtime)
 
# Access time in seconds
atime = 200000000
 
# Modification time in seconds
mtime = 100000000
 
tup = (atime, mtime)
os.utime(path, tup)
 
print("\nAccess and modification time changed\n")
 
# Print current access and modification time
print("Current access time:", os.stat(path).st_atime)
print("Current modification time:", os.stat(path).st_mtime)
