
#
# anydbm => keys and values have to be strings!! Use pickle
#
import pickle
t1 = [11, 22, 33]

#
# dumps = dump string
#
s = pickle.dumps(t1)
print('pickle.dumps(t1) = ', s)

t2 = pickle.loads(s)
print('pickle.loads(s) = ', t2)

print('t1 == t2 = ', t1 == t2)
print('t1 is t2 = ', t1 is t2)

#
# dump to file https://pythonguides.com/python-write-variable-to-file/
#
student = {"studentname" : "john","student age" : 14,}

file = open('student.p', 'wb')
pickle.dump(student, file)
file.close()

file = open('student.p', 'rb')
student2 = pickle.load(file)
file.close()

print(student)
print(student2)