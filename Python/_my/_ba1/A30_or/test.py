#!/usr/bin/python

def foo(x=None):
	out = x or 123
	print(out)

foo()
foo(456)

def bar(x=None,y=None):
	out = x or y or 789
	print(out)

bar()
bar(y='000')

score_a = 40
score_b = 49
score_c = 61
if score_a >= 60 or score_b >= 60 or score_c >= 60:
    print('at least 1 passed')