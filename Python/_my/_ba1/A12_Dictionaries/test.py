
"""
Dictionary:
	1.key-value pair or item
"""
print('========= add new k/v to dict =========')
print('========= add new k/v to dict =========')
x = dict()
x['a'] = 10
x['a'] = 20
x['b'] = 100
x.update({'d':'ruhello'})
print('x = ', x)
print('d in x = ', 'd'in x)

print('')
print('========= del item =========')
print('========= del item =========')
del x['d']
print('d in x = ', 'd'in x)

print('')
print('========= test if key in dict =========')
print('========= test if key in dict =========')
print('\'c\' in x = ', 'c' in x)
print('\'a\' in x = ', 'a' in x)
print('100 in x = ', 100 in x) # you can test value using "in"

print('')
print('========= returns the values as a list =========')
print('========= returns the values as a list =========')
vals = x.values()
print('vals = ', vals)
print('dict = ', x)


print('')
print('========= for loop a dict (items) =========')
print('========= for loop a dict (items) =========')
for keys,values in x.items():		#items()
    print('k =', keys, ' v =', values)


#
#A previously computed value that is stored for later use is called a "memo"!!!!!
#
print('')
print('========= Init_Dict_With_Mutiple_Pair =========')
print('========= Init_Dict_With_Mutiple_Pair =========')
Init_Dict_With_Mutiple_Pair = {0:44, 1:33}
print('Init_Dict_With_Mutiple_Pair = ', Init_Dict_With_Mutiple_Pair)


print('')
print('========= update dict =========')
print('========= update dict =========')
tinydict = {'Name': 'Zara', 'Age': 7}
tinydict2 = {'Sex': 'female' }

tinydict.update(tinydict2)
print ("Value : %s" %  tinydict)

tinydict2 = {'Sex': 'mid' }
tinydict.update(tinydict2)
print ("Value : %s" %  tinydict)

print('')
print('========= update tuple in dict =========')
print('========= update tuple in dict =========')
x = {'norm': (0.5,0.6)}
print(x)
#x['norm'][0] = 0 #TypeError: 'tuple' object does not support item assignment
x['norm'] = (0.3,0.7)
print(x, '\t ok to asign new value')
x.update({'norm':(0.1,0.9)})
print(x, '\t ok to asign new value')

print('')
print('========= is type dict =========')
print('========= is type dict =========')
print(type(tinydict))
print(type(tinydict)=='dict')
print(type(tinydict)=='<class \'dict\'')
print(type(tinydict)==dict, '\t correct')       #OK
print(type(tinydict)==type(dict))
print(type(tinydict) is dict, '\t correct')     #OK
print(type(tinydict) is type(dict))