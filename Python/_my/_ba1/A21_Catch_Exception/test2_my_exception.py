
import sys

class MyException(Exception):
    pass


print('\nTest 1:')
try:
    raise MyException('Test Failed')
except:
    print("Unexpected error:", sys.exc_info()[0])
    # raise # This will stop demo


print('\nTest 2:')
try:
    raise MyException('Test Failed')
except MyException as err:
	print(f"MyException: {err}")

#
# More: https://stackoverflow.com/questions/1319615/proper-way-to-declare-custom-exceptions-in-modern-python
#