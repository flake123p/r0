
#
# From another folder : https://stackoverflow.com/questions/4383571/importing-files-from-different-folder
#
import sys
sys.path.append('/path/to/application/app/folder')


#
# Basic
#
import math

print('>>> import math')
print('math        = ', math)
print('math.pi     = ', math.pi)
print('math.sin(1) = ', math.sin(1))

from math import pi
print('>>> from math import pi')
print('pi = ', pi, ' (Don\'t need to use \'math.pi\')')

from math import *
print('>>> from math import *')
print('sin(1) = ', sin(1), ' (Don\'t need to use \'math.sin(1)\')')
