
fin = open('words.txt')
print('fin = ', fin)

print('fin.readline() = ', fin.readline())
print('fin.readline() = ', fin.readline())

# for each line in file
print('for each_line in fin:')
for each_line in fin:
	print('each_line.strip() = ', each_line.strip(), '  (Remove characters: \\r\\n)')
	#each word
	each_word_list = each_line.split()
	print('each_word_list = ', each_word_list)
	#concat eachword
	concat_string = '-'
	concat_string = concat_string.join(each_word_list)
	print('concat_string = ', concat_string)

#
# Flake: Use this please
#
import sys
original_stdout = sys.stdout
with open('filename.txt', 'w') as f:
    sys.stdout = f # Change the standard output to the file we created.
    print('This message will be written to a file.')
    sys.stdout = original_stdout # Reset the standard output to its original value

#
# Flake: Don't use open directly
#
fout = open('write.txt', 'w')
fout.write('XXX\n')
fout.write('YYY')

ctr = 13
print('ctr = 0x{:08x}'.format(ctr))
print('ctr = {:08d}'.format(ctr))