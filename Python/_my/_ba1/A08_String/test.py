
myStr = 'Texi'
print('myStr     = ', myStr)
print('myStr[1]  = ', myStr[1])
print('myStr[-1] = ', myStr[-1])
print('myStr[-2] = ', myStr[-2])
print('myStr[1:3] = ', myStr[1:3])
print('myStr[:3]  = ', myStr[:3])
print('myStr[1:]  = ', myStr[1:])
print('len(myStr) = ', len(myStr))

print('for myChar in myStr:')
for myChar in myStr:
	print(myChar)

# Strings are immutable
# myStr[0] = 'X'   <= ILLEGAL!!
myStr2 = 'xx' + myStr[:3]
print('myStr2         = ', myStr2)
print('myStr2.upper() = ', myStr2.upper())
print('myStr2.lower() = ', myStr2.lower())
print('myStr2.find(\'x\') = ', myStr2.find('x'))
print('myStr2.find(\'x\', 1) = ', myStr2.find('x', 1))
print('myStr2.find(\'x\', 2, 4) = ', myStr2.find('x', 2, 4), '(Not include 4, so not found!)')
print('myStr2.find(\'x\', 2, 5) = ', myStr2.find('x', 2, 5))

if 'a' in myStr2:
	print('\'a\' in myStr2: True')
else:
	print('\'a\' in myStr2: False')

if 'amy' > 'bob':
	print('amy > bob')
else:
	print('amy < bob')

if 'amy' > 'Bob':
	print('amy > Bob')
else:
	print('amy < Bob')

#string compare
if 'amy' == 'amy':
	print('amy == amy')

print('========= int to padding str =========')
print('========= int to padding str =========')
# https://stackoverflow.com/questions/339007/how-do-i-pad-a-string-with-zeroes
n = 4
print(f'{n:03}') # Preferred method, python >= 3.6
print('%03d' % n)
print(format(n, '03')) # python >= 2.6
print('{0:03d}'.format(n))  # python >= 2.6 + python 3
print('{foo:03d}'.format(foo=n))  # python >= 2.6 + python 3
print('{:03d}'.format(n))  # python >= 2.7 + python3

print('========= float to padding str =========')
print('========= float to padding str =========')
abc = 3.1415926
print(f'ex1 : {abc:03f}')
print(f'ex2 : {abc:03.3f}')
print(f'ex3 : {abc:010.3f}')
s = f'ex4 : {abc:010.10f}'
print(s)

print('0.11 = {:.08f}'.format(0.11))
print('0.11 = {:.9f}'.format(0.11))
print('0.11 = {:.10f}'.format(0.11))

ret = 0
s = "cudn"
if len(s) >= 5 and s[0:5] == "cudnn":
    ret = 1
print("s & ret = ", s, ret)

s = "cudnoo"
if len(s) >= 5 and s[0:5] == "cudnn":
    ret = 1
print("s & ret = ", s, ret)

s = "cudnn"
if len(s) >= 5 and s[0:5] == "cudnn":
    ret = 1
print("s & ret = ", s, ret)

s = "cudnnxx"
if len(s) >= 5 and s[0:5] == "cudnn":
    ret = 1
print("s & ret = ", s, ret)

myStr = "abcxx"
print('{0:<20}'.format(myStr), '{0:<20}'.format(myStr))
print('{0:>20}'.format(myStr), '{0:>20}'.format(myStr))

myStr = "abcxx=123"
print('Replace DEMO:')
print('before:', myStr)
print('after:', myStr.replace("=", " : "))

myStr = "make[222]"
print('find make in myStr =', myStr.find('make'))
print('find xxxx in myStr =', myStr.find('xxxx')) # -1 = can't find

x = 15
print('x =', x)
print('hex(x) =', hex(x))
print('0x{:02x}'.format(x))  # python >= 2.7 + python3