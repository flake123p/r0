#
# Officail Document: https://docs.python.org/3/tutorial/inputoutput.html
#


#
# String
#
s = 'Hello, world.'
str(s)
x = 1/7
ps = ' str(s)=' + str(s) + \
   '\n repr(s)=' + repr(s) + \
   '\n repr(x)=' + repr(x)
print(ps)

#
# Formated
#
print('\n...Please Use Python 3.10 ...\n')
import math
print(f'The value of pi is approximately {math.pi:.3f}.')
print(f'The value of pi is approximately {math.pi:.6f}.')
s = 'abc'
x = 1.2233
print(f's = {s:4s}, x = {x:10.3f}')
print(f's = {s:6}, x = {x:6f}')

for x in range(1, 6):
    print('{0:2d} {1:3d} {2:4d}'.format(x, x*x, x*x*x))

for x in range(1, 6):
    print('{0:-2d} {1:-3d} {2:-4d}'.format(x, x*x, x*x*x))
    
for x in range(1, 6):
    print('{0:-2d} {1:-3d} {2:-4d}'.format(x, x*x, x*x*x)) #?? what the

#
# Old Formated!!!!!, https://www.geeksforgeeks.org/python-output-formatting/
#
print('\n...Old Formated Print...\n')
for x in range(1, 6):
    print('%6d %6d %6d' % (x, x*x, x*x*x))

for x in range(1, 6):
    print('%-6d %-6d %-6d' % (x, x*x, x*x*x))


print(" ====== print hex ======")
a = 0xF7
print(a)
# output: 247
print(hex(a))
# output: 0xf7
print("0x%x" % a)
# output: 0xf7
print("0x%X" % a)
# output: 0xF7
print("0x{:x}".format(a))
# output: 0xf7
print("0x{:X}".format(a))
# output: 0xF7
print(f"0x{a:x}")
# output: 0xf7
print(f"0x{a:X}")
# output: 0xF7

print(" ====== with leading zeroes ======")
x = 45678
print(f'{x:09d}')
x = 45678.00456000
s = f'{x:.9f}'
print(s)

#
# print('{' + ', '.join([str(x) for x in the_list]) + '},')
#
# print('{' + ', '.join([f'{x:.16f}' for x in the_list]) + '},')
#
# print('    {' + ', '.join([f'{x:22.16f}' for x in the_list]) + '},')
#

print(" ====== redirect method 1 ======")
with open('00.z.log', 'w') as f:
    print('abxx', file=f)

print(" ====== redirect method 2 (Better) ======") # Better, Better, Better !!! !!! !!!
# https://stackoverflow.com/questions/7152762/how-to-redirect-print-output-to-a-file
import sys
orig_stdout = sys.stdout
f = open('00.z2.log', 'w')
sys.stdout = f
print('abcc')
sys.stdout = orig_stdout
f.close()

print(" ====== redirect method 3 (Better with class) ======")
class PR_Redirect():
    def __init__(self, file_name : str = None):
        import sys
        self.dummy = False
        if file_name == None or file_name == 'verbose':
            self.dummy = True

        if not self.dummy:
            self.orig_stdout = sys.stdout
            self.f = open(file_name, 'w')
            sys.stdout = self.f
    
    def __del__(self):
        import sys
        if not self.dummy:
            sys.stdout = self.orig_stdout
            self.f.close()
        print("destructor of redirect...")

re = PR_Redirect('00.z3.log')
print('ab33')
del re          # call manually

re = PR_Redirect('verbose')
print('verbose, dummy')
del re          # call manually
print('end')