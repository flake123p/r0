print("\n ====== DEMO 0: int() ======")
x = int(0.1)
print(x, type(x))
# output: 0 <class 'int'>

x = int(0.5)
print(x, type(x))
# output: 0 <class 'int'>

x = int(0.5001)
print(x, type(x))
# output: 0 <class 'int'>
    
x = int(5.76543)
print(x, type(x))
# output: 5 <class 'int'>

print("\n ====== DEMO 1: round() ======")
x = round(0.1)
print(x, type(x))
# output: 0 <class 'int'>

x = round(0.5)
print(x, type(x))
# output: 0 <class 'int'>

x = round(0.5001)
print(x, type(x))
# output: 1 <class 'int'>
    
x = round(5.76543, 2)
print(x, type(x))
# output: 5.77 <class 'float'>
    
x = round(5.76443, 2)
print(x, type(x))
# output: 5.76 <class 'float'>

print("\n ====== DEMO 2: ceil() ======")
import math
x = math.ceil(0.1)
print(x, type(x))
# output: 1 <class 'int'>

x = math.ceil(0.5)
print(x, type(x))
# output: 1 <class 'int'>

x = math.ceil(0.5001)
print(x, type(x))
# output: 1 <class 'int'>
    
x = math.ceil(5.76543)
print(x, type(x))
# output: 6 <class 'int'>
    
x = math.ceil(5.76443)
print(x, type(x))
# output: 6 <class 'int'>

print("\n ====== DEMO 3: floor() ======")
import math
x = math.floor(0.1)
print(x, type(x))
# output: 0 <class 'int'>

x = math.floor(0.5)
print(x, type(x))
# output: 0 <class 'int'>

x = math.floor(0.5001)
print(x, type(x))
# output: 0 <class 'int'>
    
x = math.floor(5.76543)
print(x, type(x))
# output: 5 <class 'int'>
    
x = math.floor(5.76443)
print(x, type(x))
# output: 5 <class 'int'>
