
import time

start = time.time_ns()
print("hello")
time.sleep(0.001)
end = time.time_ns()

print(end - start, 'nano sec of elapse time')
print((end - start) / 1000000, 'milli sec of elapse time')


start = time.time()
print("hello")
time.sleep(1)
end = time.time()

print(end - start, 'sec of elapse time')
