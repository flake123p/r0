import MyUtils as ut
from ut import SHOW

print('====== Demo 1 ======')
py_string = 'Hello'

c_string1 = bytes(py_string, 'utf-8') # bytes = immutable
c_string2 = py_string.encode('utf-8')
c_string3 = bytearray(py_string, 'utf-8') # bytearray = mutable

SHOW(py_string)
SHOW(c_string1)
SHOW(c_string2)
SHOW(c_string3)
print('bytes(c_string3) =', bytes(c_string3))

py_from_c = c_string1.decode() # "utf-8" is default
SHOW(py_from_c)

py_from_c = c_string1.decode("utf-8") # "utf-8" is default
SHOW(py_from_c)

print('====== Demo 2 ======')
mutable_ba = bytearray('ABC', 'utf-8') # bytearray = mutable
SHOW(mutable_ba) # ABC
mutable_ba.pop()
SHOW(mutable_ba) # AB
mutable_ba.pop(0)
SHOW(mutable_ba) # B
mutable_ba.extend(bytearray('123', 'utf-8'))
SHOW(mutable_ba) # B123
mutable_ba.extend(map(ord, '456'))
SHOW(mutable_ba) # B456