class Base:
    def __init__(self, aa=95):
        self.a = aa
        print('base init', self.a)

    def do(self):
        print('base do')


class Derived(Base):
    def __init__(self):
        super().__init__(98)
        print('Derived init', self.a)

    def do(self):
        print('Derived do')
        super().do()


a = Derived()
a.do()
