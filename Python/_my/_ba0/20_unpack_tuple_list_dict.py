
print("\n  Unpack Tuple:")
x = (1, '13')
print(x)
a, b = x
print(a, b)

print("\n  Unpack Tuple 2:")
x = (1, '13')
print(x)
a, b = x[0], x[1]
print(a, b)

print("\n  Unpack List:")
x = [1, '13']
print(x)
a, b = x
print(a, b)

print("\n  Unpack List 2:")
x = [1, '13']
print(x)
a, b = x[0], x[1]
print(a, b)

print("\n  Unpack Dict:")
x = {1, '13'}
print(x)
a, b = x
print(a, b)
