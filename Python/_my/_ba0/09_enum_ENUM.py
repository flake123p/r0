#
# https://docs.python.org/zh-tw/3.10/library/enum.html
#
from enum import Enum
class Color(Enum):
    RED = 1
    GREEN = 2
    BLUE = 3

print(Color.RED)

print(repr(Color.RED))

print('type:', type(Color.RED))
print('type:', type(Color))

print('isinstance:', isinstance(Color.GREEN, Color))

print('Color.RED.name:', Color.RED.name)
print('type(Color.RED.name):', type(Color.RED.name))

for c in Color:
    print(c)