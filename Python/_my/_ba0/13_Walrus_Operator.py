
print('====== Demo 1 ======')
x = 3
if x := 4 + x:
    print(x)

print('====== Demo 2 ======')
x = -4
if x := 4 + x:
    print('AAA', x)
else:
    print('BBB', x)

print('====== Demo 3 ======')
x = -4

y = x + 2 if x == -4 else x + 1
print('y =', y)