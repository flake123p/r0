names = [
    'FLAKE',
    'AMY',
    'BOB',
]

# ValueError: 'flaKE' is not in list
test = 'flaKE'
#print(names.index(test))

# fixed verfsion
print('is ', test, ' in names ? Ans = ', test in names)
print('is ', test.upper(), ' in names ? Ans = ', test.upper() in names)

test = 'flaKE'
print(names.index(test.upper())) # ouput index: 0