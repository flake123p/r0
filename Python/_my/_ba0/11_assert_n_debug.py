#
# assert <condition>, Optional[<message string>]
#

print('__debug__ =', __debug__)
print('__debug__ =', __debug__)
print('__debug__ =', __debug__)

if __debug__:
    print('my debug code ...')
else:
    print('my non debug code ...')

'''
Disable debug:    DISABLE assert !!!

  python -O 11_assert_n_debug.py

'-O' : char O, not int 0

'''

assert 0 == 0

assert 0 == 1

assert 0 == 1, 'haha'

'''
Show message:

Traceback (most recent call last):
  File "/home/user0/ws/r0/Python/_my/_ba0/11_assert.py", line 9, in <module>
    assert 0 == 1, 'haha'
AssertionError: haha
'''

print('If you see this, it means you disable the assert/debug with python -O (Capital O)')