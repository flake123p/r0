


def pickle_to(var, file):
    import pickle
    with open(file, 'wb') as my_file:
        pickle.dump(var, my_file)

def pickle_from(file):
    import pickle
    with open(file, 'rb') as my_file:
        ret = pickle.load(my_file)
    return ret

def pickle_from(file, object):
    import pickle
    import sys
    try:
        with open(file, 'rb') as my_file:
            object = pickle.load(my_file)
    except IOError as exc:    # Python 2. For Python 3 use OSError
        tb = sys.exc_info()[-1]
        lineno = tb.tb_lineno
        filename = tb.tb_frame.f_code.co_filename
        print('{} at {} line {}.'.format(exc.strerror, filename, lineno))
        # sys.exit(exc.errno)
        return False
    return True

t1 = [11, 22, 33]
pickle_to(t1, 'pickle.out')

t2 = pickle_from('pickle.out')

print('t1 =', t1)
print('t2 =', t2)