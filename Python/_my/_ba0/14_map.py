def square(x) :
    return x ** 2

print(list(map(square, [1,2,3,4,5])))
# [1, 4, 9, 16, 25]

print(list(map(lambda x: x ** 2, [1, 2, 3, 4, 5])))
# [1, 4, 9, 16, 25]

print('ASCII of \'a\' =', ord('a'))
# ASCII of 'a' = 97

x = map(ord, 'abc')
print('ASCII of \'abc\' =', list(x))
# ASCII of 'abc' = [97, 98, 99]