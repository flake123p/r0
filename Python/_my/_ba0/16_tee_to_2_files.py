
print(123)
print(2/0)

'''
Command:

    $ python 16_tee_to_2_files.py 1> >(tee -a log.1.log) 2> >(tee -a log.2.log >&2)

Ref:
https://stackoverflow.com/questions/692000/how-do-i-write-standard-error-to-a-file-while-using-tee-with-a-pipe
https://stackoverflow.com/questions/363223/how-do-i-get-both-stdout-and-stderr-to-go-to-the-terminal-and-a-log-file
'''