#
# https://stackoverflow.com/questions/3056048/filename-and-line-number-of-python-script
#
from inspect import currentframe, getframeinfo
import inspect

def prloc_wrong_ver():
    import inspect
    print(inspect.currentframe().f_code.co_name + "(), Line :", inspect.getframeinfo(currentframe()).lineno)
    return

import inspect
from inspect import currentframe, getframeinfo
def prloc(func, line):
    print("[---PRLOC---] " + func + "(), Line :", line)
    return
# import inspect
# def prloc(func = inspect.currentframe().f_code.co_name, line = inspect.getframeinfo(currentframe()).lineno):
#     print("[---PRLOC---] " + func + "(), Line :", line)
#     return

frameinfo = getframeinfo(currentframe())

print("File Name:", frameinfo.filename)
print("Line     :", frameinfo.lineno)
print("inspect.stack()[0][3] :", inspect.stack()[0][3])
# print(inspect.stack()[1][3])
print("inspect.currentframe().f_code.co_name :", inspect.currentframe().f_code.co_name)

def abc():
    print("abc() File Name:", frameinfo.filename)
    print("abc() Line     :", frameinfo.lineno)
    print("abc() Line     :", inspect.getframeinfo(currentframe()).lineno)
    print("inspect.stack()[0][3] :", inspect.stack()[0][3])
    print("inspect.stack()[1][3] :", inspect.stack()[1][3])
    print("inspect.currentframe().f_code.co_name :", inspect.currentframe().f_code.co_name)
    prloc(currentframe().f_code.co_name, getframeinfo(currentframe()).lineno)
    prloc(currentframe().f_code.co_name, getframeinfo(currentframe()).lineno)
    prloc(currentframe().f_code.co_name, getframeinfo(currentframe()).lineno)

abc()
prloc(inspect.currentframe().f_code.co_name, inspect.getframeinfo(currentframe()).lineno)
prloc(inspect.currentframe().f_code.co_name, inspect.getframeinfo(currentframe()).lineno)
prloc(inspect.currentframe().f_code.co_name, inspect.getframeinfo(currentframe()).lineno)
