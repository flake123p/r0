a = dict()
a["name"] = "jack"
a["height"] = 170
a["grade"] = [59., 59, 59]

b = dict()
b["name"] = "ed"
b["height"] = 180
b["grade"] = [90, 98, 90, 90]

if 0:
    tt = dict()
    tt["aaa"] = a
    tt["bbb"] = b
else:
    tt = [a, b]

print(tt)

#
# dump json with indent : https://stackoverflow.com/questions/17055117/python-json-dump-append-to-txt-with-each-variable-on-new-line
#
if 1:
    # print('123')
    # import pickle
    # print('456')
    # md = pickle.load(open('match_data_00.pkl', 'rb'))
    # import sys
    # print('789')
    # print(md)
    import json
    f = open('05.json', "w")
    json.dump(tt, f, indent=2)
    f.close()

    #
    # To stdio
    #
    import sys
    json.dump(tt, sys.stdout, indent=2)

#
# read from json
#
if 1:
    import json
    f = open('05.json')
    rd = json.load(f)
    f.close()
    print(rd)
    print('type(rd) =', type(rd))
    grade = rd[0]['grade']
    print(grade)
    print('type(grade) =', type(grade))
    print('type(grade[0]) =', type(grade[0]))
    print('type(grade[1]) =', type(grade[1]))
    print('type(grade[2]) =', type(grade[2]))