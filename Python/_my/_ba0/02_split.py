#
# https://www.w3schools.com/python/ref_string_split.asp
#
# Syntax: string.split(separator, maxsplit)
#
txt = "welcome to the jungle"
x = txt.split()
print(x)
# outcome:
# ['welcome', 'to', 'the', 'jungle']

txt = "hello, my name is Peter, I am 26 years old"
x = txt.split(", ")
print(x)
# outcome:
# ['hello', 'my name is Peter', 'I am 26 years old']

txt = "apple#banana#cherry#orange"
# setting the maxsplit parameter to 1, will return a list with 2 elements!
x = txt.split("#", 1)
print(x)
# outcome:
# ['apple', 'banana#cherry#orange']
