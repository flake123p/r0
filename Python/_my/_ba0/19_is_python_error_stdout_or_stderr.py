
x = y

#
# Control Group (Baseline)
#
'''
$ python 19_is_python_error_stdout_or_stderr.py
Output:
Traceback (most recent call last):
  File "/home/user0/ws/r0/Python/_my/_ba0/19_is_python_error_stdout_or_stderr.py", line 2, in <module>
    x = y
NameError: name 'y' is not defined
'''

#
# Treatment Group
#
'''
$ python 19_is_python_error_stdout_or_stderr.py 1>log.1.log 2>log.2.log
Output:

log.1.log:
(null)

log.2.log:
Traceback (most recent call last):
  File "/home/user0/ws/r0/Python/_my/_ba0/19_is_python_error_stdout_or_stderr.py", line 2, in <module>
    x = y
NameError: name 'y' is not defined
'''