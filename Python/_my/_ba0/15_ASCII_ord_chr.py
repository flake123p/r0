
print('ASCII of \'a\' =', ord('a'))
# ASCII of 'a' = 97

x = map(ord, 'abc')
print('ASCII of \'abc\' =', list(x))
# ASCII of 'abc' = [97, 98, 99]

print('CHAR of 97 =', chr(97))

x = map(chr, [97, 98, 99])
print('CHAR of 97 98 =', list(x))

x = b'\x61\x62\x63'
print('ASCII of x =', x)