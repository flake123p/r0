
class MyError(BaseException):
    pass

x = 1
y = 1.

print('====== demo 1 : x ======')
print(type(x))
print(type(x).__name__)
print(x.__class__)
print(x.__class__.__name__)
'''
====== demo 1 : x ======
<class 'int'>
int
<class 'int'>
int
'''

print('====== demo 2 : y ======')
print(type(y))
print(type(y).__name__)
print(y.__class__)
print(y.__class__.__name__)
'''
====== demo 2 : y ======
<class 'float'>
float
<class 'float'>
float
'''

print('====== demo 3 : MyError ======')
print(type(MyError))
print(type(MyError).__name__)
print(MyError.__class__)
print(MyError.__class__.__name__)
'''
====== demo 3 : MyError ======
<class 'type'>
type
<class 'type'>
type
'''

print('====== demo 4 : MyError: err ======')
err = MyError()
print(type(err))
print(type(err).__name__)
print(err.__class__)
print(err.__class__.__name__)
'''
====== demo 4 : MyError: err ======
<class '__main__.MyError'>
MyError
<class '__main__.MyError'>
MyError
'''