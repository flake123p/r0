#
# https://bayareanotes.com/python-unittest/
#
import unittest

def add(a, b):
    return a + b

def add_gpu(a, b):
    return a + b + 1

class TestAdd(unittest.TestCase):

    def test_add(self):
        self.assertEqual(add(1, 2), 3)
        self.assertEqual(add(0, 0), 0)
    
    def test_add2(self):
        self.assertEqual(add(1, 3), 4)
        self.assertEqual(add(0, 1), 1)
        # self.assertEqual(add(0, 1), add_gpu(0, 1))

if __name__ == '__main__':
    if 0:
        unittest.main()
        assert 0 # Flake: wouldn't run to this line

    try:
        unittest.main()
    finally:
        print('I am here!!!')  # Flake: will run to this line