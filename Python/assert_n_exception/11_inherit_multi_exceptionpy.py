#
# https://stackoverflow.com/questions/12826291/raise-two-errors-at-the-same-time
#
class MyException(ValueError, KeyError):
    def __init__(self, a, b):
        self.message = f'{a} + {b} = ' + str(a + b)
        super().__init__(self.message)

try:
    raise MyException(1, 2)
except ValueError as e:
    import sys
    ExceptionClass = sys.exc_info()[0]
    ExceptionName = sys.exc_info()[0].__name__
    print(f'ValueError: {ExceptionName}:\n  msg =', e)
except KeyError as e:
    import sys
    ExceptionClass = sys.exc_info()[0]
    ExceptionName = sys.exc_info()[0].__name__
    print(f'KeyError: {ExceptionName}:\n  msg =', e)
except Exception as e:
    import sys
    ExceptionClass = sys.exc_info()[0]
    ExceptionName = sys.exc_info()[0].__name__
    print(f'Exception: {ExceptionName}:\n  msg =', e)


try:
    raise MyException(1, 2)
except (ValueError, KeyError) as e:
    import sys
    ExceptionClass = sys.exc_info()[0]
    ExceptionName = sys.exc_info()[0].__name__
    print(f'ValueError, KeyError: {ExceptionName}:\n  msg =', e)
except Exception as e:
    import sys
    ExceptionClass = sys.exc_info()[0]
    ExceptionName = sys.exc_info()[0].__name__
    print(f'Exception: {ExceptionName}:\n  msg =', e)
