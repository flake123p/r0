
class MyExc1(Exception):
    pass

class MyExc2(Exception):
    pass
        

try:
    try:
        raise MyExc1()
    except Exception as e:
        raise MyExc2 from e
except MyExc2 as e: # Only except outer exception 'MyExc2'
    import sys
    ExceptionClass = sys.exc_info()[0]
    ExceptionName = sys.exc_info()[0].__name__
    print(f'Exception: {ExceptionName}:\n  msg =', e)
