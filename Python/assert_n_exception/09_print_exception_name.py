
class MyException(Exception):
    def __init__(self, a, b):
        self.message = f'{a} + {b} = ' + str(a + b)
        super().__init__(self.message)

try:
    raise MyException(1, 2)
except Exception as e:
    import sys
    ExceptionClass = sys.exc_info()[0]
    ExceptionName = sys.exc_info()[0].__name__
    print("ExceptionClass =", ExceptionClass)
    print("ExceptionName  =", ExceptionName)

    print(f'Except {ExceptionName}:\n  msg =', e)
