#
# https://stackoverflow.com/questions/12826291/raise-two-errors-at-the-same-time
#
# Exception Chaining and Embedded Tracebacks, https://peps.python.org/pep-3134/#id36
#
# handling Exceptions: https://docs.python.org/3/tutorial/errors.html#handling-exceptions
#
class MyException(ValueError, KeyError):
    def __init__(self, a, b):
        self.message = f'{a} + {b} = ' + str(a + b)
        super().__init__(self.message)

try:
    try:
        1/0
    except Exception as e:
        import sys
        ExceptionClass = sys.exc_info()[0]
        ExceptionName = sys.exc_info()[0].__name__
        # print(f'Inner Exception: {ExceptionName}:\n  msg =', e)
        raise MyException(1, 0) from e
except Exception as e:
    import sys
    ExceptionClass = sys.exc_info()[0]
    ExceptionName = sys.exc_info()[0].__name__
    print(f'Outer Exception: {ExceptionName}:\n  msg =', e)
    print(f'  context          =', e.__context__)
    print(f'  cause            =', e.__cause__)
    print(f'  suppress_context =', e.__suppress_context__)
    print(f'  traceback        =', e.__traceback__)
    print(f'\n\ntraceback.format_exc() =')
    import traceback
    print(traceback.format_exc())

    
