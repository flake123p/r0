
class MyException(Exception):
    """
        Some comments
    """
    pass

try:
    raise MyException('Ha Ha')
except MyException as e:
    print('Except MyException:\n  msg =', e)