#
# https://stackoverflow.com/questions/12826291/raise-two-errors-at-the-same-time
#
# For queueing multiple exception messages!
#
class MyException(Exception):
    def __init__(self, a, b):
        self.message = f'{a} + {b} = ' + str(a + b)
        super().__init__(self.message)

class A():
    def __init__(self):
        self.modulations = ['AA', 'BB', 'CC']

    def test_me(self):
        
        errors = []
        
        for modulation in self.modulations:
            # logging.info('Testing modulation = {modulation}'.format(**locals()))

            # self.digitalModulation().set('value', modulation)
            # reply = self.getReply()
    
            try: 
                # self._test_nodeValue(reply, self.digitalModulation())
                raise MyException(1, 2)
            except Exception as e:
                errors.append(e)
                
        if errors:
            raise Exception(errors)

try:
    a = A()
    a.test_me()
except Exception as e:
    print(e)
    # [MyException('1 + 2 = 3'), MyException('1 + 2 = 3'), MyException('1 + 2 = 3')]

