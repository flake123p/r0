
class MyException(Exception):
    def __init__(self, a, b):
        self.message = f'{a} + {b} = ' + str(a + b)
        super().__init__(self.message)

try:
    raise MyException(1, 2)
except MyException as e:
    print('Except MyException:\n  msg =', e)
