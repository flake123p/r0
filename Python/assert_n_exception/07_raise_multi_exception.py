#
# https://stackoverflow.com/questions/12826291/raise-two-errors-at-the-same-time
#
# For queueing multiple exception messages!
#
class MyException(Exception):
    def __init__(self, a, b):
        self.message = f'{a} + {b} = ' + str(a + b)
        super().__init__(self.message)

try:
    raise Exception(
        [
            MyException(1, 2),
            MyException(3, 4),
            MyException(5, 6),
        ]
    )
except MyException as e:
    import sys
    ExceptionClass = sys.exc_info()[0]
    ExceptionName = sys.exc_info()[0].__name__
    print("ExceptionClass =", ExceptionClass)
    print("ExceptionName  =", ExceptionName)
    print('Except MyException:\n  msg =', e)
except Exception as e:
    import sys
    ExceptionClass = sys.exc_info()[0]
    ExceptionName = sys.exc_info()[0].__name__
    print("ExceptionClass =", ExceptionClass)
    print("ExceptionName  =", ExceptionName)

    print(f'Except {ExceptionName}:\n  msg =', e)
    # msg = [MyException('1 + 2 = 3'), MyException('3 + 4 = 7'), MyException('5 + 6 = 11')]
