

# Using gdb:
https://stackoverflow.com/questions/26198847/can-i-step-into-python-library-code

Try this:
gdb --args python whatever.py ...

Then:
start
break posix_mkdir
continue


# yep
https://stackoverflow.com/questions/2615153/profiling-python-c-extensions

https://slwu89.github.io/src/profile_rcpp.html

https://gist.github.com/casotto/a05d1ae2eaeea21b8a0bc24d5cd14bff
```
  sudo apt-get update
  sudo apt-get install google-perftools libgoogle-perftools-dev
  sudo apt-get install kcachegrind
  pip install yep
```
Then
```
  python -m yep -c -o callgrind.out -- myTest.py

  python -m yep -c -o callgrind.out -- pth.py

  unset GTK_PATH && kcachegrind
```

p.s.: unset GTK_PATH is for vscode ...

# traceback
https://realpython.com/python-traceback/
?? just dump caller


# pprof
https://stackoverflow.com/questions/58182446/is-there-a-way-to-get-the-entire-c-stack-trace-of-a-python-code-block-from-pytho
?? just use yep
