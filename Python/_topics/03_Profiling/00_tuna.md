# See doc or run below
    https://pypi.org/project/tuna/

# 1 install
    pip install tuna

# 2 prof
    python -mcProfile -o program.prof yourfile.py

# 3 show
    tuna program.prof

# 4 leave page
    use [ctrl] + C