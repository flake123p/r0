import os

path = 'folder_a'

all_things = os.listdir(path)

txt_files = [f for f in os.listdir(path) if f.endswith(".txt")]

print('all_things = ')
for i in all_things:
    print(i)

print('txt_files = ')
for i in txt_files:
    print(i)