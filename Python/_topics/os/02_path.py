import os

path = 'folder_a'

file = 'hello.txt'

print(os.path.join(path,file))
print(os.path.join(path,file,'wtf')) #mutiple join

print('os.getcwd() = ', os.getcwd())
print('abspath()   = ', os.path.abspath(os.path.join(path,file)))
print('exists()    = ', os.path.exists(os.path.join(path,file)))