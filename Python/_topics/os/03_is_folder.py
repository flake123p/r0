import os

path = 'folder_a'

all_things = os.listdir(path)

print('all_things = [name][isFile][isDirectory]')
for i in all_things:
    fpath = os.path.join(path, i)
    isFile = os.path.isfile(fpath)
    isDirectory = os.path.isdir(fpath)
    print(i,isFile,isDirectory)
