import os

path = 'folder_a'

def walk(dirname):
	for name in os.listdir(dirname):
		path = os.path.join(dirname, name)
		if os.path.isfile(path):
			print(path)
		else:
			walk(path)

print('call walk() with pwd')
walk( os.getcwd() )

print('call walk() with folder_a')
walk( 'folder_a' )