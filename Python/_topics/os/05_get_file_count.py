
def get_file_count(path):
    import os
    ctr = 0
    all_things = os.listdir(path)
    for i in all_things:
        #print(i)
        if os.path.isfile(os.path.join(path, i)):
            ctr += 1
    return ctr

print(get_file_count('./folder_a/'))