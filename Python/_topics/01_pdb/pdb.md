
# pdb — The Python Debugger

    https://docs.python.org/3/library/pdb.html

# VS_Code
    MS launch.json Link: https://code.visualstudio.com/docs/cpp/launch-json-reference

    Left Tool Bar -> Run and Debug -> create a launch.json file -> 

    Add configuration -> Python File

    Example json:
    (keypoint 1: "justMyCode")
    (keypoint 2: "python")
    ```
    {
        "configurations": [

            {
                "name": "Python: Current File",
                "type": "python",
                "request": "launch",
                "program": "${file}",
                "console": "integratedTerminal",
                "justMyCode": false,
                "python": "/home/pupil/anaconda3/bin/python",
                "args": ["arg1", "arg2"]
            },
        ]
    }
    ```
    From: 
    https://stephencowchau.medium.com/visual-studio-code-debug-multiple-python-with-conda-env-in-wsl-5e36081ac9ea


# VSCode + torchrun
    https://stackoverflow.com/questions/67518928/how-to-make-vscode-launch-json-for-a-python-module
    {
        "version": "0.2.0",
        "configurations": [
            {
                "name": "Python: Current File",
                "type": "python",
                "module": "torch.distributed.launch",
                "request": "launch",
                "console": "integratedTerminal",
                "args": [
                    "--nproc_per_node", "1", 
                    "main_swav.py",
                    "--data_path", "/dataset/imagenet/train",
                ]
            }
        ]
    }

# Common commands

    [l] list source code

    [ll] list long source code

    [s] step

    [n] next

    [r] run until return