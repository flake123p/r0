
import cv2
import numpy as np


import pickle


def pickle_load(file):
    f = open(file, 'rb')
    data = pickle.load(f)
    f.close()
    return data


def pickle_dump(data, file):
    f = open(file, 'wb')
    pickle.dump(data, f)
    f.close()


def get_roi_area_ratio(res, points):
    w_min = res[0]
    w_max = 0
    h_min = res[1]
    h_max = 0
    for p in points:
        if p[0] < w_min:
            w_min = p[0]
        if p[0] > w_max:
            w_max = p[0]
        if p[1] < h_min:
            h_min = p[1]
        if p[1] > h_max:
            h_max = p[1]
    return [w_min, w_max, h_min, h_max], (w_max-w_min)*(h_max-h_min), (w_max-w_min)/(h_max-h_min)


# roi = w_min, w_max, h_min, h_max
def visualize_2d_points_with_roi(res, points, amp=1, file_name='2d_points.png'):
    roi, area, ratio = get_roi_area_ratio(res, points)
    visualize_2d_points(res, points, amp, roi, file_name)
    return roi, area, ratio


# roi = w_min, w_max, h_min, h_max
def visualize_2d_points(res, points, amp=1, roi=[], file_name='2d_points.png'):
    # red = (255, 0, 0)
    yellow = tuple(reversed((255, 255, 0)))
    edge = (255, 255, 0)
    width, height = res[0], res[1]
    image = np.zeros((height, width, 3), np.uint8)
    if len(roi) == 4:
        roi = [ int(i+0.5) for i in roi]
        for i in range(height):
            image[i, roi[0]] = edge
        for i in range(height):
            image[i, roi[1]] = edge
        for i in range(width):
            image[height-roi[2]-1, i] = edge
        for i in range(width):
            image[height-roi[3]-1, i] = edge
    for p in points:
        x = int(p[0]+0.5)
        y = int(p[1]+0.5)
        # to cv
        y = height - y - 1
        image[y, x] = yellow
    image = cv2.resize(image, (int(width*amp), int(height*amp)), interpolation=cv2.INTER_AREA)
    cv2.imwrite(file_name, image)


