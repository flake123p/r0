import os
import sys
sys.path.append('..')


def frame_sorter_yield(path):
    all_things = os.listdir(path)
    d = dict()
    for i in all_things:
        x = i.split(".")
        y = float(x[0] + '.' + x[1])
        d[i] = y
    d = sorted(d.items(), key=lambda x: x[1], reverse=False)
    for i in d:
        yield i[0]


class FrameSorter:
    def __init__(self, path):
        self.path = path
        all_things = os.listdir(path)
        d = dict()
        for i in all_things:
            x = i.split(".")
            y = float(x[0] + '.' + x[1])
            d[i] = y
        self.results = sorted(d.items(), key=lambda x: x[1], reverse=False)
        self.files = [i[0] for i in self.results]

    def dump_diff(self):
        prev = 0.0
        for i in self.d:
            y = i[1]
            if prev == 0:
                prev = y
            diff = y - prev
            prev = y
            print(y, diff)

    def yield_name(self):
        for i in self.d:
            yield i[0]


class FrameSorterLite:
    def __init__(self, path):
        self.path = path
        all_things = os.listdir(path)
        d = dict()
        for i in all_things:
            x = i.split(".")
            y = float(x[0] + '.' + x[1])
            d[i] = y
        d = sorted(d.items(), key=lambda x: x[1], reverse=False)
        self.files = [i[0] for i in d]
