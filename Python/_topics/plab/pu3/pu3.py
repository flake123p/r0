from operator import add, sub
import pickle

def p3float(inList, start='[', delimiter=', ', end=']'):
    outStr = start
    outStr = outStr + '{:.3f}'.format(inList[0])
    for i in range(1,len(inList)):
        outStr = outStr + delimiter + '{:.3f}'.format(inList[i])
    return outStr + end

def pickleLoad(file):
    f = open(file, 'rb')
    data = pickle.load(f)      
    f.close()
    return data

def pickleDump(data, file):
    f = open(file, 'wb')    
    pickle.dump(data, f)          
    f.close()

def get_file_count(path):
    import os
    ctr = 0
    all_things = os.listdir(path)
    for i in all_things:
        #print(i)
        if os.path.isfile(os.path.join(path, i)):
            ctr += 1
    return ctr


class FolderParser():
    def __init__(self, inFolder, outFolder, extension='.pkl'):
        self.inFolder = inFolder
        self.outFolder = outFolder
        f_cnt = get_file_count(self.inFolder)
        self.file_cnt = int(f_cnt/2)
        self.extension = extension

    def dump(self):
        print('file_cont =', self.file_cnt)
        for i in range(self.file_cnt):
            dIn = self.make_in_file(i)
            dOut = self.make_out_file(i)
            print(dIn['ref_file'])
            print(dIn['pupil_file'])
            print(dOut['ref_file'])
            print(dOut['pupil_file'])

    def make_in_file(self, index, pre=''):
        d = {
            'ref_file' :   self.inFolder + pre + 'ref_data_' + f'{index:02}' + self.extension,
            'pupil_file' : self.inFolder + pre + 'pupil_data_' + f'{index:02}' + self.extension
        }
        return d

    def make_out_file(self, index, pre=''):
        d = {
            'ref_file' :   self.outFolder + pre + 'ref_data_' + f'{index:02}' + self.extension,
            'pupil_file' : self.outFolder + pre + 'pupil_data_' + f'{index:02}' + self.extension
        }
        return d

    def dump_clear_text(self, d, path):
        import json
        with open(path, 'w') as f:
            for i in d:  
                f.write(json.dumps(i)) 
                f.write('\n')

    def load_in_file(self, index):
        d = self.make_in_file(index)
        d['ref_data'] = pickleLoad(d['ref_file'])
        d['pupil_data'] = pickleLoad(d['pupil_file'])
        return d

    def load_out_file(self, index):
        d = self.make_out_file(index)
        d['ref_data'] = pickleLoad(d['ref_file'])
        d['pupil_data'] = pickleLoad(d['pupil_file'])
        return d

    def store_out_file(self, index, d):
        out_dict = self.make_out_file(index)
        if 'ref_data' in d:
            pickleDump(d['ref_data'], out_dict['ref_file'])
        if 'pupil_data' in d:
            pickleDump(d['pupil_data'], out_dict['pupil_file'])
    
    def traverse_data(self):
        for i in range(self.file_cnt):
            yield self.load_in_file(i)

    def traverse_DataParser(self):
        for i in range(self.file_cnt):
            yield DataParser(self.load_in_file(i))

    def output_with_pos_diffs(self, pos_diffs):
        for i in range(self.file_cnt):
            #print(pos_diffs[i])
            print(p3float(pos_diffs[i], start='', end=''))
            d = self.load_in_file(i)
            for j in range(len(d['pupil_data'])):
                if d['pupil_data'][j]['topic'] == 'pupil.0.3d':
                    continue
                if d['pupil_data'][j]['topic'] == 'pupil.1.3d':
                    continue
                a = d['pupil_data'][j]['norm_pos'][0] - pos_diffs[i][0]
                b = d['pupil_data'][j]['norm_pos'][1] - pos_diffs[i][1]
                #d['pupil_data'][j]['norm_pos'] = [a,b]
                d['pupil_data'][j]['norm_kal'] = [a,b]
                #d['pupil_data'][j]['norm_adj'] = [a,b]
                #d['pupil_data'][j]['norm_ori'] = [a,b]
            self.store_out_file(i, d)
        pass

    def dump_inFile_to_clear_txt(self):
        for i in range(self.file_cnt):
            d = self.load_in_file(i)
            ct_files = self.make_in_file(i, pre='CT_')
            self.dump_clear_text(d['ref_data'], ct_files['ref_file'])
            self.dump_clear_text(d['pupil_data'], ct_files['pupil_file'])

            d = self.load_out_file(i)
            ct_files = self.make_out_file(i, pre='CT_')
            self.dump_clear_text(d['ref_data'], ct_files['ref_file'])
            self.dump_clear_text(d['pupil_data'], ct_files['pupil_file'])


class DataParser():
    def __init__(self, d):
        self.ref_data = d['ref_data']
        self.pupil_data = d['pupil_data']

        self.ref_edges = self.getEdgesByPos(self.ref_data)

        self.pos_avgs = self.getFirstAvgNormPos(self.pupil_data, self.ref_edges)
    
    def dump(self):
        d = {
            'ref_data' : self.ref_data,
            'pupil_data' : self.pupil_data,
            'ref_edges' : self.ref_edges,
            'pos_avgs' : self.pos_avgs,
        }
        return d


    def getEdgesByPos(self, data, diff_th = 0.4):
        old_pos = [0.0, 0.0]
        old_item = None
        #idx = 0
        edges = []
        for i in range(len(data)):
            item = data[i]

            norm_x = item["norm_pos"][0]
            norm_y = item["norm_pos"][1]

            if old_item == None:
                old_item = item
                old_pos = [norm_x, norm_y]
                continue
            
            diff = abs(norm_x-old_pos[0])+abs(norm_y-old_pos[1])
            #print(i, diff)
            if diff < diff_th:
                ts = data[i]["timestamp"]
                data[i] = old_item.copy()
                data[i]["timestamp"] = ts
            else:
                old_item = item.copy()
                old_pos = [norm_x, norm_y]
                edge = [i, ts]
                edges.append(edge)
                continue
        return edges

    # Dump & TODO
    def getFirstAvgNormPos(self, data, edges):
        eIdx = 0
        ctr = 0
        norm_sum = [0,0]
        last_sec = False
        avgs = []
        #norm_sums = []
        for i in range(len(data)):
            if data[i]['topic'] == 'pupil.0.3d':
                continue
            if last_sec:
                #print(data[i]['norm_pos'])
                norm_sum = list( map(add, norm_sum, data[i]['norm_pos']) )
                ctr = ctr + 1
                if i == len(data) - 1:
                    avg = [norm_sum[0]/ctr, norm_sum[1]/ctr]
                    avgs.append(avg)
            else:
                if data[i]['timestamp'] > edges[eIdx][1]:
                    # do average
                    avg = [norm_sum[0]/ctr, norm_sum[1]/ctr]
                    avgs.append(avg)
                    norm_sum = data[i]['norm_pos']
                    eIdx = eIdx + 1
                    ctr = 0
                    #print('=========================')
                    if eIdx == len(edges):
                        last_sec = True
                    return avgs # temp
                else:
                    #print(data[i]['norm_pos'])
                    norm_sum = list( map(add, norm_sum, data[i]['norm_pos']) )
                    ctr = ctr + 1
        return avgs


class DataParserMgr(FolderParser):
    def __init__(self, inFolder, outFolder, extension='.pkl'):
        super().__init__(inFolder, outFolder, extension)

        self.all = []
        self.first_pos_avg = []
        for dp in self.traverse_DataParser():
            self.all.append(dp.dump())
            self.first_pos_avg.append(dp.pos_avgs[0])
        
        self.first_pos_diff = self.first_pos_diff_to_index_0()
    
    def first_pos_diff_to_index_0(self):
        ret = []
        base = self.first_pos_avg[0]
        for i in range(len(self.first_pos_avg)):
            diff = list( map(sub, self.first_pos_avg[i], base) )
            #diff = i - base
            ret.append(diff)
        return ret

    def dump(self):
        d = {
            'all' : self.all,
            'first_pos_avg' : self.first_pos_avg,
            'first_pos_diff' : self.first_pos_diff,
        }
        return d