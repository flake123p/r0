import os
import cv2


class FramesToVideo:
    def __init__(self, path, frame_files, out_video_abs_name, fps=30, fourcc=0):
        if os.path.exists(out_video_abs_name):
            print(out_video_abs_name + ' ... exist, skip FramesToVideo ...')
            print(out_video_abs_name + ' ... exist, skip FramesToVideo ...')
            print(out_video_abs_name + ' ... exist, skip FramesToVideo ...')
            return
        if len(frame_files) == 0:
            return
        # cv2 video object
        frame = cv2.imread(os.path.join(path, frame_files[0]))
        if frame is None:
            print('1123', os.path.join(path, frame_files[0]))
            print('cv2.imread error !!!')
            return
        height, width, layers = frame.shape

        video = cv2.VideoWriter(out_video_abs_name, fourcc, fps, (width, height))
        for fr in frame_files:
            video.write(cv2.imread(os.path.join(path, fr)))

        cv2.destroyAllWindows()
        video.release()


