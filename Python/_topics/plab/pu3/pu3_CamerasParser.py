import os
import cv2
import csv
from .pu3_FrameSorter import FrameSorter
from .pu3_video import FramesToVideo
from .pu3 import *

class CamerasParser:
    def __init__(self, path):
        self.path = path
        self.path_w = os.path.join(path, 'w')
        self.path_0 = os.path.join(path, '0')
        self.path_0r = os.path.join(path, '0r')
        self.path_1 = os.path.join(path, '1')
        self.path_1r = os.path.join(path, '1r')
        self.video_w = os.path.join(path, 'w.avi')
        self.video_0 = os.path.join(path, '0.avi')
        self.video_1 = os.path.join(path, '1.avi')
        self.files_w = FrameSorter(self.path_w)
        self.files_0 = FrameSorter(self.path_0)
        self.files_0r = FrameSorter(self.path_0r)
        self.files_1 = FrameSorter(self.path_1)
        self.files_1r = FrameSorter(self.path_1r)
        self.pkls_0r = None
        self.pkls_1r = None

        self.frame_start_time = 0 # for rela_time, unit = 1 second

        self.ebc_csv_info = ['rela_time', 'world_time', 'abs_time', 'ebc_2d_x', 'ebc_2d_y']

    def gen_video_w(self):
        out_video = os.path.join(self.path, 'w.avi')
        FramesToVideo(self.path_w, self.files_w.files, out_video, 30)

    def gen_video_eye0(self):
        out_video = os.path.join(self.path, 'eye0.avi')
        FramesToVideo(self.path_0, self.files_0.files, out_video, 30)

    def gen_ebc_csv(self, path, results, out_file):
        if os.path.exists(out_file):
            print(out_file + ' ... exist, skip FramesToVideo ...')
            print(out_file + ' ... exist, skip FramesToVideo ...')
            print(out_file + ' ... exist, skip FramesToVideo ...')
            return
        dict_list = list()
        first_time_is_red = False
        first_time = 0.0
        for x in results:
            file = x[0]
            time = x[1]
            if first_time_is_red is False:
                first_time_is_red = True
                first_time = time
            abs_file = os.path.join(path, file)
            plk = pickleLoad(abs_file)
            d = dict()
            d['rela_time'] = time - first_time
            d['world_time'] = time - self.frame_start_time
            d['abs_time'] = time
            d['ebc_2d_x'] = plk['projected_sphere']['center'][0]
            d['ebc_2d_y'] = plk['projected_sphere']['center'][1]
            dict_list.append(d)
        with open(out_file, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=self.ebc_csv_info)
            writer.writeheader()
            writer.writerows(dict_list)
        return dict_list

    def gen_ebc_csv_0(self):
        out_file = os.path.join(self.path, 'eye0.csv')
        return self.gen_ebc_csv(self.path_0r, self.files_0r.results, out_file)

    def gen_ebc_csv_1(self):
        out_file = os.path.join(self.path, 'eye1.csv')
        return self.gen_ebc_csv(self.path_1r, self.files_1r.results, out_file)

    def get_pkls(self, parser):
        d = list()
        for x in parser.results:
            file = x[0]
            abs_file = os.path.join(parser.path, file)
            pkl = pickleLoad(abs_file)
            d.append(pkl)
        return d

    def get_pkls_all(self):
        self.pkls_0r = self.get_pkls(self.files_0r)
        self.pkls_1r = self.get_pkls(self.files_1r)
        return [self.pkls_0r, self.pkls_1r]




