import pupil_util as pu

use_2ds_1x = True
if use_2ds_1x:
    inFolder = '/home/ai0/sda/plab/p3/pupil/pupil_src/data2ds_1x/'
    outFolder = '/home/ai0/sda/plab/p3/pupil/pupil_src/data2ds_1x/'
    default_file_postfix = '_pupil_dataX.txt'
else:
    inFolder = '/home/ai0/sda/plab/p3/pupil/pupil_src/data2_slipx/'
    outFolder = '/home/ai0/sda/plab/p3/pupil/pupil_src/data2_slipx/'
    default_file_postfix = '_pupil_dataX.txt'

print(inFolder)

pupilFile = 'c_pupil_data.txt'
refFile = 'c_ref_data.txt'

pupil_data = pu.pickleLoad(inFolder + pupilFile)
ref_data = pu.pickleLoad(inFolder + refFile)

ref_edges = pu.getEdgesByPos(ref_data)

avgs = pu.getFirstAvgNormPos(pupil_data, ref_edges)

baseline = avgs[0]
print('calib avg')
print(baseline[0], ',', baseline[1])

print('file avg')
for i in range(10):
    inFile = inFolder + 's' + str(i) + default_file_postfix
    inRefFile = inFolder + 's' + str(i) + '_ref_data.txt'
    outFile = inFolder + 's' + str(i) + '_pupil_data.txt'
    pupil_data = pu.pickleLoad(inFile)
    ref_data = pu.pickleLoad(inRefFile)
    ref_edges = pu.getEdgesByPos(ref_data)
    avgs = pu.getFirstAvgNormPos(pupil_data, ref_edges)
    #print(avgs[0])
    print(avgs[0][0], ',', avgs[0][1])
    of0 = avgs[0][0] - baseline[0]
    of1 = avgs[0][1] - baseline[1]
    pupil_data = pu.minus2dNormPos(pupil_data, of0, of1)
    pu.pickleDump(pupil_data, outFile)