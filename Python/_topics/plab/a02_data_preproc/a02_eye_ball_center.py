import pupil_util as pu
import numpy as np

def mse(a):
    m = a.mean()
    r = 0
    for i in a:
        r = r + np.square(i-m)
    return r

use_2ds_1x = False
if use_2ds_1x:
    inFolder = '/home/ai0/sda/plab/p3/pupil/pupil_src/data2ds_1x/'
    outFolder = '/home/ai0/sda/plab/p3/pupil/pupil_src/data2ds_1x/'
    default_file_postfix = '_pupil_dataX.txt'
else:
    inFolder = '/home/ai0/sda/plab/p3/pupil/pupil_src/data2_slipx/'
    outFolder = '/home/ai0/sda/plab/p3/pupil/pupil_src/data2_slipx/'
    default_file_postfix = '_pupil_dataX.txt'

print(inFolder)

pupilFile = 'c_pupil_data.txt'
refFile = 'c_ref_data.txt'

pupil_data = pu.pickleLoad(inFolder + pupilFile)
ref_data = pu.pickleLoad(inFolder + refFile)

ref_edges = pu.getEdgesByPos(ref_data)

avgs = pu.getFirstAvgNormPos(pupil_data, ref_edges)

baseline = avgs[0]
# print(avgs)

# dump_center_c, dump_radius_c = pu.getSphereCenterRadiusLists(pupil_data)
# dump_center_c = np.array(dump_center_c)
# dump_radius_c = np.array(dump_radius_c)
# dump_center_c_mse = [
#     mse(dump_center_c[:,0]),
#     mse(dump_center_c[:,1]),
#     mse(dump_center_c[:,2]),
#     ]
# dump_center_c_avg = [
#     np.average(dump_center_c[:,0]),
#     np.average(dump_center_c[:,1]),
#     np.average(dump_center_c[:,2]),
#     ]
# dump_radius_c_mse = mse(dump_radius_c)
# dump_radius_c_avg = np.average(dump_radius_c)
d = pu.parseSphereCenter(pupil_data)

z = []
for i in pupil_data:
    #print(i['topic'])
    #if i['topic'] != 'pupil.0.3d':
    #    continue
    z.append(i['confidence'])


def get_file_name_with_num(prefix, postfix, start, stop):
    for i in range(start, stop):
        out = prefix + str(i) + postfix
        yield out
nameA = get_file_name_with_num(inFolder+'a', default_file_postfix, 0, 10)
nameS = get_file_name_with_num(inFolder+'s', default_file_postfix, 0, 10)
ds = []
for i in nameS:
    #print(i)
    temp = pu.parseSphereCenterByFile(i)
    ds.append(temp)
for temp in ds:
    curr = np.array(temp['center_avg'])
    base = np.array(d['center_avg'])
    #print(curr-base)
    x = (curr-base)**2
    #print(x)
    #print(np.sum(x))
    x = np.sqrt(np.sum(x))
    print(x)
# for i in range(10):
#     inFile = inFolder + 's' + str(i) + default_file_postfix
#     inRefFile = inFolder + 's' + str(i) + '_ref_data.txt'
#     outFile = inFolder + 's' + str(i) + '_pupil_data.txt'
#     pupil_data = pu.pickleLoad(inFile)
#     ref_data = pu.pickleLoad(inRefFile)
#     ref_edges = pu.getEdgesByPos(ref_data)
#     avgs = pu.getFirstAvgNormPos(pupil_data, ref_edges)
#     print(avgs[0])
#     of0 = avgs[0][0] - baseline[0]
#     of1 = avgs[0][1] - baseline[1]
#     pupil_data = pu.minus2dNormPos(pupil_data, of0, of1)
#     pu.pickleDump(pupil_data, outFile)
