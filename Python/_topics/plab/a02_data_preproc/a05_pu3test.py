import sys
sys.path.append('..')

import pu3

inFolder = '/home/ai0/sda/plab/p42/pupil/pupil_src/data_06/'
outFolder = '/home/ai0/sda/plab/p42/pupil/pupil_src/data_06x/'

fp = pu3.DataParserMgr(inFolder, outFolder)
out = fp.dump()

pos_diffs = out['first_pos_diff']

fp.output_with_pos_diffs(pos_diffs)
#fp.dump_inFile_to_clear_txt()