import sys
sys.path.append('..')

import pu3

pu_file = '/home/ai0/sda/plab/p42/pupil/data/data_07/pupil_data_00.pkl'
re_file = '/home/ai0/sda/plab/p42/pupil/data/data_07/ref_data_00.pkl'

pupil_data = pu3.pickleLoad(pu_file)
ref_data = pu3.pickleLoad(re_file)
