import pupil_util as pu
import numpy as np
import os

class PuMainParser():
    def __init__(self, folder):
        self.folder = folder
        txt_files = [f for f in os.listdir(self.folder) if f.endswith(".txt")]
        self.file_num = int(len(txt_files) / 2)
        self.pupils = []
        self.refs = []
        for i in range(int(self.file_num)):
            ref_file = self.folder + 'ref_data_' + f'{i:02}' + '.txt'
            pupil_file = self.folder + 'pupil_data_' + f'{i:02}' + '.txt'
            p = PupilParser(pupil_file)
            r = RefParser(ref_file)
            self.pupils.append(p)
            self.refs.append(r)
        # ...
        self.norm_table_pupil_2d =[]
        for i in self.pupils:
            self.norm_table_pupil_2d.append(PupilParser.get_2d_table(i))
        # ...
        self.ebc_mean = []
        for i in self.pupils:
            self.ebc_mean.append(PupilParser.get_ebc_mean(i))



class RefParser():
    def __init__(self, file):
        self.data = pu.pickleLoad(file)

class PupilParser():
    def __init__(self, file):
        self.data = pu.pickleLoad(file)

    def get_2d_table(self):
        # pos[0] pos[1] ori[0] ori[1] adj[0] adj[1] kal[0] kal[1] type
        ret = []
        for i in self.data:
            if i['topic'] == 'pupil.0.3d' or i['topic'] == 'pupil.1.3d':
                continue
            adj = i['norm_adj']
            kal = i['norm_kal']
            if len(adj) == 0:
                adj = [0.0, 0.0]
            if len(kal) == 0:
                kal = [0.0, 0.0]
            item = [
                i['norm_pos'][0],
                i['norm_pos'][1],
                i['norm_ori'][0],
                i['norm_ori'][1],
                adj[0],
                adj[1],
                kal[0],
                kal[1],
                i['norm_type'],
            ]
            #print(item)
            ret.append(np.array(item))
        return np.array(ret)

    def get_2d_table_col_index(self):
        col_index = [
            'pos[0]',
            'pos[1]',
            'ori[0]',
            'ori[1]',
            'adj[0]',
            'adj[1]',
            'kal[0]',
            'kal[1]',
            'type'
        ]
        return col_index

    def get_ebc_mean(self, minConfidence=0.99):
        # pos[0] pos[1] ori[0] ori[1] adj[0] adj[1] kal[0] kal[1] type
        ctr = 0
        accu = [0.0, 0.0, 0.0]
        for i in self.data:
            if i['topic'] == 'pupil.0.3d' or i['topic'] == 'pupil.1.3d':
                if i['confidence'] < minConfidence:
                    continue
                accu[0] += i['sphere']['center'][0]
                accu[1] += i['sphere']['center'][1]
                accu[2] += i['sphere']['center'][2]
                ctr += 1
        if ctr != 0:
            accu = np.array(accu) / ctr
        return accu