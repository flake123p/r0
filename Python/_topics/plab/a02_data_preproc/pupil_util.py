
import pickle
def pickleLoad(file):
    f = open(file, 'rb')
    data = pickle.load(f)      
    f.close()
    return data

def pickleDump(data, file):
    f = open(file, 'wb')    
    pickle.dump(data, f)          
    f.close()

def getEdgesByPos(data, diff_th = 0.4):
    old_pos = [0.0, 0.0]
    old_item = None
    #idx = 0
    edges = []
    for i in range(len(data)):
        item = data[i]
        #print(item["timestamp"])

        norm_x = item["norm_pos"][0]
        norm_y = item["norm_pos"][1]
        #scrn_x = item["screen_pos"][0]
        #scrn_y = item["screen_pos"][1]
        
        if old_item == None:
            old_item = item
            old_pos = [norm_x, norm_y]
            continue
        
        diff = abs(norm_x-old_pos[0])+abs(norm_y-old_pos[1])
        #print(i, diff)
        if diff < diff_th:
            ts = data[i]["timestamp"]
            data[i] = old_item.copy()
            data[i]["timestamp"] = ts
        else:
            old_item = item.copy()
            old_pos = [norm_x, norm_y]
            edge = [i, ts]
            edges.append(edge)
            continue
    return edges

from operator import add
# Dump & TODO
def getAvgNormPos(data, edges):
    eIdx = 0
    ctr = 0
    norm_sum = [0,0]
    last_sec = False
    avgs = []
    #norm_sums = []
    for i in range(len(data)):
        if data[i]['topic'] == 'pupil.0.3d':
            continue
        if last_sec:
            print(data[i]['norm_pos'])
            norm_sum = list( map(add, norm_sum, data[i]['norm_pos']) )
            ctr = ctr + 1
            if i == len(data) - 1:
                avg = [norm_sum[0]/ctr, norm_sum[1]/ctr]
                avgs.append(avg)
        else:
            if data[i]['timestamp'] > edges[eIdx][1]:
                # do average
                avg = [norm_sum[0]/ctr, norm_sum[1]/ctr]
                avgs.append(avg)
                norm_sum = data[i]['norm_pos']
                eIdx = eIdx + 1
                ctr = 0
                print('=========================')
                if eIdx == len(edges):
                    last_sec = True
            else:
                print(data[i]['norm_pos'])
                norm_sum = list( map(add, norm_sum, data[i]['norm_pos']) )
                ctr = ctr + 1
    return avgs

def getFirstAvgNormPos(data, edges):
    eIdx = 0
    ctr = 0
    norm_sum = [0,0]
    last_sec = False
    avgs = []
    #norm_sums = []
    for i in range(len(data)):
        if data[i]['topic'] == 'pupil.0.3d':
            continue
        if last_sec:
            #print(data[i]['norm_pos'])
            norm_sum = list( map(add, norm_sum, data[i]['norm_pos']) )
            ctr = ctr + 1
            if i == len(data) - 1:
                avg = [norm_sum[0]/ctr, norm_sum[1]/ctr]
                avgs.append(avg)
        else:
            if data[i]['timestamp'] > edges[eIdx][1]:
                # do average
                avg = [norm_sum[0]/ctr, norm_sum[1]/ctr]
                avgs.append(avg)
                norm_sum = data[i]['norm_pos']
                eIdx = eIdx + 1
                ctr = 0
                #print('=========================')
                if eIdx == len(edges):
                    last_sec = True
                return avgs # temp
            else:
                #print(data[i]['norm_pos'])
                norm_sum = list( map(add, norm_sum, data[i]['norm_pos']) )
                ctr = ctr + 1
    return avgs

def minus2dNormPos(data, of0, of1):
    for i in range(len(data)):
        if data[i]['topic'] == 'pupil.0.3d':
            continue
        data[i]['norm_pos'][0] = data[i]['norm_pos'][0] - of0
        data[i]['norm_pos'][1] = data[i]['norm_pos'][1] - of1
    return data

def getSphereCenterRadiusLists(data):
    list_center = []
    list_radius = []
    for i in data:
        if i['topic'] != 'pupil.0.3d':
            continue
        list_center.append(i['sphere']['center'])
        list_radius.append(i['sphere']['radius'])
    return list_center, list_radius

import numpy as np

def mse(a):
    m = a.mean()
    r = 0
    for i in a:
        r = r + np.square(i-m)
    return r

def parseSphereCenter(data):
    dump_center, dump_radius = getSphereCenterRadiusLists(data)
    dump_center = np.array(dump_center)
    dump_radius = np.array(dump_radius)
    dump_center_mse = [
        mse(dump_center[:,0]),
        mse(dump_center[:,1]),
        mse(dump_center[:,2]),
        ]
    dump_center_avg = [
        np.average(dump_center[:,0]),
        np.average(dump_center[:,1]),
        np.average(dump_center[:,2]),
        ]
    print(dump_center_avg[0],',',dump_center_avg[1],',',dump_center_avg[2])
    dump_radius_mse = mse(dump_radius)
    dump_radius_avg = np.average(dump_radius)
    d = {}
    d['center'] = dump_center
    d['radius'] = dump_radius
    d['center_mse'] = dump_center_mse
    d['radius_mse'] = dump_radius_mse
    d['center_avg'] = dump_center_avg
    d['radius_avg'] = dump_radius_avg
    return d

def parseSphereCenterByFile(file):
    return parseSphereCenter(pickleLoad(file))

class PupilParser():
    def __init__(self, file):
        self.data = pickleLoad(file)

    def get_2d_table(self):
        # pos[0] pos[1] ori[0] ori[1] adj[0] adj[1] kal[0] kal[1] type
        ret = []
        for i in self.data:
            if i['topic'] == 'pupil.0.3d' or i['topic'] == 'pupil.0.3d':
                continue
            adj = i['norm_adj']
            kal = i['norm_kal']
            if len(adj) == 0:
                adj = [0.0, 0.0]
            if len(kal) == 0:
                kal = [0.0, 0.0]
            item = [
                i['norm_pos'][0],
                i['norm_pos'][1],
                i['norm_ori'][0],
                i['norm_ori'][1],
                adj[0],
                adj[1],
                kal[0],
                kal[1],
                i['norm_type'],
            ]
            #print(item)
            ret.append(np.array(item))
        index = [
            'pos[0]',
            'pos[1]',
            'ori[0]',
            'ori[1]',
            'adj[0]',
            'adj[1]',
            'kal[0]',
            'kal[1]',
            'type'
        ]
        return np.array(ret),index
