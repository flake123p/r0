import pupil_util as pu

use_2ds_1x = True
if use_2ds_1x:
    inFolder = '/home/ai0/sda/plab/p3/pupil/pupil_src/data2ds_1x/'
    outFolder = '/home/ai0/sda/plab/p3/pupil/pupil_src/data2ds_1x/'
    default_file_postfix = '_pupil_data_def.txt'
    diffs = [
        [1.66516085466918,	12.5807072823907],
        [1.54781956734877,	10.9280034774918],
        [-0.119073864331483,	12.0384542320389],
        [0.670683597262666,	11.2046364454823],
        [1.65087144736521,	12.5690465453851],
        [2.03323873357289,	14.7687957444415],
        [0.341224959372141,	11.6459738492591],
        [-0.449266212092368,	11.13954555362],
        [-1.94304407927797,	12.0757934692257],
        [-0.0706420970344652,	10.6855289751471],
    ]
else:
    inFolder = '/home/ai0/sda/plab/p3/pupil/pupil_src/data2_slipx/'
    outFolder = '/home/ai0/sda/plab/p3/pupil/pupil_src/data2_slipx/'
    default_file_postfix = '_pupil_dataX.txt'
    diffs = [
        [3.52972211629196,	20.3266075436082],
        [3.87731084450616,	21.2168419143486],
        [3.00378491497355,	22.4008774149109],
        [2.54701471884636,	21.3755195376012],
        [4.02884727313278,	23.2761244792325],
        [4.01261123314629,	23.4320437521786],
        [3.86453485903752,	23.8600668771776],
        [1.42009796404625,	22.9828567625164],
        [2.71069042901394,	22.9021834037219],
        [3.02464664998047,	23.2086118270166],
    ]
print(inFolder)

pupilFile = 'c_pupil_data.txt'
refFile = 'c_ref_data.txt'

pupil_data = pu.pickleLoad(inFolder + pupilFile)
ref_data = pu.pickleLoad(inFolder + refFile)


#ref_edges = pu.getEdgesByPos(ref_data)
#avgs = pu.getFirstAvgNormPos(pupil_data, ref_edges)

#baseline = avgs[0]
#print('calib avg')
#print(baseline[0], ',', baseline[1])

#print('file avg')
for i in range(10):
    inFile = inFolder + 's' + str(i) + default_file_postfix
    #inRefFile = inFolder + 's' + str(i) + '_ref_data.txt'
    outFile = inFolder + 's' + str(i) + '_pupil_data.txt'
    data = pu.pickleLoad(inFile)
    for j in range(len(data)):
        if data[j]['topic'] == 'pupil.0.3d':
            continue
        x = data[j]['norm_pos'][0] * 160
        y = (1 - data[j]['norm_pos'][1]) * 120
        x = x - diffs[i][0]
        y = y - diffs[i][1]
        x = x / 160
        y = 1 - (y / 120)
        data[j]['norm_pos'][0] = x
        data[j]['norm_pos'][1] = y
    #ref_data = pu.pickleLoad(inRefFile)
    #ref_edges = pu.getEdgesByPos(ref_data)
    #avgs = pu.getFirstAvgNormPos(pupil_data, ref_edges)
    #print(avgs[0])
    #print(avgs[0][0], ',', avgs[0][1])
    #of0 = avgs[0][0] - baseline[0]
    #of1 = avgs[0][1] - baseline[1]
    #pupil_data = pu.minus2dNormPos(pupil_data, of0, of1)
    pu.pickleDump(data, outFile)