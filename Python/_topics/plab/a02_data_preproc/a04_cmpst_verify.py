import pupil_util as pu
import pupil_util2 as pu2
import numpy as np
#import pandas as pd
import os

in_path = '/home/ai0/sda/plab/p42/pupil/pupil_src/data_02/'
out_path = '/home/ai0/sda/plab/p42/pupil/pupil_src/data_out/'

mainParser = pu2.PuMainParser(in_path)

ebc_mean = mainParser.ebc_mean

z = {'pos':[0.11,0.22],'ori':[0.32,0.43]}
z['adj'] = z['pos']
z['kal'] = [0.8,0.9]
z['pos'] = z['kal']