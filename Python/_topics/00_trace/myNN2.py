#
# https://pytorch.org/tutorials/recipes/recipes/defining_a_neural_network.html
# https://ithelp.ithome.com.tw/articles/10279986
#
import torch
import torch.nn as nn
import torch.nn.functional as F

print('trace start')

# https://www.datascienceweekly.org/tutorials/how-to-define-a-relu-layer-in-pytorch
fc1 = nn.Linear(9, 5)
fc2 = nn.Linear(5, 3)
relu1 = nn.ReLU(inplace=False)
relu2 = nn.ReLU(inplace=False)
model = nn.Sequential()
model.add_module("fc1", fc1)
model.add_module("relu1", relu1)
model.add_module("fc2", fc2)
model.add_module("relu2", relu2)
model.cuda()

input = torch.rand((1, 9))
device = torch.device("cuda")

input = input.to(device)
output = model.forward(input)

print(output)

'''
cu list (30) =
    cuGetProcAddress
    cuDriverGetVersion
    cuInit
    cuGetExportTable
    cuModuleGetLoadingMode
    cuDeviceGetCount
    cuDeviceGet
    cuDeviceGetName
    cuDeviceTotalMem_v2
    cuDeviceGetAttribute
    cuDeviceGetUuid
    cuCtxGetDevice
    cuDevicePrimaryCtxGetState
    cuCtxGetCurrent
    cuCtxSetCurrent
    cuDevicePrimaryCtxRetain
    cuModuleGetGlobal_v2
    cuStreamIsCapturing
    cuMemAlloc_v2
    cuMemcpyHtoDAsync_v2
    cuStreamSynchronize
    cuMemFree_v2
    cuEventCreate
    cuModuleGetFunction
    cuOccupancyMaxActiveBlocksPerMultiprocessorWithFlags
    cuLaunchKernel
    cuFuncGetAttribute
    cuMemcpyDtoHAsync_v2
    cuModuleUnload
    cuDevicePrimaryCtxRelease

cuda list (12) =
    cudaGetDeviceCount
    cudaGetDevice
    cudaGetDeviceProperties
    cudaGetLastError
    cudaStreamIsCapturing
    cudaMalloc
    cudaMemcpyAsync
    cudaStreamSynchronize
    cudaLaunchKernel
    cudaFuncGetAttributes
    cudaPeekAtLastError
    cudaDeviceGetAttribute

cudnn list (0) =

cublas list (4) =
    cublasCreate_v2
    cublasSetStream_v2
    cublasSetWorkspace_v2
    cublasSetMathMode

cublasLt list (11) =
    cublasLtMatmulDescCreate
    cublasLtMatmulDescSetAttribute
    cublasLtMatrixLayoutCreate
    cublasLtMatmulPreferenceCreate
    cublasLtMatmulPreferenceSetAttribute
    cublasLtCtxInit
    cublasLtMatmulAlgoGetHeuristic
    cublasLtMatmul
    cublasLtMatmulPreferenceDestroy
    cublasLtMatrixLayoutDestroy
    cublasLtMatmulDescDestroy
'''