# Page:
https://docs.python.org/3/library/trace.html?highlight=trace#module-trace

# TODO:
traceback
https://docs.python.org/3/library/traceback.html?highlight=trace#module-traceback

tracemalloc
https://docs.python.org/3/library/tracemalloc.html?highlight=trace#module-tracemalloc

inspect
https://docs.python.org/3/library/inspect.html?highlight=trace#inspect.trace


# Run:
python -m trace --count -C dump/ myTest.py

python -m trace --listfuncs myTest.py>out.listfuncs.dump
 - No much use
 - Just a alphabetic sorted full list

python -m trace --trackcalls myTest.py>out.trackcalls.dump
 - No much use
 - Just a alphabetic sorted full list

python -m trace -t myTest.py>out.trace.dump
 - 1, use this (2 million line... should be filtering out)
 - 2. use this
 - 3. use this
