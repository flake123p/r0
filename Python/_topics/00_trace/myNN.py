#
# https://pytorch.org/tutorials/recipes/recipes/defining_a_neural_network.html
# https://ithelp.ithome.com.tw/articles/10279986
#
import torch
import torch.nn as nn
import torch.nn.functional as F

print('trace start')

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(9, 5)
        self.fc2 = nn.Linear(5, 3)
        self.relu1 = nn.ReLU()
        self.relu2 = nn.ReLU()
        
    def forward(self, x):
        x = self.fc1(x)
        x = self.relu1(x)
        x = self.fc2(x)
        x = self.relu2(x)
        return x

myNet = Net()
input = torch.rand((1, 9))
device = torch.device("cuda")

myNet = myNet.to(device)
input = input.to(device)

print(myNet(input))

'''
cu list (38) =
    cuGetProcAddress
    cuDriverGetVersion
    cuInit
    cuGetExportTable
    cuModuleGetLoadingMode
    cuDeviceGetCount
    cuDeviceGet
    cuDeviceGetName
    cuDeviceTotalMem_v2
    cuDeviceGetAttribute
    cuDeviceGetUuid
    cuCtxGetDevice
    cuDevicePrimaryCtxGetState
    cuCtxGetCurrent
    cuCtxSetCurrent
    cuDevicePrimaryCtxRetain
    cuModuleGetGlobal_v2
    cuStreamIsCapturing
    cuMemAlloc_v2
    cuMemcpyHtoDAsync_v2
    cuStreamSynchronize
    cuModuleGetFunction
    cuLaunchKernel
    cuFuncGetAttribute
    cuMemcpyDtoHAsync_v2
    cuStreamCreate
    cuMemsetD8Async
    cuEventCreate
    cuMemHostAlloc
    cuMemHostGetDevicePointer_v2
    cuMemFree_v2
    cuEventRecord
    cuStreamGetPriority
    cuCtxGetStreamPriorityRange
    cuMemcpyDtoDAsync_v2
    cuOccupancyMaxActiveBlocksPerMultiprocessorWithFlags
    cuModuleUnload
    cuDevicePrimaryCtxRelease

cuda list (12) =
    cudaGetDeviceCount
    cudaGetDevice
    cudaGetDeviceProperties
    cudaGetLastError
    cudaStreamIsCapturing
    cudaMalloc
    cudaMemcpyAsync
    cudaStreamSynchronize
    cudaLaunchKernel
    cudaFuncGetAttributes
    cudaPeekAtLastError
    cudaDeviceGetAttribute

cudnn list (9) =
    cudnnCreate
    cudnnSetStream
    cudnnBackendCreateDescriptor
    cudnnBackendSetAttribute
    cudnnBackendFinalize
    cudnnGetModuleForDescriptor
    cudnnBackendDestroyDescriptor
    cudnnBackendGetAttribute
    cudnnBackendExecute

cublas list (5) =
    cublasCreate_v2
    cublasSetStream_v2
    cublasSetWorkspace_v2
    cublasSetMathMode
    cublasSgemm_v2

cublasLt list (4) =
    cublasLtCreate
    cublasLtCtxInit
    cublasLtSSSMatmulAlgoGetHeuristic
    cublasLtSSSMatmul
'''