# compiler_directives
# https://stackoverflow.com/questions/34603628/how-to-specify-python-3-source-in-cythons-setup-py
# https://cython.readthedocs.io/en/latest/src/userguide/source_files_and_compilation.html

from distutils.core import setup  
from distutils.extension import Extension  
from Cython.Build import cythonize  

extensions = [
    Extension(
        "cy_thread", 
        ["cy_thread.pyx"], 
        libraries=["thread"],
        library_dirs=["."],
        language="c++",
    ),
]

setup(
    ext_modules = cythonize(
        extensions,
        compiler_directives={'language_level' : "3"}
    )
)
