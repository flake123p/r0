#ifndef __HELLO_WORLD_H__
#define __HELLO_WORLD_H__

#include <stdint.h>

typedef struct {
    uint32_t id;
    union {
        uint32_t age;
        uint16_t a[2];
    } u;
} id_ct;

int hello_world(id_ct x);

#endif//__HELLO_WORLD_H__