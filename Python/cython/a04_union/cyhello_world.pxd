# https://github.com/cython/cython/blob/master/Cython/Includes/libc/stdint.pxd

cdef extern from "<stdint.h>":
    ctypedef   signed char  int8_t
    ctypedef   signed short int16_t
    ctypedef   signed int   int32_t
    ctypedef   signed long  int64_t
    ctypedef unsigned char  uint8_t
    ctypedef unsigned short uint16_t
    ctypedef unsigned int   uint32_t
    ctypedef unsigned long long uint64_t
    # 7.18.1.2 Minimum-width integer types
    ctypedef   signed char  int_least8_t
    ctypedef   signed short int_least16_t
    ctypedef   signed int   int_least32_t
    ctypedef   signed long  int_least64_t
    ctypedef unsigned char  uint_least8_t
    ctypedef unsigned short uint_least16_t
    ctypedef unsigned int   uint_least32_t
    ctypedef unsigned long long uint_least64_t
    # 7.18.1.3 Fastest minimum-width integer types
    ctypedef   signed char  int_fast8_t
    ctypedef   signed short int_fast16_t
    ctypedef   signed int   int_fast32_t
    ctypedef   signed long  int_fast64_t
    ctypedef unsigned char  uint_fast8_t
    ctypedef unsigned short uint_fast16_t
    ctypedef unsigned int   uint_fast32_t
    ctypedef unsigned long long uint_fast64_t

cdef extern from "hello_world.h":
    ctypedef union dummy:
        uint32_t age
        uint16_t a[2]
    ctypedef struct id_ct:
        uint32_t id
        dummy u

    #ctypedef id_ctcy id_ct # must use same name as C

    int hello_world(id_ct x)
