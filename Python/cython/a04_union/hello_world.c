#include <stdio.h>
#include "hello_world.h"

int hello_world(id_ct x){
  printf("Hello World, id  = %d\n",x.id);
  printf("Hello World, age = %d\n",x.u.age);
  printf("Hello World, a0  = %d\n",x.u.a[0]);
  printf("Hello World, a1  = %d\n",x.u.a[1]);
  return x.u.age + 1;
}

