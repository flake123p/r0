cimport cyhello_world

def hello_world_cy_lite():
   print('This is cython lite.')
   cdef id_ct mary
   mary.id = 11
   mary.u.age = 65536 + 77
   return hello_world(mary)

def hello_world_cy(input):
   print('This is cython wrapper.')
   cdef id_ct mary
   mary.id = input['id']
   if 'age' in input:
      mary.u.age = input['age']
   else:
      if 'a0' in input:
         mary.u.a[0] = input['a0']
      if 'a1' in input:
         mary.u.a[1] = input['a1']
   return hello_world(mary)