#import cython
from cyhello_world import *

hello_world_cy_lite()

d = {}
d.update({'id': 7})
d.update({'age': 888})
hello_world_cy(d)

d = {}
d.update({'id': 99})
d.update({'a0': 10000})
d.update({'a1': 1})
hello_world_cy(d)

