#ifndef __HELLO_WORLD_H__
#define __HELLO_WORLD_H__

#include <stdint.h>

typedef void (*cb_t)(void);

int hello_world(cb_t x);

#endif//__HELLO_WORLD_H__