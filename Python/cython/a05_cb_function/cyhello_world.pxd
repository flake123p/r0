#cdef extern from "stdio.h":
#    printf(char* string)

cdef extern from "hello_world.h":
    ctypedef void (*cb_t)()
    int hello_world(cb_t x)
