cimport cyhello_world

g_cb = None

cdef void cython_callback():
    global g_cb
    print('cython_callback start')
    d = {}
    d.update({'id':123})
    d.update({'ret':456})
    g_cb(d)
    print('cython_callback end')

def hello_world_cy_lite(input):
    print('This is cython lite.')
    d = {}
    input(d)

def hello_world_cy(input):
    global g_cb
    print('This is cython wrapper.')
    g_cb = input
    return hello_world(cython_callback)