#import cython
from cyhello_world import *

def python_callback(msg):
    print('python_callback:')
    if 'id' in msg:
        print('id = ', msg['id'])
    if 'ret' in msg:
        print('ret = ', msg['ret'])

hello_world_cy_lite(python_callback)

hello_world_cy(python_callback)
