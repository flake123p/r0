cimport cyhello_world

def hello_world_cy_lite():
   print('This is cython lite.')
   cdef id_ct mary = id_ct(10, 999)
   return hello_world(mary)

def hello_world_cy(input):
   print('This is cython wrapper.')
   cdef id_ct mary
   mary.id = input['id']
   mary.age = input['age']
   return hello_world(mary)