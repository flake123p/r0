cimport cyhello_world

cdef int ret = 500

def hello_world_cy_lite():
   global ret
   print('This is cython lite.')
   cdef id_ct mary
   mary.id = 10
   mary.age = 999
   mary.p_ret = &ret
   ret = ret + 1
   return hello_world(&mary)

def hello_world_cy(input):
   global ret
   print('This is cython wrapper.')
   cdef id_ct mary
   mary.id = input['id']
   mary.age = input['age']
   mary.p_ret = &ret
   ret = ret + 1
   return hello_world(&mary)