#include <stdio.h>
#include "hello_world.h"

int hello_world(id_ct *x){
  printf("Hello World, id = %d\n", x->id);
  printf("Hello World, age = %d\n", x->age);
  printf("Hello World, ret = %d\n", *(x->p_ret));
  return x->age + 1;
}

