# compiler_directives
# https://stackoverflow.com/questions/34603628/how-to-specify-python-3-source-in-cythons-setup-py
# https://cython.readthedocs.io/en/latest/src/userguide/source_files_and_compilation.html

from distutils.core import setup  
from distutils.extension import Extension  
from Cython.Build import cythonize  

extensions = [
    Extension(
        "cyhello_world", 
        ["cyhello_world.pyx"], 
        libraries=["hello_world"],
        library_dirs=["."],
    ),
]

setup(
    ext_modules = cythonize(
        extensions,
        compiler_directives={'language_level' : "3"}
    )
)
