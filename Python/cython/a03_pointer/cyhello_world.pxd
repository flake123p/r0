cdef extern from "hello_world.h":
    ctypedef struct id_ct:
        int id
        int age
        int *p_ret
    #ctypedef id_ctcy id_ct # must use same name as C

    int hello_world(id_ct *x)
