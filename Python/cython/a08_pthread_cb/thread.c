#include <stdio.h>
#include <stdlib.h>
#include "thread.h"

int g_LibThread_Ctr;

typedef struct {
	pthread_t thread;
	THREAD_PRIORITY_t priority;
}THREAD_HANDLE_LINUX_t;
int LibThread_NewHandle(THREAD_HANDLE_t *threadHdlPtr, THREAD_PRIORITY_t priority /* = TPRI_DEFAULT */)
{
	THREAD_HANDLE_LINUX_t *linuxThreadHdl = (THREAD_HANDLE_LINUX_t *)malloc(sizeof(THREAD_HANDLE_LINUX_t));
	if (linuxThreadHdl == NULL)
		return -1;

	linuxThreadHdl->priority = priority;
	*threadHdlPtr = linuxThreadHdl;
	g_LibThread_Ctr++;
	return 0;
}

int LibThread_Create(THREAD_HANDLE_t threadHdl, ThreadEntryFunc entry, void *arg /* = NULL */)
{
	THREAD_HANDLE_LINUX_t *linuxThreadHdl = (THREAD_HANDLE_LINUX_t *)threadHdl;

	int retVal = pthread_create(&linuxThreadHdl->thread, NULL, entry, arg);

	return retVal;
}

int LibThread_WaitThread(THREAD_HANDLE_t threadHdl)
{
	THREAD_HANDLE_LINUX_t *linuxThreadHdl = (THREAD_HANDLE_LINUX_t *)threadHdl;

	/* wait for the thread to finish */
	int retVal = pthread_join(linuxThreadHdl->thread, NULL);

	return retVal;
}
int LibThread_DestroyHandle(THREAD_HANDLE_t threadHdl)
{
	free(threadHdl);
	g_LibThread_Ctr--;
	return 0;
}

int LibThread_GetCounter(void)
{
	return g_LibThread_Ctr;
}

THREAD_HANDLE_t hdl;
static void *test_thread(void *arg)
{
	printf("hello test_thread 1\n");
	usleep(500*1000);
	printf("hello test_thread 2\n");
	usleep(500*1000);
	printf("hello test_thread 3\n");
	return NULL;
}

void demo()
{
	LibThread_NewHandle(&hdl, TPRI_M);
	LibThread_Create(hdl, test_thread, hdl);
	LibThread_WaitThread(hdl);
	LibThread_DestroyHandle(hdl);
}

void My_SleepMs(int ms)
{
	usleep(ms*1000);
}

int main()
{
	printf("hello thread\n");
	demo();
	return 0;
}