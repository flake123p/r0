# C++ object + Python object
# https://stackoverflow.com/questions/34793226/cython-class-with-an-attributes-as-python-object-instance

cimport cy_thread

gcb1 = None
gcb2 = None

cdef void *ccb_1(void *arg):
   My_SleepMs(500) # looks like python function is not thread safe ...
   d = dict()
   d.update({'id':123})
   d.update({'ret':456})
   if gcb1 != None:
      gcb1(d)

cdef void *ccb_2(void *arg):
   d = dict()
   d.update({'id':7777})
   d.update({'ret':8888})
   if gcb2 != None:
      gcb2(d)
   
class py_cb(object):
   def __init__(self, cb):
      self.cb = cb

cdef class cy_thread:
   
   # cdef public:
   #    object cb1, cb2

   cdef THREAD_HANDLE_t hdl1
   cdef THREAD_HANDLE_t hdl2

   def __cinit__(self):
      #print("__cinit__, this is before python __init__()")
      LibThread_NewHandle(&self.hdl1, TPRI_M)
      LibThread_NewHandle(&self.hdl2, TPRI_M)
      pass

   def __dealloc__(self):
      #print("__dealloc__")
      LibThread_DestroyHandle(self.hdl1)
      LibThread_DestroyHandle(self.hdl2)
      pass

   def __init__(self, cb1, cb2):
      global gcb1, gcb2
      gcb1 = cb1
      gcb2 = cb2
      #print("__init__")
      pass


   def go(self):
      LibThread_Create(self.hdl1, ccb_1, self.hdl1)
      LibThread_Create(self.hdl2, ccb_2, self.hdl2)
      LibThread_WaitThread(self.hdl1)
      LibThread_WaitThread(self.hdl2)
      pass
