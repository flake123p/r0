#ifndef __THREAD_H__
#define __THREAD_H__

// ====== Standard C/Cpp Library ======
#include <stdio.h>
#include <string.h>
#include <stdint.h> // for uint32_t, ...
#include <stdlib.h> // for exit(), ...

// ====== Standard Linux Library ======
#include <sys/types.h>
#include <sys/ipc.h> // for shared memory, ...
#include <sys/shm.h> // for shared memory, ...
#include <sys/time.h> // for gettimeofday(), ...
#include <unistd.h> // for sleep(), ...
#include <pthread.h>

typedef void * THREAD_HANDLE_t;
typedef enum {
	TPRI_HH,
	TPRI_H,
	TPRI_M,
	TPRI_L,
	TPRI_LL,

	TPRI_DEFAULT,
} THREAD_PRIORITY_t;
typedef void *(*ThreadEntryFunc)(void *);
int LibThread_NewHandle(THREAD_HANDLE_t *threadHdlPtr, THREAD_PRIORITY_t priority);
int LibThread_Create(THREAD_HANDLE_t threadHdl, ThreadEntryFunc entry, void *arg);
int LibThread_WaitThread(THREAD_HANDLE_t threadHdl);
int LibThread_DestroyHandle(THREAD_HANDLE_t threadHdl);
int LibThread_GetCounter(void);

void My_SleepMs(int ms);
void demo();

#endif//__THREAD_H__