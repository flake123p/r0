cdef extern from "thread.h":
    void demo()
    ctypedef void * THREAD_HANDLE_t
    ctypedef enum THREAD_PRIORITY_t:
        TPRI_HH
        TPRI_H
        TPRI_M
        TPRI_L
        TPRI_LL
        TPRI_DEFAULT
    ctypedef void *(*ThreadEntryFunc)(void *)
    int LibThread_NewHandle(THREAD_HANDLE_t *threadHdlPtr, THREAD_PRIORITY_t priority)
    int LibThread_Create(THREAD_HANDLE_t threadHdl, ThreadEntryFunc entry, void *arg)
    int LibThread_WaitThread(THREAD_HANDLE_t threadHdl)
    int LibThread_DestroyHandle(THREAD_HANDLE_t threadHdl)
    void My_SleepMs(int ms)
