#import cython
from cy_thread import *

def py_callback_1(msg):
    print('python_callback 1:')
    if 'id' in msg:
        print('id = ', msg['id'])
    if 'ret' in msg:
        print('ret = ', msg['ret'])

def py_callback_2(msg):
    print('python_callback 2:')
    if 'id' in msg:
        print('id = ', msg['id'])
    if 'ret' in msg:
        print('ret = ', msg['ret'])

obj = cy_thread(py_callback_1, py_callback_2)
obj.go()