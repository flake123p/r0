#ifndef __HELLO_WORLD_H__
#define __HELLO_WORLD_H__

#include <stdint.h>

typedef void (*cb_t)(void);

void *hell_alloc(int size);
int hell_new(void **p_hdl, int priority, char *name, void *buf);
int hell_dump(void *hdl);
int hell_delete(void *hdl);

void *hell_get_buf(void *hdl);
int hell_get_buf_len(void *hdl);
void hell_set_prompt(void *hdl, char *input, int len);

#endif//__HELLO_WORLD_H__