# https://cython.readthedocs.io/en/latest/src/userguide/wrapping_CPlusPlus.html

cimport cyhello_world

cdef class cy_hell:
    cdef void *hdl

    def __cinit__(self):
        print("__cinit__")
        
    def __dealloc__(self):
        print("__dealloc__")
        hell_delete(self.hdl)

    def __init__(self, priority, name):
        print("init")
        print('priority = ', priority)
        print('name = ', name)
        cdef int pri
        cdef char *na
        
        # string copy
        py_byte_string = name.encode('ASCII')
        na = <char *>hell_alloc(len(py_byte_string)+1)
        na[len(py_byte_string)] = 0
        for i in range(len(py_byte_string)):
            na[i] = py_byte_string[i]
        
        # priority copy
        pri = priority

        buf = <void *>hell_alloc(1024)

        hell_new(&self.hdl, pri, na, buf)
    
    def dump(self):
        hell_dump(self.hdl)
    
    def __del__(self):
        print("del")

    def set_prompt(self, prompt):
        # cdef char *na = <char *>hell_get_buf(self.hdl)

        py_byte_string = prompt.encode('ASCII')
        # na = <char *>hell_alloc(len(py_byte_string)+1)
        # na[len(py_byte_string)] = 0
        # for i in range(len(py_byte_string)):
        #     na[i] = py_byte_string[i]
        
        hell_set_prompt(self.hdl, py_byte_string, len(py_byte_string))
    
    def get_string(self):
        output = ''

        cdef char *buf = <char *>hell_get_buf(self.hdl)
        cdef int buf_len = hell_get_buf_len(self.hdl)

        print('buf_len =', buf_len)

        # output = str(buf)
        # print('output type =', type(output))
        for i in range(buf_len):
            output += chr(buf[i])

        return output
        
