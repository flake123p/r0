#include <stdio.h>
#include <stdlib.h>
#include "hello_world.h"
#include <string.h>

typedef struct {
    int priority;
    int b;
    int c;
    char *name; // note !! this will auto-free
    void *buf;
    int buf_len;
} inside_t;

void *hell_alloc(int size)
{
    return malloc(size);
}

int hell_new(void **p_hdl, int priority, char *name, void *buf)
{
    inside_t *hdl = (inside_t *)malloc(sizeof(inside_t));
    hdl->priority = priority;
    hdl->b = 11;
    hdl->c = 22;
    hdl->name = name;
    hdl->buf = buf;
    hdl->buf_len = 0;
    *p_hdl = (void *)hdl;
    return 0;
}

int hell_dump(void *hdl)
{
    inside_t *in_hdl = (inside_t *)hdl;
    printf("%s ...\n", __func__);
    printf("name = %s, priority = %d, b = %d, c = %d\n", in_hdl->name, in_hdl->priority, in_hdl->b, in_hdl->c);
    return 0;
}

int hell_delete(void *hdl)
{
    inside_t *in_hdl = (inside_t *)hdl;
    printf("%s ...\n", __func__);
    if (in_hdl->name != NULL) {
        free(in_hdl->name);
    }
    if (in_hdl->buf != NULL) {
        free(in_hdl->buf);
    }
    free(hdl);

    return 0;
}

void *hell_get_buf(void *hdl)
{
    inside_t *in_hdl = (inside_t *)hdl;
    return in_hdl->buf;
}

int hell_get_buf_len(void *hdl)
{
    inside_t *in_hdl = (inside_t *)hdl;
    return in_hdl->buf_len;
}

void hell_set_prompt(void *hdl, char *input, int len)
{
    inside_t *in_hdl = (inside_t *)hdl;

    in_hdl->buf_len = len;

    memcpy(in_hdl->buf, input, len);
    ((char *)in_hdl->buf)[len] = 0;

    printf("%s() : %s\n", __func__, (char *)in_hdl->buf);
}