gcc -c hello_world.c
gcc -shared -o libhello_world.so hello_world.o -lm

python3 setup.py build_ext -i

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:.
python3 test.py
