from distutils.core import setup  
from distutils.extension import Extension  
from Cython.Build import cythonize  

setup(
ext_modules = cythonize([Extension("cyhello_world", 
                                   ["cyhello_world.pyx"], 
                                   libraries=["hello_world"],
                                   library_dirs=["."]
                                   )])
)
