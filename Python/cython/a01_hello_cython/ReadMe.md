# Cython hello world


Add hello_world.c
```
#include <stdio.h>
#include "hello_world.h"

int hello_world(int x){
  printf("Hellow World, x = %d\n",x);
  return x + 1;
}
```

Add hello_world.h
```
int hello_world(int x);
```

Compile C file
```
gcc -c hello_world.c 
gcc -shared -o libhello_world.so hello_world.o -lm
```

Add cyhello_world.pxd
```
cdef extern from "hello_world.h":  
    int hello_world(int x)
```

Add cyhello_world.pyx
```
cimport cyhello_world

def hello_world_c(int x):  
   print('This is cython wrapper.')
   return hello_world(x)
```

Add setup.py
```
from distutils.core import setup  
from distutils.extension import Extension  
from Cython.Build import cythonize  

setup(
ext_modules = cythonize([Extension("cyhello_world", 
                                   ["cyhello_world.pyx"], 
                                   libraries=["hello_world"],
                                   library_dirs=["."]
                                   )])
)
```

Build Cython file
```
python setup.py build_ext -i
```

Add test.py
```
from cyhello_world import *

hello_world_c(9)
```

Run test.py
```
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:.
python test.py
```

Result:
```
This is cython wrapper.
Hello World, x = 9
```

Done.

==============================================

Ref.:
https://cython.readthedocs.io/en/latest/src/tutorial/clibraries.html

==============================================
p.s.
Use ==PYTHONPATH== when it is necessary.
```
export PYTHONPATH=.
```
