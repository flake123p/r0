#include <stdio.h>
#include <stdlib.h>
#include "hello_world.h"

typedef struct {
    int priority;
    int b;
    int c;
    char *name; // note !! this will auto-free
} inside_t;

void *hell_alloc(int size)
{
    return malloc(size);
}

int hell_new(void **p_hdl, int priority, char *name)
{
    inside_t *hdl = (inside_t *)malloc(sizeof(inside_t));
    hdl->priority = priority;
    hdl->b = 11;
    hdl->c = 22;
    hdl->name = name;
    *p_hdl = (void *)hdl;
    return 0;
}

int hell_dump(void *hdl)
{
    inside_t *in_hdl = (inside_t *)hdl;
    printf("%s ...\n", __func__);
    printf("name = %s, priority = %d, b = %d, c = %d\n", in_hdl->name, in_hdl->priority, in_hdl->b, in_hdl->c);
    return 0;
}

int hell_delete(void *hdl)
{
    inside_t *in_hdl = (inside_t *)hdl;
    printf("%s ...\n", __func__);
    if (in_hdl->name != NULL) {
        free(in_hdl->name);
    }
    free(hdl);

    return 0;
}