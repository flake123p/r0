#cdef extern from "stdio.h":
#    printf(char* string)

cdef extern from "hello_world.h":
    ctypedef void (*cb_t)()
    #int hello_world(cb_t x)
    void *hell_alloc(int size)
    int hell_new(void **p_hdl, int priority, char *name)
    int hell_dump(void *hdl)
    int hell_delete(void *hdl)