# https://cython.readthedocs.io/en/latest/src/userguide/wrapping_CPlusPlus.html

cimport cyhello_world

cdef class cy_hell:
    cdef void *hdl

    def __cinit__(self):
        print("__cinit__")
        
    def __dealloc__(self):
        print("__dealloc__")
        hell_delete(self.hdl)

    def __init__(self, priority, name):
        print("init")
        print('priority = ', priority)
        print('name = ', name)
        cdef int pri
        cdef char *na
        
        # string copy
        py_byte_string = name.encode('ASCII')
        na = <char *>hell_alloc(len(py_byte_string)+1)
        na[len(py_byte_string)] = 0
        for i in range(len(py_byte_string)):
            na[i] = py_byte_string[i]
        
        # priority copy
        pri = priority
        hell_new(&self.hdl, pri, na)
    
    def dump(self):
        hell_dump(self.hdl)
    
    def __del__(self):
        print("del")
        
