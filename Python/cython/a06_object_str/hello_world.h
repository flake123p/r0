#ifndef __HELLO_WORLD_H__
#define __HELLO_WORLD_H__

#include <stdint.h>

typedef void (*cb_t)(void);

void *hell_alloc(int size);
int hell_new(void **p_hdl, int priority, char *name);
int hell_dump(void *hdl);
int hell_delete(void *hdl);

#endif//__HELLO_WORLD_H__