import numpy as np

A = np.array([[1, 2], [3, 4]]) 
B = np.array([[10], [22]]) 

detA = np.linalg.det(A)
print("linalg.det   A   = \n", detA)
print("linalg.solve A B = \n", np.linalg.solve(A, B))


A = np.array([[1, 2], [2, 4]]) 
B = np.array([[10], [22]]) 

detA = np.linalg.det(A)
print("linalg.det   A   = \n", detA)
if detA != 0:
    print("linalg.solve A B = \n", np.linalg.solve(A, B))
