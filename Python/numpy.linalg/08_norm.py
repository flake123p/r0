import numpy as np

point0 = np.array([[1, -1, 1]])

print("point0 norm      = \n", np.linalg.norm(point0)) 

print("point0 normalize = \n", point0 / np.linalg.norm(point0)) 
