import numpy as np

A0 = np.array([[3, 1]]) # on basis0
A1 = np.array([[2, 1]])

# Mi x N x M
Basis0 = np.transpose(np.array([[1, 0], [0, 1]]))
Basis1 = np.transpose(np.array([[1, 1], [1, -1]]))

print("linalg.solve Basis0 A0 = \n", np.linalg.solve(Basis0, np.transpose(A0)))

print("linalg.solve Basis1 A0 = \n", np.linalg.solve(Basis1, np.transpose(A0)))

#
# Transition Matrix = NewMat.inv x OldMat
#
M01 = np.matmul(np.linalg.inv(Basis1), Basis0)
print('M01 & A0 to Basis 0:')
print(M01)
print(np.matmul(M01, np.transpose(A0)))

M10 = np.matmul(np.linalg.inv(Basis0), Basis1)
print('M10 & A1 to Basis 1:')
print(M10)
print(np.matmul(M10, np.transpose(A1)))