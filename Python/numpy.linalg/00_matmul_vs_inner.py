import numpy as np

a = np.array([1, 2]) 
b = np.array([5, 7]) 

print("inner a b = ", np.inner(a, b))

A = np.array([[1, 2], [3, 4]]) 
B = np.array([[5, 6], [7, 8]]) 

print("matmul A B = \n", np.matmul(A, B))