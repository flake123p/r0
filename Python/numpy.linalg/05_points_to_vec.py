import numpy as np

#
# ax + b = y, solve a & b
#
# Points = (1,0) =  a + b = 0
#      and (0,1) = 0a + b = 1
#
M = np.array([
    [1, 1],
    [0, 1]])
Y = np.array([[0, 1]])

Answer = np.linalg.solve(M, np.transpose(Y))

print("Answer= \n", Answer) # (-1, 1) => y = -x + 1
