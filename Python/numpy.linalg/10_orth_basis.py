import numpy as np

v1 = np.array([1, 1])
v2 = np.array([1, -1])

print("inner v1 v2 = ", np.inner(v1, v2))


# https://datascienceparichay.com/article/numpy-check-if-a-matrix-is-orthogonal/
V = np.array([[1, 1],[1,-1]])
V= V / np.sqrt(2)

# dot product of matrix and its transpose
dot_product = np.dot(V, V.T)

# create an identity matrix of the same shape as ar1
identity_matrix = np.identity(len(V))

# check if matrix is orthogonal
print(np.allclose(dot_product, identity_matrix))

# Another method
print(np.allclose(V.T, np.linalg.inv(V)))