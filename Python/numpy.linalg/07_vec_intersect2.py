import numpy as np

point0 = np.array([[2, 2]])
point1 = np.array([[2, 1]])

#
# Camera1.Y = Camera0.Y + 3 ... 
#
def Convert1to0(point):
    cam1to0 = np.array([
        [1, 0, 0], # bx
        [0, 1, 3], # by
        [0, 0, 1]
    ])
    point = np.append(point, 1.)
    point = np.matmul(cam1to0, point)
    point = point[0:2]
    return point

def GetObjPostion(p0, p1):
    p1ori = Convert1to0(np.array([[0, 0]]))
    p1 = Convert1to0(p1)
    print('mapped p1ori =', p1ori)
    print('mapped p1    =', p1)
    return [0, 0]


print("GetObjPostion = \n", GetObjPostion(point0, point1)) 
