import numpy as np

Basis0 = np.array([[1, 0], [0, 1]]) 
A = np.array([[1, 3]]) 

print("linalg.solve Basis0 A = \n", np.linalg.solve(np.transpose(Basis0), np.transpose(A)))


Basis1 = np.array([[1, 1], [0, 1]]) 


print("linalg.solve Basis1 A = \n", np.linalg.solve(np.transpose(Basis1), np.transpose(A)))

Basis2 = np.array([[1, 0], [0, 0]]) 


print("linalg.solve Basis2 A = \n", np.linalg.solve(np.transpose(Basis2), np.transpose(A)))