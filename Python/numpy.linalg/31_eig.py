import numpy as np
from numpy import linalg as LA

#
# y = ax
#
x = np.array([
    [4, 0], 
    [0, 1]
]) 
eigenvalues, eigenvectors = LA.eig(x)
print('eigenvalues  =', eigenvalues)
print('eigenvectors =', eigenvectors)


#
# y = ax
#
x = np.array([
    [2, 0], 
    [0, 3]
]) 
eigenvalues, eigenvectors = LA.eig(x)
print('eigenvalues  =', eigenvalues)
print('eigenvectors =', eigenvectors)