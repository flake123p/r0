import numpy as np

#
# y = -x + 1
# y = 0x - 1
#
M = np.array([
    [-1, -1],
    [0, -1]])
Constants = np.array([[-1, 1]])

IntersectXY = np.linalg.solve(M, np.transpose(Constants))

print("IntersectXY = \n", IntersectXY) 
