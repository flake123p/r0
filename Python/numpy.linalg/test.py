#
# https://stackabuse.com/pythons-classmethod-and-staticmethod-explained/
#
import numpy as np

a = np.array([1, 2, 3, 4])     #一維陣列建立
b = np.array([(2.5, 1, 3, 4.5), (5, 6, 7, 8)], dtype = float)  #二維陣列建立
c = np.array([[(2.5, 1, 3, 4.5), (5, 6, 7, 8)], [(2.5, 1, 3, 4.5), (5, 6, 7, 8)]], dtype = float)  #三維陣列建立
