import numpy as np

#
# y = ax
#
x = np.array([
    [1, 1], 
    [3, 1]
]) 
a = np.array([
    [2, 0], 
    [0, 1]
])
y = np.matmul(a, np.transpose(x))

print("yt = \n", np.transpose(y))

#
# y = ax + b
#
x = np.array([
    [1, 1, 1], 
    [3, 1, 1]
]) 
a = np.array([
    [1, 0, 2], # bx
    [0, 1, 0], # by
    [0, 0, 1]
])
y = np.matmul(a, np.transpose(x))

print("yt = \n", np.transpose(y))

#
# manual affine
#
print("1st inner = \n", np.inner(x[0], a[0]))
print("2nd inner = \n", np.inner(x[0], a[1]))