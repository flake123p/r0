import numpy as np

p1 = np.array([[1, -1, -1]])
p2 = np.array([[1, 1, -1]])


print("concat axis 0 = \n", np.concatenate((p1, p2), axis=0))
print("concat axis 1 = \n", np.concatenate((p1, p2), axis=1))

