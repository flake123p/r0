import numpy as np

A = np.array([[1, 2], [3, 4]]) 

iA = np.linalg.inv(A)
print("linalg.inv   A   = \n", iA)

print("A x iA   = \n", np.matmul(A, iA))

print("A x iA   = \n", np.matmul(iA, A))