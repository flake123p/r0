#
# https://note.nkmk.me/en/python-opencv-imread-imwrite/
#

import cv2
import numpy as np

#
# Read Image
#
img = cv2.imread('pic/Lenna.jpg')

#img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

print('shape = ', img.shape)

overlay = np.ones(img.shape[:-1], dtype=img.dtype)

print('shape = ', overlay.shape)

if 0:
    pts = [
        [157, 157],
        [157, 158],
        [158, 157],
        [158, 158],
        ]
else:
    pts = [
        [300, 300]
        ]

for gaze_point in pts:
    try:
        #print('x = ', int(gaze_point[1]), ', y = ', int(gaze_point[0]))
        #print('1', overlay[int(gaze_point[1]), int(gaze_point[0])])
        overlay[int(gaze_point[1]), int(gaze_point[0])] = 0
        #print('2', overlay[int(gaze_point[1]), int(gaze_point[0])])
    except Exception:
        pass

out = cv2.distanceTransform(overlay, cv2.DIST_L2, 5)

#
# Write Image
#
cv2.imwrite('out.jpg', out)

# fix for opencv binding inconsitency
if type(out) == tuple:
    out = out[0]

falloff = 20

overlay = 1 / (out / falloff + 1)

#print(overlay)

img[:] = np.multiply(
    img, cv2.cvtColor(overlay, cv2.COLOR_GRAY2RGB), casting="unsafe"
)

cv2.imwrite('vis_light.jpg', img)

'''
cv2.imshow('My Image', img)

while True:
    key = cv2.waitKey(1)
    if key == 27: #ESC
        break

cv2.destroyAllWindows()
'''