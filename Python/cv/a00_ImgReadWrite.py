#
# https://note.nkmk.me/en/python-opencv-imread-imwrite/
#

import cv2
import numpy as np

#
# Read Image
#
img = cv2.imread('pic/eye.jpg')

# get dimensions of image
dimensions = img.shape
 
# height, width, number of channels in image
height = img.shape[0]
width = img.shape[1]
channels = img.shape[2]
 
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

#
# Write Image
#
#cv2.imwrite('gray.jpg', img)

print('type of img = ', type(img))

while True:
    cv2.imshow('My Image', img)
    key = cv2.waitKey(1)
    if key == 27: #ESC
        break

cv2.destroyAllWindows()

print('Image Dimension    : ',dimensions)
print('Image Height       : ',height)
print('Image Width        : ',width)
print('Number of Channels : ',channels)