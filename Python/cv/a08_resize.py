#
# https://www.tutorialkart.com/opencv/python/opencv-python-resize-image/
#
import cv2
 
img = cv2.imread('pic/Lenna.jpg', cv2.IMREAD_UNCHANGED)
 
print('Original Dimensions : ',img.shape)
 
scale_percent = 160 # percent of original size
width = int(img.shape[1] * scale_percent / 100)
height = int(img.shape[0] * scale_percent / 100)
dim = (width, height)
  
# resize image
resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
 
print('Resized Dimensions : ',resized.shape)
 
while True:
    cv2.imshow('My Image', resized)
    key = cv2.waitKey(1)
    if key == 27: #ESC
        break

cv2.destroyAllWindows()