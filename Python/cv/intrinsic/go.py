
import numpy as np
import cv2
import cam

resolution = [640, 480]

dummy64 = cam.Dummy_Model([640, 480])

image_points = []

dummy64.setFocalLength(200)
image_points.append(dummy64.projectPoint([0,5,200]))

for i in image_points:
    print(i)

pts_3d = dummy64.unprojectPoints(image_points[0])
print('un-re proj =', dummy64.projectPoint(pts_3d))

'''
### ==========================================================
### ============================= big on near / small on far ============================= ###
### ==========================================================
# move in z axis at principle point = no projection change
image_points.append(dummy64.projectPoint([0,0,10])) #[320. 240.]
image_points.append(dummy64.projectPoint([0,0,11])) #[320. 240.]
image_points.append(dummy64.projectPoint([0,0,12])) #[320. 240.]

# move in z axis : big on near / small on far
image_points.append(dummy64.projectPoint([0,1,10])) #[320.         340.]
image_points.append(dummy64.projectPoint([0,1,11])) #[320.         330.90909091]
image_points.append(dummy64.projectPoint([0,1,12])) #[320.         323.33333333]

### ==========================================================
### ============================= z is focal length / image plane ============================= ###
### ==========================================================
#
# focal length = 1,2,10,100,1000
#
# object_point = [0,1,0]
#   [320. 241.]  +1
#   [320. 242.]  +2
#   [320. 250.]  +10
#   [320. 340.]  +100
#   [320. 1240.] +1000
dummy64.setFocalLength(1)
image_points.append(dummy64.projectPoint(object_point))
dummy64.setFocalLength(2)
image_points.append(dummy64.projectPoint(object_point))
dummy64.setFocalLength(10)
image_points.append(dummy64.projectPoint(object_point))
dummy64.setFocalLength(100)
image_points.append(dummy64.projectPoint(object_point))
dummy64.setFocalLength(1000)
image_points.append(dummy64.projectPoint(object_point))

### ==========================================================
### ============================= eye ball center movement (must minus radius) ============================= ###
### ==========================================================
dummy64.setFocalLength(1)
# eye ball center move 1
image_points.append(dummy64.projectPoint([0,2,5]))
image_points.append(dummy64.projectPoint([0,9,5]))
# eye ball center minus radius 3 & move 1
image_points.append(dummy64.projectPoint([0,2,2]))
image_points.append(dummy64.projectPoint([0,9,2]))
#[320.  240.4]
#[320.  240.6] diff:0.2
#[320. 241.]
#[320.  241.5] diff:0.5
#fl_ratio_old = 5/fl = 5
#fl_ratio_new = (5-r)/fl = 2
# !!! diff_new = diff_old * fl_ratio_old / fl_ratio_new !!!
'''