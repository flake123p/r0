import numpy as np
import cv2

class Dummy_Model():
    def __init__(self, resolution):
        self.resolution = resolution
        K = [
            [1000, 0.0, resolution[0] / 2.0],
            [0.0, 1000, resolution[1] / 2.0],
            [0.0, 0.0, 1.0],
        ]
        self.K = np.array(K)
        self.D = np.asarray([[0.0, 0.0, 0.0, 0.0, 0.0]])

    def projectPoint(self, object_point):
        object_points = [
            object_point
        ]
        object_points = np.float64(object_points)
        rvec = np.zeros(3).reshape(1, 1, 3)
        tvec = np.zeros(3).reshape(1, 1, 3)
        image_points, jacobian = cv2.projectPoints(
            object_points, rvec, tvec, self.K, self.D
        )
        return image_points[0][0]

    def unprojectPoints(self, pt_2d, use_distortion=True, normalize=False):
        pts_2d = [pt_2d]
        """
        Undistorts points according to the camera model.
        :param pts_2d, shape: Nx2
        :return: Array of unprojected 3d points, shape: Nx3
        """
        pts_2d = np.array(pts_2d, dtype=np.float32)

        # Delete any posibly wrong 3rd dimension
        if pts_2d.ndim == 1 or pts_2d.ndim == 3:
            pts_2d = pts_2d.reshape((-1, 2))

        # Add third dimension the way cv2 wants it
        if pts_2d.ndim == 2:
            pts_2d = pts_2d.reshape((-1, 1, 2))

        if use_distortion:
            _D = self.D
        else:
            _D = np.asarray([[0.0, 0.0, 0.0, 0.0, 0.0]])

        pts_2d_undist = cv2.undistortPoints(pts_2d, self.K, _D)

        pts_3d = cv2.convertPointsToHomogeneous(pts_2d_undist)
        pts_3d.shape = -1, 3

        if normalize:
            pts_3d /= np.linalg.norm(pts_3d, axis=1)[:, np.newaxis]

        #return pts_3d
        return pts_3d[0]

    def setFocalLength(self, fl):
        self.K[0][0] = fl
        self.K[1][1] = fl
