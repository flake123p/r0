#
# https://www.tutorialkart.com/opencv/python/opencv-python-resize-image/
#
import cv2
 
img = cv2.imread('/home/ai0/sda/logo.png', cv2.IMREAD_UNCHANGED)
 
print('Original Dimensions : ',img.shape)
 
scale_percent = 15 # percent of original size
width = int(img.shape[1] * scale_percent / 100)
height = int(img.shape[0] * scale_percent / 100)
dim = (width, height)
  
# resize image
resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
 
print('Resized Dimensions : ',resized.shape)

x = 0
all = 0
fout = open('write.txt', 'w')

for i in range(resized.shape[0]):
    for j in range(resized.shape[1]):
        p = resized[i][j]
        #print(p)
        if p[3] > 150:
            x = x + 1
            print(j,i)
            string = '['+str(j)+','+str(i)+'],'
            fout.write(string)
        all = all + 1

fout.close()
# while True:
#     cv2.imshow('My Image', resized)
#     key = cv2.waitKey(1)
#     if key == 27: #ESC
#         break

# cv2.destroyAllWindows()