#
# https://experiencor.github.io/segmentation.html
#

import numpy as np
import cv2 as cv

img = cv.imread('pic/001.jpg')
Z = img.reshape((-1,3))
# convert to np.float32
Z = np.float32(Z)
# define criteria, number of clusters(K) and apply kmeans()
criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 10, 1.0)
K = 16
ret,label,center=cv.kmeans(Z,K,None,criteria,10,cv.KMEANS_RANDOM_CENTERS)
center2 = np.uint8(center)
lb_fltn = label.flatten()
res = center2[lb_fltn]

label2 = np.uint8(label)
newImg = label2 * 16
newImg2 = newImg.reshape(400,640,1)
#newImg3 = cv.cvtColor(newImg2, cv.COLOR_GRAY2BGR)

while True:
    cv.imshow('res2',newImg2)
    key = cv.waitKey(1)
    if key == 27: #ESC
        break
cv.destroyAllWindows()

newImg3 = cv.cvtColor(newImg2, cv.COLOR_GRAY2BGR)
cv.imwrite('../001x.jpg', newImg3)
'''
# Now convert back into uint8, and make original image
center_old = center
center = np.uint8(center)
res = center[label.flatten()]
res2 = res.reshape((img.shape))
cv.imshow('res2',res2)
cv.waitKey(0)
cv.destroyAllWindows()
'''