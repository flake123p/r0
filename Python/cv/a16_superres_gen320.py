# https://learnopencv.com/super-resolution-in-opencv/

'''
(base) pupil@pupil-server:~/temp/p1/pupil_p1$ cp pupil_data/pieces/func2/data/data_func2a/0/126965.674726458.jpg /home/pupil/sda/ws/r0/Python/_cv2/pic/
(base) pupil@pupil-server:~/temp/p1/pupil_p1$ cp pupil_data/pieces/func2/data/data_func2a/0/126973.286668160.jpg /home/pupil/sda/ws/r0/Python/_cv2/pic/
(base) pupil@pupil-server:~/temp/p1/pupil_p1$ cp pupil_data/pieces/func2/data/data_func2c/0/146388.626061146.jpg /home/pupil/sda/ws/r0/Python/_cv2/pic/
(base) pupil@pupil-server:~/temp/p1/pupil_p1$ cp pupil_data/pieces/func2/data/data_func2a/0/126976.290559499.jpg /home/pupil/sda/ws/r0/Python/_cv2/pic/
'''
import os
import cv2
import matplotlib.pyplot as plt
# Read image

class SuperResolution(object):
    def __init__(self):
        self.sr = cv2.dnn_superres.DnnSuperResImpl_create()
        path = "EDSR_x2.pb"
        self.sr.readModel(path)
        self.sr.setModel("edsr",2)
    
    def start_160x120_to_216x216(self, inFile, outFile):
        img = cv2.imread(inFile)
        result = self.sr.upsample(img)
        # Resized image
        # resized = cv2.resize(img,dsize=None,fx=216/160,fy=216/120, interpolation=cv2.INTER_NEAREST_EXACT)
        the216 = cv2.resize(result,dsize=None,fx=216/320,fy=216/240, interpolation=cv2.INTER_NEAREST_EXACT)
        cv2.imwrite(outFile, the216)

    def start_x2(self, inFile, outFile):
        img = cv2.imread(inFile)
        print('SR start :', inFile)
        result = self.sr.upsample(img)
        # Resized image
        # resized = cv2.resize(img,dsize=None,fx=216/160,fy=216/120, interpolation=cv2.INTER_NEAREST_EXACT)
        #the216 = cv2.resize(result,dsize=None,fx=216/320,fy=216/240, interpolation=cv2.INTER_NEAREST_EXACT)
        cv2.imwrite(outFile, result)

def batch(path, list_file, sfolder, dfolder):
    super = SuperResolution()
    # read input
    names_file = path + list_file
    with open(names_file, 'r') as f:
        for each_line in f:
            stemp = each_line.replace("\"", "").replace(",", "").replace("\t", "").replace("\n", "")
            dtemp = stemp.replace(".jpg", ".png")
            #print(dtemp)
            sfull = path + sfolder + stemp
            dfull = path + dfolder + dtemp
            if os.path.exists(sfull) == False:
                print("?? file not exists: ", sfull)
                exit()
            super.start_x2(sfull, dfull)
            #break
            

path = "/home/pupil/temp/p1/pupil_p1x/pupil_data/pieces/func2/data/data_func2a/"
list_file = "files.txt"
sfolder = "0/"
dfolder = "0x320/"
batch(path, list_file, sfolder, dfolder)

path = "/home/pupil/temp/p1/pupil_p1x/pupil_data/pieces/func2/data/data_func2b/"
batch(path, list_file, sfolder, dfolder)

path = "/home/pupil/temp/p1/pupil_p1x/pupil_data/pieces/func2/data/data_func2c/"
batch(path, list_file, sfolder, dfolder)