
import cv2
import numpy as np
#from enum import IntEnum, auto, Enum

def get_roi_area_ratio(res, points):
    w_min = res[0]
    w_max = 0
    h_min = res[1]
    h_max = 0
    for p in points:
        if p[0] < w_min:
            w_min = p[0]
        if p[0] > w_max:
            w_max = p[0]
        if p[1] < h_min:
            h_min = p[1]
        if p[1] > h_max:
            h_max = p[1]
    return [w_min, w_max, h_min, h_max], (w_max-w_min)*(h_max-h_min), (w_max-w_min)/(h_max-h_min)


class VisualPoints2D:
    RED    = tuple(reversed((255, 0, 0)))
    GREEN  = tuple(reversed((0, 255, 0)))
    BLUE   = tuple(reversed((0, 0, 255)))
    CYAN   = tuple(reversed((0, 255, 255)))
    YELLOW = tuple(reversed((255, 255, 0)))
    WHITE = tuple(reversed((255, 255, 255)))
    BLACE  = tuple(reversed((0, 0, 0)))
    
    def __init__(self, res, points):
        self.res = res
        self.points = points
        self.amp = 1
        self.roi = []
        self.roi_center = 0
        self.roi_inner_2x2 = []  # [w_mid, y_mid]
        self.roi_inner_3x3 = []  # [w_1_3, w_2_3, y_1_3, y_2_3]
        self.roi_area = 0.0
        self.roi_wh_ratio = 0.0
        self.points_2x2 = []
        self.points_3x3 = []
        self.image = None
        self.reset_back_ground_image()
        self.calc_params()

    def dump(self):
        print('res =', self.res)
        print('amp =', self.amp)
        print('roi =', self.roi)
        print('roi_center =', self.roi_center)
        print('roi_inner_2x2 =', self.roi_inner_2x2)
        print('roi_inner_3x3 =', self.roi_inner_3x3)
        print('roi_area =', self.roi_area)
        print('roi_wh_ratio =', self.roi_wh_ratio)

    def x(self, w):
        return int(w+0.5)

    def y(self, h):
        return self.res[1]-int(h+0.5)-1

    def set_point(self, wh, color):
        self.image[self.y(wh[1]), self.x(wh[0])] = color

    def reset_back_ground_image(self, color=(0, 0, 0)):
        width = self.res[0]
        height = self.res[1]
        self.image = np.zeros((height, width, 3), np.uint8)
        self.image[:] = color
    
    def amplify_image(self, amp=None):
        if amp is None:
            amp = self.amp
        width = self.res[0]
        height = self.res[1]
        self.image = cv2.resize(self.image, (int(width*amp), int(height*amp)), interpolation=cv2.INTER_AREA)

    def save_image(self, file_name='2d_points.png'):
        cv2.imwrite(file_name, self.image)
        
    def calc_params(self):
        self.roi, self.roi_area, self.roi_wh_ratio = get_roi_area_ratio(self.res, self.points)
        self.roi_inner_2x2 = [
            (self.roi[0] + self.roi[1]) / 2,
            (self.roi[2] + self.roi[3]) / 2,
        ]
        w_diff = self.roi[1] - self.roi[0]
        h_diff = self.roi[3] - self.roi[2]
        self.roi_inner_3x3 = [
            self.roi[0] + (w_diff / 3),
            self.roi[0] + (w_diff * 2 / 3),
            self.roi[2] + (h_diff / 3),
            self.roi[2] + (h_diff * 2 / 3),
        ]
        self.roi_center = self.roi_inner_2x2
        self.calc_point_lists_2x2()
        self.calc_point_lists_3x3()
        #
        accu_x = 0
        accu_y = 0
        for p in self.points:
            accu_x += p[0]
            accu_y += p[1]


    def draw_x_line(self, y, color=CYAN, interval=1):
        for i in range(0, self.res[0], interval):
            self.set_point([i, y], color)

    def draw_y_line(self, x, color=CYAN, interval=1):
        for i in range(0, self.res[1], interval):
            self.set_point([x, i], color)

    def draw_roi(self, color=CYAN):
        self.draw_y_line(self.roi[0], color)
        self.draw_y_line(self.roi[1], color)
        self.draw_x_line(self.roi[2], color)
        self.draw_x_line(self.roi[3], color)
        
    def draw_roi_2x2(self, color=RED, interval=2):
        self.draw_y_line(self.roi_inner_2x2[0], color, interval)
        self.draw_x_line(self.roi_inner_2x2[1], color, interval)
        
    def draw_roi_3x3(self, color=RED, interval=2):
        self.draw_y_line(self.roi_inner_3x3[0], color, interval)
        self.draw_y_line(self.roi_inner_3x3[1], color, interval)
        self.draw_x_line(self.roi_inner_3x3[2], color, interval)
        self.draw_x_line(self.roi_inner_3x3[3], color, interval)

    def draw_points(self, points=None, color=YELLOW):
        if points is None:
            points = self.points
        for p in points:
            self.set_point(p, color)

    # use bigger than, NO EQUAL
    def calc_point_lists_2x2(self):
        # w_min, w_max, h_min, h_max = self.roi
        w_mid, y_mid = self.roi_inner_2x2
        li = []
        for p in self.points:
            x, y = p[0], p[1]
            xi = 0
            if x > w_mid:
                xi = 1
            yi = 0
            if y > y_mid:
                yi = 1
            li.append([[xi, yi], p])
            # print([x, y, xi, yi])
        self.points_2x2 = li

    def calc_point_lists_3x3(self):
        # w_min, w_max, h_min, h_max = self.roi
        w_1_3, w_2_3, y_1_3, y_2_3 = self.roi_inner_3x3
        li = []
        for p in self.points:
            x, y = p[0], p[1]
            xi = 0
            if x > w_1_3:
                xi = 1
            if x > w_2_3:
                xi = 2
            yi = 0
            if y > y_1_3:
                yi = 1
            if y > y_2_3:
                yi = 2
            li.append([[xi, yi], p])
        self.points_3x3 = li

    def get_points_in_mid_area(self, margin_percent):
        w_min, w_max, h_min, h_max = self.roi
        w = w_max - w_min
        h = h_max - h_min
        w_min = w_min + (margin_percent * w)
        w_max = w_max - (margin_percent * w)
        h_min = h_min + (margin_percent * h)
        h_max = h_max - (margin_percent * h)
        li = []
        for p in self.points:
            x, y = p[0], p[1]
            if w_max > x > w_min:
                if h_max > y > h_min:
                    li.append(p)
        return li

    def get_filtered_points_from_grid_points(self, grid_points, xi, yi):
        li = []
        if grid_points is None:
            return li
        for g_idx, p in grid_points:
            if g_idx[0] == xi and g_idx[1] == yi:
                li.append(p)
        return li

    # 01 11
    # 00 10
    def get_filtered_points_2x2(self, xi, yi):
        return self.get_filtered_points_from_grid_points(self.points_2x2, xi, yi)

    # 02 12 22
    # 01 11 21
    # 00 10 20
    def get_filtered_points_3x3(self, xi, yi):
        return self.get_filtered_points_from_grid_points(self.points_3x3, xi, yi)

    # out: points
    def get_points_on_corners_4_on_9(self):
        li = self.get_filtered_points_3x3(0, 0) + \
             self.get_filtered_points_3x3(0, 2) + \
             self.get_filtered_points_3x3(2, 0) + \
             self.get_filtered_points_3x3(2, 2)
        return li

    def get_points_on_corners_8_on_9(self):
        li = self.get_filtered_points_3x3(0, 0) + \
             self.get_filtered_points_3x3(0, 1) + \
             self.get_filtered_points_3x3(0, 2) + \
             self.get_filtered_points_3x3(1, 0) + \
             self.get_filtered_points_3x3(1, 2) + \
             self.get_filtered_points_3x3(2, 0) + \
             self.get_filtered_points_3x3(2, 1) + \
             self.get_filtered_points_3x3(2, 2)
        return li

    def get_sample_points_on_3x3(self, point_cnt = 10):
        li = []
        curr = []
        for i in range(0, 3):
            for j in range(0, 3):
                curr = self.get_filtered_points_3x3(i, j)
                curr = curr[0:point_cnt]
                # print(i, j, len(curr))
                # print(curr)
                if len(curr) == 0:
                    return None
                if len(curr) < point_cnt:
                    needing = point_cnt - len(curr)
                    ctr = 0
                    ctr_max = len(curr)
                    for m in range(needing):
                        curr.append(curr[ctr])
                        ctr = ctr + 1
                        if ctr == ctr_max:
                            ctr = 0
                # print(curr)
                li = li + curr
        # print(li)
        return li
                
    def run(self, file_name='2d_points.png'):
        self.draw_roi()
        # self.draw_points()
        # self.draw_points(points=self.get_filtered_points_2x2(0, 0))
        # self.draw_points(points=self.get_filtered_points_2x2(0, 1))
        # self.draw_points(points=self.get_filtered_points_2x2(1, 0))

        self.draw_roi_3x3()
        # self.get_sample_points_on_3x3()
        self.draw_points(points=self.get_sample_points_on_3x3())

        # self.draw_points(self.get_points_on_corners_8_on_9())
        # self.draw_points(self.get_points_in_mid_area(0.2))

        self.amplify_image()
        self.save_image(file_name)

        




