
import visual_points_2d as vp2d

import pickle


def pickle_load(file):
    f = open(file, 'rb')
    data = pickle.load(f)
    f.close()
    return data


def demo_0():
    res = [160, 120]
    points = [
        #[0.5, 0.5],
        [30, 30],
        [30, 90],
        [120, 30],
        [120, 90],
        [50, 50],
        [50, 51],
        ]

    vp2 = vp2d.VisualPoints2D(res, points)
    vp2.draw_roi_3x3()
    vp2.run()
    im = vp2.image

def du_run(in_file, out_file):
    d = pickle_load(in_file)
    vp2 = vp2d.VisualPoints2D(d['res'], d['points'])
    vp2.draw_roi_3x3()
    vp2.run(out_file)
    return vp2

def demo2():
    du_run('du5.pkl',  'du5.png')
    du_run('du6.pkl',  'du6.png')
    du_run('du7.pkl',  'du7.png')
    du_run('du8.pkl',  'du8.png')
    du_run('du9.pkl',  'du9.png')
    du_run('du10.pkl', 'du10.png')

d = pickle_load('du9.pkl')
vp2 = vp2d.VisualPoints2D(d['res'], d['points'])
vp2.dump()
#vp2.draw_roi_3x3()
vp2.run('du9x.png')
