#
# https://www.delftstack.com/zh-tw/howto/python/opencv-kalman-filter/
#
import cv2
import numpy as np

measured=[]
predicted=[]

mp = np.array((2,1), np.float32)
tp = np.zeros((2,1), np.float32)


kalman_fil = cv2.KalmanFilter(2,2)
kalman_fil.measurementMatrix = np.array([[1,0],[0,1]],np.float32)

#kalman_fil = cv2.KalmanFilter(3,2)
#kalman_fil.measurementMatrix = np.array([[1,0,0],[0,1,0]],np.float32)

#kalman_fil = cv2.KalmanFilter(4,2)
#kalman_fil.measurementMatrix = np.array([[1,0,0,0],[0,1,0,0]],np.float32)

#kalman_fil.transitionMatrix = np.array([[1,0,1,0],[0,1,0,1],[0,0,1,0],[0,0,0,1]],np.float32)
#kalman_fil.transitionMatrix = np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]],np.float32)
#kalman_fil.processNoiseCov = np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]],np.float32) * 0.03

d = [
     [0.0, 0.0],
     [10.0, 100.0],
     [20.0, 0.0],
     [30.0, 100.0],
     [40.0, 0.0],
     [50.0, 100.0],
     [60.0, 0.0],
     [70.0, 100.0],
     [80.0, 0.0],
     [90.0, 100.0],
     [100.0, 100.0],
     [100.0, 100.0],
     [100.0, 100.0],
     [100.0, 100.0],
     [100.0, 100.0],
]

for i in d:
    mp = np.array([[np.float32(i[0])],[np.float32(i[1])]])
    kalman_fil.correct(mp)
    tp = kalman_fil.predict()
    predicted.append((int(tp[0]),int(tp[1])))
