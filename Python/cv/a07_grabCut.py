#
# https://medium.datadriveninvestor.com/interactive-background-removal-with-opencv-and-grabcut-algorithm-351f16ec77c8
#
import cv2
import numpy as np


image = cv2.imread('pic/001.jpg')
copy = image.copy()
# Create a mask (of zeros uint8 datatype) that is the same size (width, height) as our original image 
mask = np.zeros(image.shape[:2], np.uint8)
bgdModel = np.zeros((1,65), np.float64)
fgdModel = np.zeros((1,65), np.float64)
x, y , w, h = cv2.selectROI("select the area", image)
start = (x, y)
end = (x + w, y + h)
rect = (x, y , w, h)

cv2.rectangle(copy, start, end, (0,0,255), 3)
cv2.imshow("Input Image", copy)

cv2.grabCut(image, mask, rect, bgdModel, fgdModel, 100, cv2.GC_INIT_WITH_RECT)
mask2 = np.where((mask==2)|(mask==0),0,1).astype('uint8')
image = image * mask2[:,:,np.newaxis]
cv2.imshow("Mask", mask * 80)
cv2.imshow("Mask2", mask2 * 255)
cv2.imshow("Image", image)

while True:
    #cv2.imshow("Input Image", copy)
    key = cv2.waitKey(1)
    if key == 27: #ESC
        break
cv2.destroyAllWindows()

