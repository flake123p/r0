#
# https://www.delftstack.com/zh-tw/howto/python/opencv-background-subtraction/
#

import cv2
import numpy as np
import cvzone
from cvzone.SelfiSegmentationModule import SelfiSegmentation

segmentor = SelfiSegmentation()
img = cv2.imread('pic/eye.jpg')
img_Out = segmentor.removeBG(img, (0,255,0), threshold=0.5)

while True:
    cv2.imshow('img',img_Out)
    key = cv2.waitKey(1)
    if key == 27: #ESC
        break
cv2.destroyAllWindows()
