# https://learnopencv.com/super-resolution-in-opencv/

'''
(base) pupil@pupil-server:~/temp/p1/pupil_p1$ cp pupil_data/pieces/func2/data/data_func2a/0/126965.674726458.jpg /home/pupil/sda/ws/r0/Python/_cv2/pic/
(base) pupil@pupil-server:~/temp/p1/pupil_p1$ cp pupil_data/pieces/func2/data/data_func2a/0/126973.286668160.jpg /home/pupil/sda/ws/r0/Python/_cv2/pic/
(base) pupil@pupil-server:~/temp/p1/pupil_p1$ cp pupil_data/pieces/func2/data/data_func2c/0/146388.626061146.jpg /home/pupil/sda/ws/r0/Python/_cv2/pic/
(base) pupil@pupil-server:~/temp/p1/pupil_p1$ cp pupil_data/pieces/func2/data/data_func2a/0/126976.290559499.jpg /home/pupil/sda/ws/r0/Python/_cv2/pic/
'''
import cv2
import matplotlib.pyplot as plt
# Read image

class SuperResolution(object):
    def __init__(self):
        self.sr = cv2.dnn_superres.DnnSuperResImpl_create()
        path = "EDSR_x2.pb"
        self.sr.readModel(path)
        self.sr.setModel("edsr",2)
    
    def start_160x120_to_216x216(self, inFile, outFile):
        img = cv2.imread(inFile)
        result = self.sr.upsample(img)
        # Resized image
        # resized = cv2.resize(img,dsize=None,fx=216/160,fy=216/120, interpolation=cv2.INTER_NEAREST_EXACT)
        the216 = cv2.resize(result,dsize=None,fx=216/320,fy=216/240, interpolation=cv2.INTER_NEAREST_EXACT)
        cv2.imwrite(outFile, the216)

f1  = "/home/pupil/temp/p1/pupil_p1/pupil_data/pieces/func2/data/data_func2a/0/126973.286668160_new.jpg"
f1o = "/home/pupil/temp/p1/pupil_p1/pupil_data/pieces/func2/data/data_func2a/0/126973.286668160_new216.jpg"
f2  = "/home/pupil/temp/p1/pupil_p1/pupil_data/pieces/func2/data/data_func2b/0/145800.158732006_new.png"
f2o = "/home/pupil/temp/p1/pupil_p1/pupil_data/pieces/func2/data/data_func2b/0/145800.158732006_new216.png"
f3  = "/home/pupil/temp/p1/pupil_p1/pupil_data/pieces/func2/data/data_func2c/0/146391.794837788_new.jpg"
f3o = "/home/pupil/temp/p1/pupil_p1/pupil_data/pieces/func2/data/data_func2c/0/146391.794837788_new216.jpg"
f4  = "/home/pupil/temp/p1/pupil_p1/pupil_data/pieces/func2/data/data_func2a/0/126975.914523917_new.jpg"
f4o = "/home/pupil/temp/p1/pupil_p1/pupil_data/pieces/func2/data/data_func2a/0/126975.914523917_new216.jpg"
f5  = "/home/pupil/temp/p1/pupil_p1/pupil_data/pieces/func2/data/data_func2a/0/126974.718584326_new.jpg"
f5o = "/home/pupil/temp/p1/pupil_p1/pupil_data/pieces/func2/data/data_func2a/0/126974.718584326_new216.jpg"
f6  = "/home/pupil/temp/p1/pupil_p1/pupil_data/pieces/func2/data/data_func2a/0/126976.290559499_new.jpg"
f6o = "/home/pupil/temp/p1/pupil_p1/pupil_data/pieces/func2/data/data_func2a/0/126976.290559499_new216.jpg"

sr = SuperResolution()
sr.start_160x120_to_216x216(f1, f1o)
sr.start_160x120_to_216x216(f2, f2o)
sr.start_160x120_to_216x216(f3, f3o)
sr.start_160x120_to_216x216(f4, f4o)
sr.start_160x120_to_216x216(f5, f5o)
sr.start_160x120_to_216x216(f6, f6o)

# img = cv2.imread("pic/" + img_in)
# plt.imshow(img[:,:,::-1])
# plt.show()


 

 


# cv2.imwrite("res_super.png", result)
# cv2.imwrite("res_cv.png", resized)
# cv2.imwrite(img_out, the216)

# plt.figure(figsize=(12,8))
# plt.subplot(1,3,1)
# # Original image
# plt.imshow(img[:,:,::-1])
# plt.subplot(1,3,2)
# # SR upscaled
# plt.imshow(result[:,:,::-1])
# plt.subplot(1,3,3)
# # OpenCV upscaled
# plt.imshow(resized[:,:,::-1])
# plt.show()