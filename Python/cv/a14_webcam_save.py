#
#
#

import cv2
import time

cap = cv2.VideoCapture(0)

#Check whether user selected camera is opened successfully.
if not (cap.isOpened()):
    print('Could not open video device')
else:
    print('Video device opened')

#cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
#cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 160)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 120)

base = '/home/pupil/sda/plab/_data/temp/'
ts = 0

while(True):
    ret, frame = cap.read()

    # 將圖片轉為灰階
    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    ts = ts + 1
    ts_str = f'{ts:06d}'
    frame_file = base + ts_str + '.jpg'
    print(frame_file)
    cv2.imwrite(frame_file, frame)

    #cv2.imwrite('test.jpg', frame)

    cv2.imshow('frame', frame )

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()