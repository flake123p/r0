import sys
sys.path.append('../')
from _psd_ import *
from _024_converted import Ui_MainWindow

class HBMainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

if __name__ == '__main__':
    app = QApp([])
    hb_window = HBMainWindow()
    hb_window.show()
    app.exec()