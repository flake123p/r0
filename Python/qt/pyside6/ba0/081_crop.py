import sys
sys.path.append('../')
from _psd_ import *
from PIL import Image, ImageQt

class ImageBox(QLabel):
    def __init__(self, *arg):
        super().__init__(*arg)
        # self.setAttribute(Qt.WidgetAttribute.WA_Hover, True) # Flake: Not Working ...
        # self.setMouseTracking(True)

        # 儲存選取的範圍
        self._roi: tuple(int, int, int, int) = (0, 0, 0, 0)

        # 建立 QRubberBand 物件，用來顯示選取範圍的框框
        self._rubber_band: QRubberBand = QRubberBand(QRubberBand.Rectangle, self)

        # 用來記錄選取範圍的起始點
        self._start_point = None

    # 取得 roi
    @property
    def roi(self):
        return self._roi

    #
    # Flake: Not Working ...
    #
    # hoverEnterEvent
    # hoverMoveEvent
    # hoverLeaveEvent
    # def hoverEnterEvent(self, event):
    #     pos = event.position().toPoint().toTuple()
    #     self.setText(f"hoverEnterEvent, pos:{pos}")
    #     print("hovered")

    # def hoverMoveEvent(self, event):
    #     pos = event.position().toPoint().toTuple()
    #     self.setText(f"hoverMoveEvent, pos:{pos}")

    # def hoverLeaveEvent(self, event):
    #     pos = event.position().toPoint().toTuple()
    #     self.setText(f"hoverLeaveEvent, pos:{pos}")
    def reset_roi(self):
        # 重置選取範圍並隱藏選取框
        self._roi = (0,0,0,0)
        self._rubber_band.hide()

    def show_image(self, image):
        if image:
            qt_img = ImageQt.ImageQt(image)
            q_pixmap = QPixmap.fromImage(qt_img)
            self.setPixmap(q_pixmap)

    def mousePressEvent(self, event):
        # 記錄滑鼠按下的位置作為選取範圍的起始點
        self._start_point = event.position().toPoint()

        # 設定 QRubberBand 的位置和大小
        # QRect(self._start_point, QSize()) 創建了一個 QRect 物件： self._start_point 為左上角的點，大小為QSize() （等於零）的矩形
        self._rubber_band.setGeometry(QRect(self._start_point, QSize()))

        # 顯示 QRubberBand
        self._rubber_band.show()

    def mouseMoveEvent(self, event):
        # 根據滑鼠移動的位置來更新 QRubberBand 的框框範圍
        selected_rect = QRect(self._start_point, event.position().toPoint())
        self._rubber_band.setGeometry(selected_rect.normalized())

    def mouseReleaseEvent(self, event):
        # 取得選取範圍和 Label 尺寸大小的交集，以確保選取的矩形範圍在圖像區域內
        selected_rect = self._rubber_band.geometry()
        image_rect = self.rect()

        rect = selected_rect.intersected(image_rect)    # 取得選取範圍和圖像區域的交集

        if selected_rect.isValid():
            self._roi = rect.getCoords()
        else:
            # 如果選取範圍無效，將 _roi 設置為 (0, 0, 0, 0)
            self._roi = (0,0,0,0)

class HBMainWindow:
    def __init__(self):
        loader = QUiLoader()
        # 將我的自定義部件 ImageBox 註冊到 UiLoader
        loader.registerCustomWidget(ImageBox)

        self.ui = loader.load("080.ui") # 記得註冊好自定義部件再載入 ui 檔哦

        # self.ui.image_box.setAttribute(Qt.WidgetAttribute.WA_Hover, True)

        self._image: "Image" = None
        self.ui.btn_load.clicked.connect(self.load)
        self.ui.btn_gray.clicked.connect(self.gray)
        self.ui.btn_save.clicked.connect(self.save)
        self.ui.btn_crop.clicked.connect(self.crop)

    def crop(self):
        _roi = self.ui.image_box.roi
        if _roi==(0,0,0,0):
            return None

        self._image = self._image.crop(_roi)
        self.ui.image_box.show_image(self._image)
        self.ui.image_box.reset_roi()

    def gray(self):
        if self._image:
            self._image = self._image.convert('L')
            self.show_image()

    def save(self):
        if self._image:
            file_name, file_type = QFileDialog.getSaveFileName(
                self.ui,
                "Save Image",
                "./local.png",
                "Image (*.png *.bmp *.jpg *.jpeg)"
            )
            if file_name:
                self._image.save(file_name)

    def load(self):
        # 使用上一段落介紹的 QFileDialog 來選擇圖片檔案
        file_name, file_type = QFileDialog.getOpenFileName(
            self.ui,
            "Select Image",
            "../../../../DIP/Images/camera_man.png",
            "Image (*.png *.bmp *.jpg *.jpeg);;All Files(*)"
        )

        # 再用 PIL 的 Image.open 方法來讀取選中的圖片
        if file_name:
            self._image = Image.open(file_name)

        self.show_image()

    def show_image(self):
        if self._image:
            # qt_img = ImageQt.ImageQt(self._image)
            # q_pixmap = QPixmap.fromImage(qt_img)
            # self.ui.image_box.setPixmap(q_pixmap)
            self.ui.image_box.show_image(self._image)

if __name__ == '__main__':
    app = QApp([])
    hb_window = HBMainWindow()
    hb_window.ui.show()
    app.exec()