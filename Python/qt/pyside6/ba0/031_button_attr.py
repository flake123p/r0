import sys
sys.path.append('../')
from _psd_ import *

class HBMainWindow:
    def __init__(self):
        loader = QUiLoader()
        self.ui = loader.load("010.ui")
        self.ui.my_button.setEnabled(True) # 啟用按鈕，讓它可以被點擊

        # 設定按鈕的位置(x, y)為：(100, 50) 和大小：(100, 30)
        self.ui.my_button.setGeometry(100, 50, 100, 30) 

        # 注意 .size() 返回值為一個 QSize 類；.pos() 返回值為一個 QPoint 類
        # 可以透過方法 .toTuple() 來將其轉換為 tuple。
        print(self.ui.my_button.size().toTuple()) # 輸出為：(100, 30)
        print(self.ui.my_button.x(), self.ui.my_button.y()) # 輸出為：100 50

        font = QtGui.QFont("Arial", 20)
        self.ui.label.setFont(font)

if __name__ == '__main__':
    app = QApp([])
    hb_window = HBMainWindow()
    hb_window.ui.show()
    app.exec()