import sys
sys.path.append('../')
from _psd_ import *

# Prints PySide6 version
print(ps.__version__)

# Prints the Qt version used to compile PySide6
print(ps.QtCore.__version__)

app = QApp(sys.argv)
label = QLabel("Hello World!")
label.resize(800, 600)
label.show()
app.exec()