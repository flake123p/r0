import sys
sys.path.append('../')
from _psd_ import *
from PIL import Image, ImageQt

class ImageBox(QLabel):
    def __init__(self, *arg):
        super().__init__(*arg)
        # self.setAttribute(Qt.WidgetAttribute.WA_Hover, True) # Flake: Not Working ...
        self.setMouseTracking(True)

    #
    # Flake: Not Working ...
    #
    # hoverEnterEvent
    # hoverMoveEvent
    # hoverLeaveEvent
    # def hoverEnterEvent(self, event):
    #     pos = event.position().toPoint().toTuple()
    #     self.setText(f"hoverEnterEvent, pos:{pos}")
    #     print("hovered")

    # def hoverMoveEvent(self, event):
    #     pos = event.position().toPoint().toTuple()
    #     self.setText(f"hoverMoveEvent, pos:{pos}")

    # def hoverLeaveEvent(self, event):
    #     pos = event.position().toPoint().toTuple()
    #     self.setText(f"hoverLeaveEvent, pos:{pos}")

    def show_image(self, image):
        if image:
            qt_img = ImageQt.ImageQt(image)
            q_pixmap = QPixmap.fromImage(qt_img)
            self.setPixmap(q_pixmap)

    def mousePressEvent(self, event):
        pos = event.position().toPoint().toTuple()
        if event.button() == Qt.LeftButton:
            self.setText(f"Left Pressed\npos:{pos}")
        if event.button() == Qt.RightButton:
            self.setText(f"Right Pressed\npos:{pos}")

    def mouseMoveEvent(self, event):
        pos = event.position().toPoint().toTuple()
        self.setText(f"Moving, pos:{pos}")

    def mouseReleaseEvent(self, event):
        gpos = event.globalPosition().toPoint().toTuple()
        self.setText(f"Released, global pos:{gpos}")

class HBMainWindow:
    def __init__(self):
        loader = QUiLoader()
        # 將我的自定義部件 ImageBox 註冊到 UiLoader
        loader.registerCustomWidget(ImageBox)

        self.ui = loader.load("060.ui") # 記得註冊好自定義部件再載入 ui 檔哦

        # self.ui.image_box.setAttribute(Qt.WidgetAttribute.WA_Hover, True)

        self._image: "Image" = None
        self.ui.btn_load.clicked.connect(self.load)
        self.ui.btn_gray.clicked.connect(self.gray)
        self.ui.btn_save.clicked.connect(self.save)

    def gray(self):
        if self._image:
            self._image = self._image.convert('L')
            self.show_image()

    def save(self):
        if self._image:
            file_name, file_type = QFileDialog.getSaveFileName(
                self.ui,
                "Save Image",
                "./local.png",
                "Image (*.png *.bmp *.jpg *.jpeg)"
            )
            if file_name:
                self._image.save(file_name)

    def load(self):
        # 使用上一段落介紹的 QFileDialog 來選擇圖片檔案
        file_name, file_type = QFileDialog.getOpenFileName(
            self.ui,
            "Select Image",
            "../../../../DIP/Images/camera_man.png",
            "Image (*.png *.bmp *.jpg *.jpeg);;All Files(*)"
        )

        # 再用 PIL 的 Image.open 方法來讀取選中的圖片
        if file_name:
            self._image = Image.open(file_name)

        self.show_image()

    def show_image(self):
        if self._image:
            # qt_img = ImageQt.ImageQt(self._image)
            # q_pixmap = QPixmap.fromImage(qt_img)
            # self.ui.image_box.setPixmap(q_pixmap)
            self.ui.image_box.show_image(self._image)

if __name__ == '__main__':
    app = QApp([])
    hb_window = HBMainWindow()
    hb_window.ui.show()
    app.exec()