import sys
sys.path.append('../')
from _psd_ import *

class HBMainWindow:
    def __init__(self):
        loader = QUiLoader()
        self.ui = loader.load("010.ui")

        pixmap = QPixmap('030.png')
        self.ui.label.setPixmap(pixmap)

if __name__ == '__main__':
    app = QApp([])
    hb_window = HBMainWindow()
    hb_window.ui.show()
    app.exec()