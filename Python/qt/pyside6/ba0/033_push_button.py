import sys
sys.path.append('../')
from _psd_ import *

class HBMainWindow:

    def __init__(self):
        loader = QUiLoader()
        self.ui = loader.load("010.ui")

        # 將你的函數連接到按鈕點擊訊號上
        self.ui.my_button.clicked.connect(self.say_hello)

    def say_hello(self):
        self.ui.label.setText("Hello~~~")

if __name__ == '__main__':
    app = QApp([])
    hb_window = HBMainWindow()
    hb_window.ui.show()
    app.exec()