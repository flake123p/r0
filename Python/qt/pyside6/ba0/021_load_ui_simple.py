import sys
sys.path.append('../')
from _psd_ import *

if __name__ == '__main__':
    app = QApp([])

    loader = QUiLoader()
    hb_window = loader.load("010.ui")
    hb_window.show()

    app.exec()