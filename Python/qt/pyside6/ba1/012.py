# ------------------------------------------------------
# -------------------- mplwidget.py --------------------
# ------------------------------------------------------
from PySide6.QtWidgets import*

from matplotlib.backends.backend_qt5agg import FigureCanvas

from matplotlib.figure import Figure

    
class MplWidget(QWidget):
    
    def __init__(self, parent = None):

        QWidget.__init__(self, parent)
        
        self.canvas = FigureCanvas(Figure())
        
        vertical_layout = QVBoxLayout()
        vertical_layout.addWidget(self.canvas)
        
        self.canvas.axes = self.canvas.figure.add_subplot(111)
        self.setLayout(vertical_layout)


# ------------------------------------------------------
# ---------------------- main.py -----------------------
# ------------------------------------------------------
from PySide6.QtWidgets import*
# from PySide6.uic import loadUi
from PySide6.QtUiTools import QUiLoader

from matplotlib.backends.backend_qt5agg import (NavigationToolbar2QT as NavigationToolbar)

import numpy as np
import random

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure

class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)

class MatplotlibWidget():
    
    def __init__(self):
        loader = QUiLoader()
        self.ui = loader.load("010.ui")        
        # QMainWindow.__init__(self)

        # loadUi("qt_designer.ui",self)
        # self.ui.widget.canvas.axes.clear()

        self.ui.setWindowTitle("PyQt5 & Matplotlib Example GUI")

        sc = MplCanvas(self, width=5, height=4, dpi=100)
        sc.axes.plot([0,1,2,3,4], [10,1,20,3,40])
        self.ui.widget.palette()

        # self.pushButton_generate_random_signal.clicked.connect(self.update_graph)

        # self.addToolBar(NavigationToolbar(self.MplWidget.canvas, self))


    # def update_graph(self):

    #     fs = 500
    #     f = random.randint(1, 100)
    #     ts = 1/fs
    #     length_of_signal = 100
    #     t = np.linspace(0,1,length_of_signal)
        
    #     cosinus_signal = np.cos(2*np.pi*f*t)
    #     sinus_signal = np.sin(2*np.pi*f*t)

    #     self.ui.MplWidget.canvas.axes.clear()
    #     self.ui.MplWidget.canvas.axes.plot(t, cosinus_signal)
    #     self.ui.MplWidget.canvas.axes.plot(t, sinus_signal)
    #     self.ui.MplWidget.canvas.axes.legend(('cosinus', 'sinus'),loc='upper right')
    #     self.ui.MplWidget.canvas.axes.set_title('Cosinus - Sinus Signal')
    #     self.ui.MplWidget.canvas.draw()
        
if __name__ == '__main__':
    app = QApplication([])
    hb_window = MatplotlibWidget()
    hb_window.ui.show()
    app.exec()

    # app = QApplication([])
    # window = MatplotlibWidget()
    # window.show()
    # app.exec_()