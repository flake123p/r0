import sys
sys.path.append('../')
from _psd_ import *
from PIL import Image, ImageQt

class HBMainWindow:
    def __init__(self):
        loader = QUiLoader()
        self.ui = loader.load("002.ui")

        self._image: "Image" = None
        self.ui.btn_1.clicked.connect(self.btn1)
        self.ui.btn_2.clicked.connect(self.btn2)
        self.ctr = 1000
        self.text = ''


    def btn1(self):
        print('btn1')
        self.text = self.text + str(self.ctr) + '\n'
        self.ctr += 1
        self.text = self.text + str(self.ctr) + '\n'
        self.ctr += 10
        self.ui.image_box.setText(self.text)

    def btn2(self):
        print('btn2')

if __name__ == '__main__':
    app = QApp([])
    hb_window = HBMainWindow()
    hb_window.ui.show()
    app.exec()