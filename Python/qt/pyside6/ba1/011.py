import sys
sys.path.append('../')
from _psd_ import *

import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure



class MplCanvas(FigureCanvasQTAgg):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)

class HBMainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(HBMainWindow, self).__init__(*args, **kwargs)
        loader = QUiLoader()
        self.ui = loader.load("010.ui")

        # self._image: "Image" = None
        self.ui.btn_1.clicked.connect(self.btn1)
        self.ui.btn_2.clicked.connect(self.btn2)
        self.ctr = 1000
        self.text = ''


    def btn1(self):
        print('btn1')
        self.text = self.text + str(self.ctr) + '\n'
        self.ctr += 1
        self.text = self.text + str(self.ctr) + '\n'
        self.ctr += 10
        self.ui.widget.setText(self.text)

    def btn2(self):
        print('btn2')
        sc = MplCanvas(self, width=5, height=4, dpi=100)
        sc.axes.plot([0,1,2,3,4], [10,1,20,3,40])
        self.ui.setCentralWidget(sc)
        # self.show()

if __name__ == '__main__':
    app = QApp([])
    hb_window = HBMainWindow()
    hb_window.ui.show()
    app.exec()