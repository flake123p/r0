from PySide6.QtWidgets import QApplication, QWidget, QFileDialog, QPushButton, QLabel, QVBoxLayout
import numpy as np
import matplotlib.pyplot as plt
from librosa.display import specshow
import librosa
import sys
import os


class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.app = app

        self.setMinimumSize(400, 200)

        layout = QVBoxLayout()

        self.filepath = QLabel()
        chooseButton = QPushButton("Select File")
        actionButton = QPushButton("Process")

        chooseButton.clicked.connect(self.openFileDialog)
        actionButton.clicked.connect(self.process)
        
        layout.addWidget(self.filepath)
        layout.addWidget(chooseButton)
        layout.addWidget(actionButton)
        self.setLayout(layout)
        

    def process(self):
        if self.filepath.text() == "": return

        amp, sr = librosa.load(self.filepath.text(), sr=None)
        stft = librosa.stft(amp)
        db = librosa.amplitude_to_db(np.abs(stft), ref=np.max)

        fig, ax = plt.subplots(figsize=(5, 3))
        img = specshow(db, sr=sr, ax=ax, x_axis="time", y_axis="hz", cmap="nipy_spectral")

        fig.colorbar(img, ax=ax, format=f"%0.2f dB")
        fig.tight_layout(pad=1)
        
        plt.show()

    
    def openFileDialog(self):
        file = QFileDialog.getOpenFileName(self, 
                "Open music file/s", os.getcwd(), "Audio Files (*.mp3 *.flac)")
        
        if file[0] != "":
            self.filepath.setText(file[0])


app = QApplication(sys.argv)
window = MainWindow()
window.show()
app.exec()