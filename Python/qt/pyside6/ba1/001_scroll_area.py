import sys
sys.path.append('../')
from _psd_ import *
from PIL import Image, ImageQt

class HBMainWindow:
    def __init__(self):
        loader = QUiLoader()
        self.ui = loader.load("000.ui")

        self._image: "Image" = None
        self.ui.btn_load.clicked.connect(self.load)
        self.ui.btn_gray.clicked.connect(self.gray)
        self.ui.btn_save.clicked.connect(self.save)

    def gray(self):
        if self._image:
            self._image = self._image.convert('L')
            self.show_image()

    def save(self):
        if self._image:
            file_name, file_type = QFileDialog.getSaveFileName(
                self.ui,
                "Save Image",
                "./local.png",
                "Image (*.png *.bmp *.jpg *.jpeg)"
            )
            if file_name:
                self._image.save(file_name)

    def load(self):
        # 使用上一段落介紹的 QFileDialog 來選擇圖片檔案
        file_name, file_type = QFileDialog.getOpenFileName(
            self.ui,
            "Select Image",
            "../../../../DIP/Images/camera_man.png",
            "Image (*.png *.bmp *.jpg *.jpeg);;All Files(*)"
        )

        # 再用 PIL 的 Image.open 方法來讀取選中的圖片
        if file_name:
            self._image = Image.open(file_name)

        self.show_image()

    def show_image(self):
        if self._image:
            qt_img = ImageQt.ImageQt(self._image)
            q_pixmap = QPixmap.fromImage(qt_img)
            self.ui.image_box.setPixmap(q_pixmap)

if __name__ == '__main__':
    app = QApp([])
    hb_window = HBMainWindow()
    hb_window.ui.show()
    app.exec()