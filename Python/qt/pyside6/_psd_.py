
import PySide6 as ps
import PySide6.QtCore
from PySide6.QtCore import *
from PySide6.QtWidgets import (
    QApplication, QLabel, QFileDialog, QRubberBand,
    QMainWindow)
from PySide6.QtUiTools import QUiLoader
from PySide6 import QtGui
from PySide6.QtGui import QPixmap, QIcon
from PySide6.QtCore import QSize, Qt, QRect
class QApp(QApplication):
    def __init__(self, *arg, **kwarg):
        QCoreApplication.setAttribute(Qt.AA_ShareOpenGLContexts)
        super().__init__(*arg, **kwarg)
    def exec(self, *arg, **kwarg):
        super().exec(*arg, **kwarg)
# if 1:
#     using_pyside6 = True
# else:
#     using_pyside6 = False

# if using_pyside6:
#     import PySide6 as ps
#     import PySide6.QtCore
#     from PySide6.QtCore import *
#     from PySide6.QtWidgets import (
#         QApplication, QLabel, QFileDialog, QRubberBand,
#         QMainWindow)
#     from PySide6.QtUiTools import QUiLoader
#     from PySide6 import QtGui
#     from PySide6.QtGui import QPixmap, QIcon
#     from PySide6.QtCore import QSize, Qt, QRect
#     class QApp(QApplication):
#         def __init__(self, *arg, **kwarg):
#             QCoreApplication.setAttribute(Qt.AA_ShareOpenGLContexts)
#             super().__init__(*arg, **kwarg)
#         def exec(self, *arg, **kwarg):
#             super().exec(*arg, **kwarg)
# else:
#     import PySide2 as ps
#     import PySide2.QtCore
#     from PySide2.QtCore import *
#     from PySide2.QtWidgets import (
#         QApplication, QLabel, QFileDialog, QRubberBand,
#         QMainWindow)
#     from PySide2.QtUiTools import QUiLoader
#     from PySide2 import QtGui
#     from PySide2.QtGui import QPixmap, QIcon
#     from PySide2.QtCore import QSize, Qt, QRect
#     class QApp(QApplication):
#         def __init__(self, *arg, **kwarg):
#             QCoreApplication.setAttribute(Qt.AA_ShareOpenGLContexts)
#             super().__init__(*arg, **kwarg)
#         def exec(self, *arg, **kwarg):
#             super().exec_(*arg, **kwarg)