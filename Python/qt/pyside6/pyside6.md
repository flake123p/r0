# Installation
    Ans:
        pip install pyside6 pillow



# ba0
    https://medium.com/@benson890720/python%E7%B7%B4%E7%BF%92%E7%AD%86%E8%A8%98-pyside6%E5%81%9A%E4%B8%80%E5%80%8B%E7%B0%A1%E5%96%AE%E7%9A%84gui-application-0-%E7%B0%A1%E4%BB%8B%E8%88%87%E8%A8%AD%E5%AE%9A-96c982d8f90



# From 6.5.0, xcb-cursor0 or libxcb-cursor0 is needed to load the Qt xcb platform plugin.
    Ans:
        sudo apt-get install -y libxcb-cursor-dev



# Open QT designer
    Ans:
        pyside6-designer



# Official pyside6 Tutorial
    https://www.pythonguis.com/pyside6-tutorial/



# Examples 

    Erriez:
    https://github.com/Erriez/pyside6-getting-started

    flyfire:
    https://github.com/flyfire/pyside6-examples/tree/master

    qtproject:
    https://github.com/qtproject/pyside-examples/tree/dev/examples

    Others:
    https://github.com/muziing/PySide6-Code-Tutorial/blob/main/00-PySideLearning-%E6%96%87%E7%AB%A0%E6%95%99%E7%A8%8BDemo%E7%AD%89%E8%B5%84%E6%BA%90/01-QtNamespace-Qt%E5%91%BD%E5%90%8D%E7%A9%BA%E9%97%B4.md
    



# Matplotlib for Python Developers (PDF, 2009)
    https://theswissbay.ch/pdf/Gentoomen%20Library/Programming/Python/Matplotlib%20for%20Python%20Developers%20%282009%29.pdf
    - example with matplotlib + qt designer

    ipynb (2nd)
    https://github.com/PacktPublishing/Matplotlib-for-Python-Developers-Second-Edition

    

