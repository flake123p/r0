# AttributeError: module 'pyqtgraph.Qt.QtGui' has no attribute 'QApplication'
# AttributeError: module 'pyqtgraph.Qt.QtGui' has no attribute 'QGraphicsEllipseItem'
From:
    pyqtgraph.Qt.QtGui.QApplication
To:
    pyqtgraph.Qt.QtWidgets.QApplication



# AttributeError: 'QApplication' object has no attribute 'exec_'
From:
    exe_()
To:
    exe()



# Designer
Installation:
    pip install pyqt6-tools
Run:
    pyqt6-tools designer