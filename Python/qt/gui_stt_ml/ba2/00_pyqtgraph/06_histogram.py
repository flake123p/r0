import pyqtgraph as pg
import numpy as np
import sys

x = np.random.normal(size=1000)
y, x = np.histogram(x, bins=np.linspace(-3, 3, 40))
curve = pg.PlotCurveItem(-1.*x, y, stepMode=True, fillLevel=0, brush=(0, 0, 255, 80))
# curve.rotate(-90)  # 這是 chatGPT 找到的答案，但是不能執行
curve.setRotation(-90) # 與 chatGPT 數次來回問題後，找到的答案 
 
win = pg.GraphicsLayoutWidget(show=True)
plot = win.addPlot()
plot.addItem(curve)

# Create the main application instance
app = pg.mkQApp()
plt1 = win.addPlot()
plt1.addItem(plot)
# execute the application and Gracefully exit the application
sys.exit(app.exec())