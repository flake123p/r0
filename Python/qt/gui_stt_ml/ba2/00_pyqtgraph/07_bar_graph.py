import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore # 這個宣告會引用 PyQt 的 QtGui, QtCore
 
# define the data
title = "Basic pyqtgraph plot: Bar Graph & xTicks"
plt = pg.plot()
x = [1, 2, 3]
y = [10, 30, 20]
Ticks = ["A","B","C"]
 
barItem = pg.BarGraphItem(x = x, height = y, width = 0.3, brush=(107,200,224))
plt.addItem(barItem)
 
plt.getAxis('bottom').setTicks([[(i, Ticks[i-1]) for i in x]])
font = QtGui.QFont()
font.setPixelSize(20)
plt.getAxis("bottom").setStyle(tickFont = font)
plt.getAxis("left").setStyle(tickFont = font)
plt.getAxis("bottom").setTextPen(color='g')
plt.getAxis("bottom").setPen(color='y')
plt.setWindowTitle(title)
 
# main method
if __name__ == '__main__':
    import sys
    app = pg.Qt.QtWidgets.QApplication.instance()
    sys.exit(app.exec())