'''
Usage:

import sys
sys.path.append('../../')
from pqt import *
'''

# from PyQt6.QtWidgets import QApplication, QWidget, \
#     QVBoxLayout, QComboBox, QLineEdit, QPushButton, QLabel

use_pyqt6 = 1

if use_pyqt6:
    # import PyQt6 as pqt
    from PyQt6 import (QtWidgets, uic)
    from PyQt6.QtWidgets import (QApplication, QWidget,
    QVBoxLayout, QComboBox, QLineEdit, QPushButton, QLabel)
else:
    # import PySide6 as pqt
    from PySide6 import (QtWidgets, uic)
    from PySide6.QtWidgets import (QApplication, QWidget,
    QVBoxLayout, QComboBox, QLineEdit, QPushButton, QLabel)

# from pqt.QtWidgets import (QApplication, QWidget,
#     QVBoxLayout, QComboBox, QLineEdit, QPushButton, QLabel)

# from .test import (
#     foo,
#     abxx,
#     ModXX,
# )

# # from .same_name import *  # might ambiguous

# from . import same_name  # better practice