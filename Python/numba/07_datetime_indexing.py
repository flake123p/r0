import datetime as dt
import numpy as np
import time
import numba

import MyUtils as ut

Prof = ut.ProfilingManager
pf = ut.ProfilingFunc

#
# make list
#
length = 1000*10
dates = []
dates_my = [] # MY dates = Int dates
dates_my_np = []
val = [0.0] * length
val_np = np.zeros(length, dtype = np.float64)
dates_my_np = np.zeros(length, dtype = np.int32)
t = dt.datetime(2008, 1, 1)
for i in range(length):
    dates.append(t + dt.timedelta(days=i))
    # print(dates.index(t + dt.timedelta(days=i)))
    val[i] = float(i)
    val_np[i] = i
    my = (t + dt.timedelta(days=i)).year * 10000 + (t + dt.timedelta(days=i)).month * 100 + (t + dt.timedelta(days=i)).day
    dates_my.append(my)
    dates_my_np[i] = my

def indexing_n_sum(src_dates, dst_dates, val):
    sum = val[0] - val[0]
    for i in range(len(src_dates)):
        idx = dst_dates.index(src_dates[i])
        sum += val[idx]
    return sum

def indexing_np_n_sum(src_dates, dst_dates, val):
    sum = val[0] - val[0]
    for i in range(len(src_dates)):
        idx = np.argwhere(dst_dates == src_dates[i])[0][0]
        sum += val[idx]
    return sum

@numba.jit()
def indexing_n_sum_jit(src_dates, dst_dates, val):
    sum = val[0] - val[0]
    for i in range(len(src_dates)):
        idx = dst_dates.index(src_dates[i])
        sum += val[idx]
    return sum

@numba.jit()
def indexing_np_n_sum_jit(src_dates, dst_dates, val):
    sum = val[0] - val[0]
    for i in range(len(src_dates)):
        idx = np.argwhere(dst_dates == src_dates[i])[0][0]
        sum += val[idx]
    return sum

@numba.jit()
def numpy_1d_indexing(np_1d, target) -> int:
    for i in range(np_1d.size):
        if np_1d[i] == target:
            return i
    return 0

@numba.jit()
def indexing_np_n_sum_jit2(src_dates, dst_dates, val):
    sum = val[0] - val[0]
    for i in range(src_dates.size):
        idx = numpy_1d_indexing(dst_dates, src_dates[i])
        sum += val[idx]
    return sum

# @numba.jit()
# def indexing_n_sum_my_jit(date, max, dates, val):
#     sum = val[0] - val[0]
#     for i in range(max):
#         d = date + dt.timedelta(days=i)
#         idx = dates.index(d)
#         sum += val[idx]
#     return sum
# print(dates)

# print(val_np)

# print(indexing_n_sum(dates, dates, val))
# print(indexing_n_sum(dates, dates, val_np))

epoch = 10

N = range(epoch)

pf(lambda : [indexing_n_sum(dates, dates, val)                   for _ in N], 'datetime with list')
pf(lambda : [indexing_n_sum(dates, dates, val_np)                for _ in N], 'datetime with np_f64')
pf(lambda : [indexing_n_sum(dates_my, dates_my, val)             for _ in N], 'MY datetime with list')
pf(lambda : [indexing_n_sum(dates_my, dates_my, val_np)          for _ in N], 'MY datetime with np_f64')
pf(lambda : [indexing_np_n_sum(dates_my_np, dates_my_np, val_np) for _ in N], 'MY datetime np with np_f64')

#
# JIT
#

#
# Error : Cannot determine Numba type of <class 'datetime.datetime'>
#
# profile = Prof()
# for i in range(epoch):
#     indexing_n_sum_jit(dates, dates, val)
# profile.show('[JIT] datetime with list')

#
# Error : Cannot determine Numba type of <class 'datetime.datetime'>
#
# profile = Prof()
# for i in range(epoch):
#     indexing_n_sum_jit(dates, dates, val)
# profile.show('[JIT] datetime with numpy f64')

profile = Prof()
for i in range(epoch):
    indexing_n_sum_jit(dates_my, dates_my, val)
profile.show('[JIT] MY datetime with list')

profile = Prof()
for i in range(epoch):
    indexing_n_sum_jit(dates_my, dates_my, val_np)
profile.show('[JIT] MY datetime with numpy f64')

profile = Prof()
for i in range(epoch):
    indexing_np_n_sum_jit(dates_my_np, dates_my_np, val_np)
profile.show('[JIT] MY datetime np with numpy f64')

profile = Prof()
for i in range(epoch):
    indexing_np_n_sum_jit2(dates_my_np, dates_my_np, val_np)
profile.show('[JIT] MY datetime np with numpy f64 (my indexing)')

'''
#
# Error : Cannot determine Numba type of <class 'datetime.datetime'>
#
datetime with list                                Elapsed Time = 3226.265669 ms
datetime with np_f64                              Elapsed Time = 3252.081871 ms
MY datetime with list                             Elapsed Time = 2065.738678 ms
MY datetime with np_f64                           Elapsed Time = 2063.217878 ms
MY datetime np with np_f64                        Elapsed Time = 403.620243 ms
[JIT] MY datetime with list                       Elapsed Time = 627.797604 ms
[JIT] MY datetime with numpy f64                  Elapsed Time = 297.604799 ms
[JIT] MY datetime np with numpy f64               Elapsed Time = 788.691998 ms
[JIT] MY datetime np with numpy f64 (my indexing) Elapsed Time = 231.603861 ms       BEST !!!

Conclusion: np dates + jit + my 1d indexing
'''