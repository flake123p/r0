import datetime as dt
import numpy as np
import time
import numba
import sys
from misc import OTHER_ASSERT
import MyUtils as ut
Prof = ut.ProfilingManager
pf = ut.ProfilingFunc
N = range(100)

length = 1000*100
dl = list()
dd = dict()
t = dt.datetime(2008, 1, 1)
for i in range(length):
    now = t + dt.timedelta(days=i)
    dl.append(now)
    dd[i] = now

MY_DBG_MODE = __debug__
def BASIC_ASSERT(condi):
    if MY_DBG_MODE:
        if condi == False:
            from inspect import getframeinfo, stack
            caller = getframeinfo(stack()[1][0])
            print('[FAILED] ASSERT at {} : line {}.'.format(caller.filename, caller.lineno))
            sys.exit(1)

@numba.jit()
def BASIC_ASSERT_jit_assert(condi):
    if MY_DBG_MODE:
        assert condi

def verify_MY_BASIC(l, d):
    for i in reversed(range(len(l))):
        BASIC_ASSERT(d[i] == l[i])

def verify_MY_BASIC_jit_assert(l, d):
    for i in reversed(range(len(l))):
        BASIC_ASSERT_jit_assert(d[i] == l[i])

def verify_MY_PKG_ASSERT(l, d):
    for i in reversed(range(len(l))):
        ut.MY_ASSERT(d[i] == l[i])

def verify_OTHER_ASSERT(l, d):
    for i in reversed(range(len(l))):
        OTHER_ASSERT(d[i] == l[i])

def verify_py_assert(l, d):
    for i in reversed(range(len(l))):
        assert d[i] == l[i]

print('MY_DBG_MODE =', MY_DBG_MODE)
print('__debug__   =', __debug__)

pf(lambda : [verify_MY_BASIC(dl, dd)            for _ in N], 'verify_MY_BASIC')
pf(lambda : [verify_MY_BASIC_jit_assert(dl, dd) for _ in N], 'verify_MY_BASIC_jit_assert')
pf(lambda : [verify_MY_PKG_ASSERT(dl, dd)       for _ in N], 'verify_MY_PKG_ASSERT')
pf(lambda : [verify_OTHER_ASSERT(dl, dd)        for _ in N], 'verify_OTHER_ASSERT')
pf(lambda : [verify_py_assert(dl, dd)           for _ in N], 'verify_py_assert')

'''
MY_DBG_MODE = True
__debug__   = True
verify_MY_BASIC                                   Elapsed Time = 694.978476 ms
verify_MY_BASIC_jit_assert                        Elapsed Time = 1591.591835 ms
verify_MY_PKG_ASSERT                              Elapsed Time = 835.868597 ms
verify_OTHER_ASSERT                               Elapsed Time = 704.084873 ms
verify_py_assert                                  Elapsed Time = 342.521667 ms

Run with __debug__ disabled : python -O

MY_DBG_MODE = False
__debug__   = False
verify_MY_BASIC                                   Elapsed Time = 620.273829 ms
verify_MY_BASIC_jit_assert                        Elapsed Time = 1621.178389 ms
verify_MY_PKG_ASSERT                              Elapsed Time = 764.986753 ms
verify_OTHER_ASSERT                               Elapsed Time = 629.280806 ms
verify_py_assert                                  Elapsed Time = 99.843502 ms
'''
