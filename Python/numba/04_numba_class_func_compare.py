#
# https://numba.readthedocs.io/en/stable/user/jitclass.html
#
import time
from numba.experimental import jitclass
from numba import jit               # import the decorator
from numba import int32, float32    # import the types
import numba

spec = [
    ('loops', int32)
]

@jitclass(spec)
class BusyJit:
    def __init__(self, loops):
        self.loops = loops
        pass

    def run(self):
        sum_num = 0
        start = -(1000*1000*10*1*1)
        end   =  (1000*1000*10*1*1) + 1
        for l in range(self.loops):
            for i in range(start, end):
                sum_num += i
        return sum_num

class Busy:
    def __init__(self, loops):
        self.loops = loops
        pass

    def run(self):
        sum_num = 0
        start = -(1000*1000*10*1*1)
        end   =  (1000*1000*10*1*1) + 1
        for l in range(self.loops):
            for i in range(start, end):
                sum_num += i
        return sum_num

b = Busy(1)
start_time = time.time()
print(b.run())
print('Method   Without JIT: Time used: {:.6f} ms'.format((time.time() - start_time) * 1000))
start_time = time.time()
print(b.run())
print('Method   Without JIT: Time used: {:.6f} ms'.format((time.time() - start_time) * 1000))

b = BusyJit(1)
start_time = time.time()
print(b.run())
print('Method   With    JIT: Time used: {:.6f} ms'.format((time.time() - start_time) * 1000))
start_time = time.time()
print(b.run())
print('Method   With    JIT: Time used: {:.6f} ms'.format((time.time() - start_time) * 1000))

def func(loops):
    sum_num = 0
    start = -(1000*1000*10*1*1)
    end   =  (1000*1000*10*1*1) + 1
    for l in range(loops):
        for i in range(start, end):
            sum_num += i
    return sum_num

@jit
def func_jit(loops):
    sum_num = 0
    start = -(1000*1000*10*1*1)
    end   =  (1000*1000*10*1*1) + 1
    for l in range(loops):
        for i in range(start, end):
            sum_num += i
    return sum_num

start_time = time.time()
print(func(1))
print('Function Without JIT: Time used: {:.6f} ms'.format((time.time() - start_time) * 1000))
start_time = time.time()
print(func(1))
print('Function Without JIT: Time used: {:.6f} ms'.format((time.time() - start_time) * 1000))

start_time = time.time()
print(func_jit(1))
print('Function With    JIT: Time used: {:.6f} ms'.format((time.time() - start_time) * 1000))
start_time = time.time()
print(func_jit(1))
print('Function With    JIT: Time used: {:.6f} ms'.format((time.time() - start_time) * 1000))

@jit(nopython=False)
def func_jit_nF(loops):
    sum_num = 0
    start = -(1000*1000*10*1*1)
    end   =  (1000*1000*10*1*1) + 1
    for l in range(loops):
        for i in range(start, end):
            sum_num += i
    return sum_num
start_time = time.time()
print(func_jit_nF(1))
print('Function With    JIT: Time used: {:.6f} ms (nopython=False)'.format((time.time() - start_time) * 1000))
start_time = time.time()
print(func_jit_nF(1))
print('Function With    JIT: Time used: {:.6f} ms (nopython=False)'.format((time.time() - start_time) * 1000))

@jit(nopython=True)
def func_jit_nT(loops):
    sum_num = 0
    start = -(1000*1000*10*1*1)
    end   =  (1000*1000*10*1*1) + 1
    for l in range(loops):
        for i in range(start, end):
            sum_num += i
    return sum_num
start_time = time.time()
print(func_jit_nT(1))
print('Function With    JIT: Time used: {:.6f} ms (nopython=True)'.format((time.time() - start_time) * 1000))
start_time = time.time()
print(func_jit_nT(1))
print('Function With    JIT: Time used: {:.6f} ms (nopython=True)'.format((time.time() - start_time) * 1000))
'''
0
Method   Without JIT: Time used: 441.895723 ms
0
Method   Without JIT: Time used: 442.616701 ms
0
Method   With    JIT: Time used: 66.139698 ms
0
Method   With    JIT: Time used: 0.007629 ms
0
Function Without JIT: Time used: 465.226173 ms
0
Function Without JIT: Time used: 461.571455 ms
0
Function With    JIT: Time used: 36.559582 ms
0
Function With    JIT: Time used: 0.002861 ms
0
Function With    JIT: Time used: 35.226822 ms (nopython=False)
0
Function With    JIT: Time used: 0.002861 ms (nopython=False)
0
Function With    JIT: Time used: 35.346031 ms (nopython=True)
0
Function With    JIT: Time used: 0.004053 ms (nopython=True)
'''