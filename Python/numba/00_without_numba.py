#
# https://medium.com/jacky-life/%E9%AD%AF%E8%9B%87%E8%AE%8A%E8%9F%92%E8%9B%87%E8%A8%98-41e9c047e8e5
#
import time

def sum_cal(x,y):
    sum_num = 0
    for i in range(x,y):
        sum_num += i
    return sum_num

start_time = time.time()

print(sum_cal(-100000000, 100000000+1))
print('Time used: {} sec'.format(time.time()-start_time))
'''
0
Time used: 4.4545252323150635 sec
'''