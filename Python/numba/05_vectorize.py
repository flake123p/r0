from numba import jit
import numpy as np
import time
import numba as nb

y = np.random.random(10**5).astype(np.float32)


def func_wo_vec(a, b):
    for i in range(1000):
        a * b + b
    return a

start_time = time.time()
func_wo_vec(y, 100)
print('Function Without vectorize: Time used: {:.6f} ms'.format((time.time() - start_time) * 1000))
start_time = time.time()
func_wo_vec(y, 100)
print('Function Without vectorize: Time used: {:.6f} ms'.format((time.time() - start_time) * 1000))


@nb.vectorize()
def func_with_vec(a, b):
    for i in range(1000):
        a * b + b
    return a

start_time = time.time()
func_with_vec(y, 100)
print('Function With    vectorize: Time used: {:.6f} ms'.format((time.time() - start_time) * 1000))
start_time = time.time()
func_with_vec(y, 100)
print('Function With    vectorize: Time used: {:.6f} ms'.format((time.time() - start_time) * 1000))
start_time = time.time()
func_with_vec(y, 100)
print('Function With    vectorize: Time used: {:.6f} ms'.format((time.time() - start_time) * 1000))
start_time = time.time()
func_with_vec(y, 100)
print('Function With    vectorize: Time used: {:.6f} ms'.format((time.time() - start_time) * 1000))


@nb.jit()
def func_with_jit(a, b):
    for i in range(1000):
        a * b + b
    return a

start_time = time.time()
func_with_jit(y, 100)
print('Function With    jit      : Time used: {:.6f} ms'.format((time.time() - start_time) * 1000))
start_time = time.time()
func_with_jit(y, 100)
print('Function With    jit      : Time used: {:.6f} ms'.format((time.time() - start_time) * 1000))
start_time = time.time()
func_with_jit(y, 100)
print('Function With    jit      : Time used: {:.6f} ms'.format((time.time() - start_time) * 1000))
start_time = time.time()
func_with_jit(y, 100)
print('Function With    jit      : Time used: {:.6f} ms'.format((time.time() - start_time) * 1000))

'''
Function Without vectorize: Time used: 17.850399 ms
Function Without vectorize: Time used: 17.777205 ms
Function With    vectorize: Time used: 188.732147 ms
Function With    vectorize: Time used: 0.041246 ms
Function With    vectorize: Time used: 0.026941 ms
Function With    vectorize: Time used: 0.026226 ms
Function With    jit      : Time used: 160.732508 ms
Function With    jit      : Time used: 11.616707 ms
Function With    jit      : Time used: 11.385441 ms
Function With    jit      : Time used: 11.449575 ms
'''