import time
import numba

leng = 1000*1000*1
x = [0] * leng
y = [0] * leng
z = [0] * leng
for i in range(leng):
    x[i] = i
    y[i] = i

def add_list(x, y, z):
    for i in range(len(x)):
        z[i] = x[i] + y[i]

start_time = time.time()
add_list(x, y, z)
elapsed = time.time() - start_time
f = 'List with Pure'
print(f'{f:20}' + 'Elapsed Time = {:.6f} ms'.format(elapsed * 1000))

@numba.jit()
def add_list_j(x, y, z):
    for i in range(len(x)):
        z[i] = x[i] + y[i]

start_time = time.time()
add_list_j(x, y, z)
elapsed = time.time() - start_time
f = 'List With JIT'
print(f'{f:20}' + 'Elapsed Time = {:.6f} ms'.format(elapsed * 1000))
start_time = time.time()
add_list_j(x, y, z)
elapsed = time.time() - start_time
f = 'List With JIT'
print(f'{f:20}' + 'Elapsed Time = {:.6f} ms'.format(elapsed * 1000))

import array
x = array.array('l', x)
y = array.array('l', y)
z = array.array('l', z)

start_time = time.time()
add_list(x, y, z)
elapsed = time.time() - start_time
f = 'Array with Pure'
print(f'{f:20}' + 'Elapsed Time = {:.6f} ms'.format(elapsed * 1000))

start_time = time.time()
add_list_j(x, y, z)
elapsed = time.time() - start_time
f = 'Array With JIT'
print(f'{f:20}' + 'Elapsed Time = {:.6f} ms'.format(elapsed * 1000))
start_time = time.time()
add_list_j(x, y, z)
elapsed = time.time() - start_time
f = 'Array With JIT'
print(f'{f:20}' + 'Elapsed Time = {:.6f} ms'.format(elapsed * 1000))
start_time = time.time()
add_list_j(x, y, z)
elapsed = time.time() - start_time
f = 'Array With JIT'
print(f'{f:20}' + 'Elapsed Time = {:.6f} ms'.format(elapsed * 1000))

import numpy
x = numpy.array(x, dtype = numpy.int32)
y = numpy.array(y, dtype = numpy.int32)
z = numpy.array(z, dtype = numpy.int32)

start_time = time.time()
add_list(x, y, z)
elapsed = time.time() - start_time
f = 'Numpy_S32 with Pure'
print(f'{f:20}' + 'Elapsed Time = {:.6f} ms'.format(elapsed * 1000))

start_time = time.time()
add_list_j(x, y, z)
elapsed = time.time() - start_time
f = 'Numpy_S32 With JIT'
print(f'{f:20}' + 'Elapsed Time = {:.6f} ms'.format(elapsed * 1000))
#
start_time = time.time()
add_list_j(x, y, z)
elapsed = time.time() - start_time
f = 'Numpy_S32 With JIT'
print(f'{f:20}' + 'Elapsed Time = {:.6f} ms'.format(elapsed * 1000))
#
start_time = time.time()
add_list_j(x, y, z)
elapsed = time.time() - start_time
f = 'Numpy_S32 With JIT'
print(f'{f:20}' + 'Elapsed Time = {:.6f} ms'.format(elapsed * 1000))

# You can't vectorize list & array !!!!!!!!!!!!!!!!!!!!!!!!!!!
# You can't vectorize list & array !!!!!!!!!!!!!!!!!!!!!!!!!!!
# You can't vectorize list & array !!!!!!!!!!!!!!!!!!!!!!!!!!!
# You can't vectorize list & array !!!!!!!!!!!!!!!!!!!!!!!!!!!
# You can't vectorize list & array !!!!!!!!!!!!!!!!!!!!!!!!!!!
# You can't vectorize list & array !!!!!!!!!!!!!!!!!!!!!!!!!!!
# You can't vectorize list & array !!!!!!!!!!!!!!!!!!!!!!!!!!!
# You can't vectorize list & array !!!!!!!!!!!!!!!!!!!!!!!!!!!
# You can't vectorize list & array !!!!!!!!!!!!!!!!!!!!!!!!!!!
# You can't vectorize list & array !!!!!!!!!!!!!!!!!!!!!!!!!!!
# You can't vectorize list & array !!!!!!!!!!!!!!!!!!!!!!!!!!!

#
# Failed with numpy + vectorize
#
# @numba.vectorize()
# def add_list_v(x, y, z):
#     for i in range(len(x)):
#         z[i] = x[i] + y[i]

# start_time = time.time()
# add_list_v(x, y, z)
# elapsed = time.time() - start_time
# f = 'Numpy_S32 With VEC'
# print(f'{f:20}' + 'Elapsed Time = {:.6f} ms'.format(elapsed * 1000))
# #
# start_time = time.time()
# add_list_v(x, y, z)
# elapsed = time.time() - start_time
# f = 'Numpy_S32 With VEC'
# print(f'{f:20}' + 'Elapsed Time = {:.6f} ms'.format(elapsed * 1000))



# print(z[0], z[1], z[2])
'''
List with Pure      Elapsed Time = 44.418335 ms
List With JIT       Elapsed Time = 2287.621260 ms
List With JIT       Elapsed Time = 1952.219725 ms
Array with Pure     Elapsed Time = 64.647198 ms
Array With JIT      Elapsed Time = 58.821201 ms
Array With JIT      Elapsed Time = 0.971317 ms
Array With JIT      Elapsed Time = 0.823736 ms
Numpy_S32 with Pure Elapsed Time = 122.975349 ms
Numpy_S32 With JIT  Elapsed Time = 57.197809 ms
Numpy_S32 With JIT  Elapsed Time = 0.582933 ms
Numpy_S32 With JIT  Elapsed Time = 0.329018 ms
'''