import math
from concurrent.futures import ThreadPoolExecutor
import numba as nb
import numpy as np
import time

@nb.jit(nopython=True, nogil=False)
def without_gil(output : list, x, y):
    sum_num = 0
    for i in range(x,y):
        sum_num += i
    output[0] = sum_num + 1

@nb.jit(nopython=True, nogil=True)
def with_gil(output : list, x, y):
    sum_num = 0
    for i in range(x,y):
        sum_num += i
    output[0] = sum_num + 1

def make_1_task(kernel, s, e):
    out = [0]
    kernel(out, s, e)
    return out[0]

def make_4_tasks(kernel, s, e):
    outputs = [[0], [0], [0], [0]]
    all_len = e - s
    step = round(all_len/4)
    with ThreadPoolExecutor(max_workers=4) as executor:
        executor.submit(kernel, outputs[0], s,              s + (step * 1))
        executor.submit(kernel, outputs[1], s + (step * 1), s + (step * 2))
        executor.submit(kernel, outputs[2], s + (step * 2), s + (step * 3))
        executor.submit(kernel, outputs[3], s + (step * 3), e)
        
    executor.shutdown(wait=True)
    # print('x', s,              s + (step * 1))
    # print('x', s + (step * 1), s + (step * 2))
    # print('x', s + (step * 2), s + (step * 3))
    # print('x', s + (step * 3), e)
    # print(outputs[0][0])
    # print(outputs[1][0])
    # print(outputs[2][0])
    # print(outputs[3][0])
    # print(outputs[0][0] + outputs[1][0] + outputs[2][0] + outputs[3][0])
    return outputs[0][0] + outputs[1][0] + outputs[2][0] + outputs[3][0]
    # args[0] = outputs[0][0] + outputs[1][0] + outputs[2][0] + outputs[3][0]

start = -(1000*1000*1000*1000*100)
end   =  (1000*1000*1000*1000*100) + 1

# print('abxx', round(7/4))
# print('make_1_task(without_gil, start, end) =', make_1_task(without_gil, start, end))
# print('make_4_tasks(with_gil, start, end)   =', make_4_tasks(with_gil, start, end))

# exit(0)

single_wo_gil = 0 
multi_wo_gil = 0
single_with_gil = 0
multi_with_gil = 0

total_run = 100

for i in range(total_run):

    start_time = time.time()
    result = make_1_task(without_gil, start, end)
    if result != 1:
        print('[FAILED]result error, {result} != 1, AAA')
        exit(1)
    single_wo_gil += (time.time() - start_time)


    start_time = time.time()
    result = make_4_tasks(without_gil, start, end)
    if result != 4:
        print('[FAILED]result error, {result} != 4, BBB')
        exit(1)
    multi_wo_gil += (time.time() - start_time)


    start_time = time.time()
    result = make_1_task(with_gil, start, end)
    if result != 1:
        print('[FAILED]result error, {result} != 1, CCC')
        exit(1)
    single_with_gil += (time.time() - start_time)


    start_time = time.time()
    result = make_4_tasks(with_gil, start, end)
    if result != 4:
        print('[FAILED]result error, {result} != 4, DDD')
        exit(1)
    multi_with_gil += (time.time() - start_time)

print("single_wo_gil   : ", single_wo_gil)
print("multi_wo_gil    : ", multi_wo_gil)
print("single_with_gil : ", single_with_gil)
print("multi_with_gil  : ", multi_with_gil)
'''
single_wo_gil   :  0.3003513813018799
multi_wo_gil    :  1.2849419116973877
single_with_gil :  0.14270901679992676
multi_with_gil  :  1.3111813068389893

Latest: (reduce multithread overhead)
single_wo_gil   :  0.2520129680633545
multi_wo_gil    :  0.013848304748535156
single_with_gil :  0.08020830154418945
multi_with_gil  :  0.013636589050292969
'''