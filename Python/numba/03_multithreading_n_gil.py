import math
import threading
import numba as nb
import numpy as np
import time

@nb.jit(nopython=True, nogil=False)
def without_gil(output : list, x, y):
    sum_num = 0
    for i in range(x,y):
        sum_num += i
    output[0] = sum_num + 1

@nb.jit(nopython=True, nogil=True)
def with_gil(output : list, x, y):
    sum_num = 0
    for i in range(x,y):
        sum_num += i
    output[0] = sum_num + 1

def make_1_task(kernel, s, e):
    out = [0]
    kernel(out, s, e)
    return out[0]

def make_4_tasks(kernel, s, e):
    outputs = [[0], [0], [0], [0]]
    all_len = e - s
    step = round(all_len/4)

    threads = []
    threads.append(threading.Thread(target = kernel, args = (outputs[0], s,              s + (step * 1))))
    threads.append(threading.Thread(target = kernel, args = (outputs[1], s + (step * 1), s + (step * 2))))
    threads.append(threading.Thread(target = kernel, args = (outputs[2], s + (step * 2), s + (step * 3))))
    threads.append(threading.Thread(target = kernel, args = (outputs[3], s + (step * 3), e)))
    for i in range(4):
        threads[i].start()
    for i in range(4):
        threads[i].join()
    # print('x', s,              s + (step * 1))
    # print('x', s + (step * 1), s + (step * 2))
    # print('x', s + (step * 2), s + (step * 3))
    # print('x', s + (step * 3), e)
    # print(outputs[0][0])
    # print(outputs[1][0])
    # print(outputs[2][0])
    # print(outputs[3][0])
    # print(outputs[0][0] + outputs[1][0] + outputs[2][0] + outputs[3][0])
    return outputs[0][0] + outputs[1][0] + outputs[2][0] + outputs[3][0]
    # args[0] = outputs[0][0] + outputs[1][0] + outputs[2][0] + outputs[3][0]

start = -(1000*1000*1000*1000*100)
end   =  (1000*1000*1000*1000*100) + 1

# print('abxx', round(7/4))
# print('make_1_task(without_gil, start, end) =', make_1_task(without_gil, start, end))
# print('make_4_tasks(with_gil, start, end)   =', make_4_tasks(with_gil, start, end))

# exit(0)

single_wo_gil = 0 
multi_wo_gil = 0
single_with_gil = 0
multi_with_gil = 0

total_run = 100

for i in range(total_run):

    start_time = time.time()
    result = make_1_task(without_gil, start, end)
    if result != 1:
        print('[FAILED]result error, {result} != 1, AAA')
        exit(1)
    single_wo_gil += (time.time() - start_time)


    start_time = time.time()
    result = make_4_tasks(without_gil, start, end)
    if result != 4:
        print('[FAILED]result error, {result} != 4, BBB')
        exit(1)
    multi_wo_gil += (time.time() - start_time)


    start_time = time.time()
    result = make_1_task(with_gil, start, end)
    if result != 1:
        print('[FAILED]result error, {result} != 1, CCC')
        exit(1)
    single_with_gil += (time.time() - start_time)


    start_time = time.time()
    result = make_4_tasks(with_gil, start, end)
    if result != 4:
        print('[FAILED]result error, {result} != 4, DDD')
        exit(1)
    multi_with_gil += (time.time() - start_time)

print("single_wo_gil   : ", single_wo_gil)
print("multi_wo_gil    : ", multi_wo_gil)
print("single_with_gil : ", single_with_gil)
print("multi_with_gil  : ", multi_with_gil)
'''
single_wo_gil   :  0.29978132247924805
multi_wo_gil    :  1.1269683837890625
single_with_gil :  0.13384079933166504
multi_with_gil  :  1.0991425514221191

Latest: (reduce multithread overhead)       threading is faster than concurrent.futures.ThreadPoolExecutor !!!
single_wo_gil   :  0.23613190650939941
multi_wo_gil    :  0.01134800910949707
single_with_gil :  0.07756924629211426
multi_with_gil  :  0.01129770278930664
'''