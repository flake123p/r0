#
# https://numba.readthedocs.io/en/stable/user/jitclass.html
#

import numpy as np
from numba import int32, float32    # import the types
from numba.experimental import jitclass
import MyUtils as ut
Prof = ut.ProfilingManager
pf = ut.ProfilingFunc
N = range(3)

spec = [
    ('r',     int32),               # a simple scalar field
    ('c',     int32),               # a simple scalar field
    ('array', float32[:,:]),        # an 2d array field
    # ('array', float32[:]),          # an 1d array field
]

@jitclass(spec)
class BagJit(object):
    def __init__(self, r, c):
        self.r = r
        self.c = c
        self.array = np.zeros((r, c), dtype=np.float32)

    def increment(self, val):
        for i in range(self.r):
            for j in range(self.c):
                self.array[i][j] += val
        return self.array

class Bag(object):
    def __init__(self, r, c):
        self.r = r
        self.c = c
        self.array = np.zeros((r, c), dtype=np.float32)

    def increment(self, val):
        for i in range(self.r):
            for j in range(self.c):
                self.array[i][j] += val
        return self.array


r = 1000*1
c = 1000*1
mybag = Bag(r, c)
mybagJit = BagJit(r, c)

if 1:
    pf(lambda : [mybag.increment(1) for _ in N], 'basic')
    pf(lambda : [mybagJit.increment(1) for _ in N], 'jit')

print(mybag.array[33][33])
print(mybagJit.array[33][33])

'''
basic                                             Elapsed Time = 1094.798565 ms
jit                                               Elapsed Time = 112.725496 ms
1.0
1.0

basic                                             Elapsed Time = 2086.743116 ms
jit                                               Elapsed Time = 115.541935 ms
2.0
2.0

basic                                             Elapsed Time = 3075.073957 ms
jit                                               Elapsed Time = 114.568472 ms
3.0
3.0
'''