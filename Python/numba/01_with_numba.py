import time
from numba import jit

@jit
def sum_cal(x,y):
    sum_num = 0
    for i in range(x,y):
        sum_num += i
    return sum_num

start_time = time.time()

print(sum_cal(-100000000, 100000000+1))
print('Time used: {} sec'.format(time.time()-start_time))
'''
0
Time used: 0.18717527389526367 sec
'''