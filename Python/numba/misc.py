import sys

def OTHER_ASSERT(condi):
    if __debug__:
        if condi == False:
            from inspect import getframeinfo, stack
            caller = getframeinfo(stack()[1][0])
            print('[FAILED] ASSERT at {} : line {}.'.format(caller.filename, caller.lineno))
            sys.exit(1)