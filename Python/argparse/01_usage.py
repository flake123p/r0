import argparse

parser = argparse.ArgumentParser('MY_USAGE')

args = parser.parse_args()

#
# python 01_usage.py -h
#
'''
usage: MY_USAGE [-h]

optional arguments:
  -h, --help  show this help message and exit
'''