#
# 1. Python 3.9+
# 2. Create both enable and disable flags, e.g.: --verbose --no-verbose
# 3. Default value = None
#
import argparse

# Create the parser
parser = argparse.ArgumentParser(description="Example of BooleanOptionalAction")

# Add a boolean argument
parser.add_argument(
    "--verbose",
    action=argparse.BooleanOptionalAction,
    default=False,
    help="Enable or disable verbose mode",
)

# Parse the arguments
args = parser.parse_args()

# Use the argument
if args.verbose:
    print("Verbose mode is enabled.")
else:
    print("Verbose mode is disabled.")

#
# python 08_BooleanOptionalAction.py -h
#
'''
usage: 08_BooleanOptionalAction.py [-h] [--verbose | --no-verbose]

Example of BooleanOptionalAction

optional arguments:
  -h, --help            show this help message and exit
  --verbose, --no-verbose
                        Enable or disable verbose mode (default: False)
'''

#
# python 08_BooleanOptionalAction.py
#
'''
Verbose mode is disabled.
'''

#
# python 08_BooleanOptionalAction.py --verbose
#
'''
Verbose mode is enabled.
'''

#
# python 08_BooleanOptionalAction.py --no-verbose
#
'''
Verbose mode is disabled.
'''