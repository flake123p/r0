import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
    "-a", "--age", 
    type=int,
    choices=[18, 60],
    help="Your Age",
    default=25)
args = parser.parse_args()

print(f"Age = {args.age}!")

#
# python 07_optional_choices.py -h
#
'''
usage: 07_optional_choices.py [-h] [-a {18,60}]

optional arguments:
  -h, --help            show this help message and exit
  -a {18,60}, --age {18,60}
                        Your Age
'''

#
# python 07_optional_choices.py
#
'''
Age = 25!
'''

#
# python 07_optional_choices.py -a 88
#
'''
usage: 07_choices.py [-h] [-a {18,60}]
07_choices.py: error: argument -a/--age: invalid choice: 88 (choose from 18, 60)
'''

#
# python 07_optional_choices.py -a 60
#
'''
Age = 60!
'''