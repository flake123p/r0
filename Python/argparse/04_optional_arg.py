import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-a", "--age", type=int, help="Your age", default=25)
args = parser.parse_args()

print(f"Hello {args.age}!")

#
# python 04_optional_arg.py -h
#
'''
usage: 04_optional_arg.py [-h] [-a AGE]

optional arguments:
  -h, --help         show this help message and exit
  -a AGE, --age AGE  Your age
'''

#
# python 04_optional_arg.py
#
'''
Hello 25!
'''