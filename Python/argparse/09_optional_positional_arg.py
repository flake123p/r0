#
# 1. nargs="?": Indicates that the positional argument is optional.
# 2. default="99": Sets the default value if the argument is not provided.
#
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("name", help="Your name")
parser.add_argument("age", nargs="?", help="Your Age", default="99")
args = parser.parse_args()

print(f"Hello {args.name}, {args.age}!")

#
# python 09_optional_positional_arg.py -h
#
'''
usage: 09_optional_positional_arg.py [-h] name [age]

positional arguments:
  name        Your name
  age         Your Age

optional arguments:
  -h, --help  show this help message and exit
'''

#
# python 09_optional_positional_arg.py bob
#
'''
Hello bob, 99!
'''

#
# python 09_optional_positional_arg.py bob 77
#
'''
Hello bob, 77!
'''