import argparse

parser = argparse.ArgumentParser()
parser.add_argument("name", help="Your name")
parser.add_argument("age", help="Your Age")
args = parser.parse_args()

print(f"Hello {args.name}, {args.age}!")

#
# python 03_positional_arg.py -h
#
'''
usage: 03_positional_arg.py [-h] name age

positional arguments:
  name        Your name
  age         Your Age

optional arguments:
  -h, --help  show this help message and exit
'''

#
# python 03_positional_arg.py bob 30
#
'''
Hello bob, 39!
'''

#
# python 03_positional_arg.py 39 bob
#
'''
Hello 39, bob!
'''