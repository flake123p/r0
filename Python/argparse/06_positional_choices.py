import argparse

parser = argparse.ArgumentParser()
parser.add_argument("name", help="Your name")
parser.add_argument(
    "age", 
    type=int,
    choices=[18, 60],
    help="Your Age")
args = parser.parse_args()

print(f"Hello {args.name}, {args.age}!")

#
# python 06_positional_choices.py -h
#
'''
usage: 06_positional_choices.py [-h] name {18,60}

positional arguments:
  name        Your name
  {18,60}     Your Age

optional arguments:
  -h, --help  show this help message and exit
'''

#
# python 06_positional_choices.py bob 18
#
'''
Hello bob, 18!
'''

#
# python 06_positional_choices.py bob 25
#
'''
usage: 06_positional_choices.py [-h] name {18,60}
06_positional_choices.py: error: argument age: invalid choice: 25 (choose from 18, 60)
'''