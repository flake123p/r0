import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
    "--mp-index", 
    type=int,
    help="PID, index for MP.",
    default=0)
args = parser.parse_args()

print(f"MP Index = {args.mp_index}")
pid = args.__dict__['mp_index']
print(f"MP Index = {pid}")

'''

  Name: --mp-index.
  
  Using it: mp_index.

'''