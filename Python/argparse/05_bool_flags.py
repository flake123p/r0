import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-v", "--verbose", action="store_true", help="Enable verbose mode", default=True)

args = parser.parse_args()

if args.verbose:
    print("Verbose mode is enabled!")
else:
    print("Verbose mode is disabled.")

#
# python 05_bool_flags.py -h
#
'''
usage: 05_bool_flags.py [-h] [-v]

optional arguments:
  -h, --help     show this help message and exit
  -v, --verbose  Enable verbose mode
'''

#
# python 05_bool_flags.py
#
'''
Verbose mode is disabled.
'''

#
# python 05_bool_flags.py -v
#
'''
Verbose mode is enabled!
'''