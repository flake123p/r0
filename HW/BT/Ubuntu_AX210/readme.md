# Cannot activate bluetooth on Ubuntu 22.04
https://askubuntu.com/questions/1484396/cannot-activate-bluetooth-on-ubuntu-22-04

1. Restart Bluetooth Service:

sudo systemctl restart bluetooth

2. Install Bluetooth Manager (Optional):

sudo apt-get install blueman

3. Update Your System:

sudo apt-get update

sudo apt-get upgrade

4. Check Bluetooth Status:

rfkill list

5. Reset Bluetooth Settings:

sudo rm -r /var/lib/bluetooth/
sudo systemctl restart bluetooth

6. Verify Bluetooth Modules:

lsmod | grep bluetooth

7. Check Logs for Errors:

journalctl | grep -i bluetooth



# Failed to start Bluetooth service
https://superuser.com/questions/1716226/failed-to-start-bluetooth-service

    sudo install -dm700 /var/lib/bluetooth



# Find bluetooth manager in menu after the installation above:

Search in your PC: Bluetooth Manager