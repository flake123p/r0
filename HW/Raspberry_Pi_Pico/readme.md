
# Bring up
https://projects.raspberrypi.org/en/projects/getting-started-with-the-pico/2
1. install Thonny (windows)
2. (One time) Press BOOTSEL button -> connect PICO usb to PC
3. (One time) Thonny GUI -> Down Right -> Install CircuitPython
4. Don't press BOOTSEL button -> connect to PC
5. Program main.py(auto start) with THonny02
More:
1. Download "Bundle for Version 9.x" and unzip:
    https://circuitpython.org/libraries
2. Copy adafruit_hid/ to lib/ folder of Pi_Pico.


# Keyboard API (press/release)
https://docs.circuitpython.org/projects/hid/en/latest/api.html



# PL2303 USB to TTL
https://www.meiyagroup.com.tw/product/pl2303-usb-to-ttl-%E9%80%A3%E6%8E%A5%E7%B7%9A/

RED: VCC(5V)
BLACK: GND (to pico PIN3)
GREEN: TXD (to pico RX, PIN2)
WHITE: RXD (to pici TX, PIN1 - first pin)



# Linux rx test
https://askubuntu.com/questions/1490582/how-to-read-data-from-a-serial-port

sudo apt-get install minicom
sudo dmesg | grep tty
sudo minicom -b 115200 -o -D /dev/ttyUSB0



# byte to int
d = int.from_bytes(data, 'big')