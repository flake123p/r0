
#include "common.hpp"

class Role2: public Common {
public:
    void Run(){};

    void Run1F_x7(int right_run=7, int left_run=7) {
        printf("%s() ... \n", __func__);
        RunRight(right_run);
        RunLeft(left_run);
    };

    void Run2F_00() {
        printf("%s() ... \n", __func__);
        RightOn();
        JJHit();
        RightOff();

        Rope(); //2F
        WaitMs(800);
        Ctrl();
        WaitMs(900);

        RightOn();
        WaitMs(1800);
        Ctrl();
        RightOff();
        WaitMs(900);

        Run1F_x7(5, 7);
    };

    void Run2F_01() {
        printf("%s() ... \n", __func__);
        RightOn();
        JJHit();
        RightOff();

        Rope(); //2F
        WaitMs(800);
        Ctrl();
        WaitMs(900);

        RightOn();
        WaitMs(1600);
        Ctrl();
        RightOff();
        WaitMs(900);

        RunRight(3);

        WaitMs(1200);
        RopeCtrl();

        LeftCtrl(1500);

        RopeCtrl();
        WaitMs(1000);

        RunLeft(6, 1500);
    };

    void Run3F_00() {
        printf("%s() ... \n", __func__);
        RightOn();
        JJHit();
        RightOff();

        Rope(); //2F
        WaitMs(800);
        Ctrl();
        WaitMs(900);

        LeftOn();
        Ctrl();
        LeftOff();
        WaitMs(900);

        RightOn();
        Ctrl();
        RightOff();
        WaitMs(900);

        Rope(); //3F
        WaitMs(1000);
        Ctrl();
        WaitMs(1500);

        // LeftCtrl(1200);
        DirectAndAct(KEY_LEFT, KEY_CTRL, 800);

        Run1F_x7(5, 7);
        // LeftOn();
        // JJHit();
        // JJHit();
        // LeftOff();
    };

    void Run3F_01() {
        printf("%s() ... \n", __func__);
        RunRight(7);
        RunLeft(1);

        WaitMs(1200);
        RopeCtrl(1500); // 2F

        // LeftCtrl(1500);
        DirectAndAct(KEY_LEFT, KEY_CTRL, 800);
        DirectAndAct(KEY_RIGHT, KEY_CTRL, 1500);

        RopeCtrl();
        WaitMs(1000);
        DirectAndAct(KEY_LEFT, KEY_CTRL, 800);

        RunLeft(6, 1500);
    }
};

int main()
{
    // std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    // usleep((useconds_t)(1000 * 1000));
    // std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

    // std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count()/1000 << "[ms]" << std::endl;
    // printf("since : %ld\n", since_sec(begin).count());
    // return 0;
    class Role2 ins;
    ins.time_max = 15 * 60 + 30; // seconds

    ins.rand_threshold = 66;
    ins.jjhithit_en = 0;
    
    ins.rope = KEY_5;

    ins.t0.en = 1;
    ins.t0.act = KEY_6;
    ins.t0.init_sec = 173;
    ins.t0.duration = 183;

    ins.t1.en = 1;
    ins.t1.act = KEY_1;
    ins.t1.init_sec = 120;
    ins.t1.duration = 180;

    ins.t2.en = 1;
    ins.t2.act = KEY_7;
    ins.t2.init_sec = 60;
    ins.t2.duration = 120;

    ins.t3.en = 1;
    ins.t3.act = KEY_HOME;
    ins.t3.init_sec = 18;
    ins.t3.duration = 20;

    ins.act00_en = 1;
    ins.act00_num = 3;
    ins.act00_code = KEY_SHIFT;

    // ins.rand10_bb_en = 0;
    // ins.rand10_bb_act0 = KEY_INSERT;
    // ins.rand10_bb_act0 = KEY_PAGE_DOWN;
    // ins.rand10_bb_add_delay = 200;

    // ins.rand_00_num = 3;
    // ins.rand_00_key0 = KEY_DEL;
    // ins.rand_00_key1 = KEY_DEL;
    // ins.rand_00_key2 = KEY_DEL;


    // ins.Send(0x35);
    // return 0;
    // ins.rand10[1] = KEY_WAVE;
    // ins.rand10[2] = KEY_WAVE;
    // ins.rand10[3] = KEY_WAVE;
    // ins.rand10[4] = KEY_WAVE;
    // ins.rand10[5] = KEY_CTRL;
    // ins.rand10[6] = KEY_CTRL;
    // ins.rand10[7] = KEY_CTRL;
    // ins.rand10[8] = KEY_CTRL;
    // ins.rand10[9] = KEY_SHIFT;
    // ins.rand10[0] = KEY_SHIFT;



    // ins.rand_00_key0 = 0x35; // `
    // // ins.rand_00_key1 = 0x22; // 5
    // ins.rand_00_key1 = 0x04; // A
    // ins.rand_00_key2 = 0x23; // 6

    while(1) {
        if (ins.RandInt100()>0) ins.Run3F_01();
        if (ins.RandInt100()>50) ins.Run1F_x7();
        if (ins.RandInt100()>30) ins.Run2F_00();
        if (ins.RandInt100()>30) ins.Run2F_01();
        if (ins.RandInt100()>10) ins.Run3F_00();
    }

    return 0;
}