
#include "common.hpp"
#include "script00.hpp"

void init_rand10_1(class Script00 &ins)
{
    ins.rand_threshold = 75;

    // ins.Send(0x35);
    // return 0;
    ins.rand10[3] = KEY_INSERT;
    ins.rand10[4] = KEY_PAGE_DOWN;
    ins.rand10[5] = KEY_PAGE_DOWN;
    ins.rand10[6] = KEY_PAGE_UP;
    ins.rand10[7] = KEY_1;
    ins.rand10[8] = KEY_1;
}

void init_rand10_2(class Script00 &ins)
{
    ins.rand_threshold = 78;

    // ins.Send(0x35);
    // return 0;
    ins.rand10[0] = KEY_INSERT;
    ins.rand10[1] = KEY_INSERT;
    ins.rand10[2] = KEY_INSERT;
    ins.rand10[3] = KEY_INSERT;
    ins.rand10[4] = KEY_INSERT;
    ins.rand10[5] = KEY_INSERT;
    ins.rand10[6] = KEY_INSERT;
    ins.rand10[7] = KEY_INSERT;
    ins.rand10[8] = KEY_INSERT;
    ins.rand10[9] = KEY_INSERT;
}

int main()
{
    class Script00 ins;
    ins.time_max = 15 * 60 + 30; // seconds
    // ins.rand_00_num = 4;

    ins.t0.en = 1;
    ins.t0.act = KEY_6;
    ins.t0.init_sec = 180;
    ins.t0.duration = 180;

    // // ins.Send(0x35);
    // // return 0;
    // ins.rand10[3] = KEY_INSERT;
    // ins.rand10[4] = KEY_PAGE_DOWN;
    // ins.rand10[5] = KEY_PAGE_DOWN;
    // ins.rand10[6] = KEY_PAGE_UP;
    // ins.rand10[7] = KEY_1;
    // ins.rand10[8] = KEY_1;

    // ins.rand_00_key0 = 0x4B; // page up
    // ins.rand_00_key1 = 0x1E; // 1
    // ins.rand_00_key2 = 0x4E; // page down

    ins.rand_00_num = 4;
    ins.rand_00_key0 = KEY_WAVE; // `
    // ins.rand_00_key1 = 0x22; // 5
    ins.rand_00_key1 = KEY_A;
    ins.rand_00_key2 = KEY_5;
    ins.rand3BBHit_en = 1;
    ins.rand10_bb_en = 1;

    while(1) {
        if (ins.RandInt100()>10) { init_rand10_1(ins); ins.Run3F_01(); }
        if (ins.RandInt100()>40) { init_rand10_2(ins); ins.Run1F_2loop(); }
        if (ins.RandInt100()>0)  { init_rand10_1(ins); ins.Run2F_00(); }
        if (ins.RandInt100()>10) { init_rand10_1(ins); ins.Run3F_00(); }
        if (ins.RandInt100()>40) { init_rand10_2(ins); ins.Run1F_x7(); }
        if (ins.RandInt100()>0)  { init_rand10_1(ins); ins.Run2F_01(); }
    }

    return 0;
}