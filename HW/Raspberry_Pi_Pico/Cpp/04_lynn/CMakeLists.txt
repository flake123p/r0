cmake_minimum_required(VERSION 3.6)

project(main)

#file(GLOB_RECURSE SRC CONFIGURE_DEPENDS "*.c" "*.cpp" "*.cxx" "*.cu") 
file(GLOB SRC CONFIGURE_DEPENDS "*.cpp" "*.cxx" "*.cc" "../00_common/*.cpp")
#AUX_SOURCE_DIRECTORY(src SRC)

SET_SOURCE_FILES_PROPERTIES(${SRC} PROPERTIES LANGUAGE CXX )

add_executable( a.out ${SRC})

target_include_directories(
    a.out PUBLIC
    "${PROJECT_SOURCE_DIR}"
    "${PROJECT_SOURCE_DIR}/../00_common"
)

target_link_libraries(a.out PUBLIC)