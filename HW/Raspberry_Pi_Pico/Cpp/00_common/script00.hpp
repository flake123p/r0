#pragma once

#include "common.hpp"

class Script00: public Common {
public:
    void Run(){};

    void Run1F_x7(int right_run=7, int left_run=7) {
        printf("%s() ... \n", __func__);
        RunRight(right_run);
        RunLeft(left_run);
    };

    void Run1F_2loop() {
        printf("%s() ... \n", __func__);
        RunRight(7);
        RunLeft(5);
        RunRight(5);
        RunLeft(7);
    };

    void Run2F_00() {
        printf("%s() ... \n", __func__);
        RightOn();
        JJHit();
        RightOff();

        RopeCtrl(800); //2F

        RightOn();
        WaitMs(1800);
        Ctrl();
        RightOff();
        WaitMs(900);

        Run1F_x7(5, 7);
    };

    void Run2F_01() {
        printf("%s() ... \n", __func__);
        RightOn();
        JJHit();
        RightOff();

        RopeCtrl(800); //2F

        LeftOn();
        WaitMs(300);
        Ctrl();
        RightOff();
        WaitMs(400);

        RightOn();
        WaitMs(1400);
        Ctrl();
        RightOff();
        WaitMs(900);

        RunRight(3);

        WaitMs(500);
        RopeCtrl();

        LeftCtrl(800);

        RopeCtrl();
        WaitMs(500);

        RunLeft(5, 1200);
    };

    void Run3F_00() {
        printf("%s() ... \n", __func__);
        RightOn();
        JJHit();
        RightOff();

        RopeCtrl(800); //2F

        LeftOn();
        Ctrl();
        LeftOff();
        WaitMs(900);

        RightOn();
        Ctrl();
        RightOff();
        WaitMs(900);

        RopeCtrl(1000); //3F

        // LeftCtrl(1200);
        DirectAndAct(KEY_LEFT, KEY_CTRL, 800);

        Run1F_x7(5, 7);
        // LeftOn();
        // JJHit();
        // JJHit();
        // LeftOff();
    };

    void Run3F_01() {
        printf("%s() ... \n", __func__);
        RunRight(7);
        RunLeft(1);

        WaitMs(1000);
        RopeCtrl(1300); // 2F

        // LeftCtrl(1500);
        DirectAndAct(KEY_LEFT, KEY_CTRL, 800);
        DirectAndAct(KEY_RIGHT, KEY_CTRL, 1500);

        RopeCtrl();
        WaitMs(800);
        DirectAndAct(KEY_LEFT, KEY_CTRL, 800);

        RunLeft(3, 1300);
        DirectAndAct(KEY_LEFT, KEY_CTRL, 800);
        RunLeft(1, 1500);
    }
};