#pragma once

// C library headers
#include <stdio.h>
#include <string.h>

// Linux headers
#include <fcntl.h> // Contains file controls like O_RDWR
#include <errno.h> // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <unistd.h> // write(), read(), close()
#include <stdlib.h>
#include <time.h>


#include <iostream>

#include <chrono>
template <
    class result_t   = std::chrono::seconds,
    class clock_t    = std::chrono::steady_clock,
    class duration_t = std::chrono::seconds
>
auto since_sec(std::chrono::time_point<clock_t, duration_t> const& start)
{
    return std::chrono::duration_cast<result_t>(clock_t::now() - start);
}


#define KEY_RIGHT 0xF0
#define KEY_LEFT 0xF2
#define KEY_DOWN 0xF4
#define KEY_UP 0xF6

#define KEY_CTRL 0xE0
#define KEY_SHIFT 0xE1
#define KEY_INSERT 0x49

#define KEY_PAGE_UP 0x4B
#define KEY_PAGE_DOWN 0x4E
#define KEY_DEL 0x4C
#define KEY_HOME 0x4A

#define KEY_WAVE 0x35

#define KEY_F4 0x3D
#define KEY_F5 0x3E

#define KEY_A 0x04
#define KEY_B 0x05
#define KEY_C 0x06
#define KEY_D 0x07

#define KEY_1 0x1E
#define KEY_2 0x1F
#define KEY_3 0x20
#define KEY_4 0x21
#define KEY_5 0x22
#define KEY_6 0x23
#define KEY_7 0x24
#define KEY_8 0x25
#define KEY_9 0x26
#define KEY_0 0x27

class CountDown {
public:
    int en = 0;
    int act = 0;
    long int init_sec = 0;
    long int duration = 1000000;
    std::chrono::steady_clock::time_point begin;
    //return 1 for execution
    int update(int &ret_act) {
        if (en == 0) {
            return 0;
        }
        long int diff = since_sec(begin).count() + init_sec;
        if (diff > duration) {
            begin = std::chrono::steady_clock::now();
            ret_act = this->act;
            init_sec = 0;
            return 1;
        }
        return 0;
    };
    CountDown() {
        begin = std::chrono::steady_clock::now();
    }
};

class Common {
public:
    int ret;
    int serial_port;
    struct termios tty;
    unsigned char msg[1];

    int rand_ctr = 0;
    int rand3BBHit_en = 0;
    int rand_00_num = 9999;
    int rand_00_key0 = KEY_CTRL; //ctrl
    int rand_00_key1 = KEY_CTRL; //ctrl
    int rand_00_key2 = KEY_CTRL; //ctrl
    int rand_00_idx = 0;

    int rand_01_num = 9999;
    int rand_01_key0 = KEY_CTRL; //ctrl
    int rand_01_key1 = KEY_CTRL; //ctrl
    int rand_01_key2 = KEY_CTRL; //ctrl
    int rand_01_idx = 0;

    int rand_threshold = 75;
    int rope = KEY_INSERT;
    int act_ctr = 0;
    int act00_en = 0;
    int act00_num = 9999;
    int act00_code= KEY_CTRL;
    int jjhithit_en = 0;
    int time_ctr = 0;
    int time_chk = 3;
    int time_max = 15 * 60 - 30; // seconds
    int time_out_act = KEY_F5;

    int rand10[10] = {
        KEY_CTRL, KEY_CTRL, KEY_CTRL, KEY_CTRL, KEY_CTRL,
        KEY_CTRL, KEY_CTRL, KEY_CTRL, KEY_CTRL, KEY_CTRL,
    };
    int rand10_bb_en = 0;
    int rand10_bb_add_delay = 0;
    int rand10_bb_act0 = KEY_CTRL;
    int rand10_bb_act1 = KEY_CTRL;
    // int rand10_bb_act2 = KEY_CTRL;

    class CountDown t0;
    class CountDown t1;
    class CountDown t2;
    class CountDown t3;

    Common();
    ~Common();
    virtual void Run() = 0;

    int Rand10Get() {
        int idx = RandInt100(10);
        return rand10[idx];
    }
    int Rand10FinishChech(int finish) {
        if (rand10_bb_en) {
            return 0;
        }
        return finish;
    }

    void Rand10BB(int finish=900) {
        if (rand10_bb_en) {
            Send(rand10_bb_act0);
            Send(rand10_bb_act1);
            // Send(rand10_bb_act2);
            WaitMs(finish + rand10_bb_add_delay);
        }
    }

    int RandInt100(int mod=100) {
        return rand() % mod;
    }

    void Send(unsigned char msg_to_send) {
        // if ((int)msg_to_send == rope) {
        //     RopeCtrl();
        // } else {
            msg[0] = msg_to_send;
            write(serial_port, msg, sizeof(msg));
        // }
    };

    void CancelAll() {
        printf("Cancel ALL\n");
        Send(0xFF);
    };

    void RightOn (int delay=100) { Send(0xF0);WaitMs(delay); };
    void RightOff(int delay=100) { Send(0xF1);WaitMs(delay); };
    void LeftOn  (int delay=100) { Send(0xF2);WaitMs(delay); };
    void LeftOff (int delay=100) { Send(0xF3);WaitMs(delay); };
    void DownOn  (int delay=100) { Send(0xF4);WaitMs(delay); };
    void DownOff (int delay=100) { Send(0xF5);WaitMs(delay); };
    void UpOn    (int delay=100) { Send(0xF6);WaitMs(delay); };
    void UpOff   (int delay=100) { Send(0xF7);WaitMs(delay); };
    void DirectOn (int direct, int delay=100) { Send(direct);WaitMs(delay); };
    void DirectOff(int direct, int delay=100) { Send(direct+1);WaitMs(delay); };

    void WaitMs(int ms) {
        if (ms) {
            usleep((useconds_t)(1000 * ms));
        }
    };

    void Jump() { Send(0xE2); };
    void JumpX2() { Send(0xE2);WaitMs(100);Send(0xE2);WaitMs(100);Send(0xE2);WaitMs(100); };

    void Ctrl() { Send(0xE0); };
    void Insert() { Send(0x49); };
    void Rope() { Send(rope); };

    std::chrono::steady_clock::time_point begin;
    // std::chrono::steady_clock::time_point end;
    void TimeOut() {
        // WaitMs(1500);
        CancelAll();
        Send(time_out_act);
        WaitMs(1500);
        Send(time_out_act);
        WaitMs(500);
        Send(time_out_act);
        exit(1);
    }

    void TimeCheck() {
        time_ctr += 1;
        if (time_ctr % time_chk != 0) {
            return;
        }
        long int du = since_sec(begin).count();
        printf("du = %ld sec (max:%d)\n", du, time_max);
        if (du > time_max) {
            TimeOut();
        }

        int act = 0;
        if (t0.update(act)) {
            Send(act);
            WaitMs(800);
        }
        if (t1.update(act)) {
            Send(act);
            WaitMs(800);
        }
        if (t2.update(act)) {
            Send(act);
            WaitMs(800);
        }
        if (t3.update(act)) {
            Send(act);
            WaitMs(800);
        }
    }

    void ActCheck() {
        act_ctr++;
        if (act00_en) {
            if (act_ctr % act00_num == 0) {
                Send(act00_code);
                WaitMs(800);
            }
        }

        // if (act_ctr % time_chk == 0) {
        //     TimeCheck();
        // }
    }

    void JJHit(int wait = 800) {
        WaitMs(100);
        JumpX2();
        Ctrl();
        if (jjhithit_en) {
            WaitMs(500);
            Ctrl();
        }
        WaitMs(wait + 0);
        ActCheck();
        TimeCheck();
    };

    void RunRight(int right_run, int wait = 800) {
        RightOn();
        for(int i = 0; i < right_run; i++) {
            JJHit(wait);
            RandLeftCtrl(1000, rand_threshold);
            RightOn();
        }
        RightOff();
    };

    void RunLeft(int left_run, int wait = 800) {
        LeftOn();
        for(int i = 0; i < left_run; i++) {
            JJHit(wait);
            RandRightCtrl(1000, rand_threshold);
            LeftOn();
        }
        LeftOff();
    };

    void JumpUp() {
        UpOn();
        JumpX2();
        UpOff();
    }

    void InsertUp(int delay=1000) {
        Insert();
        WaitMs(delay);
        Ctrl();
        WaitMs(900);
    }

    void RopeCtrl(int delay=1000, int finish=900) {
        TimeCheck();
        Rope();
        WaitMs(delay);
        Ctrl();
        WaitMs(finish);
        ActCheck();
    }

    void LeftCtrl(int finish=900) {
        LeftOn();
        Ctrl();
        LeftOff();
        WaitMs(finish);
    }

    void RightCtrl(int finish=900) {
        RightOn();
        Ctrl();
        RightOff();
        WaitMs(finish);
    }

    void DirectAct(int direct, int act, int finish=900) {
        DirectOn(direct);
        if ((int)act == rope) {
            RopeCtrl(1200, 0);
        } else {
            Send(act);
        }
        DirectOff(direct);
        WaitMs(finish);
    }

    void DirectAndAct(int direct, int act, int delay, int finish=900) {
        DirectOn(direct);
        WaitMs(delay);
        Send(act);
        DirectOff(direct);
        WaitMs(finish);
        ActCheck();
    }

    void RandLeftCtrl(int finish=900, int threshold=50) {
        if (RandInt100() > threshold) {
            // LeftCtrl(finish);
            DirectAct(KEY_LEFT, Rand10Get(), Rand10FinishChech(finish));
            Rand10BB();
            ActCheck();
            // RightCtrl(finish);
            RandCheck();
        }
    }

    void RandRightCtrl(int finish=900, int threshold=50) {
        if (RandInt100() > threshold) {
            // RightCtrl(finish);
            DirectAct(KEY_RIGHT, Rand10Get(), Rand10FinishChech(finish));
            Rand10BB();
            ActCheck();
            // LeftCtrl(finish);
            RandCheck();
        }
    }

    void RandCheck(int finish=900) {
        rand_ctr++;
        if (rand_ctr % rand_00_num == 0) {
            switch (rand_00_idx)
            {
                case 0:
                    Send(rand_00_key0);
                    rand_00_idx++;
                    break;

                case 1:
                    Send(rand_00_key1);
                    rand_00_idx++;
                    break;

                case 2:
                    Send(rand_00_key2);
                    rand_00_idx = 0;
                    break;
                
                default:
                    rand_00_idx = 0;
                    break;
            }
            if (rand3BBHit_en) {Ctrl();};
            WaitMs(finish);
            TimeCheck();
        }
        if (rand_ctr % rand_01_num == 0) {
            switch (rand_01_idx)
            {
                case 0:
                    Send(rand_01_key0);
                    rand_01_idx++;
                    break;

                case 1:
                    Send(rand_01_key1);
                    rand_01_idx++;
                    break;

                case 2:
                    Send(rand_01_key2);
                    rand_01_idx = 0;
                    break;
                
                default:
                    rand_01_idx = 0;
                    break;
            }
            if (rand3BBHit_en) {Ctrl();};
            WaitMs(finish);
            TimeCheck();
        }
    }
};
