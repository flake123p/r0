#
# Random: https://stackoverflow.com/questions/6088077/how-to-get-a-random-number-between-a-float-range
# 

#
# max = 2
#

import random

def foo(n):
    return n*n

input = [i/10 for i in range(0,31,1)]
output = [foo(i) for i in input]
import matplotlib.pyplot as plt
plt.grid(True)
plt.plot(input, output)
#plt.show()

inside = 0
max = 1000000

for i in range(max):
    x = random.uniform(0, 3)
    y = random.uniform(0, 10)
    if (y <= foo(x)):
        inside+=1

ans = (inside / max) * 30
print(ans)