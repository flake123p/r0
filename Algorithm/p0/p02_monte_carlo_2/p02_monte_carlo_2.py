#
# Random: https://stackoverflow.com/questions/6088077/how-to-get-a-random-number-between-a-float-range
# 

#
# max = 2
#

import random
#import math

inside = 0
max = 1000000

for i in range(max):
    x = random.uniform(0, 2)
    area = x * x
    if area <= 2:
        inside+=1

sqrt2 = 2 * (inside / max)
print(sqrt2)