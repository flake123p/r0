#include <stdio.h>

#include "My_Basics.h"
#include "My_Basics.hpp"

#include "AbsClaList.hpp"
#include "AbsClaTree.hpp"

class MyList00 : public ACList {
public:
	static DLList_Head_t head;
	u32 val;
	MyList00() {pHead = &head;};
	//inline class MyList00 *First(){return (class MyList00 *)ACList::First();}; //override original virtual function
	//inline class MyList00 *Next(){return (class MyList00 *)ACList::Next();};
};

DLList_Head_t MyList00::head = DLLIST_HEAD_INIT(&(MyList00::head));

class MyList01 : public ACList {
public:
	static DLList_Head_t head;
	u32 val;
	MyList01() {pHead = &head;};
};

DLList_Head_t MyList01::head = DLLIST_HEAD_INIT(&(MyList01::head));

int main(int argc, char *argv[])
{
	//
	// print target[] before sort
	//
	printf("[BEFORE]target[] = ");
	printf("\n");
	

	//
	// print target[] after sort
	//
	printf("[AFTER] target[] = ");
	printf("\n");

	class MyList00 obj1, obj2;
	class MyList00 *p00;

	class MyList01 objA;
	class MyList01 *p01;

	objA.InsertLast();
	objA.val = 9999;

	obj1.val = 222;
	obj2.val = 777;

	obj2.InsertLast();
	obj1.InsertLast();

	for (p00 = (class MyList00 *)obj1.First(); !obj1.IsDone(); p00 = (class MyList00 *)obj1.Next()) {
		DNA(p00);
		DND(p00->val);
	}

	for (p01 = (class MyList01 *)objA.First(); !objA.IsDone(); p01 = (class MyList01 *)objA.Next()) {
		DNA(p01);
		DND(p01->val);
	}

	void *pp;
	void (MyList00::* func)(void) = &MyList00::InsertLast;
	pp = (void*&) func; // this works
	DNP(pp);
	void (MyList01::* func2)(void) = &MyList01::InsertLast;
	pp = (void*&) func2; // this works
	DNP(pp);

	return 0;
}
