
import random
import math

def ln(n):
    return math.log(n)

input = [i/10 for i in range(10,101,1)]
output = [ln(i) for i in input]
import matplotlib.pyplot as plt
plt.grid(True)
plt.plot(input, output)
#plt.show()

inside = 0.0
max = 1000000

for i in range(max):
    x = random.uniform(0, 10)
    y = random.uniform(0, 4)
    if x >= 1:
        if (y <= ln(x)):
            inside+=1

ans = (inside / max) * 40
print(ans)