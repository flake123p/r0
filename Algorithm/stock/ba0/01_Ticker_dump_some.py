#
# https://www.geeksforgeeks.org/get-financial-data-from-yahoo-finance-with-python/
#

import yfinance as yahooFinance
 
# Here We are getting Facebook financial information
# We need to pass FB as argument for that
GetFacebookInformation = yahooFinance.Ticker("META")
 
# # whole python dictionary is printed here
# print(GetFacebookInformation.info)


# display Company Sector
print("Company Sector : ", GetFacebookInformation.info['sector'])
 
# display Price Earnings Ratio
print("Price Earnings Ratio : ", GetFacebookInformation.info['trailingPE'])
 
# display Company Beta
print(" Company Beta : ", GetFacebookInformation.info['beta'])
