#
# https://www.geeksforgeeks.org/get-financial-data-from-yahoo-finance-with-python/
#

import yfinance as yahooFinance
 
# in order to specify start date and 
# end date we need datetime package
import datetime
 
# startDate , as per our convenience we can modify
startDate = datetime.datetime(2019, 5, 31)
 
# endDate , as per our convenience we can modify
endDate = datetime.datetime(2021, 1, 30)
GetFacebookInformation = yahooFinance.Ticker("META")
 
# pass the parameters as the taken dates for start and end
h = GetFacebookInformation.history(start=startDate, 
                                     end=endDate)
print(h)
#                                  Open        High         Low       Close    Volume  Dividends  Stock Splits
# Date                                                                                                        
# 2019-05-31 00:00:00-04:00  179.910299  180.169760  176.796702  177.106064  15226500        0.0           0.0
# 2019-06-03 00:00:00-04:00  174.641126  174.691026  160.679810  163.813370  56059600        0.0           0.0
# 2019-06-04 00:00:00-04:00  163.374273  167.934893  160.510149  167.156494  46044300        0.0           0.0
# 2019-06-05 00:00:00-04:00  167.136547  168.374009  164.292400  167.825134  19758300        0.0           0.0
# 2019-06-06 00:00:00-04:00  167.954865  169.351988  166.887052  167.984802  12446400        0.0           0.0
# ...                               ...         ...         ...         ...       ...        ...           ...
# 2021-01-25 00:00:00-05:00  277.569649  279.525621  270.953240  277.439911  19087000        0.0           0.0
# 2021-01-26 00:00:00-05:00  277.569633  284.804766  277.240293  281.471588  19373600        0.0           0.0
# 2021-01-27 00:00:00-05:00  281.950586  282.868712  267.570134  271.581909  35346200        0.0           0.0
# 2021-01-28 00:00:00-05:00  276.611557  286.201865  264.157170  264.456543  37758800        0.0           0.0
# 2021-01-29 00:00:00-05:00  264.755940  266.013366  254.327388  257.800232  30389500        0.0           0.0