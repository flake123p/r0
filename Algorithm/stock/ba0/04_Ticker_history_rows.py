#
# https://www.geeksforgeeks.org/get-financial-data-from-yahoo-finance-with-python/
#

import yfinance as yahooFinance
import pandas as pd
 
GetFacebookInformation = yahooFinance.Ticker("META")
 
pd.set_option('display.max_rows', None)
# Let us  get historical stock prices for Facebook 
# covering the past few years.
# max->maximum number of daily prices available 
# for Facebook.
# Valid options are 1d, 5d, 1mo, 3mo, 6mo, 1y, 2y, 
# 5y, 10y and ytd.
print(GetFacebookInformation.history(period="max"))