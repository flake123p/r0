import numpy as np
import util
import matplotlib.pyplot as plt

# NVDA META INTC MCHP AAPL AMD QCOM AVGO GOOG AMZN

if __name__ == '__main__':
    s = util.get_stock("NVDA")
    s = util.get_stock("META")
    s = util.get_stock("INTC")
    s = util.get_stock("MCHP")
    s = util.get_stock("AAPL")
    s = util.get_stock("AMD")
    s = util.get_stock("QCOM")
    s = util.get_stock("AVGO")
    s = util.get_stock("GOOG")
    s = util.get_stock("AMZN")
