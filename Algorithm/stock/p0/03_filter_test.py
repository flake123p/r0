import numpy as np
import util
from util import BASIC_ASSERT
import matplotlib.pyplot as plt

# -0.109861
# -0.089039
# 0.0322581
# 0.0321875
# -0.0339086
# -0.0962081
# -0.0225381
# 0.0500177
# -0.0635136
# -0.0295815
# -0.0382899
# 0.0363355
# -0.0186498
# 0.0300267
# -0.00332102
# 0.014439
# -0.00474448
# 0.0374038
# 0.0607988
# 0.0466511
# 0.0159185

def find_consecutive_raise_X(out_ndary_list, in_ndary, consec_num, total_num):
    BASIC_ASSERT(len(in_ndary) >= total_num)
    BASIC_ASSERT(total_num >= consec_num)
    state = 0
    for i in range(len(in_ndary) - total_num + 1):
        if state == 0:
            if in_ndary[i] < 0:
                state = 1
        elif state == 1:
            match = True
            for j in range(consec_num):
                if in_ndary[i + j] <= 0:
                    match = False
                    break
            if match:
                # found
                # print('found')
                sub = in_ndary[i : i + total_num]
                # print(the10)
                out_ndary_list.append(sub)
                # i+=11
                state = 0



dbg_cl = None

# return diff
def extract_list10(list10, name ,rise = True):
    global dbg_cl
    s = util.get_stock(name)
    cl = s['Close'].to_numpy()
    dbg_cl = cl
    data_len = len(cl)
    BASIC_ASSERT(data_len >= 2)
    #
    # diff ratio
    #
    percent_array = util.to_percent_array(cl)
    
    #
    # find 2 rises in the roll
    #
    util.find_consecutive(list10, percent_array, 2, 10, rise)
    return percent_array

#
# Test input
# Test output
# Golden output
# Compare Test output with Golden output
#
def TEST_to_percent_array():
    test11 = np.empty([12], dtype = np.float64)
    test11[0] = 100
    test11[1] = 99 # fell 1%
    for i in range(2, 12):
        test11[i] = test11[i-1] * 1.01 # rise 1%
    # print(test11)
    # print(test11.dtype)
    # print(test11.size)
    # print(type(test11))

    test11_per = util.to_percent_array(test11)
    # print(test11_per)

    golden11 = np.array([-0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01], dtype = np.float64)
    # print('cmp = ', np.allclose(test11_per, golden11))
    # Verify
    BASIC_ASSERT(np.allclose(test11_per, golden11))

def TEST_calc_avg():
    test10 = np.array([0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01], dtype = np.float64)
    avg = util.calc_avg(test10, 0, 10)
    # print('avg =', avg)
    BASIC_ASSERT(np.allclose(avg, 0.01))

    test10 = np.array([0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.02, 0.03], dtype = np.float64)
    avg = util.calc_avg(test10, 7, 10)
    # print('avg =', avg)
    BASIC_ASSERT(np.allclose(avg, 0.02))

def TEST_find_consecutive_raise():
    matched_list = []
    test = np.array([
        -0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01,
        -0.01, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.03, 0.04], dtype = np.float64)
    util.find_consecutive(matched_list, test, 2, 10)
    # print(matched_list)
    BASIC_ASSERT(len(matched_list) == 2)
    # avg = util.calc_avg(matched_list[0], 5+2, 8+2)
    # print(avg)
    # avg = util.calc_avg(matched_list[1], 5+2, 8+2)
    # print(avg)
    total_sum = 0.0
    for l in matched_list:
        total_sum += util.calc_avg(l, 5+2, 8+2)
    avg_all = total_sum / len(matched_list)
    # print('avg_all   = ', avg_all)
    BASIC_ASSERT(np.allclose(avg_all, util.calc_avg_2d(matched_list, 5+2, 8+2)))
    # avg_all = util.calc_avg_2d(matched_list, 0+2, 3+2)
    # print('avg_all   = ', avg_all)

def dump_avg(nd_list, diff, name):
    print('avg_1   = ', f'{100.0 * util.calc_avg_2d(nd_list, 0+2, 1+2):.4f}')
    print('avg_2   = ', f'{100.0 * util.calc_avg_2d(nd_list, 0+2, 2+2):.4f}')
    # print('mid_2   = ', f'{100.0 * util.calc_avg_2d(nd_list, 0+1+2, 1+2+2):.4f}')
    print('avg_3   = ', f'{100.0 * util.calc_avg_2d(nd_list, 0+2, 3+2):.4f}')
    print('avg_5   = ', f'{100.0 * util.calc_avg_2d(nd_list, 0+2, 5+2):.4f}')
    print('avg_7   = ', f'{100.0 * util.calc_avg_2d(nd_list, 0+2, 7+2):.4f}')
    print('avg_all = ', f'{100.0 * sum(diff)/len(diff):.4f}', name)

def parsing10(name, accu_list = None, rise = True):
    list10 = []
    diff = extract_list10(list10, name, rise)
    dump_avg(list10, diff, name + '\n')
    if accu_list != None:
        accu_list = accu_list + list10
    return diff, accu_list

if __name__ == '__main__':
    TEST_to_percent_array()
    TEST_calc_avg()
    TEST_find_consecutive_raise()

    DO_RISE_PARSING = False

    accu = []
    diff_all = None
    diff, accu = parsing10("META", accu, DO_RISE_PARSING)
    diff_all = diff
    diff, accu = parsing10("AMZN", accu, DO_RISE_PARSING)
    diff_all = np.append(diff_all, diff)
    diff, accu = parsing10("GOOG", accu, DO_RISE_PARSING)
    diff_all = np.append(diff_all, diff)
    diff, accu = parsing10("NVDA", accu, DO_RISE_PARSING)
    diff_all = np.append(diff_all, diff)

    dump_avg(accu, diff_all, 'ALL')
