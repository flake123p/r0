import numpy as np
import util
import matplotlib.pyplot as plt

# NVDA META INTC MCHP AAPL AMD QCOM AVGO GOOG AMZN

if __name__ == '__main__':
    s = util.get_stock("NVDA", force = True)
    s = util.get_stock("META", force = True)
    s = util.get_stock("INTC", force = True)
    s = util.get_stock("MCHP", force = True)
    s = util.get_stock("AAPL", force = True)
    s = util.get_stock("AMD", force = True)
    s = util.get_stock("QCOM", force = True)
    s = util.get_stock("AVGO", force = True)
    s = util.get_stock("GOOG", force = True)
    s = util.get_stock("AMZN", force = True)
