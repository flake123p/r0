import numpy as np
import util
from util import BASIC_ASSERT
import matplotlib.pyplot as plt

# -0.109861
# -0.089039
# 0.0322581
# 0.0321875
# -0.0339086
# -0.0962081
# -0.0225381
# 0.0500177
# -0.0635136
# -0.0295815
# -0.0382899
# 0.0363355
# -0.0186498
# 0.0300267
# -0.00332102
# 0.014439
# -0.00474448
# 0.0374038
# 0.0607988
# 0.0466511
# 0.0159185

dbg_cl = None

# return diff
def extract_list10(list10, name):
    global dbg_cl
    s = util.get_stock(name)
    cl = s['Close'].to_numpy()
    dbg_cl = cl
    data_len = len(cl)
    BASIC_ASSERT(data_len >= 2)
    #
    # diff ratio
    #
    diff = util.to_percent_array(cl)
    
    #
    # find 2 rises in the roll
    #

    BASIC_ASSERT(len(diff) >= 11)
    for i in range(len(diff)-10):
        if diff[i] > 0:
            continue
        if diff[i+1] > 0 and diff[i+2] > 0:
            # found
            # print('found')
            the10 = diff[i+1 : i+11]
            # print(the10)
            list10.append(the10)
            # i+=11
    
    # print(diff)
    # list10 = list10.append(diff)
    return diff

def avg_calc(list10):
    total_sum = 0.0
    for l in list10:
        sum = 0.0
        for i in range(3):
            sum += l[i+2]
        sum /= 3.0
        total_sum += sum
    total_sum = total_sum / len(list10)
    print('total_sum 3 = ', total_sum * 100.0)

    total_sum = 0.0
    for l in list10:
        sum = 0.0
        for i in range(5):
            sum += l[i+2]
        sum /= 5.0
        total_sum += sum
    total_sum = total_sum / len(list10)
    print('total_sum 5 = ', total_sum * 100.0)

    total_sum = 0.0
    for l in list10:
        sum = 0.0
        for i in range(7):
            sum += l[i+2]
        sum /= 7.0
        total_sum += sum
    total_sum = total_sum / len(list10)
    print('total_sum 7 = ', total_sum * 100.0)

    total_sum = 0.0
    for l in list10:
        sum = 0.0
        for i in range(3):
            sum += l[i+2+3]
        sum /= 3.0
        total_sum += sum
    total_sum = total_sum / len(list10)
    print('total_mid 3 = ', total_sum * 100.0)

if __name__ == '__main__':
    # NVDA META INTC MCHP AAPL AMD QCOM AVGO GOOG AMZN

    list10 = []
    diff = extract_list10(list10, "META")
    # print(list10)
    avg_calc(list10)
    print('avg rises   = ', 100.0 * sum(diff)/len(diff))

    # list10 = []
    # diff = extract_list10(list10, "AMZN")
    # # print(list10)
    # avg_calc(list10)
    # print('avg rises   = ', 100.0 * sum(diff)/len(diff))

    # list10 = []
    # diff = extract_list10(list10, "GOOG")
    # # print(list10)
    # avg_calc(list10)
    # print('avg rises   = ', 100.0 * sum(diff)/len(diff))


