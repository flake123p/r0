import sys
import yfinance as yahooFinance
import numpy as np

def BASIC_ASSERT(condi):
    if condi == False:
        from inspect import getframeinfo, stack
        # tb = sys.exc_info()[-1]
        # lineno = tb.tb_lineno
        # filename = tb.tb_frame.f_code.co_filename
        # print('[FAILED] ASSERT at {} line {}.'.format(filename, lineno))
        caller = getframeinfo(stack()[1][0])
        print('[FAILED] ASSERT at {} : line {}.'.format(caller.filename, caller.lineno))
        sys.exit(1)

def fine_name(name):
    return 'db/' + name + '.pkl'

def pickle_to(var, file):
    import pickle
    with open(file, 'wb') as my_file:
        pickle.dump(var, my_file)

def pickle_from(file):
    import pickle
    try:
        with open(file, 'rb') as my_file:
            return pickle.load(my_file)
    except IOError as exc:    # Python 2. For Python 3 use OSError
        tb = sys.exc_info()[-1]
        lineno = tb.tb_lineno
        filename = tb.tb_frame.f_code.co_filename
        print('{} at {} line {}.'.format(exc.strerror, filename, lineno))
        # sys.exit(exc.errno)
        return None

def get_stock(name, force = False):
    if force:
        do_download = True
    else:
        ret = pickle_from(fine_name(name))
        if type(ret) == type(None):
            do_download = True
        else:
            return ret
    
    if do_download:
        GetFacebookInformation = yahooFinance.Ticker(name)
        ret = GetFacebookInformation.history(period="max")
        pickle_to(ret, fine_name(name))
    return ret

def to_percent_array(np_ary):
    BASIC_ASSERT(type(np_ary) == np.ndarray)
    BASIC_ASSERT(len(np_ary.shape) == 1) # dims check

    ret = np.empty(np_ary.size - 1)

    for i in range(1, np_ary.size):
        ret[i-1] = (np_ary[i] - np_ary[i - 1]) / np_ary[i - 1]
    return ret

def calc_avg(ndary, start, stop):
    BASIC_ASSERT(start >= 0)
    BASIC_ASSERT(stop <= ndary.size)
    sum = 0.0
    for i in range(start, stop):
        sum += ndary[i]
    avg = sum / (stop - start)
    return avg

def calc_avg_2d(ndary_list, start, stop):
    BASIC_ASSERT(start >= 0)
    BASIC_ASSERT(stop <= ndary_list[0].size)
    sum = 0.0
    for ndary in ndary_list:
        sum += calc_avg(ndary, start, stop)
    avg = sum / len(ndary_list)
    return avg

def find_consecutive(out_ndary_list, in_ndary, consec_num, total_num, rise = True):
    BASIC_ASSERT(len(in_ndary) >= total_num)
    BASIC_ASSERT(total_num >= consec_num)

    for i in range(len(in_ndary) - total_num):
        if in_ndary[i] > 0:
            continue
        
        match = True
        for j in range(consec_num):
            if rise:
                if in_ndary[i + 1 + j] <= 0:
                    match = False
                    break
            else:
                if in_ndary[i + 1 + j] >= 0:
                    match = False
                    break
        if match:
            # found
            # print('found')
            sub = in_ndary[i + 1 : i + total_num + 1]
            # print(the10)
            out_ndary_list.append(sub)
            # i+=11