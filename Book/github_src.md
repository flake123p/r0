
# download_ebooks.sh
git clone https://github.com/SuperCV/Book.git
git clone https://github.com/LLMBook-zh/LLMBook-zh.github.io.git
git clone https://github.com/zhangyachen/ComputerArchitectureAndCppBooks.git
git clone https://github.com/sallenkey-wei/cuda-handbook.git
git clone https://github.com/TIM168/technical_books.git

# SuperCV (AI, Stock, STT!!!)
git clone https://github.com/SuperCV/Book.git


# 《大语言模型》 (updated 2024-04-15)
Download:
https://github.com/LLMBook-zh/LLMBook-zh.github.io/tree/main
git clone https://github.com/LLMBook-zh/LLMBook-zh.github.io.git

Intro:
https://blog.csdn.net/wjjc1017/article/details/138460753
    - A Survey of Large Language Models
    - LLMBox
    - YuLan


# CUDA, Paralell Programming
https://github.com/zhangyachen/ComputerArchitectureAndCppBooks

git clone https://github.com/zhangyachen/ComputerArchitectureAndCppBooks.git


# CUDA handbook
git clone https://github.com/sallenkey-wei/cuda-handbook.git


# Tech Books (C++, Android, HTML, AI, Hack ...)
git clone https://github.com/TIM168/technical_books.git