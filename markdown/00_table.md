# Online generator
https://www.tablesgenerator.com/markdown_tables



# Syntax
| Syntax    | Description |
|-----------|-------------|
| Header    | Title       |
| Paragraph | Text        |

| A   | B   | CCC |
| --- | --- | --- |
| F   | F   |     |
| F   | T   |     |
| T   | F   |     |
| T   | T   |     |



# Vscode
    Markdown Table
    Markdown Table Prettifier

  **Table Auto Alignment**
    Mouse selection -> Right Click -> Format Selection [Ctrl+K Ctrl+F]

  **Table Insert**
    Cursor on table -> Right Click -> Markdown Table: add column to the right