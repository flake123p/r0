
# Newline : '\' (in the end)

# Space : '&nbsp'

where \
&nbsp;&nbsp; x0, the x value of the function's midpoint; \
&nbsp;&nbsp; L,  the supremum of the values of the function; \
&nbsp;&nbsp; k,  the logistic growth rate or steepness of the curve.