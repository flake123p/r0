# 1. add these to .vscode/task.json:
        {
            "label": "Run JavaScript File",
            "type": "shell",
            "command": "node",
            "args": [
                "${file}"
            ],
            "group": {
                "kind": "build",
                "isDefault": true
            },
            "problemMatcher": []
        }


# 2. add these to user keybindings.json:
    {
        "key": "alt+q",
        "command": "workbench.action.tasks.runTask",
        "args": "Run JavaScript File"
    }


# 3. open a js source file

    $ alt + q