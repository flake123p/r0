
const fs = require('fs');

// Read a file and log its content
fs.readFile('00_hello_world.js', 'utf8', (err, data) => {
    if (err) {
        console.error("Error reading file:", err);
        return;
    }
    console.log("File content:", data);
});