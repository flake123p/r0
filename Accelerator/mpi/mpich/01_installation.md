
*********************************************************************
#    Build Source / Compile MPI C file / RUN MPI C application      #
*********************************************************************

# Installation (Source)
https://mpitutorial.com/tutorials/installing-mpich2/

# MPICH.org (Download & Doc)
https://www.mpich.org/downloads/

## MPICH.org - PDF: Installation guide
https://www.mpich.org/static/downloads/4.2.0/mpich-4.2.0-installguide.pdf

## MPICH.org - PDF: User guide
https://www.mpich.org/static/downloads/4.2.0/mpich-4.2.0-userguide.pdf

## Build from source (add arguments for fix nvcc error)

Build Error:
    - [ ] nvcc fatal   : Unsupported gpu architecture 'compute_89'

**SOLUTION :**
1. export PATH=/usr/local/cuda-12.3:%PATH
2. ./configure --prefix=/home/pupil/temp/mpich-install/ --with-cuda=/usr/local/cuda-12.3 --enable-g=dbg |& tee z.config.txt
3. make -j12 |& tee z.make.txt
4. make install


# Installation (apt-get)
https://chenhh.gitbooks.io/parallel_processing/content/mpi/mpich_setting.html

    sudo apt-get install mpich libmpich-dev


--
(Misc)
 Hydra
 gforker
Slurm
OSC & PBS
TotalView
BLCR


# Compile

    mpicc hello.c

# Run

    mpiexec -n <number> ./a.out

--

    mpiexec -f machinefile -n <number> ./a.out

    The ’machinefile’ is of the form:
    host1
    host2:2
    host3:4 # Random comments
    host4:1

--

    mpirun -np 4 -hosts localhost ai1@ai1 ./b.out