num=4

if [ "$2" != "" ]; then
    num=$2
fi

echo build : $1, num = $num

mpicc $1 -lm

mpiexec -n $num ./a.out