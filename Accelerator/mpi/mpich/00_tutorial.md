
# Tutorials - Kendall

https://mpitutorial.com/tutorials/


Introduction and MPI installation
- [x] MPI tutorial introduction (中文版)
    - 01_installation.md
- [ ] Installing MPICH2 on a single machine (中文版)
- [ ] Launching an Amazon EC2 MPI cluster
- [ ] Running an MPI cluster within a LAN
- [ ] Running an MPI hello world application (中文版)

Blocking point-to-point communication
- [ ] Sending and receiving with MPI_Send and MPI_Recv (中文版)
- [ ] Dynamic receiving with MPI_Probe and MPI_Status (中文版)
- [ ] Point-to-point communication application - Random walking (中文版)

Basic collective communication
- [ ] Collective communication introduction with MPI_Bcast (中文版)
- [ ] Common collectives - MPI_Scatter, MPI_Gather, and MPI_Allgather (中文版)
- [ ] Application example - Performing parallel rank computation with basic collectives (中文版)

Advanced collective communication
- [ ] Using MPI_Reduce and MPI_Allreduce for parallel number reduction (中文版)
- [ ] Groups and communicators
- [ ] Introduction to groups and communicators (中文版)



# MPICH.org - Flake
https://www.mpich.org/documentation/guides/

Download Box Link : https://anl.app.box.com/v/2019-ANL-MPI

OMP + MPI
OMPI
MPICH
Hadoop
PBS + MPI
MapRecude (NO)


# Examples:

ba0 : Kendal examples
ba1 : builtin example
ba2 : org example