
# Running an MPI Cluster within a LAN
https://mpitutorial.com/tutorials/running-an-mpi-cluster-within-a-lan/

# Step 1: Configure your hosts file

    $ cat /etc/hosts

    $ sudo vim /etc/hosts

Example: master
    172.16.60.108   master
    172.16.60.108   manager
    172.16.60.118   ai1
    172.16.60.108   PUPIL-SERVER

Example: worker
    172.16.60.108   master
    172.16.60.108   manager
    172.16.60.118   ai1
    172.16.60.108   PUPIL-SERVER

Maker Sure Host Names(Machine Name - /etc/hostname) are DIFFERENT !!!
Maker Sure Host Names(Machine Name - /etc/hostname) are DIFFERENT !!!
Maker Sure Host Names(Machine Name - /etc/hostname) are DIFFERENT !!!

# Step 2: Create a new user

    $ sudo adduser mpi
    $ sudo usermod -aG sudo mpi

    ...

    $ ssh-keygen -t rsa

    ... exchange key

    $ vim ~/.ssh/authorized_keys

Refer to SSH- 03_passwordless_login.md for more details !!!
Refer to SSH- 03_passwordless_login.md for more details !!!
Refer to SSH- 03_passwordless_login.md for more details !!!

The premission of .ssh/ is 700 !!!
The premission of .ssh/ is 700 !!!
The premission of .authorized_keys/ is 600 !!!
The premission of .authorized_keys/ is 600 !!!
The premission of /home/mpi can't be 777 !!!
The premission of /home/mpi can't be 777 !!!

# Step 3: Create NFS
https://mpitutorial.com/tutorials/running-an-mpi-cluster-within-a-lan/

Master/Server
    $ sudo apt-get install nfs-kernel-server
    $ sudo mkdir /mpich
    $ sudo chmod 777 /mpich
    # sudo vim /etc/exports
        /mpich *(rw,sync,no_root_squash,no_subtree_check)
    $ exportfs -a
    $ sudo service nfs-kernel-server restart


Worker/Nodes
    $ sudo apt-get install nfs-common
    $ sudo mkdir /mpich
    $ sudo chmod 777 /mpich
    $ sudo mount -t nfs manager:/mpich /mpich
    $ df -h (check mount dir...)

    mount permanent
    $ sudo vim /etc/fstab
        #MPI CLUSTER SETUP
        manager:/mpich /mpich nfs




# Step 4: Launch with machine file:

Command:

    mpirun -np 2 -f machinefile /mpich/a.out


machinefile:

    mpi@PUPIL-SERVER:1
    mpi@ai1:1