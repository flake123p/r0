# 1

## OpenCL-SDK Github
https://github.com/KhronosGroup/OpenCL-SDK

## Great OpenCL Examples
https://github.com/michel-meneses/great-opencl-examples

## NVIDIA OpenCL SDK Code Samples
https://developer.nvidia.com/opencl

OpenCL-Vulkan Interop Samples
OpenCL Multi Threads
Using Inline PTX with OpenCL
...

## How To Uninstall nvidia-opencl-dev on Ubuntu 22.04
https://installati.one/install-nvidia-opencl-dev-ubuntu-22-04/?expand_article=1

## Cuda Runtime API
https://docs.nvidia.com/cuda/cuda-runtime-api/index.html

