
# https://stackoverflow.com/questions/15852417/compiling-opengl-program-gl-glew-h-missing

    $ sudo apt-get install libglew-dev

# https://askubuntu.com/questions/1344364/i-am-getting-error-about-x11-library

    $ sudo apt-get install libx11-dev libxrandr-dev

# https://askubuntu.com/questions/1410615/g-cant-find-ludev-but-libudev-so-is-there

    $ apt-get install libudev-dev


Then:

    sudo apt-get install opencl-headers

    Intel: sudo apt-get install beignet-dev
    AMD: sudo apt-get install amd-opencl-dev
    Nvidia: sudo apt-get install nvidia-opencl-dev