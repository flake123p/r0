export LD_LIBRARY_PATH=/usr/local/cuda-12.3/targets/x86_64-linux/lib/

g++ -std=c++0x $1 -lOpenCL -I/usr/local/cuda-12.3/targets/x86_64-linux/include/ -L/usr/local/cuda-12.3/targets/x86_64-linux/lib/

./a.out