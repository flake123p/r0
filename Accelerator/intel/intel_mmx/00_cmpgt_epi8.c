#include <immintrin.h>
#include <stdio.h>

int main() {
    const int N = 16;

    __m128i a = _mm_set_epi8(
        -1, 2, -3, 4, -5, 6, -7, 8, -9, 10, -11, 12, -13, 14, -15, 16
    );
    __m128i b = _mm_set1_epi8(0);

    __m128i mask = _mm_cmpgt_epi8(a, b);

    printf("Mask: ");
    for (int i = 0; i < N; i++) {
        printf("%d ", ((char*)&mask)[i]);
    }
    printf("\n");

    // 2nd run
    b = a;

    mask = _mm_cmpgt_epi8(a, b);

    printf("Mask: ");
    for (int i = 0; i < N; i++) {
        printf("%d ", ((char*)&mask)[i]);
    }
    printf("\n");

    return 0;
}