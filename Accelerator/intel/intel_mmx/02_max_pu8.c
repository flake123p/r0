#include <stdio.h>
#include <mmintrin.h>
#include <xmmintrin.h>
#include <cstring>

#define ARRAY_SIZE 8

// void mmx_store(__m64 input, unsigned char* output) {
//     // Store the lower 64 bits of the input to the output array
//     _mm_storel_epi64((__m128i_u*)output, (__m128i)input);
// }

void mmx_max_pu8(unsigned char* input1, unsigned char* input2, unsigned char* output) {
    __m64 mmx_input1, mmx_input2, mmx_output;

    // Load input arrays into MMX registers
    mmx_input1 = _mm_set_pi8(input1[7], input1[6], input1[5], input1[4], input1[3], input1[2], input1[1], input1[0]);
    mmx_input2 = _mm_set_pi8(input2[7], input2[6], input2[5], input2[4], input2[3], input2[2], input2[1], input2[0]);

    // Perform element-wise maximum operation
    mmx_output = _mm_max_pu8(mmx_input1, mmx_input2);

    // Store result back into output array
    //_mm_store_si64((__m64*)output, mmx_output);
    //mmx_store(mmx_output, output);
    //*(__m64*)&output = mmx_output;
    memcpy(output, &mmx_output, ARRAY_SIZE);
}

int main() {
    unsigned char input1[ARRAY_SIZE] = { 5, 3, 8, 10, 2, 6, 1, 4 };
    unsigned char input2[ARRAY_SIZE] = { 7, 4, 6, 2, 12, 1, 9, 11 };
    unsigned char output[ARRAY_SIZE] = {0};

    // Perform maximum operation using MMX
    mmx_max_pu8(input1, input2, output);

    // Print input and output arrays
    printf("Input1: ");
    for (int i = 0; i < ARRAY_SIZE; i++) {
        printf("%3d ", input1[i]);
    }
    printf("\n");

    printf("Input2: ");
    for (int i = 0; i < ARRAY_SIZE; i++) {
        printf("%3d ", input2[i]);
    }
    printf("\n");

    printf("Output: ");
    for (int i = 0; i < ARRAY_SIZE; i++) {
        printf("%3d ", output[i]);
    }
    printf("\n");

    return 0;
}