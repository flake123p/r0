#include <stdio.h>
#include <mmintrin.h> // for MMX intrinsics
#include <emmintrin.h>

#define ROWS 10
#define COLS 10

void max_filter_mmx(int input[ROWS][COLS], int output[ROWS][COLS], int rows, int cols) {
    int i, j, k, l;

    __m64 a, b, c, min_val;

    for (i = 1; i < rows - 1; i++) {
        for (j = 1; j < cols - 1; j++) {
            min_val = _mm_set1_pi8(input[i][j]);

            for (k = -1; k <= 1; k++) {
                for (l = -1; l <= 1; l++) {
                    a = _mm_set1_pi8(input[i + k][j + l]);
                    min_val = _mm_max_pu8(min_val, a);
                }
            }

            output[i][j] = _mm_cvtsi64_si32(min_val);
        }
    }
}

void max_filter(int input[ROWS][COLS], int output[ROWS][COLS], int rows, int cols) {
    int i, j, k, l, max_val;

    for (i = 1; i < rows - 1; i++) {
        for (j = 1; j < cols - 1; j++) {
            max_val = input[i][j];

            for (k = -1; k <= 1; k++) {
                for (l = -1; l <= 1; l++) {
                    if (input[i + k][j + l] > max_val) {
                        max_val = input[i + k][j + l];
                    }
                }
            }

            output[i][j] = max_val;
        }
    }
}

int main() {
    int input[ROWS][COLS] = {{1,1,1,1,1},{1,1,1,2,2},{1,1,1,1,3},};
    int output[ROWS][COLS] = {0};

    const int N = 16;

    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            printf("%3d ", input[i][j]);
        }
        printf("\n");
    }
    printf("\n");

    max_filter(input, output, 10, 10);

    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            printf("%3d ", output[i][j]);
        }
        printf("\n");
    }
    printf("\n");

    return 0;
}