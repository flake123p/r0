#include <immintrin.h>
#include <stdio.h>

int main() {
    __m128i a = _mm_set_epi8(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
    __m128i b = _mm_set_epi8(2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32);

    unsigned int mask = _mm_cmp_epi8_mask(a, b, _MM_CMPINT_EQ);

    printf("Mask: 0x%x\n", mask);

    return 0;
}