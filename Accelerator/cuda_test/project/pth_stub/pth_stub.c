#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include <math.h>
#include <float.h> // FLT, DBL
#include <limits.h>

#include "buda.h"

//#include <ATen/core/TensorBody.h>
#include <c10/core/TensorImpl.h>
#include <c10/core/DynamicCast.h>
#include <c10/util/Exception.h>
#include <c10/util/TypeCast.h>
#include <c10/macros/Macros.h>
#include <ATen/core/Array.h>
#include <ATen/detail/FunctionTraits.h>
#include <ATen/cuda/detail/OffsetCalculator.cuh>

#include <ATen/native/cuda/thread_constants.h>
#include <ATen/native/cuda/MemoryAccess.cuh>
#include <ATen/native/cuda/Loops.cuh>
#include <ATen/native/cuda/CUDALoops.cuh>
#include <thrust/tuple.h>

#include <ATen/native/TensorIterator.h>
#include <c10/core/ScalarType.h>
#include <c10/util/TypeCast.h>
#include <c10/util/C++17.h>

#include "MyTorchCuda.h"

#include "pth_stub.h"

void pth_stub()
{
    PRLOC;
    {
        // int x = 0;
        // at::detail::Array<char *, 2> ary;
        // at::native::AbsFunctor<float> float_abs;

        // at::native::vectorized_elementwise_kernel<4>(x, float_abs, ary);
    }
}