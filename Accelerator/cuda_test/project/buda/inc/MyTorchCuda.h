#pragma once

#undef C10_LAUNCH_BOUNDS_1
#define C10_LAUNCH_BOUNDS_1(...)

#undef C10_LAUNCH_BOUNDS_2
#define C10_LAUNCH_BOUNDS_2(...)

#define MY_TORCH_CUDA_ENABLE ( 1 )

//
// Comment out the pure cuda code
//
#define MY_TORCH_CUDA_PURE_CODE ( 0 )

#include "device_launch_parameters.h"

#define MY_TORCH_CUDA_LAUNCH(...)

#include "buda_sys.h"

//
// Stub
//
static inline unsigned int __activemask(){return 0;}
static inline void __syncwarp(unsigned mask){}
static inline unsigned int __ballot_sync(int predicate, unsigned int mask){return 0;}
static inline void __syncthreads(){}
static inline void __threadfence(){}
static inline int atomicAdd(int a, int b){return 0;}
//static inline int cudaGetLastError(){return 0;}

#define __shfl_down_sync(...) (0)
#define __shfl_xor_sync(...) (0)

#undef TORCH_INTERNAL_ASSERT
#include <cassert>
#define TORCH_INTERNAL_ASSERT(a, ...) assert(a);

#undef TORCH_CHECK
#define TORCH_CHECK(...)