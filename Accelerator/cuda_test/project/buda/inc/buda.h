
#pragma once

#define USING_PYTORCH (0)

#define __ubsan_ignore_float_divide_by_zero__
#define __ubsan_ignore_undefined__
#define __ubsan_ignore_signed_int_overflow__
#define __ubsan_ignore_pointer_overflow__
#define __ubsan_ignore_function__
#define C10_HOST_DEVICE
#define C10_DEVICE 
#define C10_HOST 
#define C10_ALWAYS_INLINE __attribute__((__always_inline__)) inline
#define __forceinline__ \
        __inline__ __attribute__((always_inline))
#define __device__
#define __global__
#define __host__

// From Pytorch directly
#include <ATen/OpMathType.h>
//#include "FunctionTraits.h"
#include <stdlib.h>
#include <ATen/detail/FunctionTraits.h>

//#include "Loops.cu.h"
//#include "vector_types.h"
//#include "device_launch_parameters.h"
//#include "MemoryAccess.cu.h"
//#include "cub/dispatch_scan.cu.h"

// Our version
#include "add.h"
#include "CompareEQKernel.h"

#include "buda_sys.h"

// src 2 hdr
#include "src_2_hdr/AbsKernel.cu.h"
#include "src_2_hdr/CompareKernels.cu.h"