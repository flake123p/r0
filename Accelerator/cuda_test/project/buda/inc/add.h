
#pragma once

#include "buda.h"

namespace at {
namespace native {
namespace ufunc {

template <typename T>
C10_HOST_DEVICE C10_ALWAYS_INLINE T add(T self, T other, T alpha) __ubsan_ignore_undefined__ {
  return self + alpha * other;
}

}}}  // namespace at::native::ufunc



namespace at {
namespace native {

template <typename scalar_t>
struct CUDAFunctor_add {
  using opmath_t = at::opmath_type<scalar_t>;
  opmath_t alpha_;
  CUDAFunctor_add(opmath_t alpha) : alpha_(alpha) {}
  __device__ scalar_t operator()(scalar_t self, scalar_t other) const {
    return ufunc::add(static_cast<opmath_t>(self), static_cast<opmath_t>(other), alpha_);
  }
};
}}  // namespace at::native


