
#include "buda.h"

int run_test_lite(test_lite_t cb, const char *pass_str)
{
    if (pass_str) {
        printf("[START ]: %s\n", pass_str);
    }
    cb();
    if (pass_str) {
        printf("[PASS  ]: %s\n", pass_str);
    }
    return 0;
}

#include "MyTorchCuda.h"
uint3 const threadIdx = {0,0,0};
uint3 const blockIdx = {0,0,0};
dim3  const blockDim = {0,0,0};
dim3  const gridDim = {0,0,0};
int   const warpSize = 0;