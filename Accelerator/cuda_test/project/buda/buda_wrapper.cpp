#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include <math.h>
#include <float.h> // FLT, DBL
#include <limits.h>

#include "buda.h"

//#include <ATen/core/TensorBody.h>
#include <c10/core/TensorImpl.h>
#include <c10/core/DynamicCast.h>
#include <c10/util/Exception.h>
#include <c10/util/TypeCast.h>
#include <c10/macros/Macros.h>
#include <ATen/core/Array.h>
#include <ATen/detail/FunctionTraits.h>
#include <ATen/cuda/detail/OffsetCalculator.cuh>

#include <ATen/native/cuda/thread_constants.h>
#include <ATen/native/cuda/MemoryAccess.cuh>
#include <ATen/native/cuda/Loops.cuh>
#include <ATen/native/cuda/CUDALoops.cuh>
#include <ATen/native/cuda/PersistentSoftmax.cuh>
#include <thrust/tuple.h>

#include <ATen/native/TensorIterator.h>
#include <c10/core/ScalarType.h>
#include <c10/util/TypeCast.h>
#include <c10/util/C++17.h>

#include <c10/core/SymInt.h>
#include <c10/core/impl/SizesAndStrides.h>
#include <c10/util/SmallVector.h>
#include <cstdint>

#include "MyTorchCuda.h"


__attribute__((__visibility__("default"))) int bw_get_version()
{
    return 1 * 10000 + 2 * 100 + 4;
}

/*
struct TORCH_API TensorIteratorBase : public impl::MetaBase {
*/
// void* at::TensorIteratorBase::data_ptr(int arg) const {
//     printf("operands size = %d\n", operands_.size());
//     return operands_[arg].data;
// }
__attribute__((__visibility__("default"))) void *bw_TensorIteratorBase__data_ptr(void *it, int arg) 
{
    printf("Iter!!!\n");
    //at::TensorIterator it; // undefined reference to `vtable for at::TensorIteratorBase'
    at::TensorIteratorBase *iter = (at::TensorIteratorBase *)it;
    //(void)iter;
    //void *p = iter->data_ptr(0);
    return iter->data_ptr(arg);
#if 0
    at::TensorIteratorBase *iter = nullptr;
    (void)iter;

    char *data[10];
    for (int i = 0; i < 0; i++) {
        data[i] = (char*)iter->data_ptr(i);
    }
    (void)data;
#endif
    //void at::native::vectorized_elementwise_kernel<4, at::native::compare_scalar_kernel<double>(at::TensorIteratorBase&, at::native::(anonymous namespace)::OpType, double)::{lambda(double)#1}, at::detail::Array<char*, 2> >(int, at::native::compare_scalar_kernel<double>(at::TensorIteratorBase&, at::native::(anonymous namespace)::OpType, double)::{lambda(double)#1}, at::detail::Array<char*, 2>)
}
