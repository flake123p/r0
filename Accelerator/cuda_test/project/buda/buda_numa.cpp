#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include <math.h>
#include <float.h> // FLT, DBL
#include <limits.h>

#include "buda.h"

//#include <ATen/core/TensorBody.h>
#include <c10/core/TensorImpl.h>
#include <c10/core/DynamicCast.h>
#include <c10/util/Exception.h>
#include <c10/util/TypeCast.h>
#include <c10/macros/Macros.h>
#include <ATen/core/Array.h>
#include <ATen/detail/FunctionTraits.h>
#include <ATen/cuda/detail/OffsetCalculator.cuh>

#include <ATen/native/cuda/thread_constants.h>
#include <ATen/native/cuda/MemoryAccess.cuh>
#include <ATen/native/cuda/Loops.cuh>
#include <ATen/native/cuda/CUDALoops.cuh>
#include <ATen/native/cuda/PersistentSoftmax.cuh>
#include <thrust/tuple.h>

#include <ATen/native/TensorIterator.h>
#include <c10/core/ScalarType.h>
#include <c10/util/TypeCast.h>
#include <c10/util/C++17.h>

#include <c10/core/SymInt.h>
#include <c10/core/impl/SizesAndStrides.h>
#include <c10/util/SmallVector.h>
#include <cstdint>

#include "MyTorchCuda.h"

#ifdef __cplusplus
extern "C" {
#endif

long mbind(void *start, unsigned long len, int mode,
	const unsigned long *nmask, unsigned long maxnode, unsigned flags){return 0;}
long get_mempolicy(int *mode, unsigned long *nmask,
			unsigned long maxnode, void *addr, unsigned flags){return 0;}

/* NUMA support available. If this returns a negative value all other function
   in this library are undefined. */
int numa_available(void){return 0;}

/* Basic NUMA state */

/* Get max available node */
int numa_max_node(void){return 0;}
int numa_max_possible_node(void){return 0;}
/* Return preferred node */
int numa_preferred(void){return 0;}

/* Return node size and free memory */
long long numa_node_size64(int node, long long *freep){return 0;}
long long numa_node_size(int node, long long *freep){return 0;}

int numa_pagesize(void){return 0;}

/* Only run and allocate memory from a specific set of nodes. */
void numa_bind(struct bitmask *nodes){}

/* Set the NUMA node interleaving mask. 0 to turn off interleaving */
void numa_set_interleave_mask(struct bitmask *nodemask){}

void numa_bitmask_free(struct bitmask *){}
struct bitmask *numa_bitmask_setbit(struct bitmask *a, unsigned intb){return a;}

/* allocate a bitmask big enough for all nodes */
struct bitmask *numa_allocate_nodemask(void){return NULL;}

/* nodes in the system */
int numa_num_configured_nodes(void){return 0;}

/* report the node of the specified cpu. -1/errno on invalid cpu. */
int numa_node_of_cpu(int cpu){return 0;}

#ifdef __cplusplus
}
#endif