#pragma once
// pytorch/aten/src/ATen/native/cuda/AbsKernel.cu

namespace at::native {

template<typename scalar_t>
struct AbsFunctor {
  __device__ __forceinline__ scalar_t operator() (const scalar_t a) const {
    return std::abs(a);
  }
};

} //namespace at::native