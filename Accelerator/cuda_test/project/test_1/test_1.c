#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include <math.h>
#include <float.h> // FLT, DBL
#include <limits.h>

#include "buda.h"
#include "test_1.h"

//#include <ATen/core/TensorBody.h>
#include <c10/core/TensorImpl.h>
#include <c10/core/DynamicCast.h>
#include <c10/util/Exception.h>
#include <c10/util/TypeCast.h>
#include <c10/macros/Macros.h>
#include <ATen/core/Array.h>
#include <ATen/detail/FunctionTraits.h>
#include <ATen/cuda/detail/OffsetCalculator.cuh>

#include <ATen/native/cuda/thread_constants.h>
#include <ATen/native/cuda/MemoryAccess.cuh>
#include <ATen/native/cuda/Loops.cuh>
#include <ATen/native/cuda/CUDALoops.cuh>
#include <ATen/native/cuda/PersistentSoftmax.cuh>
#include <thrust/tuple.h>

#include <ATen/native/TensorIterator.h>
#include <c10/core/ScalarType.h>
#include <c10/util/TypeCast.h>
#include <c10/util/C++17.h>

#include <c10/core/SymInt.h>
#include <c10/core/impl/SizesAndStrides.h>
#include <c10/util/SmallVector.h>
#include <cstdint>

#include "MyTorchCuda.h"

void test_1_FunctionTraits_Test()
{
    {
        auto lambdaA = [](int i) { return long(i*10); };

        //printf("A = %ld\n", lambdaA(10));

        typedef function_traits<decltype(lambdaA)> traits;
        static_assert(std::is_same<long, traits::result_type>::value, "err");
        static_assert(std::is_same<int, traits::arg<0>::type>::value, "err");

        traits::result_type a;
        traits::arg<0>::type b;
        //printf("%lu %lu\n", sizeof(a), sizeof(b));

        BUDA_ASSERT(sizeof(a) == sizeof(long));
        BUDA_ASSERT(sizeof(b) == sizeof(int));
        //return 0;
    }
    {
        auto lambdaA = []() { return 10; };
        int arity = function_traits<decltype(lambdaA)>::arity;
        //printf("arity = %d\n", arity);
        BUDA_ASSERT(arity == 0);
    }
    {
        auto lambdaA = [](int i) { return long(i*10); };
        int arity = function_traits<decltype(lambdaA)>::arity;
        //printf("arity = %d\n", arity);
        BUDA_ASSERT(arity == 1);
    }
    {
        auto lambdaA = [](int i, int j) { return i*j; };
        int arity = function_traits<decltype(lambdaA)>::arity;
        //printf("arity = %d\n", arity);
        BUDA_ASSERT(arity == 2); 
    }
    {
        auto lambdaA = [](int i, int j, int k) { return i*j*k; };
        int arity = function_traits<decltype(lambdaA)>::arity;
        //printf("arity = %d\n", arity);
        BUDA_ASSERT(arity == 3);
    }
}

void test_1_DeviceCompactInitKernel_Test()
{
    // DeviceCompactInitKernel
}

void test_1_detail_Array_Test()
{
    {
        at::detail::Array<char*, 2> a;
        (void)a;
    }
    {
        at::detail::Array<char*, 2> a(NULL);
        BUDA_ASSERT(a.data[1] == NULL);
    }
}

#define MY_VAR_0 5
#define MY_VAR_1 6
#define MY_LAM_FACTOR 10
template<int vec_size, typename func_t, typename array_t>
void my_vectorized_elementwise_kernel(int N, func_t f, array_t data) {
    for (int i = 0; i < N; i++) {
        //printf("my_vectorized_elementwise_kernel = %d\n", f(data.data[i]));
    }
    BUDA_ASSERT(f(data.data[0]) == MY_VAR_0 * MY_LAM_FACTOR);
    BUDA_ASSERT(f(data.data[1]) == MY_VAR_1 * MY_LAM_FACTOR);
}

template<typename scalar_t>
struct MyFunctor {
    __device__ __forceinline__ scalar_t operator() (const scalar_t a) const {
    return std::abs(a);
  }
};

void test_1_vectorized_elementwise_kernel_Test()
{
    auto lambdaA = [](int i) { return int(i*MY_LAM_FACTOR); };

    at::detail::Array<int, 2> ary(3);
    BUDA_ASSERT(ary.data[0] == 3);
    BUDA_ASSERT(ary.data[1] == 3);
    ary.data[0] = MY_VAR_0;
    ary.data[1] = MY_VAR_1;

    my_vectorized_elementwise_kernel<100>(2, lambdaA, ary);

    //at::native::vectorized_elementwise_kernel<1>(2, lambdaA, ary);

    {
        at::detail::Array<char *, 2> ary;
        MyFunctor<float> float_abs;

        float dst[5] = {0, 0, 0, 0, 0};
        float src[5] = {-3, -2, -11, -7, -8};
        ary.data[0] = (char *)dst;
        ary.data[1] = (char *)src;

        at::native::vectorized_elementwise_kernel<4>(5, float_abs, ary);
        
        ARRAYDUMPF(dst, 5);
    }

    {
        int x = 0;
        at::detail::Array<char *, 2> ary;
        at::native::AbsFunctor<float> float_abs;

        at::native::vectorized_elementwise_kernel<4>(x, float_abs, ary);
    }
    
    // printf("num_threads() = %d\n", num_threads());
    // printf("block_work_size() = %d\n", block_work_size());
    // printf("thread_work_size() = %d\n", thread_work_size());
    // printf("blockIdx.x = %d\n", blockIdx.x);
}

template<typename func_t, typename array_t>
static inline void my_launch_vectorized_kernel(int64_t N, const func_t& f, array_t data) 
{
  //TORCH_INTERNAL_ASSERT(N > 0 && N <= std::numeric_limits<int32_t>::max());
  using traits = function_traits<func_t>;
  int64_t grid = (N + block_work_size() - 1) / block_work_size();
  (void)grid;
  //auto stream = at::cuda::getCurrentCUDAStream();
  int vec_size = at::native::memory::can_vectorize_up_to<func_t>(data);
  printf("vec_size = %d\n", vec_size);
}

void test_1_launch_vectorized_kernel_Test()
{
    {
        at::detail::Array<char *, 2> ary;
        MyFunctor<float> float_abs;

        float dst[5] = {0, 0, 0, 0, 0};
        float src[5] = {-3, -2, -11, -7, -8};
        ary.data[0] = (char *)dst;
        ary.data[1] = (char *)src;

        my_launch_vectorized_kernel(5, float_abs, ary);

        int vec_size = at::native::memory::can_vectorize_up_to<MyFunctor<float>>(ary);
        printf("vec_size = %d\n", vec_size);

        //ARRAYDUMPF(dst, 5);

        //at::TensorBase a;
    }
}

float AUnaryFunctor_TestFunc(float a, float b) {
    return (a + b);
}

void test_1_AUnaryFunctor_Test()
{
    at::native::AUnaryFunctor<float, float, float, float (*)(float, float)> af(AUnaryFunctor_TestFunc, 10);

    //printf("AUnaryFunctor_Test ... %f\n", af(7));

    float output = af(7);
    const float gold_out = 17;

    BUDA_ASSERT(output == gold_out);
}

void test_1_CUDAFunctor_add_Test() 
{
    auto ans = at::native::ufunc::add<float>(1,2,3);

    printf("ans = %f\n", ans);

    at::native::CUDAFunctor_add<float> cuda_add(1);
    auto ans2 = cuda_add(1,2);

    printf("ans2 = %f\n", ans2);

    BUDA_ASSERT(ans == (float)7);
    BUDA_ASSERT(ans2 == (float)3);
}

void test_1_cmake_macros_h_Test()
{
    // Test this ported define is included
#ifdef C10_BUILD_SHARED_LIBS
    // pass
#else
    BUDA_ASSERT(0);
#endif
}

void test_1_Array_h_Test()
{
    at::detail::Array<int, 10> a(999);
    BUDA_ASSERT(a.data[0] == 999 );
    BUDA_ASSERT(a.data[9] == 999);
}

/*
struct TORCH_API TensorIteratorBase : public impl::MetaBase {
*/
void test_1_TensorIteratorBase_Test() 
{
    //at::TensorIterator it; // undefined reference to `vtable for at::TensorIteratorBase'
    at::TensorIteratorBase *iter = nullptr;
    (void)iter;
    //void *p = iter->data_ptr(0);

    extern void *bw_TensorIteratorBase__data_ptr(void *it, int arg);
    //void *p = bw_TensorIteratorBase__data_ptr(NULL, 0);
#if 0
    at::TensorIteratorBase *iter = nullptr;
    (void)iter;

    char *data[10];
    for (int i = 0; i < 0; i++) {
        data[i] = (char*)iter->data_ptr(i);
    }
    (void)data;
#endif
    //void at::native::vectorized_elementwise_kernel<4, at::native::compare_scalar_kernel<double>(at::TensorIteratorBase&, at::native::(anonymous namespace)::OpType, double)::{lambda(double)#1}, at::detail::Array<char*, 2> >(int, at::native::compare_scalar_kernel<double>(at::TensorIteratorBase&, at::native::(anonymous namespace)::OpType, double)::{lambda(double)#1}, at::detail::Array<char*, 2>)
}

void test_1_softmax_warp_forward_Test()
{
    float a[2] = {1.0f, 1.0f};
    float b[2] = {9999.0f, 7777.0f};
    softmax_warp_forward<float, float, float, 2, false, false>(b, a, 0, 0, 2);
    printf("SMax: %f, %f\n", b[0], b[1]);
}

void test_1_SmallVector_Test()
{
    c10::SmallVector<int64_t, c10::kDimVectorStaticSize> sv;
    printf("kDimVectorStaticSize = %ld\n", c10::kDimVectorStaticSize);
}

void test_1()
{
    //PRLOC
    RUN_TEST_LITE(test_1_FunctionTraits_Test);
    RUN_TEST_LITE(test_1_DeviceCompactInitKernel_Test);
    RUN_TEST_LITE(test_1_detail_Array_Test);
    RUN_TEST_LITE(test_1_vectorized_elementwise_kernel_Test);
    RUN_TEST_LITE(test_1_launch_vectorized_kernel_Test);
    RUN_TEST_LITE(test_1_AUnaryFunctor_Test);
    RUN_TEST_LITE(test_1_CUDAFunctor_add_Test);
    RUN_TEST_LITE(test_1_cmake_macros_h_Test);
    RUN_TEST_LITE(test_1_Array_h_Test);
    RUN_TEST_LITE(test_1_TensorIteratorBase_Test);
    RUN_TEST_LITE(test_1_softmax_warp_forward_Test);
    RUN_TEST_LITE(test_1_SmallVector_Test);
}


/*

void at::native::vectorized_elementwise_kernel
    <
        4, 
        at::native::compare_scalar_kernel
            <
                double
            >
            (
                at::TensorIteratorBase&, 
                at::native::(anonymous namespace)::OpType, double
            )::{lambda(double)#1},
        at::detail::Array<char*, 2> 
    >
    (
        int, 
        at::native::compare_scalar_kernel
            <
                double
            >
            (
                at::TensorIteratorBase&, 
                at::native::(anonymous namespace)::OpType, double
            )::{lambda(double)#1}, 
        at::detail::Array<char*, 2>)



    void at::native::vectorized_elementwise_kernel
        <
            int=4, 
            at::native::AbsFunctor<float>, 
            at::detail::Array<char*, int=2>
        >
        (
            int, 
            float, 
            at::native::AbsFunctor<float>
        )
*/