#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include <math.h>
#include <float.h> // FLT, DBL
#include <limits.h>

#include "buda.h"
#include "test_0.h"

void test_0_demo()
{
    //PRLOC
}

void test_0_CompareEqFunctor_Test() 
{
    // //using namespace at::native;

    // struct at::native::CompareEqFunctor<int> eqop(at::native::EqOpType::EQ);

    // //printf("eqop 4,4  = %d\n",eqop(4,4));
    // BUDA_ASSERT(eqop(4,4) == 1);

    // struct at::native::CompareEqFunctor<int> neqop(at::native::EqOpType::NE);

    // //printf("neqop 4,4  = %d\n",neqop(4,4));
    // BUDA_ASSERT(neqop(4,4) == 0);
   
    // //return 0;
}

int add(int a,int b){
    return a+b;
}
// void test_0_BinaryFunctor_Test() 
// {
//     struct at::native::BinaryFunctor<int,int,int, decltype(&add)> binf(&add);

//     //printf("binf 3+4  = %d\n", binf(3,4));
//     BUDA_ASSERT(binf(3,4) == 7);
   
//     //return 0;
// }

void test_0()
{
    RUN_TEST_LITE(test_0_demo);
    //RUN_TEST_LITE(test_0_CompareEqFunctor_Test);
    //RUN_TEST_LITE(test_0_BinaryFunctor_Test);

    RUN_TEST_LITE(test_0_vec);
}
