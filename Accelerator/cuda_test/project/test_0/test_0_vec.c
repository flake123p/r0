#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include <math.h>
#include <float.h> // FLT, DBL
#include <limits.h>

#include "buda.h"
#include "test_0.h"
#include <tuple>
#include <cmath>
#include <iostream>

#include <c10/core/DynamicCast.h>
#include <c10/util/Exception.h>
#include <c10/util/TypeCast.h>
#include <c10/macros/Macros.h>
#include <ATen/core/Array.h>
#include <ATen/detail/FunctionTraits.h>
#include <ATen/cuda/detail/OffsetCalculator.cuh>

#include <ATen/native/cuda/thread_constants.h>
#include <ATen/native/cuda/MemoryAccess.cuh>
#include <ATen/native/cuda/Loops.cuh>
#include <ATen/native/cuda/CUDALoops.cuh>
#include <thrust/tuple.h>

#include "MyTorchCuda.h"

void test_0_FunctionTraits_Test()
{
    {
        auto lambdaA = [](int i) { return long(i*10); };

        //printf("A = %ld\n", lambdaA(10));

        typedef function_traits<decltype(lambdaA)> traits;
        static_assert(std::is_same<long, traits::result_type>::value, "err");
        static_assert(std::is_same<int, traits::arg<0>::type>::value, "err");

        traits::result_type a;
        traits::arg<0>::type b;
        //printf("%lu %lu\n", sizeof(a), sizeof(b));

        BUDA_ASSERT(sizeof(a) == sizeof(long));
        BUDA_ASSERT(sizeof(b) == sizeof(int));
        //return 0;
    }
    {
        auto lambdaA = []() { return 10; };
        int arity = function_traits<decltype(lambdaA)>::arity;
        //printf("arity = %d\n", arity);
        BUDA_ASSERT(arity == 0);
    }
    {
        auto lambdaA = [](int i) { return long(i*10); };
        int arity = function_traits<decltype(lambdaA)>::arity;
        //printf("arity = %d\n", arity);
        BUDA_ASSERT(arity == 1);
    }
    {
        auto lambdaA = [](int i, int j) { return i*j; };
        int arity = function_traits<decltype(lambdaA)>::arity;
        //printf("arity = %d\n", arity);
        BUDA_ASSERT(arity == 2); 
    }
    {
        auto lambdaA = [](int i, int j, int k) { return i*j*k; };
        int arity = function_traits<decltype(lambdaA)>::arity;
        //printf("arity = %d\n", arity);
        BUDA_ASSERT(arity == 3);
    }
}



void test_0_detail_Array_Test()
{
    {
        at::detail::Array<char*, 2> a;
        (void)a;
    }
    {
        at::detail::Array<char*, 2> a(NULL);
        BUDA_ASSERT(a.data[1] == NULL);
    }
}

#define MY_VAR_0 5
#define MY_VAR_1 6
#define MY_LAM_FACTOR 10


// template<typename scalar_t>
// struct AbsFunctor {
//     __device__ __forceinline__ scalar_t operator() (const scalar_t a) const {
//     return std::abs(a);
//   }
// };

void test_0_vectorized_elementwise_kernel_Test()
{
    //auto lambdaA = [](int i) { return int(i*MY_LAM_FACTOR); };
   
    at::detail::Array<int, 2> ary(3);
    BUDA_ASSERT(ary.data[0] == 3);
    BUDA_ASSERT(ary.data[1] == 3);
    ary.data[0] = MY_VAR_0;
    ary.data[1] = MY_VAR_1;

    //my_vectorized_elementwise_kernel<100>(2, lambdaA, ary);

    //at::native::vectorized_elementwise_kernel<1>(2, lambdaA, ary);

    {
        using namespace at::native;
        at::detail::Array<char *, 2> ary;
        AbsFunctor<float> float_abs;

        float dst[5] = {0, 0, 0, 0, 0};
        float src[5] = {-3, -2, -11, -7, -8};
        ary.data[0] = (char *)dst;
        ary.data[1] = (char *)src;

        at::native::vectorized_elementwise_kernel<4>(5, float_abs, ary);
        
        ARRAYDUMPF(dst, 5);
    }
    
    // printf("num_threads() = %d\n", num_threads());
    // printf("block_work_size() = %d\n", block_work_size());
    // printf("thread_work_size() = %d\n", thread_work_size());
    // printf("blockIdx.x = %d\n", blockIdx.x);
}
// void test_0_launch_vectorized_kernel_Test()
// {
//     //auto lambdaA = [](int i) { return int(i*MY_LAM_FACTOR); };


//     {
//         at::detail::Array<char *, 2> ary;
//         AbsFunctor<float> float_abs;

//         float dst[5] = {0, 0, 0, 0, 0};
//         float src[5] = {-3, -2, -11, -7, -8};
//         ary.data[0] = (char *)dst;
//         ary.data[1] = (char *)src;

//         at::native::launch_vectorized_kernel<>(5, float_abs, ary);
        
//     }
    
//     // num_thread=128;
//     // block_work_size=512;
//     // thread_work_size=4;
//     // blockIdx=0;
// }


// void TensorIteratorBase::build_binary_op(const TensorBase& out, const TensorBase& a, const TensorBase& b) {
//   build(BINARY_OP_CONFIG()
//       .add_owned_output(out)
//       .add_owned_input(a)
//       .add_owned_input(b));
// }

// TensorIterator TensorIterator::binary_op(TensorBase& out, const TensorBase& a, const TensorBase& b) {
//   TensorIterator iter;
//   iter.build_binary_op(out, a, b);
//   return iter;
// }
// void test_0_TensorBase()
// {
//     {
//         at::IntArrayRef size={2,2};
//         at::Tensor tensor = at::ones(size); //check at::Tensor
       
//         //auto c=a.size(1);
//         //printf("**********%ld",c);PRLOC
//         //ta=ta.operator=(2);
        
        
//     }
// }

void test_0_reduce_kernel()
{
    {
        //struct MaxNanFunctor<int> MaxF;


        
    }
}

// void test_0_TensorIteratorBase()
// {
//     {
//       at::TensorBase a; //check at::Tensor
//       at::TensorBase& ra=a;
//       at::TensorBase b;
//       at::TensorBase& rb=b;
//       at::TensorBase out;
//       at::TensorBase& rout=out;
//       at::TensorIterator inititer=at::TensorIterator::binary_op(out, a, b);
//       //at::TensorIteratorBase iterbase; //cannot declare variable ‘iterbase’ to be of abstract type ‘at::TensorIteratorBase’
//       //iterbase.build_binary_op(rout,ra,rb);
        
//     }
// }
void test_0_vec()
{
    //PRLOC
    // RUN_TEST_LITE(test_0_FunctionTraits_Test);
    // RUN_TEST_LITE(test_0_detail_Array_Test);
    // RUN_TEST_LITE(test_0_vectorized_elementwise_kernel_Test);
    RUN_TEST_LITE(test_0_reduce_kernel);

}


/*

void at::native::vectorized_elementwise_kernel
    <
        4, 
        at::native::compare_scalar_kernel
            <
                double
            >
            (
                at::TensorIteratorBase&, 
                at::native::(anonymous namespace)::OpType, double
            )::{lambda(double)#1},
        at::detail::Array<char*, 2> 
    >
    (
        int, 
        at::native::compare_scalar_kernel
            <
                double
            >
            (
                at::TensorIteratorBase&, 
                at::native::(anonymous namespace)::OpType, double
            )::{lambda(double)#1}, 
        at::detail::Array<char*, 2>)



    void at::native::vectorized_elementwise_kernel
        <
            int=4, 
            at::native::AbsFunctor<float>, 
            at::detail::Array<char*, int=2>
        >
        (
            int, 
            float, 
            at::native::AbsFunctor<float>
        )
*/


/*
void at::native::reduce_kernel<
          int=512, 
          int=1, 
          at::native::ReduceOp<
            double, 
            at::native::func_wrapper_t<
                 double, 
                at::native::MinNanFunctor<double>
            >, 
            unsigned int, 
            double, 
            int=4
          >
    >(double)

*/