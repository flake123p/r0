#pragma once

#include <stdio.h>
#include <cmath>

void test_0();
void test_0_vec();

template <typename acc_t>
struct MaxNanFunctor {
    acc_t operator()(acc_t a, acc_t b) const {
    return (std::isnan(a) || a > b) ? a : b;
  }
};