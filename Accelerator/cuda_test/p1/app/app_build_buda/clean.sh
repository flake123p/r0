#!/bin/bash

rm build*/ -rf
rm ../../lib/buda/lib/libbu* -rf
rm ../../lib/buda/lib/libcu* -rf
rm ../../lib/cmd/lib/lib* -rf
rm ../../lib/swc/lib/lib* -rf
# rm inc/ -rf
# rm lib/ -rf