function(cmd_device_sim_cmake curr_target)
#message("curr_target = " ${curr_target})
target_include_directories(
    ${curr_target} PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR}/../../cmd/cmd_device_sim"
)

if (NOT TARGET cmd_device_sim)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../../cmd/cmd_device_sim ${CMAKE_BINARY_DIR}/cmd/cmd_device_sim)
endif()

target_link_libraries(${curr_target} PUBLIC cmd_device_sim)

endfunction()
