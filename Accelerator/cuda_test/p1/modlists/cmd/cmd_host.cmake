function(cmd_host_cmake curr_target)
#message("curr_target = " ${curr_target})
target_include_directories(
    ${curr_target} PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR}/../../cmd/cmd_interface"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../cmd/cmd_host"
)

if (NOT TARGET cmd_host)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../../cmd/cmd_host ${CMAKE_BINARY_DIR}/cmd/cmd_host)
endif()

target_link_libraries(${curr_target} PUBLIC cmd_host)

endfunction()
