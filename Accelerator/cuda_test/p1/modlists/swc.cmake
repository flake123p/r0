function(swc_cmake curr_target)
#message("curr_target = " ${curr_target})
target_include_directories(
    ${curr_target} PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR}/../../swc"
)

if (NOT TARGET swc)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../../swc ${CMAKE_BINARY_DIR}/swc)
endif()

target_link_libraries(${curr_target} PUBLIC swc)

endfunction()
