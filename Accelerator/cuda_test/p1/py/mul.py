''' nn.LayerNorm vs. RMSNorm '''
import torch
import torch.nn as nn

# src1 = torch.ones(2, 10)
# src1[0][0] = 10
# src2 = torch.tensor([[2],[3]])
src1 = torch.ones(2, 10)
src1[0][0] = 10
src2 = torch.tensor([3])
src1 = src1.cuda()
src2 = src2.cuda()
dst = src1 * src2
dst = dst.cpu()
print(dst)



