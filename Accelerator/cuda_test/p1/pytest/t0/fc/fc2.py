import torch
import torch.nn as nn
import torch.nn.functional as F
import sys

if len(sys.argv) < 2:
    print('[FAILED] argv should be 2 or 3 ...')
    exit()

to_gpu = False
if sys.argv[1] == 'GPU':
    to_gpu = True

auto_compare = False
if len(sys.argv) >= 3:
    if sys.argv[2] == 'AUTO_CMP':
        auto_compare = True

#
# Test Case Start
#
inDim = (3, 64, 1024)
outNum = 512
# print(inDim[-1])

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.fc = nn.Linear(inDim[-1],outNum)
        self.fc.weight.data = torch.ones(outNum,inDim[-1])
        # self.fc.weight.data = torch.tensor([[1.0, 1.0, 1.0],[ 2.0,2.0,2.0]])
        product = 1
        for i in range(len(inDim)-1):
            product * inDim[i]
        # print('product =', product)
        self.fc.bias.data = torch.zeros(product,outNum)
    def forward(self, x):
        return self.fc(x)
 
myNet = Net()
input = torch.ones(inDim)
# print('input =', input)

if to_gpu:
    myNet = myNet.cuda()
    input = input.cuda()
else:
    myNet = myNet.cpu()
    input = input.cpu()

# print(myNet.fc.weight.data)
# print(myNet.fc.bias.data)

ans = myNet(input).cpu()

print('LOG_START')
print(str(sys.argv[1]))
print('[', inDim, ']')
print(ans)
print('to_gpu =', to_gpu)
print('LOG_END')

if auto_compare:
    print('AUTO_CMP_START')
    ans = torch.flatten(ans)
    for a in ans:
        print("{:.40f}".format(a.item()))
    print('AUTO_CMP_END')