import torch
import torch.nn as nn
import torch.nn.functional as F
import sys
pure_cpu = False
if len(sys.argv) == 2:
    if sys.argv[1] == '[CPU]':
        pure_cpu = True
else:
    print('[FAILED] argv is not 2 ...')
    exit()

inDim = (2, 3)
outNum = 1
#print(inDim[-1])

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.fc = nn.Linear(inDim[-1],outNum)
        self.fc.weight.data = torch.ones(outNum,inDim[-1])
        # # self.fc.weight.data = torch.tensor([[1.0, 1.0, 1.0],[ 2.0,2.0,2.0]])
        product = 1
        for i in range(len(inDim)-1):
            product * inDim[i]
        # print('product =', product)
        self.fc.bias.data = torch.zeros(product,outNum)
    def forward(self, x):
        return self.fc(x)
 
myNet = Net()
input = torch.ones(inDim)
#print('input =', input)

if pure_cpu:
    myNet = myNet.cpu()
    input = input.cpu()
else:
    myNet = myNet.cuda()
    input = input.cuda()

# print(myNet.fc.weight.data)
# print(myNet.fc.bias.data)

ans = myNet(input).cpu()

print('LOG_START')
print(str(sys.argv[1]))
print('in  shape =', input.shape)
print('out shape =', ans.shape)
print('wei shape =', myNet.fc.weight.shape)
print('bai shape =', myNet.fc.bias.shape)
print('[', inDim, ']')
print(ans)
print('LOG_END')
print('RESULT_START')
ans = torch.flatten(ans)
for a in ans:
    print("{:.40f}".format(a.item()))
print('RESULT_END')