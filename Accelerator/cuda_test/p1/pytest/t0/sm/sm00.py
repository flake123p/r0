import torch
import torch.nn as nn
import math

torch.set_printoptions(precision=9)
inDim = (1,2,4)

input_tensor = torch.ones(inDim)
# input_tensor = torch.randn(inDim)
# print(input_tensor)

input_tensor_cpu = torch.ones(inDim)
# print(input_tensor_cpu)

input_tensor_cpu = input_tensor_cpu.to('cpu')
input_tensor = input_tensor.to('cuda')

output_cpu = torch.nn.functional.softmax(input_tensor_cpu, dim=-1)
output = torch.nn.functional.softmax(input_tensor, dim=-1)

print(output_cpu)
print(output)
Nui_List = torch.flatten(output)
Cpu_List = torch.flatten(output_cpu)

ctr = 0

if len(Nui_List) == len(Cpu_List):
    for i in range(len(Nui_List)):
        # Cpu_List[0]=3.000045
        same = Nui_List[i] == Cpu_List[i]
        # print(Nui_List[i],Cpu_List[i])
        OneToPow3 = math.fabs(Nui_List[i] - Cpu_List[i]) < 1e-3
        OneToPow4 = math.fabs(Nui_List[i] - Cpu_List[i]) < 1e-4
        OneToPow5 = math.fabs(Nui_List[i] - Cpu_List[i]) < 1e-5
        OneToPow6 = math.fabs(Nui_List[i] - Cpu_List[i]) < 1e-6
        OneToPow7 = math.fabs(Nui_List[i] - Cpu_List[i]) < 1e-7
        OneToPow8 = math.fabs(Nui_List[i] - Cpu_List[i]) < 1e-8
        OneToPow9 = math.fabs(Nui_List[i] - Cpu_List[i]) < 1e-9
        OneToPow10 = math.fabs(Nui_List[i] - Cpu_List[i]) < 1e-10
        OneToPow11 = math.fabs(Nui_List[i] - Cpu_List[i]) < 1e-11
        OneToPow12 = math.fabs(Nui_List[i] - Cpu_List[i]) < 1e-12
        precision_cmp = ""
        if not same:
            precision_cmp = \
                ", <1e-3=" + str(int(OneToPow3)) + \
                ", <1e-4=" + str(int(OneToPow4)) + \
                ", <1e-5=" + str(int(OneToPow5)) + \
                ", <1e-6=" + str(int(OneToPow6)) + \
                ", <1e-7=" + str(int(OneToPow7)) + \
                ", <1e-8=" + str(int(OneToPow8)) + \
                ", <1e-9=" + str(int(OneToPow9)) + \
                ", <1e-10=" + str(int(OneToPow10)) + \
                ", <1e-11=" + str(int(OneToPow11)) + \
                ", <1e-12=" + str(int(OneToPow12))
        print_en = False
        if ctr < 20:
            print_en = True
        else:
            if not same:
                print_en = True
        if print_en:
            print(
                "[GPU/CPU] {:.12f} /".format(Nui_List[i]), 
                "{:.12f}".format(Cpu_List[i]), 
                "SAME =", int(same), 
                precision_cmp)
            ctr += 1