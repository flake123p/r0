import torch
import torch.nn as nn
import math

torch.set_printoptions(precision=9)
inDim = (1)

input_tensor = torch.arange(0.0,1.0).view(inDim)
# input_tensor = torch.randn(inDim)
print(input_tensor)

input_tensor_cpu = torch.arange(0.0,1.0).view(inDim)
print(input_tensor_cpu)

input_tensor_cpu = input_tensor_cpu.to('cpu')
input_tensor = input_tensor.to('cuda')

output_cpu = torch.nn.functional.softmax(input_tensor_cpu, dim=1)
output = torch.nn.functional.softmax(input_tensor, dim=1)

print(output_cpu)
print(output)
Nui_List = torch.flatten(output)
Cpu_List = torch.flatten(output_cpu)

x = [0 for k in range(100)]
ctr = 0
j=0
if len(Nui_List) == len(Cpu_List):
    for i in range(len(Nui_List)):
        OneToPow3_Record = int(0)
        OneToPow4_Record = int(0)
        OneToPow5_Record = int(0)
        OneToPow6_Record = int(0)
        OneToPow7_Record = int(0)
        OneToPow8_Record = int(0)
        OneToPow9_Record = int(0)
        # Cpu_List[0]=3.000045
        same = Nui_List[i] == Cpu_List[i]
        # print(Nui_List[i],Cpu_List[i])
        OneToPow3 = math.fabs(Nui_List[i] - Cpu_List[i]) < 1e-3
        OneToPow4 = math.fabs(Nui_List[i] - Cpu_List[i]) < 1e-4
        OneToPow5 = math.fabs(Nui_List[i] - Cpu_List[i]) < 1e-5
        OneToPow6 = math.fabs(Nui_List[i] - Cpu_List[i]) < 1e-6
        OneToPow7 = math.fabs(Nui_List[i] - Cpu_List[i]) < 1e-7
        OneToPow8 = math.fabs(Nui_List[i] - Cpu_List[i]) < 1e-8
        OneToPow9 = math.fabs(Nui_List[i] - Cpu_List[i]) < 1e-9
        OneToPow3_Record = int(OneToPow3) | OneToPow3_Record
        OneToPow4_Record = int(OneToPow4) | OneToPow4_Record
        OneToPow5_Record = int(OneToPow5) | OneToPow5_Record
        OneToPow6_Record = int(OneToPow6) | OneToPow6_Record
        OneToPow7_Record = int(OneToPow7) | OneToPow7_Record
        OneToPow8_Record = int(OneToPow8) | OneToPow8_Record
        OneToPow9_Record = int(OneToPow9) | OneToPow9_Record
        if OneToPow3_Record == 0:
            x[j] = math.fabs(Nui_List[i] - Cpu_List[i])
            j=j+1
        elif OneToPow4_Record == 0:
            x[j] = math.fabs(Nui_List[i] - Cpu_List[i])
            j=j+1
        elif OneToPow5_Record == 0:
            x[j] = math.fabs(Nui_List[i] - Cpu_List[i])
            j=j+1
        elif OneToPow6_Record == 0:
            x[j] = math.fabs(Nui_List[i] - Cpu_List[i])
            j=j+1
        elif OneToPow7_Record == 0:
            x[j] = math.fabs(Nui_List[i] - Cpu_List[i])
            j=j+1
        elif OneToPow8_Record == 0:
            x[j] = math.fabs(Nui_List[i] - Cpu_List[i])
            j=j+1
        elif OneToPow9_Record == 0:
            x[j] = math.fabs(Nui_List[i] - Cpu_List[i])
            j=j+1
        precision_cmp = ""
        if not same:
            precision_cmp = \
                ", <1e-3=" + str(int(OneToPow3)) + \
                ", <1e-4=" + str(int(OneToPow4)) + \
                ", <1e-5=" + str(int(OneToPow5)) + \
                ", <1e-6=" + str(int(OneToPow6)) + \
                ", <1e-7=" + str(int(OneToPow7)) + \
                ", <1e-8=" + str(int(OneToPow8)) + \
                ", <1e-9=" + str(int(OneToPow9))
        print_en = False
        if ctr < 20:
            print_en = True
        else:
            if not same:
                print_en = True
        
        if print_en:
            print(
                "[NUI/CPU] {:.12f} /".format(Nui_List[i]), 
                "{:.12f}".format(Cpu_List[i]), 
                "SAME =", int(same), 
                precision_cmp)
            ctr += 1
    worst = max(x)
    print("worst=",worst)