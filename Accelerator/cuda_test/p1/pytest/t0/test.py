#
# https://stackoverflow.com/questions/52874575/subprocess-echo-environment-var-outputs-environment-var
#
import os
import subprocess
from subprocess import Popen
from os import environ
import math
import numpy
import sys

#
# Usage: python test.py [TESTCASE.py] [GOLDEN_PRECISION]
#
# examples: 
#   python test.py fc/fc2.py SAME
#   python test.py fc/fc7.py 1e-6
#
if len(sys.argv) == 3:
    pass
else:
    print('[FAILED] argv is not 3 ...')
    exit()

test_python_file = sys.argv[1]
test_golden = sys.argv[2]
args_base = ['python', test_python_file]
#print('args_base =', args_base)

MY_HW_RUN = False
MY_SW_RUN = True
GPU_RUN = False
CPU_RUN = True

MyHW_List = []
MySW_List = []
Gpu_List = []
Cpu_List = []

# args_base = ['python', 'fc4.py']
# NUI_SW_RUN = False
# GPU_RUN =True
# CPU_RUN =True
# NUI_SW_RUN = []
# Gpu_List = []
# Cpu_List = []

def dump_log_f32(log):
    dump_flag = False
    float_capture_flag = False
    out_list = []
    for i in log:
        if i == 'LOG_START':
            dump_flag = True
            continue
        elif i == 'LOG_END':
            dump_flag = False
        if dump_flag == True:
            print(i)
        if i == 'AUTO_CMP_START':
            float_capture_flag = True
            continue
        elif i == 'AUTO_CMP_END':
            float_capture_flag = False
            continue
        if float_capture_flag == True:
            #print(float(i))
            out_list.append(float(i))
    return out_list


if MY_SW_RUN:
    print('MY_SW_RUN')
    env = dict(os.environ)
    env['LD_PRELOAD'] = '../../lib/buda/lib/libcuda.so.1:../../lib/buda/lib/libcudart.so.11.0:../../lib/buda/lib/libcublas.so.11:../../lib/buda/lib/libcublasLt.so.11:../../lib/buda/lib/libbudartpp.so'
    env['LD_LIBRARY_PATH'] = '../../lib/buda/lib:../../lib/cmd/lib'
    args = args_base.copy()
    args.append('GPU')
    args.append('AUTO_CMP')
    #myproc = Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=env, shell=True).stdout

    #p = subprocess.run(args, stderr=subprocess.PIPE, env=env).stderr.decode('utf-8')
    p = subprocess.run(args, stdout=subprocess.PIPE, env=env).stdout.decode('utf-8').split('\n')

    #print(p)
    MySW_List = dump_log_f32(p)

#
# GPU Run
#
if GPU_RUN:
    print('GPU_RUN')
    env = dict(os.environ)
    env['LD_PRELOAD'] = ''
    env['LD_LIBRARY_PATH'] = ''
    args = args_base.copy()
    args.append('GPU')
    args.append('AUTO_CMP')
    #myproc = Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=env, shell=True).stdout

    #p = subprocess.run(args, stderr=subprocess.PIPE, env=env).stderr.decode('utf-8')
    p = subprocess.run(args, stdout=subprocess.PIPE, env=env).stdout.decode('utf-8').split('\n')

    #print(p)
    Gpu_List = dump_log_f32(p)

#
# CPU Run
#
if CPU_RUN:
    print('CPU_RUN')
    env = dict(os.environ)
    env['LD_PRELOAD'] = ''
    env['LD_LIBRARY_PATH'] = ''
    args = args_base.copy()
    args.append('CPU')
    args.append('AUTO_CMP')
    #myproc = Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=env, shell=True).stdout

    #p = subprocess.run(args, stderr=subprocess.PIPE, env=env).stderr.decode('utf-8')
    p = subprocess.run(args, stdout=subprocess.PIPE, env=env).stdout.decode('utf-8').split('\n')

    #print(p)
    Cpu_List = dump_log_f32(p)

Same_Record = int(1)
OneToPow1_Record  = int(1)
OneToPow2_Record  = int(1)
OneToPow3_Record  = int(1)
OneToPow4_Record  = int(1)
OneToPow5_Record  = int(1)
OneToPow6_Record  = int(1)
OneToPow7_Record  = int(1)
OneToPow8_Record  = int(1)
OneToPow9_Record  = int(1)
OneToPow10_Record = int(1)
OneToPow11_Record = int(1)
OneToPow12_Record = int(1)

Target_List = MySW_List
if len(Target_List) == len(Cpu_List):
    print_ctr = 0
    for i in range(len(Target_List)):
        # Cpu_List[0]=3.000045
        same = Target_List[i] == Cpu_List[i]
        # print(Target_List[i],Cpu_List[i])
        OneToPow1 = math.fabs(Target_List[i] - Cpu_List[i]) < 1e-1
        OneToPow2 = math.fabs(Target_List[i] - Cpu_List[i]) < 1e-2
        OneToPow3 = math.fabs(Target_List[i] - Cpu_List[i]) < 1e-3
        OneToPow4 = math.fabs(Target_List[i] - Cpu_List[i]) < 1e-4
        OneToPow5 = math.fabs(Target_List[i] - Cpu_List[i]) < 1e-5
        OneToPow6 = math.fabs(Target_List[i] - Cpu_List[i]) < 1e-6
        OneToPow7 = math.fabs(Target_List[i] - Cpu_List[i]) < 1e-7
        OneToPow8 = math.fabs(Target_List[i] - Cpu_List[i]) < 1e-8
        OneToPow9 = math.fabs(Target_List[i] - Cpu_List[i]) < 1e-9
        OneToPow10 = math.fabs(Target_List[i] - Cpu_List[i]) < 1e-10
        OneToPow11 = math.fabs(Target_List[i] - Cpu_List[i]) < 1e-11
        OneToPow12 = math.fabs(Target_List[i] - Cpu_List[i]) < 1e-12
        
        Same_Record = int(same) & Same_Record
        OneToPow1_Record  = int(OneToPow1)  & OneToPow1_Record
        OneToPow2_Record  = int(OneToPow2)  & OneToPow2_Record
        OneToPow3_Record  = int(OneToPow3)  & OneToPow3_Record
        OneToPow4_Record  = int(OneToPow4)  & OneToPow4_Record
        OneToPow5_Record  = int(OneToPow5)  & OneToPow5_Record
        OneToPow6_Record  = int(OneToPow6)  & OneToPow6_Record
        OneToPow7_Record  = int(OneToPow7)  & OneToPow7_Record
        OneToPow8_Record  = int(OneToPow8)  & OneToPow8_Record
        OneToPow9_Record  = int(OneToPow9)  & OneToPow9_Record
        OneToPow10_Record = int(OneToPow10) & OneToPow10_Record
        OneToPow11_Record = int(OneToPow11) & OneToPow11_Record
        OneToPow12_Record = int(OneToPow12) & OneToPow12_Record\

        precision_cmp = ""
        if not same:
            precision_cmp = \
                ", <-1=" + str(int(OneToPow1)) + \
                ", <-2=" + str(int(OneToPow2)) + \
                ", <-3=" + str(int(OneToPow3)) + \
                ", <-4=" + str(int(OneToPow4)) + \
                ", <-5=" + str(int(OneToPow5)) + \
                ", <-6=" + str(int(OneToPow6)) + \
                ", <-7=" + str(int(OneToPow7)) + \
                ", <-8=" + str(int(OneToPow8)) + \
                ", <-9=" + str(int(OneToPow9)) + \
                ", <-10=" + str(int(OneToPow10)) + \
                ", <-11=" + str(int(OneToPow11)) + \
                ", <-12=" + str(int(OneToPow12))  
        print_en = False
        if print_ctr < 10:
            print_en = True
        else:
            if not same:
                print_en = True
        
        if print_en:
            print(
                "[TAR/CPU] {:.12f} /".format(Target_List[i]), 
                "{:.12f}".format(Cpu_List[i]), 
                "SAME =", int(same), 
                precision_cmp)
            print_ctr += 1
    #         print("worst1=",x[j-1])
    # worst = max(x)
    #print("worst=",worst)

# SAME is 0
# -1   is 1
# -2   is 2
# ...
worst = 0
if Same_Record:
    worst = 0
else:
    if OneToPow1_Record:
        worst = 1
    if OneToPow2_Record:
        worst = 2
    if OneToPow3_Record:
        worst = 3
    if OneToPow4_Record:
        worst = 4
    if OneToPow5_Record:
        worst = 5
    if OneToPow6_Record:
        worst = 6
    if OneToPow7_Record:
        worst = 7
    if OneToPow8_Record:
        worst = 8
    if OneToPow9_Record:
        worst = 9
    if OneToPow10_Record:
        worst = 10
    if OneToPow11_Record:
        worst = 11
    if OneToPow12_Record:
        worst = 12

golden_comapre_failed = False
if worst == 0:
    if test_golden != "SAME":
        golden_comapre_failed = True
elif worst == 1:
    if test_golden != "1e-1":
        golden_comapre_failed = True
elif worst == 2:
    if test_golden != "1e-2":
        golden_comapre_failed = True
elif worst == 3:
    if test_golden != "1e-3":
        golden_comapre_failed = True
elif worst == 4:
    if test_golden != "1e-4":
        golden_comapre_failed = True
elif worst == 5:
    if test_golden != "1e-5":
        golden_comapre_failed = True
elif worst == 6:
    if test_golden != "1e-6":
        golden_comapre_failed = True
elif worst == 7:
    if test_golden != "1e-7":
        golden_comapre_failed = True
elif worst == 8:
    if test_golden != "1e-8":
        golden_comapre_failed = True
elif worst == 9:
    if test_golden != "1e-9":
        golden_comapre_failed = True
elif worst == 10:
    if test_golden != "1e-10":
        golden_comapre_failed = True
elif worst == 11:
    if test_golden != "1e-11":
        golden_comapre_failed = True
elif worst == 12:
    if test_golden != "1e-12":
        golden_comapre_failed = True

if golden_comapre_failed:
    print("[FAILED] golden comapre failed : Golden =", test_golden, ", Worst =", worst)
else:
    print("[PASS]")