#
# https://stackoverflow.com/questions/52874575/subprocess-echo-environment-var-outputs-environment-var
#
import os
import subprocess
from subprocess import Popen
from os import environ
import math
import numpy
import sys

test_list = [
    ['fc/fc2.py', 'SAME'],
    ['fc/fc7.py', '1e-6'],
]

#
#   python test.py fc/fc2.py SAME
#   python test.py fc/fc7.py 1e-6
#
args_base = ['python', 'test.py']

for t in test_list:
    args = args_base.copy()
    args.append(t[0])
    args.append(t[1])
    print(args)
    p = subprocess.run(args, stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')
    for line in p:
        split = line.split()
        if len(split) > 1:
            if split[0] == '[FAILED]':
                print(line)
                exit()
    print('[PASS]')
print('[ALL_PASS]')
    