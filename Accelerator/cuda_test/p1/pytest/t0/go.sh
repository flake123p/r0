#!/bin/bash

CURR_PATH=$PWD
cd ../app/app_build_buda
#./run.sh
./run_rt.sh
./run_rtpp.sh
cd $CURR_PATH

#export LD_PRELOAD=/home/pupil/.local/lib/python3.10/site-packages/torch/lib/libtorch_cpu.so:./lib/libcuda.so.1:./lib/libcudart.so.11.0:./lib/libcublas.so.11:./lib/libcublasLt.so.11:./lib/libcudartpp.so:./lib/libbuda.so
#export LD_PRELOAD=./lib/libcuda.so.1:./lib/libcudart.so.11.0:./lib/libcublas.so.11:./lib/libcublasLt.so.11:./lib/libcudartpp.so:./lib/libfx3_rw.so
#export LD_PRELOAD=./lib/libbuda.so
#export LD_LIBRARY_PATH=./lib:/home/pupil/.local/lib/python3.10/site-packages/torch/lib/
#export LD_LIBRARY_PATH=./lib

buda_path=../../lib/buda/lib
cmd_path=../../lib/cmd/lib

cuda_drv=$buda_path/libcuda.so.1
cuda_rt=$buda_path/libcudart.so.11.0
cuda_blas=$buda_path/libcublas.so.11
cuda_blaslt=$buda_path/libcublasLt.so.11
#buda=$buda_path/libbuda.so
rtpp=$buda_path/libbudartpp.so

preload00=$cuda_drv:$cuda_rt:$cuda_blas:$cuda_blaslt:$buda:$rtpp

cmd_dev=$cmd_path/libcmd_device.so
cmd_sim_dev=$cmd_path/libcmd_device_sim.so
cmd_host=$cmd_path/libcmd_host.so

preload01=$cmd_dev:$cmd_sim_dev:$cmd_host
# echo $preload
# exit
export LD_PRELOAD=$preload00
export LD_LIBRARY_PATH=$buda_path:$cmd_path

#python softmax.py > log.run.log
python $1 pure_gpu
#strace -o __dummy_strace.txt python $1