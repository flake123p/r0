#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define __host__
#define __device__
#define __cudart_builtin__
#define CUDARTAPI
#define __dv(...) 
#define CUDART_CB


//#include "meta/tcp_client_lt2.txt"

#define PRLOC fprintf(stdout, "[PR] %s(), %d\n", __func__, __LINE__);
#define ERRLOC fprintf(stderr, "[ERR] %s(), %d\n", __func__, __LINE__);

#define RT_PRLOC PRLOC
#define RT_GET_DEVICE_PRLOC
#define RT_GET_LAST_ERROR_PRLOC
#define RT_PEEK_AT_LAST_ERROR_PRLOC

#define PROF_PRLOC PRLOC
//#define INTN_PRLOC ERRLOC
#define INTN_PRLOC
// #define RT_PRLOC TCP_FUNC();fprintf(stderr, "[ERR] %s(), %d\n", __func__, __LINE__);
// #define PROF_PRLOC TCP_FUNC()

#include "cmd_interface.h"
#include "cmd_host.h"

#include <driver_types.h>
#include <surface_types.h>
#include <texture_types.h>
#include <vector_types.h>
typedef void (CUDART_CB *cudaStreamCallback_t)(cudaStream_t stream, cudaError_t status, void *userData);

void *g_target_addr = NULL;

//
// Replace part
//
#define cudaMalloc__DEFINED
cudaError_t cudaMalloc(void **devPtr, size_t size)
{
    CMD_REQ_MALLOC_t req;
    req.req_hdr.cmd_id = CMD_REQ_MALLOC;
    req.size = (uint32_t)size;
    cmdHostSendReq(&req, sizeof(CMD_REQ_MALLOC_t));

    CMD_RES_MALLOC_t res;
    cmdHostRecvRes(&res, sizeof(CMD_RES_MALLOC_t));
    //assert(req.req_hdr.cmd_id == res.res_hdr.cmd_id);
    //assert(req.req_hdr.cmd_handle == res.res_hdr.cmd_handle);

    *devPtr = (void *)res.addr;

    // *devPtr = malloc(size);
    PRLOC;
    printf("size = %lu, loc = %p\n", size, *devPtr);
    return (cudaError_t)0;
}

#define cudaFree__DEFINED
 __cudart_builtin__ cudaError_t CUDARTAPI cudaFree(void *devPtr){
    free(devPtr);
    RT_PRLOC;
    return (cudaError_t)0;
}

#define cudaMemcpyAsync__DEFINED
__cudart_builtin__ cudaError_t CUDARTAPI cudaMemcpyAsync(void *dst, const void *src, size_t count, enum cudaMemcpyKind kind, cudaStream_t stream __dv(0)){
    memcpy(dst, src, count);
    RT_PRLOC;
    printf("[MEMCPY] cudaMemcpyKind = %d, count = %ld, dst = %p, src = %p\n", kind, count, dst, src);

#if 0
    if (count >= 8 && kind == cudaMemcpyDeviceToHost) {
        const float *pf = (const float *)src;
        printf("src[0] = %f\n", pf[0]);
        printf("src[1] = %f\n", pf[1]);
    } else if (count >= 4 && kind == cudaMemcpyDeviceToHost) {
        const float *pf = (const float *)src;
        printf("src[0] = %f\n", pf[0]);
    }
#endif
    return (cudaError_t)0;
}

#define cudaGetDeviceCount__DEFINED
__cudart_builtin__ cudaError_t CUDARTAPI cudaGetDeviceCount(int *count){
    RT_PRLOC;
    *count = 1;
    return (cudaError_t)0;
}

#define cudaGetDevice__DEFINED
__cudart_builtin__ cudaError_t CUDARTAPI cudaGetDevice(int *device){
    RT_GET_DEVICE_PRLOC;
    *device = 0;
    return (cudaError_t)0;
}

#define cudaGetLastError__DEFINED
__cudart_builtin__ cudaError_t CUDARTAPI cudaGetLastError(void){
    RT_GET_LAST_ERROR_PRLOC;
    return (cudaError_t)0;
}

#define cudaPeekAtLastError__DEFINED
__cudart_builtin__ cudaError_t CUDARTAPI cudaPeekAtLastError(void){
    RT_PEEK_AT_LAST_ERROR_PRLOC;
    return (cudaError_t)0;
}

#define cudaGetDeviceProperties__DEFINED
 __cudart_builtin__ cudaError_t CUDARTAPI cudaGetDeviceProperties(struct cudaDeviceProp *prop, int device){
    struct cudaDeviceProp pp;
    strcpy(pp.name, "NVIDIA GeForce GTX 1060 3GB");
    // pp.totalGlobalMem = 1024;
    // pp.major = 3;
    // pp.minor = 7;
    pp.uuid.bytes[0] = 0xD3;
    pp.uuid.bytes[1] = 0x95;
    pp.uuid.bytes[2] = 0xC7;
    pp.uuid.bytes[3] = 0x68;
    pp.uuid.bytes[4] = 0x51;
    pp.uuid.bytes[5] = 0x20;
    pp.uuid.bytes[6] = 0x70;
    pp.uuid.bytes[7] = 0x82;
    pp.uuid.bytes[8] = 0xEF;
    pp.uuid.bytes[9] = 0x79;
    pp.uuid.bytes[10] = 0xBE;
    pp.uuid.bytes[11] = 0x6;
    pp.uuid.bytes[12] = 0xAD;
    pp.uuid.bytes[13] = 0xBE;
    pp.uuid.bytes[14] = 0xD6;
    pp.uuid.bytes[15] = 0xE4;
    pp.luidDeviceNodeMask =  0;
    pp.totalGlobalMem = 3157065728;
    pp.sharedMemPerBlock = 49152;
    pp.regsPerBlock = 65536;
    pp.warpSize = 32;
    pp.memPitch = 2147483647;
    pp.maxThreadsPerBlock = 1024;
    pp.maxThreadsDim[0] = 1024;
    pp.maxThreadsDim[1] = 1024;
    pp.maxThreadsDim[2] = 64;
    pp.maxGridSize[0] = 2147483647;
    pp.maxGridSize[1] = 65535;
    pp.maxGridSize[2] = 65535;
    pp.clockRate = 1771500;
    pp.totalConstMem = 65536;
    pp.major =  6;
    pp.minor =  1;
    pp.textureAlignment = 512;
    pp.texturePitchAlignment = 32;
    pp.deviceOverlap =  1;
    pp.multiProcessorCount =  9;
    pp.kernelExecTimeoutEnabled =  1;
    pp.integrated =  0;
    pp.canMapHostMemory =  1;
    pp.computeMode =  0;
    pp.maxTexture1D = 131072;
    pp.maxTexture1DMipmap = 16384;
    pp.maxTexture1DLinear = 268435456;
    pp.maxTexture2D[0] = 131072;
    pp.maxTexture2D[1] = 65536;
    pp.maxTexture2DMipmap[0] = 32768;
    pp.maxTexture2DMipmap[1] = 32768;
    pp.maxTexture2DLinear[0] = 131072;
    pp.maxTexture2DLinear[1] = 65000;
    pp.maxTexture2DLinear[2] = 2097120;
    pp.maxTexture2DGather[0] = 32768;
    pp.maxTexture2DGather[1] = 32768;
    pp.maxTexture3D[0] = 16384;
    pp.maxTexture3D[1] = 16384;
    pp.maxTexture3D[2] = 16384;
    pp.maxTexture3DAlt[0] = 8192;
    pp.maxTexture3DAlt[1] = 8192;
    pp.maxTexture3DAlt[2] = 32768;
    pp.maxTextureCubemap = 32768;
    pp.maxTexture1DLayered[0] = 32768;
    pp.maxTexture1DLayered[1] = 2048;
    pp.maxTexture2DLayered[0] = 32768;
    pp.maxTexture2DLayered[1] = 32768;
    pp.maxTexture2DLayered[2] = 2048;
    pp.maxTextureCubemapLayered[0] = 32768;
    pp.maxTextureCubemapLayered[1] = 2046;
    pp.maxSurface1D = 32768;
    pp.maxSurface2D[0] = 131072;
    pp.maxSurface2D[1] = 65536;
    pp.maxSurface3D[0] = 16384;
    pp.maxSurface3D[1] = 16384;
    pp.maxSurface3D[2] = 16384;
    pp.maxSurface1DLayered[0] = 32768;
    pp.maxSurface1DLayered[1] = 2048;
    pp.maxSurface2DLayered[0] = 32768;
    pp.maxSurface2DLayered[1] = 32768;
    pp.maxSurface2DLayered[2] = 2048;
    pp.maxSurfaceCubemap = 32768;
    pp.maxSurfaceCubemapLayered[0] = 32768;
    pp.maxSurfaceCubemapLayered[1] = 2046;
    pp.surfaceAlignment = 512;
    pp.concurrentKernels =  1;
    pp.ECCEnabled =  0;
    pp.pciBusID =  1;
    pp.pciDeviceID =  0;
    pp.pciDomainID =  0;
    pp.tccDriver =  0;
    pp.asyncEngineCount =  2;
    pp.unifiedAddressing =  1;
    pp.memoryClockRate = 4004000;
    pp.memoryBusWidth = 192;
    pp.l2CacheSize = 1572864;
    pp.persistingL2CacheMaxSize =  0;
    pp.maxThreadsPerMultiProcessor = 2048;
    pp.streamPrioritiesSupported =  1;
    pp.globalL1CacheSupported =  1;
    pp.localL1CacheSupported =  1;
    pp.sharedMemPerMultiprocessor = 98304;
    pp.regsPerMultiprocessor = 65536;
    pp.managedMemory =  1;
    pp.isMultiGpuBoard =  0;
    pp.multiGpuBoardGroupID =  0;
    pp.hostNativeAtomicSupported =  0;
    pp.singleToDoublePrecisionPerfRatio = 32;
    pp.pageableMemoryAccess =  0;
    pp.concurrentManagedAccess =  1;
    pp.computePreemptionSupported =  1;
    pp.canUseHostPointerForRegisteredMem =  1;
    pp.cooperativeLaunch =  1;
    pp.cooperativeMultiDeviceLaunch =  1;
    pp.sharedMemPerBlockOptin = 49152;
    pp.pageableMemoryAccessUsesHostPageTables =  0;
    pp.directManagedMemAccessFromHost =  0;
    pp.maxBlocksPerMultiProcessor = 32;
    pp.accessPolicyMaxWindowSize =  0;
    pp.reservedSharedMemPerBlock =  0;
    RT_PRLOC;
    *prop = pp;
    return (cudaError_t)0;
}

#define _PTR(a) *((void **)(a))
dim3 g_curr_gridDim;
dim3 g_curr_blockDim;
size_t g_curr_sharedMem;
void *g_curr_stream;

#define cudaLaunchKernel__DEFINED
cudaError_t CUDARTAPI cudaLaunchKernel(const void *func, dim3 gridDim, dim3 blockDim, void **args, size_t sharedMem, cudaStream_t stream){
    printf("\n");
    RT_PRLOC;
    void **hdl = (void **)func;
    int isT = func == g_target_addr ? 1997 : 0;

    gridDim = g_curr_gridDim;
    blockDim = g_curr_blockDim;
    sharedMem = g_curr_sharedMem;
    stream = (cudaStream_t)g_curr_stream;

    printf("func = %p, hdl = %p [ sm = %lu ] ( %d )\n", func, *hdl, sharedMem, isT);
    printf("gridDim : %d, %d, %d\n", gridDim.x, gridDim.y, gridDim.z);
    printf("blockDim: %d, %d, %d\n", blockDim.x, blockDim.y, blockDim.z);
    {
        char *BudaGetFunString(const void *f);
        //printf("%s\n", BudaGetFunString(func));
    }
    {
        extern int budaLaunchKernel(const void *f, void **args);
        extern void budaLaunchKernelParam(unsigned int gridDim[3], unsigned int blockDim[3], size_t sharedMem, void *stream);
        
        unsigned int _gridDim[3] = {gridDim.x, gridDim.y, gridDim.z};
        unsigned int _blockDim[3] = {blockDim.x, blockDim.y, blockDim.z};

        budaLaunchKernelParam(_gridDim, _blockDim, sharedMem, stream);

        int ret = budaLaunchKernel(func, args);
        g_curr_gridDim.x = 0;
        g_curr_gridDim.y = 0;
        g_curr_gridDim.z = 0;
        g_curr_blockDim.x = 0;
        g_curr_blockDim.y = 0;
        g_curr_blockDim.z = 0;
        g_curr_sharedMem = 0;
        g_curr_stream = NULL;
        return (cudaError_t)ret;
    }
#if 0
    if (isT) {
        printf("ARGS: %p, %p, %p, %p, %p, %p, %p\n", 
            args[0],
            args[1],
            args[2],
            args[3],
            args[4],
            args[5],
            args[6]);

        printf("222: %p, %p, %p, %p, %p, %p, %p\n", 
            args[7],
            args[8],
            args[9],
            args[10],
            args[11],
            args[12],
            args[13]);

        printf("333: %p, %p, %p\n", 
            args[14],
            args[15],
            args[16]);

        printf("ARGS: %f, %f\n", 
            *(float *)(args[0]),
            *(float *)(args[1]));

        printf("ARGS: %p, %p, %p %p %p %p\n", 
            _PTR(args[0]),
            _PTR(args[1]),
            _PTR(args[2]),
            _PTR(args[4]),
            _PTR(args[5]),
            _PTR(args[16]));

        printf("gridDim : %d, %d, %d\n", gridDim.x, gridDim.y, gridDim.z);
        printf("blockDim: %d, %d, %d\n", blockDim.x, blockDim.y, blockDim.z);

        float *pf = (float *)_PTR(args[2]);
        float *pf1 = (float *)_PTR(args[2]+1);
        printf("%f\n", *pf);
        *pf = 97.f;

        {
            // add()
            float **abc = (float **)args[2];
            *abc[0] = *abc[1] + *abc[2] + 0.2f;
        }
        void **data = (void **)args[2];
        void *temp = data[0];
        printf("%f\n", *(float *)temp);
        temp = data[1];
        printf("%f\n", *(float *)temp);
        temp = data[2];
        printf("%f\n", *(float *)temp);
    }
#endif
    return (cudaError_t)0;
}

#define cudaStreamIsCapturing__DEFINED
cudaError_t CUDARTAPI cudaStreamIsCapturing(cudaStream_t stream, enum cudaStreamCaptureStatus *pCaptureStatus){
    RT_PRLOC;
    *pCaptureStatus = cudaStreamCaptureStatusNone;
    printf("cudaStreamCaptureStatus = %d\n", *pCaptureStatus);
    return (cudaError_t)0;
}

#define cudaThreadExchangeStreamCaptureMode__DEFINED
cudaError_t CUDARTAPI cudaThreadExchangeStreamCaptureMode(enum cudaStreamCaptureMode *mode){
    RT_PRLOC;
    *mode = cudaStreamCaptureModeGlobal;
    return (cudaError_t)0;
}

//
// Internal function part
//
#define __cudaRegisterFunction__DEFINED
void __cudaRegisterFunction(void **fatCubinHandle, const char *hostFun, char *deviceFun,
                            const char *deviceName, int thread_limit, uint3 *tid,
                            uint3 *bid, dim3 *bDim, dim3 *gDim, int *wSize)
{
    INTN_PRLOC;
    // char buf[1024];
    // strcpy(buf, hostFun);
    // printf("%-3lu ", strlen(hostFun));
    // for (int i = 0; i < strlen(hostFun); i++) {
    //     printf("%02X ", (unsigned char)buf[i]);
    // }
    // printf("\n");

    // fprintf(stderr, "[ERR] %s(), %d\n", __func__, __LINE__);
    // fprintf(stderr, "%s\n", hostFun);

    // fprintf(stdout, "%s\n", deviceFun);

    //printf("ABXX\n");

    {
        struct FuncParam {
            unsigned int tid[3];
            unsigned int bid[3];
            unsigned int bDIm[3];
            unsigned int gDim[3];
            int wSize;
        };
        // extern void __budaRegisterFunctionParam(const char *hostFun, struct FuncParam *param);
        // struct FuncParam pa = {0};
        //printf("%p %p %p %p %p\n", tid, bid, bDim, gDim, wSize);
        // pa.tid[0] = tid->x;
        // pa.tid[1] = tid->y;
        // pa.tid[2] = tid->z;
        // pa.bid[0] = bid->x;
        // pa.bid[1] = bid->y;
        // pa.bid[2] = bid->z;
        // if (bDim != NULL) {
        //     pa.bDIm[0] = bDim->x;
        //     pa.bDIm[1] = bDim->y;
        //     pa.bDIm[2] = bDim->z;
        // }
        // if (gDim != NULL) {
        //     pa.gDim[0] = gDim->x;
        //     pa.gDim[1] = gDim->y;
        //     pa.gDim[2] = gDim->z;
        // }
        //pa.wSize = *wSize;
        // __budaRegisterFunctionParam(hostFun, &pa);
    }

    void __budaRegisterFunction(const char *deviceName, const char *hostFun);
    __budaRegisterFunction(deviceName, hostFun);

#if 0
    fprintf(stdout, "++ %p, %s\n", *fatCubinHandle, deviceName);
    fprintf(stdout, "-- %p, %p\n", hostFun, deviceFun);
#endif
    char target[] = "_ZN2at6native29vectorized_elementwise_kernelILi4ENS0_15CUDAFunctor_addIfEENS_6detail5ArrayIPcLi3EEEEEviT0_T1_";
    if (strcmp(target, deviceName) == 0) {
        // printf("TARGET\n");
        // printf("TARGET\n");
        // printf("TARGET\n");
        g_target_addr = (void *)hostFun;
    }
}

#define __cudaRegisterVar__DEFINED
void CUDARTAPI __cudaRegisterVar(
        void **fatCubinHandle,
        char  *hostVar,
        char  *deviceAddress,
  const char  *deviceName,
        int    ext,
        int    size,
        int    constant,
        int    global
) {
    //INTN_PRLOC;
    //printf("@ %s / %s / %s\n", hostVar, deviceAddress, deviceName);
}

void *abxx[4] = {
    (void *)0x11,
    (void *)0x22,
    (void *)0x33,
    (void *)0x44
};

#define __cudaRegisterFatBinary__DEFINED
void** __cudaRegisterFatBinary(const void *fat_bin){
    //INTN_PRLOC;

    // void *fat_bin_container = malloc(sizeof(void *));
    // fat_bin_container = fat_bin;

    //void *fat_bin_container = malloc(sizeof(void *));
    return &abxx[3];
}

#define __cudaUnregisterFatBinary__DEFINED
void __cudaUnregisterFatBinary(void **handle) {
    //INTN_PRLOC;
    extern void BudaUnregister();
    BudaUnregister();
}

#define __cudaPushCallConfiguration__DEFINED
unsigned CUDARTAPI __cudaPushCallConfiguration(
    dim3 gridDim, dim3 blockDim, size_t sharedMem, void *stream) 
{
    INTN_PRLOC;
    g_curr_gridDim = gridDim;
    g_curr_blockDim = blockDim;
    g_curr_sharedMem = sharedMem;
    g_curr_stream = stream;
    return (cudaError_t)0;
}


//
// Keep these at bottom !!!
//
#include "../buda_meta/buda_internal_simple.txt"
#include "../buda_meta/buda_rt_simple.txt"
#include "../buda_meta/buda_rt_manual.txt"
#include "../buda_meta/buda_profiler_simple.txt"


/*
#
# export LD_PRELOAD=./libcuda.so.1:./libcudart.so.11.0:./libcublas.so.11:./libcublasLt.so.11
#
# export LD_PRELOAD=
#
# export LD_LIBRARY_PATH=.
# export LD_LIBRARY_PATH=
#
#
# ltrace -o __dummy_ltrace.txt -x 'cu*' python pth.py
#
# strace -o __dummy_strace.txt -f -F  python pth.py
#
#  python -m yep -c -o callgrind.out -- pth.py
#
#  unset GTK_PATH && kcachegrind
#
#  unset LD_PRELOAD
#  unset LD_LIBRARY_PATH
#
*/