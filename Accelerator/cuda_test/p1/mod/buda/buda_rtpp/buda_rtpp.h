#pragma once

#include "FunctionTraits.h"

#define C10_HOST_DEVICE__ C10_HOST_DEVICE
#undef C10_HOST_DEVICE
#define C10_HOST_DEVICE

//
// Template part
//
namespace {
template<typename arg1_t, typename arg2_t, typename return_t, typename func_t>
struct AUnaryFunctor {
  return_t operator()(arg2_t b) const {
    return f(a, b);
  }
  // NB: scalar is stored in higher precision!
  AUnaryFunctor(func_t f_, arg1_t a_): f(f_), a(a_) {}
  public:
    func_t f;
    arg1_t a;
};
template<typename arg1_t, typename arg2_t, typename return_t, typename func_t>
struct BUnaryFunctor {
  return_t operator()(arg1_t a) const {
    return f(a, b);
  }
  BUnaryFunctor(func_t f_, arg2_t b_): f(f_), b(b_) {}
  public:
    func_t f;
    arg2_t b;
};
template <typename arg1_t, typename arg2_t, typename return_t, typename func_t>
struct BinaryFunctor {
  __device__ return_t operator()(arg1_t a, arg2_t b) const {
    return f(a, b);
  }
  BinaryFunctor(func_t f_): f(f_) {}
  public:
    func_t f;
};
//
// Nonzero.cu
//
template<typename T>
struct NonZeroOp
{
    bool operator()(const T& a) const {
      return (a!=T(0));
    }
};

//
// Iterator
//
template <
    typename ValueType,
    typename ConversionOp,
    typename InputIteratorT,
    typename OffsetT = ptrdiff_t>
class TransformInputIterator
{
//private:
public:
    ConversionOp    conversion_op;
    InputIteratorT  input_itr;
};

//
// EQ
//
enum class EqOpType {EQ, NE};

template<typename scalar_t>
struct CompareEqFunctor{
  CompareEqFunctor(EqOpType op): op_(op) {}
  const EqOpType op_;
  bool operator() (scalar_t a, scalar_t b) const {
    if (op_ == EqOpType::EQ) {
      return a == b;
    } else { //NE
      return a != b;
    }

  }
 };
}
//
//
//
namespace {
//
// at::detail::Array
//
template <typename T, int size_>
struct at_detail_Array {
  T data[size_];

  C10_HOST_DEVICE T operator[](int i) const {
    return data[i];
  }
  C10_HOST_DEVICE T& operator[](int i) {
    return data[i];
  }
#if defined(USE_ROCM)
  C10_HOST_DEVICE Array() = default;
  C10_HOST_DEVICE Array(const Array&) = default;
  C10_HOST_DEVICE Array& operator=(const Array&) = default;
#else
  at_detail_Array() = default;
  at_detail_Array(const at_detail_Array&) = default;
  at_detail_Array& operator=(const at_detail_Array&) = default;
#endif
  static constexpr int size(){return size_;}
  // Fill the array with x.
  C10_HOST_DEVICE at_detail_Array(T x) {
    for (int i = 0; i < size_; i++) {
      data[i] = x;
    }
  }
};
//
// at::native::reduce_kernel
//
template<int nt, int output_vec_size, typename R>
void at_native_reduce_kernel(R reduction) {
  reduction.template run<output_vec_size>();
}
//
// at::native::MaxNanFunctor
//
template <typename acc_t>
struct at_native_MaxNanFunctor {
  acc_t operator()(acc_t a, acc_t b) const {
    return (std::isnan(a) || a > b) ? a : b;
  }
};
//
// at::native::func_wrapper_t
//
template <typename out_scalar_t, typename func_t>
struct at_native_func_wrapper_t {
  using arg_t = typename binary_function_traits<func_t>::arg1_t;
  using scalar_t = typename binary_function_traits<func_t>::arg2_t;

  func_t combine;
  static inline out_scalar_t project(arg_t arg) {
    return (out_scalar_t) arg;
  }
  static inline arg_t warp_shfl_down(arg_t arg, int offset) {
    //return WARP_SHFL_DOWN(arg, offset);
    return (arg_t)0;
  }

  static arg_t translate_idx(arg_t acc, int64_t /*idx*/) {
    return acc;
  }

  at_native_func_wrapper_t(const func_t& op) : combine(op) {
  }

  // wrap a normal reduction that ignores the index
  arg_t reduce(arg_t acc, scalar_t val, int64_t idx) const {
    return combine(acc, val);
  }
};
//
// ReduceOp
//
#if defined(USE_ROCM)
constexpr int MAX_DIMS = 16;
#else
constexpr int MAX_DIMS = 25;
#endif
//------------------------------------------------------------------------------------------------------
template <typename Value>
struct at_cuda_detail_IntDivider {
  Value divisor;
};
//------------------------------------------------------------------------------------------------------
template <int NARGS, typename index_t = uint32_t, bool signed_strides = false>
struct OffsetCalculator {
  using stride_t = std::conditional_t<signed_strides,
                                      std::make_signed_t<index_t>,
                                      index_t>;
  int dims;
  at_cuda_detail_IntDivider<index_t> sizes_[MAX_DIMS];
  stride_t strides_[MAX_DIMS][std::max<int>(NARGS, 1)];
};
//------------------------------------------------------------------------------------------------------
struct ReduceConfig {
  static constexpr int BLOCK_X = 0;
  static constexpr int BLOCK_Y = 1;
  static constexpr int CTA = 2;

  static constexpr int input_vec_size = 4;

  ReduceConfig(int element_size_bytes, int num_outputs, int num_inputs)
    : element_size_bytes(element_size_bytes)
    , num_inputs(num_inputs)
    , num_outputs(num_outputs) {}
  int element_size_bytes;
  int num_inputs;
  int num_outputs;
  int step_input = 1;
  int step_output = 1;
  int ctas_per_output = 1;
  int input_mult[3] = {0, 0, 0};
  int output_mult[2] = {0, 0};

  int block_width;
  int block_height;
  int num_threads;

  bool vectorize_input = false;
  int output_vec_size = 1;
};
//------------------------------------------------------------------------------------------------------
template <typename scalar_t, typename ops_t, typename index_t, typename out_scalar_t=scalar_t, int vt0=4>
struct at_native_ReduceOp {
  using traits = function_traits<decltype(&ops_t::reduce)>;
  using arg_t = typename std::decay<typename traits::template arg<0>::type>::type;

  using InputCalculator = OffsetCalculator<1, index_t>;
  using OutputCalculator = OffsetCalculator<2, index_t>;

  ops_t ops;
  arg_t ident;
  ReduceConfig config;
  InputCalculator input_calc;
  OutputCalculator output_calc;
  const void* src;
  const char* dst[2]; //it accepts at most two destinations
  // acc_buf used for accumulation among sub Tensor Iterator when accumulation on
  // output is not permissible
  void* acc_buf;
  // cta_buf used for accumulation between blocks during global reduction
  void* cta_buf;
  int* semaphores;
  int64_t base_idx;
  bool accumulate;
  bool final_output;
  int noutputs;
};
//
//
//
} //namespace {

#undef C10_HOST_DEVICE
#define C10_HOST_DEVICE C10_HOST_DEVICE__