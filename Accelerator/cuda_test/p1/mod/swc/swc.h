#ifndef __SWC_H_INCLUDED__
#define __SWC_H_INCLUDED__

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

//
// RT
//
int swcRtMalloc(uint64_t *addr, uint32_t size);

//
// RTPP
//
int swcRtppMulBroadcast(uint8_t dtype, uint64_t _dst, uint64_t _src1, uint64_t _src2, uint32_t batch_size, uint32_t stride, uint32_t element_count);

#ifdef __cplusplus
};//extern "C"
#endif

#endif //__SWC_H_INCLUDED__