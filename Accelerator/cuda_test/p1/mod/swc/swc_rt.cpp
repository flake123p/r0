#include "swc.h"
#include <stdlib.h>
#include <stdio.h>

int swcRtMalloc(uint64_t *addr, uint32_t size)
{
    printf(">>> %s()\n", __func__);
    void *ptr = malloc(size);
    *addr = (uint64_t)ptr;
    return 0;
}