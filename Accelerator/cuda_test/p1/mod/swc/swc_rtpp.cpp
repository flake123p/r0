#include "swc.h"
#include <stdlib.h>
#include <stdio.h>
#include "cmd_interface.h"

int swcRtppMulBroadcast(uint8_t dtype, uint64_t _dst, uint64_t _src1, uint64_t _src2, uint32_t batch_size, uint32_t stride, uint32_t element_count)
{
    printf(">>> %s(), dtype=%u [%u, %u, %u]\n", __func__, dtype, batch_size, stride, element_count);
    switch (dtype)
    {
        case CMD_DTYPE_FP32 : {
            float *dst = (float *)_dst;
            float *src1 = (float *)_src1;
            float *src2 = (float *)_src2;
            uint32_t i, j;

            for (i = 0; i < batch_size; i++) {
                for (j = 0; j < element_count; j++) {
                    dst[j] = src1[j] * src2[0];
                }
                dst += stride;
                src1 += stride;
                src2 += 1;
            }
        } break;
        
        default:
            break;
    }
    return 0;
}
