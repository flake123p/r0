#ifndef __CMD_INTERFACE_H_INCLUDED__
#define __CMD_INTERFACE_H_INCLUDED__

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    //system
    CMD_REQ_MALLOC,
    CMD_REQ_FREE,
    CMD_REQ_MEMCPY_H2D, //will sync = wait for all device activities done
    CMD_REQ_MEMCPY_D2D, //will sync = wait for all device activities done
    CMD_REQ_MEMCPY_D2H, //will sync = wait for all device activities done
    CMD_REQ_DEVICE_SYNC,

    //math
    CMD_REQ_SOFTMAX,
    CMD_REQ_MUL_POINTWISE,
    CMD_REQ_MUL_BROADCAST,

    CMD_MAX,
} CMD_REQ_t;

typedef enum {
    CMD_DTYPE_FP32 = 0,

    // TBD
    CMD_DTYPE_FP64,
    CMD_DTYPE_FP16,
    CMD_DTYPE_BP16,
    // CMD_DTYPE_TF32,
    CMD_DTYPE_INT64,
    CMD_DTYPE_INT32,
    CMD_DTYPE_INT16,
    CMD_DTYPE_INT8,
    // CMD_DTYPE_INT6,
    // CMD_DTYPE_INT5,
    // CMD_DTYPE_INT4,
    CMD_DTYPE_BOOL,
} CMD_DATA_TYPE_t;

typedef struct {
    uint16_t cmd_id; //CMD_REQ_t
    uint16_t cmd_handle;
    uint8_t data_type; //CMD_DATA_TYPE_t
    uint8_t RESERVED_0;
    uint16_t RESERVED_1;
} CMD_REQ_HDR_t; //Request Header

typedef struct {
    uint16_t cmd_id; //CMD_REQ_t
    uint16_t cmd_handle;
    int32_t status;
} CMD_RES_HDR_t; //Response Header


//
// System
//
typedef struct {
    CMD_REQ_HDR_t req_hdr;
    uint32_t size;
} CMD_REQ_MALLOC_t; //__host____device__cudaError_t cudaMalloc (void **devPtr, size_t size)

typedef struct {
    CMD_RES_HDR_t res_hdr;
    uint64_t addr;
} CMD_RES_MALLOC_t; //response

typedef struct {
    CMD_REQ_HDR_t req_hdr;
    uint64_t addr;
} CMD_REQ_FREE_t; //__host____device__cudaError_t cudaFree (void *devPtr)

typedef struct {
    CMD_REQ_HDR_t req_hdr;
    uint64_t dst;
    uint64_t src;
    uint32_t count;
    uint32_t kind;
} CMD_REQ_MEMCPY_t; //__host__cudaError_t cudaMemcpy (void *dst, const void *src, size_t count, cudaMemcpyKind kind)

//
// Math
//
typedef struct {
    CMD_REQ_HDR_t req_hdr;
    uint64_t dst;
    uint64_t src;
    uint32_t batch_size;
    uint32_t stride;
    uint32_t element_count;
    //const bool *mask = nullptr
    //const int head_chunk_size = -1
    //bool is_transformer_mask
} CMD_REQ_SOFTMAX_t; // ref: softmax_warp_forward()

typedef struct {
    CMD_REQ_HDR_t req_hdr;
    uint64_t dst;
    uint64_t src1;
    uint64_t src2;
    uint32_t element_count;
} CMD_REQ_MUL_POINTWISE_t; // ref: Kernel_MUL_FUNC_BINARY()

typedef struct {
    CMD_REQ_HDR_t req_hdr;
    uint64_t dst;
    uint64_t src1; // long one
    uint64_t src2; // short one
    uint32_t batch_size;
    uint32_t stride;
    uint32_t element_count;
} CMD_REQ_MUL_BROADCAST_t; // ref: Kernel_MUL_FUNC_BROADCAST_F32()

#ifdef __cplusplus
};//extern "C"
#endif

#endif//__CMD_INTERFACE_H_INCLUDED__