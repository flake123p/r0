#include <string.h>
#include "cmd_interface.h"
#include "cmd_call.h"
#include "cmd_parser.h"

static void cmd_prepare_response_header(void *req, void *res)
{
    CMD_REQ_HDR_t *_req = (CMD_REQ_HDR_t *)req;
    CMD_RES_HDR_t *_res = (CMD_RES_HDR_t *)res;
    _res->cmd_id = _req->cmd_id;
}

void cmd_parser(CMD_REQ_HDR_t *cmd_hdr)
{
    switch (cmd_hdr->cmd_id) {
        //
        // RT
        //
        case CMD_REQ_MALLOC: {
            CMD_REQ_MALLOC_t *req = (CMD_REQ_MALLOC_t *)cmd_hdr;
            CMD_RES_MALLOC_t res;
            cmd_prepare_response_header(req, &res);
            res.res_hdr.status = CALL_CMD_REQ_MALLOC(&res.addr, req->size);
            REPLY_RESPONSE(&res, sizeof(CMD_RES_MALLOC_t));
        } break;

        //
        // RTPP
        //
        case CMD_REQ_MUL_BROADCAST: {
            CMD_REQ_MUL_BROADCAST_t *req = (CMD_REQ_MUL_BROADCAST_t *)cmd_hdr;
            CMD_RES_HDR_t res;
            cmd_prepare_response_header(req, &res);
            res.status = CALL_CMD_REQ_MUL_BROADCAST(
                    req->req_hdr.data_type,
                    req->dst,
                    req->src1,
                    req->src2,
                    req->batch_size,
                    req->stride,
                    req->element_count);
            REPLY_RESPONSE(&res, sizeof(CMD_RES_MALLOC_t));
        } break;

        default:
            break;
    }
}