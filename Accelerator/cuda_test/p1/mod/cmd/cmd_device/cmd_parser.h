#ifndef __CMD_PARSER_H_INCLUDED__
#define __CMD_PARSER_H_INCLUDED__

#include "cmd_interface.h"

#ifdef __cplusplus
extern "C" {
#endif

void cmd_parser(CMD_REQ_HDR_t *cmd_hdr);

void cmd_get_response(void *res, uint32_t len); //This will be moved out of here.
extern void cmd_response(void *res, uint32_t len); //This will be moved out of here.
#define REPLY_RESPONSE(res,len) cmd_response(res,len)

#ifdef __cplusplus
};//extern "C"
#endif

#endif //__CMD_PARSER_H_INCLUDED__