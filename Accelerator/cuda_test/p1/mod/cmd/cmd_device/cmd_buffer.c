#include <string.h>
#include "cmd_interface.h"
#include "cmd_call.h"

#define RES_BUF_LEN (128) //This will be moved out of here.
uint8_t g_res_buf[RES_BUF_LEN]; //This will be moved out of here.

void cmd_response(void *res, uint32_t len) //This will be moved out of here.
{
    memcpy(g_res_buf, res, len);
}

void cmd_get_response(void *res, uint32_t len) //This will be moved out of here.
{
    memcpy(res, g_res_buf, len);
}
