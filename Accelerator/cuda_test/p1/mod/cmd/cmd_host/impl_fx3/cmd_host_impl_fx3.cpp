#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

int cmdHostSendReq(void *cmdBuf, uint32_t bufLenInBytes)
{
    return 0;
}

int cmdHostRecvRes(void *cmdResBuf, uint32_t bufLenInBytes)
{
    return 0;
}

#ifdef __cplusplus
};//extern "C"
#endif