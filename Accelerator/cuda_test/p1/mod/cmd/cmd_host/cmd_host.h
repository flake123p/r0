#pragma once

#include <stdint.h>
#include <cmd_interface.h>

#ifdef __cplusplus
extern "C" {
#endif

int cmdHostSendReq(void *cmdBuf, uint32_t bufLenInBytes);
int cmdHostRecvRes(void *cmdResBuf, uint32_t bufLenInBytes);

#ifdef __cplusplus
};//extern "C"
#endif