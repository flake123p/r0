#include "cmd_host.h"
#include "cmd_device_sim.h"

#ifdef __cplusplus
extern "C" {
#endif

int cmdHostSendReq(void *cmdBuf, uint32_t bufLenInBytes)
{
    cmdDeviceChanHostSendReq(cmdBuf, bufLenInBytes);
    return 0;
}

int cmdHostRecvRes(void *cmdResBuf, uint32_t bufLenInBytes)
{
    cmdDeviceChanHostRecvRes(cmdResBuf, bufLenInBytes);
    return 0;
}

#ifdef __cplusplus
};//extern "C"
#endif
