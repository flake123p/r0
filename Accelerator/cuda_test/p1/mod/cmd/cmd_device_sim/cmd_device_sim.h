#include "cmd_interface.h"

#ifdef __cplusplus
extern "C" {
#endif

int cmdDeviceChanHostSendReq(void *cmdBuf, uint32_t bufLenInBytes);
int cmdDeviceChanHostRecvRes(void *cmdResBuf, uint32_t bufLenInBytes);

#ifdef __cplusplus
};//extern "C"
#endif