cmake_minimum_required(VERSION 3.6)

file(GLOB SRC CONFIGURE_DEPENDS "*.c" "*.cpp" "*.cxx" "*.cc")

add_library(cmd_device_sim ${SRC})
SET_SOURCE_FILES_PROPERTIES(${SRC} PROPERTIES LANGUAGE CXX )

target_include_directories(
    cmd_device_sim PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR}"
)

include(${CMAKE_CURRENT_SOURCE_DIR}/../../../modlists/cmd/cmd_device.cmake)
cmd_device_cmake(cmd_device_sim) # include, subdirectory, link

target_link_libraries(cmd_device_sim PUBLIC)
