#include <stdio.h>
#include "cmd_parser.h"

#ifdef __cplusplus
extern "C" {
#endif

int cmdDeviceChanHostSendReq(void *cmdBuf, uint32_t bufLenInBytes)
{
    printf(">>> %s(), Channel = SIM\n", __func__);
    cmd_parser((CMD_REQ_HDR_t *)cmdBuf);
    return 0;
}

int cmdDeviceChanHostRecvRes(void *cmdResBuf, uint32_t bufLenInBytes)
{
    printf(">>> %s(), Channel = SIM\n", __func__);
    cmd_get_response(cmdResBuf, bufLenInBytes);
    return 0;
}

#ifdef __cplusplus
};//extern "C"
#endif