#include "swc.h"

#define DUMMY(...) (0)

#define CALL_CMD_REQ_MALLOC(addr,size) swcRtMalloc(addr,size)
#define CALL_CMD_REQ_MUL_BROADCAST(dtype,dst,src1,src2,batch,stride,count) swcRtppMulBroadcast(dtype,dst,src1,src2,batch,stride,count)