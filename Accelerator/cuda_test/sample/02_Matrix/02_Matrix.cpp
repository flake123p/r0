#include <cublasLt.h>
#include <cuda_runtime.h>
#include <cublas.h>
#include <stdio.h>
#include <stdlib.h>
#include "cublas_v2.h"



int main(){
    {
        printf("cudaCreate\n");
        cublasStatus_t err;
        cublasHandle_t handle;
        err = cublasCreate(&handle);
        printf("err= %d\n",err);
    }
    {
        printf("cublasSetStream\n");
        cublasStatus_t err;
        cudaStream_t stream0;
        cudaStreamCreate(&stream0);
        cublasHandle_t handle;
        cublasCreate(&handle);
        err=cublasSetStream(handle, stream0);
        printf("err= %d\n",err);
        // This function sets the cuBLAS library stream, which will be used to execute all subsequent calls 
        // to the cuBLAS library functions. If the cuBLAS library stream is not set, all kernels use the default NULL
        // stream. 
    }
    {
        printf("cublasSetWorkspace\n");
        cublasStatus_t err;
        cublasHandle_t handle;
        cublasCreate(&handle);
        void *workspace;
        err=cublasSetWorkspace(handle,workspace, 256); 
        printf("err= %d\n",err);
    }
    {
        printf("cublasSetMathMode\n");
        cublasStatus_t err;
        cublasHandle_t handle;
        cublasCreate(&handle);
        cublasMath_t mode=CUBLAS_DEFAULT_MATH;
        err=cublasSetMathMode(handle,mode); 
        printf("err= %d\n",err);
    }
    {
        printf("cublasSgemm\n");
        cublasStatus_t err;
        cublasHandle_t handle;
        cublasCreate(&handle);
        cublasOperation_t transa=CUBLAS_OP_N;
        cublasOperation_t transb=CUBLAS_OP_N;
        int m=2,n=2,k=3;
        float alpha=1;
        float beta=0;
        float* h_A=(float*)malloc(sizeof(float)*m*k);
        float* h_B=(float*)malloc(sizeof(float)*k*n);
        float* h_C=(float*)malloc(sizeof(float)*m*n);
        for(int i=0;i<m*k;i++){
                h_A[i]=i;
                //printf("h_A=%f\n",h_A[i]);
            }
        for(int i=0;i<n*k;i++){
                h_B[i]=i;
                //printf("h_B=%f\n",h_B[i]);
            }
        float* d_A=NULL;
        float* d_B=NULL;
        float* d_C=NULL;
        cudaMalloc((void **)&d_A, sizeof(float)*m*k);
        cudaMemcpy(d_A, h_A, sizeof(float)*m*k, cudaMemcpyHostToDevice);
        cudaMalloc((void **)&d_B, sizeof(float)*k*n);
        cudaMemcpy(d_B, h_B, sizeof(float)*k*n, cudaMemcpyHostToDevice);
        cudaMalloc((void **)&d_C, sizeof(float)*m*n);
        cudaMemcpy(d_C, h_C, sizeof(float)*m*n, cudaMemcpyHostToDevice);

        err=cublasSgemm(handle,transa,transb,m,n,k,&alpha,d_A,m,d_B,k,&beta,d_C,m);
        printf("err= %d\n",err);
        cudaMemcpy(h_C, d_C, sizeof(float)*m*n, cudaMemcpyDeviceToHost);
        for(int i=0;i<m*n;i++){
                //printf("%f\n",h_C[i]);
            }
        free(h_A);
        free(h_B);
        free(h_C);
        cudaFree(d_A);
        cudaFree(d_B);
        cudaFree(d_C);
    }
    return 0;
}