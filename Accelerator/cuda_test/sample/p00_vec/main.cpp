#include <stdio.h>
#include<iostream>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <tuple>
#include "help.hpp"

// STUB START
struct _dim3
{
    unsigned int x, y, z;
};
struct _dim3 threadIdx = {0,0,0};
struct _dim3 blockIdx = {0,0,0};
constexpr int _thread_work_size() { return 4; }
constexpr int _block_work_size() { return 1; }
constexpr uint32_t _num_threads() {
  return 32 * 4;
}
//
// STUB END
//

template <typename T>
struct function_traits : public function_traits<decltype(&T::operator())> {
};

// Pointers to class members that are themselves functors.
// For example, in the following code:
// template <typename func_t>
// struct S {
//     func_t f;
// };
// template <typename func_t>
// S<func_t> make_s(func_t f) {
//     return S<func_t> { .f = f };
// }
//
// auto s = make_s([] (int, float) -> double { /* ... */ });
//
// function_traits<decltype(&s::f)> traits;
template <typename ClassType, typename T>
struct function_traits<T ClassType::*> : public function_traits<T> {
};

// Const class member functions
template <typename ClassType, typename ReturnType, typename... Args>
struct function_traits<ReturnType(ClassType::*)(Args...) const> : public function_traits<ReturnType(Args...)> {
};

// Reference types
template <typename T>
struct function_traits<T&> : public function_traits<T> {};
template <typename T>
struct function_traits<T*> : public function_traits<T> {};

// Free functions
template <typename ReturnType, typename... Args>
struct function_traits<ReturnType(Args...)> {
  // arity is the number of arguments.
  enum { arity = sizeof...(Args) };

  typedef std::tuple<Args...> ArgsTuple;
  typedef ReturnType result_type;

  template <size_t i>
  struct arg
  {
      typedef typename std::tuple_element<i, std::tuple<Args...>>::type type;
      // the i-th argument is equivalent to the i-th tuple element of a tuple
      // composed of those arguments.
  };
};


/////////////////////////////////////////////////////////////////////
template<typename scalar_t, int vec_size>
struct alignas(sizeof(scalar_t) * vec_size) aligned_vector {
  scalar_t val[vec_size];
};

template <int vec_size, typename scalar_t>
aligned_vector<scalar_t, vec_size> load_vector(const scalar_t *base_ptr, uint32_t offset) {
  using vec_t = aligned_vector<scalar_t, vec_size>;
  auto *from = reinterpret_cast<const vec_t *>(base_ptr);
  return from[offset];
}

template<template<int i> typename func, int end, int current=0>
struct static_unroll {
  template<typename... Args>
  static inline  void with_args(Args&&... args) { //C10_HOST_DEVICE???
    func<current>::apply(std::forward<Args>(args)...);
    static_unroll<func, end, current+1>::with_args(args...);
  }
};

template<int arg_index>
struct vectorized_load_helper {
  template <typename args_t, typename policy_t>
  static __device__ void apply(policy_t &self, args_t *args, int idx) {
    using arg_t = std::tuple_element_t<arg_index, args_t>;
    // `data` hold the data_ptr for tensors [output, input0, input1, ...], so we
    // need a +1 offset to get the input
    auto ptr = reinterpret_cast<arg_t *>(self.data[arg_index + 1]) + _block_work_size() * idx;
    auto args_accessor = [&args] __device__ (int thread_unroll_idx) -> arg_t & { return std::get<arg_index>(args[thread_unroll_idx]); };
    self.load_single_arg(args_accessor, ptr);
  }
};

template <int vec_size, typename data_t>  // vec_size: number of scalars, can be 1, 2, or 4.
struct vectorized {

  static_assert(_thread_work_size() % vec_size == 0, "The workload per thread must be a multiple of vec_size");
  static constexpr int loop_size = _thread_work_size() / vec_size;

  data_t data;

  vectorized(data_t data) : data(data) {}

  inline constexpr bool check_inbounds(int thread_work_elem) {
    return true;
  }

  template<typename args_t>
  __device__ inline void load(args_t *args, int idx) {
      constexpr int arity = std::tuple_size<args_t>::value;
      static_unroll<vectorized_load_helper, arity>::with_args(*this, args, idx);//?
  }

  template<typename scalar_t>
  __device__ inline void store(scalar_t *from, int idx) {
      using vec_t = aligned_vector<scalar_t, vec_size>;
      scalar_t *to = reinterpret_cast<scalar_t *>(data[0]) + _block_work_size() * idx;
      vec_t *to_ = reinterpret_cast<vec_t *>(to);
      int thread_idx = threadIdx.x;
      #pragma unroll
      for (int i = 0; i < loop_size; i++) {
        int index = thread_idx + i * _num_threads();
        vec_t v;
        for (int j = 0; j < vec_size; j++) {
          v.val[j] = from[vec_size * i + j];
        }
        to_[index] = v;
      }
  }

  template<typename accessor_t, typename scalar_t>
  inline void load_single_arg(accessor_t to, scalar_t *from) {
    int thread_idx = threadIdx.x;
    //#pragma unroll
    for (int i = 0; i < loop_size; i++) {
      int index = thread_idx + i * _num_threads();
      auto v = load_vector<vec_size>(from, index);
      //#pragma unroll
      for (int j = 0; j < vec_size; j++) {
        to(vec_size * i + j) = v.val[j];
      }
    }
  }
};

// struct StoreWithoutCast {
//   template<typename scalar_t>
//   __device__ void store(scalar_t value, char *base_ptr, uint32_t offset, int arg = 0) {
//     *(reinterpret_cast<scalar_t *>(base_ptr) + offset) = value;
//   }
// };

// struct LoadWithoutCast {
//   template<typename scalar_t>
//   __device__ scalar_t load(char *base_ptr, uint32_t offset, int arg) {
//     return c10::load(reinterpret_cast<scalar_t *>(base_ptr) + offset);
//   }
// };



template <typename T, int size_>
struct Array {
  T data[size_];

  T operator[](int i) const {
    return data[i];
  }
  T& operator[](int i) {
    return data[i];
  }
#if defined(USE_ROCM)
  C10_HOST_DEVICE Array() = default;
  C10_HOST_DEVICE Array(const Array&) = default;
  C10_HOST_DEVICE Array& operator=(const Array&) = default;
#else
  Array() = default;
  Array(const Array&) = default;
  Array& operator=(const Array&) = default;
#endif
  static constexpr int size(){return size_;}
  // Fill the array with x.
  Array(T x) {
    for (int i = 0; i < size_; i++) {
      data[i] = x;
    }
  }
};

template <int NARGS, typename index_t = uint32_t>
struct TrivialOffsetCalculator {
  // The offset for each argument. Wrapper around fixed-size array.
  // The offsets are in # of elements, not in bytes.
  // On CUDA, zero sized array is not allowed, so when we are handling nullary
  // operators, we need to create a size 1 offset to avoid compiler failure.
  // This size 1 offset is just a placeholder, and we will not use it.
  using offset_type = Array<index_t, std::max<int>(NARGS, 1)>;

  offset_type get(index_t linear_idx) const {
    offset_type offsets;
    #pragma unroll
    for (int arg = 0; arg < NARGS; arg++) {
      offsets[arg] = linear_idx;
    }
    return offsets;
  }
};

template <class F, class Tuple, std::size_t... INDEX>
constexpr decltype(auto) apply_impl(
    F&& f,
    Tuple&& t,
    std::index_sequence<INDEX...>);

template <class F, class Tuple>
constexpr decltype(auto) apply(F&& f, Tuple&& t) {
  return apply_impl(
      std::forward<F>(f),
      std::forward<Tuple>(t),
      std::make_index_sequence<
          std::tuple_size<std::remove_reference_t<Tuple>>::value>{});
};

template<typename func_t, typename policy_t>
__device__ inline void elementwise_kernel_helper(func_t f, policy_t policy) {
    // printf("%s()\n", __func__);
    // f(policy.data[0],policy.data[1],policy.data[2]);
    // for(int i=0;i<3;i++){
    //     printf("%f\n",policy.data[2][i]);
    // }
    using traits = function_traits<func_t>;
    using return_t = typename traits::result_type; //???
    using args_t = typename traits::ArgsTuple;

    int idx = blockIdx.x;

    return_t results[_thread_work_size()];
    args_t args[_thread_work_size()];

    // load
    policy.load(args, idx);

    // compute
    // #pragma unroll
    // for (int i = 0; i < _thread_work_size(); i++) {
    //     if (policy.check_inbounds(i)) {
    //         results[i] = apply(f, args[i]);
    //     }
    // }

    // store
    //policy.store(results, idx);  
}

template<int vec_size, typename func_t, typename array_t>
void vectorized_elementwise_kernel(int N, func_t f, array_t data) {
  //using traits = function_traits<func_t>;
  //int remaining = N - _block_work_size() * blockIdx.x;

  // if (remaining < block_work_size()) {  // if this block handles the reminder, just do a naive unrolled loop
  //   auto input_calc = TrivialOffsetCalculator<traits::arity>();
  //   auto output_calc = TrivialOffsetCalculator<1>();
  //   auto loader = memory::LoadWithoutCast();
  //   auto storer = memory::StoreWithoutCast();
  //   auto policy = memory::policies::unroll<array_t, decltype(input_calc), decltype(output_calc),
  //                                          memory::LoadWithoutCast, memory::StoreWithoutCast>(
  //     data, remaining, input_calc, output_calc, loader, storer);
  //   elementwise_kernel_helper(f, policy);
  // } else {  // if this block has a full `block_work_size` data to handle, use vectorized memory access
  //   elementwise_kernel_helper(f, memory::policies::vectorized<vec_size, array_t>(data));
  // }
  elementwise_kernel_helper(f, vectorized<vec_size, array_t>(data));
}


template<typename scalar_t>
struct AbsFunctor {
    __device__ __forceinline__ scalar_t operator() (const scalar_t a) const {
    return std::abs(a);
  }
};


int main() 
{
    float in1[] = {1.0f, 2.0f, 35.0f};
    float in2[] = {1.0f, 2.0f, 3.0f};
    float out[] = {1.0f, 2.0f, 3.0f};
    float* in[3];
    in[0]=in1;
    in[1]=in2;
    in[2]=out;
    AbsFunctor<float> float_abs;
    vectorized_elementwise_kernel<1>(3,float_abs,in1);
    return 0;
}
