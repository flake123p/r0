#include <stdio.h>
#include <iostream>
#include <tuple>
#include "help.hpp"

#include "01_functor.hpp"

int test_01() 
{
    struct AbsFunctor<int> absf;

    printf("abs 3  = %d\n", absf(3));
    printf("abs -7 = %d\n", absf(-7));
    
    return 0;
}
int add(int a,int b){
    return a+b;
}
int test_02() 
{
    struct BinaryFunctor<int,int,int, decltype(&add)> binf(&add);

    printf("binf 3+4  = %d\n", binf(3,4));
   
    
    return 0;
}

int test_03() 
{
    struct CompareEqFunctor<int> eqop(EqOpType::EQ);

    printf("eqop 4,4  = %d\n",eqop(4,4));

    struct CompareEqFunctor<int> neqop(EqOpType::NE);

    printf("neqop 4,4  = %d\n",neqop(4,4));
   
    
    return 0;
}

int main() 
{
    test_01();
    test_02();
    test_03();
    return 0;
}