#include <stdio.h>
#include <iostream>
#include <tuple>
//#include "help.hpp"

#include "01_functor.hpp"

#define __ubsan_ignore_float_divide_by_zero__
#define __ubsan_ignore_undefined__
#define __ubsan_ignore_signed_int_overflow__
#define __ubsan_ignore_pointer_overflow__
#define __ubsan_ignore_function__
#define C10_HOST_DEVICE
#define C10_DEVICE 
#define C10_HOST 
#define C10_ALWAYS_INLINE __attribute__((__always_inline__)) inline
#define __device__

namespace at {

template <typename scalar_t>
struct OpMathType {
  using type = scalar_t;
};
template <typename T>
using opmath_type = typename OpMathType<T>::type;

namespace native {
namespace ufunc {

template <typename T>
C10_HOST_DEVICE C10_ALWAYS_INLINE T add(T self, T other, T alpha) __ubsan_ignore_undefined__ {
  return self + alpha * other;
}

}}}  // namespace at::native::ufunc

namespace at {
namespace native {
template <typename scalar_t>
struct CUDAFunctor_add {
  using opmath_t = at::opmath_type<scalar_t>;
  opmath_t alpha_;
  CUDAFunctor_add(opmath_t alpha) : alpha_(alpha) {}
  __device__ scalar_t operator()(scalar_t self, scalar_t other) const {
    return ufunc::add(static_cast<opmath_t>(self), static_cast<opmath_t>(other), alpha_);
  }
};
}}  // namespace at::native

int test_02() 
{
    auto ans = at::native::ufunc::add<int>(1,2,3);

    printf("ans = %d\n", ans);

    at::native::CUDAFunctor_add<int> cuda_add(1);
    auto ans2 = cuda_add(1,2);

    printf("ans = %d\n", ans);
    
    return 0;
}

int main() 
{
    return test_02();
}
