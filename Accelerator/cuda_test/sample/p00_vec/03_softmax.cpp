#include <stdio.h>
#include <iostream>
#include <tuple>
#include "help.hpp"
#include <limits>
#include <stdexcept>
#include "p03_softmax.hpp"

int test_01(){
    float in[]={1.0f,0.0f,0.0f};
    float out[]={1.0f,0.0f,0.0f};
    softmax_warp_forward<float, float, float, 10, true, false>(out,in,100,0,3);
    return 0;
}

int main(){
    //printf("--------");
    test_01();
    return 0;
}