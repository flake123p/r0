
#pragma once

template<typename scalar_t>
struct AbsFunctor {
    scalar_t operator() (const scalar_t a) const {
        return std::abs(a);
    }
};


template <typename arg1_t, typename arg2_t, typename return_t, typename func_t>
struct BinaryFunctor {
    return_t operator()(arg1_t a, arg2_t b) const {
      return f(a, b);
    }
    BinaryFunctor(func_t f_): f(f_) {}
    private:
        func_t f;
};

enum class EqOpType {EQ, NE};

template<typename scalar_t>
struct CompareEqFunctor{
  CompareEqFunctor(EqOpType op): op_(op) {}
  const EqOpType op_;
  __device__ __forceinline__ bool operator() (scalar_t a, scalar_t b) const {
    if (op_ == EqOpType::EQ) {
      return a == b;
    } else { //NE
      return a != b;
    }

  }
};

// template<typename arg1_t, typename arg2_t, typename return_t, typename func_t>
// struct AUnaryFunctor {
//     using traits = function_traits<func_t>;
//     using opmath_arg1_t = typename traits::template arg<0>::type;
//     __device__ return_t operator()(arg2_t b) const {
//         return f(a, b);
//     }
//     // NB: scalar is stored in higher precision!
//     AUnaryFunctor(func_t f_, opmath_arg1_t a_): f(f_), a(a_) {}
//     private:
//         func_t f;
//         opmath_arg1_t a;
// };

//softmax_warp_forward