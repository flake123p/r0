#include <stdio.h>
//
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

#include "help.hpp"

cudaError_t rc;

int main() {
    //
    // Create corresponding int arrays on the GPU.
    // ('d' stands for "device".)
    //
    int *da, *db;
    cudaMalloc((void **)&da, 10*sizeof(int));
    cudaMalloc((void **)&db, 10*sizeof(int));

    cudaFree(da);
    cudaFree(db);

    printf("Hello Example\n");
   

    int devID = 0;
    int computeMode = -1, major = 0, minor = 0;
    checkCudaErrors(cudaDeviceGetAttribute(&computeMode, cudaDevAttrComputeMode, devID));
    checkCudaErrors(cudaDeviceGetAttribute(&major, cudaDevAttrComputeCapabilityMajor, devID));
    checkCudaErrors(cudaDeviceGetAttribute(&minor, cudaDevAttrComputeCapabilityMinor, devID));

    DND(computeMode);
    DND(major);
    DND(minor);

    {
        printf("cudaGetDeviceCount\n");
        int devCount;
        rc = cudaGetDeviceCount(&devCount);
        printf("RC = %d, devCount = %d\n", rc, devCount);
    }
    {
        printf("cudaGetDevice\n");
        int dev;
        rc = cudaGetDevice(&dev);
        DND(rc);
        DND(dev);
    }
    {
        printf("cudaSetDevice\n");
        rc = cudaSetDevice(0);
        DND(rc);
    }
    {
        printf("cudaGetDevice\n");
        int dev;
        rc = cudaGetDevice(&dev);
        DND(rc);
        DND(dev);
    }
    {
        printf("cudaGetDeviceCount\n");
        int devCount;
        rc = cudaGetDeviceCount(&devCount);
        printf("RC = %d, devCount = %d\n", rc, devCount);
    }
    {
        printf("cudaGetDeviceProperties\n");
        struct cudaDeviceProp prop;
        rc = cudaGetDeviceProperties(&prop, 0);
        printf("RC = %d, name = %s\n", rc, prop.name);
    }
    return 0;
}
