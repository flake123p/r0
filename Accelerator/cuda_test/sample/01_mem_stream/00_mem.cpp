#include <stdio.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

#include "help.hpp"

/*
Pure mem copy:
    H2D: 364us
    D2H: 964us
*/

#define TEST_DATA_LEN  (1024*1024*20)
#define TEST_DATA_SIZE (TEST_DATA_LEN*sizeof(int))

int main() {
    int *da;
    cudaMalloc((void **)&da, TEST_DATA_SIZE);

    int *ha;
    ha = (int *)malloc(TEST_DATA_SIZE);
    int *hb;
    hb = (int *)malloc(TEST_DATA_SIZE);

    for (int i = 0; i < 1024 * 1024; i++) {
        ha[i] = i;
    }
    // for (int i = 0; i < 128; i++) {
    //     printf("%8d ", ha[i]);
    //     if (i % 16 == 15)
    //         printf("\n");
    // }
#if 0
    extern __host__ cudaError_t CUDARTAPI cudaMemcpy(void *dst, const void *src, size_t count, enum cudaMemcpyKind kind);
    enum __device_builtin__ cudaMemcpyKind
    {
        cudaMemcpyHostToHost          =   0,      /**< Host   -> Host */
        cudaMemcpyHostToDevice        =   1,      /**< Host   -> Device */
        cudaMemcpyDeviceToHost        =   2,      /**< Device -> Host */
        cudaMemcpyDeviceToDevice      =   3,      /**< Device -> Device */
        cudaMemcpyDefault             =   4       /**< Direction of the transfer is inferred from the pointer values. Requires unified virtual addressing */
    };
#endif
    cudaError_t err = cudaErrorAddressOfConstant;
    
    {
        printf("cudaMemcpyAsync\n");
        int8_t *h_A = (int8_t *)malloc(sizeof(int8_t)*1);
        *h_A=3;
        int8_t *d_A = NULL;
        err = cudaMalloc((void **)&d_A, sizeof(int8_t));
        err = cudaMemcpyAsync(d_A, h_A, sizeof(int8_t), cudaMemcpyHostToDevice,0);
        err = cudaMemcpyAsync(h_A, d_A, sizeof(int8_t), cudaMemcpyDeviceToHost,0);
        printf("h_A=%d\n",*h_A);

    }
    

    {
        printf("cudaPeekAtLastError\n");
        err=cudaPeekAtLastError();
        printf("err=%d\n",err); 
    }
    
    

    cudaMemcpy(da, ha, TEST_DATA_SIZE, cudaMemcpyHostToDevice);
    cudaMemcpy(hb, da, TEST_DATA_SIZE, cudaMemcpyDeviceToHost);

    for (int i = 0; i < 128; i++) {
        printf("%8d ", hb[i]);
        if (i % 16 == 15)
            printf("\n");
    }
    printf("\n");
    cudaFree(da);
    free(ha);
    free(hb);

    printf("Hello Example\n");
    return 0;
}
