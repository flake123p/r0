#include <cublasLt.h>
#include <cuda_runtime.h>
#include <cublas.h>
#include <stdio.h>
#include <stdlib.h>
#include "cublas_v2.h"

int main(){
    {
        printf("cublasGemmEx\n");
        cublasStatus_t err;
        cublasHandle_t handle;
        cublasCreate(&handle);
        cublasOperation_t transa=CUBLAS_OP_N;
        cublasOperation_t transb=CUBLAS_OP_N;
        int m=2,n=2,k=3;
        float alpha=1;
        float beta=0;
        float* h_A=(float*)malloc(sizeof(float)*m*k);
        float* h_B=(float*)malloc(sizeof(float)*k*n);
        float* h_C=(float*)malloc(sizeof(float)*m*n);
        for(int i=0;i<m*k;i++){
                h_A[i]=i;
                //printf("h_A=%f\n",h_A[i]);
            }
        for(int i=0;i<n*k;i++){
                h_B[i]=i;
                //printf("h_B=%f\n",h_B[i]);
            }
        float* d_A=NULL;
        float* d_B=NULL;
        float* d_C=NULL;
        cudaDataType_t type=CUDA_R_32F;
        cublasGemmAlgo_t algo = CUBLAS_GEMM_DEFAULT;
        cublasComputeType_t ctype=CUBLAS_COMPUTE_32F;
        cudaMalloc((void **)&d_A, sizeof(float)*m*k);
        cudaMemcpy(d_A, h_A, sizeof(float)*m*k, cudaMemcpyHostToDevice);
        cudaMalloc((void **)&d_B, sizeof(float)*k*n);
        cudaMemcpy(d_B, h_B, sizeof(float)*k*n, cudaMemcpyHostToDevice);
        cudaMalloc((void **)&d_C, sizeof(float)*m*n);
        cudaMemcpy(d_C, h_C, sizeof(float)*m*n, cudaMemcpyHostToDevice);

        err=cublasGemmEx(handle,transa,transb,m,n,k,&alpha,d_A,type,m,d_B,type,k,&beta,d_C,type,m,ctype,algo);
        printf("err= %d\n",err);
        cudaMemcpy(h_C, d_C, sizeof(float)*m*n, cudaMemcpyDeviceToHost);
        for(int i=0;i<m*n;i++){
                //printf("%f\n",h_C[i]);
            }
        free(h_A);
        free(h_B);
        free(h_C);
        cudaFree(d_A);
        cudaFree(d_B);
        cudaFree(d_C);
    }
    {
        printf("cublasGemmBatchedEx\n");
        cublasStatus_t err;
        cublasHandle_t handle;
        cublasCreate(&handle);
        cublasOperation_t transa=CUBLAS_OP_N;
        cublasOperation_t transb=CUBLAS_OP_N;
        int m=2,n=2,k=3;
        float alpha=1;
        float beta=0;
        cudaDataType_t type=CUDA_R_32F;
        cublasGemmAlgo_t algo = CUBLAS_GEMM_DEFAULT;
        cublasComputeType_t ctype=CUBLAS_COMPUTE_32F;
        int batchCount=2;
        float* h_A[2];
        h_A[0]=(float*)malloc(sizeof(float)*m*k);
        h_A[1]=(float*)malloc(sizeof(float)*m*k);
        float* h_B[2];
        h_B[0]=(float*)malloc(sizeof(float)*n*k);
        h_B[1]=(float*)malloc(sizeof(float)*n*k);
        float* h_C[2];
        h_C[0]=(float*)malloc(sizeof(float)*n*m);
        h_C[1]=(float*)malloc(sizeof(float)*n*m);
        for(int i=0;i<m*k;i++){
                h_A[0][i]=i;
                h_A[1][i]=i;
            }
        for(int i=0;i<n*k;i++){
                h_B[0][i]=i;
                h_B[1][i]=i;
            }

        void* d_A[2];
        void* d_B[2];
        void* d_C[2];
        cudaMalloc((void **)&d_A[0], sizeof(float)*m*k);
        cudaMemcpy(d_A[0], h_A[0], sizeof(float)*m*k, cudaMemcpyHostToDevice);
        cudaMalloc((void **)&d_A[1], sizeof(float)*m*k);
        cudaMemcpy(d_A[1], h_A[1], sizeof(float)*m*k, cudaMemcpyHostToDevice);
        cudaMalloc((void **)&d_B[0], sizeof(float)*k*n);
        cudaMemcpy(d_B[0], h_B[0], sizeof(float)*k*n, cudaMemcpyHostToDevice);
        cudaMalloc((void **)&d_B[1], sizeof(float)*k*n);
        cudaMemcpy(d_B[1], h_B[1], sizeof(float)*k*n, cudaMemcpyHostToDevice);
        cudaMalloc((void **)&d_C[0], sizeof(float)*m*n);
        cudaMemcpy(d_C[0], h_C[0], sizeof(float)*m*n, cudaMemcpyHostToDevice);
        cudaMalloc((void **)&d_C[1], sizeof(float)*m*n);
        cudaMemcpy(d_C[1], h_C[1], sizeof(float)*m*n, cudaMemcpyHostToDevice);
        int batch_size=2;
        float **d_dA, **d_dB, **d_dC;
        cudaMalloc(&d_dA, sizeof(float *)*batch_size);
        cudaMalloc(&d_dB, sizeof(float *)*batch_size);
        cudaMalloc(&d_dC, sizeof(float *)*batch_size);
        cudaMemcpy(d_dA, d_A, sizeof(float*)* batch_size, cudaMemcpyHostToDevice);
        cudaMemcpy(d_dB, d_B, sizeof(float*)* batch_size, cudaMemcpyHostToDevice);
        cudaMemcpy(d_dC, d_C, sizeof(float*)* batch_size, cudaMemcpyHostToDevice);
        
        err=cublasGemmBatchedEx(handle,transa,transb,m,n,k,&alpha,(const void**)d_dA,type,m,(const void**)d_dB,type,k,&beta,(void**)d_dC,type,m,batchCount,ctype,algo);
        printf("err= %d\n",err);
        cudaMemcpy(h_C[0], d_C[0], sizeof(float)*m*n, cudaMemcpyDeviceToHost);
        cudaMemcpy(h_C[1], d_C[1], sizeof(float)*m*n, cudaMemcpyDeviceToHost);
        for(int i=0;i<m*n;i++){
                //printf("%f\n",((float *)h_C[0])[i]);
                //printf("%f\n",((float *)h_C[1])[i]);
            }
        
        cudaFree(d_dA);
        cudaFree(d_dB);
        cudaFree(d_dC);
    }
    return 0;
}