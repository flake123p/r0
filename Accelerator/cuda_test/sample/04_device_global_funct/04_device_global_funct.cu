#include "cuda.h" 
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>

__device__ int run_on_gpu() {
	return 1;
}

__host__ int run_on_cpu() {
	return 1;
}
 
__global__ void run_on_gpu1(int* A, int* B,int* C,int size){
		int j=blockIdx.x*blockDim.x+threadIdx.x;
		C[j]=A[j]*B[j];
		printf("C[%d]=%d\n",j,C[j]);
		
    }

int main() {
	//printf("run_on_cpu_or_gpu CPU: %d\n", run_on_cpu_or_gpu());
	int* h_A=(int*)malloc(sizeof(int)*4);
	int* h_B=(int*)malloc(sizeof(int)*4);
	int* h_C=(int*)malloc(sizeof(int)*4);
	for(int i=0;i<4;i++){
			h_A[i]=i;
			//printf("h_A=%f\n",h_A[i]);
		}
	for(int i=0;i<4;i++){
			h_B[i]=i;
			//printf("h_B=%f\n",h_B[i]);
		}
	int* d_A=NULL;
	int* d_B=NULL;
	int* d_C=NULL;
	cudaMalloc((void **)&d_A, sizeof(int)*4);
	cudaMemcpy(d_A, h_A, sizeof(int)*4, cudaMemcpyHostToDevice);
	cudaMalloc((void **)&d_B, sizeof(int)*4);
	cudaMemcpy(d_B, h_B, sizeof(int)*4, cudaMemcpyHostToDevice);
	cudaMalloc((void **)&d_C, sizeof(int)*4);
	cudaMemcpy(d_C, h_C, sizeof(int)*4, cudaMemcpyHostToDevice);

	printf("run_host=%d\n",run_on_cpu());
	run_on_gpu1<<<2, 2>>>(d_A,d_B,d_C,4);
	cudaDeviceReset();
	printf("will end\n");
	
	return 0;
}
