#include <stdio.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

#include "help.hpp"

#if 1
#define TEST_INFO printf
#else
#define TEST_INFO(...)
#endif

int cudaMalloc_Test()
{
    int ret = 0;
    int *da = NULL;
    cudaError_t rc = cudaMalloc((void **)&da, 10*sizeof(int));

    TEST_INFO("%s()\n", __func__);

    if (rc == cudaSuccess) {
        ret = 0;
        if (da == NULL) {
            ret = 1;
        }
    } else {
        ret = 1;
    }

    if (da != NULL) {
        cudaFree(da);
    }

    return ret;
}


int _00_mem_Test()
{
    int ret = 0;
    ret = cudaMalloc_Test();
    if (ret) {
        return ret;
    }

    return ret;
}

int main() {
    int ret;
    ret = _00_mem_Test();
    printf("ret = %d\n", ret);
}
