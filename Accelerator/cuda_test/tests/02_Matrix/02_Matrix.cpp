#include <cublasLt.h>
#include <cuda_runtime.h>
#include <cublas.h>
#include <stdio.h>
#include <stdlib.h>
#include "cublas_v2.h"

#define check(a) printf("err=%d\n",a); 

int cudaCreate_test(){
    int ret=0;
    printf("cudaCreate\n");
    cublasStatus_t err;
    cublasHandle_t handle;
    err = cublasCreate(&handle);
    if(err!=0){
        ret=1;
    }
    return ret; 
}

int cublasSetStream_test(){
    int ret=0;
    printf("cublasSetStream\n");
    cublasStatus_t err;
    cudaStream_t stream0;
    cudaStreamCreate(&stream0);
    cublasHandle_t handle;
    cublasCreate(&handle);
    err=cublasSetStream(handle, stream0);
    if(err!=0){
        ret=1;
    }
    // This function sets the cuBLAS library stream, which will be used to execute all subsequent calls 
    // to the cuBLAS library functions. If the cuBLAS library stream is not set, all kernels use the default NULL
    // stream.
    return ret; 
}

int cublasSetWorkspace_test(){
    int ret=0;
    printf("cublasSetWorkspace\n");
    cublasStatus_t err;
    cublasHandle_t handle;
    cublasCreate(&handle);
    void *workspace;
    err=cublasSetWorkspace(handle,workspace, 256); 
    if(err!=0) ret=1;
    return ret;
}

int cublasSetMathMode_test(){
    int ret=0;
    printf("cublasSetMathMode\n");
    cublasStatus_t err;
    cublasHandle_t handle;
    cublasCreate(&handle);
    cublasMath_t mode=CUBLAS_DEFAULT_MATH;
    err=cublasSetMathMode(handle,mode); 
    if(err!=0) ret=1;
    return ret;
}

int cublasSgemm_test(){
    int ret=0;
    printf("cublasSgemm\n");
    cublasStatus_t err;
    cublasHandle_t handle;
    cublasCreate(&handle);
    cublasOperation_t transa=CUBLAS_OP_N;
    cublasOperation_t transb=CUBLAS_OP_N;
    int m=2,n=2,k=3;
    float alpha=1;
    float beta=0;
    float* h_A=(float*)malloc(sizeof(float)*m*k);
    float* h_B=(float*)malloc(sizeof(float)*k*n);
    float* h_C=(float*)malloc(sizeof(float)*m*n);
    for(int i=0;i<m*k;i++){
            h_A[i]=i;
            //printf("h_A=%f\n",h_A[i]);
        }
    for(int i=0;i<m*k;i++){
            h_B[i]=i;
            //printf("h_B=%f\n",h_B[i]);
        }
    float* d_A=NULL;
    float* d_B=NULL;
    float* d_C=NULL;
    cudaMalloc((void **)&d_A, sizeof(float)*m*k);
    cudaMemcpy(d_A, h_A, sizeof(float)*m*k, cudaMemcpyHostToDevice);
    cudaMalloc((void **)&d_B, sizeof(float)*k*n);
    cudaMemcpy(d_B, h_B, sizeof(float)*k*n, cudaMemcpyHostToDevice);
    cudaMalloc((void **)&d_C, sizeof(float)*m*n);
    cudaMemcpy(d_C, h_C, sizeof(float)*m*n, cudaMemcpyHostToDevice);

    err=cublasSgemm(handle,transa,transb,m,n,k,&alpha,d_A,m,d_B,k,&beta,d_C,m);
    //printf("err= %d\n",err);
    cudaMemcpy(h_C, d_C, sizeof(float)*m*n, cudaMemcpyDeviceToHost);
    if(err!=0||h_C[0]!=10.0||h_C[1]!=13.0||h_C[2]!=28.0||h_C[3]!=40.0) ret=1;
    free(h_A);
    free(h_B);
    free(h_C);
    cudaFree(d_A);
    cudaFree(d_B);
    cudaFree(d_C);
    return ret;
}

int main() {
    int ret;
    check(cudaCreate_test());
    check(cublasSetStream_test());
    check(cublasSetWorkspace_test());
    check(cublasSetMathMode_test());
    check(cublasSgemm_test());
    //printf("ret = %d\n", ret);
}