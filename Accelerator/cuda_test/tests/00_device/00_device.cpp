#include <stdio.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

#include "help.hpp"

#if 1
#define TEST_INFO printf
#else
#define TEST_INFO(...)
#endif

int cudaGetDeviceCount_Test()
{
    int ret = 0;
    int devCount;
    cudaError_t rc = cudaGetDeviceCount(&devCount);

    TEST_INFO("%s()\n", __func__);
    
    if (rc == cudaSuccess) {
        ret = 0;
        if (devCount != 1) {
            ret = 1;
        }
    } else {
        ret = 1;
    }

    return ret;
}

int cudaGetDevice_Test()
{
    int ret = 0;
    int dev = 999;
    cudaError_t rc = cudaGetDevice(&dev);

    TEST_INFO("%s()\n", __func__);

    if (rc == cudaSuccess && dev == 0) {
        ret = 0;
    } else {
        ret = 1;
    }

    return ret;
}

int cudaGetDeviceProperties_Test()
{
    struct cudaDeviceProp pp;
    int ret = 0;
    cudaError_t rc = cudaGetDeviceProperties(&pp, 0);

    TEST_INFO("%s()\n", __func__);

    if (rc == cudaSuccess) {
        ret = 0;
        if (pp.major != 8) {
            ret = 1;
        } else if (pp.minor != 6) {
            ret = 1;
        }
    } else {
        ret = 1;
    }

    return ret;
}

#define RETURN_CHECK(a) {ret=a();if(ret){return ret;}}

int _00_device_Test()
{
    int ret = 0;

    RETURN_CHECK(cudaGetDeviceCount_Test);
    RETURN_CHECK(cudaGetDevice_Test);
    RETURN_CHECK(cudaGetDeviceProperties_Test);

    return ret;
}

int main() {
    int ret;
    ret = _00_device_Test();
    printf("ret = %d\n", ret);
}
