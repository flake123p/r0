import torch
import torch.nn as nn
import torch.nn.functional as F

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.fc = nn.Linear(3, 1)
        self.fc.weight.data = torch.ones(1, 3)
        
    def forward(self, x):
        return self.fc(x)

myNet = Net()
input = torch.tensor([1.0, 2.0, 3.0])
gpu_dev = torch.device("cuda")

myNet = myNet.to(gpu_dev)
input = input.to(gpu_dev)

print(myNet.fc.weight.data)
print(myNet.fc.bias.data)
ans = myNet(input)
print(ans)

'''
==95864== Profiling result:
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:    8.98%  17.857us         3  5.9520us  4.5440us  6.7200us  void at::native::reduce_kernel<int=512, int=1, at::native::ReduceOp<double, at::native::func_wrapper_t<double, at::native::MaxNanFunctor<double>>, unsigned int, double, int=4>>(double)
                    8.50%  16.898us         3  5.6320us  4.2880us  6.3370us  void at::native::reduce_kernel<int=512, int=1, at::native::ReduceOp<double, at::native::func_wrapper_t<double, at::native::MinNanFunctor<double>>, unsigned int, double, int=4>>(double)
                    8.50%  16.897us         8  2.1120us  1.7280us  3.0080us  _ZN2at6native29vectorized_elementwise_kernelILi4EZNS0_21compare_scalar_kernelIdEEvRNS_18TensorIteratorBaseENS0_50_GLOBAL__N__e50ef81d_17_CompareKernels_cu_8f1b29aa6OpTypeET_EUldE_NS_6detail5ArrayIPcLi2EEEEEviT0_T1_
                    8.34%  16.576us        24     690ns     640ns     960ns  [CUDA memcpy DtoH]
                    6.67%  13.249us         6  2.2080us  1.9200us  2.6560us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<float, float, bool, at::native::_GLOBAL__N__28ce311f_18_CompareEQKernel_cu_d8008c96::CompareEqFunctor<float>>, at::detail::Array<char*, int=3>>(int, float, float)
                    6.13%  12.192us         6  2.0320us  1.6640us  2.5920us  void at::native::vectorized_elementwise_kernel<int=4, at::native::AbsFunctor<float>, at::detail::Array<char*, int=2>>(int, float, at::native::AbsFunctor<float>)
                    6.07%  12.064us         6  2.0100us  1.6640us  2.6240us  void at::native::vectorized_elementwise_kernel<int=4, at::native::AUnaryFunctor<float, float, bool, at::native::_GLOBAL__N__28ce311f_18_CompareEQKernel_cu_d8008c96::CompareEqFunctor<float>>, at::detail::Array<char*, int=2>>(int, float, float)
                    5.70%  11.329us         3  3.7760us  3.1690us  4.1280us  _ZN2at6native24index_elementwise_kernelILi128ELi4EZNS0_16gpu_index_kernelIZNS0_17index_kernel_implINS0_10OpaqueTypeILi4EEEEEvRNS_18TensorIteratorBaseEN3c108ArrayRefIlEESA_EUlPcSB_lE_EEvS7_SA_SA_RKT_EUliE_EEviT1_
                    5.01%  9.9520us         3  3.3170us  2.6240us  3.7440us  void at_cuda_detail::cub::DeviceReduceSingleTileKernel<at_cuda_detail::cub::DeviceReducePolicy<bool, int, int, at_cuda_detail::cub::Sum>::Policy600, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool*, long>, int*, int, at_cuda_detail::cub::Sum, int>(int, int, at_cuda_detail::cub::Sum, at_cuda_detail::cub::DeviceReducePolicy<bool, int, int, at_cuda_detail::cub::Sum>::Policy600, bool)
                    4.96%  9.8580us         3  3.2860us  2.6570us  3.7120us  _ZN2at6native27unrolled_elementwise_kernelIZZZNS0_23direct_copy_kernel_cudaERNS_18TensorIteratorBaseEENKUlvE0_clEvENKUlvE4_clEvEUldE_NS_6detail5ArrayIPcLi2EEE23TrivialOffsetCalculatorILi1EjESC_NS0_6memory12LoadWithCastILi1EEENSD_13StoreWithCastILi1EEEEEviT_T0_T1_T2_T3_T4_
                    4.80%  9.5370us         3  3.1790us  2.5920us  3.5530us  void at_cuda_detail::cub::DeviceSelectSweepKernel<at_cuda_detail::cub::DispatchSelectIf<at_cuda_detail::cub::CountingInputIterator<long, long>, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool*, long>, long*, int*, at_cuda_detail::cub::NullType, at_cuda_detail::cub::NullType, int, bool=0>::PtxSelectIfPolicyT, at_cuda_detail::cub::CountingInputIterator<long, long>, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool*, long>, long*, int*, at_cuda_detail::cub::ScanTileState<int, bool=1>, at_cuda_detail::cub::NullType, at_cuda_detail::cub::NullType, int, bool=0>(long, at_cuda_detail::cub::CountingInputIterator<long, long>, bool, bool, at::native::_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool*, long, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool*, long>, int)
                    4.77%  9.4720us         3  3.1570us  2.4640us  3.5200us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<double, double, double, at::native::binary_internal::DivFunctor<double>>, at::detail::Array<char*, int=3>>(int, double, double)
                    3.51%  6.9770us         3  2.3250us  1.9520us  2.6250us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<bool, bool, bool, at::native::BitwiseAndFunctor<bool>>, at::detail::Array<char*, int=3>>(int, bool, bool)
                    3.32%  6.5930us         3  2.1970us  1.9520us  2.3690us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<bool, bool, bool, at::native::binary_internal::MulFunctor<bool>>, at::detail::Array<char*, int=3>>(int, bool, bool)
                    2.91%  5.7930us         3  1.9310us  1.7920us  2.0170us  _ZN2at6native29vectorized_elementwise_kernelILi4EZZZNS0_16ceil_kernel_cudaERNS_18TensorIteratorBaseEENKUlvE_clEvENKUlvE0_clEvEUlfE_NS_6detail5ArrayIPcLi2EEEEEviT0_T1_
                    2.62%  5.2170us         3  1.7390us  1.6960us  1.7610us  void at_cuda_detail::cub::DeviceCompactInitKernel<at_cuda_detail::cub::ScanTileState<int, bool=1>, int*>(int, int, bool=1)
                    2.58%  5.1200us         1  5.1200us  5.1200us  5.1200us  void gemv2N_kernel<int, int, float, float, float, float, int=128, int=1, int=4, int=4, int=1, bool=0, cublasGemvParams<cublasGemvTensorStridedBatched<float const >, cublasGemvTensorStridedBatched<float const >, cublasGemvTensorStridedBatched<float>, float>>(float const )
                    1.35%  2.6880us         1  2.6880us  2.6880us  2.6880us  void at::native::vectorized_elementwise_kernel<int=2, at::native::BinaryFunctor<float, float, bool, at::native::_GLOBAL__N__28ce311f_18_CompareEQKernel_cu_d8008c96::CompareEqFunctor<float>>, at::detail::Array<char*, int=3>>(int, float, float)
                    1.22%  2.4320us         1  2.4320us  2.4320us  2.4320us  void at::native::vectorized_elementwise_kernel<int=4, at::native::CUDAFunctor_add<float>, at::detail::Array<char*, int=3>>(int, float, at::native::CUDAFunctor_add<float>)
                    1.09%  2.1760us         1  2.1760us  2.1760us  2.1760us  void at::native::unrolled_elementwise_kernel<at::native::BinaryFunctor<float, float, bool, at::native::_GLOBAL__N__28ce311f_18_CompareEQKernel_cu_d8008c96::CompareEqFunctor<float>>, at::detail::Array<char*, int=3>, TrivialOffsetCalculator<int=2, unsigned int>, TrivialOffsetCalculator<int=1, unsigned int>, at::native::memory::LoadWithoutCast, at::native::memory::StoreWithoutCast>(int, float, float, bool, float, at::native::_GLOBAL__N__28ce311f_18_CompareEQKernel_cu_d8008c96::CompareEqFunctor<float>, at::native::BinaryFunctor<float, float, bool, at::native::_GLOBAL__N__28ce311f_18_CompareEQKernel_cu_d8008c96::CompareEqFunctor<float>>)
                    1.03%  2.0480us         3     682ns     544ns     960ns  [CUDA memcpy HtoD]
                    1.00%  1.9840us         1  1.9840us  1.9840us  1.9840us  _ZN2at6native29vectorized_elementwise_kernelILi2EZZZNS0_16ceil_kernel_cudaERNS_18TensorIteratorBaseEENKUlvE_clEvENKUlvE0_clEvEUlfE_NS_6detail5ArrayIPcLi2EEEEEviT0_T1_
                    0.93%  1.8560us         1  1.8560us  1.8560us  1.8560us  _ZN2at6native27unrolled_elementwise_kernelIZZZNS0_16ceil_kernel_cudaERNS_18TensorIteratorBaseEENKUlvE_clEvENKUlvE0_clEvEUlfE_NS_6detail5ArrayIPcLi2EEE23TrivialOffsetCalculatorILi1EjESC_NS0_6memory15LoadWithoutCastENSD_16StoreWithoutCastEEEviT_T0_T1_T2_T3_T4_
      API calls:   75.05%  900.81ms         2  450.40ms  2.6100us  900.81ms  cudaStreamIsCapturing
                   24.79%  297.52ms         2  148.76ms  139.40ms  158.12ms  cudaFree
                    0.04%  462.63us        65  7.1170us  3.7580us  34.054us  cudaLaunchKernel
                    0.03%  373.24us         5  74.647us  3.2320us  127.51us  cudaMalloc
                    0.03%  341.42us        27  12.645us  4.4970us  91.131us  cudaMemcpyAsync
                    0.02%  228.52us       297     769ns     113ns  32.641us  cuDeviceGetAttribute
                    0.02%  184.97us       599     308ns     224ns  4.6630us  cudaGetDevice
                    0.01%  146.66us       756     193ns     119ns  3.1280us  cuGetProcAddress
                    0.00%  56.933us        27  2.1080us  1.0680us  5.7240us  cudaStreamSynchronize
                    0.00%  56.559us         1  56.559us  56.559us  56.559us  cudaGetDeviceProperties
                    0.00%  55.094us         3  18.364us  14.912us  21.896us  cuDeviceGetName
                    0.00%  26.243us       145     180ns     112ns  1.5790us  cudaGetLastError
                    0.00%  11.114us         1  11.114us  11.114us  11.114us  cudaFuncGetAttributes
                    0.00%  9.4730us        18     526ns     368ns  1.8390us  cudaEventCreateWithFlags
                    0.00%  8.7830us        17     516ns     230ns  2.0190us  cudaDeviceGetAttribute
                    0.00%  7.6860us         1  7.6860us  7.6860us  7.6860us  cuDeviceGetPCIBusId
                    0.00%  5.4820us         4  1.3700us     141ns  4.5120us  cudaGetDeviceCount
                    0.00%  4.9450us        18     274ns     116ns  1.4420us  cudaPeekAtLastError
                    0.00%  4.3510us         1  4.3510us  4.3510us  4.3510us  cudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags
                    0.00%  2.7750us         2  1.3870us  1.1940us  1.5810us  cuInit
                    0.00%  1.8390us         5     367ns     166ns  1.1350us  cuDeviceGetCount
                    0.00%  1.5070us         3     502ns     332ns     742ns  cuDeviceTotalMem
                    0.00%  1.2400us         3     413ns     125ns     973ns  cuDevicePrimaryCtxGetState
                    0.00%     883ns         4     220ns     138ns     433ns  cuDeviceGet
                    0.00%     738ns         3     246ns     191ns     289ns    �
                    0.00%     650ns         1     650ns     650ns     650ns  cudaGetSymbolAddress
                    0.00%     575ns         3     191ns     180ns     212ns  cuDeviceGetUuid
                    0.00%     409ns         2     204ns     204ns     205ns  cuDriverGetVersion
'''