import torch
def precompute_freqs_cis(dim: int, end: int, theta: float = 10000.0):
    freqs = 1.0 / (theta ** (torch.arange(0, dim, 2)[: (dim // 2)].float() / dim))
    freqs = freqs.to('cuda')
    t = torch.arange(end, device=freqs.device)
    t = t.to('cuda')
    freqs = torch.outer(t, freqs).float()
    freqs_cos = torch.cos(freqs)
    freqs_sin = torch.sin(freqs)
    return freqs_cos, freqs_sin
# Example usage
dim = 32
end = 100
theta = 10000.0
freqs_cos, freqs_sin = precompute_freqs_cis(dim, end, theta)
print("freqs_cos shape:", freqs_cos.shape)
print("freqs_sin shape:", freqs_sin.shape)
print("freqs_cos values:\n", freqs_cos)
print("freqs_sin values:\n", freqs_sin)

print('\n################## Matrix Outer Product ######################')
# Example tensors
t = torch.tensor([0, 1, 2], dtype=torch.bfloat16).to('cuda')  # Shape (3,)
freqs = torch.tensor([0.1, 0.2, 0.3], dtype=torch.bfloat16).to('cuda')    # Shape (3,)
# Compute the outer product of t and freqs
outer_product = torch.outer(t, freqs)
print("Outer product shape:", outer_product.shape)
print("Outer product values:\n", outer_product)

print('\n################## Matrix Inter Product ######################')
# inner_product = t @ freqs
inner_product = torch.dot(t, freqs)
print("Inner product values:\n", inner_product, inner_product.shape)

print('\n################## Numpy Outer Product ######################')
import numpy as np
a = np.ones((3,3))
b = np.ones((3,2))
c = a @ b
print('Numpy product: \n', c, '\n\nc.shape: \n', c.shape)