''' nn.LayerNorm vs. RMSNorm '''
import torch
import torch.nn as nn

input = torch.tensor([
    [ 2.,  2.,  2.],
    [ 4.,  4.,  4.]])

output  = input.mean(-1, keepdim=True)
output1 = input.mean()
output2 = input.mean(-1)
output3 = input.mean(-1, keepdim=False)

print(input)
print(output)
print(output1)
print(output2)
print(output3)