''' nn.LayerNorm vs. RMSNorm '''
import torch
import torch.nn as nn

class RMSNorm(nn.Module):
    def __init__(self, dim: int, eps: float):
        super().__init__()
        self.eps = eps
        self.weight = nn.Parameter(torch.ones(dim))
    def _norm(self, x):
        return x * torch.rsqrt(x.pow(2).mean(-1, keepdim=True) + self.eps)
    def forward(self, x):
        output = self._norm(x.float()).type_as(x)
        return output * self.weight

# Create an instance of RMSNorm
dim = 16  # Dimension along which normalization is applied
eps = 1e-5  # Small epsilon value
rms_norm = RMSNorm(dim, eps).to('cuda')

# Create an instance of nn.LayerNorm
layer_norm = nn.LayerNorm(dim, eps=1e-5).to('cuda')

# Generate example input tensor
batch_size = 16
seq_length = 32
input_tensor = torch.randn(batch_size, seq_length, dim).to('cuda')

# Apply RMSNorm and nn.LayerNorm to the input tensor
normalized_rms_output = rms_norm(input_tensor)
normalized_layer_output = layer_norm(input_tensor)

# Compare the outputs
print("RMSNorm normalized output:")
print(normalized_rms_output[0, 0])  # Print the first element of the first sequence
print("\nnn.LayerNorm normalized output:")
print(normalized_layer_output[0, 0])  # Print the first element of the first sequence
