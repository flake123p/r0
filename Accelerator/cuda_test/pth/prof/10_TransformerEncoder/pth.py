import torch
import torch.nn as nn

# Define hyperparameters
d_model = 512  # Dimension of model
nhead = 8      # Number of self-attention heads
dim_feedforward = 2048  # Dimension of feedforward network
dropout = 0.1  # Dropout rate

# Create an instance of nn.TransformerEncoderLayer
encoder_layer = nn.TransformerEncoderLayer(d_model, nhead, dim_feedforward, dropout)

# Create a sequence of vectors as input
batch_size = 16
seq_length = 32
input_seq = torch.randn(seq_length, batch_size, d_model)  # Sequence length first for Transformer

# Apply the encoder layer to the input sequence
input_seq = input_seq.to('cuda')
encoder_layer = encoder_layer.to('cuda')
output_seq = encoder_layer(input_seq)

# Print shapes of input and output sequences
print("Input sequence shape:", input_seq.shape)
print("Output sequence shape:", output_seq, output_seq.shape)

# import torch.nn as nn

# # Create an instance of nn.TransformerEncoderLayer
# transformer_layer = nn.TransformerEncoderLayer(d_model=512, nhead=8)

# # Initialize counters
# total_params =      {'MultiheadAttention': 0, 'Linear': 0, 'LayerNorm': 0, 'Dropout': 0}
# total_elements =    {'MultiheadAttention': 0, 'Linear': 0, 'LayerNorm': 0, 'Dropout': 0}
# total_operations =  {'MultiheadAttention': 0, 'Linear': 0, 'LayerNorm': 0, 'Dropout': 0}

# # Estimate total parameters of the model
# estimated_total_params = sum(p.numel() for p in transformer_layer.parameters())
# # print(' ', f"Estimated Total Parameters: {estimated_total_params:,}")

# # Print the name of the layer
# print(type(transformer_layer).__name__, ':', f"{estimated_total_params:>16,}")

# # Iterate through sub-layers and analyze their complexity
# sub_layers = list(transformer_layer.children())
# for i, layer in enumerate(sub_layers, start=1):
#     sub_layer_type = layer.__class__.__name__
    
#     if sub_layer_type in total_params:
#         sub_layer_params = sum(p.numel() for p in layer.parameters())
#         sub_layer_elements = sum(p.numel() for p in layer.parameters())
        
#         # Count operations for each sub-layer type
#         if sub_layer_type == 'MultiheadAttention':
#             sub_layer_operations = sub_layer_elements * 2  # For matrix multiplications
#         elif sub_layer_type == 'Linear':
#             sub_layer_operations = sub_layer_elements * 2  # One for weight, one for bias
#         elif sub_layer_type == 'Dropout':
#             sub_layer_operations = 0  # Dropout doesn't involve operations
#         elif sub_layer_type == 'LayerNorm':
#             sub_layer_operations = 0  # Layer normalization doesn't involve operations
        
#         total_params[sub_layer_type] += sub_layer_params
#         total_elements[sub_layer_type] += sub_layer_elements
#         total_operations[sub_layer_type] += sub_layer_operations

# # Print the summary for each sub-layer type
# for layer_type in total_params.keys():
#     print(' ', f"{layer_type}:")
#     print(' ', f"   Parameters: {total_params[layer_type]:>25,}")
#     print(' ', f"     Elements: {total_elements[layer_type]:>25,}")
#     print(' ', f"   Operations: {total_operations[layer_type]:>25,}")

# # Compare total counted parameters with estimated parameters
# total_counted_params = sum(total_params.values())
# print('\n', f"Total Counted Parameters: {total_counted_params:>15,}")
# print('\n', f"Parameter Estimation Error: {total_counted_params - estimated_total_params:,}")
