==192064== NVPROF is profiling process 192064, command: python3 pth.py
==192064== Warning: Profiling results might be incorrect with current version of nvcc compiler used to compile cuda app. Compile with nvcc compiler 9.0 or later version to get correct profiling results. Ignore this warning if code is already compiled with the recommended nvcc version 
==192064== Profiling application: python3 pth.py
==192064== Profiling result:
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:   38.28%  1.3005ms         4  325.13us  116.81us  473.02us  maxwell_sgemm_64x64_tn
                   36.14%  1.2278ms        13  94.445us     704ns  387.45us  [CUDA memcpy HtoD]
                    5.91%  200.75us       293     685ns     639ns  2.1440us  [CUDA memcpy DtoH]
                    3.52%  119.49us         4  29.873us  12.288us  71.237us  void at::native::_GLOBAL__N__50f1b6c4_10_Dropout_cu_f3381911::fused_dropout_kernel_vec<float, float, unsigned int, int=1, int=4, bool>(at::cuda::detail::TensorInfo<float, unsigned int>, unsigned int, at::cuda::detail<bool, float>, float, float, at::PhiloxCudaState)
                    2.81%  95.432us        42  2.2720us  2.0160us  3.1360us  void at::native::_GLOBAL__N__0f1a8107_8_Shape_cu_49f7391c::CatArrayBatchedCopy<float, unsigned int, int=1, int=128, int=1>(float*, at::native::_GLOBAL__N__0f1a8107_8_Shape_cu_49f7391c::CatArrInputTensorMetadata<at::native::_GLOBAL__N__0f1a8107_8_Shape_cu_49f7391c::CatArrayBatchedCopy<float, unsigned int, int=1, int=128, int=1>, unsigned int, int=128, int=1>, at::native::_GLOBAL__N__0f1a8107_8_Shape_cu_49f7391c::TensorSizeStride<at::native::_GLOBAL__N__0f1a8107_8_Shape_cu_49f7391c::CatArrInputTensorMetadata, unsigned int=4>, int, at::native::_GLOBAL__N__0f1a8107_8_Shape_cu_49f7391c::CatArrInputTensorMetadata)
                    2.16%  73.317us         1  73.317us  73.317us  73.317us  maxwell_sgemm_128x64_tn
                    2.04%  69.156us         2  34.578us  30.018us  39.138us  void at::native::_GLOBAL__N__21739cd4_20_layer_norm_kernel_cu_a73fe0d9::vectorized_layer_norm_kernel<float, float>(int, float, float const *, float const , float const , at::native::_GLOBAL__N__21739cd4_20_layer_norm_kernel_cu_a73fe0d9::vectorized_layer_norm_kernel<float, float>*, float const *, float*)
                    1.83%  62.116us         2  31.058us  16.641us  45.475us  _ZN2at6native18elementwise_kernelILi128ELi2EZNS0_15gpu_kernel_implIZZZNS0_23direct_copy_kernel_cudaERNS_18TensorIteratorBaseEENKUlvE0_clEvENKUlvE5_clEvEUlfE_EEvS4_RKT_EUliE_EEviT1_
                    1.59%  54.147us         1  54.147us  54.147us  54.147us  _ZN2at6native29vectorized_elementwise_kernelILi4EZZZNS0_49_GLOBAL__N__d2ba64fb_16_TensorCompare_cu_d0af11f719launch_clamp_scalarERNS_18TensorIteratorBaseEN3c106ScalarES6_NS0_6detail11ClampLimitsEENKUlvE_clEvENKUlvE5_clEvEUlfE_NS_6detail5ArrayIPcLi2EEEEEviT0_T1_
                    1.47%  49.922us         1  49.922us  49.922us  49.922us  maxwell_sgemm_128x64_nn
                    1.18%  39.938us         2  19.969us  19.905us  20.033us  void at::native::vectorized_elementwise_kernel<int=4, at::native::CUDAFunctor_add<float>, at::detail::Array<char*, int=3>>(int, float, at::native::CUDAFunctor_add<float>)
                    0.78%  26.529us         2  13.264us  13.185us  13.344us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BUnaryFunctor<float, float, float, at::native::binary_internal::MulFunctor<float>>, at::detail::Array<char*, int=2>>(int, float, float)
                    0.32%  10.817us         1  10.817us  10.817us  10.817us  void _GLOBAL__N__b6de9c8c_10_SoftMax_cu_9f978f63::softmax_warp_forward<float, float, float, int=5, bool=0, bool=0>(float*, float const *, int, int, int, bool const *, int, bool)
                    0.20%  6.6560us         3  2.2180us  1.6960us  3.2320us  _ZN2at6native29vectorized_elementwise_kernelILi4EZNS0_21compare_scalar_kernelIdEEvRNS_18TensorIteratorBaseENS0_50_GLOBAL__N__e50ef81d_17_CompareKernels_cu_8f1b29aa6OpTypeET_EUldE_NS_6detail5ArrayIPcLi2EEEEEviT0_T1_
                    0.19%  6.5600us         1  6.5600us  6.5600us  6.5600us  void at::native::reduce_kernel<int=512, int=1, at::native::ReduceOp<double, at::native::func_wrapper_t<double, at::native::MaxNanFunctor<double>>, unsigned int, double, int=4>>(double)
                    0.18%  6.0160us         1  6.0160us  6.0160us  6.0160us  void at::native::reduce_kernel<int=512, int=1, at::native::ReduceOp<double, at::native::func_wrapper_t<double, at::native::MinNanFunctor<double>>, unsigned int, double, int=4>>(double)
                    0.15%  5.1200us         1  5.1200us  5.1200us  5.1200us  _ZN2at6native24index_elementwise_kernelILi128ELi4EZNS0_16gpu_index_kernelIZNS0_17index_kernel_implINS0_10OpaqueTypeILi4EEEEEvRNS_18TensorIteratorBaseEN3c108ArrayRefIlEESA_EUlPcSB_lE_EEvS7_SA_SA_RKT_EUliE_EEviT1_
                    0.14%  4.8320us         1  4.8320us  4.8320us  4.8320us  _ZN2at6native27unrolled_elementwise_kernelIZZZNS0_23direct_copy_kernel_cudaERNS_18TensorIteratorBaseEENKUlvE0_clEvENKUlvE4_clEvEUldE_NS_6detail5ArrayIPcLi2EEE23TrivialOffsetCalculatorILi1EjESC_NS0_6memory12LoadWithCastILi1EEENSD_13StoreWithCastILi1EEEEEviT_T0_T1_T2_T3_T4_
                    0.14%  4.7690us         2  2.3840us  2.0480us  2.7210us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<float, float, bool, at::native::_GLOBAL__N__28ce311f_18_CompareEQKernel_cu_d8008c96::CompareEqFunctor<float>>, at::detail::Array<char*, int=3>>(int, float, float)
                    0.13%  4.5760us         2  2.2880us  1.7600us  2.8160us  void at::native::vectorized_elementwise_kernel<int=4, at::native::AUnaryFunctor<float, float, bool, at::native::_GLOBAL__N__28ce311f_18_CompareEQKernel_cu_d8008c96::CompareEqFunctor<float>>, at::detail::Array<char*, int=2>>(int, float, float)
                    0.12%  4.1600us         1  4.1600us  4.1600us  4.1600us  void at_cuda_detail::cub::DeviceSelectSweepKernel<at_cuda_detail::cub::DispatchSelectIf<at_cuda_detail::cub::CountingInputIterator<long, long>, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool*, long>, long*, int*, at_cuda_detail::cub::NullType, at_cuda_detail::cub::NullType, int, bool=0>::PtxSelectIfPolicyT, at_cuda_detail::cub::CountingInputIterator<long, long>, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool*, long>, long*, int*, at_cuda_detail::cub::ScanTileState<int, bool=1>, at_cuda_detail::cub::NullType, at_cuda_detail::cub::NullType, int, bool=0>(long, at_cuda_detail::cub::CountingInputIterator<long, long>, bool, bool, at::native::_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool*, long, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool*, long>, int)
                    0.12%  3.9360us         2  1.9680us  1.8560us  2.0800us  void at::native::vectorized_elementwise_kernel<int=4, at::native::AbsFunctor<float>, at::detail::Array<char*, int=2>>(int, float, at::native::AbsFunctor<float>)
                    0.11%  3.7450us         1  3.7450us  3.7450us  3.7450us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<double, double, double, at::native::binary_internal::DivFunctor<double>>, at::detail::Array<char*, int=3>>(int, double, double)
                    0.10%  3.4560us         1  3.4560us  3.4560us  3.4560us  void at::native::_GLOBAL__N__0f1a8107_8_Shape_cu_49f7391c::CatArrayBatchedCopy<float, unsigned int, int=2, int=128, int=1>(float*, at::native::_GLOBAL__N__0f1a8107_8_Shape_cu_49f7391c::CatArrInputTensorMetadata<at::native::_GLOBAL__N__0f1a8107_8_Shape_cu_49f7391c::CatArrayBatchedCopy<float, unsigned int, int=2, int=128, int=1>, unsigned int, int=128, int=1>, at::native::_GLOBAL__N__0f1a8107_8_Shape_cu_49f7391c::TensorSizeStride<at::native::_GLOBAL__N__0f1a8107_8_Shape_cu_49f7391c::CatArrInputTensorMetadata, unsigned int=4>, int, at::native::_GLOBAL__N__0f1a8107_8_Shape_cu_49f7391c::CatArrInputTensorMetadata)
                    0.10%  3.3600us         1  3.3600us  3.3600us  3.3600us  void at_cuda_detail::cub::DeviceReduceSingleTileKernel<at_cuda_detail::cub::DeviceReducePolicy<bool, int, int, at_cuda_detail::cub::Sum>::Policy600, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool*, long>, int*, int, at_cuda_detail::cub::Sum, int>(int, int, at_cuda_detail::cub::Sum, at_cuda_detail::cub::DeviceReducePolicy<bool, int, int, at_cuda_detail::cub::Sum>::Policy600, bool)
                    0.09%  3.0080us         1  3.0080us  3.0080us  3.0080us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<bool, bool, bool, at::native::binary_internal::MulFunctor<bool>>, at::detail::Array<char*, int=3>>(int, bool, bool)
                    0.08%  2.7200us         1  2.7200us  2.7200us  2.7200us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<bool, bool, bool, at::native::BitwiseAndFunctor<bool>>, at::detail::Array<char*, int=3>>(int, bool, bool)
                    0.07%  2.2720us         1  2.2720us  2.2720us  2.2720us  _ZN2at6native29vectorized_elementwise_kernelILi4EZZZNS0_16ceil_kernel_cudaERNS_18TensorIteratorBaseEENKUlvE_clEvENKUlvE0_clEvEUlfE_NS_6detail5ArrayIPcLi2EEEEEviT0_T1_
                    0.05%  1.7600us         1  1.7600us  1.7600us  1.7600us  void at_cuda_detail::cub::DeviceCompactInitKernel<at_cuda_detail::cub::ScanTileState<int, bool=1>, int*>(int, int, bool=1)
                    0.02%     800ns         1     800ns     800ns     800ns  [CUDA memset]
      API calls:   75.23%  911.64ms        12  75.970ms     714ns  911.63ms  cudaStreamIsCapturing
                   24.18%  292.96ms         2  146.48ms  131.68ms  161.28ms  cudaFree
                    0.30%  3.5991ms       306  11.761us  3.8090us  394.44us  cudaMemcpyAsync
                    0.07%  834.82us       306  2.7280us  1.0710us  85.227us  cudaStreamSynchronize
                    0.07%  809.77us        11  73.615us  4.1630us  136.77us  cudaMalloc
                    0.05%  660.14us      2185     302ns     221ns  5.6930us  cudaGetDevice
                    0.05%  622.88us        83  7.5040us  4.1280us  42.015us  cudaLaunchKernel
                    0.02%  241.17us       297     812ns     112ns  48.975us  cuDeviceGetAttribute
                    0.01%  146.27us       756     193ns     121ns  2.8240us  cuGetProcAddress
                    0.00%  58.441us         1  58.441us  58.441us  58.441us  cudaGetDeviceProperties
                    0.00%  56.481us         3  18.827us  15.070us  25.880us  cuDeviceGetName
                    0.00%  23.033us        28     822ns     317ns  4.6010us  cudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags
                    0.00%  22.143us       121     183ns     115ns     653ns  cudaGetLastError
                    0.00%  14.932us        18     829ns     356ns  7.6070us  cudaEventCreateWithFlags
                    0.00%  8.8180us         1  8.8180us  8.8180us  8.8180us  cudaFuncGetAttributes
                    0.00%  7.9330us         1  7.9330us  7.9330us  7.9330us  cudaMemsetAsync
                    0.00%  7.5350us         1  7.5350us  7.5350us  7.5350us  cuDeviceGetPCIBusId
                    0.00%  5.7770us        15     385ns     236ns  1.2760us  cudaDeviceGetAttribute
                    0.00%  5.0820us         4  1.2700us     122ns  4.3890us  cudaGetDeviceCount
                    0.00%  2.7360us         7     390ns     136ns  1.0440us  cuDevicePrimaryCtxGetState
                    0.00%  2.4820us         2  1.2410us  1.1180us  1.3640us  cuInit
                    0.00%  1.6010us         5     320ns     160ns     880ns  cuDeviceGetCount
                    0.00%  1.0040us         1  1.0040us  1.0040us  1.0040us  cudaGetSymbolAddress
                    0.00%     967ns         6     161ns     116ns     254ns  cudaPeekAtLastError
                    0.00%     953ns         3     317ns     297ns     332ns  cuDeviceTotalMem
                    0.00%     794ns         4     198ns     110ns     408ns  cuDeviceGet
                    0.00%     712ns         3     237ns     188ns     266ns    �
                    0.00%     535ns         3     178ns     166ns     186ns  cuDeviceGetUuid
                    0.00%     332ns         2     166ns     141ns     191ns  cuDriverGetVersion
