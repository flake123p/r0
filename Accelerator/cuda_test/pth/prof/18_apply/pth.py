''' apply the custom_init to submodules '''
import torch
import torch.nn as nn
class MyModule(nn.Module):
    def __init__(self):
        super(MyModule, self).__init__()
        self.linear1 = nn.Linear(3,4)
        self.linear2 = nn.Linear(3, 3)
    def forward(self, x):
        x = self.linear1(x)
        x = self.linear2(x)
        return x
    def custom_init(self, module):
        if isinstance(module, nn.Linear):
            module.weight.data.fill_(1.0)
            if module.bias is not None:
                module.bias.data.fill_(0.0)

# Create an instance of MyModule
model = MyModule().to('cuda')
# Print the model's initial weights
print("Initial weights: \n")
print(model.state_dict())
# Apply the custom_init function to all linear submodules
model.apply(model.custom_init)
# Print the model's weights after applying custom_init
print("\nWeights after applying custom_init:")
print(model.state_dict())