import torch
import torch.nn as nn

class TensorAddLayer(nn.Module):
    def __init__(self):
        super(TensorAddLayer, self).__init__()
    def forward(self, x1, x2):
        return x1 + x2

gpu_dev = torch.device("cuda")

# Create an instance of the tensor add layer
tensor_add_layer = TensorAddLayer()
# Generate example input tensors
input1 = torch.randn(8, 64).to(gpu_dev)
input2 = torch.randn(8, 64).to(gpu_dev)
# Get the output from the tensor add layer
output = tensor_add_layer(input1, input2)
# Print input shapes
print("Input1 shape:", input1.shape)  # (8, 64)
print("Input2 shape:", input2.shape)  # (8, 64)
# Print output shape
print("Output shape:", output.shape)  # (8, 64)
