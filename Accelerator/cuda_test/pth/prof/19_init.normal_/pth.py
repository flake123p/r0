import torch
import torch.nn.init as init
import math

# Define the tensor dimensions
n_layers = 4
input_size = 128
output_size = 64

# Create a linear layer
linear_layer = torch.nn.Linear(input_size, output_size).to('cuda')

# Initialize the weights using normal distribution
std_dev = 0.02 / math.sqrt(2 * n_layers)
init.normal_(linear_layer.weight, mean=0.0, std=std_dev)

# Print the initialized weights
print("Initialized weights:")
print(linear_layer.weight)