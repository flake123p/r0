"""
tril: generate tri-low array
masked_fill: fill '-inf' if element == 0
"""
import torch
import torch.nn as nn
import torch.nn.functional as F

a = torch.tril(torch.ones(3, 3))
print('[Debug].....a.tril: \n', a)
masked = a.masked_fill(a == 0, float('-inf'))
print('[Debug].....masked:  \n', masked)
a = a / torch.sum(a, 1, keepdim=True)
b = torch.randint(0,10,(3,2)).float()
c = a @ b
print('a=')
print(a, a.shape)
# ---------------------------------------------------------
# Define hyperparameters
block_size = 32
# Example input tensor
input_tensor = torch.randn(block_size, block_size).to('cuda')  # Shape: (32, 32)
class CausalMaskLayer(nn.Module):
    def __init__(self, block_size):
        super(CausalMaskLayer, self).__init__()
        self.register_buffer('tril', torch.tril(torch.ones(block_size, block_size)))
    def forward(self, x):
        mask = self.tril.to(x.device)
        masked_x = x.masked_fill(mask == 0, float('-inf'))
        return masked_x
# Instantiate the CausalMaskLayer module
causal_mask_layer = CausalMaskLayer(block_size).to('cuda')
# Get the output from the causal mask layer
output = causal_mask_layer(input_tensor)
# Print input shape
print("Input shape:", input_tensor.shape)  # (32, 32)
# Print output shape
print("Output shape:", output.shape)  # (32, 32)

print('\n################## casual mask with softmax #####################')
# Create an example tensor
a = torch.tensor([[1, 2, 3],[4, 5, 6],[7, 8,9]]).to(torch.bfloat16)
# Create a causal mask (lower triangular) with the same shape
mask = torch.tril(torch.ones(a.size(0), a.size(1)))
# Apply causal mask
masked_tensor = a.masked_fill(mask == 0, float('-inf'))
# Apply softmax along the specified dimension
softmaxed_tensor = F.softmax(masked_tensor, dim=-1)
print("Original Tensor:")
print(a)
print("\nCausal Mask:")
print(mask)
print("\nMasked Tensor:")
print(masked_tensor)
print("\nSoftmaxed Tensor with Causal Mask:")
print(softmaxed_tensor)