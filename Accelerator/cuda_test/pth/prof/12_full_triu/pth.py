import torch

print('################# torch.full ########################')
# Create a tensor of shape (2, 3) filled with the value 5.0
tensor = torch.full((6,6), 5.0).to('cuda')
print(tensor)

print('\n################# triu ########################')
mask = torch.triu(tensor, diagonal=1)
print(mask)