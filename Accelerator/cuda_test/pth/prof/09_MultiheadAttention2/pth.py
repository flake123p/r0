import torch
import torch.nn as nn

# Define hyperparameters
n_embd = 64
num_heads = 2
dropout = 0.1

# Example input tensor
input_tensor = torch.randn(8, 32, n_embd)  # Batch size of 8, block size of 32, embedding dimension of 64

class CustomMultiHeadAttention(nn.Module):
    def __init__(self, n_embd, num_heads, dropout):
        super(CustomMultiHeadAttention, self).__init__()
        self.multihead_attention = nn.MultiheadAttention(n_embd, num_heads, dropout=dropout)

    def forward(self, x):
        # Permute input tensor to (T, B, C) as required by nn.MultiheadAttention
        x = x.permute(1, 0, 2)
        output, _ = self.multihead_attention(x, x, x)  # Query, Key, Value are all same
        # Permute output tensor back to (B, T, C)
        output = output.permute(1, 0, 2)
        return output

# Instantiate the CustomMultiHeadAttention module
multihead_attention = CustomMultiHeadAttention(n_embd, num_heads, dropout)

# Get the output from the custom multi-head attention
input_tensor = input_tensor.to('cuda')
multihead_attention = multihead_attention.to('cuda')
output = multihead_attention(input_tensor)

# Print input shape
print("Input shape:", input_tensor.shape)  # (8, 32, 64)

# Print output shape
print("Output shape:", output.shape)  # (8, 32, 64)