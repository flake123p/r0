import torch
import torch.nn as nn
# Define hyperparameters and other necessary variables
batch_size = 64
block_size = 32
vocab_size = 1000  # Example value
n_embd = 128
# Create a BigramLanguageModel class (simplified for illustration)
class BigramLanguageModel(nn.Module):
    def __init__(self, vocab_size, n_embd):
        super().__init__()
        self.token_embedding_table = nn.Embedding(vocab_size, n_embd).to('cuda')
        self.position_embedding_table = nn.Embedding(block_size, n_embd).to('cuda')
    def forward(self, idx):
        B, T = idx.shape
        print('[Debug].....B, T: ', B, T)           
        # idx is a tensor of shape (B, T) containing integer indices
        tok_emb = self.token_embedding_table(idx).to('cuda')  # Shape: (B, T, n_embd)
        print('[Debug].....tok_emb: ', tok_emb.shape)        
        # Generate position embeddings using torch.arange
        pos_emb = self.position_embedding_table(torch.arange(T, device='cuda'))  # Shape: (T, n_embd)
        print('[Debug].....pos_emb: ', pos_emb.shape)
        # Broadcasting: Add position embeddings to token embeddings
        x = tok_emb + pos_emb.unsqueeze(0)  # Shape: (B, T, n_embd)
        print('[Debug].....pos_emb.un: ', pos_emb.unsqueeze(0).shape)              
        return x
# Instantiate the model
model = BigramLanguageModel(vocab_size, n_embd)
model = model.to('cuda')
# Generate example input tensor
example_input = torch.randint(0, vocab_size, (batch_size, block_size), dtype=torch.long).to('cuda')
# Get the output from the model
output = model(example_input)
# Print input shape
print("Input shape:", example_input.shape)  # (batch_size, block_size)
# Print output shape
print("Output shape:", output.shape)  # (batch_size, block_size, n_embd)