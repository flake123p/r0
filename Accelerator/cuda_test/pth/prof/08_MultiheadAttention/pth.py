import torch
import torch.nn as nn
import torch.nn.functional as F

# Define hyperparameters
n_embd = 64
block_size = 32
dropout = 0.1

# Example input tensor
input_tensor = torch.randn(8, block_size, n_embd)  # Batch size of 8, block size of 32, embedding dimension of 64

class Head(nn.Module):
    """ one head of self-attention """

    def __init__(self, head_size):
        super().__init__()
        self.key = nn.Linear(n_embd, head_size, bias=False)
        self.query = nn.Linear(n_embd, head_size, bias=False)
        self.value = nn.Linear(n_embd, head_size, bias=False)
        self.register_buffer('tril', torch.tril(torch.ones(block_size, block_size)))

        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        B,T,C = x.shape
        k = self.key(x)   # (B,T,C)
        q = self.query(x) # (B,T,C)
        # compute attention scores ("affinities")
        wei = q @ k.transpose(-2,-1) * C**-0.5 # (B, T, C) @ (B, C, T) -> (B, T, T)
        wei = wei.masked_fill(self.tril[:T, :T] == 0, float('-inf')) # (B, T, T) ------------> Masked
        wei = F.softmax(wei, dim=-1) # (B, T, T)
        wei = self.dropout(wei)
        # perform the weighted aggregation of the values
        v = self.value(x) # (B,T,C)
        out = wei @ v # (B, T, T) @ (B, T, C) -> (B, T, C)
        return out

class MultiHeadAttention(nn.Module):
    """ multiple heads of self-attention in parallel """

    def __init__(self, num_heads, head_size):
        super().__init__()
        self.heads = nn.ModuleList([Head(head_size) for _ in range(num_heads)])
        self.proj = nn.Linear(n_embd, n_embd)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        out = torch.cat([h(x) for h in self.heads], dim=-1)
        out = self.dropout(self.proj(out))
        return out

# Instantiate the MultiHeadAttention module
num_heads = 2
head_size = 32
multihead_attention = MultiHeadAttention(num_heads, head_size)

# Get the output from the multi-head attention
input_tensor = input_tensor.to('cuda')
multihead_attention = multihead_attention.to('cuda')
output = multihead_attention(input_tensor)

# Print input shape
print("Input shape:", input_tensor.shape)  # (8, 32, 64)

# Print output shape
print("Output shape:", output.shape)  # (8, 32, 64)