''' normalized_x = (x - mean) / (std + ε) '''
import torch
import torch.nn as nn
import numpy as np

# Constant input data
input_data = np.array([[1.0, 2.0, 3.0, 4.0, 5.0]], dtype=np.float32)
input_data = np.random.rand(1, 5).astype(np.float32)
'''------------------ nn.LayerNorm '''
# PyTorch implementation using nn.LayerNorm
layer_norm_torch = nn.LayerNorm(input_data.shape[1], eps=1e-5).to('cuda')
input_torch = torch.tensor(input_data).to('cuda')
output_torch = layer_norm_torch(input_torch).cpu().detach().numpy()
'''----------------- numpy
        normalized_x = (x - mean) / (std + ε)
        output_numpy = normalized_data * gamma + beta '''
# Numpy implementation of LayerNorm
eps = 1e-5                                             # ε
mean = np.mean(input_data, axis=1, keepdims=True)      # std
std = np.std(input_data, axis=1, keepdims=True)
normalized_data = (input_data - mean) / (std + eps)    
gamma = layer_norm_torch.cpu().weight.detach().numpy()       # gamma
beta = layer_norm_torch.cpu().bias.detach().numpy()          # beta 
output_numpy = normalized_data * gamma + beta
# Compare results
print("PyTorch Output:")
print(output_torch)
print("Numpy Output:")
print(output_numpy)
# Check if results are similar
if np.allclose(output_torch, output_numpy, rtol=1e-5, atol=1e-5):
    print("Results are similar!")
else:
    print("Results are not similar.")