import torch
import torch.nn.functional as F

# Create an example tensor
x = torch.tensor([-1.0, 0.0, 1.0, 2.0]).to('cuda')

# Apply the SiLU (Sigmoid-weighted Linear Unit) activation function
output = F.silu(x)

print("Input Tensor:")
print(x)
print("\nOutput Tensor after SiLU Activation:")
print(output)