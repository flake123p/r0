from typing import Any, Optional, Tuple
import numpy as np
import torch
import torch.nn.functional as F
from torch import nn
def precompute_freqs_cis(dim: int, end: int, theta: float = 10000.0):
    freqs = 1.0 / (theta ** (torch.arange(0, dim, 2)[: (dim // 2)].float() / dim))
    freqs = freqs.to('cuda')
    t = torch.arange(end, device=freqs.device)
    t = t.to('cuda')
    freqs = torch.outer(t, freqs).float()
    freqs_cos = torch.cos(freqs)
    freqs_sin = torch.sin(freqs)
    return freqs_cos, freqs_sin

def reshape_for_broadcast(freqs_cis: torch.Tensor, x: torch.Tensor):
    freqs_cis = freqs_cis.to('cuda')
    x = x.to('cuda')
    ndim = x.ndim
    assert 0 <= 1 < ndim
    print('shape: ', freqs_cis.shape, x.shape[1], x.shape[-1])
    assert freqs_cis.shape == (x.shape[1], x.shape[-1])
    shape = [d if i == 1 or i == ndim - 1 else 1 for i, d in enumerate(x.shape)]
    return freqs_cis.view(shape)

def apply_rotary_emb(
    xq: torch.Tensor,
    xk: torch.Tensor,
    freqs_cos: torch.Tensor,
    freqs_sin: torch.Tensor
) -> Tuple[torch.Tensor, torch.Tensor]:
    xq = xq.to('cuda')
    xk = xk.to('cuda')
    freqs_cos = freqs_cos.to('cuda')
    freqs_sin = freqs_sin.to('cuda')
    # reshape xq and xk to match the complex representation
    xq_r, xq_i = xq.float().reshape(xq.shape[:-1] + (-1, 2)).unbind(-1)
    xk_r, xk_i = xk.float().reshape(xk.shape[:-1] + (-1, 2)).unbind(-1)

    # reshape freqs_cos and freqs_sin for broadcasting
    freqs_cos = reshape_for_broadcast(freqs_cos, xq_r)
    freqs_sin = reshape_for_broadcast(freqs_sin, xq_r)

    # apply rotation using real numbers
    xq_out_r = xq_r * freqs_cos - xq_i * freqs_sin
    xq_out_i = xq_r * freqs_sin + xq_i * freqs_cos
    xk_out_r = xk_r * freqs_cos - xk_i * freqs_sin
    xk_out_i = xk_r * freqs_sin + xk_i * freqs_cos

    # flatten last two dimensions
    xq_out = torch.stack([xq_out_r, xq_out_i], dim=-1).flatten(3)
    xk_out = torch.stack([xk_out_r, xk_out_i], dim=-1).flatten(3)

    return xq_out.type_as(xq), xk_out.type_as(xk)


dim = 128
end = 32
freqs_cos, freqs_sin = precompute_freqs_cis(dim, end)
print("freqs_cos shape:", freqs_cos.shape)  # (32, 64)
print("freqs_sin shape:", freqs_sin.shape)  # (32, 64)


x = torch.randn(8, 32, 64)
reshaped_freqs_cis = reshape_for_broadcast(freqs_cos, x)
print("Original freqs_cos shape:", freqs_cos.shape)  # (32, 64)
print("Reshaped freqs_cos shape:", reshaped_freqs_cis.shape)  # (8, 32, 64)


xq = torch.randn(8, 32, 128)
xk = torch.randn(8, 32, 128)
xq_out, xk_out = apply_rotary_emb(xq, xk, freqs_cos, freqs_sin)
print("xq_out shape:", xq_out.shape)  # (8, 32, 128)
print("xk_out shape:", xk_out.shape)  # (8, 32, 128)
