import torch
import torch.nn as nn

class MyModule(nn.Module):
    def __init__(self):
        super(MyModule, self).__init__()

        # Create a tensor and register it as a buffer
        buffer_data = torch.tensor([1, 2, 3])
        self.register_buffer("my_buffer", buffer_data)

# Create an instance of the module
my_module = MyModule()

# Move the module to a specific device (e.g., GPU)
device = torch.device("cuda")
my_module.to(device)

# Access the registered buffer
print(my_module.my_buffer)