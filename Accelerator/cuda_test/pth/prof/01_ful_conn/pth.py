import torch
import torch.nn as nn
class FullyConnectedLayer(nn.Module):
    def __init__(self, input_dim, output_dim):
        super(FullyConnectedLayer, self).__init__()
        gpu_dev = torch.device("cuda")
        self.linear = nn.Linear(input_dim, output_dim).to(gpu_dev)
    def forward(self, x):
        return self.linear(x)

gpu_dev = torch.device("cuda")

# Create an instance of the fully connected layer
input_dim = 64
output_dim = 128
fc_layer = FullyConnectedLayer(input_dim, output_dim)
# Generate example input tensor
example_input = torch.randn(8, input_dim).to(gpu_dev)
# Get the output from the fully connected layer
output = fc_layer(example_input)
# Print input shape
print("Input shape:", example_input.shape)  # (8, 64)
# Print output shape
print("Output shape:", output.shape)  # (8, 128)