import torch
import torch.nn.functional as F
import math

# Example data
batch_size = 4
num_classes = 10

# Simulated predicted logits from a model
predicted_logits = torch.randn(batch_size, num_classes).to('cuda')

# Simulated true labels
true_labels = torch.randint(0, num_classes, (batch_size,)).to('cuda')

# Calculate cross-entropy loss
loss = F.cross_entropy(predicted_logits, true_labels)

print("Predicted logits:")
print(predicted_logits)
print("True labels:")
print(true_labels)
print("\nCalculated loss(F.cross_entropy):      ", loss.item())
# Calculate cross-entropy loss for each example in the batch
total_loss = 0
for i in range(batch_size):
    predicted_probs = torch.softmax(predicted_logits[i], dim=0)
    true_label = true_labels[i]
    
    # Get the predicted probability for the true class
    p = predicted_probs[true_label]
    
    # Calculate the cross-entropy loss for this example
    loss = -math.log(p)
    
    # Accumulate the loss
    total_loss += loss

# Calculate average loss over the batch
average_loss = total_loss / batch_size

print("Average Cross-entropy loss(basic math):", average_loss)
print('\n############### cross_entropy in math ################')
# True label
y = 1
# Predicted probability
p = 0.8
# Cross-entropy loss formula
loss = - (y * math.log(p) + (1 - y) * math.log(1 - p))
print("Cross-entropy loss:", loss)