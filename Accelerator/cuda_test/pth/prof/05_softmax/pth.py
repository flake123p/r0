import torch
import torch.nn.functional as F
# Using F.softmax
def softmax_with_torch(x):
    return F.softmax(x, dim=-1)
# Example input tensor
input_tensor = torch.tensor([2.0, 1.0, 0.1]).to('cuda')
output = softmax_with_torch(input_tensor)
print('[Debug].....: F.softmax:                ', output)
# Using basic math to implement softmax
def softmax_basic(x):
    e_x = torch.exp(x - torch.max(x))
    return e_x / e_x.sum()
# Example input tensor
# input_tensor = torch.tensor([2.0, 1.0, 0.1])
output = softmax_basic(input_tensor)
print('[Debug].....: softmax(basic math):      ', output)