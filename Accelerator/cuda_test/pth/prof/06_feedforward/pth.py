import torch
import torch.nn as nn

# Define a simple feedforward layer
class FeedForward(nn.Module):
    def __init__(self, input_dim, hidden_dim, output_dim):
        super(FeedForward, self).__init__()
        self.fc = nn.Linear(input_dim, hidden_dim)
        self.activation = nn.ReLU()
        self.output_layer = nn.Linear(hidden_dim, output_dim)
    
    def forward(self, x):
        x = self.fc(x)               # linear
        x = self.activation(x)       # ReLU
        x = self.output_layer(x)     # linear
        return x

# Example usage
input_dim = 10
hidden_dim = 20
output_dim = 5

model = FeedForward(input_dim, hidden_dim, output_dim).to('cuda')
input_data = torch.randn(32, input_dim).to('cuda')  # Batch size: 32
output = model(input_data)
print(output.shape)  # Shape should be (32, output_dim)