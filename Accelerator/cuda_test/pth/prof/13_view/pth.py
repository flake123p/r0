import torch

# Create a tensor of shape (2, 3)
original_tensor = torch.tensor([[1, 2, 3], [4, 5, 6]]).to('cuda')

# Reshape the tensor into shape (3, 2)
new_tensor = original_tensor.view(3, 2)

print("Original tensor:\n", original_tensor, original_tensor.shape)
print("New tensor:\n", new_tensor, new_tensor.shape)