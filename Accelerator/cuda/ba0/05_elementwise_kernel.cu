#include <stdio.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include <stdint.h>

#define GPU_LAMBDA __device__ __host__

#if __CUDA_ARCH__ == 750
constexpr uint32_t CUDA_MAX_THREADS_PER_SM = 1024;
#elif __CUDA_ARCH__ == 860 || __CUDA_ARCH__ == 870 || __CUDA_ARCH__ == 890
constexpr uint32_t CUDA_MAX_THREADS_PER_SM = 1536;
#else
constexpr uint32_t CUDA_MAX_THREADS_PER_SM = 2048;
#endif
constexpr uint32_t CUDA_MAX_THREADS_PER_BLOCK = 1024;
constexpr uint32_t CUDA_THREADS_PER_BLOCK_FALLBACK = 256;

/*
    max_threads_per_block : e.g.:128. Must =< 1024, if bigger than 1024 (CUDA_MAX_THREADS_PER_BLOCK), fallback to 256 (CUDA_THREADS_PER_BLOCK_FALLBACK)
    min_blocks_per_sm     : e.g.:4.   128x4=512, if smaller than 2048 (CUDA_MAX_THREADS_PER_SM), it's 4. Else it's ceil(2048/threads_per_block) = 16
*/
#define C10_LAUNCH_BOUNDS_2(max_threads_per_block, min_blocks_per_sm) \
  __launch_bounds__(                                                  \
      (C10_MAX_THREADS_PER_BLOCK((max_threads_per_block))),           \
      (C10_MIN_BLOCKS_PER_SM((max_threads_per_block), (min_blocks_per_sm))))

#define C10_MAX_THREADS_PER_BLOCK(val)           \
  (((val) <= CUDA_MAX_THREADS_PER_BLOCK) ? (val) \
                                         : CUDA_THREADS_PER_BLOCK_FALLBACK)
#define C10_MIN_BLOCKS_PER_SM(threads_per_block, blocks_per_sm)        \
  ((((threads_per_block) * (blocks_per_sm) <= CUDA_MAX_THREADS_PER_SM) \
        ? (blocks_per_sm)                                              \
        : ((CUDA_MAX_THREADS_PER_SM + (threads_per_block)-1) /         \
           (threads_per_block))))

template<int nt, int vt, typename func_t>
C10_LAUNCH_BOUNDS_2(nt, 4) // (128, 4)
__global__ void elementwise_kernel(int N, func_t f) {
  int tid = threadIdx.x;
  int nv = nt * vt; //128x2=256
  int idx = nv * blockIdx.x + tid;
  printf("%d, %d\n", tid, idx);
  #pragma unroll
  for (int i = 0; i < vt; i++) {
    if (idx < N) {
      f(idx);
      idx += nt;
    }
  }
}

int main() 
{
    elementwise_kernel<128,2><<<1, 128>>>(10, [=]GPU_LAMBDA(int idx) {
        printf("%d\n", idx);
        });
    cudaDeviceSynchronize();

    return 0;
}