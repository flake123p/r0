#include <stdio.h>

__global__
void square(int *a) {
    *a = *a * *a;
}

int main() 
{
    int hostA[1];

    int *devA;
    cudaMalloc((void **)&devA, sizeof(int));


    hostA[0] = 3;

    //
    // Copy input data to array on GPU.
    //
    cudaMemcpy(devA, hostA, sizeof(int), cudaMemcpyHostToDevice);

    //
    // Launch GPU code with N threads, one per
    // array element.
    //
    square<<<1, 1>>>(devA);

    //
    // Copy output array from GPU back to CPU.
    //
    cudaMemcpy(hostA, devA, sizeof(int), cudaMemcpyDeviceToHost);

    for (int i = 0; i<1; ++i) {
        printf("%d\n", hostA[i]);
    }

    //
    // Free up the arrays on the GPU.
    //
    cudaFree(devA);

    return 0;
}