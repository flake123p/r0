#include <stdio.h>

/*
    https://stackoverflow.com/questions/12639552/does-cpu-waits-for-device-to-let-it-finish-its-kernel-execution

    cudaDeviceSynchronize();
    or
    cudaStreamSynchronize(cudaStream);
*/
__global__
void global_func() {
    printf("bid = %d, tid = %d\n", blockIdx.x, threadIdx.x);
}

int main() 
{
    global_func<<<3, 4>>>();
    cudaDeviceSynchronize();

    return 0;
}