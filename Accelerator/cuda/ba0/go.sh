#!/bin/bash

#nvcc -g $1 && ./a.out
nvcc -g $1

objdump -SlzafphxgeGWtTrRs a.out > 1_ALL.log
readelf -a  a.out>readelf_0g_a.log
readelf -h  a.out>readelf_1g_h.log
readelf -l  a.out>readelf_2g_l.log
readelf -Ws a.out>readelf_3g_Ws.log             # Symbol dump
readelf -Ws a.out | c++filt >readelf_4g_Ws.log  # Symbol dump & c++filt