#!/bin/bash

gcc main.c -lcudart

gcc -c buda_rt.c -fPIC

gcc -shared -o libcudart.so buda_rt.o

#
# ...
#
rm libcudart.so.11.0
cp libcudart.so libcudart.so.11.0 -f

objdump -S -drwC a.out>a.out.dump
