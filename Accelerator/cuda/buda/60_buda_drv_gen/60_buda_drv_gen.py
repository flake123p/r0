import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '../includepy'))

from Fout import Fout

cuda_drv_header_file = "/usr/include/cuda.h"

output_file = "60_buda_drv_gen.dump"

fout = Fout(output_file, True)

func_db = []
func_db_line = []

multi_line_declair_enable = False
multi_line_declair = ""

#
# Always return function line
#
def smart_split(input):
    global multi_line_declair_enable
    global multi_line_declair
    
    if multi_line_declair_enable:
        if len(input) < 2:
            print('multi line error')
            multi_line_declair_enable = False
            return ""
        input = input.replace('\n', ' ')
        multi_line_declair += input
        if input[-2] == ';':
            multi_line_declair_enable = False
            #print(multi_line_declair)
            return multi_line_declair
        else:
            return ""

    ignore_strings = [
        '/**',
        '*',
        '*/',
        '#undef',
        '#if',
        '#endif',
        '//',
        '#elif',
        '#define'
    ]
    sp = input.split()
    if len(sp) == 0:
        return ""
    
    if sp[0] in ignore_strings:
        return ""
    
    if 'CUDAAPI' in sp:
        pass
    else:
        return ""
    
    if sp[0] == '__CUDA_DEPRECATED':
        sp = sp[1:]
    
    if sp[0] != 'CUresult':
        print('<<<', sp)
    else:
        #print('>>>', sp)
        pass
    
    # Multiple Line Function declairation
    if len(sp) == 2 or line.find(';') == -1:
        #print(sp)
        multi_line_declair = input.replace('\n', ' ')
        multi_line_declair_enable = True
        #print(multi_line_declair)
        return ""
    
    return input.replace('\n', ' ')

#
# Always return function name
#
def smart_splitII(input):
    func_line = smart_split(input) 
    sp = func_line.split()
    if len(sp) == 0:
        return []
    if sp[0] == '__CUDA_DEPRECATED':
        sp = sp[1:]
    if len(sp) < 3:
        print('Weired Line :', sp)
        print('Weired Line :', sp)
        print('Weired Line :', sp)
        return []
    sp = sp[2].split('(')[0]
    return [sp, func_line]

funcCtr = 0
with open(cuda_drv_header_file) as f:
    for line in f:
        ret = smart_splitII(line)
        if len(ret):
            #print(ret[0])
            funcCtr += 1
            if ret[0] in func_db:
                pass
            else:
                func_db.append(ret[0])
                func_db_line.append(ret[1])

print('funcCtr =', funcCtr)
#quit()

func_v2 = [] # save v2 function without v2 string

if 0:
    #
    # Dump v2 first
    #
    db_idx = 0
    for i in func_db_line:
        print(func_db[db_idx])
        v2_idx = func_db[db_idx].find("_v2")
        if v2_idx != -1:
            func_name = func_db[db_idx][:v2_idx]
            func_v2.append(func_name)
            #print(func_name)
            
            #
            # Same
            #
            sp = i.split(';')[0]
            #print(sp)
            #print(func_db[db_idx])
            
            #  #if !defined(cudaMalloc__DEFINED)
            #  #endif
            fout.dump('#if !defined(' + func_db[db_idx] + '__DEFINED)\n')
            fout.dump(sp)
            fout.dump("{DRV_PRLOC;return 0;}\n")
            fout.dump('#endif\n')
        
        db_idx += 1

    #
    # Dump non v2
    #
    db_idx = 0
    for i in func_db_line:
        if func_db[db_idx] in func_v2:
            continue
        sp = i.split(';')[0]
        #print(sp)
        #print(func_db[db_idx])
        
        #  #if !defined(cudaMalloc__DEFINED)
        #  #endif
        fout.dump('#if !defined(' + func_db[db_idx] + '__DEFINED)\n')
        fout.dump(sp)
        fout.dump("{DRV_PRLOC;return 0;}\n")
        fout.dump('#endif\n')
        
        db_idx += 1
else:
    db_idx = 0
    for i in func_db_line:
        sp = i.split(';')[0]
        #print(sp)
        #print(func_db[db_idx])
        
        #  #if !defined(cudaMalloc__DEFINED)
        #  #endif
        fout.dump('#if !defined(' + func_db[db_idx] + '__DEFINED)\n')
        fout.dump(sp)
        fout.dump("{DRV_PRLOC;return 0;}\n")
        fout.dump('#endif\n')
        
        db_idx += 1