
import torch

print('1')
print('2')
#print(torch.cuda.is_available())
print('3')


'''
Dump:
    cu list (11) =
        cuGetProcAddress
        cuDriverGetVersion
        cuInit
        cuGetExportTable
        cuModuleGetLoadingMode
        cuDeviceGetCount
        cuDeviceGet
        cuDeviceGetName
        cuDeviceTotalMem_v2
        cuDeviceGetAttribute
        cuDeviceGetUuid

    cuda list (1) =
        cudaGetDeviceCount
'''

'''
Run with:
    export LD_PRELOAD=./libcudart.so.11.0

Issue:
    undefined symbol: cudaStreamQuery, version libcudart.so.11.0
    
    undefined symbol: cudaEventRecord, version libcudart.so.11.0
    
    undefined symbol: cudaMemPoolSetAttribute, version libcudart.so.11.0
    
    undefined symbol: cudaFreeAsync, version libcudart.so.11.0
'''