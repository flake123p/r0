#include <stddef.h>
#include <stdio.h>

#define __host__
#define __device__
#define __cudart_builtin__
#define CUDARTAPI
#define __dv(...) 
#define CUDART_CB

#define PRLOC fprintf(stdout, "[PR] %s(), %d\n", __func__, __LINE__);
#define ERRLOC fprintf(stderr, "[ERR] %s(), %d\n", __func__, __LINE__);

#define RT_PRLOC PRLOC
#define PROF_PRLOC PRLOC
//#define INTN_PRLOC ERRLOC
#define INTN_PRLOC
// #define RT_PRLOC TCP_FUNC();fprintf(stderr, "[ERR] %s(), %d\n", __func__, __LINE__);
// #define PROF_PRLOC TCP_FUNC()

#include <driver_types.h>
#include <surface_types.h>
#include <texture_types.h>
#include <vector_types.h>
typedef void (CUDART_CB *cudaStreamCallback_t)(cudaStream_t stream, cudaError_t status, void *userData);

#include "../include/tcp_client_lt2.txt"

//
// Replace part
//
#define cudaMalloc__DEFINED
cudaError_t cudaMalloc(void **devPtr, size_t size)
{
    PRLOC;
    return cudaErrorMemoryAllocation;
}

#define cudaGetDeviceCount__DEFINED
__cudart_builtin__ cudaError_t CUDARTAPI cudaGetDeviceCount(int *count){
    PRLOC;
    *count = 1;
    return 0;
}

#define cudaGetDevice__DEFINED
 __cudart_builtin__ cudaError_t CUDARTAPI cudaGetDevice(int *device){
    RT_PRLOC;
    *device = 0;
    return 0;
}
#include "../include/buda_internal_simple.txt"
#include "../include/buda_rt_simple.txt"
#include "../include/buda_rt_manual.txt"
#include "../include/buda_profiler_simple.txt"