
cuda_rt_header_file = "/usr/include/cuda_runtime_api.h"

output_file = "50_buda_rt_gen.dump"

class Fout:
    def __init__(self, output_file, en=False):
        self.output_file = output_file
        self.enable = en
        self.enabled = False
        
    def enable(self, en):
        self.enable = en

    def dump(self, input):
        if self.enable:
            if self.enabled == False:
                self.f = open(output_file, "w")
                self.enabled = True
        if self.enable and self.enabled:
            self.f.write(input)

fout = Fout(output_file, True)

func_db = []
func_db_line = []

__cplusplus_scanned = False

def smart_split(input):
    global __cplusplus_scanned
    
    if __cplusplus_scanned:
        __cplusplus_scanned = False
        return []

    if input == '#if defined(__cplusplus)\n':
        #print(input)
        __cplusplus_scanned = True
        return []
        
    ignore_strings = [
        '/**',
        '*',
        '*/',
        '#undef',
        '#if',
        '#endif',
        '{',
        '}',
        '//',
        '#elif',
        '#define'
    ]
    sp = input.split()
    if len(sp) < 3 or len(input) <= 3:
        return []
    
    if sp[0] in ignore_strings:
        return []
    
    if input[-2] != ';' or input[-3] != ')':
        return []
    
    if sp[0] != 'extern' or sp[1] != '__host__':
        return []
    
    if sp[2] == '__cudart_builtin__':
        return sp[3:]
    else:
        return sp[2:]
    

with open(cuda_rt_header_file) as f:
    for line in f:
        sp = smart_split(line)
        if len(sp):
            #print(sp[0], sp[1])
            if sp[0] != 'cudaError_t':
                print(">>>>>", line)
            else:
                #print(sp)
                if len(sp) >= 3:
                    sp2 = sp[2].split('(')[0]
                    if sp2 not in func_db:
                        func_db.append(sp2)
                        func_db_line.append(line)
                    #print(sp2[0])
            #fout.dump(line)

db_idx = 0
for i in func_db_line:
    sp = i.split(';')[0]
    sp = sp.split('__host__')[1]
    #print(sp)
    #print(func_db[db_idx])
    
    #  #if !defined(cudaMalloc__DEFINED)
    #  #endif
    fout.dump('#if !defined(' + func_db[db_idx] + '__DEFINED)\n')
    fout.dump(sp)
    fout.dump("{RT_PRLOC;return 0;}\n")
    fout.dump('#endif\n')
    
    db_idx += 1