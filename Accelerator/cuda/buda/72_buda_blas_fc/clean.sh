#!/bin/bash

rm a.out -f
rm *.o -f
rm lib/*.so -f
rm *.s -f
rm *.dump -f
rm lib/lib*
rm __dummy*
