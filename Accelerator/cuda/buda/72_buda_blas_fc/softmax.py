import torch
import torch.nn.functional as F

# Using F.softmax
def softmax_with_torch(x):
    return F.softmax(x, dim=-1)

# Example input tensor
input_tensor = torch.tensor([2.0, -1.0, 1.0]).to('cuda')
# input_tensor = torch.ones(3,1025).to('cuda')
# input_tensor[0][0] = 3
# input_tensor[0][1] = 2
output = softmax_with_torch(input_tensor)
print('[Debug].....: F.softmax:                \n', output)

# [Debug].....: F.softmax:                 tensor([0.5000, 0.5000], device='cuda:0')

'''
torch.ones(2)       1,  2,    2
torch.ones(200)     1,  200,  200
torch.ones(1000)    1,  1000, 1000
torch.ones(1024)    1,  1024, 1024
torch.ones(1025)    _ZN2at6native43_GLOBAL__N__b6de9c8c_10_SoftMax_cu_9f978f6319cunn_SoftMaxForwardILi4EfffNS1_22SoftMaxForwardEpilogueEEEvPT2_PT0_i

    void at::native::(anonymous namespace)::cunn_SoftMaxForward<4, float, float, float, at::native::(anonymous namespace)::SoftMaxForwardEpilogue>(float*, float*, int)
    
    void at::native::(anon)::cunn_SoftMaxForward<4, float, float, float, at::native::(anon)::SoftMaxForwardEpilogue>(float*, float*, int)

torch.ones(5,3)     5,  3,    3
torch.ones(5,3,2)   15, 2,    2
torch.ones(2,1024)  2,  1024, 1024


'''