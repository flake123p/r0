#!/bin/bash

./run.sh

#export LD_PRELOAD=./lib/libcuda.so.1:./lib/libcudart.so.11.0:./lib/libcublas.so.11:./lib/libcublasLt.so.11:./lib/libcudartpp.so:./lib/libbuda.so
export LD_PRELOAD=./lib/libcuda.so.1:./lib/libcudart.so.11.0:./lib/libcublas.so.11:./lib/libcublasLt.so.11:./lib/libcudartpp.so
export LD_LIBRARY_PATH=./lib

#python softmax.py > log.run.log
python $1
