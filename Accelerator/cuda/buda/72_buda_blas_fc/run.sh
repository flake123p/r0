#!/bin/bash

#rm lib*

gcc main.c -lcudart -lcuda -lcublas -lcublasLt

gcc -c buda_rt.c -fPIC
gcc -shared -o libcudart.so buda_rt.o

gcc -c buda_drv.c -fPIC
gcc -shared -o libcuda.so buda_drv.o

g++ -c buda_blas.cpp -fPIC
gcc -shared -o libcublas.so buda_blas.o

gcc -c buda_blaslt.cpp -fPIC
gcc -shared -o libcublasLt.so buda_blaslt.o

g++ -c buda_rtpp.cpp -fPIC
gcc -shared -o libcudartpp.so buda_rtpp.o -lstdc++ #-L . -lbuda

#gcc -shared -o libbuda.so -Wl,--whole-archive libbuda.a
# rm libbuda.so -f
# g++ -shared -o libbuda.so libbuda.a

#
# ...
#
rm *.o
mv libcudart.so lib/libcudart.so.11.0 -f
mv libcuda.so lib/libcuda.so.1 -f
mv libcublas.so lib/libcublas.so.11 -f
mv libcublasLt.so lib/libcublasLt.so.11 -f
mv libcudartpp.so lib/ -f
#mv libbuda.so lib/ -f

#
# objdump -S -drwC libcublas.so>libcublas.so.dump
#

#
# export LD_PRELOAD=./libcuda.so.1:./libcudart.so.11.0:./libcublas.so.11:./libcublasLt.so.11:./libcudartpp.so
# export LD_LIBRARY_PATH=.
#
# ltrace -o __dummy_ltrace.txt -x 'cu*' python pth.py
#
# strace -o __dummy_strace.txt -f -F  python pth.py
#
#  python -m yep -c -o callgrind.out -- pth.py
#
#  unset GTK_PATH && kcachegrind
#
#  unset LD_PRELOAD
#  unset LD_LIBRARY_PATH
#
