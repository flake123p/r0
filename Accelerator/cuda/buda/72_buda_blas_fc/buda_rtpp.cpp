//#include <iostream>
#include <cstdint>
#include <memory>
#include <vector>
#include <numeric>
#include <cmath>
#include <map>
#include <thread>
#include <algorithm>
#include <future>
#include <unordered_map>

#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <float.h>

#define __host__
#define __device__
#define __cudart_builtin__
#define CUDARTAPI
#define __dv(...) 
#define CUDART_CB

#include "buda_rtpp.h"

//#include "../include/tcp_client_lt2.txt"

#define RTPP_PRINT printf

#define PRLOC fprintf(stdout, "[PR] %s(), %d\n", __func__, __LINE__);
#define ERRLOC fprintf(stderr, "[ERR] %s(), %d\n", __func__, __LINE__);

#define RT_PRLOC PRLOC
#define PROF_PRLOC PRLOC
//#define INTN_PRLOC ERRLOC
#define INTN_PRLOC
// #define RT_PRLOC TCP_FUNC();fprintf(stderr, "[ERR] %s(), %d\n", __func__, __LINE__);
// #define PROF_PRLOC TCP_FUNC()

// #include <driver_types.h>
// #include <surface_types.h>
// #include <texture_types.h>
// #include <vector_types.h>
// typedef void (CUDART_CB *cudaStreamCallback_t)(cudaStream_t stream, cudaError_t status, void *userData);

//
// Replace part
//
namespace /* anonymous */ {
struct theDim3 {
    unsigned int x, y, z;
};
class BudaCallConfig {
public:
    union {
        struct theDim3 gridDimMax;
        unsigned int gridDim[3];
    };
    union {
        struct theDim3 bloxDimMax;
        unsigned int blockDim[3];
    };
    size_t sharedMem;
    void *stream;
    void importConfig(unsigned int g[3], unsigned int b[3], size_t sm, void *inStream) {
        memcpy(gridDim, g, sizeof(unsigned int) * 3);
        memcpy(blockDim, b, sizeof(unsigned int) * 3);
        sharedMem = sm;
        stream = inStream;
    }
};

class BudaCallConfig g_CallConfig;
} // namespace /* anonymous */

#define SMAX_F32_PRE  "_ZN43_GLOBAL__N__b6de9c8c_10_SoftMax_cu_9f978f6320softmax_warp_forwardIfffLi"
#define SMAX_F32_POST "ELb0ELb0EEEvPT0_PKT_iiiPKbib"
#define SMAX_F32_MAKE(a) SMAX_F32_PRE #a SMAX_F32_POST

#define SMAX_F32_1 "_ZN43_GLOBAL__N__b6de9c8c_10_SoftMax_cu_9f978f6320softmax_warp_forwardIfffLi1ELb0ELb0EEEvPT0_PKT_iiiPKbib"
#define SMAX_F32_6 "_ZN43_GLOBAL__N__b6de9c8c_10_SoftMax_cu_9f978f6320softmax_warp_forwardIfffLi6ELb0ELb0EEEvPT0_PKT_iiiPKbib"
#define SMAX_F32_2 "_ZN43_GLOBAL__N__b6de9c8c_10_SoftMax_cu_9f978f6320softmax_warp_forwardIfffLi2ELb0ELb0EEEvPT0_PKT_iiiPKbib"

// _ZN2at6native43_GLOBAL__N__b6de9c8c_10_SoftMax_cu_9f978f6319cunn_SoftMaxForwardILi4EfffNS1_22SoftMaxForwardEpilogueEEEvPT2_PT0_i
// void at::native::(anonymous namespace)::cunn_SoftMaxForward<4, float, float, float, at::native::(anonymous namespace)::SoftMaxForwardEpilogue>(float*, float*, int)

int Kernel_SMAX_F32(void **args)
{
    float **_dst = (float **)args[0];
    float **_src = (float **)args[1];
    int *_batch_size = (int *)args[2];
    int *_stride = (int *)args[3];
    int *_element_count = (int *)args[4];
    const bool **_mask = (const bool **)args[5];
    const int *_head_chunk_size = (const int *)args[6];
    bool *_is_transformer_mask = (bool *)args[7];
    RTPP_PRINT(">>> >>> %d, %d, %d, %p, %d, %d\n", *_batch_size, *_stride, *_element_count, *_mask, *_head_chunk_size, *_is_transformer_mask);
    {
        float *dst = *_dst;
        float *src = *_src;
        int element_count = *_element_count;

        extern int run_sm(float *dst, float *src, int len);
        //run_sm(dst, src, element_count);

        for (int j = 0; j < *_batch_size; j++) {
            src = src + (j * element_count);
            dst = dst + (j * element_count);

            double total = 0.;
            for (int i = 0; i < element_count; i++) {
                total += exp(src[i]);
            }
            for (int i = 0; i < element_count; i++) {
                dst[i] = exp(src[i]) / total;
                //RTPP_PRINT("%f\n", dst[i]);
            }
        }
        //RTPP_PRINT("dst = %p\n", dst);
        //RTPP_PRINT("src = %p\n", src);
    }
    return 0;
}

#define SMAX_F32_CUNN "_ZN2at6native43_GLOBAL__N__b6de9c8c_10_SoftMax_cu_9f978f6319cunn_SoftMaxForwardILi4EfffNS1_22SoftMaxForwardEpilogueEEEvPT2_PT0_i"
int Kernel_SMAX_F32_CUNN(void **args)
{
    float **_dst = (float **)args[0];
    const float **_src = (const float **)args[1];
    int *_classes = (int *)args[2];

    RTPP_PRINT(">>> %s(), %p, %p, %d\n", __func__, *_dst, *_src, *_classes);
    for (int blockX = 0; blockX < g_CallConfig.gridDimMax.x; blockX++)
    {
        float *dst = *_dst;
        const float *src = *_src;
        int element_count = *_classes;
        double total = 0.;
        dst = dst + blockX * element_count;
        src = src + blockX * element_count;
        //RTPP_PRINT("%f\n%f\n%f\n%f\n", src[0], src[1], src[2], src[3]);
        for (int i = 0; i < element_count; i++) {
            total += exp(src[i]);
        }
        for (int i = 0; i < element_count; i++) {
            dst[i] = exp(src[i]) / total;
        }
    }
    #if 0
    {
        float *dst = *_dst;
        const float *src = *_src;
        int element_count = *_classes;
        double total = 0.;
        dst += element_count;
        src += element_count;
        for (int i = 0; i < element_count; i++) {
            total += exp(src[i]);
        }
        for (int i = 0; i < element_count; i++) {
            dst[i] = exp(src[i]) / total;
        }
    }
    #endif
    return 0;
}

#define VEC_F32_ABS "_ZN2at6native29vectorized_elementwise_kernelILi4ENS0_10AbsFunctorIfEENS_6detail5ArrayIPcLi2EEEEEviT0_T1_"
// void at::native::vectorized_elementwise_kernel<4, at::native::AbsFunctor<float>, at::detail::Array<char*, 2> >(int, at::native::AbsFunctor<float>, at::detail::Array<char*, 2>)
int Kernel_VEC_F32_ABS(void **args)
{
    int *_num = (int *)args[0];
    at_detail_Array<char *, 2> *ary = (at_detail_Array<char *, 2> *)args[2];

    float *dst = (float *)ary->data[0];
    float *src = (float *)ary->data[1];

    for (int i = 0; i < *_num; i++) {
        dst[i] = fabs(src[i]);
    }
    
    RTPP_PRINT(">>> %s(), num=%d, dst=%p, src=%p, %f, %f\n", __func__, *_num, dst, src, dst[0], src[0]);
    return 0;
}

#define COMPARE_EQ_F32_UNARY "_ZN2at6native29vectorized_elementwise_kernelILi4ENS0_13AUnaryFunctorIffbNS0_51_GLOBAL__N__28ce311f_18_CompareEQKernel_cu_d8008c9616CompareEqFunctorIfEEEENS_6detail5ArrayIPcLi2EEEEEviT0_T1_"
// void at::native::vectorized_elementwise_kernel<4, at::native::AUnaryFunctor<float, float, bool, at::native::(anonymous namespace)::CompareEqFunctor<float> >, at::detail::Array<char*, 2> >(int, at::native::AUnaryFunctor<float, float, bool, at::native::(anonymous namespace)::CompareEqFunctor<float> >, at::detail::Array<char*, 2>)
int Kernel_COMPARE_EQ_F32_UNARY(void **args)
{
    using FuncType = AUnaryFunctor<float, float, bool, CompareEqFunctor<float>>;

    int *_num = (int *)args[0];
    FuncType *auCompare = (FuncType*)args[1];
    at_detail_Array<char *, 2> *ary = (at_detail_Array<char *, 2> *)args[2];

    bool *dst = (bool *)ary->data[0];
    float *src = (float *)ary->data[1];

    for (int i = 0; i < *_num; i++) {
        dst[i] = (*auCompare)(src[i]);
    }

    RTPP_PRINT(">>> %s(), num=%d, dst=%p, src=%p - %d, %f ... a=%f, op=%d [%d,%d]\n", __func__, *_num, dst, src, dst[0], src[0], auCompare->a, (int)auCompare->f.op_, (int)EqOpType::EQ, (int)EqOpType::NE);
    return 0;
}

#define COMPARE_EQ_F32_BINARY "_ZN2at6native29vectorized_elementwise_kernelILi4ENS0_13BinaryFunctorIffbNS0_51_GLOBAL__N__28ce311f_18_CompareEQKernel_cu_d8008c9616CompareEqFunctorIfEEEENS_6detail5ArrayIPcLi3EEEEEviT0_T1_"
// void at::native::vectorized_elementwise_kernel<4, at::native::BinaryFunctor<float, float, bool, at::native::(anonymous namespace)::CompareEqFunctor<float> >, at::detail::Array<char*, 3> >(int, at::native::BinaryFunctor<float, float, bool, at::native::(anonymous namespace)::CompareEqFunctor<float> >, at::detail::Array<char*, 3>)
int Kernel_COMPARE_EQ_F32_BINARY(void **args)
{
    using FuncType = BinaryFunctor<float, float, bool, CompareEqFunctor<float>>;

    int *_num = (int *)args[0];
    FuncType *bCompare = (FuncType*)args[1];
    at_detail_Array<char *, 3> *ary = (at_detail_Array<char *, 3> *)args[2];

    bool *dst = (bool *)ary->data[0];
    float *src = (float *)ary->data[1];
    float *src2  = (float *)ary->data[2];

    for (int i = 0; i < *_num; i++) {
        dst[i] = (*bCompare)(src[i], src2[i]);
    }

    RTPP_PRINT(">>> %s(), %d, %p, %p, %p, %d, %f, %f ... op=%d\n", __func__, *_num, dst, src, src2, dst[0], src[0], src2[0], (int)bCompare->f.op_);
    return 0;
}

#define MUL_FUNC_BINARY "_ZN2at6native29vectorized_elementwise_kernelILi4ENS0_13BinaryFunctorIbbbNS0_15binary_internal10MulFunctorIbEEEENS_6detail5ArrayIPcLi3EEEEEviT0_T1_"
//void at::native::vectorized_elementwise_kernel<4, at::native::BinaryFunctor<bool, bool, bool, at::native::binary_internal::MulFunctor<bool> >, at::detail::Array<char*, 3> >(int, at::native::BinaryFunctor<bool, bool, bool, at::native::binary_internal::MulFunctor<bool> >, at::detail::Array<char*, 3>)
int Kernel_MUL_FUNC_BINARY(void **args)
{
    int *_num = (int *)args[0];
    at_detail_Array<char *, 3> *ary = (at_detail_Array<char *, 3> *)args[2];

    bool *dst = (bool *)ary->data[0];
    bool *src = (bool *)ary->data[1];
    bool *src2  = (bool *)ary->data[2];

    for (int i = 0; i < *_num; i++) {
        dst[i] = src[i] * src2[i];
    }

    RTPP_PRINT(">>> %s(), %d, %p, %p, %p - %d, %d, %d (%d)\n", __func__, *_num, dst, src, src2, dst[0], src[0], src2[0], (int)sizeof(bool));
    return 0;
}

#define VEC_BITWISE_AND_BINARY "_ZN2at6native29vectorized_elementwise_kernelILi4ENS0_13BinaryFunctorIbbbNS0_17BitwiseAndFunctorIbEEEENS_6detail5ArrayIPcLi3EEEEEviT0_T1_"
//void at::native::vectorized_elementwise_kernel<4, at::native::BinaryFunctor<bool, bool, bool, at::native::BitwiseAndFunctor<bool> >, at::detail::Array<char*, 3> >(int, at::native::BinaryFunctor<bool, bool, bool, at::native::BitwiseAndFunctor<bool> >, at::detail::Array<char*, 3>)
int Kernel_VEC_BITWISE_AND_BINARY(void **args)
{
    int *_num = (int *)args[0];
    at_detail_Array<char *, 3> *ary = (at_detail_Array<char *, 3> *)args[2];

    bool *dst = (bool *)ary->data[0];
    bool *src = (bool *)ary->data[1];
    bool *src2  = (bool *)ary->data[2];

    for (int i = 0; i < *_num; i++) {
        dst[i] = src[i] & src2[i];
    }

    RTPP_PRINT(">>> %s(), %d, %p, %p, %p - %d, %d, %d\n", __func__, *_num, dst, src, src2, dst[0], src[0], src2[0]);
    return 0;
}

#define REDUCE_BOOL_POLICY600 "_ZN14at_cuda_detail3cub28DeviceReduceSingleTileKernelINS0_18DeviceReducePolicyIbiiNS0_3SumEE9Policy600ENS0_22TransformInputIteratorIbN2at6native43_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa09NonZeroOpIbEEPblEEPiiS3_iEEvT0_T1_T2_T3_T4_"
/*
void at_cuda_detail::cub::DeviceReduceSingleTileKernel
  <
     ChainedPolicyT,    at_cuda_detail::cub::DeviceReducePolicy<bool, int, int, at_cuda_detail::cub::Sum>::Policy600, 
     InputIteratorT,    at_cuda_detail::cub::TransformInputIterator<bool, at::native::(anonymous namespace)::NonZeroOp<bool>, bool*, long>, 
     OutputIteratorT,   int*, 
     OffsetT,           int, 
     ReductionOpT,      at_cuda_detail::cub::Sum, 
     OutputT,           int
  >
  (
     InputIteratorT,    d_in,           at_cuda_detail::cub::TransformInputIterator<bool, at::native::(anonymous namespace)::NonZeroOp<bool>, bool*, long>, 
     OutputIteratorT,   d_out,          int*, 
     OffsetT,           num_items,      int, 
     ReductionOpT,      reduction_op,   at_cuda_detail::cub::Sum, 
     OutputT,           init,           int
  )
*/
int Kernel_REDUCE_BOOL_POLICY600(void **args)
{
    using FuncType = TransformInputIterator<bool, NonZeroOp<bool>, bool*, long>;
    FuncType *d_in = (FuncType *)args[0];
    int **d_out = (int **)args[1]; 
    int *num_items = (int *)args[2];

    int ctr = 0;
    for (int i = 0; i < *num_items; i++) {
        RTPP_PRINT("%d (%d)\n", d_in->input_itr[i], d_in->input_itr[i] != (bool)0);
        if (d_in->input_itr[i] != (bool)0) {
            ctr++;
        }
    }
    **d_out = ctr;

    RTPP_PRINT(">>> %s(), num_items=%d, d_out=%p\n", __func__, *num_items, *d_out);
    return 0;
}
#define POW_SCALAR_F32_LAMBDA1_A "_ZN2at6native29vectorized_elementwise_kernelILi4EZNS0_51_GLOBAL__N__b1108092_12_PowKernel_cu_6f8d5651_2468729pow_tensor_scalar_kernel_implIffEEvRNS_18TensorIteratorBaseET0_EUlfE_NS_6detail5ArrayIPcLi2EEEEEviS6_T1_"
#define POW_SCALAR_F32_LAMBDA1   "_ZN2at6native29vectorized_elementwise_kernelILi4EZNS0_51_GLOBAL__N__b1108092_12_PowKernel_cu_8777ef34_2473629pow_tensor_scalar_kernel_implIffEEvRNS_18TensorIteratorBaseET0_EUlfE_NS_6detail5ArrayIPcLi2EEEEEviS6_T1_"
#define POW_SCALAR_F32_LAMBDA2   "_ZN2at6native29vectorized_elementwise_kernelILi4EZNS0_51_GLOBAL__N__b1108092_12_PowKernel_cu_8777ef34_2473629pow_tensor_scalar_kernel_implIffEEvRNS_18TensorIteratorBaseET0_EUlfE0_NS_6detail5ArrayIPcLi2EEEEEviS6_T1_"
#define POW_SCALAR_F32_LAMBDA3   "_ZN2at6native29vectorized_elementwise_kernelILi4EZNS0_51_GLOBAL__N__b1108092_12_PowKernel_cu_8777ef34_2473629pow_tensor_scalar_kernel_implIffEEvRNS_18TensorIteratorBaseET0_EUlfE1_NS_6detail5ArrayIPcLi2EEEEEviS6_T1_"
#define POW_SCALAR_F32_LAMBDA4   "_ZN2at6native29vectorized_elementwise_kernelILi4EZNS0_51_GLOBAL__N__b1108092_12_PowKernel_cu_8777ef34_2473629pow_tensor_scalar_kernel_implIffEEvRNS_18TensorIteratorBaseET0_EUlfE2_NS_6detail5ArrayIPcLi2EEEEEviS6_T1_"
/*
void at::native::vectorized_elementwise_kernel
  <
   4, 
   at::native::(anonymous namespace)::pow_tensor_scalar_kernel_impl<float, float>(at::TensorIteratorBase&, float)::{lambda(float)#1}, 
   at::detail::Array<char*, 2> 
  >
  (
   int, 
   at::native::(anonymous namespace)::pow_tensor_scalar_kernel_impl<float, float>(at::TensorIteratorBase&, float)::{lambda(float)#1}, 
   at::detail::Array<char*, 2>
  )
*/
/* _ZN2at6native29vectorized_elementwise_kernelILi4EZNS0_51_GLOBAL__N__b1108092_12_PowKernel_cu_8777ef34_2473629pow_tensor_scalar_kernel_implIffEEvRNS_18TensorIteratorBaseET0_EUlfE0_NS_6detail5ArrayIPcLi2EEEEEviS6_T1_
void at::native::vectorized_elementwise_kernel
  <
   4, 
   at::native::(anonymous namespace)::pow_tensor_scalar_kernel_impl<float, float>(at::TensorIteratorBase&, float)::{lambda(float)#2}, 
   at::detail::Array<char*, 2> >
  (
    int, 
    at::native::(anonymous namespace)::pow_tensor_scalar_kernel_impl<float, float>(at::TensorIteratorBase&, float)::{lambda(float)#2}, 
    at::detail::Array<char*, 2>
   )
*/
/*
void at::native::vectorized_elementwise_kernel
  <
   4, 
   at::native::(anonymous namespace)::pow_tensor_scalar_kernel_impl<float, float>(at::TensorIteratorBase&, float)::{lambda(float)#1}, 
   at::detail::Array<char*, 2> 
  >
  (
    int, 
    at::native::(anonymous namespace)::pow_tensor_scalar_kernel_impl<float, float>(at::TensorIteratorBase&, float)::{lambda(float)#1}, 
    at::detail::Array<char*, 2>
   )
*/
int Kernel_POW_SCALAR_F32_LAMBDA1(void **args)
{
    int *num = (int *)args[0];
    void *it = (void *)args[1];
    at_detail_Array<char *, 2> *ary = (at_detail_Array<char *, 2> *)args[2];

    float *dst = (float *)ary->data[0];
    float *src = (float *)ary->data[1];

    RTPP_PRINT(">>> %s(), dst=%p, src=%p, num=%d, exp=%f, (args[1]=%p / ary[0]=%p)\n", 
        __func__, dst, src, *num, *src, args[1], ary->data[0]);

    // uint64_t curr;
    // float **p;
    // uint64_t addr = reinterpret_cast<uint64_t>(args[1]);
    // printf("args    = %p\n", args);
    // printf("args[0] = %p\n", args[0]);
    // printf("args[1] = %p\n", args[1]);
    // printf("args[2] = %p\n", args[2]);
    // for (int i = 0; i < 0x500; i += 8) {
    //     curr = addr + i;
    //     p = reinterpret_cast<float **>(curr);
    //     printf("[%4d] %18p : %18p\n", i, p, *p);
    // }
    for (int i = 0; i < *num; i++) {
        dst[i] = pow(src[i], 2);
    }

    // extern void *bw_TensorIteratorBase__data_ptr(void *it, int arg);
    // void *d = bw_TensorIteratorBase__data_ptr(it, 0);
    // printf("data[0] = %p\n", d);

    return 0;
}

int Kernel_POW_SCALAR_F32_LAMBDA2(void **args)
{
    int *num = (int *)args[0];
    void *it = (void *)args[1];
    at_detail_Array<char *, 2> *ary = (at_detail_Array<char *, 2> *)args[2];

    float *dst = (float *)ary->data[0];
    float *src = (float *)ary->data[1];

    RTPP_PRINT(">>> %s(), num=%d, exp=%f, (args[1]=%p / ary[0]=%p)\n", __func__, *num, *src, args[1], ary->data[0]);

    for (int i = 0; i < *num; i++) {
        dst[i] = pow(src[i], 3);
    }

    return 0;
}

int Kernel_POW_SCALAR_F32_LAMBDA3(void **args)
{
    int *num = (int *)args[0];
    void *it = (void *)args[1];
    at_detail_Array<char *, 2> *ary = (at_detail_Array<char *, 2> *)args[2];

    float *dst = (float *)ary->data[0];
    float *src = (float *)ary->data[1];

    RTPP_PRINT(">>> %s(), num=%d, exp=%f, (args[1]=%p / ary[0]=%p)\n", __func__, *num, *src, args[1], ary->data[0]);

    for (int i = 0; i < *num; i++) {
        dst[i] = 1.0 / pow(src[i], 2);
    }

    return 0;
}

int Kernel_POW_SCALAR_F32_LAMBDA4(void **args)
{
    int *num = (int *)args[0];
    float *exp = (float *)args[1];
    at_detail_Array<char *, 2> *ary = (at_detail_Array<char *, 2> *)args[2];

    float *dst = (float *)ary->data[0];
    float *src = (float *)ary->data[1];

    RTPP_PRINT(">>> %s(), num=%d, exp=%f, (args[1]=%p / ary[0]=%p) [%f]\n", __func__, *num, *src, args[1], ary->data[0], *((float *)args[1]));

    for (int i = 0; i < *num; i++) {
        dst[i] = pow(src[i], *exp);
    }
    // uint64_t curr;
    // float **p;
    // uint64_t addr = reinterpret_cast<uint64_t>(args[1]);
    // printf("args    = %p\n", args);
    // printf("args[0] = %p\n", args[0]);
    // printf("args[1] = %p\n", args[1]);
    // printf("args[2] = %p\n", args[2]);
    // for (int i = 0; i < 0x500; i += 8) {
    //     curr = addr + i;
    //     p = reinterpret_cast<float **>(curr);
    //     printf("[%4d] %18p : %18p\n", i, p, *p);
    // }
    return 0;
}

#define RSQRT_LAMBDA221_F32 "_ZN2at6native29vectorized_elementwise_kernelILi4EZZZNS0_17rsqrt_kernel_cudaERNS_18TensorIteratorBaseEENKUlvE0_clEvENKUlvE0_clEvEUlfE_NS_6detail5ArrayIPcLi2EEEEEviT0_T1_"
/*
void at::native::vectorized_elementwise_kernel
  <
   4, 
   at::native::rsqrt_kernel_cuda(at::TensorIteratorBase&)::{lambda()#2}::operator()() const::{lambda()#2}::operator()() const::{lambda(float)#1}, 
   at::detail::Array<char*, 2> 
  >
  (
   int, 
   at::native::rsqrt_kernel_cuda(at::TensorIteratorBase&)::{lambda()#2}::operator()() const::{lambda()#2}::operator()() const::{lambda(float)#1}, 
   at::detail::Array<char*, 2>
  )
*/
int Kernel_RSQRT_LAMBDA221_F32(void **args)
{
    int *num = (int *)args[0];
    float *exp = (float *)args[1];
    at_detail_Array<char *, 2> *ary = (at_detail_Array<char *, 2> *)args[2];

    float *dst = (float *)ary->data[0];
    float *a1 = (float *)ary->data[1];

    RTPP_PRINT(">>> %s(), dst=%p, num=%d, *a1=%f[%p], (args[1]=%p / ary[0]=%p) [%p]\n", 
        __func__, dst, *num, *a1, a1, args[1], ary->data[0], args[1]);

    // {
    //     uint64_t curr;
    //     float **p;
    //     uint64_t addr = reinterpret_cast<uint64_t>(args[1]);
    //     printf("args    = %p\n", args);
    //     printf("args[0] = %p\n", args[0]);
    //     printf("args[1] = %p\n", args[1]);
    //     printf("args[2] = %p\n", args[2]);
    //     for (int i = 0; i < 0x700; i += 8) {
    //         curr = addr + i;
    //         p = reinterpret_cast<float **>(curr);
            
    //         uint64_t pcurr = reinterpret_cast<uint64_t>(*p), diff;
    //         if (pcurr > curr) {
    //             diff = pcurr - curr;
    //         } else {
    //             diff = curr - pcurr;
    //         }
    //         if (diff < 0x10000000) {
    //             float ***ppp = reinterpret_cast<float ***>(curr);
    //             printf("[%4d] %18p : %18p : %18p\n", i, p, *p, **ppp);
    //         } else {
    //             printf("[%4d] %18p : %18p\n", i, p, *p);
    //         }
    //     }
    // }

#if 0 // old way
    uint64_t addr = reinterpret_cast<uint64_t>(args[1]);
    uint64_t curr;

    curr = addr + 352;
    //curr = addr + 1496;
    float **src = reinterpret_cast<float **>(curr);
    printf("*src = %p\n", *src);
    for (int i = 0; i < *num; i++) {
        dst[i] = 1.0/sqrt((*src)[i]);
    }
#else
    for (int i = 0; i < *num; i++) {
        dst[i] = 1.0/sqrt(a1[i]);
    }
#endif
#if 0
    extern void *bw_TensorIteratorBase__data_ptr(void *it, int arg);
    void *d = bw_TensorIteratorBase__data_ptr(args[1], 2);
    //printf("data[0] = %p\n", d);
    for (int i = 0; i < 14; i++) {
        d = bw_TensorIteratorBase__data_ptr(args[1], i);
        printf("data[%d] = %p\n", i, d);
    }
#endif
    return 0;
}

#define REDUCE_MAX_F32 "_ZN2at6native13reduce_kernelILi512ELi1ENS0_8ReduceOpIfNS0_14func_wrapper_tIfNS0_13MaxNanFunctorIfEEEEjfLi4EEEEEvT1_"
//void at::native::reduce_kernel<512, 1, at::native::ReduceOp<float, at::native::func_wrapper_t<float, at::native::MaxNanFunctor<float> >, unsigned int, float, 4> >(at::native::ReduceOp<float, at::native::func_wrapper_t<float, at::native::MaxNanFunctor<float> >, unsigned int, float, 4>)
int Kernel_REDUCE_MAX_F32(void **args)
{
    using type1 = at_native_func_wrapper_t<float, at_native_MaxNanFunctor<float> >;

    using type2 = at_native_ReduceOp<float, type1, unsigned int, float, 4>;

    type2 *obj = (type2 *)args[0];

    uint64_t addr = reinterpret_cast<uint64_t>(obj);

    RTPP_PRINT(">>> %s(), [%p/%p], %p, %p - %lu\n", 
        __func__, obj, &obj->src, obj->src, obj->dst[0], addr);
    

    void *dump = (void *)obj;
    uint64_t curr;
    float **p;

    // printf("args    = %p\n", args);
    // printf("args[0] = %p\n", args[0]);
    // printf("args[1] = %p\n", args[1]);
    // printf("args[2] = %p\n", args[2]);
    // for (int i = 0; i < 0x500; i += 8) {
    //     curr = addr + i;
    //     p = reinterpret_cast<float **>(curr);
    //     printf("[%4d] %18p : %18p\n", i, p, *p);
    // }
    curr = addr + 1240;
    unsigned int *num = reinterpret_cast<unsigned int *>(curr);
    RTPP_PRINT("num = %d\n", *num);

    // src
    curr = addr + 984;
    p = reinterpret_cast<float **>(curr);
    //printf("%f, %f, %f, %f\n", (*p)[0], (*p)[1], (*p)[2], (*p)[3]);
    float max = 0;
    for (int i = 0; i < *num; i++) {
        if ((*p)[i] > max) {
            max = (*p)[i];
        }
    }
    // dst
    curr = addr + 992;
    p = reinterpret_cast<float **>(curr);
    (*p)[0] = max;
    
    return 0;
}

#define REDUCE_MIN_F32 "_ZN2at6native13reduce_kernelILi512ELi1ENS0_8ReduceOpIfNS0_14func_wrapper_tIfNS0_13MinNanFunctorIfEEEEjfLi4EEEEEvT1_"
//void at::native::reduce_kernel<512, 1, at::native::ReduceOp<float, at::native::func_wrapper_t<float, at::native::MinNanFunctor<float> >, unsigned int, float, 4> >(at::native::ReduceOp<float, at::native::func_wrapper_t<float, at::native::MinNanFunctor<float> >, unsigned int, float, 4>)
int Kernel_REDUCE_MIN_F32(void **args)
{
    using type1 = at_native_func_wrapper_t<float, at_native_MaxNanFunctor<float> >;

    using type2 = at_native_ReduceOp<float, type1, unsigned int, float, 4>;

    type2 *obj = (type2 *)args[0];

    uint64_t addr = reinterpret_cast<uint64_t>(obj);

    RTPP_PRINT(">>> %s(), [%p/%p], %p, %p - %lu\n", 
        __func__, obj, &obj->src, obj->src, obj->dst[0], addr);
    

    void *dump = (void *)obj;
    uint64_t curr;
    float **p;

    // printf("args    = %p\n", args);
    // printf("args[0] = %p\n", args[0]);
    // printf("args[1] = %p\n", args[1]);
    // printf("args[2] = %p\n", args[2]);
    // for (int i = 0; i < 0x500; i += 8) {
    //     curr = addr + i;
    //     p = reinterpret_cast<float **>(curr);
    //     printf("[%4d] %18p : %18p\n", i, p, *p);
    // }

    curr = addr + 1240;
    unsigned int *num = reinterpret_cast<unsigned int *>(curr);
    RTPP_PRINT("num = %d\n", *num);

    // src
    curr = addr + 984;
    p = reinterpret_cast<float **>(curr);
    //printf("%f, %f, %f, %f\n", (*p)[0], (*p)[1], (*p)[2], (*p)[3]);
    float min = FLT_MAX;
    for (int i = 0; i < *num; i++) {
        if ((*p)[i] < min) {
            min = (*p)[i];
        }
    }
    // dst
    curr = addr + 992;
    p = reinterpret_cast<float **>(curr);
    (*p)[0] = min;
    
    return 0;
}

#define MUL_FUNC_BROADCAST_F32 "_ZN2at6native18elementwise_kernelILi128ELi2EZNS0_15gpu_kernel_implINS0_13BinaryFunctorIfffNS0_15binary_internal10MulFunctorIfEEEEEEvRNS_18TensorIteratorBaseERKT_EUliE_EEviT1_"
/*
void at::native::elementwise_kernel
  <
   128, 
   2, 
   at::native::gpu_kernel_impl
     <
      at::native::BinaryFunctor
        <
         float, 
         float, 
         float, 
         at::native::binary_internal::MulFunctor<float> 
        >
     >
     (
        at::TensorIteratorBase&, 
        at::native::BinaryFunctor
          <
           float, 
           float, 
           float, at::native::binary_internal::MulFunctor<float> 
          > const&
     )::{lambda(int)#1}
  >
  (
    int, 
    at::native::gpu_kernel_impl
      <
       at::native::BinaryFunctor
         <
          float, 
          float, 
          float, 
          at::native::binary_internal::MulFunctor<float>
         >
      >
      (
       at::TensorIteratorBase&, 
       at::native::BinaryFunctor
         <
          float, 
          float, 
          float, 
          at::native::binary_internal::MulFunctor<float> 
         > const&
      )::{lambda(int)#1}
  )
#
#
#
void at::native::elementwise_kernel
  <
   128, 
   2, 
   at::native::gpu_kernel_impl
     <
      at::native::BinaryFunctor< float, float, float, at::native::binary_internal::MulFunctor<float> >
     >
     (
      at::TensorIteratorBase&, 
      at::native::BinaryFunctor<float, float, float, at::native::binary_internal::MulFunctor<float> > const&
     )::{lambda(int)#1}
  >
  (
    int, 
    at::native::gpu_kernel_impl
      <
       at::native::BinaryFunctor<float, float, float, at::native::binary_internal::MulFunctor<float> > 
      >
      (
       at::TensorIteratorBase&, 
       at::native::BinaryFunctor<float, float, float, at::native::binary_internal::MulFunctor<float> > const&
      )::{lambda(int)#1}
  )
*/
int Kernel_MUL_FUNC_BROADCAST_F32(void **args)
{
    int *N = (int *)args[0]; 

    RTPP_PRINT(">>> %s(), N=%d\n", __func__, *N);

    // printf("args    = %p\n", args);
    // printf("args[0] = %p\n", args[0]);
    // printf("args[1] = %p\n", args[1]);
    // printf("args[2] = %p\n", args[2]);
    uint64_t addr = reinterpret_cast<uint64_t>(args[0]);
    uint64_t curr;
    float **p;
    // for (int i = 0; i < 0x500; i += 8) {
    //     curr = addr + i;
    //     p = reinterpret_cast<float **>(curr);
    //     printf("[%4d] %18p : %18p\n", i, p, *p);
    // }
    addr = reinterpret_cast<uint64_t>(args[1]);
    // for (int i = 0; i < 0x500; i += 8) {
    //     curr = addr + i;
    //     p = reinterpret_cast<float **>(curr);
    //     printf("[%4d] %18p : %18p\n", i, p, *p);
    // }

    float **_dst  = reinterpret_cast<float **>(addr + 608);
    float **_src1 = reinterpret_cast<float **>(addr + 616);
    float **_src2 = reinterpret_cast<float **>(addr + 624);
    int *batch = reinterpret_cast<int *>(addr + 64);
    int batch_size = *N / *batch;
    float *dst = *_dst;
    float *src1 = *_src1;
    float *src2 = *_src2;
    for (int i = 0; i < *N; i++) {
        dst[i] = src1[i] * src2[i/batch_size];
        // printf("--> %f, %f, %f\n", dst[i], src1[i], src2[0]);
        // printf("==> %p, %p, %p\n", &dst[i], &src1[i], &src2[0]);
    }

    return 0;
}

// #define CUDA_ADD_F32 "_ZN2at6native29vectorized_elementwise_kernelILi4ENS0_15CUDAFunctor_addIfEENS_6detail5ArrayIPcLi3EEEEEviT0_T1_"
// int Kernel_CUDA_ADD_F32(void **args)
// {
// /*
//     template <typename scalar_t>
//     struct CUDAFunctor_add {
//     using opmath_t = at::opmath_type<scalar_t>;
//     opmath_t alpha_;
//     CUDAFunctor_add(opmath_t alpha) : alpha_(alpha) {}
//     __device__ scalar_t operator()(scalar_t self, scalar_t other) const {
//         return ufunc::add(static_cast<opmath_t>(self), static_cast<opmath_t>(other), alpha_);
//     }
// */
//     // float *_dst = (float *)args[0];
//     // float *_src1 = (float *)args[1];
//     // float *_src2 = (float *)args[2];
//     int *num = (int *)args[0];
//     float **at_detail_array = (float **)args[2];

//     float *dst = (float *)at_detail_array[0];
//     float *src = (float *)at_detail_array[1];
//     float *_2  = (float *)at_detail_array[2];

    
//     for (int i = 0; i < *num; i++) {
//         *dst = *src + *_2;
        
//         dst++;
//         src++;
//         _2++;
//     }
//     //RTPP_PRINT(" %f, %f, %f\n", *_dst, *_src1, *_src2);
//     RTPP_PRINT(">>> %s(), num = %d, %f\n", __func__, *num, *dst);
//     return 0;
// }

//
// TINSLEY
//

template <typename T>
struct MulFunctor {
   T operator()(T a, T b) const {
    return a * b;
  }
};

template <>
struct MulFunctor<bool> {
    bool operator()(bool a, bool b) const {
    return a && b;
  }
};

template <typename scalar_t>
struct DivFunctor {
    scalar_t operator()(scalar_t a, scalar_t b) const {
    return a / b;
  }
};

template <typename scalar_t>
struct OpMathType {
  using type = scalar_t;
};

template <typename T>
using opmath_type = typename OpMathType<T>::type;

template <typename T>
T cudaadd(T self, T other, T alpha) {
  return self + alpha * other;
}

template <typename scalar_t>
struct CUDAFunctorOnSelf_add {
    using opmath_t = opmath_type<scalar_t>;
    opmath_t other_;
    opmath_t alpha_;
    CUDAFunctorOnSelf_add(opmath_t other, opmath_t alpha) : other_(other), alpha_(alpha) {}
    __device__ scalar_t operator()(scalar_t self) const {
      return cudaadd(static_cast<opmath_t>(self), other_, alpha_);
    }
};

#define CUDA_ADD_INPLACE_F32 "_ZN2at6native29vectorized_elementwise_kernelILi4ENS0_21CUDAFunctorOnSelf_addIfEENS_6detail5ArrayIPcLi2EEEEEviT0_T1_"
//void at::native::vectorized_elementwise_kernel<4, at::native::CUDAFunctorOnSelf_add<float>, at::detail::Array<char*, 2> >(int, at::native::CUDAFunctorOnSelf_add<float>, at::detail::Array<char*, 2>)
int Kernel_CUDA_ADD_INPLACE_F32(void **args)
{
    using FuncType = CUDAFunctorOnSelf_add<float>;
    int *num = (int *)args[0];
    FuncType *auadd = (FuncType*)args[1];
    float **at_detail_array = (float **)args[2];

    float *dst = (float *)at_detail_array[0];
    float *src = (float *)at_detail_array[1];

    

    for (int i = 0; i < *num; i++) {
        dst[i]=(*auadd)(src[i]);
        
    }
    //printf(" %f, %f, %f\n", *_dst, *_src1, *_src2);
    printf(">>> %s(), num = %d, %f,%f\n", __func__, *num, *dst, *src);
    return 0;
}

// template<typename func_t, int nargs=function_traits<func_t>::arity>
// struct uses_non_c10_complex {
//   constexpr static bool check() {
//     using traits = function_traits<func_t>;
//     using type = typename traits::template arg<nargs - 1>::type;
//     constexpr bool non_c10_complex =
//         std::is_same<std::complex<float>, type>::value
//         || std::is_same<std::complex<double>, type>::value
//         || std::is_same<thrust::complex<float>, type>::value
//         || std::is_same<thrust::complex<double>, type>::value;

//     if constexpr (non_c10_complex) {
//       return true;
//     } else {
//       return uses_non_c10_complex<func_t, nargs - 1>::check();
//     }
//   }
// };

// template<typename func_t>
// struct uses_non_c10_complex<func_t, 0> {
//   constexpr static bool check() {
//     using traits = function_traits<func_t>;
//     using type = typename traits::result_type;
//     constexpr bool non_c10_complex =
//         std::is_same<std::complex<float>, type>::value
//         || std::is_same<std::complex<double>, type>::value
//         || std::is_same<thrust::complex<float>, type>::value
//         || std::is_same<thrust::complex<double>, type>::value;

//     return non_c10_complex;
//   }
// };

// template <typename func_t>
// void gpu_kernel_impl(TensorIteratorBase& iter, const func_t& f) {
//   using traits = function_traits<func_t>;
//   using arg0_t = typename traits::result_type;
//   constexpr int ntensors = traits::arity + 1;

// //   TORCH_INTERNAL_ASSERT(iter.can_use_32bit_indexing());
// //   TORCH_INTERNAL_ASSERT(iter.ntensors() == traits::arity + 1);
//   bool non_c10_complex = uses_non_c10_complex<func_t>::check();

//   at_detail_array<char*, ntensors> data;
//   for (int i = 0; i < ntensors; i++) {
//     data[i] = (char*)iter.data_ptr(i);
//   }

//   at_detail_array<ScalarType, ntensors> dtypes;
//   for (int i = 0; i < ntensors; i++) {
//     dtypes[i] = iter.dtype(i);
//   }

//   int64_t numel = iter.numel();
//   if (iter.is_trivial_1d()) {
//     auto inner_strides = iter.get_inner_strides();
//     at_detail_array<int, ntensors> strides;
//     for (int i = 0; i < ntensors; i++) {
//       strides[i] = inner_strides[i];
//     }

//     // TODO: can non_c10_complex go through the other path?  Need to verify.
//     if (needs_dynamic_casting<func_t>::check(iter) || non_c10_complex) {
//       legacy::launch_kernel<launch_size_1d, 1>(numel, [=]GPU_LAMBDA(int idx) {
//         void* out = data[0] + strides[0] * idx;
//         arg0_t result = legacy::invoke(f, &data.data[1], &strides.data[1], &dtypes.data[1], idx);
//         c10::cast_and_store<arg0_t>(dtypes[0], out, result);
//       });
//     } else if (iter.has_contiguous_first_dim() && modern::detail::has_same_arg_types<func_t>::value) {
//       modern::launch_kernel(numel, f, data);
//     } else {
//       legacy::launch_kernel<launch_size_1d, 1>(numel, [=]GPU_LAMBDA(int idx) {
//         arg0_t* out = (arg0_t*)(data[0] + strides[0] * idx);
//         *out = legacy::invoke(f, &data.data[1], &strides.data[1], idx);
//       });
//     }
//   } else {
//     auto offset_calc = ::make_offset_calculator<traits::arity + 1>(iter);
//     // TODO: can non_c10_complex go through the other path?  Need to verify.
//     if (needs_dynamic_casting<func_t>::check(iter) || non_c10_complex) {
//       legacy::launch_kernel<launch_size_nd, launch_bound2>(numel, [=]GPU_LAMBDA(int idx) {
//         auto offsets = offset_calc.get(idx);
//         void* out = data[0] + offsets[0];
//         arg0_t result = legacy::invoke(f, &data.data[1], &offsets.data[1], &dtypes.data[1], 1);
//         c10::cast_and_store<arg0_t>(dtypes[0], out, result);
//       });
//     } else {
//       legacy::launch_kernel<launch_size_nd, launch_bound2>(numel, [=]GPU_LAMBDA(int idx) {
//         auto offsets = offset_calc.get(idx);
//         arg0_t* out = (arg0_t*)(data[0] + offsets[0]);
//         *out = legacy::invoke(f, &data.data[1], &offsets.data[1], 1);
//       });
//     }
//   }
// }


// #define ADD_FUNC_BROADCAST_F32 "_ZN2at6native18elementwise_kernelILi128ELi2EZNS0_15gpu_kernel_implINS0_15CUDAFunctor_addIfEEEEvRNS_18TensorIteratorBaseERKT_EUliE_EEviT1_"
// //void at::native::elementwise_kernel<128, 2, at::native::gpu_kernel_impl<at::native::CUDAFunctor_add<float> >(at::TensorIteratorBase&, at::native::CUDAFunctor_add<float> const&)::{lambda(int)#1}>(int, at::native::gpu_kernel_impl<at::native::CUDAFunctor_add<float> >(at::TensorIteratorBase&, at::native::CUDAFunctor_add<float> const&)::{lambda(int)#1})
// int Kernel_ADD_FUNC_BROADCAST_F32(void **args)
// {
//     using FuncType = at::native::gpu_kernel_impl<CUDAFunctor_add<float>>;
//     FuncType *addfuctor = (FuncType*)args[1];

//     int *num = (int *)args[0]; 
//     float **at_detail_array = (float **)FuncType->data;

//     float *dst = (float *)at_detail_array[0];
//     float *src = (float *)at_detail_array[1];
//     float *_2  = (float *)at_detail_array[2];

//     for (int i = 0; i < *num; i++) {
//         dst[i] = src[i] * (*_2);
       
//     }

//     RTPP_PRINT(">>> %s(), N=%d\n", __func__, *num);
//     return 0;
// }

#define CUDA_MUL_F32_Binary "_ZN2at6native29vectorized_elementwise_kernelILi4ENS0_13BinaryFunctorIfffNS0_15binary_internal10MulFunctorIfEEEENS_6detail5ArrayIPcLi3EEEEEviT0_T1_"
//void at::native::vectorized_elementwise_kernel<4, at::native::BinaryFunctor<float, float, float, at::native::binary_internal::MulFunctor<float> >, at::detail::Array<char*, 3> >(int, at::native::BinaryFunctor<float, float, float, at::native::binary_internal::MulFunctor<float> >, at::detail::Array<char*, 3>)
int Kernel_CUDA_MUL_F32_Binary(void **args)
{
    int *num = (int *)args[0];
    float **at_detail_array = (float **)args[2];

    float *dst = (float *)at_detail_array[0];
    float *src = (float *)at_detail_array[1];
    float *_2  = (float *)at_detail_array[2];
    
    for (int i = 0; i < *num; i++) {
        *dst = *src * (*_2);
        dst++;
        src++;
        _2++;
    }
    //printf(" %f, %f, %f\n", *_dst, *_src1, *_src2);
    printf(">>> %s(), num = %d, %f\n", __func__, *num, *dst);
    return 0;
}

#define CUDA_MUL_F32_UNARY "_ZN2at6native29vectorized_elementwise_kernelILi4ENS0_13AUnaryFunctorIfffNS0_15binary_internal10MulFunctorIfEEEENS_6detail5ArrayIPcLi2EEEEEviT0_T1_"
//void at::native::vectorized_elementwise_kernel<4, at::native::AUnaryFunctor<float, float, float, at::native::binary_internal::MulFunctor<float> >, at::detail::Array<char*, 2> >(int, at::native::AUnaryFunctor<float, float, float, at::native::binary_internal::MulFunctor<float> >, at::detail::Array<char*, 2>)int Kernel_CUDA_MUL_F32_UNARY(void **args)
int Kernel_CUDA_MUL_F32_UNARY(void **args)
{
    using FuncType = AUnaryFunctor<float, float, float, MulFunctor<float> >;
    FuncType *mulfuctor = (FuncType*)args[1];

    int *num = (int *)args[0];
    float **at_detail_array = (float **)args[2];

    float *dst = (float *)at_detail_array[0];
    float *src = (float *)at_detail_array[1];
    
    
    for (int i = 0; i < *num; i++) {
        dst[i] = (*mulfuctor)(src[i]);
        
    }
    //printf(" %f, %f, %f\n", *_dst, *_src1, *_src2);
    printf(">>> %s(), num = %d, %f\n", __func__, *num, *dst);
    return 0;
}



#define COMPARE_EQ_BOOL_UNARY "_ZN2at6native29vectorized_elementwise_kernelILi4ENS0_13AUnaryFunctorIbbbNS0_51_GLOBAL__N__28ce311f_18_CompareEQKernel_cu_d8008c9616CompareEqFunctorIbEEEENS_6detail5ArrayIPcLi2EEEEEviT0_T1_"
// void at::native::vectorized_elementwise_kernel<4, at::native::AUnaryFunctor<bool, bool, bool, at::native::(anonymous namespace)::CompareEqFunctor<bool> >, at::detail::Array<char*, 2> >(int, at::native::AUnaryFunctor<bool, bool, bool, at::native::(anonymous namespace)::CompareEqFunctor<bool> >, at::detail::Array<char*, 2>)
int Kernel_COMPARE_EQ_BOOL_UNARY(void **args)
{
    using FuncType = AUnaryFunctor<bool, bool, bool, CompareEqFunctor<bool>>;

    int *_num = (int *)args[0];
    FuncType *auCompare = (FuncType*)args[1];
    at_detail_Array<char *, 2> *ary = (at_detail_Array<char *, 2> *)args[2];

    bool *dst = (bool *)ary->data[0];
    bool *src = (bool *)ary->data[1];

    for (int i = 0; i < *_num; i++) {
        dst[i] = (*auCompare)(src[i]);
    }

    RTPP_PRINT(">>> %s(), %d, %p, %p - %d, %d ... a=%d, op=%d [%d,%d]\n", __func__, *_num, dst, src, dst[0], src[0], auCompare->a, (int)auCompare->f.op_, (int)EqOpType::EQ, (int)EqOpType::NE);
    return 0;
}

#define COMPARE_EQ_BOOL_BINARY "_ZN2at6native29vectorized_elementwise_kernelILi4ENS0_13BinaryFunctorIbbbNS0_51_GLOBAL__N__28ce311f_18_CompareEQKernel_cu_d8008c9616CompareEqFunctorIbEEEENS_6detail5ArrayIPcLi3EEEEEviT0_T1_"
//void at::native::vectorized_elementwise_kernel<4, at::native::BinaryFunctor<bool, bool, bool, at::native::(anonymous namespace)::CompareEqFunctor<bool> >, at::detail::Array<char*, 3> >(int, at::native::BinaryFunctor<bool, bool, bool, at::native::(anonymous namespace)::CompareEqFunctor<bool> >, at::detail::Array<char*, 3>)int Kernel_COMPARE_EQ_F32_BINARY(void **args)
int Kernel_COMPARE_EQ_BOOL_BINARY(void **args)
{
    using FuncType = BinaryFunctor<bool, bool, bool, CompareEqFunctor<bool>>;

    int *_num = (int *)args[0];
    FuncType *bCompare = (FuncType*)args[1];
    at_detail_Array<char *, 3> *ary = (at_detail_Array<char *, 3> *)args[2];

    bool *dst = (bool *)ary->data[0];
    float *src = (float *)ary->data[1];
    float *src2  = (float *)ary->data[2];

    for (int i = 0; i < *_num; i++) {
        dst[i] = (*bCompare)(src[i], src2[i]);
    }

    RTPP_PRINT(">>> %s(), %d, %p, %p, %p, %d, %f, %f ... op=%d\n", __func__, *_num, dst, src, src2, dst[0], src[0], src2[0], (int)bCompare->f.op_);
    return 0;
}

#define CUDA_DIV_F32_Binary "_ZN2at6native29vectorized_elementwise_kernelILi4ENS0_13BinaryFunctorIfffNS0_15binary_internal10DivFunctorIfEEEENS_6detail5ArrayIPcLi3EEEEEviT0_T1_"
//void at::native::vectorized_elementwise_kernel<4, at::native::BinaryFunctor<float, float, float, at::native::binary_internal::DivFunctor<float> >, at::detail::Array<char*, 3> >(int, at::native::BinaryFunctor<float, float, float, at::native::binary_internal::DivFunctor<float> >, at::detail::Array<char*, 3>)
int Kernel_CUDA_DIV_F32_Binary(void **args)
{
    int *num = (int *)args[0];
    float **at_detail_array = (float **)args[2];

    float *dst = (float *)at_detail_array[0];
    float *src = (float *)at_detail_array[1];
    float *_2  = (float *)at_detail_array[2];
    
    for (int i = 0; i < *num; i++) {
        *dst = *src / (*_2);
        dst++;
        src++;
        _2++;
    }
    //printf(" %f, %f, %f\n", *_dst, *_src1, *_src2);
    printf(">>> %s(), num = %d, %f\n", __func__, *num, *dst);
    return 0;
}

#define CUDA_DIV_F32_UNARY "_ZN2at6native29vectorized_elementwise_kernelILi4ENS0_13BUnaryFunctorIfffNS0_15binary_internal10MulFunctorIfEEEENS_6detail5ArrayIPcLi2EEEEEviT0_T1_"
//void at::native::vectorized_elementwise_kernel<4, at::native::BUnaryFunctor<float, float, float, at::native::binary_internal::MulFunctor<float> >, at::detail::Array<char*, 2> >(int, at::native::BUnaryFunctor<float, float, float, at::native::binary_internal::MulFunctor<float> >, at::detail::Array<char*, 2>)
int Kernel_CUDA_DIV_F32_UNARY(void **args)
{
    using FuncType = BUnaryFunctor<float, float, float, MulFunctor<float> >;
    FuncType *mulfuctor = (FuncType*)args[1];

    int *num = (int *)args[0];
    float **at_detail_array = (float **)args[2];

    float *dst = (float *)at_detail_array[0];
    float *src = (float *)at_detail_array[1];
    
    
    for (int i = 0; i < *num; i++) {
        dst[i] = (*mulfuctor)(src[i]);
        
    }
    //printf(" %f, %f, %f\n", *_dst, *_src1, *_src2);
    printf(">>> %s(), num = %d, %f\n", __func__, *num, *dst);
    return 0;
}


template <typename scalar_t>
struct CUDAFunctor_add {
  using opmath_t = opmath_type<scalar_t>;
  opmath_t alpha_;
  CUDAFunctor_add(opmath_t alpha) : alpha_(alpha) {}
  __device__ scalar_t operator()(scalar_t self, scalar_t other) const {
    return cudaadd(static_cast<opmath_t>(self), static_cast<opmath_t>(other), alpha_);
  }
};

#define CUDA_ADD_F32 "_ZN2at6native29vectorized_elementwise_kernelILi4ENS0_15CUDAFunctor_addIfEENS_6detail5ArrayIPcLi3EEEEEviT0_T1_"
int Kernel_CUDA_ADD_F32(void **args)
{
/*
    template <typename scalar_t>
    struct CUDAFunctor_add {
    using opmath_t = at::opmath_type<scalar_t>;
    opmath_t alpha_;
    CUDAFunctor_add(opmath_t alpha) : alpha_(alpha) {}
    __device__ scalar_t operator()(scalar_t self, scalar_t other) const {
        return ufunc::add(static_cast<opmath_t>(self), static_cast<opmath_t>(other), alpha_);
    }
*/
    // float *_dst = (float *)args[0];
    // float *_src1 = (float *)args[1];
    // float *_src2 = (float *)args[2];

    using FuncType = CUDAFunctor_add<float>;

    int *num = (int *)args[0];
    FuncType *auadd = (FuncType*)args[1];
    float **at_detail_array = (float **)args[2];

    float *dst = (float *)at_detail_array[0];
    float *src = (float *)at_detail_array[1];
    float *_2  = (float *)at_detail_array[2];

    
    for (int i = 0; i < *num; i++) {
        dst[i] = (*auadd)(src[i], _2[i]);
        
    }
    //RTPP_PRINT(" %f, %f, %f\n", *_dst, *_src1, *_src2);
    RTPP_PRINT(">>> %s(), num = %d, %f\n", __func__, *num, *dst);
    return 0;
}


template <typename scalar_t, typename acc_t=scalar_t, typename factor_t=acc_t, typename out_t = acc_t>
struct MeanOps {
  factor_t factor;

  inline  acc_t reduce(acc_t a, scalar_t b, int64_t /*idx*/) const {
    return combine(a, static_cast<acc_t>(b));
  }

  inline  acc_t combine(acc_t a, acc_t b) const {
    return a + b;
  }

  inline  out_t project(acc_t a) const {
    return a * factor;
  }

  static  acc_t translate_idx(acc_t acc, int64_t /*base_idx*/) {
    return acc;
  }

#if defined(__CUDACC__) || defined(__HIPCC__)
  inline  acc_t warp_shfl_down(acc_t data, int offset) const {
    return WARP_SHFL_DOWN(data, offset);
  }
#endif

  MeanOps(factor_t factor): factor(factor) {
  }
};

#define REDUCE_MEAN_F32 "_ZN2at6native13reduce_kernelILi512ELi1ENS0_8ReduceOpIfNS0_7MeanOpsIffEEjfLi4EEEEEvT1_"
//void at::native::reduce_kernel<512, 1, at::native::ReduceOp<float, at::native::MeanOps<float, float>, unsigned int, float, 4> >(at::native::ReduceOp<float, at::native::MeanOps<float, float>, unsigned int, float, 4>)
int Kernel_REDUCE_MEAN_F32(void **args)
{
    using type1 = MeanOps<float, float>;

    using type2 = at_native_ReduceOp<float, type1, unsigned int, float, 4>;

    type2 *obj = (type2 *)args[0];
    type1 *fun = (type1 *)args[1];
    uint64_t addr = reinterpret_cast<uint64_t>(obj);

    RTPP_PRINT(">>> %s(), [%p/%p], %p, %p - %lu\n", 
        __func__, obj, &obj->src, obj->src, obj->dst[0], addr);
    

    void *dump = (void *)obj;
    uint64_t curr,src_curr,dst_curr;
    float **p,**src_p,**dst_p;

    // printf("args    = %p\n", args);
    // printf("args[0] = %p\n", args[0]);
    // printf("args[1] = %p\n", args[1]);
    // printf("args[2] = %p\n", args[2]);
    // for (int i = 0; i < 0x500; i += 8) {
    //     curr = addr + i;
    //     p = reinterpret_cast<float **>(curr);
    //     printf("[%4d] %18p : %18p\n", i, p, *p);
    // }

    //num
    curr = addr + 72;
    unsigned int *num = reinterpret_cast<unsigned int *>(curr);
    RTPP_PRINT("num = %d\n", num[1]);

    //dim2_num
    curr = addr + 16;
    unsigned int *dim2_num = reinterpret_cast<unsigned int *>(curr);
    RTPP_PRINT("dim2_num = %d\n", dim2_num[0]);

    //[1096] keep_dim

    // src
    src_curr = addr + 984;
    src_p = reinterpret_cast<float **>(src_curr);
    //printf("%f, %f, %f, %f\n", (*p)[0], (*p)[1], (*p)[2], (*p)[3]);
    dst_curr= addr + 992;
    dst_p = reinterpret_cast<float **>(dst_curr);
    for(int d=0;d<dim2_num[0];d++){
            float sum = 0.0;
            for (int i = d*num[1]; i < d*num[1]+num[1]; i++) {
                sum+=(*src_p)[i];

            }
            //printf("SSSS,%f\n",sum/(num[1]));
        (*dst_p)[d] = sum/(num[1]);

        }
    
        
    return 0;
}
#define POW_F32_Tinsley_EXP2 "_ZN2at6native29vectorized_elementwise_kernelILi4EZNS0_51_GLOBAL__N__b1108092_12_PowKernel_cu_6f8d5651_2468729pow_tensor_scalar_kernel_implIffEEvRNS_18TensorIteratorBaseET0_EUlfE_NS_6detail5ArrayIPcLi2EEEEEviS6_T1_"
//void at::native::vectorized_elementwise_kernel<4, at::native::(anonymous namespace)::pow_tensor_scalar_kernel_impl<float, float>(at::TensorIteratorBase&, float)::{lambda(float)#1}, at::detail::Array<char*, 2> >(int, at::native::(anonymous namespace)::pow_tensor_scalar_kernel_impl<float, float>(at::TensorIteratorBase&, float)::{lambda(float)#1}, at::detail::Array<char*, 2>)
int Kernel_POW_F32_Tinsley_EXP2(void **args)
{
    int *num = (int *)args[0];
    void *it = (void *)args[1];
    at_detail_Array<char *, 2> *ary = (at_detail_Array<char *, 2> *)args[2];

    float *dst = (float *)ary->data[0];
    float *src = (float *)ary->data[1];

    RTPP_PRINT(">>> %s(), num=%d, exp=%f, (args[1]=%p / ary[0]=%p)\n", __func__, *num, *src, args[1], ary->data[0]);

    // uint64_t curr;
    // float **p;
    // uint64_t addr = reinterpret_cast<uint64_t>(args[1]);
    // printf("args    = %p\n", args);
    // printf("args[0] = %p\n", args[0]);
    // printf("args[1] = %p\n", args[1]);
    // printf("args[2] = %p\n", args[2]);
    // for (int i = 0; i < 0x500; i += 8) {
    //     curr = addr + i;
    //     p = reinterpret_cast<float **>(curr);
    //     printf("[%4d] %18p : %18p\n", i, p, *p);
    // }
    for (int i = 0; i < *num; i++) {
        dst[i] = pow(src[i], 2);
    }

    // extern void *bw_TensorIteratorBase__data_ptr(void *it, int arg);
    // void *d = bw_TensorIteratorBase__data_ptr(it, 0);
    // printf("data[0] = %p\n", d);

    return 0;
}

#define POW_F32_Tinsley "_ZN2at6native29vectorized_elementwise_kernelILi4EZNS0_51_GLOBAL__N__b1108092_12_PowKernel_cu_6f8d5651_2468729pow_tensor_scalar_kernel_implIffEEvRNS_18TensorIteratorBaseET0_EUlfE2_NS_6detail5ArrayIPcLi2EEEEEviS6_T1_"
int Kernel_POW_F32_Tinsley(void **args)
{
    int *num = (int *)args[0];
    float *exp = (float *)args[1];
    at_detail_Array<char *, 2> *ary = (at_detail_Array<char *, 2> *)args[2];

    float *dst = (float *)ary->data[0];
    float *src = (float *)ary->data[1];

    RTPP_PRINT(">>> %s(), num=%d, exp=%f, (args[1]=%p / ary[0]=%p) [%f]\n", __func__, *num, *src, args[1], ary->data[0], *((float *)args[1]));

    for (int i = 0; i < *num; i++) {
        dst[i] = pow(src[i], *exp);
    }
    // uint64_t curr;
    // float **p;
    // uint64_t addr = reinterpret_cast<uint64_t>(args[1]);
    // printf("args    = %p\n", args);
    // printf("args[0] = %p\n", args[0]);
    // printf("args[1] = %p\n", args[1]);
    // printf("args[2] = %p\n", args[2]);
    // for (int i = 0; i < 0x500; i += 8) {
    //     curr = addr + i;
    //     p = reinterpret_cast<float **>(curr);
    //     printf("[%4d] %18p : %18p\n", i, p, *p);
    // }
    return 0;
}
#define MAX_TENSORINFO_DIMS 25

template <typename T, typename IndexType>
struct TensorInfo {
  TensorInfo();
  TensorInfo(T* p,
             int dim,
             IndexType sz[MAX_TENSORINFO_DIMS],
             IndexType st[MAX_TENSORINFO_DIMS]);

  T* data;
  IndexType sizes[MAX_TENSORINFO_DIMS];
  IndexType strides[MAX_TENSORINFO_DIMS];
  int dims;
};

template <typename T, typename IndexType, int Dims>
struct IndexToOffset {
  static __host__ __device__ IndexType get(
    IndexType linearId,
    const TensorInfo<T, IndexType>& info) {

    IndexType offset = 0;

    // Uses static dims
    for (int i = Dims - 1; i > 0; --i) {
      IndexType curDimIndex = linearId % info.sizes[i];
      IndexType curDimOffset = curDimIndex * info.strides[i];
      offset += curDimOffset;
      linearId /= info.sizes[i];
    }

    return offset + linearId * info.strides[0];
  }
};



#define NN_IndexSelectLarge "_ZN2at6native44_GLOBAL__N__50c743a2_11_Indexing_cu_89862edb21indexSelectLargeIndexIfljLi2ELi2ELin2ELb1EEEvNS_4cuda6detail10TensorInfoIT_T1_EES8_NS5_IT0_S7_EEiiS7_S7_l"
int Kernel_NN_IndexSelectLarge(void **args)
{
    TensorInfo<float, unsigned int> *dst = (TensorInfo<float, unsigned int> *)args[0];
    TensorInfo<float, unsigned int> *src = (TensorInfo<float, unsigned int> *)args[1];
    TensorInfo<long, unsigned int> *indices = (TensorInfo<long, unsigned int> *)args[2];
    int* dstSelectDim=(int *)args[3];
    int* srcSelectDim=(int *)args[4];
    unsigned int* totalSize=(unsigned int *)args[5];
    unsigned int* innerSize=(unsigned int *)args[6];

    for (unsigned int linearIndex = 0;
       linearIndex < *totalSize;
       linearIndex ++) {
    unsigned int dstIndex, elementInSlice;
    
    dstIndex = linearIndex / *innerSize;
    elementInSlice = linearIndex % *innerSize;
    

    unsigned int srcIndex =
        indices->data[IndexToOffset<long, unsigned int, -2>::get(dstIndex, *indices)];

    unsigned int dstOffset =
        IndexToOffset<float, unsigned int, 2>::get(elementInSlice, *dst);
    dstOffset += dstIndex * dst->strides[*dstSelectDim];

    unsigned int srcOffset =
        IndexToOffset<float, unsigned int, 2>::get(elementInSlice, *src);
    srcOffset += srcIndex * src->strides[*srcSelectDim];

    dst->data[dstOffset] = src->data[srcOffset];
  }
    printf("MY_BIG_NN_EMBEDDING\n");
    return 0;
}



#define NN_IndexSelectSmall "_ZN2at6native44_GLOBAL__N__50c743a2_11_Indexing_cu_89862edb21indexSelectSmallIndexIfljLi2ELi2ELin2EEEvNS_4cuda6detail10TensorInfoIT_T1_EES8_NS5_IT0_S7_EEiiS7_l"
int Kernel_NN_IndexSelectSmall(void **args)
{
    TensorInfo<float, unsigned int> *dst = (TensorInfo<float, unsigned int> *)args[0];
    TensorInfo<float, unsigned int> *src = (TensorInfo<float, unsigned int> *)args[1];
    TensorInfo<long, unsigned int> *indices = (TensorInfo<long, unsigned int> *)args[2];
    int* dstSelectDim=(int *)args[3];
    int* srcSelectDim=(int *)args[4];
    unsigned int* totalSize=(unsigned int *)args[5];
    unsigned int* innerSize=(unsigned int *)args[6];

    for (unsigned int dstIndex = 0; dstIndex < indices->sizes[0]; ++dstIndex) {
    unsigned int srcIndex =
      indices->data[IndexToOffset<long, unsigned int, -2>::get(dstIndex, *indices)];
   

    // We stride over the output ignoring the indexed dimension
    // (innerSize), whose offset calculation is handled differently
    for (unsigned int linearIndex = 0;
         linearIndex < *innerSize;
         linearIndex ++) {
      unsigned int dstOffset =
        IndexToOffset<float, unsigned int, 2>::get(linearIndex, *dst);
      dstOffset += dstIndex * dst->strides[*dstSelectDim];

      unsigned int srcOffset =
        IndexToOffset<float, unsigned int, 2>::get(linearIndex, *src);
      srcOffset += srcIndex * src->strides[*srcSelectDim];

      dst->data[dstOffset] = src->data[srcOffset];
    }
  }

    printf("MY_SMALL_NN_EMBEDDING\n");
    return 0;
}

//void (anonymous namespace)::elementwise_kernel_with_index<int, at::native::arange_cuda_out(c10::Scalar const&, c10::Scalar const&, c10::Scalar const&, at::Tensor&)::{lambda()#1}::operator()() const::{lambda()#4}::operator()() const::{lambda(long)#1}>
//(int, at::native::arange_cuda_out(c10::Scalar const&, c10::Scalar const&, c10::Scalar const&, at::Tensor&)::{lambda()#1}::operator()() const::{lambda()#4}::operator()() const::{lambda(long)#1}, 
//function_traits<at::native::arange_cuda_out(c10::Scalar const&, c10::Scalar const&, c10::Scalar const&, at::Tensor&)::{lambda()#1}::operator()() const::{lambda()#4}::operator()() const::{lambda(long)#1}>::result_type*)

typedef int (*KernelFunc_t)(void **args);
std::unordered_map<std::string, KernelFunc_t> budaFuncCallbackDB = {
    {SMAX_F32_MAKE(1),          Kernel_SMAX_F32},
    {SMAX_F32_MAKE(2),          Kernel_SMAX_F32},
    {SMAX_F32_MAKE(3),          Kernel_SMAX_F32},
    {SMAX_F32_MAKE(4),          Kernel_SMAX_F32},
    {SMAX_F32_MAKE(5),          Kernel_SMAX_F32},
    {SMAX_F32_MAKE(6),          Kernel_SMAX_F32},
    {SMAX_F32_MAKE(7),          Kernel_SMAX_F32},
    {SMAX_F32_MAKE(8),          Kernel_SMAX_F32},
    {SMAX_F32_MAKE(9),          Kernel_SMAX_F32},
    {SMAX_F32_MAKE(10),         Kernel_SMAX_F32},
    {VEC_F32_ABS,               Kernel_VEC_F32_ABS},
    {COMPARE_EQ_F32_UNARY,      Kernel_COMPARE_EQ_F32_UNARY},
    {COMPARE_EQ_F32_BINARY,     Kernel_COMPARE_EQ_F32_BINARY},
    {MUL_FUNC_BINARY,           Kernel_MUL_FUNC_BINARY},
    {VEC_BITWISE_AND_BINARY,    Kernel_VEC_BITWISE_AND_BINARY},
    {SMAX_F32_CUNN,             Kernel_SMAX_F32_CUNN},
    {CUDA_ADD_F32,              Kernel_CUDA_ADD_F32},
    {REDUCE_BOOL_POLICY600,     Kernel_REDUCE_BOOL_POLICY600},
    {POW_SCALAR_F32_LAMBDA1_A,  Kernel_POW_SCALAR_F32_LAMBDA1},
    {POW_SCALAR_F32_LAMBDA1,    Kernel_POW_SCALAR_F32_LAMBDA1},
    {POW_SCALAR_F32_LAMBDA2,    Kernel_POW_SCALAR_F32_LAMBDA2},
    {POW_SCALAR_F32_LAMBDA3,    Kernel_POW_SCALAR_F32_LAMBDA3},
    {POW_SCALAR_F32_LAMBDA4,    Kernel_POW_SCALAR_F32_LAMBDA4},
    {RSQRT_LAMBDA221_F32,       Kernel_RSQRT_LAMBDA221_F32},
    {REDUCE_MAX_F32,            Kernel_REDUCE_MAX_F32},
    {REDUCE_MIN_F32,            Kernel_REDUCE_MIN_F32},
    {MUL_FUNC_BROADCAST_F32,    Kernel_MUL_FUNC_BROADCAST_F32},
    //
    // TINSLEY
    //
    {CUDA_ADD_INPLACE_F32,      Kernel_CUDA_ADD_INPLACE_F32},
    {CUDA_MUL_F32_Binary,       Kernel_CUDA_MUL_F32_Binary},
    {CUDA_MUL_F32_UNARY,        Kernel_CUDA_MUL_F32_UNARY},
    {COMPARE_EQ_BOOL_UNARY,     Kernel_COMPARE_EQ_BOOL_UNARY},
    {CUDA_DIV_F32_Binary,       Kernel_CUDA_DIV_F32_Binary},
    {CUDA_DIV_F32_UNARY,        Kernel_CUDA_DIV_F32_UNARY},
    {COMPARE_EQ_BOOL_BINARY,    Kernel_COMPARE_EQ_BOOL_BINARY},
    {REDUCE_MEAN_F32,           Kernel_REDUCE_MEAN_F32},
    // {POW_F32_Tinsley_EXP2,      Kernel_POW_F32_Tinsley_EXP2},
    // {POW_F32_Tinsley,           Kernel_POW_F32_Tinsley},
    {NN_IndexSelectLarge,       Kernel_NN_IndexSelectLarge},
    {NN_IndexSelectSmall,       Kernel_NN_IndexSelectSmall},
    
};

struct FuncParam {
    unsigned int tid[3];
    unsigned int bid[3];
    unsigned int bDIm[3];
    unsigned int gDim[3];
    int wSize;
};
std::unordered_map<void *, std::string> budaFuncDB;
std::unordered_map<void *, struct FuncParam *> budaFuncParamDB;
char budaFuncStr[500];

void __budaRegisterFunctionImpl(const char *deviceName, const char *hostFun)
{
    std::string str(deviceName);

    std::size_t found = str.find("softmax_warp");

#if 0
    if (found != std::string::npos) {
#else
    if (true) {
#endif
        //RTPP_PRINT("%s\n", str.c_str());
        void *f = (void *)hostFun;
        budaFuncDB.emplace(f, deviceName); 
    }
}

void __budaRegisterFunctionParamImpl(const char *hostFun, struct FuncParam *param)
{
    struct FuncParam *pa = (struct FuncParam *)malloc(sizeof(struct FuncParam));
    *pa = *param;
    void *f = (void *)hostFun;
    budaFuncParamDB.emplace(f, pa); 
}

int budaLaunchKernelImpl(void *f, void **args)
{
    std::unordered_map<void *, std::string>::const_iterator got = budaFuncDB.find(f);
    std::unordered_map<void *, struct FuncParam *>::const_iterator para_it = budaFuncParamDB.find(f);
    // extern int bw_get_version();
    // printf("ver = %d\n", bw_get_version());
    if ( got == budaFuncDB.end() ) {
        // not found
        RTPP_PRINT("...... not found ......\n");
    } else {
        std::unordered_map<std::string, KernelFunc_t>::const_iterator it = budaFuncCallbackDB.find(got->second);
        if ( it == budaFuncCallbackDB.end() ) {
            RTPP_PRINT("...... NOT IMPLEMENT ......\n");
            RTPP_PRINT("%s\n", got->second.c_str());
        } else {
            // if (para_it != budaFuncParamDB.end()) {
            //     RTPP_PRINT("[PA] %u %u %u / %u %u %u / %u %u %u / %u %u %u / %d\n",
            //         para_it->second->tid[0], para_it->second->tid[1], para_it->second->tid[2],
            //         para_it->second->bid[0], para_it->second->bid[1], para_it->second->bid[2],
            //         para_it->second->bDIm[0], para_it->second->bDIm[1], para_it->second->bDIm[2],
            //         para_it->second->gDim[0], para_it->second->gDim[1], para_it->second->gDim[2],
            //         para_it->second->wSize);
            // }
            budaFuncCallbackDB[got->second](args);
        }
    }

    return 0;
}

char *BudaGetFunStringImpl(void *f)
{
    std::unordered_map<void *, std::string>::const_iterator got = budaFuncDB.find(f);

    if ( got == budaFuncDB.end() ) {
        strcpy(budaFuncStr, "hostfun not found ...");
    } else {
        strncpy(budaFuncStr, budaFuncDB[f].c_str(), 499);
    }

    
    return budaFuncStr;
}

void BudaUnregisterImpl()
{
    return;
    if (budaFuncParamDB.size() != 0) {
        for ( auto& x: budaFuncParamDB ) {
            free(x.second);
        }
        budaFuncParamDB.clear();
    }
}

void budaLaunchKernelParamImpl(unsigned int gridDim[3], unsigned int blockDim[3], size_t sharedMem, void *stream)
{
    g_CallConfig.importConfig(gridDim, blockDim, sharedMem, stream);
}

#ifdef __cplusplus
extern "C" {
#endif

void __budaRegisterFunction(const char *deviceName, const char *hostFun)
{
    //RTPP_PRINT("rtpp x\n");
    
    __budaRegisterFunctionImpl(deviceName, hostFun);
}

void __budaRegisterFunctionParam(const char *hostFun, struct FuncParam *param)
{
    __budaRegisterFunctionParamImpl(hostFun, param);
}

int budaLaunchKernel(const void *f, void **args)
{
    int ret = budaLaunchKernelImpl((void *)f, args);
    
    return ret;
}

void budaLaunchKernelParam(unsigned int gridDim[3], unsigned int blockDim[3], size_t sharedMem, void *stream)
{
    // RTPP_PRINT("<<< %u, %u, %u, ... %u, %u, %u ... %lu>>>\n", 
    //     gridDim[0],
    //     gridDim[1],
    //     gridDim[2],
    //     blockDim[0],
    //     blockDim[1],
    //     blockDim[2],
    //     sharedMem);
    budaLaunchKernelParamImpl(gridDim, blockDim, sharedMem, stream);
}

//
// Buda for internal function
//
char *BudaGetFunString(const void *f)
{
    //void *ff = (void *)f;
    return BudaGetFunStringImpl((void *)f);
}

void BudaUnregister()
{
    BudaUnregisterImpl();
}
#ifdef __cplusplus
}
#endif
