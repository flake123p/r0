#include <stddef.h>
#include <stdio.h>
#include <string.h>

#define __host__
#define __device__
#define __cudart_builtin__
#define CUDARTAPI
#define __dv(...) 
#define CUDART_CB

#include "meta/tcp_client_lt2.txt"

#define PRLOC fprintf(stdout, "[PR] %s(), %d\n", __func__, __LINE__);
#define ERRLOC fprintf(stderr, "[ERR] %s(), %d\n", __func__, __LINE__);

#define BLAS_PRLOC PRLOC
//#define BLAS_PRLOC PRLOC;TCP_FUNC()

#include <cuda.h>
#include <driver_types.h>
#include <surface_types.h>
#include <texture_types.h>
#include <vector_types.h>
#include <cublas.h>
#include <cublas_v2.h>
#include <cublasLt.h>
#include <float.h>
//#include <cublas_api.h>

//
// Replace part
//
extern "C" {


struct myblas {
    int mathMode;
    int streamId;
    int WorkspaceSize;
    void* WorkspaceAddress;

};

struct mystream {
    int streamId;

};

#define cublasCreate_v2__DEFINED
CUBLASAPI cublasStatus_t CUBLASWINAPI cublasCreate_v2(cublasHandle_t* handle) 
{
    BLAS_PRLOC;
    struct myblas *myb = (struct myblas *)malloc(sizeof(struct myblas));
    printf("handle = %p\n", handle);
    *handle = (cublasHandle_t)myb;
    return (cublasStatus_t)0;
}



#define cublasSetStream_v2__DEFINED
CUBLASAPI cublasStatus_t CUBLASWINAPI cublasSetStream_v2(cublasHandle_t handle, cudaStream_t streamId) 
{
    BLAS_PRLOC;
    struct myblas *myb = (struct myblas *)handle;
    struct mystream *mys = (struct mystream *)streamId;
    printf("handle = %p, stream = %p\n", handle, streamId);
    // myb->streamId=mys->streamId;
    // printf("myb_stream = %d\n",myb->streamId);
    return (cublasStatus_t)0;
}

#define cublasSetMathMode__DEFINED
CUBLASAPI cublasStatus_t CUBLASWINAPI cublasSetMathMode(cublasHandle_t handle, cublasMath_t mode) 
{
    BLAS_PRLOC;
    
    struct myblas *myb = (struct myblas *)handle;
    
    myb->mathMode=mode;
    printf("mode = %d\n",myb->mathMode);
    return (cublasStatus_t)0;
}

#define cublasSetWorkspace_v2__DEFINED
CUBLASAPI cublasStatus_t CUBLASWINAPI cublasSetWorkspace_v2(cublasHandle_t handle,
                                                            void* workspace,
                                                            size_t workspaceSizeInBytes) 
{
    BLAS_PRLOC;
    workspace=malloc((int)workspaceSizeInBytes);
    struct myblas *myb = (struct myblas *)handle;
    myb->WorkspaceAddress=workspace;
    printf("workspace address:%p\n",workspace);
    return (cublasStatus_t)0;
}
#define cublasSgemm_v2__DEFINED
CUBLASAPI cublasStatus_t CUBLASWINAPI cublasSgemm_v2(cublasHandle_t handle,
                                                     cublasOperation_t transa,
                                                     cublasOperation_t transb,
                                                     int m,
                                                     int n,
                                                     int k,
                                                     const float* alpha, /* host or device pointer */
                                                     const float* A,
                                                     int lda,
                                                     const float* B,
                                                     int ldb,
                                                     const float* beta, /* host or device pointer */
                                                     float* C,
                                                     int ldc) 
{
    BLAS_PRLOC;
    if(m<=0||n<=0||k<=0){
        return (cublasStatus_t) 1;
    }
    if(m*k==FLT_MAX||n*k==FLT_MAX||m*n==FLT_MAX){
        return (cublasStatus_t) 2;
    }
    int idt=0;
    float* t_B=(float*)malloc(sizeof(float)*n*k);
    for(int ida=0;ida<k;ida++){
        for(int idk=0;idk<n;idk++){
            t_B[idt]=B[m*idk+ida];
            //printf(" %d\n", m*idk+ida);
            //printf("t_A%d:%f\n",idt,t_A[idt]);
            idt++;
            
        }
    }
    idt=0;
    float* t_A=(float*)malloc(sizeof(float)*m*k);
    for(int ida=0;ida<m;ida++){
        for(int idk=0;idk<k;idk++){
            t_A[idt]=A[m*idk+ida];
            //printf(" %d\n", m*idk+ida);
            //printf("t_A%d:%f\n",idt,t_A[idt]);
            idt++;
            
        }
    }
    for(int idc=0;idc<m*n;idc++){
        int i=idc%m,j=idc/m;
        float sum=0.0;
        for(int idk=0;idk<k;idk++){
            if(transb==1&&transa==1) sum+=t_A[i+m*idk]*t_B[j*k+idk];
            if(transb==0&&transa==1) sum+=t_A[i+m*idk]*B[j*k+idk];
            if(transb==1&&transa==0) sum+=A[i+m*idk]*t_B[j*k+idk];
            if(transa==0&&transb==0) sum+=A[i+m*idk]*B[j*k+idk];
        }
        //printf("%dsum:%f\n",idc,sum);
        C[idc]=(*alpha)*sum+(*beta)*C[idc];
    }
    

    
    return (cublasStatus_t)0;
}
#include "meta/buda_blas_simple.txt"

} // extern "C"
