
import torch
import torch.nn as nn
import torch.testing as tt
torch.set_printoptions(precision=9, sci_mode=False)

# Define the input tensor
input_size = 4096
output_size = 256
batch_size = 4

# Create an instance of nn.Linear
linear_layer = nn.Linear(input_size, output_size)

# Initialize weight and bias with random values
nn.init.normal_(linear_layer.weight)  # Initialize weight with random values
nn.init.normal_(linear_layer.bias)    # Initialize bias with random values

# Generate random input data
input_data = torch.rand((batch_size, input_size))

# Print the shapes of input, weight, and bias
print(f">>>>>> Input Shape: {input_data.shape}")
print(f">>>>>> Weight Shape: {linear_layer.weight.shape}")
print(f">>>>>> Bias Shape: {linear_layer.bias.shape}")

# Perform the forward pass through the linear layer
output = linear_layer(input_data)

# Print the shape of the output
print(f">>>>>> Output Shape: {output.shape}", '\n')

# You can print the weight and bias as well
weight = linear_layer.weight
bias = linear_layer.bias
print('>>>>>> Weight:', weight, weight.shape )
print('>>>>>> Bias:', bias, bias.shape)