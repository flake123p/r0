
import torch
import torch.nn as nn
import torch.testing as tt
torch.set_printoptions(precision=9, sci_mode=False)
print('111')
# Check if CUDA (GPU) is available
#device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
device = torch.device("cpu")
# Define the input tensor
input_size = 4096*4096
output_size = 256
batch_size = 4
print('222')
# Create an instance of nn.Linear on the GPU
linear_layer = nn.Linear(input_size, output_size).to(device)
print('333')
# Initialize weight and bias with random values on the GPU
nn.init.normal_(linear_layer.weight)
nn.init.normal_(linear_layer.bias)
print('444')
# Generate random input data on the GPU
input_data = torch.rand((batch_size, input_size)).to(device)

# Print the shapes of input, weight, and bias
print(f"Input Shape: {input_data.shape}")
print(f"Weight Shape: {linear_layer.weight.shape}")
print(f"Bias Shape: {linear_layer.bias.shape}")
print('555')
# Perform the forward pass through the linear layer
output = linear_layer(input_data).cpu()

# Print the shape of the output
print(f"Output Shape: {output.shape}", '\n')

# You can print the weight and bias as well
print('Weight:', linear_layer.weight)
print('Bias:', linear_layer.bias)