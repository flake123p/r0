#
# https://stackoverflow.com/questions/52874575/subprocess-echo-environment-var-outputs-environment-var
#
import os
import subprocess
from subprocess import Popen
from os import environ
import math

args_base = ['python', 'p2.py']
HIJACK_RUN =True
GPU_RUN =False
CPU_RUN =True
Hijack_List = []
Gpu_List = []
Cpu_List = []

def dump_log_f32(log):
    dump_flag = False
    float_capture_flag = False
    out_list = []
    for i in log:
        if i == 'LOG_START':
            dump_flag = True
            continue
        elif i == 'LOG_END':
            dump_flag = False
        if dump_flag == True:
            print(i)
        if i == 'RESULT_START':
            float_capture_flag = True
            continue
        elif i == 'RESULT_END':
            float_capture_flag = False
            continue
        if float_capture_flag == True:
            #print(float(i))
            out_list.append(float(i))
    return out_list

#
# Hijack Run
#
if HIJACK_RUN:
    env = dict(os.environ)
    env['LD_PRELOAD'] = './lib/libcuda.so.1:./lib/libcudart.so.11.0:./lib/libcublas.so.11:./lib/libcublasLt.so.11:./lib/libcudartpp.so:./lib/libfx3_rw.so'
    env['LD_LIBRARY_PATH'] = './lib'
    args = args_base.copy()
    args.append('pure_hijack')
    #myproc = Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=env, shell=True).stdout

    #p = subprocess.run(args, stderr=subprocess.PIPE, env=env).stderr.decode('utf-8')
    p = subprocess.run(args, stdout=subprocess.PIPE, env=env).stdout.decode('utf-8').split('\n')

    #print(p)
    Hijack_List = dump_log_f32(p)

#
# GPU Run
#
if GPU_RUN:
    env = dict(os.environ)
    env['LD_PRELOAD'] = ''
    env['LD_LIBRARY_PATH'] = ''
    args = args_base.copy()
    args.append('pure_gpu')
    #myproc = Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=env, shell=True).stdout

    #p = subprocess.run(args, stderr=subprocess.PIPE, env=env).stderr.decode('utf-8')
    p = subprocess.run(args, stdout=subprocess.PIPE, env=env).stdout.decode('utf-8').split('\n')

    #print(p)
    Gpu_List = dump_log_f32(p)

#
# CPU Run
#
if CPU_RUN:
    env = dict(os.environ)
    env['LD_PRELOAD'] = ''
    env['LD_LIBRARY_PATH'] = ''
    args = args_base.copy()
    args.append('pure_cpu')
    #myproc = Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=env, shell=True).stdout

    #p = subprocess.run(args, stderr=subprocess.PIPE, env=env).stderr.decode('utf-8')
    p = subprocess.run(args, stdout=subprocess.PIPE, env=env).stdout.decode('utf-8').split('\n')

    #print(p)
    Cpu_List = dump_log_f32(p)

if len(Hijack_List) == len(Cpu_List):
    for i in range(len(Hijack_List)):
        same = Hijack_List[i] == Cpu_List[i]
        OneToPow3 = math.fabs(Hijack_List[i] - Cpu_List[i]) < 1e-3
        OneToPow4 = math.fabs(Hijack_List[i] - Cpu_List[i]) < 1e-4
        OneToPow5 = math.fabs(Hijack_List[i] - Cpu_List[i]) < 1e-5
        OneToPow6 = math.fabs(Hijack_List[i] - Cpu_List[i]) < 1e-6
        OneToPow7 = math.fabs(Hijack_List[i] - Cpu_List[i]) < 1e-7
        OneToPow8 = math.fabs(Hijack_List[i] - Cpu_List[i]) < 1e-8
        OneToPow9 = math.fabs(Hijack_List[i] - Cpu_List[i]) < 1e-9
        precision_cmp = ""
        if not same:
            precision_cmp = \
                ", <1e-3=" + str(int(OneToPow3)) + \
                ", <1e-4=" + str(int(OneToPow4)) + \
                ", <1e-5=" + str(int(OneToPow5)) + \
                ", <1e-6=" + str(int(OneToPow6)) + \
                ", <1e-7=" + str(int(OneToPow7)) + \
                ", <1e-8=" + str(int(OneToPow8)) + \
                ", <1e-9=" + str(int(OneToPow9))
        print(
            "{:.12f}".format(Hijack_List[i]), 
            "{:.12f}".format(Cpu_List[i]), 
            "SAME =", int(same), 
            precision_cmp)