#include <stddef.h>
#include <stdio.h>
#include <string.h>

#define __host__
#define __device__
#define __cudart_builtin__
#define CUDARTAPI
#define __dv(...) 
#define CUDART_CB

#define PRLOC fprintf(stdout, "[PR] %s(), %d\n", __func__, __LINE__);
#define ERRLOC fprintf(stderr, "[ERR] %s(), %d\n", __func__, __LINE__);

#define DRV_PRLOC PRLOC

#include <cuda.h>
#include <driver_types.h>
#include <surface_types.h>
#include <texture_types.h>
#include <vector_types.h>

//
// Replace part
//
#define cuGetErrorString__DEFINED
CUresult CUDAAPI cuGetErrorString(CUresult error, const char **pStr){
    DRV_PRLOC;
    *pStr = "BUDA";
    return 0;
}

#define cuGetErrorName__DEFINED
CUresult CUDAAPI cuGetErrorName(CUresult error, const char **pStr){
    DRV_PRLOC;
    *pStr = "BUDA";
    return 0;
}

#define cuGetProcAddress__DEFINED
CUresult CUDAAPI cuGetProcAddress(const char *symbol, void **pfn, int cudaVersion, cuuint64_t flags){
    DRV_PRLOC;
    *pfn = NULL;
    printf("symbol = %s, cudaVersion = %d, flags = %lu\n", symbol, cudaVersion, flags);
    return 0;
}

#define cuDriverGetVersion__DEFINED
CUresult CUDAAPI cuDriverGetVersion(int *driverVersion){
    *driverVersion = 11030;
    DRV_PRLOC;
    return 0;
}

#define cuGetExportTable__DEFINED
typedef int (*ExportedFunction)();
int dummy0() {
  DRV_PRLOC;
  return 0;
}
static ExportedFunction exportTable[3] = {&dummy0, &dummy0, &dummy0};
CUresult CUDAAPI cuGetExportTable(const void **ppExportTable, const CUuuid *pExportTableId){
    *ppExportTable = &exportTable;
    for (int i = 0; i < 16; i++) {
        printf("%02X ", (unsigned char)pExportTableId->bytes[i]);
    }
    printf("\n");
    DRV_PRLOC;
    return 0;
}

#define cuDeviceGetCount__DEFINED
CUresult CUDAAPI cuDeviceGetCount(int *count){
    *count = 1;
    DRV_PRLOC;
    return 0;
}

#define cuDeviceGet__DEFINED
CUresult CUDAAPI cuDeviceGet(CUdevice *device, int ordinal){
    printf("ordinal = %d\n", ordinal);
    *device = 0;
    DRV_PRLOC;
    return 0;
}

#define cuDeviceGetName__DEFINED
CUresult CUDAAPI cuDeviceGetName(char *name, int len, CUdevice dev){
    char _name[] = "NVIDIA GeForce GTX 1060 3GB";
    strncpy(name, _name, len);
    printf("name = %s\n", name);
    DRV_PRLOC;
    return 0;
}

#undef __CUDA_DEPRECATED
#define __CUDA_DEPRECATED
#include "meta/buda_drv_define.txt"
#include "meta/buda_drv_simple.txt"
