
import torch
import torch.nn as nn
torch.set_printoptions(precision=9, sci_mode=False)

# def run_cmd_to_buf(cmd):
#     import subprocess
#     return subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')

#result = run_cmd_to_buf(['export', 'LD_PRELOAD=./lib/libcuda.so.1:./lib/libcudart.so.11.0:./lib/libcublas.so.11:./lib/libcublasLt.so.11:./lib/libcudartpp.so:./lib/libfx3_rw.so'])
#result = run_cmd_to_buf(['export', 'LD_LIBRARY_PATH=./lib'])
#result = run_cmd_to_buf(['ls', '-al'])

# for each_line in run_cmd_to_buf(['ls']):
#     seach_line = each_line.split()
#     print(seach_line)
#     for each_word in seach_line:
#         #print(each_word)
#         pass

input_tensor = torch.tensor([2.0, -1.0, 1.0]).to('cuda')
output = torch.nn.functional.softmax(input_tensor, dim=-1)
print(output)