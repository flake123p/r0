
import torch
import torch.nn as nn
torch.set_printoptions(precision=9, sci_mode=False)

import sys

# print('Number of arguments:', len(sys.argv), 'arguments.')
# print ('Argument List:', str(sys.argv))

pure_cpu = False
if len(sys.argv) == 2:
    if sys.argv[1] == 'pure_cpu':
        pure_cpu = True
else:
    exit()

input_tensor = torch.tensor([2.0, -1.0, 1.0])
if pure_cpu:
    input_tensor = input_tensor.cpu()
else:
    input_tensor = input_tensor.cuda()
output = torch.nn.functional.softmax(input_tensor, dim=-1)
output = output.cpu()


print('LOG_START')
print(str(sys.argv[1]))
print(output) 
print('LOG_END')
print('RESULT_START')
for o in output:
    print("{:.40f}".format(o.item()))
print('RESULT_END')
# print(output[1])
# print(1.2, 3,4)
# print("{:.40f}".format(output[0].item()))