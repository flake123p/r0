import torch

print(torch.cuda.is_available())

gpu = torch.device("cuda:0")

with torch.inference_mode():
    a = torch.tensor([31.0], device=gpu)
    b = torch.tensor([22.0], device=gpu)
    c = b + a
    print(c)
    quit()
import torch

print('1')
print('2')
print(torch.cuda.is_available())
print('3')

device = torch.device("cuda:0")

a = torch.tensor([31.0], device=device)
print('$ a tensor to GPU Done')
b = torch.tensor([22.0], device=device)
print('$ b tensor to GPU Done')

if 1:
    # gpu_add = a + b ########################## This is def 1
    # c = gpu_add.to('cpu')
    # print(c)
    c = b + a   ########################## This is def 0
    print(c)
else:
    b = a.clone()
    c = b.to('cpu')
    print(c)
'''
cu list (27) =
    cuGetProcAddress
    cuDriverGetVersion
    cuInit
    cuGetExportTable
    cuModuleGetLoadingMode
    cuDeviceGetCount
    cuDeviceGet
    cuDeviceGetName
    cuDeviceTotalMem_v2
    cuDeviceGetAttribute
    cuDeviceGetUuid
    cuCtxGetDevice
    cuDevicePrimaryCtxGetState
    cuCtxGetCurrent
    cuCtxSetCurrent
    cuDevicePrimaryCtxRetain
    cuModuleGetFunction
    cuModuleGetGlobal_v2
    cuStreamIsCapturing
    cuMemAlloc_v2
    cuMemcpyHtoDAsync_v2
    cuStreamSynchronize
    cuLaunchKernel
    cuFuncGetAttribute
    cuMemcpyDtoHAsync_v2
    cuModuleUnload
    cuDevicePrimaryCtxRelease

cuda list (12) =
    cudaGetDeviceCount
    cudaGetDevice
    cudaGetDeviceProperties
    cudaGetLastError
    cudaStreamIsCapturing
    cudaMalloc
    cudaMemcpyAsync
    cudaStreamSynchronize
    cudaLaunchKernel
    cudaFuncGetAttributes
    cudaPeekAtLastError
    cudaDeviceGetAttribute
    

Add func:
    cublasGemmStridedBatchedEx()
    
    cublasLtMatrixTransform()
    
    _ZN2at6native29vectorized_elementwise_kernelILi4ENS0_15CUDAFunctor_addIfEENS_6detail5ArrayIPcLi3EEEEEviT0_T1_()
'''