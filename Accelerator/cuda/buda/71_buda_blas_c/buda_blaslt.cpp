#include <stddef.h>
#include <stdio.h>
#include <string.h>

#define __host__
#define __device__
#define __cudart_builtin__
#define CUDARTAPI
#define __dv(...) 
#define CUDART_CB

#define PRLOC fprintf(stdout, "[PR] %s(), %d\n", __func__, __LINE__);
#define ERRLOC fprintf(stderr, "[ERR] %s(), %d\n", __func__, __LINE__);

#define BLAS_PRLOC PRLOC

#include <cuda.h>
#include <driver_types.h>
#include <surface_types.h>
#include <texture_types.h>
#include <vector_types.h>
#include <cublasLt.h>

//
// Replace part
//
extern "C" {
cublasStatus_t CUBLASWINAPI cublasLtCreate(cublasLtHandle_t* lightHandle)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtDestroy(cublasLtHandle_t lightHandle)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

const char* CUBLASWINAPI cublasLtGetStatusName(cublasStatus_t status)
{
    BLAS_PRLOC;
    return "BLASLT_DUMMY";
}

const char* CUBLASWINAPI cublasLtGetStatusString(cublasStatus_t status)
{
    BLAS_PRLOC;
    return "BLASLT_DUMMY";
}


size_t CUBLASWINAPI cublasLtGetVersion(void)
{
    BLAS_PRLOC;
    return (size_t)0;
}

size_t CUBLASWINAPI cublasLtGetCudartVersion(void)
{
    BLAS_PRLOC;
    return (size_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtGetProperty(libraryPropertyType type, int* value)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatmul(cublasLtHandle_t lightHandle,
                                           cublasLtMatmulDesc_t computeDesc,
                                           const void* alpha, /* host or device pointer */
                                           const void* A,
                                           cublasLtMatrixLayout_t Adesc,
                                           const void* B,
                                           cublasLtMatrixLayout_t Bdesc,
                                           const void* beta, /* host or device pointer */
                                           const void* C,
                                           cublasLtMatrixLayout_t Cdesc,
                                           void* D,
                                           cublasLtMatrixLayout_t Ddesc,
                                           const cublasLtMatmulAlgo_t* algo,
                                           void* workspace,
                                           size_t workspaceSizeInBytes,
                                           cudaStream_t stream)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatrixTransform(cublasLtHandle_t lightHandle,
                                                    cublasLtMatrixTransformDesc_t transformDesc,
                                                    const void* alpha, /* host or device pointer */
                                                    const void* A,
                                                    cublasLtMatrixLayout_t Adesc,
                                                    const void* beta, /* host or device pointer */
                                                    const void* B,
                                                    cublasLtMatrixLayout_t Bdesc,
                                                    void* C,
                                                    cublasLtMatrixLayout_t Cdesc,
                                                    cudaStream_t stream)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatrixLayoutInit_internal(  //
    cublasLtMatrixLayout_t matLayout,
    size_t size,
    cudaDataType type,
    uint64_t rows,
    uint64_t cols,
    int64_t ld)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatrixLayoutCreate(  //
    cublasLtMatrixLayout_t* matLayout,
    cudaDataType type,
    uint64_t rows,
    uint64_t cols,
    int64_t ld)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatrixLayoutDestroy(cublasLtMatrixLayout_t matLayout)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatrixLayoutSetAttribute(  //
    cublasLtMatrixLayout_t matLayout,
    cublasLtMatrixLayoutAttribute_t attr,
    const void* buf,
    size_t sizeInBytes)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatrixLayoutGetAttribute(  //
    cublasLtMatrixLayout_t matLayout,
    cublasLtMatrixLayoutAttribute_t attr,
    void* buf,
    size_t sizeInBytes,
    size_t* sizeWritten)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatmulDescInit_internal(  //
    cublasLtMatmulDesc_t matmulDesc,
    size_t size,
    cublasComputeType_t computeType,
    cudaDataType_t scaleType)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatmulDescCreate(cublasLtMatmulDesc_t* matmulDesc,
                                                     cublasComputeType_t computeType,
                                                     cudaDataType_t scaleType)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatmulDescDestroy(cublasLtMatmulDesc_t matmulDesc)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatmulDescSetAttribute(  //
    cublasLtMatmulDesc_t matmulDesc,
    cublasLtMatmulDescAttributes_t attr,
    const void* buf,
    size_t sizeInBytes)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatmulDescGetAttribute(  //
    cublasLtMatmulDesc_t matmulDesc,
    cublasLtMatmulDescAttributes_t attr,
    void* buf,
    size_t sizeInBytes,
    size_t* sizeWritten)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatrixTransformDescInit_internal(cublasLtMatrixTransformDesc_t transformDesc,
                                                                     size_t size,
                                                                     cudaDataType scaleType)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatrixTransformDescCreate(cublasLtMatrixTransformDesc_t* transformDesc,
                                                              cudaDataType scaleType)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatrixTransformDescDestroy(cublasLtMatrixTransformDesc_t transformDesc)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatrixTransformDescSetAttribute(  //
    cublasLtMatrixTransformDesc_t transformDesc,
    cublasLtMatrixTransformDescAttributes_t attr,
    const void* buf,
    size_t sizeInBytes)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatrixTransformDescGetAttribute(  //
    cublasLtMatrixTransformDesc_t transformDesc,
    cublasLtMatrixTransformDescAttributes_t attr,
    void* buf,
    size_t sizeInBytes,
    size_t* sizeWritten)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatmulPreferenceInit_internal(cublasLtMatmulPreference_t pref, size_t size)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatmulPreferenceCreate(cublasLtMatmulPreference_t* pref)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatmulPreferenceDestroy(cublasLtMatmulPreference_t pref)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatmulPreferenceSetAttribute(  //
    cublasLtMatmulPreference_t pref,
    cublasLtMatmulPreferenceAttributes_t attr,
    const void* buf,
    size_t sizeInBytes)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatmulPreferenceGetAttribute(  //
    cublasLtMatmulPreference_t pref,
    cublasLtMatmulPreferenceAttributes_t attr,
    void* buf,
    size_t sizeInBytes,
    size_t* sizeWritten)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatmulAlgoGetHeuristic(cublasLtHandle_t lightHandle,
                                                           cublasLtMatmulDesc_t operationDesc,
                                                           cublasLtMatrixLayout_t Adesc,
                                                           cublasLtMatrixLayout_t Bdesc,
                                                           cublasLtMatrixLayout_t Cdesc,
                                                           cublasLtMatrixLayout_t Ddesc,
                                                           cublasLtMatmulPreference_t preference,
                                                           int requestedAlgoCount,
                                                           cublasLtMatmulHeuristicResult_t heuristicResultsArray[],
                                                           int* returnAlgoCount)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatmulAlgoGetIds(cublasLtHandle_t lightHandle,
                                                     cublasComputeType_t computeType,
                                                     cudaDataType_t scaleType,
                                                     cudaDataType_t Atype,
                                                     cudaDataType_t Btype,
                                                     cudaDataType_t Ctype,
                                                     cudaDataType_t Dtype,
                                                     int requestedAlgoCount,
                                                     int algoIdsArray[],
                                                     int* returnAlgoCount)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatmulAlgoInit(cublasLtHandle_t lightHandle,
                                                   cublasComputeType_t computeType,
                                                   cudaDataType_t scaleType,
                                                   cudaDataType_t Atype,
                                                   cudaDataType_t Btype,
                                                   cudaDataType_t Ctype,
                                                   cudaDataType_t Dtype,
                                                   int algoId,
                                                   cublasLtMatmulAlgo_t* algo)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatmulAlgoCheck(  //
    cublasLtHandle_t lightHandle,
    cublasLtMatmulDesc_t operationDesc,
    cublasLtMatrixLayout_t Adesc,
    cublasLtMatrixLayout_t Bdesc,
    cublasLtMatrixLayout_t Cdesc,
    cublasLtMatrixLayout_t Ddesc,
    const cublasLtMatmulAlgo_t* algo,  ///< may point to result->algo
    cublasLtMatmulHeuristicResult_t* result)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatmulAlgoCapGetAttribute(const cublasLtMatmulAlgo_t* algo,
                                                              cublasLtMatmulAlgoCapAttributes_t attr,
                                                              void* buf,
                                                              size_t sizeInBytes,
                                                              size_t* sizeWritten)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatmulAlgoConfigSetAttribute(cublasLtMatmulAlgo_t* algo,
                                                                 cublasLtMatmulAlgoConfigAttributes_t attr,
                                                                 const void* buf,
                                                                 size_t sizeInBytes)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtMatmulAlgoConfigGetAttribute(const cublasLtMatmulAlgo_t* algo,
                                                                 cublasLtMatmulAlgoConfigAttributes_t attr,
                                                                 void* buf,
                                                                 size_t sizeInBytes,
                                                                 size_t* sizeWritten)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtLoggerSetCallback(cublasLtLoggerCallback_t callback)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtLoggerSetFile(FILE* file)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtLoggerOpenFile(const char* logFile)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtLoggerSetLevel(int level)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtLoggerSetMask(int mask)
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

cublasStatus_t CUBLASWINAPI cublasLtLoggerForceDisable()
{
    BLAS_PRLOC;
    return (cublasStatus_t)0;
}

} //extern "C"