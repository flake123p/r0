==220455== NVPROF is profiling process 220455, command: python pth.py
==220455== Profiling application: python pth.py
==220455== Profiling result:
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:    9.87%  6.7850us         1  6.7850us  6.7850us  6.7850us  void at::native::reduce_kernel<int=512, int=1, at::native::ReduceOp<double, at::native::func_wrapper_t<double, at::native::MaxNanFunctor<double>>, unsigned int, double, int=4>>(double)
                    9.22%  6.3370us         1  6.3370us  6.3370us  6.3370us  void at::native::reduce_kernel<int=512, int=1, at::native::ReduceOp<double, at::native::func_wrapper_t<double, at::native::MinNanFunctor<double>>, unsigned int, double, int=4>>(double)
                    7.41%  5.0890us         2  2.5440us  2.4320us  2.6570us  void at::native::vectorized_elementwise_kernel<int=4, at::native::AUnaryFunctor<float, float, bool, at::native::_GLOBAL__N__28ce311f_18_CompareEQKernel_cu_d8008c96::CompareEqFunctor<float>>, at::detail::Array<char*, int=2>>(int, float, float)
                    7.31%  5.0240us         2  2.5120us  2.4960us  2.5280us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<float, float, bool, at::native::_GLOBAL__N__28ce311f_18_CompareEQKernel_cu_d8008c96::CompareEqFunctor<float>>, at::detail::Array<char*, int=3>>(int, float, float)
                    6.99%  4.8010us         2  2.4000us  2.1440us  2.6570us  void at::native::vectorized_elementwise_kernel<int=4, at::native::AbsFunctor<float>, at::detail::Array<char*, int=2>>(int, float, at::native::AbsFunctor<float>)
                    6.90%  4.7380us         2  2.3690us  1.7290us  3.0090us  _ZN2at6native29vectorized_elementwise_kernelILi4EZNS0_21compare_scalar_kernelIdEEvRNS_18TensorIteratorBaseENS0_50_GLOBAL__N__e50ef81d_17_CompareKernels_cu_8f1b29aa6OpTypeET_EUldE_NS_6detail5ArrayIPcLi2EEEEEviT0_T1_
                    6.66%  4.5770us         6     762ns     640ns  1.0240us  [CUDA memcpy DtoH]
                    6.01%  4.1290us         1  4.1290us  4.1290us  4.1290us  _ZN2at6native24index_elementwise_kernelILi128ELi4EZNS0_16gpu_index_kernelIZNS0_17index_kernel_implINS0_10OpaqueTypeILi4EEEEEvRNS_18TensorIteratorBaseEN3c108ArrayRefIlEESA_EUlPcSB_lE_EEvS7_SA_SA_RKT_EUliE_EEviT1_
                    5.22%  3.5840us         1  3.5840us  3.5840us  3.5840us  _ZN2at6native27unrolled_elementwise_kernelIZZZNS0_23direct_copy_kernel_cudaERNS_18TensorIteratorBaseEENKUlvE0_clEvENKUlvE4_clEvEUldE_NS_6detail5ArrayIPcLi2EEE23TrivialOffsetCalculatorILi1EjESC_NS0_6memory12LoadWithCastILi1EEENSD_13StoreWithCastILi1EEEEEviT_T0_T1_T2_T3_T4_
                    4.89%  3.3600us         1  3.3600us  3.3600us  3.3600us  void at_cuda_detail::cub::DeviceSelectSweepKernel<at_cuda_detail::cub::DispatchSelectIf<at_cuda_detail::cub::CountingInputIterator<long, long>, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool*, long>, long*, int*, at_cuda_detail::cub::NullType, at_cuda_detail::cub::NullType, int, bool=0>::PtxSelectIfPolicyT, at_cuda_detail::cub::CountingInputIterator<long, long>, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool*, long>, long*, int*, at_cuda_detail::cub::ScanTileState<int, bool=1>, at_cuda_detail::cub::NullType, at_cuda_detail::cub::NullType, int, bool=0>(long, at_cuda_detail::cub::CountingInputIterator<long, long>, bool, bool, at::native::_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool*, long, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool*, long>, int)
                    4.89%  3.3600us         1  3.3600us  3.3600us  3.3600us  void at_cuda_detail::cub::DeviceReduceSingleTileKernel<at_cuda_detail::cub::DeviceReducePolicy<bool, int, int, at_cuda_detail::cub::Sum>::Policy600, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool*, long>, int*, int, at_cuda_detail::cub::Sum, int>(int, int, at_cuda_detail::cub::Sum, at_cuda_detail::cub::DeviceReducePolicy<bool, int, int, at_cuda_detail::cub::Sum>::Policy600, bool)
                    4.80%  3.2960us         1  3.2960us  3.2960us  3.2960us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<double, double, double, at::native::binary_internal::DivFunctor<double>>, at::detail::Array<char*, int=3>>(int, double, double)
                    4.38%  3.0080us         1  3.0080us  3.0080us  3.0080us  void at::native::vectorized_elementwise_kernel<int=4, at::native::CUDAFunctor_add<float>, at::detail::Array<char*, int=3>>(int, float, at::native::CUDAFunctor_add<float>)
                    3.73%  2.5600us         1  2.5600us  2.5600us  2.5600us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<bool, bool, bool, at::native::BitwiseAndFunctor<bool>>, at::detail::Array<char*, int=3>>(int, bool, bool)
                    3.68%  2.5280us         1  2.5280us  2.5280us  2.5280us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<bool, bool, bool, at::native::binary_internal::MulFunctor<bool>>, at::detail::Array<char*, int=3>>(int, bool, bool)
                    3.12%  2.1440us         1  2.1440us  2.1440us  2.1440us  _ZN2at6native29vectorized_elementwise_kernelILi4EZZZNS0_16ceil_kernel_cudaERNS_18TensorIteratorBaseEENKUlvE_clEvENKUlvE0_clEvEUlfE_NS_6detail5ArrayIPcLi2EEEEEviT0_T1_
                    2.66%  1.8250us         1  1.8250us  1.8250us  1.8250us  void at_cuda_detail::cub::DeviceCompactInitKernel<at_cuda_detail::cub::ScanTileState<int, bool=1>, int*>(int, int, bool=1)
                    2.28%  1.5680us         2     784ns     576ns     992ns  [CUDA memcpy HtoD]
      API calls:   99.91%  932.42ms         1  932.42ms  932.42ms  932.42ms  cudaStreamIsCapturing
                    0.02%  190.87us        20  9.5430us  4.6720us  33.159us  cudaLaunchKernel
                    0.02%  179.18us         8  22.397us  5.7220us  108.99us  cudaMemcpyAsync
                    0.01%  127.17us         1  127.17us  127.17us  127.17us  cudaMalloc
                    0.01%  107.43us         8  13.428us  1.3770us  90.000us  cudaStreamSynchronize
                    0.01%  71.997us       101     712ns     114ns  29.812us  cuDeviceGetAttribute
                    0.01%  60.807us       191     318ns     226ns  3.9600us  cudaGetDevice
                    0.01%  54.543us         1  54.543us  54.543us  54.543us  cudaGetDeviceProperties
                    0.00%  18.035us        49     368ns     112ns  10.200us  cudaGetLastError
                    0.00%  14.243us         1  14.243us  14.243us  14.243us  cuDeviceGetName
                    0.00%  9.3880us         1  9.3880us  9.3880us  9.3880us  cudaFuncGetAttributes
                    0.00%  5.5440us         1  5.5440us  5.5440us  5.5440us  cuDeviceGetPCIBusId
                    0.00%  4.8640us         4  1.2160us     114ns  4.1010us  cudaGetDeviceCount
                    0.00%  1.0770us         1  1.0770us  1.0770us  1.0770us  cudaDeviceGetAttribute
                    0.00%     973ns         6     162ns     114ns     213ns  cudaPeekAtLastError
                    0.00%     898ns         3     299ns     157ns     563ns  cuDeviceGetCount
                    0.00%     768ns         3     256ns     136ns     482ns  cuDevicePrimaryCtxGetState
                    0.00%     633ns         1     633ns     633ns     633ns  cuDeviceTotalMem
                    0.00%     501ns         2     250ns     112ns     389ns  cuDeviceGet
                    0.00%     230ns         1     230ns     230ns     230ns     �
                    0.00%     227ns         1     227ns     227ns     227ns  cuDeviceGetUuid
