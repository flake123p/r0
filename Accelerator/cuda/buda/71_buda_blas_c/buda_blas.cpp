#include <stddef.h>
#include <stdio.h>
#include <string.h>

#define __host__
#define __device__
#define __cudart_builtin__
#define CUDARTAPI
#define __dv(...) 
#define CUDART_CB

#include "../include/tcp_client_lt2.txt"

#define PRLOC fprintf(stdout, "[PR] %s(), %d\n", __func__, __LINE__);
#define ERRLOC fprintf(stderr, "[ERR] %s(), %d\n", __func__, __LINE__);

//#define BLAS_PRLOC PRLOC
#define BLAS_PRLOC PRLOC;TCP_FUNC()

#include <cuda.h>
#include <driver_types.h>
#include <surface_types.h>
#include <texture_types.h>
#include <vector_types.h>
#include <cublas.h>
#include <cublas_v2.h>
#include <cublasLt.h>
//#include <cublas_api.h>

//
// Replace part
//
extern "C" {

#include "../include/buda_blas_simple.txt"

} // extern "C"
