#!/bin/bash

rm lib*

gcc main.c -lcudart -lcuda -lcublas -lcublasLt

gcc -c buda_rt.c -fPIC
gcc -shared -o libcudart.so buda_rt.o

gcc -c buda_drv.c -fPIC
gcc -shared -o libcuda.so buda_drv.o

g++ -c buda_blas.cpp -fPIC
gcc -shared -o libcublas.so buda_blas.o

gcc -c buda_blaslt.cpp -fPIC
gcc -shared -o libcublasLt.so buda_blaslt.o

#
# ...
#
cp libcudart.so libcudart.so.11.0 -f
cp libcuda.so libcuda.so.1 -f
cp libcublas.so libcublas.so.11 -f
cp libcublasLt.so libcublasLt.so.11 -f

#
# objdump -S -drwC libcublas.so>libcublas.so.dump
#

#
# export LD_PRELOAD=./libcuda.so.1:./libcudart.so.11.0:./libcublas.so.11:./libcublasLt.so.11
#
# export LD_PRELOAD=
#
# export LD_LIBRARY_PATH=.
# export LD_LIBRARY_PATH=
#
#
# ltrace -o __dummy_ltrace.txt -x 'cu*' python pth.py
#
# strace -o __dummy_strace.txt -f -F  python pth.py
#
#  python -m yep -c -o callgrind.out -- pth.py
#
#  unset GTK_PATH && kcachegrind
#
#  unset LD_PRELOAD
#  unset LD_LIBRARY_PATH
#
