==216565== NVPROF is profiling process 216565, command: python pth.py
==216565== Profiling application: python pth.py
==216565== Profiling result:
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:   10.15%  6.6560us         1  6.6560us  6.6560us  6.6560us  void at::native::reduce_kernel<int=512, int=1, at::native::ReduceOp<double, at::native::func_wrapper_t<double, at::native::MinNanFunctor<double>>, unsigned int, double, int=4>>(double)
                    9.42%  6.1770us         1  6.1770us  6.1770us  6.1770us  void at::native::reduce_kernel<int=512, int=1, at::native::ReduceOp<double, at::native::func_wrapper_t<double, at::native::MaxNanFunctor<double>>, unsigned int, double, int=4>>(double)
                    7.17%  4.7040us         2  2.3520us  1.7600us  2.9440us  _ZN2at6native29vectorized_elementwise_kernelILi4EZNS0_21compare_scalar_kernelIdEEvRNS_18TensorIteratorBaseENS0_50_GLOBAL__N__e50ef81d_17_CompareKernels_cu_8f1b29aa6OpTypeET_EUldE_NS_6detail5ArrayIPcLi2EEEEEviT0_T1_
                    6.83%  4.4800us         6     746ns     640ns     992ns  [CUDA memcpy DtoH]
                    6.78%  4.4480us         2  2.2240us  2.0160us  2.4320us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<float, float, bool, at::native::_GLOBAL__N__28ce311f_18_CompareEQKernel_cu_d8008c96::CompareEqFunctor<float>>, at::detail::Array<char*, int=3>>(int, float, float)
                    6.54%  4.2890us         2  2.1440us  1.7920us  2.4970us  void at::native::vectorized_elementwise_kernel<int=4, at::native::AbsFunctor<float>, at::detail::Array<char*, int=2>>(int, float, at::native::AbsFunctor<float>)
                    6.34%  4.1600us         2  2.0800us  1.6960us  2.4640us  void at::native::vectorized_elementwise_kernel<int=4, at::native::AUnaryFunctor<float, float, bool, at::native::_GLOBAL__N__28ce311f_18_CompareEQKernel_cu_d8008c96::CompareEqFunctor<float>>, at::detail::Array<char*, int=2>>(int, float, float)
                    6.15%  4.0320us         1  4.0320us  4.0320us  4.0320us  _ZN2at6native24index_elementwise_kernelILi128ELi4EZNS0_16gpu_index_kernelIZNS0_17index_kernel_implINS0_10OpaqueTypeILi4EEEEEvRNS_18TensorIteratorBaseEN3c108ArrayRefIlEESA_EUlPcSB_lE_EEvS7_SA_SA_RKT_EUliE_EEviT1_
                    5.37%  3.5200us         1  3.5200us  3.5200us  3.5200us  void at_cuda_detail::cub::DeviceReduceSingleTileKernel<at_cuda_detail::cub::DeviceReducePolicy<bool, int, int, at_cuda_detail::cub::Sum>::Policy600, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool*, long>, int*, int, at_cuda_detail::cub::Sum, int>(int, int, at_cuda_detail::cub::Sum, at_cuda_detail::cub::DeviceReducePolicy<bool, int, int, at_cuda_detail::cub::Sum>::Policy600, bool)
                    5.32%  3.4880us         1  3.4880us  3.4880us  3.4880us  void at_cuda_detail::cub::DeviceSelectSweepKernel<at_cuda_detail::cub::DispatchSelectIf<at_cuda_detail::cub::CountingInputIterator<long, long>, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool*, long>, long*, int*, at_cuda_detail::cub::NullType, at_cuda_detail::cub::NullType, int, bool=0>::PtxSelectIfPolicyT, at_cuda_detail::cub::CountingInputIterator<long, long>, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool*, long>, long*, int*, at_cuda_detail::cub::ScanTileState<int, bool=1>, at_cuda_detail::cub::NullType, at_cuda_detail::cub::NullType, int, bool=0>(long, at_cuda_detail::cub::CountingInputIterator<long, long>, bool, bool, at::native::_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool*, long, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__e625f313_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool*, long>, int)
                    5.17%  3.3920us         1  3.3920us  3.3920us  3.3920us  _ZN2at6native27unrolled_elementwise_kernelIZZZNS0_23direct_copy_kernel_cudaERNS_18TensorIteratorBaseEENKUlvE0_clEvENKUlvE4_clEvEUldE_NS_6detail5ArrayIPcLi2EEE23TrivialOffsetCalculatorILi1EjESC_NS0_6memory12LoadWithCastILi1EEENSD_13StoreWithCastILi1EEEEEviT_T0_T1_T2_T3_T4_
                    4.88%  3.2000us         1  3.2000us  3.2000us  3.2000us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<double, double, double, at::native::binary_internal::DivFunctor<double>>, at::detail::Array<char*, int=3>>(int, double, double)
                    4.19%  2.7520us         1  2.7520us  2.7520us  2.7520us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<bool, bool, bool, at::native::binary_internal::MulFunctor<bool>>, at::detail::Array<char*, int=3>>(int, bool, bool)
                    4.00%  2.6240us         1  2.6240us  2.6240us  2.6240us  void at::native::vectorized_elementwise_kernel<int=4, at::native::CUDAFunctor_add<float>, at::detail::Array<char*, int=3>>(int, float, at::native::CUDAFunctor_add<float>)
                    3.51%  2.3040us         1  2.3040us  2.3040us  2.3040us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<bool, bool, bool, at::native::BitwiseAndFunctor<bool>>, at::detail::Array<char*, int=3>>(int, bool, bool)
                    3.12%  2.0490us         1  2.0490us  2.0490us  2.0490us  _ZN2at6native29vectorized_elementwise_kernelILi4EZZZNS0_16ceil_kernel_cudaERNS_18TensorIteratorBaseEENKUlvE_clEvENKUlvE0_clEvEUlfE_NS_6detail5ArrayIPcLi2EEEEEviT0_T1_
                    2.73%  1.7920us         1  1.7920us  1.7920us  1.7920us  void at_cuda_detail::cub::DeviceCompactInitKernel<at_cuda_detail::cub::ScanTileState<int, bool=1>, int*>(int, int, bool=1)
                    2.34%  1.5370us         2     768ns     545ns     992ns  [CUDA memcpy HtoD]
      API calls:   99.93%  931.37ms         1  931.37ms  931.37ms  931.37ms  cudaStreamIsCapturing
                    0.02%  165.32us        20  8.2650us  4.3260us  30.236us  cudaLaunchKernel
                    0.01%  107.47us         1  107.47us  107.47us  107.47us  cudaMalloc
                    0.01%  107.11us         8  13.389us  5.5840us  36.306us  cudaMemcpyAsync
                    0.01%  71.482us       101     707ns     115ns  29.643us  cuDeviceGetAttribute
                    0.01%  60.233us       191     315ns     222ns  4.7570us  cudaGetDevice
                    0.01%  56.050us         1  56.050us  56.050us  56.050us  cudaGetDeviceProperties
                    0.00%  22.288us         8  2.7860us  1.3860us  5.5200us  cudaStreamSynchronize
                    0.00%  13.540us         1  13.540us  13.540us  13.540us  cuDeviceGetName
                    0.00%  8.5240us         1  8.5240us  8.5240us  8.5240us  cudaFuncGetAttributes
                    0.00%  7.8280us        49     159ns     112ns     331ns  cudaGetLastError
                    0.00%  5.6870us         1  5.6870us  5.6870us  5.6870us  cuDeviceGetPCIBusId
                    0.00%  4.8730us         4  1.2180us     126ns  4.0780us  cudaGetDeviceCount
                    0.00%  1.0230us         1  1.0230us  1.0230us  1.0230us  cudaDeviceGetAttribute
                    0.00%     898ns         6     149ns     117ns     214ns  cudaPeekAtLastError
                    0.00%     804ns         3     268ns     139ns     503ns  cuDeviceGetCount
                    0.00%     712ns         3     237ns     128ns     443ns  cuDevicePrimaryCtxGetState
                    0.00%     547ns         2     273ns     125ns     422ns  cuDeviceGet
                    0.00%     314ns         1     314ns     314ns     314ns  cuDeviceTotalMem
                    0.00%     221ns         1     221ns     221ns     221ns  cuDeviceGetUuid
                    0.00%     215ns         1     215ns     215ns     215ns    �
