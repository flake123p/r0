#include <stdio.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

#include <cublas.h>
#include <cublasLt.h>

int main() {

    cublasHandle_t handle;
    cublasStatus_t rc = cublasCreate_v2(&handle); 

    printf("Hello Example , %d\n", rc);

    cublasLtHandle_t lightHandle;

    rc = cublasLtCreate(&lightHandle);
    printf("Hello Example , %d\n", rc);

    return 0;
}