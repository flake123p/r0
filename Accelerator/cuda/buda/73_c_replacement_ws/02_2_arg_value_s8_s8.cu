#include <stdio.h>

//
// Nearly minimal CUDA example.
// Compile with:
//
// nvcc -o example example.cu
//

// __global__ = cpu    call, run on device
// __device__ = device call, run on device
// __host__   = cpu    call, run on    cpu
#define BLOCK_NUM 1
#define THREAD_NUM 4

__global__
void myGlobal1234(char x, char y) {
    printf("%d\n", threadIdx.x + x + y);
}

int main() {

    myGlobal1234<<<BLOCK_NUM, THREAD_NUM>>>(0x22, 0x66);

    return 0;
}