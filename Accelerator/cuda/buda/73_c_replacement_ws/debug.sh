#!/bin/bash

# nvcc -O0 -g -std=c++17 $1

# ./run.sh

#export LD_PRELOAD=/home/pupil/.local/lib/python3.10/site-packages/torch/lib/libtorch_cpu.so:./lib/libcuda.so.1:./lib/libcudart.so.11.0:./lib/libcublas.so.11:./lib/libcublasLt.so.11:./lib/libcudartpp.so:./lib/libbuda.so
#export LD_PRELOAD=./lib/libbuda.so
#export LD_LIBRARY_PATH=./lib:/home/pupil/.local/lib/python3.10/site-packages/torch/lib/

export LD_PRELOAD=./lib/libcuda.so.1:./lib/libcudart.so.11.0:./lib/libcublas.so.11:./lib/libcublasLt.so.11:./lib/libcudartpp.so
export LD_LIBRARY_PATH=./lib

#python softmax.py > log.run.log
gdb -x debug.rc
#strace -o __dummy_strace.txt python $1