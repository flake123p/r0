#include <stdio.h>

//
// Nearly minimal CUDA example.
// Compile with:
//
// nvcc -o example example.cu
//

// __global__ = cpu    call, run on device
// __device__ = device call, run on device
// __host__   = cpu    call, run on    cpu
#define BLOCK_NUM 1
#define THREAD_NUM 4

__global__
void myGlobal1234(char x, int y[4]) {
    printf("%d\n", threadIdx.x + x + y[0] + y[1] + y[2] + y[3]);
}

int main() {
    int y[4] = {0x5566, 0x5577, 0x5588, 0x5599};
    printf("y addr = %p\n", &y);
    myGlobal1234<<<BLOCK_NUM, THREAD_NUM>>>(0x33, y);

    return 0;
}