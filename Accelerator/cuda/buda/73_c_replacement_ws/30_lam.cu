#include <stdio.h>

//
// Nearly minimal CUDA example.
// Compile with:
//
// nvcc -o example example.cu
//

// __global__ = cpu    call, run on device
// __device__ = device call, run on device
// __host__   = cpu    call, run on    cpu
#define BLOCK_NUM 1
#define THREAD_NUM 4

template<typename Func>
__global__ void myGlobal1234(Func f){
    f(0x2266);
}

int main() {
    int a = 10;
    //int b = 20;
    printf("addr of a = %p\n", &a);

    auto mylam = [] __device__ (int input) -> void {printf("0x%x\n", threadIdx.x + input);};
    printf("addr of mylam = %p\n", &mylam);
    
    myGlobal1234<<<BLOCK_NUM, THREAD_NUM>>>(mylam);
    cudaDeviceSynchronize();

    return 0;
}