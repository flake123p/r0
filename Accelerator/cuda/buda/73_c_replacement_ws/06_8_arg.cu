#include <stdio.h>

//
// Nearly minimal CUDA example.
// Compile with:
//
// nvcc -o example example.cu
//

// __global__ = cpu    call, run on device
// __device__ = device call, run on device
// __host__   = cpu    call, run on    cpu
#define BLOCK_NUM 1
#define THREAD_NUM 4

__global__
void myGlobal1234(int &x, int &y, int &z, int *a, int *b, int *c, int *d, int *e) {
    printf("%d\n", threadIdx.x + x + y + z + *a + *b + *c + *d + *e);
}

int main() {
    int a = 10;
    int b = 20;
    int c = 30;
    int d = 40;
    int e = 50;
    int x = 1;
    int y = 2;
    int z = 3;
    printf("x addr = %p\n", &x);
    printf("y addr = %p\n", &y);
    printf("z addr = %p\n", &z);
    printf("a addr = %p\n", &a);
    printf("b addr = %p\n", &b);
    printf("c addr = %p\n", &c);
    printf("d addr = %p\n", &d);
    printf("e addr = %p\n", &e);
    myGlobal1234<<<BLOCK_NUM, THREAD_NUM>>>(x, y, z, &a, &b, &c, &d, &e);

    return 0;
}