#include <stdio.h>

//
// Nearly minimal CUDA example.
// Compile with:
//
// nvcc -o example example.cu
//

// __global__ = cpu    call, run on device
// __device__ = device call, run on device
// __host__   = cpu    call, run on    cpu
#define BLOCK_NUM 1
#define THREAD_NUM 4

template<typename Func>
__global__ void myGlobal1234(Func f, int input){
    f(input);
}

template<typename Func>
void myTemp1234(Func f){
    int b = 0x550000;
    printf("addr of b = %p\n", &b);

    myGlobal1234<<<BLOCK_NUM, THREAD_NUM>>>(
        [=] __device__ (int idx) {
            f();
            printf("0x%x\n", threadIdx.x + b + idx);
        },
        0x99000000
    );
}

int main() {
    int a = 0x00660000;
    printf("addr of a = %p\n", &a);

    auto mylam = [=] __device__ () -> void {
        printf("0x%x\n", threadIdx.x + a);
    };
    
    myTemp1234(mylam);
    cudaDeviceSynchronize();

    return 0;
}