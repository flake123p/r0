# 00_1_arg_ptr.cu (int * 0x564ca39c1990)

__global__ void fill_id(int *workbuf) {
    workbuf[threadIdx.x] = threadIdx.x;
}
------------------------------------------------------------------------------
[PR] cudaMalloc(), 44
size = 16, loc = 0x564ca39c1990

[PR] cudaLaunchKernel(), 239
func = 0x564ca1fc55b0, hdl = 0xe5894855fa1e0ff3 [ sm = 0 ] ( 0 )
gridDim : 1, 1, 1
blockDim: 4, 1, 1
args = 0x7ffe6e6a7470
args[0] is in 0x7ffe6e6a7470 = 0x7ffe6e6a7438
args[1] is in 0x7ffe6e6a7478 = 0x11d5c90318abd00
args[2] is in 0x7ffe6e6a7480 = 0x7ffe6e6a74a0
args[3] is in 0x7ffe6e6a7488 = 0x564ca1fc55cc
//target is args[0]
target[0] is in 0x7ffe6e6a7438 = 0x564ca39c1990
target[1] is in 0x7ffe6e6a7440 = 0x1a1fc531d
target[2] is in 0x7ffe6e6a7448 = 0x564ca1fc8d48
target[3] is in 0x7ffe6e6a7450 = 0x7f307a913040
------------------------------------------------------------------------------
(1st argument is in args[0])



# 01_1_arg_value.cu (int 0x2266)

__global__ void fill_id(int *workbuf) {
    workbuf[threadIdx.x] = threadIdx.x;
}
------------------------------------------------------------------------------
[PR] cudaLaunchKernel(), 239
func = 0x5568cd7464c4, hdl = 0xe5894855fa1e0ff3 [ sm = 0 ] ( 0 )
gridDim : 1, 1, 1
blockDim: 4, 1, 1
args = 0x7ffc45089a10
args[0] is in 0x7ffc45089a10 = 0x7ffc450899dc
args[1] is in 0x7ffc45089a18 = 0xa9971a45753c7500
args[2] is in 0x7ffc45089a20 = 0x7ffc45089a40
args[3] is in 0x7ffc45089a28 = 0x5568cd7464dd
args[4] is in 0x7ffc45089a30 = 0x100000001
args[5] is in 0x7ffc45089a38 = 0x226600000001
//target is args[0]
target[0] is in 0x7ffc450899dc = 0x2266
target[1] is in 0x7ffc450899e4 = 0x1
target[2] is in 0x7ffc450899ec = (nil)
target[3] is in 0x7ffc450899f4 = 0x100000000
target[4] is in 0x7ffc450899fc = 0x100000001
target[5] is in 0x7ffc45089a04 = 0x100000001
------------------------------------------------------------------------------



# 02_2_arg_value_s8_s8 (char 0x22, char 0x66)
------------------------------------------------------------------------------
[PR] cudaLaunchKernel(), 239
func = 0x55db239db4e2, hdl = 0xe5894855fa1e0ff3 [ sm = 0 ] ( 0 )
gridDim : 1, 1, 1
blockDim: 4, 1, 1
...... NOT IMPLEMENT ......
_Z12myGlobal1234cc
args = 0x7ffe30ce6740
args[0] is in 0x7ffe30ce6740 = 0x7ffe30ce670c
args[1] is in 0x7ffe30ce6748 = 0x7ffe30ce6708
args[2] is in 0x7ffe30ce6750 = (nil)
args[3] is in 0x7ffe30ce6758 = 0xc69f43913df65200
//target is args[0]
target[0] is in 0x7ffe30ce670c = 0xc00007f22
target[1] is in 0x7ffe30ce6714 = 0x355e6feb00000002
target[2] is in 0x7ffe30ce671c = 0x7f43
target[3] is in 0x7ffe30ce6724 = 0x100000000
//target is args[1]
target[0] is in 0x7ffe30ce6708 = 0x7f2235609766
target[1] is in 0x7ffe30ce6710 = 0x20000000c
target[2] is in 0x7ffe30ce6718 = 0x7f43355e6feb
target[3] is in 0x7ffe30ce6720 = (nil)
------------------------------------------------------------------------------

# 03_2_arg_value_s8_iptr (char 0x22, int * 0x7ffc31d37e1c)
------------------------------------------------------------------------------
[PR] cudaLaunchKernel(), 239
func = 0x5635fbef851e, hdl = 0xe5894855fa1e0ff3 [ sm = 0 ] ( 0 )
gridDim : 1, 1, 1
blockDim: 4, 1, 1
...... NOT IMPLEMENT ......
_Z12myGlobal1234cPi
args = 0x7ffc31d37dc0
args[0] is in 0x7ffc31d37dc0 = 0x7ffc31d37d8c
args[1] is in 0x7ffc31d37dc8 = 0x7ffc31d37d80
args[2] is in 0x7ffc31d37dd0 = (nil)
args[3] is in 0x7ffc31d37dd8 = 0xf3f4c385bb970c00
//target is args[0]
target[0] is in 0x7ffc31d37d8c = 0x7f22
target[1] is in 0x7ffc31d37d94 = 0xa8a2f76000000002
target[2] is in 0x7ffc31d37d9c = 0xc00007f57
target[3] is in 0x7ffc31d37da4 = 0x100000000
//target is args[1]
target[0] is in 0x7ffc31d37d80 = 0x7ffc31d37e1c
target[1] is in 0x7ffc31d37d88 = 0x7f22a89d7770
target[2] is in 0x7ffc31d37d90 = 0x200000000
target[3] is in 0x7ffc31d37d98 = 0x7f57a8a2f760
------------------------------------------------------------------------------



# 04_2_arg_value_s8_iref (char 0x33, int & y)
------------------------------------------------------------------------------
y addr = 0x7ffeecac666c

[PR] cudaLaunchKernel(), 239
func = 0x5615bbd7051e, hdl = 0xe5894855fa1e0ff3 [ sm = 0 ] ( 0 )
gridDim : 1, 1, 1
blockDim: 4, 1, 1
...... NOT IMPLEMENT ......
_Z12myGlobal1234cRi
args = 0x7ffeecac6610
args[0] is in 0x7ffeecac6610 = 0x7ffeecac65dc
args[1] is in 0x7ffeecac6618 = 0x7ffeecac65d0
args[2] is in 0x7ffeecac6620 = (nil)
args[3] is in 0x7ffeecac6628 = 0xc57e71f04d9a5200
//target is args[0]
target[0] is in 0x7ffeecac65dc = 0x7f33
target[1] is in 0x7ffeecac65e4 = 0x6d0ca76000000002
target[2] is in 0x7ffeecac65ec = 0xc00007ff6
target[3] is in 0x7ffeecac65f4 = 0x100000000
//target is args[1]
target[0] is in 0x7ffeecac65d0 = 0x7ffeecac666c
target[1] is in 0x7ffeecac65d8 = 0x7f336d072770
target[2] is in 0x7ffeecac65e0 = 0x200000000
target[3] is in 0x7ffeecac65e8 = 0x7ff66d0ca760
------------------------------------------------------------------------------



# 05_4_arg.cu (int &x, int &y, int &z, int *a)
------------------------------------------------------------------------------
x addr = 0x7ffd3fbc928c
y addr = 0x7ffd3fbc9290
z addr = 0x7ffd3fbc9294
a addr = 0x7ffd3fbc9288

[PR] cudaLaunchKernel(), 239
func = 0x55c635eac5cb, hdl = 0xe5894855fa1e0ff3 [ sm = 0 ] ( 0 )
gridDim : 1, 1, 1
blockDim: 4, 1, 1
...... NOT IMPLEMENT ......
_Z12myGlobal1234RiS_S_Pi
args = 0x7ffd3fbc9200
args[0] is in 0x7ffd3fbc9200 = 0x7ffd3fbc91c8
args[1] is in 0x7ffd3fbc9208 = 0x7ffd3fbc91c0
args[2] is in 0x7ffd3fbc9210 = 0x7ffd3fbc91b8
args[3] is in 0x7ffd3fbc9218 = 0x7ffd3fbc91b0
//target is args[0]
target[0] is in 0x7ffd3fbc91c8 = 0x7ffd3fbc928c
target[1] is in 0x7ffd3fbc91d0 = 0x400000000
//target is args[1]
target[0] is in 0x7ffd3fbc91c0 = 0x7ffd3fbc9290
target[1] is in 0x7ffd3fbc91c8 = 0x7ffd3fbc928c
//target is args[2]
target[0] is in 0x7ffd3fbc91b8 = 0x7ffd3fbc9294
target[1] is in 0x7ffd3fbc91c0 = 0x7ffd3fbc9290
//target is args[3]
target[0] is in 0x7ffd3fbc91b0 = 0x7ffd3fbc9288
target[1] is in 0x7ffd3fbc91b8 = 0x7ffd3fbc9294
------------------------------------------------------------------------------



# 06_8_arg.cu (int &x, int &y, int &z, int *a, int *b, int *c, int *d, int *e)
------------------------------------------------------------------------------
x addr = 0x7ffda25a4c14
y addr = 0x7ffda25a4c18
z addr = 0x7ffda25a4c1c
a addr = 0x7ffda25a4c00
b addr = 0x7ffda25a4c04
c addr = 0x7ffda25a4c08
d addr = 0x7ffda25a4c0c
e addr = 0x7ffda25a4c10

[PR] cudaLaunchKernel(), 239
func = 0x5557addeb6ee, hdl = 0xe5894855fa1e0ff3 [ sm = 0 ] ( 0 )
gridDim : 1, 1, 1
blockDim: 4, 1, 1
...... NOT IMPLEMENT ......
_Z12myGlobal1234RiS_S_PiS0_S0_S0_S0_
args = 0x7ffda25a4b30
args[0] is in 0x7ffda25a4b30 = 0x7ffda25a4af8
args[1] is in 0x7ffda25a4b38 = 0x7ffda25a4af0
args[2] is in 0x7ffda25a4b40 = 0x7ffda25a4ae8
args[3] is in 0x7ffda25a4b48 = 0x7ffda25a4ae0
args[4] is in 0x7ffda25a4b50 = 0x7ffda25a4ad8
args[5] is in 0x7ffda25a4b58 = 0x7ffda25a4ad0
args[6] is in 0x7ffda25a4b60 = 0x7ffda25a4ac8
args[7] is in 0x7ffda25a4b68 = 0x7ffda25a4ac0
//target is args[0]
target[0] is in 0x7ffda25a4af8 = 0x7ffda25a4c14
//target is args[1]
target[0] is in 0x7ffda25a4af0 = 0x7ffda25a4c18
//target is args[2]
target[0] is in 0x7ffda25a4ae8 = 0x7ffda25a4c1c
//target is args[3]
target[0] is in 0x7ffda25a4ae0 = 0x7ffda25a4c00
//target is args[4]
target[0] is in 0x7ffda25a4ad8 = 0x7ffda25a4c04
//target is args[5]
target[0] is in 0x7ffda25a4ad0 = 0x7ffda25a4c08
//target is args[6]
target[0] is in 0x7ffda25a4ac8 = 0x7ffda25a4c0c
//target is args[7]
target[0] is in 0x7ffda25a4ac0 = 0x7ffda25a4c10
------------------------------------------------------------------------------



# 07_2_arg_s8_intArray.cu (char x, int y[4])
------------------------------------------------------------------------------
y addr = 0x7ffd532ea9e0

[PR] cudaLaunchKernel(), 239
func = 0x55870692d52f, hdl = 0xe5894855fa1e0ff3 [ sm = 0 ] ( 0 )
gridDim : 1, 1, 1
blockDim: 4, 1, 1
...... NOT IMPLEMENT ......
_Z12myGlobal1234cPi
args = 0x7ffd532ea970
args[0] is in 0x7ffd532ea970 = 0x7ffd532ea93c
args[1] is in 0x7ffd532ea978 = 0x7ffd532ea930
args[2] is in 0x7ffd532ea980 = (nil)
args[3] is in 0x7ffd532ea988 = 0x3e2fc6c1d03ea700
args[4] is in 0x7ffd532ea990 = 0x7ffd532ea9b0
args[5] is in 0x7ffd532ea998 = 0x55870692d556
args[6] is in 0x7ffd532ea9a0 = 0x7ffd532ea9e0
args[7] is in 0x7ffd532ea9a8 = 0x7f3300000001
//target is args[0]
target[0] is in 0x7ffd532ea93c = 0x532ea96000000033
target[1] is in 0x7ffd532ea944 = 0xa315781800000002
//target is args[1]
target[0] is in 0x7ffd532ea930 = 0x7ffd532ea9e0
target[1] is in 0x7ffd532ea938 = 0x3300000000
target[2] is in 0x7ffd532ea940 = 0x2532ea960
target[3] is in 0x7ffd532ea948 = 0x7f7ea3157818
------------------------------------------------------------------------------



# 30_lam.cu (can't find 0x2266)

    template<typename Func>
    __global__ void myGlobal1234(Func f){
        f(0x2266);
    }

    auto mylam = [] __device__ (int input) -> void {printf("0x%x\n", threadIdx.x + input);};

    myGlobal1234<<<BLOCK_NUM, THREAD_NUM>>>(mylam);
------------------------------------------------------------------------------
...... NOT IMPLEMENT ......
_Z12myGlobal1234IZ4mainEUliE_EvT_
args = 0x7ffd8f5aae00
args[0] is in 0x7ffd8f5aae00 = 0x7ffd8f5aae4f
args[1] is in 0x7ffd8f5aae08 = 0x157ebd5d46d97e00
args[2] is in 0x7ffd8f5aae10 = 0x7ffd8f5aae30
args[3] is in 0x7ffd8f5aae18 = 0x55749173250e
args[4] is in 0x7ffd8f5aae20 = (nil)
args[5] is in 0x7ffd8f5aae28 = 0x7ffd8f5aae4f
args[6] is in 0x7ffd8f5aae30 = 0x7ffd8f5aae50
args[7] is in 0x7ffd8f5aae38 = 0x55749173268b
//target is args[0]
target[0] is in 0x7ffd8f5aae4f = 0x7ffd8f5aae9000
target[1] is in 0x7ffd8f5aae57 = 0x55749173234b00
------------------------------------------------------------------------------



# 31_lam_cap_1val.cu

    int a = 0x1122;

    auto mylam = [a] __device__ (int input) -> void {printf("0x%x\n", threadIdx.x + input + a);};

------------------------------------------------------------------------------
...... NOT IMPLEMENT ......
_Z12myGlobal1234IZ4mainEUliE_EvT_
args = 0x7ffe43495890
args[0] is in 0x7ffe43495890 = 0x7ffe434958dc
args[1] is in 0x7ffe43495898 = 0x4fc6e05676963000
args[2] is in 0x7ffe434958a0 = 0x7ffe434958c0
args[3] is in 0x7ffe434958a8 = 0x555b3744651f
args[4] is in 0x7ffe434958b0 = (nil)
args[5] is in 0x7ffe434958b8 = 0x7ffe434958dc
args[6] is in 0x7ffe434958c0 = 0x7ffe434958e0
args[7] is in 0x7ffe434958c8 = 0x555b374466ac
//target is args[0]
target[0] is in 0x7ffe434958dc = 0x4349592000001122
target[1] is in 0x7ffe434958e4 = 0x3744635c00007ffe
------------------------------------------------------------------------------



# 32_lam_cap_2val.cu

    int a = 0x00660000;
    int b = 0x22000000;

    auto mylam = [a,b] __device__ (int input) -> void {printf("0x%x\n", threadIdx.x + input + a + b);};
    
------------------------------------------------------------------------------
...... NOT IMPLEMENT ......
_Z12myGlobal1234IZ4mainEUliE_EvT_
args = 0x7ffced2116d0
args[0] is in 0x7ffced2116d0 = 0x7ffced211718
args[1] is in 0x7ffced2116d8 = 0x334240619bc2c600
args[2] is in 0x7ffced2116e0 = 0x7ffced211700
args[3] is in 0x7ffced2116e8 = 0x5597e867152b
args[4] is in 0x7ffced2116f0 = (nil)
args[5] is in 0x7ffced2116f8 = 0x7ffced211718
args[6] is in 0x7ffced211700 = 0x7ffced211720
args[7] is in 0x7ffced211708 = 0x5597e86716c6
//target is args[0]
target[0] is in 0x7ffced211718 = 0x2200000000660000
target[1] is in 0x7ffced211720 = 0x7ffced211760
[PR] cudaDeviceSynchronize(), 5
------------------------------------------------------------------------------



# 33_lam_cap_4val
    int a = 0x00660000;
    int b = 0x22000000;
    char c = 0x77;
    int d = 0x88;

    auto mylam = [a,b,c,d] __device__ (int input) -> void {printf("0x%x\n", threadIdx.x + input + a + b);};
------------------------------------------------------------------------------
...... NOT IMPLEMENT ......
_Z12myGlobal1234IZ4mainEUliE_EvT_
args = 0x7ffc0412e140
args[0] is in 0x7ffc0412e140 = 0x7ffc0412e180
args[1] is in 0x7ffc0412e148 = 0xefb1c9b7200dcc00
args[2] is in 0x7ffc0412e150 = 0x7ffc0412e170
args[3] is in 0x7ffc0412e158 = 0x55644b61b545
args[4] is in 0x7ffc0412e160 = (nil)
args[5] is in 0x7ffc0412e168 = 0x7ffc0412e180
args[6] is in 0x7ffc0412e170 = 0x7ffc0412e190
args[7] is in 0x7ffc0412e178 = 0x55644b61b70b
//target is args[0]
target[0] is in 0x7ffc0412e180 = 0x2200000000660000
target[1] is in 0x7ffc0412e188 = 0x886c656e77
[PR] cudaDeviceSynchronize(), 5
------------------------------------------------------------------------------



# 34_lam_cap_random
    int a = 0x00660000;
    int b = 0x22000000;
    char c = 0x77;
    int d = 0x88;

    auto mylam = [a,c] __device__ (int input) -> void {printf("0x%x\n", threadIdx.x + input + a + c);};
------------------------------------------------------------------------------
...... NOT IMPLEMENT ......
_Z12myGlobal1234IZ4mainEUliE_EvT_
args = 0x7ffe1cf10a60
args[0] is in 0x7ffe1cf10a60 = 0x7ffe1cf10aa8
args[1] is in 0x7ffe1cf10a68 = 0xca7116cf2b613e00
args[2] is in 0x7ffe1cf10a70 = 0x7ffe1cf10a90
args[3] is in 0x7ffe1cf10a78 = 0x5604db27c537
args[4] is in 0x7ffe1cf10a80 = (nil)
args[5] is in 0x7ffe1cf10a88 = 0x7ffe1cf10aa8
args[6] is in 0x7ffe1cf10a90 = 0x7ffe1cf10ab0
args[7] is in 0x7ffe1cf10a98 = 0x5604db27c6d5
//target is args[0]
target[0] is in 0x7ffe1cf10aa8 = 0x6e656d7700660000
target[1] is in 0x7ffe1cf10ab0 = 0x7ffe1cf10b00
[PR] cudaDeviceSynchronize(), 5
------------------------------------------------------------------------------



# 35_lam_cap_random2
    int a = 0x00660000;
    int b = 0x22000000;
    char c = 0x77;
    int d = 0x88;

    auto mylam = [c,a] __device__ (int input) -> void {printf("0x%x\n", threadIdx.x + input + a + c);};
------------------------------------------------------------------------------
...... NOT IMPLEMENT ......
_Z12myGlobal1234IZ4mainEUliE_EvT_
args = 0x7ffca751bad0
args[0] is in 0x7ffca751bad0 = 0x7ffca751bb18
args[1] is in 0x7ffca751bad8 = 0x7a33e04a09540400
args[2] is in 0x7ffca751bae0 = 0x7ffca751bb00
args[3] is in 0x7ffca751bae8 = 0x560e17538537
args[4] is in 0x7ffca751baf0 = (nil)
args[5] is in 0x7ffca751baf8 = 0x7ffca751bb18
args[6] is in 0x7ffca751bb00 = 0x7ffca751bb20
args[7] is in 0x7ffca751bb08 = 0x560e175386d5
//target is args[0]
target[0] is in 0x7ffca751bb18 = 0x6600006c655f77
target[1] is in 0x7ffca751bb20 = 0x7ffca751bb70
[PR] cudaDeviceSynchronize(), 5
------------------------------------------------------------------------------



# 36_lam_cap_all
    int a = 0x00660000;
    int b = 0x22000000;
    char c = 0x77;
    int d = 0x88;

    auto mylam = [=] __device__ (int input) -> void {printf("0x%x\n", threadIdx.x + input + a + c);};
------------------------------------------------------------------------------
...... NOT IMPLEMENT ......
_Z12myGlobal1234IZ4mainEUliE_EvT_
args = 0x7ffe34d66330
args[0] is in 0x7ffe34d66330 = 0x7ffe34d66378
args[1] is in 0x7ffe34d66338 = 0x3a7a56ce270fe500
args[2] is in 0x7ffe34d66340 = 0x7ffe34d66360
args[3] is in 0x7ffe34d66348 = 0x557bbe9c8537
args[4] is in 0x7ffe34d66350 = (nil)
args[5] is in 0x7ffe34d66358 = 0x7ffe34d66378
args[6] is in 0x7ffe34d66360 = 0x7ffe34d66380
args[7] is in 0x7ffe34d66368 = 0x557bbe9c86d5
//target is args[0]
target[0] is in 0x7ffe34d66378 = 0x6e656d7700660000
target[1] is in 0x7ffe34d66380 = 0x7ffe34d663d0
[PR] cudaDeviceSynchronize(), 5
------------------------------------------------------------------------------



# 37_lam_cap_all2
    int a = 0x00660000;
    int b = 0x22000000;
    char c = 0x77;
    int d = 0x88;

    auto mylam = [=] __device__ (int input) -> void {printf("0x%x\n", threadIdx.x + input + c + a);};
------------------------------------------------------------------------------
...... NOT IMPLEMENT ......
_Z12myGlobal1234IZ4mainEUliE_EvT_
args = 0x7ffe66741d10
args[0] is in 0x7ffe66741d10 = 0x7ffe66741d58
args[1] is in 0x7ffe66741d18 = 0x5fa07932babf7e00
args[2] is in 0x7ffe66741d20 = 0x7ffe66741d40
args[3] is in 0x7ffe66741d28 = 0x55e5ebef0537
args[4] is in 0x7ffe66741d30 = (nil)
args[5] is in 0x7ffe66741d38 = 0x7ffe66741d58
args[6] is in 0x7ffe66741d40 = 0x7ffe66741d60
args[7] is in 0x7ffe66741d48 = 0x55e5ebef06d5
//target is args[0]
target[0] is in 0x7ffe66741d58 = 0x6600006c655f77
target[1] is in 0x7ffe66741d60 = 0x7ffe66741db0
[PR] cudaDeviceSynchronize(), 5
------------------------------------------------------------------------------



# 38_lam_cap_all_1val
template<typename Func>
__global__ void myGlobal1234(Func f, int input){
    f(input);
}

    int a = 0x00660000;
    int b = 0x22000000;
    char c = 0x77;
    int d = 0x88;

    auto mylam = [=] __device__ (int input) -> void {printf("0x%x\n", threadIdx.x + input + c + a);};
------------------------------------------------------------------------------
...... NOT IMPLEMENT ......
_Z12myGlobal1234IZ4mainEUliE_EvT_i
args = 0x7ffea2e40b20
args[0] is in 0x7ffea2e40b20 = 0x7ffea2e40b78
args[1] is in 0x7ffea2e40b28 = 0x7ffea2e40ae4
args[2] is in 0x7ffea2e40b30 = 0xc
args[3] is in 0x7ffea2e40b38 = 0x5ce831226593400
args[4] is in 0x7ffea2e40b40 = 0x7ffea2e40b60
args[5] is in 0x7ffea2e40b48 = 0x56431fbf055d
args[6] is in 0x7ffea2e40b50 = 0x7ffea2e40b74
args[7] is in 0x7ffea2e40b58 = 0x7ffea2e40b78
//target is args[0]
target[0] is in 0x7ffea2e40b78 = 0x6600006c655f77
target[1] is in 0x7ffea2e40b80 = 0x7ffea2e40bd0
//target is args[1]
target[0] is in 0x7ffea2e40ae4 = 0xa2e40b7800003344
target[1] is in 0x7ffea2e40aec = 0x7ffe
[PR] cudaDeviceSynchronize(), 5
------------------------------------------------------------------------------



# 39_lam_cap_all_1val2
template<typename Func>
__global__ void myGlobal1234(int input, Func f){
    f(input);
}

    int a = 0x00660000;
    int b = 0x22000000;
    char c = 0x77;
    int d = 0x88;

    auto mylam = [=] __device__ (int input) -> void {
        printf("0x%x\n", threadIdx.x + input + c + a);};
    
    myGlobal1234<<<BLOCK_NUM, THREAD_NUM>>>(0x3344, mylam);
------------------------------------------------------------------------------
...... NOT IMPLEMENT ......
_Z12myGlobal1234IZ4mainEUliE_EviT_
args = 0x7ffc276b1860
args[0] is in 0x7ffc276b1860 = 0x7ffc276b182c
args[1] is in 0x7ffc276b1868 = 0x7ffc276b18b4
args[2] is in 0x7ffc276b1870 = 0xc
args[3] is in 0x7ffc276b1878 = 0x55342d9e43d72000
args[4] is in 0x7ffc276b1880 = 0x7ffc276b18a0
args[5] is in 0x7ffc276b1888 = 0x5597ca68b55d
args[6] is in 0x7ffc276b1890 = 0x7ffc276b18b4
args[7] is in 0x7ffc276b1898 = 0x7ffc276b18bc
//target is args[0]
target[0] is in 0x7ffc276b182c = 0x3344
target[1] is in 0x7ffc276b1834 = 0x2
//target is args[1]
target[0] is in 0x7ffc276b18b4 = 0x6600006c655f77
target[1] is in 0x7ffc276b18bc = 0x276b191000003344
[PR] cudaDeviceSynchronize(), 5
------------------------------------------------------------------------------



# 50_two_lam
#define BLOCK_NUM 1
#define THREAD_NUM 4

template<typename Func>
__global__ void myGlobal1234(Func f, int input){
    f(input);
}

template<typename Func>
void myTemp1234(Func f){
    int b = 0x550000;
    printf("addr of b = %p\n", &b);

    myGlobal1234<<<BLOCK_NUM, THREAD_NUM>>>(
        [=] __device__ (int idx) {
            f();
            printf("0x%x\n", threadIdx.x + b + idx);
        },
        0x99000000
    );
}

int main() {
    int a = 0x00660000;
    printf("addr of a = %p\n", &a);

    auto mylam = [=] __device__ () -> void {
        printf("0x%x\n", threadIdx.x + a);
    };
    
    myTemp1234(mylam);
    cudaDeviceSynchronize();

    return 0;
}
------------------------------------------------------------------------------
addr of a = 0x7ffe9aed6c60
addr of b = 0x7ffe9aed6c24

[PR] cudaLaunchKernel(), 239
func = 0x55bf7a311762, hdl = 0xe5894855fa1e0ff3 [ sm = 0 ] ( 0 )
gridDim : 1, 1, 1
blockDim: 4, 1, 1
...... NOT IMPLEMENT ......
_Z12myGlobal1234IZ10myTemp1234IZ4mainEUlvE_EvT_EUliE_EvS2_i
args = 0x7ffe9aed6ba0
args[0] is in 0x7ffe9aed6ba0 = 0x7ffe9aed6bf8
args[1] is in 0x7ffe9aed6ba8 = 0x7ffe9aed6b64
args[2] is in 0x7ffe9aed6bb0 = 0x7ffe9aed6d98
args[3] is in 0x7ffe9aed6bb8 = 0x24d9486c59631f00
args[4] is in 0x7ffe9aed6bc0 = 0x7ffe9aed6be0
args[5] is in 0x7ffe9aed6bc8 = 0x55bf7a311516
args[6] is in 0x7ffe9aed6bd0 = 0x7ffe9aed6bf4
args[7] is in 0x7ffe9aed6bd8 = 0x7ffe9aed6bf8
//target is args[0]
target[0] is in 0x7ffe9aed6bf8 = 0x55000000660000
target[1] is in 0x7ffe9aed6c00 = 0x7ffe9aed6c50
//target is args[1]
target[0] is in 0x7ffe9aed6b64 = 0x9aed6bf899000000
target[1] is in 0x7ffe9aed6b6c = 0x7ffe
//target is args[2]
target[0] is in 0x7ffe9aed6d98 = 0x7ffe9aed79ab
target[1] is in 0x7ffe9aed6da0 = 0x7ffe9aed79bb
[PR] cudaDeviceSynchronize(), 5
------------------------------------------------------------------------------