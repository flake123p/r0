#
# https://pytorch.org/tutorials/recipes/recipes/defining_a_neural_network.html
# https://ithelp.ithome.com.tw/articles/10279986
#
import torch
import torch.nn as nn
import torch.nn.functional as F

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.fc = nn.Linear(3, 1)
        self.fc.weight.data = torch.ones(1, 3)
        
    def forward(self, x):
        return self.fc(x)

myNet = Net()
input = torch.tensor([1.0, 2.0, 3.0])
device = torch.device("cuda")

myNet = myNet.to(device)
input = input.to(device)

print(myNet.fc.weight.data)
print(myNet.fc.bias.data)
ans = myNet(input)
print(ans)

'''
cu list (30) =
    cuGetProcAddress
    cuDriverGetVersion
    cuInit
    cuGetExportTable
    cuModuleGetLoadingMode
    cuDeviceGetCount
    cuDeviceGet
    cuDeviceGetName
    cuDeviceTotalMem_v2
    cuDeviceGetAttribute
    cuDeviceGetUuid
    cuCtxGetDevice
    cuDevicePrimaryCtxGetState
    cuCtxGetCurrent
    cuCtxSetCurrent
    cuDevicePrimaryCtxRetain
    cuModuleGetGlobal_v2
    cuStreamIsCapturing
    cuMemAlloc_v2
    cuMemcpyHtoDAsync_v2
    cuStreamSynchronize
    cuModuleGetFunction
    cuLaunchKernel
    cuFuncGetAttribute
    cuMemcpyDtoHAsync_v2
    cuMemFree_v2
    cuEventCreate
    cuOccupancyMaxActiveBlocksPerMultiprocessorWithFlags
    cuModuleUnload
    cuDevicePrimaryCtxRelease

cuda list (12) =
    cudaGetDeviceCount
    cudaGetDevice
    cudaGetDeviceProperties
    cudaGetLastError
    cudaStreamIsCapturing
    cudaMalloc
    cudaMemcpyAsync
    cudaStreamSynchronize
    cudaLaunchKernel
    cudaFuncGetAttributes
    cudaPeekAtLastError
    cudaDeviceGetAttribute

cudnn list (0) =

cublas list (5) =
+   cublasCreate_v2
+   cublasSetStream_v2
+   cublasSetWorkspace_v2
+   cublasSetMathMode
+   cublasSgemm_v2

cublasLt list (3) =
*   cublasLtCtxInit
*   cublasLtSSSMatmulAlgoGetHeuristic
*   cublasLtSSSMatmul

+ = called by pytorch
* = called by cuda
'''
