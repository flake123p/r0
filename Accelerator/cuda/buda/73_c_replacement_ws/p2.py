
import torch
import torch.nn as nn

# a = torch.tensor([-2., -3.]).to('cuda')
# a = torch.abs(a)
# print(a)

# a = torch.tensor([2., 5., 8.]).to('cuda')
# b = torch.tensor([3.]).to('cuda')
# print(a*b)

# a = torch.tensor([2., 5., 8., 9.]).to('cuda')
# print(torch.pow(a, 2))

### _ZN2at6native13reduce_kernelILi512ELi1ENS0_8ReduceOpIfNS0_14func_wrapper_tIfNS0_13MaxNanFunctorIfEEEEjfLi4EEEEEvT1_
### void at::native::reduce_kernel<512, 1, at::native::ReduceOp<float, at::native::func_wrapper_t<float, at::native::MaxNanFunctor<float> >, unsigned int, float, 4> >(at::native::ReduceOp<float, at::native::func_wrapper_t<float, at::native::MaxNanFunctor<float> >, unsigned int, float, 4>)
a = torch.tensor([222., 41., 377.]).to('cuda')
print(torch.min(a))