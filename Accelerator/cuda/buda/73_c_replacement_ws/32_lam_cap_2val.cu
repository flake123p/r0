#include <stdio.h>

//
// Nearly minimal CUDA example.
// Compile with:
//
// nvcc -o example example.cu
//

// __global__ = cpu    call, run on device
// __device__ = device call, run on device
// __host__   = cpu    call, run on    cpu
#define BLOCK_NUM 1
#define THREAD_NUM 4

template<typename Func>
__global__ void myGlobal1234(Func f){
    f(0x3344);
}

int main() {
    int a = 0x00660000;
    int b = 0x22000000;

    auto mylam = [a,b] __device__ (int input) -> void {printf("0x%x\n", threadIdx.x + input + a + b);};
    
    myGlobal1234<<<BLOCK_NUM, THREAD_NUM>>>(mylam);
    cudaDeviceSynchronize();

    return 0;
}