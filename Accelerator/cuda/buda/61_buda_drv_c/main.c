#include <stdio.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

int main() {
    //
    // Create corresponding int arrays on the GPU.
    // ('d' stands for "device".)
    //
    int *da, *db;
    cudaMalloc((void **)&da, 10*sizeof(int));
    cudaMalloc((void **)&db, 10*sizeof(int));

    cudaFree(da);
    cudaFree(db);

    const char *str;
    cuGetErrorString(3, &str);

    printf("Hello Example , %s\n", str);

    return 0;
}