#!/bin/bash

rm lib*

gcc main.c -lcudart -lcuda

gcc -c buda_rt.c -fPIC
gcc -shared -o libcudart.so buda_rt.o

gcc -c buda_drv.c -fPIC
gcc -shared -o libcuda.so buda_drv.o

#
# ...
#
cp libcudart.so libcudart.so.11.0 -f
cp libcuda.so libcuda.so.1 -f

#objdump -S -drwC a.out>a.out.dump
