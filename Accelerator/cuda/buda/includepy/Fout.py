class Fout:
    def __init__(self, output_file, en=False):
        self.output_file = output_file
        self.enable = en
        self.enabled = False
        
    def enable(self, en):
        self.enable = en

    def write(self, input):
        self.dump(input)
        
    def dump(self, input):
        if self.enable:
            if self.enabled == False:
                self.f = open(self.output_file, "w")
                self.enabled = True
        if self.enable and self.enabled:
            self.f.write(input)