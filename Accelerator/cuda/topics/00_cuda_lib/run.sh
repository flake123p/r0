#!/bin/bash

gcc -c mem.c -fPIC

gcc -shared -o libcudart.so mem.o

#
# ...
#
cp libcudart.so libcudart.so.11.0

export LD_LIBRARY_PATH=. 

nvcc -c 00_example.cu


#nvcc 00_example.o -lcudart -L.
nvcc 00_example.o --cudart shared

objdump -S -drwC a.out>a.out.dump


#
# Set path for using our lib: export LD_LIBRARY_PATH=.
#
