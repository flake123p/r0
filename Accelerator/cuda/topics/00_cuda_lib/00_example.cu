#include <stdio.h>

int main() {
    //
    // Create corresponding int arrays on the GPU.
    // ('d' stands for "device".)
    //
    int *da, *db;
    cudaMalloc((void **)&da, 10*sizeof(int));
    cudaMalloc((void **)&db, 10*sizeof(int));

    cudaFree(da);
    cudaFree(db);

    printf("Hello Example\n");

    return 0;
}