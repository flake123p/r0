import tensorflow as tf

print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))
print("CPU List: ", tf.config.list_physical_devices('CPU'))
print("GPU List: ", tf.config.list_physical_devices('GPU'))
print("List all: \n", tf.config.list_physical_devices())
