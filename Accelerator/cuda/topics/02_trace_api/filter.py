class LtraceFilter:
    def __init__(self, file, dump=False):
        self.file = file
        self.f = open(file)
        if dump:
            self.fileOut = file + ".fltr.txt"
            self.fout = open(self.fileOut, "w")
        
        self.cudnn_list = []
        self.cublas_list = []
        self.cublasLt_list = []
        self.cu_list = []
        self.cuda_list = []
        self.unknown_list = []
        
        self.StartParsing()
        
    def StartParsing(self):
        for line in self.f:
            # filtering string start with "cu"
            if len(line) >= 2 and line[0:2] == "cu":
                pass
            else:
                continue
            
            # cut of "("
            words = line.split('(')
            line = words[0]
            
            # cut of "@"
            words = line.split('@')
            line = words[0]
            
            if len(line) >= 8 and line[0:8] == "cublasLt":
                if not line in self.cublasLt_list:
                    self.cublasLt_list.append(line)
            elif len(line) >= 6 and line[0:6] == "cublas":
                if not line in self.cublas_list:
                    self.cublas_list.append(line)
            elif len(line) >= 5 and line[0:5] == "cudnn":
                if not line in self.cudnn_list:
                    self.cudnn_list.append(line)
            elif len(line) >= 4 and line[0:4] == "cuda":
                if not line in self.cuda_list:
                    self.cuda_list.append(line)
            elif len(line) >= 2 and line[0:2] == "cu":
                if not line in self.cu_list:
                    self.cu_list.append(line)
            else:
                if not line in self.unknown_list:
                    self.unknown_list.append(line)
                    
    def Dump(self):
        print(f'cu list ({len(self.cu_list)}) =')
        for i in self.cu_list:
            print('   ', i)
        print("")
        
        print(f'cuda list ({len(self.cuda_list)}) =')
        for i in self.cuda_list:
            print('   ', i)
        print("")
        
        print(f'cudnn list ({len(self.cudnn_list)}) =')
        for i in self.cudnn_list:
            print('   ', i)
        print("")
        
        print(f'cublas list ({len(self.cublas_list)}) =')
        for i in self.cublas_list:
            print('   ', i)
        print("")
        
        print(f'cublasLt list ({len(self.cublasLt_list)}) =')
        for i in self.cublasLt_list:
            print('   ', i)
        print("")
        
        if len(self.unknown_list):
            print(f'unknown list ({len(self.unknown_list)}) =')
            for i in self.unknown_list:
                print('   ', i)
            print("")

if __name__ == '__main__':
    import sys
    file_pth = "__dummy_ltrace_pth.txt"
    file_tf = "__dummy_ltrace_tf.txt"
    
    # pth = LtraceFilter(file_pth)
    # pth.Dump()

    # tf = LtraceFilter(file_tf)
    # tf.Dump()
    
    # tf = LtraceFilter("__dummy_ltrace.txt")
    # tf.Dump()
    print('Number of arguments:', len(sys.argv), 'arguments.')
    print ('Argument List:', str(sys.argv))
    
    if len(sys.argv) >= 2:
        obj = LtraceFilter(sys.argv[1])
        obj.Dump()