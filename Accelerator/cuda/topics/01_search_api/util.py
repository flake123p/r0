
class TxtParser:
    def __init__(self, fileStr, outTag=''):
        self.fileStr = fileStr
        self.f = open(fileStr)
        self.fout = None
        self.outTag = outTag
        
        if self.fileStr == "api_list_cuda_drv.txt":
            self.style = 1
        elif self.fileStr == "api_list_cudart.txt":
            self.style = 1
        elif self.fileStr == "api_list_cudnn.txt":
            self.style = 2
        else:
            self.style = 0
            
        self.all_key_string = []

    def SplitAndFilter(self, str):
        # Drop "......."
        each_word_list = str.split("...")
        if len(each_word_list) == 0:
            return []
        
        each_word_list = each_word_list[0].split()
        if len(each_word_list) == 0:
            return []
        if each_word_list[0] == "#":
            return []
        if each_word_list[0] == "```":
            return []
        if len(each_word_list) == 2 and len(each_word_list[1]) >= 3:
            if each_word_list[1][-1] == ')' and each_word_list[1][-2] == '(':
                each_word_list[1] = each_word_list[1][0:-2]
            elif each_word_list[1][-1] == '(':
                each_word_list[1] = each_word_list[1][0:-1]
        return each_word_list

    def StartParsing(self):
        self.all_key_string = []
        if self.style == 0:
            return
        elif self.style == 1:
            for each_line in self.f:
                each_word_list = self.SplitAndFilter(each_line)
                if len(each_word_list):
                    #print(each_word_list)
                    self.all_key_string.append(each_word_list)
        elif self.style == 2:
            for each_line in self.f:
                each_word_list = self.SplitAndFilter(each_line)
                if len(each_word_list):
                    #print(each_word_list)
                    self.all_key_string.append(each_word_list)
                    
    def IsRealFunc(self, str):
        ret = 0
        if self.style == 1:
            if len(str) >= 2 and str[0:2] == "cu":
                ret = 1
        elif self.style == 2:
            if len(str) >= 5 and str[0:5] == "cudnn":
                ret = 1
        return ret
        
    # return is_title title_list[] func_prefix func_name
    def GetKeyString(self, string_list):
        is_title = 0
        title_list = []
        func_prefix = ""
        func_name = ""
        is_real_func = 0
        
        if self.style == 1:
            if len(string_list) > 1:
                is_title = 1
                title_list = string_list
            else:
                is_title = 0
                func_name = string_list[0]
                is_real_func = self.IsRealFunc(func_name)
        elif self.style == 2:
            if string_list[0] == "###":
                is_title = 1
                title_list = string_list[1:]
            elif len(string_list) == 2 and string_list[0] != "###":
                is_title = 0
                func_prefix = string_list[0]
                func_name = string_list[1]
                is_real_func = self.IsRealFunc(func_name)
            
        return is_title, title_list, func_prefix, func_name, is_real_func
    
    # [DEPRECATED]
    # def WriteStringListToDumpFile(self, str_list, nextLine = False):
    #     if self.fout == None:
    #         self.fout = open(self.fileStr + self.outTag + ".dump", 'w')
    #     whole_str = ""
    #     for s in str_list:
    #         whole_str = whole_str + s + ' '
    #     if nextLine:
    #         whole_str = whole_str + "\n"    
    #     self.fout.write(whole_str)

if __name__ == '__main__':
    a = TxtParser("api_list_cuda_drv.txt")
    a.StartParsing()
    b = TxtParser("api_list_cudart.txt")
    b.StartParsing()
    c = TxtParser("api_list_cudnn.txt")
    c.StartParsing()
    
    for s in b.all_key_string:
        is_title, title_list, func_prefix, func_name = b.GetKeyString(s)
        if is_title:
            print("[TITLE]", title_list)
        else:
            print("[FUNCTION]", func_prefix, func_name)
            break
            
            

