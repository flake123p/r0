from util import *
import subprocess

class CmdAgent:
    def __init__(self):
        #self.cmd = ["find", "LOC", "-type", "f", "-exec", "grep", "-m", "1", "KEY", '{}', '/dev/null', ';']
        #self.cmd = ["find", "LOC", "-type", "f"]
        self.found = 0
        self.cmd_list_files = ["find", "FIND_PATH", "-type", "f"]
        self.all_files = []
        
        self.cmd_grep_string = ["grep", "-m", "1", "KEY", "FILE"]
        self.grep_result = []
        
    def get_all_files(self, findPath):
        self.cmd_list_files[1] = findPath
        self.all_files = []
        temp = subprocess.run(self.cmd_list_files, stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')
        for t in temp:
            c_ext = ["c", "cc", "cu", "cpp", "cxx", "h", "hpp", "hxx", "hh", "inc"]
            ts = t.split(".")
            if len(ts) >= 2 and ts[1] in c_ext:
                self.all_files.append(t)

    def run_grep_string(self, keyString):
        self.cmd_grep_string[3] = keyString
        self.found = 0
        
        for f in self.all_files:
            self.cmd_grep_string[4] = f
            self.grep_result = subprocess.run(self.cmd_grep_string, stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')
            if len(self.grep_result) == 1 and self.grep_result[0] == "":
                self.found = 0
            else:
                self.found = 1
                break



def Find_And_Dump(txtParser: TxtParser, findPath, outTag=".dump"):
    #
    # inline function
    #
    def StringListToFile(str_list, nextLine = False):
        whole_str = ""
        for s in str_list:
            whole_str = whole_str + s + ' '
        if nextLine:
            whole_str = whole_str + "\n"    
        fout.write(whole_str)

    #
    #
    #
    cmd = CmdAgent()
    cmd.get_all_files(findPath)
    
    #
    # fout init
    #
    fout_name = txtParser.fileStr + outTag
    fout = open(fout_name, 'w')
    
    #
    # parse every string
    #
    total_func_count = 0
    found_func_count = 0
    found_real_func_count = 0
    curr = txtParser
    for s in curr.all_key_string:
        is_title, title_list, func_prefix, func_name, is_real_func = curr.GetKeyString(s)
        if is_title:
            print("[TITLE]", title_list)
            StringListToFile(["[TITLE]"])
            StringListToFile(title_list, nextLine=True)
        else:
            total_func_count += 1
            cmd.run_grep_string(func_name)
            if cmd.found:
                found = "[1]"
                found_func_count += 1
                if is_real_func:
                    real = "<1>"
                    found_real_func_count += 1
                else:
                    real = "<0>"
            else:
                found = "[0]"
            print("[FUNCTION]", func_prefix, func_name, found, real)
            StringListToFile(["[FUNCTION]"])
            if func_prefix == "":
                func_prefix = '---'
            StringListToFile([found, real, func_prefix, func_name], nextLine=True)

    
    result = \
        "Found = " + str(found_func_count) + \
        ", Real = " + str(found_real_func_count) + \
        ", Total = " + str(total_func_count)
    StringListToFile([result], nextLine=True)
            
if __name__ == '__main__':
    # tf_path = "/home/pupil/sda/ws/source_tf/tensorflow"
    # pth_path = "/home/pupil/sda/ws/source_pth/pytorch"
    tf_path = "/home/pupil/temp/source_tf/tensorflow"
    pth_path = "/home/pupil/temp/source_pth/pytorch"
    
    drv = TxtParser("api_list_cuda_drv.txt")
    drv.StartParsing()
    rt = TxtParser("api_list_cudart.txt")
    rt.StartParsing()
    dnn = TxtParser("api_list_cudnn.txt")
    dnn.StartParsing()
    
    # Find_And_Dump(drv, tf_path, outTag=".tf.dump")
    # Find_And_Dump(rt, tf_path, outTag=".tf.dump")
    # Find_And_Dump(dnn, tf_path, outTag=".tf.dump")
    # Find_And_Dump(drv, pth_path, outTag=".pth.dump")
    # Find_And_Dump(rt, pth_path, outTag=".pth.dump")
    # Find_And_Dump(dnn, pth_path, outTag=".pth.dump")

