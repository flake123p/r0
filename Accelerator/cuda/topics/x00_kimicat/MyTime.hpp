#ifndef __MY_TIME_HPP__
#define __MY_TIME_HPP__

#include <time.h>
#include <stdio.h>

struct mytime {
    /*
        Usage: 
        struct mytime myt;
        ...
        myt.show();
    */
    clock_t curr;
    mytime(){curr = 0; start();};
    void start(){curr = clock();}
    void show(int enNewLine = 1) {
        auto diff = clock() - curr;
        printf ("It took me %4d clicks (%f seconds).", (int)diff, ((double)diff)/CLOCKS_PER_SEC);
        if (enNewLine) {
            printf("\n");
        }
    }
};

#endif //__MY_TIME_HPP__