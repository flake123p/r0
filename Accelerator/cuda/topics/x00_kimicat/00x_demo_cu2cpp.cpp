#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "MyTime.hpp"

#define N 4

struct {
    int x;
} blockIdx;

void add_default(int *a, int *b) {
    int i = blockIdx.x;
    if (i<N) {
        b[i] = 2*a[i];
    }
}

void add_default_dispatch(int loop, int *a, int *b) {
    for (int i = 0; i < loop; i++) {
        blockIdx.x = i;
        add_default(a, b);
    }
}

int main() {
    int ha[N], hb[N];
    int *da, *db;
    da = (int *)malloc(N*sizeof(int));
    db = (int *)malloc(N*sizeof(int));
    for (int i = 0; i<N; ++i) {
        ha[i] = i;
    }
    memcpy(da, ha, N*sizeof(int));

    mytime myt;
    //add_default<<<N, 1>>>(da, db);
    add_default_dispatch(4, da, db);
    myt.show();

    memcpy(hb, db, N*sizeof(int));
    for (int i = 0; i<N; ++i) {
        printf("%d\n", hb[i]);
    }

    free(da);
    free(db);

    return 0;
}