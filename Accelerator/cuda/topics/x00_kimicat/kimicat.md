# Page:
http://www2.kimicat.com/gpu%E7%9A%84%E7%A1%AC%E9%AB%94%E6%9E%B6%E6%A7%8B

--

## 1 Grid for 1 Program

grid     | grid     | grid     | grid     |
program0 | program1 | program2 | program3 |

--

## Multi Block = 1 Grid

Share Code
Not Share Data

--

## Multi Thread = 1 Block

Share Data (share memory)
Not Share Register
Not Share local memory
--

## Thread
Owned Register
local memory

--

## CUDA execution unit = WARP
1    warp : 32 threads
half warp : 16 threads

--

## Multiprocessor & Shared Memory
Has registers (e.g.: 8192)
Distribute to threads
if 1 thread use 16 registers then the limit is 512 threads at top

Has shared memory (e.g.: 16K, 16 Bank)

--

## BANK CONFLICT
1 bank for 1 thread or it will delay by BANK CONFLICT !!!!

Bank unit: 4 bytes e.g:
    __shared__ int data[128];

Conflict Code:
    int number = data[base + 4 * tid];

No Conflict Code:
    int number = data[16 * tid];

--

## Reading Broadcast (no conflict)

e.g.:
    int number = data[3];

--

## Global Memory

No cache
Prefer big (DRAM feature)
Prefer aligned(start address) and continuous (COALESCED)

COALESCED:
    start address = 16 * (bytes read by thread)

Prefered bytes read by thread(CUDA 1.1)
    32  bits - 4  bytes (best performance)
    64  bits - 8  bytes 
    128 bits - 16 bytes 

Use align directive:
    struct __align__(16) vec3d { float x, y, z; };

Or read it to share memory to bypass struct size problem

--

## Memory Summary
Shared Memory - avoid bank conflict
Global Memory - prefer coalesce

--

## Texture
Read only (global memory can be use to read/write)
Trick: fast conversion from 32 bits RGBA to 4 32 bites float (texture filtering - bilinear filtering)

--

## OP Unit in Stream Processor: fmad
....... integer mul can't run on full speed
24 bits integer mul can   run on full speed

float division : reciprocal & multiply = low precision (can't meet IEEE754)

__fdividef(x,y) is fast, but error when 2^216 < y < 2^218

--

## CUDA low accurarcy func

__expf、__logf、__sinf、__cosf、__powf ...

fast
low accurarcy

--

## cudaMallocHost

normal memory -> floating by OS

Use cudaMallocHost() get a page locked memory, then copy to CUDA

--


shader = multiprocessors x n

multiprocessor = stream processors x 4 x 2

也就是說實際上可以看成是有兩組 4D 的 SIMD 處理器。

此外，每個 multiprocessor 還具有 8192 個暫存器，

16KB 的 share memory，以及 texture cache 和 constant cache。

![](http://www2.kimicat.com/cuda_mp-medium-init-.jpg)

multiprocessor info:
from cudaGetDeviceProperties() 
or cuDeviceGetProperties()

