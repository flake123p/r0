#include <stdio.h>
#include "MyTime.hpp"

#define N 4

__device__
void dev_add(int *a, int *b) {
    int i = blockIdx.x;
    if (i<N) {
        b[i] = 2*a[i];
    }
}

__global__
void add(int *a, int *b) {
    int i = blockIdx.x;
    printf("glo add, %d\n", i);

    dev_add(a, b);
}

__global__
void add_default(int *a, int *b) {
    int i = blockIdx.x;
    if (i<N) {
        b[i] = 2*a[i];
    }
}

int main() {
    int ha[N], hb[N];
    int *da, *db;
    cudaMalloc((void **)&da, N*sizeof(int));
    cudaMalloc((void **)&db, N*sizeof(int));
    for (int i = 0; i<N; ++i) {
        ha[i] = i;
    }
    cudaMemcpy(da, ha, N*sizeof(int), cudaMemcpyHostToDevice);

    mytime myt;
    add_default<<<N, 1>>>(da, db);
    myt.show();

    cudaMemcpy(hb, db, N*sizeof(int), cudaMemcpyDeviceToHost);
    for (int i = 0; i<N; ++i) {
        printf("%d\n", hb[i]);
    }

    cudaFree(da);
    cudaFree(db);

    return 0;
}