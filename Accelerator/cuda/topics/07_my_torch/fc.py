import torch
import torch.nn as nn
class FullyConnectedLayer(nn.Module):
    def __init__(self, input_dim, output_dim):
        super(FullyConnectedLayer, self).__init__()
        gpu_dev = torch.device("cuda")
        self.linear = nn.Linear(input_dim, output_dim).to(gpu_dev)
    def forward(self, x):
        return self.linear(x)

gpu_dev = torch.device("cuda")

# Create an instance of the fully connected layer
input_dim = 64
output_dim = 128
fc_layer = FullyConnectedLayer(input_dim, output_dim)
# Generate example input tensor
example_input = torch.randn(8, input_dim).to(gpu_dev)
# Get the output from the fully connected layer
output = fc_layer(example_input)
# Print input shape
print("Input shape:", example_input.shape)  # (8, 64)
# Print output shape
print("Output shape:", output.shape)  # (8, 128)

'''
nvprof (py39)

==523211== Profiling result:
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:   64.67%  8.9610us         1  8.9610us  8.9610us  8.9610us  void gemmSN_TN_kernel<float, int=128, int=16, int=2, int=4, int=8, int=9, bool=0, cublasGemvTensorStridedBatched<float const >, cublasGemvTensorStridedBatched<float const >, cublasGemvTensorStridedBatched<float>>(cublasGemmSmallNParams<float const , cublasGemvTensorStridedBatched<float const >, cublasGemvTensorStridedBatched<float const >, float>)
                   35.33%  4.8960us         3  1.6320us     608ns  3.5840us  [CUDA memcpy HtoD]
      API calls:   94.44%  5.41033s         1  5.41033s  5.41033s  5.41033s  cudaDeviceGetStreamPriorityRange
                    5.54%  317.63ms         2  158.81ms  145.17ms  172.46ms  cudaFree
                    0.01%  422.56us         5  84.512us  3.5330us  142.24us  cudaMalloc
                    0.00%  264.17us       297     889ns     113ns  47.924us  cuDeviceGetAttribute
                    0.00%  142.15us       746     190ns     124ns     879ns  cuGetProcAddress
                    0.00%  56.509us         3  18.836us  16.932us  21.516us  cuDeviceGetName
                    0.00%  56.479us         1  56.479us  56.479us  56.479us  cudaGetDeviceProperties
                    0.00%  40.217us         1  40.217us  40.217us  40.217us  cudaLaunchKernel
                    0.00%  31.387us         3  10.462us  7.1740us  16.290us  cudaMemcpyAsync
                    0.00%  29.393us        52     565ns     235ns  3.9880us  cudaGetDevice
                    0.00%  17.282us        18     960ns     345ns  10.525us  cudaEventCreateWithFlags
                    0.00%  15.652us         3  5.2170us  3.6870us  8.0870us  cudaStreamSynchronize
                    0.00%  11.437us         2  5.7180us  3.1740us  8.2630us  cudaStreamIsCapturing
                    0.00%  6.4450us         1  6.4450us  6.4450us  6.4450us  cuDeviceGetPCIBusId
                    0.00%  4.8730us         3  1.6240us     107ns  4.5170us  cudaGetDeviceCount
                    0.00%  4.5930us        14     328ns     229ns  1.1310us  cudaDeviceGetAttribute
                    0.00%  2.7750us         2  1.3870us  1.2660us  1.5090us  cuInit
                    0.00%  2.7510us         2  1.3750us     412ns  2.3390us  cudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags
                    0.00%  2.1980us         5     439ns     158ns  1.4120us  cuDeviceGetCount
                    0.00%  2.1690us         3     723ns     419ns  1.0480us  cudaGetLastError
                    0.00%     956ns         3     318ns     273ns     364ns  cuDeviceTotalMem
                    0.00%     799ns         4     199ns     117ns     346ns  cuDeviceGet
                    0.00%     704ns         3     234ns     123ns     454ns  cuDevicePrimaryCtxGetState
                    0.00%     636ns         1     636ns     636ns     636ns  cudaGetSymbolAddress
                    0.00%     563ns         3     187ns     165ns     215ns  cuDeviceGetUuid
                    0.00%     286ns         2     143ns     134ns     152ns  cuDriverGetVersion
'''