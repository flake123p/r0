#
# Run this on py39 env
#
import torch

print(torch.cuda.is_available())

gpu = torch.device("cuda:0")

a = torch.tensor([31.0], device=gpu)
b = torch.tensor([22.0], device=gpu)
c = b + a
print(c)
quit()
import torch

print('1')
print('2')
print(torch.cuda.is_available())
print('3')

device = torch.device("cuda:0")

a = torch.tensor([31.0], device=device)
print('$ a tensor to GPU Done')
b = torch.tensor([22.0], device=device)
print('$ b tensor to GPU Done')

if 1:
    # gpu_add = a + b ########################## This is def 1
    # c = gpu_add.to('cpu')
    # print(c)
    c = b + a   ########################## This is def 0
    print(c)
else:
    b = a.clone()
    c = b.to('cpu')
    print(c)
'''
cu list (27) =
    cuGetProcAddress
    cuDriverGetVersion
    cuInit
    cuGetExportTable
    cuModuleGetLoadingMode
    cuDeviceGetCount
    cuDeviceGet
    cuDeviceGetName
    cuDeviceTotalMem_v2
    cuDeviceGetAttribute
    cuDeviceGetUuid
    cuCtxGetDevice
    cuDevicePrimaryCtxGetState
    cuCtxGetCurrent
    cuCtxSetCurrent
    cuDevicePrimaryCtxRetain
    cuModuleGetFunction
    cuModuleGetGlobal_v2
    cuStreamIsCapturing
    cuMemAlloc_v2
    cuMemcpyHtoDAsync_v2
    cuStreamSynchronize
    cuLaunchKernel
    cuFuncGetAttribute
    cuMemcpyDtoHAsync_v2
    cuModuleUnload
    cuDevicePrimaryCtxRelease

cuda list (12) =
    cudaGetDeviceCount
    cudaGetDevice
    cudaGetDeviceProperties
    cudaGetLastError
    cudaStreamIsCapturing
    cudaMalloc
    cudaMemcpyAsync
    cudaStreamSynchronize
    cudaLaunchKernel
    cudaFuncGetAttributes
    cudaPeekAtLastError
    cudaDeviceGetAttribute
    

Add func:
    cublasGemmStridedBatchedEx()
    
    cublasLtMatrixTransform()
    
    _ZN2at6native29vectorized_elementwise_kernelILi4ENS0_15CUDAFunctor_addIfEENS_6detail5ArrayIPcLi3EEEEEviT0_T1_()

nvprof (py39):
==521263== Profiling result:
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:    9.95%  6.6560us         1  6.6560us  6.6560us  6.6560us  void at::native::reduce_kernel<int=512, int=1, at::native::ReduceOp<double, at::native::func_wrapper_t<double, at::native::MinNanFunctor<double>>, unsigned int, double, int=4>>(double)
                    9.71%  6.4970us         1  6.4970us  6.4970us  6.4970us  void at::native::reduce_kernel<int=512, int=1, at::native::ReduceOp<double, at::native::func_wrapper_t<double, at::native::MaxNanFunctor<double>>, unsigned int, double, int=4>>(double)
                    6.94%  4.6410us         2  2.3200us  1.7600us  2.8810us  _ZN2at6native29vectorized_elementwise_kernelILi4EZNS0_21compare_scalar_kernelIdEEvRNS_18TensorIteratorBaseENS0_50_GLOBAL__N__89c782d4_17_CompareKernels_cu_8f1b29aa6OpTypeET_EUldE_NS_6detail5ArrayIPcLi2EEEEEviT0_T1_
                    6.74%  4.5130us         2  2.2560us  2.0170us  2.4960us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<float, float, bool, at::native::_GLOBAL__N__ca1a8271_18_CompareEQKernel_cu_d8008c96::CompareEqFunctor<float>>, at::detail::Array<char*, int=3>>(int, float, float)
                    6.74%  4.5120us         6     752ns     672ns     960ns  [CUDA memcpy DtoH]
                    6.41%  4.2880us         2  2.1440us  1.7920us  2.4960us  void at::native::vectorized_elementwise_kernel<int=4, at::native::AbsFunctor<float>, at::detail::Array<char*, int=2>>(int, float, at::native::AbsFunctor<float>)
                    6.36%  4.2560us         1  4.2560us  4.2560us  4.2560us  _ZN2at6native24index_elementwise_kernelILi128ELi4EZNS0_16gpu_index_kernelIZNS0_17index_kernel_implINS0_10OpaqueTypeILi4EEEEEvRNS_18TensorIteratorBaseEN3c108ArrayRefIlEESA_EUlPcSB_lE_EEvS7_SA_SA_RKT_EUliE_EEviT1_
                    6.12%  4.0960us         2  2.0480us  1.6960us  2.4000us  void at::native::vectorized_elementwise_kernel<int=4, at::native::AUnaryFunctor<float, float, bool, at::native::_GLOBAL__N__ca1a8271_18_CompareEQKernel_cu_d8008c96::CompareEqFunctor<float>>, at::detail::Array<char*, int=2>>(int, float, float)
                    5.45%  3.6480us         1  3.6480us  3.6480us  3.6480us  _ZN2at6native27unrolled_elementwise_kernelIZZZNS0_23direct_copy_kernel_cudaERNS_18TensorIteratorBaseEENKUlvE0_clEvENKUlvE4_clEvEUldE_NS_6detail5ArrayIPcLi2EEE23TrivialOffsetCalculatorILi1EjESC_NS0_6memory12LoadWithCastILi1EEENSD_13StoreWithCastILi1EEEEEviT_T0_T1_T2_T3_T4_
                    5.16%  3.4560us         1  3.4560us  3.4560us  3.4560us  void at_cuda_detail::cub::DeviceSelectSweepKernel<at_cuda_detail::cub::DispatchSelectIf<at_cuda_detail::cub::CountingInputIterator<long, long>, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__08acd284_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool const *, long>, long*, int*, at_cuda_detail::cub::NullType, at_cuda_detail::cub::NullType, int, bool=0>::PtxSelectIfPolicyT, at_cuda_detail::cub::CountingInputIterator<long, long>, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__08acd284_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool const *, long>, long*, int*, at_cuda_detail::cub::ScanTileState<int, bool=1>, at_cuda_detail::cub::NullType, at_cuda_detail::cub::NullType, int, bool=0>(long, at_cuda_detail::cub::CountingInputIterator<long, long>, bool, bool, at::native::_GLOBAL__N__08acd284_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool const *, long, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__08acd284_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool const *, long>, int)
                    5.07%  3.3920us         1  3.3920us  3.3920us  3.3920us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<double, double, double, at::native::binary_internal::DivFunctor<double>>, at::detail::Array<char*, int=3>>(int, double, double)
                    5.02%  3.3600us         1  3.3600us  3.3600us  3.3600us  void at_cuda_detail::cub::DeviceReduceSingleTileKernel<at_cuda_detail::cub::DeviceReducePolicy<bool, int, int, at_cuda_detail::cub::Sum>::Policy600, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__08acd284_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool const *, long>, int*, int, at_cuda_detail::cub::Sum, int>(int, int, at_cuda_detail::cub::Sum, at_cuda_detail::cub::DeviceReducePolicy<bool, int, int, at_cuda_detail::cub::Sum>::Policy600, bool)
                    4.50%  3.0080us         1  3.0080us  3.0080us  3.0080us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<bool, bool, bool, at::native::BitwiseAndFunctor<bool>>, at::detail::Array<char*, int=3>>(int, bool, bool)
                    4.11%  2.7520us         1  2.7520us  2.7520us  2.7520us  void at::native::vectorized_elementwise_kernel<int=4, at::native::CUDAFunctor_add<float>, at::detail::Array<char*, int=3>>(int, float, at::native::CUDAFunctor_add<float>)
                    3.73%  2.4970us         1  2.4970us  2.4970us  2.4970us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<bool, bool, bool, at::native::binary_internal::MulFunctor<bool>>, at::detail::Array<char*, int=3>>(int, bool, bool)
                    3.01%  2.0160us         1  2.0160us  2.0160us  2.0160us  _ZN2at6native29vectorized_elementwise_kernelILi4EZZZNS0_16ceil_kernel_cudaERNS_18TensorIteratorBaseEENKUlvE_clEvENKUlvE0_clEvEUlfE_NS_6detail5ArrayIPcLi2EEEEEviT0_T1_
                    2.73%  1.8250us         1  1.8250us  1.8250us  1.8250us  void at_cuda_detail::cub::DeviceCompactInitKernel<at_cuda_detail::cub::ScanTileState<int, bool=1>, int*>(int, int, bool=1)
                    2.25%  1.5040us         2     752ns     544ns     960ns  [CUDA memcpy HtoD]
      API calls:   99.99%  5.47343s         1  5.47343s  5.47343s  5.47343s  cudaDeviceGetStreamPriorityRange
                    0.00%  217.49us        20  10.874us  4.7610us  38.392us  cudaLaunchKernel
                    0.00%  145.43us         1  145.43us  145.43us  145.43us  cudaMalloc
                    0.00%  98.561us         8  12.320us  8.3870us  17.025us  cudaMemcpyAsync
                    0.00%  86.080us       101     852ns     112ns  37.444us  cuDeviceGetAttribute
                    0.00%  78.066us       191     408ns     227ns  3.8980us  cudaGetDevice
                    0.00%  57.640us         1  57.640us  57.640us  57.640us  cudaGetDeviceProperties
                    0.00%  25.325us         8  3.1650us  1.7810us  5.7120us  cudaStreamSynchronize
                    0.00%  20.701us         1  20.701us  20.701us  20.701us  cuDeviceGetName
                    0.00%  10.958us         1  10.958us  10.958us  10.958us  cudaFuncGetAttributes
                    0.00%  9.2500us        49     188ns     113ns  1.2880us  cudaGetLastError
                    0.00%  8.7580us         1  8.7580us  8.7580us  8.7580us  cudaStreamIsCapturing
                    0.00%  5.6590us         1  5.6590us  5.6590us  5.6590us  cuDeviceGetPCIBusId
                    0.00%  4.9470us         4  1.2360us     119ns  4.1340us  cudaGetDeviceCount
                    0.00%  1.1000us         3     366ns     177ns     745ns  cuDeviceGetCount
                    0.00%  1.0920us         1  1.0920us  1.0920us  1.0920us  cudaDeviceGetAttribute
                    0.00%     984ns         6     164ns     115ns     263ns  cudaPeekAtLastError
                    0.00%     824ns         3     274ns     120ns     569ns  cuDevicePrimaryCtxGetState
                    0.00%     589ns         2     294ns     116ns     473ns  cuDeviceGet
                    0.00%     385ns         1     385ns     385ns     385ns  cuDeviceTotalMem
                    0.00%     236ns         1     236ns     236ns     236ns  cuDeviceGetUuid
'''