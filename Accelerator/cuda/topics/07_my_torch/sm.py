import torch
import torch.nn.functional as F
# Using F.softmax
def softmax_with_torch(x):
    return F.softmax(x, dim=-1)
# Example input tensor
input_tensor = torch.tensor([2.0, 1.0, 0.1]).to('cuda')
output = softmax_with_torch(input_tensor)
print('[Debug].....: F.softmax:                ', output)
# Using basic math to implement softmax
def softmax_basic(x):
    e_x = torch.exp(x - torch.max(x))
    return e_x / e_x.sum()
# Example input tensor
# input_tensor = torch.tensor([2.0, 1.0, 0.1])
output = softmax_basic(input_tensor)
print('[Debug].....: softmax(basic math):      ', output)

'''
cu list (27) =
    cuGetProcAddress
    cuDriverGetVersion
    cuInit
    cuGetExportTable
    cuModuleGetLoadingMode
    cuDeviceGetCount
    cuDeviceGet
    cuDeviceGetName
    cuDeviceTotalMem_v2
    cuDeviceGetAttribute
    cuDeviceGetUuid
    cuCtxGetDevice
    cuDevicePrimaryCtxGetState
    cuCtxGetCurrent
    cuCtxSetCurrent
    cuDevicePrimaryCtxRetain
    cuModuleGetGlobal_v2
    cuStreamIsCapturing
    cuMemAlloc_v2
    cuMemcpyHtoDAsync_v2
    cuStreamSynchronize
    cuModuleGetFunction
    cuLaunchKernel
    cuFuncGetAttribute
    cuMemcpyDtoHAsync_v2
    cuModuleUnload
    cuDevicePrimaryCtxRelease

cuda list (12) =
    cudaGetDeviceCount
    cudaGetDevice
    cudaGetDeviceProperties
    cudaGetLastError
    cudaStreamIsCapturing
    cudaMalloc
    cudaMemcpyAsync
    cudaStreamSynchronize
    cudaLaunchKernel
    cudaFuncGetAttributes
    cudaPeekAtLastError
    cudaDeviceGetAttribute

cudnn list (0) =

cublas list (0) =

cublasLt list (0) =
'''

'''
==568618== Profiling result:
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:    8.69%  12.641us        18     702ns     640ns     960ns  [CUDA memcpy DtoH]
                    8.31%  12.097us         2  6.0480us  4.7040us  7.3930us  void at::native::reduce_kernel<int=512, int=1, at::native::ReduceOp<double, at::native::func_wrapper_t<double, at::native::MaxNanFunctor<double>>, unsigned int, double, int=4>>(double)
                    8.25%  12.003us         6  2.0000us  1.7280us  3.0080us  _ZN2at6native29vectorized_elementwise_kernelILi4EZNS0_21compare_scalar_kernelIdEEvRNS_18TensorIteratorBaseENS0_50_GLOBAL__N__89c782d4_17_CompareKernels_cu_8f1b29aa6OpTypeET_EUldE_NS_6detail5ArrayIPcLi2EEEEEviT0_T1_
                    8.18%  11.906us         2  5.9530us  4.8010us  7.1050us  void at::native::reduce_kernel<int=512, int=1, at::native::ReduceOp<double, at::native::func_wrapper_t<double, at::native::MinNanFunctor<double>>, unsigned int, double, int=4>>(double)
                    5.87%  8.5460us         4  2.1360us  1.9210us  2.6560us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<float, float, bool, at::native::_GLOBAL__N__ca1a8271_18_CompareEQKernel_cu_d8008c96::CompareEqFunctor<float>>, at::detail::Array<char*, int=3>>(int, float, float)
                    5.63%  8.1940us         4  2.0480us  1.6960us  2.7530us  void at::native::vectorized_elementwise_kernel<int=4, at::native::AUnaryFunctor<float, float, bool, at::native::_GLOBAL__N__ca1a8271_18_CompareEQKernel_cu_d8008c96::CompareEqFunctor<float>>, at::detail::Array<char*, int=2>>(int, float, float)
                    5.41%  7.8720us         4  1.9680us  1.6960us  2.5600us  void at::native::vectorized_elementwise_kernel<int=4, at::native::AbsFunctor<float>, at::detail::Array<char*, int=2>>(int, float, at::native::AbsFunctor<float>)
                    4.84%  7.0410us         1  7.0410us  7.0410us  7.0410us  void at::native::reduce_kernel<int=512, int=1, at::native::ReduceOp<float, at::native::func_wrapper_t<float, at::native::MaxNanFunctor<float>>, unsigned int, float, int=4>>(float)
                    4.35%  6.3370us         1  6.3370us  6.3370us  6.3370us  _ZN2at6native13reduce_kernelILi512ELi1ENS0_8ReduceOpIfNS0_14func_wrapper_tIfZNS0_11sum_functorIfffEclERNS_14TensorIteratorEEUlffE_EEjfLi4EEEEEvT1_
                    4.31%  6.2720us         2  3.1360us  2.8800us  3.3920us  _ZN2at6native24index_elementwise_kernelILi128ELi4EZNS0_16gpu_index_kernelIZNS0_17index_kernel_implINS0_10OpaqueTypeILi4EEEEEvRNS_18TensorIteratorBaseEN3c108ArrayRefIlEESA_EUlPcSB_lE_EEvS7_SA_SA_RKT_EUliE_EEviT1_
                    4.22%  6.1450us         2  3.0720us  2.6890us  3.4560us  void at_cuda_detail::cub::DeviceSelectSweepKernel<at_cuda_detail::cub::DispatchSelectIf<at_cuda_detail::cub::CountingInputIterator<long, long>, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__08acd284_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool const *, long>, long*, int*, at_cuda_detail::cub::NullType, at_cuda_detail::cub::NullType, int, bool=0>::PtxSelectIfPolicyT, at_cuda_detail::cub::CountingInputIterator<long, long>, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__08acd284_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool const *, long>, long*, int*, at_cuda_detail::cub::ScanTileState<int, bool=1>, at_cuda_detail::cub::NullType, at_cuda_detail::cub::NullType, int, bool=0>(long, at_cuda_detail::cub::CountingInputIterator<long, long>, bool, bool, at::native::_GLOBAL__N__08acd284_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool const *, long, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__08acd284_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool const *, long>, int)
                    4.18%  6.0820us         2  3.0410us  2.4970us  3.5850us  void at_cuda_detail::cub::DeviceReduceSingleTileKernel<at_cuda_detail::cub::DeviceReducePolicy<bool, int, int, at_cuda_detail::cub::Sum>::Policy600, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__08acd284_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool const *, long>, int*, int, at_cuda_detail::cub::Sum, int>(int, int, at_cuda_detail::cub::Sum, at_cuda_detail::cub::DeviceReducePolicy<bool, int, int, at_cuda_detail::cub::Sum>::Policy600, bool)
                    4.02%  5.8570us         2  2.9280us  2.4970us  3.3600us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<double, double, double, at::native::binary_internal::DivFunctor<double>>, at::detail::Array<char*, int=3>>(int, double, double)
                    4.02%  5.8570us         2  2.9280us  2.5600us  3.2970us  _ZN2at6native27unrolled_elementwise_kernelIZZZNS0_23direct_copy_kernel_cudaERNS_18TensorIteratorBaseEENKUlvE0_clEvENKUlvE4_clEvEUldE_NS_6detail5ArrayIPcLi2EEE23TrivialOffsetCalculatorILi1EjESC_NS0_6memory12LoadWithCastILi1EEENSD_13StoreWithCastILi1EEEEEviT_T0_T1_T2_T3_T4_
                    3.32%  4.8330us         2  2.4160us  1.9530us  2.8800us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<bool, bool, bool, at::native::BitwiseAndFunctor<bool>>, at::detail::Array<char*, int=3>>(int, bool, bool)
                    2.97%  4.3200us         2  2.1600us  1.9840us  2.3360us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<bool, bool, bool, at::native::binary_internal::MulFunctor<bool>>, at::detail::Array<char*, int=3>>(int, bool, bool)
                    2.77%  4.0320us         2  2.0160us  1.8240us  2.2080us  _ZN2at6native29vectorized_elementwise_kernelILi4EZZZNS0_16ceil_kernel_cudaERNS_18TensorIteratorBaseEENKUlvE_clEvENKUlvE0_clEvEUlfE_NS_6detail5ArrayIPcLi2EEEEEviT0_T1_
                    2.40%  3.4890us         2  1.7440us  1.6330us  1.8560us  void at_cuda_detail::cub::DeviceCompactInitKernel<at_cuda_detail::cub::ScanTileState<int, bool=1>, int*>(int, int, bool=1)
                    2.18%  3.1680us         1  3.1680us  3.1680us  3.1680us  void _GLOBAL__N__5857bd1b_10_SoftMax_cu_9f978f63::softmax_warp_forward<float, float, float, int=2, bool=0, bool=0>(float*, float const *, int, int, int, bool const *, int, bool)
                    1.96%  2.8480us         1  2.8480us  2.8480us  2.8480us  _ZN2at6native29vectorized_elementwise_kernelILi4EZZZNS0_15exp_kernel_cudaERNS_18TensorIteratorBaseEENKUlvE0_clEvENKUlvE0_clEvEUlfE_NS_6detail5ArrayIPcLi2EEEEEviT0_T1_
                    1.87%  2.7200us         1  2.7200us  2.7200us  2.7200us  _ZN2at6native18elementwise_kernelILi128ELi2EZNS0_15gpu_kernel_implINS0_13BinaryFunctorIfffNS0_15binary_internal10DivFunctorIfEEEEEEvRNS_18TensorIteratorBaseERKT_EUliE_EEviT1_
                    1.58%  2.3040us         1  2.3040us  2.3040us  2.3040us  _ZN2at6native18elementwise_kernelILi128ELi2EZNS0_15gpu_kernel_implINS0_15CUDAFunctor_addIfEEEEvRNS_18TensorIteratorBaseERKT_EUliE_EEviT1_
                    0.66%     960ns         1     960ns     960ns     960ns  [CUDA memcpy HtoD]
      API calls:   99.98%  5.61895s         1  5.61895s  5.61895s  5.61895s  cudaDeviceGetStreamPriorityRange
                    0.01%  445.04us        46  9.6740us  4.0250us  38.680us  cudaLaunchKernel
                    0.00%  226.87us        19  11.940us  8.6640us  18.444us  cudaMemcpyAsync
                    0.00%  166.31us       424     392ns     229ns  4.2350us  cudaGetDevice
                    0.00%  159.43us         1  159.43us  159.43us  159.43us  cudaMalloc
                    0.00%  74.175us       101     734ns     116ns  30.790us  cuDeviceGetAttribute
                    0.00%  57.800us         1  57.800us  57.800us  57.800us  cudaGetDeviceProperties
                    0.00%  47.660us        19  2.5080us  1.5010us  7.0440us  cudaStreamSynchronize
                    0.00%  18.457us       100     184ns     111ns  1.3590us  cudaGetLastError
                    0.00%  18.373us         1  18.373us  18.373us  18.373us  cuDeviceGetName
                    0.00%  11.234us         1  11.234us  11.234us  11.234us  cudaFuncGetAttributes
                    0.00%  10.043us         1  10.043us  10.043us  10.043us  cudaStreamIsCapturing
                    0.00%  6.1300us         1  6.1300us  6.1300us  6.1300us  cuDeviceGetPCIBusId
                    0.00%  4.9390us         4  1.2340us     115ns  4.3420us  cudaGetDeviceCount
                    0.00%  2.3500us         3     783ns     173ns  1.4350us  cuDeviceGetCount
                    0.00%  2.0420us         2  1.0210us  1.0060us  1.0360us  cudaDeviceGetAttribute
                    0.00%  1.8910us        12     157ns     117ns     232ns  cudaPeekAtLastError
                    0.00%     671ns         3     223ns     123ns     419ns  cuDevicePrimaryCtxGetState
                    0.00%     660ns         2     330ns     212ns     448ns  cuDeviceGet
                    0.00%     347ns         1     347ns     347ns     347ns  cuDeviceTotalMem
                    0.00%     245ns         1     245ns     245ns     245ns  cuDeviceGetUuid
'''