==621342== NVPROF is profiling process 621342, command: python3 sm.py
==621342== Profiling application: python3 sm.py
==621342== Profiling result:
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:    9.07%  14.338us        18     796ns     640ns  1.2800us  [CUDA memcpy DtoH]
                    8.69%  13.728us         2  6.8640us  6.8160us  6.9120us  void at::native::reduce_kernel<int=512, int=1, at::native::ReduceOp<double, at::native::func_wrapper_t<double, at::native::MaxNanFunctor<double>>, unsigned int, double, int=4>>(double)
                    8.34%  13.184us         2  6.5920us  6.3680us  6.8160us  void at::native::reduce_kernel<int=512, int=1, at::native::ReduceOp<double, at::native::func_wrapper_t<double, at::native::MinNanFunctor<double>>, unsigned int, double, int=4>>(double)
                    8.16%  12.896us         6  2.1490us  1.7280us  2.9760us  _ZN2at6native29vectorized_elementwise_kernelILi4EZNS0_21compare_scalar_kernelIdEEvRNS_18TensorIteratorBaseENS0_50_GLOBAL__N__89c782d4_17_CompareKernels_cu_8f1b29aa6OpTypeET_EUldE_NS_6detail5ArrayIPcLi2EEEEEviT0_T1_
                    5.85%  9.2480us         4  2.3120us  1.9200us  2.6880us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<float, float, bool, at::native::_GLOBAL__N__ca1a8271_18_CompareEQKernel_cu_d8008c96::CompareEqFunctor<float>>, at::detail::Array<char*, int=3>>(int, float, float)
                    5.51%  8.7040us         4  2.1760us  1.6960us  2.8160us  void at::native::vectorized_elementwise_kernel<int=4, at::native::AUnaryFunctor<float, float, bool, at::native::_GLOBAL__N__ca1a8271_18_CompareEQKernel_cu_d8008c96::CompareEqFunctor<float>>, at::detail::Array<char*, int=2>>(int, float, float)
                    5.24%  8.2880us         4  2.0720us  1.6960us  2.6560us  void at::native::vectorized_elementwise_kernel<int=4, at::native::AbsFunctor<float>, at::detail::Array<char*, int=2>>(int, float, at::native::AbsFunctor<float>)
                    4.39%  6.9450us         2  3.4720us  3.4240us  3.5210us  void at_cuda_detail::cub::DeviceSelectSweepKernel<at_cuda_detail::cub::DispatchSelectIf<at_cuda_detail::cub::CountingInputIterator<long, long>, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__08acd284_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool const *, long>, long*, int*, at_cuda_detail::cub::NullType, at_cuda_detail::cub::NullType, int, bool=0>::PtxSelectIfPolicyT, at_cuda_detail::cub::CountingInputIterator<long, long>, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__08acd284_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool const *, long>, long*, int*, at_cuda_detail::cub::ScanTileState<int, bool=1>, at_cuda_detail::cub::NullType, at_cuda_detail::cub::NullType, int, bool=0>(long, at_cuda_detail::cub::CountingInputIterator<long, long>, bool, bool, at::native::_GLOBAL__N__08acd284_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool const *, long, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__08acd284_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool const *, long>, int)
                    4.37%  6.9120us         2  3.4560us  3.3280us  3.5840us  void at_cuda_detail::cub::DeviceReduceSingleTileKernel<at_cuda_detail::cub::DeviceReducePolicy<bool, int, int, at_cuda_detail::cub::Sum>::Policy600, at_cuda_detail::cub::TransformInputIterator<bool, at::native::_GLOBAL__N__08acd284_10_Nonzero_cu_cba1aaa0::NonZeroOp<bool>, bool const *, long>, int*, int, at_cuda_detail::cub::Sum, int>(int, int, at_cuda_detail::cub::Sum, at_cuda_detail::cub::DeviceReducePolicy<bool, int, int, at_cuda_detail::cub::Sum>::Policy600, bool)
                    4.29%  6.7850us         1  6.7850us  6.7850us  6.7850us  void at::native::reduce_kernel<int=512, int=1, at::native::ReduceOp<float, at::native::func_wrapper_t<float, at::native::MaxNanFunctor<float>>, unsigned int, float, int=4>>(float)
                    4.25%  6.7200us         2  3.3600us  3.2960us  3.4240us  _ZN2at6native24index_elementwise_kernelILi128ELi4EZNS0_16gpu_index_kernelIZNS0_17index_kernel_implINS0_10OpaqueTypeILi4EEEEEvRNS_18TensorIteratorBaseEN3c108ArrayRefIlEESA_EUlPcSB_lE_EEvS7_SA_SA_RKT_EUliE_EEviT1_
                    4.13%  6.5290us         1  6.5290us  6.5290us  6.5290us  _ZN2at6native13reduce_kernelILi512ELi1ENS0_8ReduceOpIfNS0_14func_wrapper_tIfZNS0_11sum_functorIfffEclERNS_14TensorIteratorEEUlffE_EEjfLi4EEEEEvT1_
                    4.11%  6.4960us         2  3.2480us  3.2320us  3.2640us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<double, double, double, at::native::binary_internal::DivFunctor<double>>, at::detail::Array<char*, int=3>>(int, double, double)
                    4.09%  6.4650us         2  3.2320us  3.1680us  3.2970us  _ZN2at6native27unrolled_elementwise_kernelIZZZNS0_23direct_copy_kernel_cudaERNS_18TensorIteratorBaseEENKUlvE0_clEvENKUlvE4_clEvEUldE_NS_6detail5ArrayIPcLi2EEE23TrivialOffsetCalculatorILi1EjESC_NS0_6memory12LoadWithCastILi1EEENSD_13StoreWithCastILi1EEEEEviT_T0_T1_T2_T3_T4_
                    3.30%  5.2160us         2  2.6080us  2.5600us  2.6560us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<bool, bool, bool, at::native::binary_internal::MulFunctor<bool>>, at::detail::Array<char*, int=3>>(int, bool, bool)
                    3.18%  5.0240us         2  2.5120us  2.4960us  2.5280us  void at::native::vectorized_elementwise_kernel<int=4, at::native::BinaryFunctor<bool, bool, bool, at::native::BitwiseAndFunctor<bool>>, at::detail::Array<char*, int=3>>(int, bool, bool)
                    2.57%  4.0640us         2  2.0320us  2.0160us  2.0480us  _ZN2at6native29vectorized_elementwise_kernelILi4EZZZNS0_16ceil_kernel_cudaERNS_18TensorIteratorBaseEENKUlvE_clEvENKUlvE0_clEvEUlfE_NS_6detail5ArrayIPcLi2EEEEEviT0_T1_
                    2.23%  3.5210us         2  1.7600us  1.7600us  1.7610us  void at_cuda_detail::cub::DeviceCompactInitKernel<at_cuda_detail::cub::ScanTileState<int, bool=1>, int*>(int, int, bool=1)
                    2.02%  3.2000us         1  3.2000us  3.2000us  3.2000us  _ZN2at6native18elementwise_kernelILi128ELi2EZNS0_15gpu_kernel_implINS0_15CUDAFunctor_addIfEEEEvRNS_18TensorIteratorBaseERKT_EUliE_EEviT1_
                    1.94%  3.0720us         1  3.0720us  3.0720us  3.0720us  void _GLOBAL__N__5857bd1b_10_SoftMax_cu_9f978f63::softmax_warp_forward<float, float, float, int=2, bool=0, bool=0>(float*, float const *, int, int, int, bool const *, int, bool)
                    1.88%  2.9760us         1  2.9760us  2.9760us  2.9760us  _ZN2at6native29vectorized_elementwise_kernelILi4EZZZNS0_15exp_kernel_cudaERNS_18TensorIteratorBaseEENKUlvE0_clEvENKUlvE0_clEvEUlfE_NS_6detail5ArrayIPcLi2EEEEEviT0_T1_
                    1.74%  2.7520us         1  2.7520us  2.7520us  2.7520us  _ZN2at6native18elementwise_kernelILi128ELi2EZNS0_15gpu_kernel_implINS0_13BinaryFunctorIfffNS0_15binary_internal10DivFunctorIfEEEEEEvRNS_18TensorIteratorBaseERKT_EUliE_EEviT1_
                    0.63%     992ns         1     992ns     992ns     992ns  [CUDA memcpy HtoD]
      API calls:   99.97%  5.41139s         1  5.41139s  5.41139s  5.41139s  cudaDeviceGetStreamPriorityRange
                    0.01%  445.52us        46  9.6850us  4.1440us  37.153us  cudaLaunchKernel
                    0.01%  392.12us        19  20.637us  8.5690us  171.29us  cudaMemcpyAsync
                    0.00%  174.72us       424     412ns     238ns  6.5680us  cudaGetDevice
                    0.00%  153.71us         1  153.71us  153.71us  153.71us  cudaMalloc
                    0.00%  78.054us       101     772ns     116ns  32.767us  cuDeviceGetAttribute
                    0.00%  60.057us         1  60.057us  60.057us  60.057us  cudaGetDeviceProperties
                    0.00%  47.519us        19  2.5010us  1.5080us  6.5790us  cudaStreamSynchronize
                    0.00%  27.906us         1  27.906us  27.906us  27.906us  cuDeviceGetName
                    0.00%  18.035us       100     180ns     114ns  1.2400us  cudaGetLastError
                    0.00%  10.594us         1  10.594us  10.594us  10.594us  cudaFuncGetAttributes
                    0.00%  9.3440us         1  9.3440us  9.3440us  9.3440us  cudaStreamIsCapturing
                    0.00%  5.8740us         1  5.8740us  5.8740us  5.8740us  cuDeviceGetPCIBusId
                    0.00%  5.3140us         4  1.3280us     132ns  4.7110us  cudaGetDeviceCount
                    0.00%  2.1100us         2  1.0550us  1.0530us  1.0570us  cudaDeviceGetAttribute
                    0.00%  1.9390us         3     646ns     162ns  1.4390us  cuDeviceGetCount
                    0.00%  1.9160us        12     159ns     118ns     249ns  cudaPeekAtLastError
                    0.00%     710ns         3     236ns     132ns     433ns  cuDevicePrimaryCtxGetState
                    0.00%     634ns         2     317ns     116ns     518ns  cuDeviceGet
                    0.00%     328ns         1     328ns     328ns     328ns  cuDeviceTotalMem
                    0.00%     235ns         1     235ns     235ns     235ns  cuDeviceGetUuid
