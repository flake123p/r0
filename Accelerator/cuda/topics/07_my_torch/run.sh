
nvprof --openacc-profiling off --log-file nvprof.txt python3 sm.py

ltrace -o __dummy_ltraceX.txt -x 'cu*' python3 sm.py

ltrace -o __dummy_ltraceE.txt -e 'cu*' python3 sm.py
