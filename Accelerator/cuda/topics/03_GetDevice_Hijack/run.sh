#!/bin/bash

rm lib*

gcc -c stub_GetDevice2.cc -fPIC

gcc -shared -o libcudart.so stub_GetDevice2.o

#
# ...
#
cp libcudart.so libcudart.so.11.0

export LD_LIBRARY_PATH=. 

objdump -S -drwC libcudart.so>libcudart.so.dump