


# Tensorflow SW requirements
https://www.tensorflow.org/install/pip
1. NVIDIA® GPU drivers version 450.80.02 or higher.
2. CUDA® Toolkit 11.8.
3. cuDNN SDK 8.6.0.
4. (Optional) TensorRT to improve latency and throughput for inference.



# Pytorch 
(Should like TF)



## TensorRT
1. **an SDK for high-performance deep learning inference**
2. Support TensorFlow、Caffe、Mxnet、Pytorch
3. Mxnet、Pytorch have transfer to ONNX
4. Support Op: http://giantpandacv.com/project/%E9%83%A8%E7%BD%B2%E4%BC%98%E5%8C%96/AI%20%E9%83%A8%E7%BD%B2%E5%8F%8A%E5%85%B6%E5%AE%83%E4%BC%98%E5%8C%96%E7%AE%97%E6%B3%95/TensorRT/%E4%B8%80%EF%BC%8CTensorRT%E4%BB%8B%E7%BB%8D%EF%BC%8C%E5%AE%89%E8%A3%85%E5%8F%8A%E5%A6%82%E4%BD%95%E4%BD%BF%E7%94%A8%EF%BC%9F/



## The CUDA Compilation Trajectory
https://docs.nvidia.com/cuda/cuda-compiler-driver-nvcc/index.html

Picture:![](https://docs.nvidia.com/cuda/cuda-compiler-driver-nvcc/_images/cuda-compilation-from-cu-to-executable.png)



## cuBLAS
1. Basic Linear Algebra Subroutine : https://docs.nvidia.com/cuda/cublas/index.html#
2. cuBLASXt : lightweight library dedicated to GEneral Matrix-to-matrix Multiply (GEMM) 
3. cuBLASLt : exposes a multi-GPU capable Host interface

4. Mat Mul : cublasLtMatmul()



# Legion
https://github.com/StanfordLegion/legion/tree/7b5ff2fb9974511c28aec8d97b942f26105b5f6d


# API numbers
CUDA Driver API : 46x
CUDA Runtime API : 55x
cuDNN : 26x