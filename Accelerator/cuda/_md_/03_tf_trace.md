TF
https://github.com/tensorflow/tensorflow
PTH
https://github.com/pytorch/pytorch



# [Top Down] tf py func: 
```
list_physical_devices()

_initialize_physical_devices()
    pywrap_tfe.TF_ListPhysicalDevices
    pywrap_tfe.TF_ListPluggablePhysicalDevices
    self._import_config()
```



# [Bottom UP]
cudaMalloc() -> only test (This is runtime API)

cuMemAlloc() -> used in TF body (This is driver API)