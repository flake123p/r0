
# F.Y.I

Sample Device Function:
_ZN2at6native29vectorized_elementwise_kernelILi4ENS0_15CUDAFunctor_addIfEENS_6detail5ArrayIPcLi3EEEEEviT0_T1_

GPGPU-SIM / CUDA Sim:
https://juniorprincewang.github.io/2018/05/14/CUDA-API-Remoting/

Simulators:
https://zhuanlan.zhihu.com/p/494149559

# Internal Functions
https://github.com/Geof23/Gklee/blob/master/Gklee/include/cuda/crt/host_runtime.h#L160
__cudaRegisterFatBinary
__cudaRegisterTexture
__cudaRegisterSurface
...


# Pytorch - CUDA Hijack Summary

1. prepare pytorch program e.g.: **pth.py**
2. Using ltrace to dump all CUDA API calls using keyword(**cu***) filtering, e.g.:
```=
$ ltrace -o dump_ltrace.txt -x 'cu*' python pth.py
```

3. Parsing the dump file, get the CUDA APIs and remove redundant APIs, sample result:
```=
CUDA driver API list (27) =
    cuGetProcAddress
    cuDriverGetVersion
    cuInit
    cuGetExportTable
    cuModuleGetLoadingMode
    cuDeviceGetCount
    cuDeviceGet
    cuDeviceGetName
    cuDeviceTotalMem_v2
    cuDeviceGetAttribute
    cuDeviceGetUuid
    cuCtxGetDevice
    cuDevicePrimaryCtxGetState
    cuCtxGetCurrent
    cuCtxSetCurrent
    cuDevicePrimaryCtxRetain
    cuModuleGetFunction
    cuModuleGetGlobal_v2
    cuStreamIsCapturing
    cuMemAlloc_v2
    cuMemcpyHtoDAsync_v2
    cuStreamSynchronize
    cuLaunchKernel
    cuFuncGetAttribute
    cuMemcpyDtoHAsync_v2
    cuModuleUnload
    cuDevicePrimaryCtxRelease

CUDA Runtime API list (12) =
    cudaGetDeviceCount
    cudaGetDevice
    cudaGetDeviceProperties
    cudaGetLastError
    cudaStreamIsCapturing
    cudaMalloc
    cudaMemcpyAsync
    cudaStreamSynchronize
    cudaLaunchKernel
    cudaFuncGetAttributes
    cudaPeekAtLastError
    cudaDeviceGetAttribute
```

4. Replace Runtime API first, because the driver API might be called from Runtime API. If we replace the Runtime API, there is a chance that no need to replace driver API.

- 4.1 Create your own API, e.g.:
```=
cudaError_t cudaMalloc(void **devPtr, size_t size)
{
    *devPtr = malloc(size);
    printf("size = %lu, loc = %p\n", size, *devPtr);
    return 0;
}
```

- 4.2 Build it as a shared library, and rename the library version according to pytorch cuda version!
```=
gcc -c your_runtime_api.c -fPIC
gcc -shared -o libcudart.so your_runtime_api.o
cp libcudart.so libcudart.so.11.0
```

- 4.3 Use LD_PRELOAD to replace shared library:
```=
$ export LD_PRELOAD=./libcudart.so.11.0
```

- 4.4 Unset LD_PRELOAD to disable hijack
```=
$ unset LD_PRELOAD
```

5. Prepare to Hijack cudaLaunchKernel()

- 5.1 Hijack __cudaRegisterFunction(), create a function pointer(*hostFun by caller: pytorch) & function name(*deviceFun by caller: pytorch) mapping table.
```=
void __cudaRegisterFunction(
    void **fatCubinHandle, const char *hostFun, char *deviceFun,
    const char *deviceName, int thread_limit, uint3 *tid,
    uint3 *bid, dim3 *bDim, dim3 *gDim, int *wSize)
{
    // Insert a *hostFunr & *deviceFun pair to database.
}
```

```=
void __cudaUnregisterFatBinary(void **handle) 
{
    // You can free database in this function.
}
```

- 5.2 Implement the replacement by function name and put it into database. (The original function behavior is in Pytorch Source Code.)
```=
Device(Kernel) Function Database:
GPU device func pointer <-> func name

Replacement Database:
func name <-> replacement func
```

- 5.3 Study and Rollback the mangled function to original function name in pytorch source code.
```=
From:

_ZN2at6native29vectorized_elementwise_kernelILi4ENS0_15CUDAFunctor_addIfEENS_6detail5ArrayIPcLi3EEEEEviT0_T1_()

To:

vectorized_elementwise_kernel() ...
```

- 5.4 Hijack cudaLaunchKernel(), use the input function pointer(from pytorch) as an index to mapping to our replacement function in step 5.2.
```=
cudaError_t CUDARTAPI cudaLaunchKernel(
    const void *func, dim3 gridDim, dim3 blockDim, void **args, 
    size_t sharedMem, cudaStream_t stream)
{
    // Search database with input function pointer to find out the replacement function.
}
```