- [ ] cuda install
- [ ] cuda example (nvcc & .cu)
- [ ] cuda example (gcc + nvcc)
- [ ] cuda adapter layer (with example above)

# 10/11 Missing driver after reboot
Fixing Guide:
sudo apt install nvidia-driver-525 (usless)
--- another try (Follow Installation Section)---
sudo apt-get --purge remove "*cublas*" "cuda*" "*nvidia*"
sudo apt-get clean
sudo apt-get autoremove
sudo apt-get update
sudo apt-get upgrade
(REBOOT!!! DO REBOOT HERE!!!!!!!!!!!!!!!!!!!)
sudo apt install libnvidia-common-525
sudo apt install libnvidia-compute-525
sudo apt install nvidia-utils-525
sudo apt install nvidia-driver-525
(REBOOT)
sudo apt install nvidia-cuda-toolkit
========= 
sudo apt install libnvidia-common-535
sudo apt install libnvidia-compute-535
sudo apt install nvidia-utils-535
sudo apt install nvidia-driver-535

=========
CUDA Toolkit 12.3 Downloads

$ wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/cuda-keyring_1.1-1_all.deb
$ sudo dpkg -i cuda-keyring_1.1-1_all.deb
$ sudo apt-get update
$ sudo apt-get -y install cuda-toolkit-12-3

$ sudo apt-get install -y cuda-drivers
=========

sudo apt-get --purge remove "*nvidia*"
sudo apt-get clean
sudo apt-get autoremove
sudo apt-get update
(REBOOT)
sudo apt install nvidia-driver-515           
(REBOOT)
sudo apt install nvidia-cuda-toolkit



# CUDA Toolkit script
```
$ wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/cuda-keyring_1.1-1_all.deb
$ sudo dpkg -i cuda-keyring_1.1-1_all.deb
$ sudo apt-get update
$ sudo apt-get -y install cuda-toolkit-12-3

$ sudo apt-get install -y cuda-drivers

sudo apt install nvidia-cuda-toolkit
```


# Torch Cuda site-package (Old ver. copy):
copy torch/ folder
copy nvidia/ folder
//create env:
conda create -n py39 python=3.9
//libusb
sudo apt-get install libusb-dev
sudo apt-get install libusb-1.0-0-dev
//pip
pip install typing_extensions
pip install sympy numpy



# Installation
https://askubuntu.com/questions/1289811/cant-install-nvidia-driver-toolkit-on-ubuntu-20-04-lts-needs-uninstallable-pa
sudo apt-get --purge remove "*cublas*" "cuda*" "*nvidia*"
sudo apt-get clean
sudo apt-get autoremove
sudo apt-get update
sudo apt-get upgrade

sudo apt install libnvidia-common-525
sudo apt install libnvidia-compute-525
sudo apt install nvidia-utils-525
sudo apt install nvidia-driver-525           (REBOOT)
sudo apt install nvidia-cuda-toolkit

https://askubuntu.com/questions/1428661/installing-nvidia-cuda-toolkit-removes-nvidia-smi

The problem is that the latest version of the nvidia-cuda-toolkit does not match the latest driver version.

sudo apt install nvidia-driver-525

535 is ok? no

## 0905
sudo apt install nvidia-driver-525 (REBOOT)
```=
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/cuda-keyring_1.1-1_all.deb
sudo dpkg -i cuda-keyring_1.1-1_all.deb
sudo apt-get update
sudo apt-get install cuda (Flake: this will cause fail, install libraries below)
```
sudo apt install libnvidia-common-525
sudo apt install libnvidia-compute-525
sudo apt install nvidia-utils-525
sudo apt install nvidia-driver-525
sudo apt install nvidia-cuda-toolkit



# Replacement:
## cudart_hijack
https://github.com/StanfordLegion/legion/blob/7b5ff2fb9974511c28aec8d97b942f26105b5f6d/runtime/realm/cuda/cudart_hijack.cc#L87


# Chinese Study
- [ ] https://blog.csdn.net/jslove1997/article/details/113737934
- [ ] https://zhuanlan.zhihu.com/p/111602648
- [ ] https://stackoverflow.com/questions/53422407/different-cuda-versions-shown-by-nvcc-and-nvidia-smi

### APIs
cudart
cudadevrt
cupti: profiling tools interface
nvml: management libray
nvtrc: CUDA runtime compilation
cublas
cublas_device

### TF & PTH API
- [ ] Find Used Function (TF/PTH)
- [ ] Figure out Fatbin cubin (TF/PTH)
- [ ] Trace cuda function in TF & PTH
- [ ] Replace 1 basic example : some_get_GPU_device_function()



# Hijack

|             | Total | TF      | PTH |
| ----------- | ----- | ------- | --- |
| CUDA RT     | 596   | 397(*1) | 429 |
| CUDA DRIVER | 476   | 379     | 102 |
| CUDNN       | 256   | 243     | 129 |

*1: Only appears in *.inc files.

### CUDA Stack
![](https://i.stack.imgur.com/TD51p.jpg)

### CUDA Visual Profiler (nvvp)
![](https://developer.nvidia.com/sites/default/files/akamai/cuda/images/CUDALibs/Visprofiler41.png)
https://developer.nvidia.com/nvidia-visual-profiler

### nvprof example:
```
$ nvprof ./a.out
[Vector addition of 50000 elements]
==3140847== NVPROF is profiling process 3140847, command: ./a.out
Copy input data from the host memory to the CUDA device
CUDA kernel launch with 196 blocks of 256 threads
Copy output data from the CUDA device to the host memory
Test PASSED
Done
==3140847== Profiling application: ./a.out
==3140847== Profiling result:
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:   62.45%  33.315us         2  16.657us  16.322us  16.993us  [CUDA memcpy HtoD]
                   30.11%  16.065us         1  16.065us  16.065us  16.065us  [CUDA memcpy DtoH]
                    7.44%  3.9690us         1  3.9690us  3.9690us  3.9690us  vectorAdd(float const *, float const *, float*, int)
      API calls:   99.05%  72.682ms         3  24.227ms  2.2360us  72.675ms  cudaMalloc
                    0.45%  332.10us         1  332.10us  332.10us  332.10us  cuDeviceGetPCIBusId
                    0.24%  179.33us         3  59.778us  36.365us  102.12us  cudaMemcpy
                    0.10%  75.445us       101     746ns     107ns  31.684us  cuDeviceGetAttribute
                    0.09%  68.891us         3  22.963us  2.5170us  60.009us  cudaFree
                    0.03%  24.181us         1  24.181us  24.181us  24.181us  cuDeviceGetName
                    0.02%  17.097us         1  17.097us  17.097us  17.097us  cudaLaunchKernel
                    0.00%  1.1840us         3     394ns     177ns     809ns  cuDeviceGetCount
                    0.00%     690ns         2     345ns     106ns     584ns  cuDeviceGet
                    0.00%     272ns         1     272ns     272ns     272ns  cudaGetLastError
                    0.00%     270ns         1     270ns     270ns     270ns  cuDeviceTotalMem
                    0.00%     220ns         1     220ns     220ns     220ns  cuDeviceGetUuid
```

### NVCC Compile Trajectory
https://docs.nvidia.com/cuda/cuda-compiler-driver-nvcc/index.html
![](https://docs.nvidia.com/cuda/cuda-compiler-driver-nvcc/_images/cuda-compilation-from-cu-to-executable.png)

### CUDA RT API Summery
Need nvcc
No context, no module

### CUDA Driver API Summery
Don't need nvcc
Coding with context & module

Thread, block, grid, kernel management

Shared memory, cache, register management

module management

Graphic functions

Interoperation

OpenGL/D3D mapping


### CUDA DNN API Summary

tensor operation

normalize, softmax, pooling, conv

create RNN



# Accelerating PyTorch with CUDA Graphs
https://pytorch.org/blog/accelerating-pytorch-with-cuda-graphs/
![](https://pytorch.org/assets/images/cuda-image-2.png)
![](https://pytorch.org/assets/images/cuda-image-7.png)

