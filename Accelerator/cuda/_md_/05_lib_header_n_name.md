Log:

cudaGetDeviceCount@libcudart.so.11.0(0x7ffcb528a588, 0, 256, 0x7f571e8f8240) = 0
cudaGetDeviceCount@libcudart.so.11.0(0x7ffcb5289ec8, 0x275c010, 0x757c04e, 1) = 0
cudaGetDevice@libcudart.so.11.0(0x7ffcb5289e4c, 0x7f56e17a2938, 40, 1) = 0
cudaGetDeviceCount@libcudart.so.11.0(0x7ffcb5289d98, 0x7f574fde6092, 0x7f574fde5f86, 126) = 0
cuDevicePrimaryCtxGetState@libcuda.so.1(0, 0x7ffcb5289ca0, 0x7ffcb5289ca4, 1) = 3
cuGetErrorString@libcuda.so.1(3, 0x7ffcb5289ca8, 1, 1)   = 0

Name:
    libcuda.so.1
    libcudart.so.11.0
    libcublasLt.so.11  (cublasLtMatrixTransform)


Header:
    RT:
        /usr/include/cuda_runtime_api.h

    Drv
        /usr/include/cuda.h

    BLAS
        /usr/include/cublasLt.h
            cublasLtHSHMatmulAlgoInit ???
        /usr/include/cublas_api.h

export LD_PRELOAD=./libcuda.so.1:./libcudart.so.11.0:./libcublas.so.11:./libcublasLt.so.11
