# StackOverflow - vecAdd sample
    Vector Example:
        https://stackoverflow.com/questions/76080519/whats-the-difference-between-cudamalloc-and-cumemalloc
    
    RT API: (This Example)
        https://github.com/NVIDIA/cuda-samples/blob/master/Samples/0_Introduction/vectorAdd/vectorAdd.cu

    Dri API:
        https://github.com/NVIDIA/cuda-samples/blob/master/Samples/0_Introduction/vectorAddDrv/vectorAddDrv.cpp


# Offical Sample (No driver)
https://github.com/NVIDIA/cuda-samples


# Library Sample (small lib)
https://github.com/NVIDIA/CUDALibrarySamples
    cuBLAS,
    cuTENSOR,
    cuSPARSE,
    cuSOLVER,
    cuFFT,
    cuRAND,
    ...

# Sample Web Page:
https://developer.nvidia.com/cuda-code-samples