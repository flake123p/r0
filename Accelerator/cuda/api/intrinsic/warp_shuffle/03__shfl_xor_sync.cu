#include <iostream>
#include <cuda_runtime.h>
#include <vector>

#define ID_OFFSET (0)

__global__ void demo_0_shfl_xor_sync__laneMask_1()
{
    int val;
    int laneMask = 1;
    val = (threadIdx.x + ID_OFFSET) * 0x00010001;

    __syncthreads();
    val = __shfl_xor_sync(0xffffffff, val, laneMask, 32);

    printf("%s(), [%3d] val = 0x%08X, laneMask = %d\n", __func__, threadIdx.x, val, laneMask);
}

__global__ void demo_1_shfl_xor_sync__laneMask_2()
{
    int val;
    int laneMask = 2;
    val = (threadIdx.x + ID_OFFSET) * 0x00010001;

    __syncthreads();
    val = __shfl_xor_sync(0xffffffff, val, laneMask, 32);

    printf("%s(), [%3d] val = 0x%08X, laneMask = %d\n", __func__, threadIdx.x, val, laneMask);
}

__global__ void demo_2_shfl_xor_sync__laneMask_3()
{
    int val;
    int laneMask = 3;
    val = (threadIdx.x + ID_OFFSET) * 0x00010001;

    __syncthreads();
    val = __shfl_xor_sync(0xffffffff, val, laneMask, 32);

    printf("%s(), [%3d] val = 0x%08X, laneMask = %d\n", __func__, threadIdx.x, val, laneMask);
}

__global__ void demo_3_shfl_xor_sync__laneMask_4()
{
    int val;
    int laneMask = 4;
    val = (threadIdx.x + ID_OFFSET) * 0x00010001;

    __syncthreads();
    val = __shfl_xor_sync(0xffffffff, val, laneMask, 32);

    printf("%s(), [%3d] val = 0x%08X, laneMask = %d\n", __func__, threadIdx.x, val, laneMask);
}

__global__ void demo_4_shfl_xor_sync__laneMask_5()
{
    int val;
    int laneMask = 5;
    val = (threadIdx.x + ID_OFFSET) * 0x00010001;

    __syncthreads();
    val = __shfl_xor_sync(0xffffffff, val, laneMask, 32);

    printf("%s(), [%3d] val = 0x%08X, laneMask = %d\n", __func__, threadIdx.x, val, laneMask);
}

int main() {

    demo_0_shfl_xor_sync__laneMask_1<<<1, 8>>>();
    demo_1_shfl_xor_sync__laneMask_2<<<1, 8>>>();
    demo_2_shfl_xor_sync__laneMask_3<<<1, 8>>>();
    demo_3_shfl_xor_sync__laneMask_4<<<1, 8>>>();
    demo_4_shfl_xor_sync__laneMask_5<<<1, 32>>>();

    cudaDeviceSynchronize();

    return 0;
}

