#include <iostream>
#include <cuda_runtime.h>
#include <vector>

#define ID_OFFSET (0)

__global__ void demo_0_shfl_sync__broadcast_tid0_to_others() 
{
    int tid = threadIdx.x;

    __syncthreads();
    tid = __shfl_sync(0xffffffff, tid, 0, 32);

    printf("%s(), tid = %d\n", __func__, tid);
}

__global__ void demo_1_shfl_sync__broadcast_tid1_to_others() 
{
    int tid = threadIdx.x;

    __syncthreads();
    tid = __shfl_sync(0xffffffff, tid, 1, 32);

    printf("%s(), tid = %d\n", __func__, tid);
}

__global__ void demo_2_shfl_sync__witdh_test()
{
    int val;
    val = (threadIdx.x + ID_OFFSET) * 0x00010001;

    __syncthreads();
    val = __shfl_sync(0xffffffff, val, 0, 16);

    printf("%s(), [%3d] val = 0x%08X\n", __func__, threadIdx.x, val);
}

int main() {

    demo_0_shfl_sync__broadcast_tid0_to_others<<<1, 2>>>();

    demo_1_shfl_sync__broadcast_tid1_to_others<<<1, 2>>>();

    demo_2_shfl_sync__witdh_test<<<1, 34>>>();

    cudaDeviceSynchronize();

    return 0;
}

