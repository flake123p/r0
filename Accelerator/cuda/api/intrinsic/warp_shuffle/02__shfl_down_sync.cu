#include <iostream>
#include <cuda_runtime.h>
#include <vector>

#define ID_OFFSET (0)

__global__ void demo_0_shfl_down_sync()
{
    int val;
    val = (threadIdx.x + ID_OFFSET) * 0x00010001;

    __syncthreads();
    val = __shfl_down_sync(0xffffffff, val, 2, 4);

    printf("%s(), [%3d] val = 0x%08X\n", __func__, threadIdx.x, val);
}

__global__ void demo_1_shfl_down_sync__mask_test()
{
    int val;
    val = (threadIdx.x + ID_OFFSET) * 0x00010001;

    __syncthreads();
    val = __shfl_down_sync(0x0, val, 2, 4); // ?????????????????????

    printf("%s(), [%3d] val = 0x%08X\n", __func__, threadIdx.x, val);
}

__global__ void demo_2_shfl_down_sync__sum()
{
    int val;
    val = 1;

    __syncthreads();

    val += __shfl_down_sync(0xFFFFFFFF, val, 4, 8);
    val += __shfl_down_sync(0xFFFFFFFF, val, 2, 4);
    val += __shfl_down_sync(0xFFFFFFFF, val, 1, 2);

    printf("%s(), [%3d] val = 0x%08X\n", __func__, threadIdx.x, val);
}

int main() {

    demo_0_shfl_down_sync<<<1, 8>>>();

    demo_1_shfl_down_sync__mask_test<<<1, 8>>>();

    demo_2_shfl_down_sync__sum<<<1, 8>>>();

    cudaDeviceSynchronize();

    return 0;
}

