/*
    From ba0/06_use_gcc

*/

#include <stdio.h>
#include <stdint.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

#include "help.hpp"

cudaError_t rc;

__global__
void add_1234() {
    int i = blockIdx.x;
    printf("glo add, blockIdx.x = %d\n", i);
}

int main() {


    cudaError_t status;
    struct cudaFuncAttributes attr;
    
    status = cudaFuncGetAttributes(&attr, add_1234);
    printf("status = %d\n", (int)status);
    if (status == cudaSuccess) {
        DNU(attr.sharedSizeBytes);
        DNU(attr.constSizeBytes);
        DNU(attr.localSizeBytes);
        DND(attr.maxThreadsPerBlock);
        DND(attr.numRegs);
        DND(attr.ptxVersion);
        DND(attr.binaryVersion);
        DND(attr.cacheModeCA);
        DND(attr.maxDynamicSharedSizeBytes);
        DND(attr.preferredShmemCarveout);
    }

    add_1234<<<4, 2>>>();
    cudaDeviceSynchronize();

    status = cudaFuncGetAttributes(&attr, add_1234);
    printf("status = %d\n", (int)status);
    if (status == cudaSuccess) {
        DNU(attr.sharedSizeBytes);
        DNU(attr.constSizeBytes);
        DNU(attr.localSizeBytes);
        DND(attr.maxThreadsPerBlock);
        DND(attr.numRegs);
        DND(attr.ptxVersion);
        DND(attr.binaryVersion);
        DND(attr.cacheModeCA);
        DND(attr.maxDynamicSharedSizeBytes);
        DND(attr.preferredShmemCarveout);
    }

    return 0;
}
