/*
    From ba0/06_use_gcc

*/

#include <stdio.h>
#include <stdint.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

#include "help.hpp"

cudaError_t rc;

int main() {
    {
        printf("cudaGetDeviceProperties\n");
        struct cudaDeviceProp pp;
        rc = cudaGetDeviceProperties(&pp, 0);
        printf("RC = %d, name = %s\n", rc, pp.name);

        // for (int i = 0; i < 16; i++) {
        //     printf("%02X ", (unsigned char)pp.uuid.bytes[i]);
        // }
        // printf("\n");
        ARRAYDUMPX(pp.uuid.bytes, 16);

        DND(pp.luidDeviceNodeMask);
        DNU(pp.totalGlobalMem);
        DND(pp.sharedMemPerBlock);
        DND(pp.regsPerBlock);
        DND(pp.warpSize);
        DND(pp.memPitch);
        DND(pp.maxThreadsPerBlock);
        ARRAYDUMPD(pp.maxThreadsDim, 3);
        ARRAYDUMPD(pp.maxGridSize, 3);

        DND(pp.clockRate);
        DND(pp.totalConstMem);
        DND(pp.major);
        DND(pp.minor);
        DND(pp.textureAlignment);
        DND(pp.texturePitchAlignment);
        DND(pp.deviceOverlap);
        DND(pp.multiProcessorCount);
        DND(pp.kernelExecTimeoutEnabled);

        DND(pp.integrated);
        DND(pp.canMapHostMemory);
        DND(pp.computeMode);
        DND(pp.maxTexture1D);
        DND(pp.maxTexture1DMipmap);
        DND(pp.maxTexture1DLinear);

        ARRAYDUMPD(pp.maxTexture2D, 2);

        ARRAYDUMPD(pp.maxTexture2DMipmap, 2);
        ARRAYDUMPD(pp.maxTexture2DLinear, 3);
        ARRAYDUMPD(pp.maxTexture2DGather, 2);

        ARRAYDUMPD(pp.maxTexture3D, 3);
        ARRAYDUMPD(pp.maxTexture3DAlt, 3);
        DND(pp.maxTextureCubemap);
        ARRAYDUMPD(pp.maxTexture1DLayered, 2);
        ARRAYDUMPD(pp.maxTexture2DLayered, 3);
        ARRAYDUMPD(pp.maxTextureCubemapLayered, 2);
        DND(pp.maxSurface1D);
        ARRAYDUMPD(pp.maxSurface2D, 2);
        ARRAYDUMPD(pp.maxSurface3D, 3);

        ARRAYDUMPD(pp.maxSurface1DLayered, 2);
        ARRAYDUMPD(pp.maxSurface2DLayered, 3);

        DND(pp.maxSurfaceCubemap);
        ARRAYDUMPD(pp.maxSurfaceCubemapLayered, 2);

        DND(pp.surfaceAlignment);

        DND(pp.concurrentKernels);
        DND(pp.ECCEnabled);
        DND(pp.pciBusID);
        DND(pp.pciDeviceID);
        DND(pp.pciDomainID);
        DND(pp.tccDriver);
        DND(pp.asyncEngineCount);
        DND(pp.unifiedAddressing);
        DND(pp.memoryClockRate);
        DND(pp.memoryBusWidth);
        DND(pp.l2CacheSize);
        DND(pp.persistingL2CacheMaxSize);
        DND(pp.maxThreadsPerMultiProcessor);
        DND(pp.streamPrioritiesSupported);
        DND(pp.globalL1CacheSupported);
        DND(pp.localL1CacheSupported);
        DND(pp.sharedMemPerMultiprocessor);
        DND(pp.regsPerMultiprocessor);
        DND(pp.managedMemory);
        DND(pp.isMultiGpuBoard);
        DND(pp.multiGpuBoardGroupID);
        DND(pp.hostNativeAtomicSupported);
        DND(pp.singleToDoublePrecisionPerfRatio);
        DND(pp.pageableMemoryAccess);
        DND(pp.concurrentManagedAccess);

        DND(pp.computePreemptionSupported);
        DND(pp.canUseHostPointerForRegisteredMem);
        DND(pp.cooperativeLaunch);
        DND(pp.cooperativeMultiDeviceLaunch);
        DND(pp.sharedMemPerBlockOptin);
        DND(pp.pageableMemoryAccessUsesHostPageTables);
        DND(pp.directManagedMemAccessFromHost);
        DND(pp.maxBlocksPerMultiProcessor);
        DND(pp.accessPolicyMaxWindowSize);
        DND(pp.reservedSharedMemPerBlock);

/*
        DND(pp.xxxxxxxxxx);
        DND(pp.xxxxxxxxxx);
        DND(pp.xxxxxxxxxx);
        DND(pp.xxxxxxxxxx);
        DND(pp.xxxxxxxxxx);
*/        
    }
    return 0;
}
