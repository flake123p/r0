#include <stdio.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

#include "help.hpp"

/*
Pure mem copy:
    H2D: 364us
    D2H: 964us
*/

#define TEST_DATA_LEN  (1024*1024*1)
#define TEST_DATA_SIZE (TEST_DATA_LEN*sizeof(int))

int testImpl() {
    int *da;
    cudaMalloc((void **)&da, TEST_DATA_SIZE);

    int *ha;
    ha = (int *)malloc(TEST_DATA_SIZE);
    int *hb;
    hb = (int *)malloc(TEST_DATA_SIZE);

    for (int i = 0; i < 1024 * 1024; i++) {
        ha[i] = i;
    }

#if 0
    extern __host__ cudaError_t CUDARTAPI cudaMemcpy(void *dst, const void *src, size_t count, enum cudaMemcpyKind kind);
    enum __device_builtin__ cudaMemcpyKind
    {
        cudaMemcpyHostToHost          =   0,      /**< Host   -> Host */
        cudaMemcpyHostToDevice        =   1,      /**< Host   -> Device */
        cudaMemcpyDeviceToHost        =   2,      /**< Device -> Host */
        cudaMemcpyDeviceToDevice      =   3,      /**< Device -> Device */
        cudaMemcpyDefault             =   4       /**< Direction of the transfer is inferred from the pointer values. Requires unified virtual addressing */
    };
#endif
    cudaStream_t s0;
    cudaStream_t s1;
    cudaStreamCreate(&s0);
    cudaStreamCreate(&s1);

    // H 2 D : 1
    cudaMemcpyAsync(da, ha, TEST_DATA_SIZE/2, cudaMemcpyHostToDevice, s0);

    // D 2 H : 1
    cudaStreamSynchronize(s0);
    cudaMemcpyAsync(hb, da, TEST_DATA_SIZE/2, cudaMemcpyDeviceToHost, s1);

    // H 2 D : 2
    cudaMemcpyAsync(da+(TEST_DATA_LEN/2), ha+(TEST_DATA_LEN/2), TEST_DATA_SIZE/2, cudaMemcpyHostToDevice, s0);
    
    // D 2 H : 2
    cudaStreamSynchronize(s0);
    cudaStreamSynchronize(s1);
    cudaMemcpyAsync(hb+(TEST_DATA_LEN/2), da+(TEST_DATA_LEN/2), TEST_DATA_SIZE/2, cudaMemcpyDeviceToHost, s1);
    
    // // H 2 D : 3
    // cudaMemcpyAsync(da+(TEST_DATA_LEN/4*2), ha+(TEST_DATA_LEN/4*2), TEST_DATA_SIZE/4, cudaMemcpyHostToDevice, s0);
    
    // // D 2 H : 3
    // cudaStreamSynchronize(s0);
    // cudaStreamSynchronize(s1);
    // cudaMemcpyAsync(hb+(TEST_DATA_LEN/4*2), da+(TEST_DATA_LEN/4*2), TEST_DATA_SIZE/4, cudaMemcpyDeviceToHost, s1);

    // // H 2 D : 4
    // cudaMemcpyAsync(da+(TEST_DATA_LEN/4*3), ha+(TEST_DATA_LEN/4*3), TEST_DATA_SIZE/4, cudaMemcpyHostToDevice, s0);
    
    // // D 2 H : 4
    // cudaStreamSynchronize(s0);
    // cudaStreamSynchronize(s1);
    // cudaMemcpyAsync(hb+(TEST_DATA_LEN/4*3), da+(TEST_DATA_LEN/4*3), TEST_DATA_SIZE/4, cudaMemcpyDeviceToHost, s1);

    cudaStreamSynchronize(s1);

    for (int i = 0; i < 128; i++) {
        printf("%8d ", hb[i]);
        if (i % 16 == 15)
            printf("\n");
    }
    printf("\n");
    cudaFree(da);
    free(ha);
    free(hb);

    printf("Hello Example\n");
    return 0;
}

int test() {
    for (int i = 0; i < 1; i++) {
        testImpl();
    }
}

int main() {
    test();
    return 0;
}
