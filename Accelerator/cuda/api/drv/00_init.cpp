/*
[PR] cuGetProcAddress(), 1154
[PR] cuDriverGetVersion(), 119
False
3
Traceback (most recent call last):
  File "/home/pupil/sda/ws/r0/Accelerator/cuda/topics/52_buda_rt_pth/pth.py", line 11, in <module>
    a = torch.tensor([3.0], device=device)
  File "/home/pupil/anaconda3/lib/python3.9/site-packages/torch/cuda/__init__.py", line 247, in _lazy_init
    torch._C._cuda_init()
RuntimeError: Found no NVIDIA driver on your system. Please check that you have an NVIDIA GPU and installed a driver from http://www.nvidia.com/Download/index.aspx
*/

#include <stdio.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

#include "help.hpp"

CUresult rc;

int main() 
{
    {
        CUresult rc;
        void *fn;
        printf("cuGetProcAddress\n");
        int devCount;
        rc = cuGetProcAddress("cuGetProcAddress", &fn, 12000, 0);
        printf("rc = %d, fn = %p\n", rc, fn);
    }

    {
        CUresult rc;
        printf("cuDriverGetVersion\n");
        int ver;
        rc = cuDriverGetVersion(&ver);
        printf("rc = %d, ver = %d\n", rc, ver);
    }

    {
        printf("cuInit\n");
        CUresult rc;
        rc = cuInit(0);
        printf("rc = %d\n", rc);
    }

    {
        printf("cuDeviceGetCount\n");
        CUresult rc;
        int count = 0;
        rc = cuDeviceGetCount(&count);
        printf("rc = %d, count = %d\n", rc, count);
    }

    {
        printf("cuDeviceGet\n");
        CUresult rc;
        CUdevice dev = 0;
        rc = cuDeviceGet(&dev, 0);
        printf("rc = %d, dev = %d\n", rc, dev);
    }

    {
        printf("cuDeviceGetName\n");
        char name[128];
        rc = cuDeviceGetName(name, 128, 0);
        printf("rc = %d, name = %s\n", rc, name);
    }
    return 0;
}
