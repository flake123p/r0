#include <stdio.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

#include "help.hpp"

cudaError_t rc;

int main() {
    int *da;
    cudaMalloc((void **)&da, 1024*1024*sizeof(int));

    int *ha;
    ha = (int *)malloc(1024*1024*sizeof(int));
    int *hb;
    hb = (int *)malloc(1024*1024*sizeof(int));

    for (int i = 0; i < 1024 * 1024; i++) {
        ha[i] = i;
    }

#if 0
    extern __host__ cudaError_t CUDARTAPI cudaMemcpy(void *dst, const void *src, size_t count, enum cudaMemcpyKind kind);
    enum __device_builtin__ cudaMemcpyKind
    {
        cudaMemcpyHostToHost          =   0,      /**< Host   -> Host */
        cudaMemcpyHostToDevice        =   1,      /**< Host   -> Device */
        cudaMemcpyDeviceToHost        =   2,      /**< Device -> Host */
        cudaMemcpyDeviceToDevice      =   3,      /**< Device -> Device */
        cudaMemcpyDefault             =   4       /**< Direction of the transfer is inferred from the pointer values. Requires unified virtual addressing */
    };
#endif
    cudaMemcpy(da, ha, 1024*1024*sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(hb, da, 1024*1024*sizeof(int), cudaMemcpyDeviceToHost);

    for (int i = 0; i < 256; i++) {
        printf("%8d ", hb[i]);
        if (i % 16 == 15)
            printf("\n");
    }
    printf("\n");
    cudaFree(da);
    free(ha);
    free(hb);

    printf("Hello Example\n");
    return 0;
}
