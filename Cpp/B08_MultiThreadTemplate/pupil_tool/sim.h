#ifndef _SIM_H_INCLUDED_
#define _SIM_H_INCLUDED_

#include "sys.h"
#include "sys_init.h"

int sim_sys_init(sys_hdl_t *tool_hdl, sys_hdl_t *isr_hdl);
int sim_start_with_tool(task_cb_t tool_task_cb);
#define SIM_START_WITH_TOOL_LITE(cb) sim_start_with_tool_lite(cb, #cb)
int sim_start_with_tool_lite(task_cb_lite_t tool_task_cb_lite, const char *pass_str);
int sim_start_with_tool_and_isr(task_cb_t tool_task_cb, task_cb_t isr_task_cb);
int sim_stop_state_hi_lo();

#endif//_SIM_H_INCLUDED_