#ifndef _PUPIL_TOOL_H_INCLUDED_
#define _PUPIL_TOOL_H_INCLUDED_

#include "sys.h"
#include "sys_init.h"

void LibOs_SleepSeconds(unsigned int seconds);
void LibOs_SleepMiliSeconds(unsigned int miliSeconds);
void LibOs_SleepMicroSeconds(unsigned int microSeconds);

#include "sim.h"

#endif//_PUPIL_TOOL_H_INCLUDED_