#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include "pupil.h"
#include "sys_init.h"
#include "sys.h"
#include "state.h"
#include "sim.h"

static sys_hdl_t g_sim_tool;
static sys_hdl_t g_sim_isr;

int sim_sys_init(sys_hdl_t *tool_hdl, sys_hdl_t *isr_hdl)
{    
    sys_init2(); //keep this at top for mem_init()
    if (tool_hdl) {
        msg_task_new_handle(&tool_hdl->task_hdl, 0);
        cmd_external_interface_config(tool_hdl->task_hdl);
    }
    if (isr_hdl) {
        msg_task_new_handle(&isr_hdl->task_hdl, 0);
        cmd_external_interface_config(isr_hdl->task_hdl);
    }

    sys_start2();
    if (tool_hdl) {
        msg_task_create(tool_hdl->task_hdl, tool_hdl->task_cb, tool_hdl);
    }
    if (isr_hdl) {
        msg_task_create(isr_hdl->task_hdl, isr_hdl->task_cb, isr_hdl);
    }
    
    sys_wait2();
    if (tool_hdl) {
        msg_task_wait(tool_hdl->task_hdl);
    }
    if (isr_hdl) {
        msg_task_wait(isr_hdl->task_hdl);
    }
    
    if (tool_hdl) {
        msg_task_destory_handle(tool_hdl->task_hdl);
    }
    if (isr_hdl) {
        msg_task_destory_handle(isr_hdl->task_hdl);
    }
    sys_uninit2(); //keep this at bottom for mem_uninit()

    return 0;
}

int sim_stop_state_hi_lo()
{
    msg_t *msg_2_hi = (msg_t *)GET_TSK_MSG(sizeof(msg_t));
    msg_t *msg_2_lo = (msg_t *)GET_TSK_MSG(sizeof(msg_t));
    msg_2_hi->id = MSG_TERMINATE_TASK;
    msg_2_lo->id = MSG_TERMINATE_TASK;
    msg_send(TASK_HI_HDL, msg_2_hi);
    msg_send(TASK_LO_HDL, msg_2_lo);
    return 0;
}

int sim_start_with_tool(task_cb_t tool_task_cb)
{
    g_sim_tool.task_cb = tool_task_cb;
    sim_sys_init(&g_sim_tool, NULL);
    return 0;
}

task_cb_lite_t g_task_cb_lite;

void *sim_start_with_tool_lite_cb(void *arg)
{
    (*g_task_cb_lite)((sys_hdl_t *)arg);
    sim_stop_state_hi_lo();
    return NULL;
}

int sim_start_with_tool_lite(task_cb_lite_t tool_task_cb_lite, const char *pass_str)
{
    g_task_cb_lite = tool_task_cb_lite;
    if (pass_str) {
        printf("[START ]: %s\n", pass_str);
    }
    sim_start_with_tool(sim_start_with_tool_lite_cb);
    if (pass_str) {
        printf("[PASS  ]: %s\n", pass_str);
    }
    return 0;
}

int sim_start_with_tool_and_isr(task_cb_t tool_task_cb, task_cb_t isr_task_cb)
{
    g_sim_tool.task_cb = tool_task_cb;
    g_sim_isr.task_cb = isr_task_cb;
    sim_sys_init(&g_sim_tool, &g_sim_isr);
    return 0;
}
