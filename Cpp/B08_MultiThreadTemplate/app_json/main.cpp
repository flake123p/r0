
/*
Offical website: http://rapidjson.org/md_doc_tutorial.html#QueryValue
*/

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include <iostream>
#include <stdio.h>
#include <stdint.h>

using namespace rapidjson;

static const char* kTypeNames[] = 
    { "Null", "False", "True", "Object", "Array", "String", "Number" };

// 1. Parse a JSON string into DOM.
const char* g_json = "{\n\
\"project\" : [10.0, 33.567],\n\
\"axis\" : \"xyz\",\n\
\"option\" : 12.34,\n\
\"theObj\" : {\"abc\" : [12.34, 56.78], \"def\" : 10}}";

int main() {
    
    Document d;
    printf("g_json = %s\n", g_json);
    d.Parse(g_json);

    for (Value::ConstMemberIterator itr = d.MemberBegin();
        itr != d.MemberEnd(); ++itr)
    {
        printf("Type of member %s is %s\n", itr->name.GetString(), kTypeNames[itr->value.GetType()]);
        switch (itr->value.GetType())
        {
            case kArrayType:
                for (auto x = itr->value.GetArray().begin(); x !=  itr->value.GetArray().end(); ++x){
                    printf(">> %f\n", x->GetDouble());
                }
                break;
            case kStringType:
                printf(">> %s\n", itr->value.GetString());
                break;
            case kNumberType:
                printf(">> %f\n", itr->value.GetDouble());
                break;
            case kObjectType:
                auto x = itr->value.GetObject();
                for (Value::ConstMemberIterator it = x.MemberBegin(); it != x.MemberEnd(); ++it) {
                    switch (it->value.GetType())
                    {
                        case kArrayType:
                            for (auto x = it->value.GetArray().begin(); x !=  it->value.GetArray().end(); ++x){
                                printf("ARY >> %s >> %f\n", it->name.GetString(), x->GetDouble());
                            }
                            break;
                        case kStringType:
                            printf("STR >> %s >> %s\n", it->name.GetString(), it->value.GetString());
                            break;
                        case kNumberType:
                            printf("NUM >> %s >> %f\n", it->name.GetString(), it->value.GetDouble());
                            break;
                    }
                }
                break;
        }
    }

    return 0;
}