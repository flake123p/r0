#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include "pupil.h"
#include "sys_init.h"
#include "sys.h"
#include "state.h"
#include "pupil_tool.h"

msg_thread_t msg_task_tool;

int state_sim_tool(msg_t *msg)
{
    printf("[%s]\n", __func__);

    CMD_RESOLUTION_t *data = (CMD_RESOLUTION_t *)msg;
    printf("[%s] data->msg_hdr.id = %x\n", __func__, data->msg_hdr.id);
    switch(data->msg_hdr.id & ~FLAG_CMD_RES)
    {
        case CMD_WORLD_RESOLUTION_SET:
        {
            printf("[%s] data->msg_hdr.id = %x\n", __func__, data->msg_hdr.id);
            printf("[%s] data->msg_hdr.ret = %d\n", __func__, data->msg_hdr.ret);
        } break;
        case CMD_WORLD_RESOLUTION_GET:
        {
            printf("[%s] data->msg_hdr.id = %x\n", __func__, data->msg_hdr.id);
            printf("[%s] data->msg_hdr.ret = %d\n", __func__, data->msg_hdr.ret);
            printf("[%s] data->reso.w = %d\n", __func__, data->reso.w);
            printf("[%s] data->reso.h = %d\n", __func__, data->reso.h);
        } break;
    }
    return 0;
}

void test_state(void)
{
    printf("Hello %s, %d\n", __FILE__, CMD_WORLD_RESOLUTION_SET);

    CMD_RESOLUTION_t *set_world_res = (CMD_RESOLUTION_t *)MEM_ALLOC(sizeof(CMD_RESOLUTION_t));
    CMD_RESOLUTION_t *get_world_res = (CMD_RESOLUTION_t *)MEM_ALLOC(sizeof(CMD_RESOLUTION_t));
    static msg_t end_task = {0};
    end_task.id = MSG_TERMINATE_TASK;

    //start task_hi, task_lo
    OLD_sys_init();
    OLD_sys_ext_init(TASK_SIM_TOOL, &msg_task_tool);

    //set state callback function
    OLD_set_state_cb(TASK_HI, state_hi);
    OLD_set_state_cb(TASK_LO, state_lo);
    OLD_set_state_cb(TASK_SIM_TOOL, state_sim_tool);

    OLD_sys_start();
    OLD_sys_ext_start(&msg_task_tool, task_generic_function);

    printf("[%s] msg = CMD_WORLD_RESOLUTION_SET(%d) , w = 1920 , h = 1080\n", __func__, CMD_WORLD_RESOLUTION_SET);
    set_world_res->msg_hdr.id = CMD_WORLD_RESOLUTION_SET;
    set_world_res->reso.w = 1920;
    set_world_res->reso.h = 1080;
    OLD_send_command_req((msg_t *)set_world_res);

    printf("[%s] msg = CMD_WORLD_RESOLUTION_GET(%d)\n", __func__, CMD_WORLD_RESOLUTION_GET);
    get_world_res->msg_hdr.id = CMD_WORLD_RESOLUTION_GET;
    OLD_send_command_req((msg_t *)get_world_res);

    //end tasks, delay 2s let above commands finished.
    sleep(2);
    OLD_send_message(TASK_HI, &end_task);
    OLD_send_message(TASK_LO, &end_task);
    OLD_send_message(TASK_SIM_TOOL, &end_task);

    //we need this to avoid main function leave immediately.
    OLD_sys_wait();
    OLD_sys_ext_wait(&msg_task_tool);
}



void *task_test_calib_list(void *)
{
    /*
        send
        wait_rx
        

    */
    //printf("[%s]555\n", __func__);
    {
        MSG_REF_IMAGE_t refMsg;
        MSG_REF_IMAGE_t *msg;
        uint32_t id, flag;
        msg = &refMsg;
        msg->msg_hdr.id = CMD_CALIB_REF_LIST_DUMP;
        OLD_send_command_req((msg_t *)msg);
        msg = (MSG_REF_IMAGE_t *)wait_and_receive_message(TASK_SIM_TOOL);
        id = msg->msg_hdr.id & 0x0000FFFF;
        flag = msg->msg_hdr.id & 0xFFFF0000;
        printf("msg id = %u (0x%08X) %d\n", id, flag, msg->msg_hdr.ret);
    }
    {
        MSG_PUPIL_CENTER_t pupilMsg;
        MSG_PUPIL_CENTER_t *msg;
        uint32_t id, flag;
        msg = &pupilMsg;
        msg->msg_hdr.id = CMD_CALIB_PUPIL_LIST_DUMP;
        OLD_send_command_req((msg_t *)msg);
        msg = (MSG_PUPIL_CENTER_t *)wait_and_receive_message(TASK_SIM_TOOL);
        id = msg->msg_hdr.id & 0x0000FFFF;
        flag = msg->msg_hdr.id & 0xFFFF0000;
        printf("msg id = %u (0x%08X) %d\n", id, flag, msg->msg_hdr.ret);
    }

    printf("PASS\n");

    {
        OLD_send_simple_message(TASK_HI, MSG_TERMINATE_TASK);
        OLD_send_simple_message(TASK_LO, MSG_TERMINATE_TASK);
    }

    return NULL;
}

int test_calib_list(void)
{
    int ret = 0;

    //start task_hi, task_lo
    OLD_sys_init();
    OLD_sys_ext_init(TASK_SIM_TOOL, &msg_task_tool);

    //set state callback function
    OLD_set_state_cb(TASK_HI, state_hi);
    OLD_set_state_cb(TASK_LO, state_lo);
    //OLD_set_state_cb(TASK_SIM_TOOL, state_sim_tool);

    OLD_sys_start();
    OLD_sys_ext_start(&msg_task_tool, task_test_calib_list);

    //we need this to avoid main function leave immediately.
    OLD_sys_wait();
    OLD_sys_ext_wait(&msg_task_tool);

    return ret;
}

int test_init_simple(function_t cb)
{
    int ret = 0;

    //start task_hi, task_lo
    OLD_sys_init();
    OLD_sys_ext_init(TASK_SIM_TOOL, &msg_task_tool);

    //set state callback function
    OLD_set_state_cb(TASK_HI, (task_cb)state_hi);
    OLD_set_state_cb(TASK_LO, (task_cb)state_lo);
    //OLD_set_state_cb(TASK_SIM_TOOL, state_sim_tool);

    OLD_sys_start();
    OLD_sys_ext_start(&msg_task_tool, cb);

    //we need this to avoid main function leave immediately.
    OLD_sys_wait();
    OLD_sys_ext_wait(&msg_task_tool);

    return ret;
}

int test_uninit()
{
    OLD_send_simple_message(TASK_HI, MSG_TERMINATE_TASK);
    OLD_send_simple_message(TASK_LO, MSG_TERMINATE_TASK);
    return 0;
}

int sim_start(sys_hdl_t *sys_tool_hdl)
{    
    sys_init2();
    msg_task_new_handle(&sys_tool_hdl->task_hdl, 0);
    cmd_external_interface_config(sys_tool_hdl->task_hdl);

    msg_task_create(sys_tool_hdl->task_hdl, sys_tool_hdl->task_cb, sys_tool_hdl);
    sys_start2();

    msg_task_wait(sys_tool_hdl->task_hdl);
    sys_wait2();

    msg_task_destory_handle(sys_tool_hdl->task_hdl);
    sys_uninit2();
    return 0;
}

sys_hdl_t g_sys_tool;

int sim_start_lite(task_cb_t task_cb)
{
    g_sys_tool.task_cb = task_cb;
    sim_start(&g_sys_tool);
    return 0;
}

int sim_stop_lite()
{
    static msg_t msg_2_hi;
    static msg_t msg_2_lo;
    msg_2_hi.id = MSG_TERMINATE_TASK;
    msg_2_lo.id = MSG_TERMINATE_TASK;
    msg_send(TASK_HI_HDL, &msg_2_hi);
    msg_send(TASK_LO_HDL, &msg_2_lo);
    return 0;
}

void *tc_sys_init_uninit_old(void *)
{
    // static msg_t simple_msg, *msg;
    // msg = &simple_msg;
    // msg->id = CMD_STATE_GET;
    // OLD_send_command_req(msg);
    // msg = wait_and_receive_message(TASK_SIM_TOOL);
    void *ptr = GET_TSK_MSG(2);
    release_task_msg(ptr);
    

    sim_stop_lite();
    printf("PASS: %s\n", __func__);
    return NULL;
}

void *tc_cmd_resolution_old(sys_hdl_t *sys_hdl)
{
    // static msg_t simple_msg, *msg;
    // msg = &simple_msg;
    // msg->id = CMD_STATE_GET;
    // OLD_send_command_req(msg);
    // msg = wait_and_receive_message(TASK_SIM_TOOL);
    CMD_RESOLUTION_t *msg = (CMD_RESOLUTION_t *)GET_CMD_REQ_MSG(sizeof(CMD_RESOLUTION_t));
    msg->msg_hdr.id = CMD_WORLD_RESOLUTION_SET;
    msg->reso.h = 10;
    msg->reso.w = 20;
    msg_send(TASK_LO_HDL, (msg_t *)msg);
    msg = (CMD_RESOLUTION_t *)msg_wait_and_receive(sys_hdl->task_hdl);
    printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
    RELEASE_CMD_RES_MSG(msg);    

    //get
    CMD_RESOLUTION_t *get_msg = (CMD_RESOLUTION_t *)GET_CMD_REQ_MSG(sizeof(CMD_RESOLUTION_t));
    get_msg->msg_hdr.id = CMD_WORLD_RESOLUTION_GET;
    msg_send(TASK_LO_HDL, (msg_t *)get_msg);
    get_msg = (CMD_RESOLUTION_t *)msg_wait_and_receive(sys_hdl->task_hdl);
    printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
    #if 0
    printf("get_msg->reso.h = %d\n", get_msg->reso.h);
    printf("get_msg->reso.w = %d\n", get_msg->reso.w);
    #endif
    RELEASE_CMD_RES_MSG(get_msg);

    sim_stop_lite();
    printf("PASS: %s\n", __func__);
    return NULL;
}


void *tc_state_read_write_old(void *)
{
    GET_ISR_MSG(10);
    sim_stop_lite();
    printf("PASS: %s\n", __func__);
    return NULL;
}

int test_state_main()
{
    sim_start_lite((task_cb_t)tc_sys_init_uninit_old);
    sim_start_lite((task_cb_t)tc_cmd_resolution_old);

    //sim_start_lite(tc_state_read_write_old);

    //g_sys_tool.task_cb = tc_sys_init_uninit_old;
    //g_sys_tool.task_cb = task_generic;
    //g_sys_tool.sm_cb = state_lo;
    //sys_hdl_t *sys_tool_hdl = &g_sys_tool;
    //msg_task_new_handle(&sys_tool_hdl->task_hdl, 0);
    //msg_task_create(sys_tool_hdl->task_hdl, sys_tool_hdl->task_cb, sys_tool_hdl);
    //msg_task_wait(sys_tool_hdl->task_hdl);
    //msg_task_destory_handle(sys_tool_hdl->task_hdl);
    //cmd_external_interface_config(sys_tool_hdl->task_hdl);
    //sim_start(&g_sys_tool);
    return 0;
}
