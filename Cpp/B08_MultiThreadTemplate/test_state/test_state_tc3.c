#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include "pupil.h"
#include "sys_init.h"
#include "sys.h"
#include "state.h"
#include "pupil_tool.h"
#include "test_state.h"

static int get_evt_cnt = 0;
static int retry = 0;

void tc_cmd_accu_state(sys_hdl_t *sys_hdl)
{
    msg_t *res;
    //accu start
    {
        CMD_REQ_SEND_LITE(CMD_ACCU_START);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_ACCU_START);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 0);
        RELEASE_CMD_RES_MSG(res);
    }
    //re-send start, get fail
    {
        // redundant start command
        CMD_REQ_SEND_LITE(CMD_ACCU_START);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_ACCU_START);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 1);
        RELEASE_CMD_RES_MSG(res);
    }
    //abort
    {
        CMD_REQ_SEND_LITE(CMD_ACCU_ABORT);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_ACCU_ABORT);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 0);
        RELEASE_CMD_RES_MSG(res);
    }
    //end, get fail
    {
        CMD_REQ_SEND_LITE(CMD_ACCU_END);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_ACCU_END);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 1);
        RELEASE_CMD_RES_MSG(res);
    }
    //start
    {
        CMD_REQ_SEND_LITE(CMD_ACCU_START);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_ACCU_START);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 0);
        RELEASE_CMD_RES_MSG(res);
    }
    //end
    {
        CMD_REQ_SEND_LITE(CMD_ACCU_END);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_ACCU_END);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 0);
        RELEASE_CMD_RES_MSG(res);
    }
}

void tc_cmd_accu_access(sys_hdl_t *sys_hdl)
{
    #define ACCU_NULL   0
    #define ACCU_VAL    0.888
    //set
    {
        CMD_ACCU_t *msg = (CMD_ACCU_t *)GET_CMD_REQ_MSG(sizeof(CMD_ACCU_t));
        msg->msg_hdr.id = CMD_ACCU_SET;
        msg->is_accu_null = ACCU_NULL;
        msg->accu = ACCU_VAL;
        CMD_REQ_SEND(msg);
        msg = (CMD_ACCU_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_ACCU_SET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(msg) == 0);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
    //get
    {
        CMD_ACCU_t *msg = (CMD_ACCU_t *)GET_CMD_REQ_MSG(sizeof(CMD_ACCU_t));
        msg->msg_hdr.id = CMD_ACCU_GET;
        CMD_REQ_SEND(msg);
        msg = (CMD_ACCU_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_ACCU_GET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(msg) == 0);
        //check value
        BASIC_ASSERT(msg->is_accu_null == ACCU_NULL);
        BASIC_ASSERT(msg->accu == ACCU_VAL);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
}

void tc_cmd_world_intrinsic(sys_hdl_t *sys_hdl)
{
    //set
    {
        CMD_INTRINSIC_t *msg = (CMD_INTRINSIC_t *)GET_CMD_REQ_MSG(sizeof(CMD_INTRINSIC_t));
        msg->msg_hdr.id = CMD_WORLD_INTRINSIC_SET;
        for(int i=0; i<(sizeof(msg->intr.K)/sizeof(FLOAT_T)); i++)  msg->intr.K[i] = i;
        for(int i=0; i<(sizeof(msg->intr.D)/sizeof(FLOAT_T)); i++)  msg->intr.D[i] = i;
        CMD_REQ_SEND(msg);
        msg = (CMD_INTRINSIC_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_WORLD_INTRINSIC_SET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(msg) == 0);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
    //get
    {
        CMD_INTRINSIC_t *msg = (CMD_INTRINSIC_t *)GET_CMD_REQ_MSG(sizeof(CMD_INTRINSIC_t));
        msg->msg_hdr.id = CMD_WORLD_INTRINSIC_GET;
        CMD_REQ_SEND(msg);
        msg = (CMD_INTRINSIC_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_WORLD_INTRINSIC_GET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(msg) == 0);
        //check value
        for(int i=0; i<(sizeof(msg->intr.K)/sizeof(FLOAT_T)); i++) {
            BASIC_ASSERT(msg->intr.K[i] == i);
        }
        for(int i=0; i<(sizeof(msg->intr.D)/sizeof(FLOAT_T)); i++) {
            BASIC_ASSERT(msg->intr.D[i] == i);
        }
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
}

void tc_cmd_world_focal_len(sys_hdl_t *sys_hdl)
{
    #define FOCAL_LEN_VAL   1234
    //set
    {
        CMD_FOCAL_LEN_t *msg = (CMD_FOCAL_LEN_t *)GET_CMD_REQ_MSG(sizeof(CMD_FOCAL_LEN_t));
        msg->msg_hdr.id = CMD_WORLD_FOCAL_LEN_SET;
        msg->flen.len = FOCAL_LEN_VAL;
        CMD_REQ_SEND(msg);
        msg = (CMD_FOCAL_LEN_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_WORLD_FOCAL_LEN_SET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(msg) == 0);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
    //get
    {
        CMD_FOCAL_LEN_t *msg = (CMD_FOCAL_LEN_t *)GET_CMD_REQ_MSG(sizeof(CMD_FOCAL_LEN_t));
        msg->msg_hdr.id = CMD_WORLD_FOCAL_LEN_GET;
        CMD_REQ_SEND(msg);
        msg = (CMD_FOCAL_LEN_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_WORLD_FOCAL_LEN_GET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(msg) == 0);
        //check value
        BASIC_ASSERT(msg->flen.len == FOCAL_LEN_VAL);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
}

void tc_cmd_compensate_flag(sys_hdl_t *sys_hdl)
{
    //set 0
    {
        msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
        msg->id = CMD_COMPENSATE_FLAG_SET;
        msg->val = 0;
        CMD_REQ_SEND(msg);
        msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_COMPENSATE_FLAG_SET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(msg) == 0);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
    //get
    {
        msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
        msg->id = CMD_COMPENSATE_FLAG_GET;
        CMD_REQ_SEND(msg);
        msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_COMPENSATE_FLAG_GET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        //check value
        BASIC_ASSERT(MSG_RET(msg) == 0);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
    //set 1
    {
        msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
        msg->id = CMD_COMPENSATE_FLAG_SET;
        msg->val = 1;
        CMD_REQ_SEND(msg);
        msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_COMPENSATE_FLAG_SET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(msg) == 0);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
    //get
    {
        msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
        msg->id = CMD_COMPENSATE_FLAG_GET;
        CMD_REQ_SEND(msg);
        msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_COMPENSATE_FLAG_GET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        //check value
        BASIC_ASSERT(MSG_RET(msg) == 1);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
}

void tc_cmd_predict_flag(sys_hdl_t *sys_hdl)
{
    //set 0
    {
        msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
        msg->id = CMD_PREDICT_FLAG_SET;
        msg->val = 0;
        CMD_REQ_SEND(msg);
        msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_PREDICT_FLAG_SET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(msg) == 0);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
    //get
    {
        msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
        msg->id = CMD_PREDICT_FLAG_GET;
        CMD_REQ_SEND(msg);
        msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_PREDICT_FLAG_GET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        //check value
        BASIC_ASSERT(MSG_RET(msg) == 0);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
    //set 1
    {
        msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
        msg->id = CMD_PREDICT_FLAG_SET;
        msg->val = 1;
        CMD_REQ_SEND(msg);
        msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_PREDICT_FLAG_SET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(msg) == 0);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
    //get
    {
        msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
        msg->id = CMD_PREDICT_FLAG_GET;
        CMD_REQ_SEND(msg);
        msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_PREDICT_FLAG_GET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        //check value
        BASIC_ASSERT(MSG_RET(msg) == 1);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
}

void tc_cmd_ref_return_flag(sys_hdl_t *sys_hdl)
{
    //set 0
    {
        msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
        msg->id = CMD_REF_RETURN_FLAG_SET;
        msg->val = 0;
        CMD_REQ_SEND(msg);
        msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_REF_RETURN_FLAG_SET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(msg) == 0);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
    //get
    {
        msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
        msg->id = CMD_REF_RETURN_FLAG_GET;
        CMD_REQ_SEND(msg);
        msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_REF_RETURN_FLAG_GET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        //check value
        BASIC_ASSERT(MSG_RET(msg) == 0);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
    //set 1
    {
        msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
        msg->id = CMD_REF_RETURN_FLAG_SET;
        msg->val = 1;
        CMD_REQ_SEND(msg);
        msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_REF_RETURN_FLAG_SET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(msg) == 0);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
    //get
    {
        msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
        msg->id = CMD_REF_RETURN_FLAG_GET;
        CMD_REQ_SEND(msg);
        msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_REF_RETURN_FLAG_GET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        //check value
        BASIC_ASSERT(MSG_RET(msg) == 1);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
}

void tc_cmd_ebc_seq(sys_hdl_t *sys_hdl)
{
    //set 0
    {
        msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
        msg->id = CMD_EBC_SEQ_SET;
        msg->val = 0;
        CMD_REQ_SEND(msg);
        msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_EBC_SEQ_SET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(msg) == 0);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
    //get
    {
        msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
        msg->id = CMD_EBC_SEQ_GET;
        CMD_REQ_SEND(msg);
        msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_EBC_SEQ_GET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        //check value
        BASIC_ASSERT(MSG_RET(msg) == 0);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
    //set 1
    {
        msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
        msg->id = CMD_EBC_SEQ_SET;
        msg->val = 1;
        CMD_REQ_SEND(msg);
        msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_EBC_SEQ_SET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(msg) == 0);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
    //get
    {
        msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
        msg->id = CMD_EBC_SEQ_GET;
        CMD_REQ_SEND(msg);
        msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_EBC_SEQ_GET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        //check value
        BASIC_ASSERT(MSG_RET(msg) == 1);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
}

void tc_cmd_calib_ebc(sys_hdl_t *sys_hdl)
{
    msg_t *res;
    uint32_t seq_no;
    #define EBC_SET_TYPE 3
    #define EBC_VAL_X(a) ((a+1.0)/100)
    #define EBC_VAL_Y(a) ((a+2.0)/100)
    uint32_t set_cmd[3] = {CMD_CALIB_EBC_SET, MSG_CALIB_EBC_SET, ISR_CALIB_EBC_SET};
    uint32_t get_cmd = CMD_CALIB_EBC_GET;

    for(int i=0; i<EBC_SET_TYPE; i++) {
        //get seq no
        {
            msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
            msg->id = CMD_EBC_SEQ_GET;
            CMD_REQ_SEND(msg);
            msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
            //check response
            BASIC_ASSERT(MSG_ID(msg) == CMD_EBC_SEQ_GET);
            BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
            seq_no = msg->ret;
            //release msg
            RELEASE_CMD_RES_MSG(msg);
        }
        //set ebc
        {
            CMD_MSG_EBC_t *msg;
            if(i==0)        { msg = (CMD_MSG_EBC_t *)GET_CMD_REQ_MSG(sizeof(CMD_MSG_EBC_t)); }
            else if(i==1)   { msg = (CMD_MSG_EBC_t *)GET_TSK_MSG(sizeof(CMD_MSG_EBC_t)); }
            else if(i==2)   { msg = (CMD_MSG_EBC_t *)GET_ISR_MSG(sizeof(CMD_MSG_EBC_t)); }
            else            { BASIC_ASSERT(0); }
            msg->msg_hdr.id = set_cmd[i];
            msg->ebc.x = EBC_VAL_X(i);
            msg->ebc.y = EBC_VAL_Y(i);
            if(i==0)        { CMD_REQ_SEND(msg); }
            else if(i==1)   { MSG_SEND_HI(msg); }
            else if(i==2)   { ISR_SEND_HI(msg); }
            else            { BASIC_ASSERT(0); }
            //only CMD need waiting
            if(i==0) {
                msg = (CMD_MSG_EBC_t *)msg_wait_and_receive(sys_hdl->task_hdl);
                //check response
                BASIC_ASSERT(MSG_ID(msg) == set_cmd[i]);
                BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
                BASIC_ASSERT(MSG_RET(msg) == 0);
                //release msg
                RELEASE_CMD_RES_MSG(msg);
            }
        }
        //wait
        do
        {
            static int retry = 0;
            CMD_REQ_SEND_LITE(CMD_EBC_SEQ_GET);
            res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
            //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(res), MSG_FLAG(res), MSG_RET(res));
            BASIC_ASSERT(MSG_ID(res) == CMD_EBC_SEQ_GET);
            BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
            //BASIC_ASSERT(MSG_RET(res) == 0);
            //printf("%d\n", res->ret);
            if (res->ret == (seq_no+1)) {
                RELEASE_CMD_RES_MSG(res);
                break;
            } else {
                RELEASE_CMD_RES_MSG(res);
                retry++;
                BASIC_ASSERT(retry<1000);
            }
        } while (1);
        //get ebc
        {
            CMD_MSG_EBC_t *msg = (CMD_MSG_EBC_t *)GET_CMD_REQ_MSG(sizeof(CMD_MSG_EBC_t));
            msg->msg_hdr.id = get_cmd;
            CMD_REQ_SEND(msg);
            msg = (CMD_MSG_EBC_t *)msg_wait_and_receive(sys_hdl->task_hdl);
            //check response
            BASIC_ASSERT(MSG_ID(msg) == get_cmd);
            BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
            BASIC_ASSERT(MSG_RET(msg) == 0);
            //check value
            BASIC_ASSERT(msg->ebc.x == EBC_VAL_X(i));
            BASIC_ASSERT(msg->ebc.y == EBC_VAL_Y(i));
            //release msg
            RELEASE_CMD_RES_MSG(msg);
        }
    }
}

void tc_cmd_base_ebc(sys_hdl_t *sys_hdl)
{
    msg_t *res;
    uint32_t seq_no;
    #define EBC_SET_TYPE 3
    #define EBC_VAL_X(a) ((a+1.0)/100)
    #define EBC_VAL_Y(a) ((a+2.0)/100)
    uint32_t set_cmd[3] = {CMD_BASE_EBC_SET, MSG_BASE_EBC_SET, ISR_BASE_EBC_SET};
    uint32_t get_cmd = CMD_BASE_EBC_GET;

    for(int i=0; i<EBC_SET_TYPE; i++) {
        //get seq no
        {
            msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
            msg->id = CMD_EBC_SEQ_GET;
            CMD_REQ_SEND(msg);
            msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
            //check response
            BASIC_ASSERT(MSG_ID(msg) == CMD_EBC_SEQ_GET);
            BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
            seq_no = msg->ret;
            //release msg
            RELEASE_CMD_RES_MSG(msg);
        }
        //set ebc
        {
            CMD_MSG_EBC_t *msg;
            if(i==0)        { msg = (CMD_MSG_EBC_t *)GET_CMD_REQ_MSG(sizeof(CMD_MSG_EBC_t)); }
            else if(i==1)   { msg = (CMD_MSG_EBC_t *)GET_TSK_MSG(sizeof(CMD_MSG_EBC_t)); }
            else if(i==2)   { msg = (CMD_MSG_EBC_t *)GET_ISR_MSG(sizeof(CMD_MSG_EBC_t)); }
            else            { BASIC_ASSERT(0); }
            msg->msg_hdr.id = set_cmd[i];
            msg->ebc.x = EBC_VAL_X(i);
            msg->ebc.y = EBC_VAL_Y(i);
            if(i==0)        { CMD_REQ_SEND(msg); }
            else if(i==1)   { MSG_SEND_HI(msg); }
            else if(i==2)   { ISR_SEND_HI(msg); }
            else            { BASIC_ASSERT(0); }
            //only CMD need waiting
            if(i==0) {
                msg = (CMD_MSG_EBC_t *)msg_wait_and_receive(sys_hdl->task_hdl);
                //check response
                BASIC_ASSERT(MSG_ID(msg) == set_cmd[i]);
                BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
                BASIC_ASSERT(MSG_RET(msg) == 0);
                //release msg
                RELEASE_CMD_RES_MSG(msg);
            }
        }
        //wait
        do
        {
            static int retry = 0;
            CMD_REQ_SEND_LITE(CMD_EBC_SEQ_GET);
            res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
            //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(res), MSG_FLAG(res), MSG_RET(res));
            BASIC_ASSERT(MSG_ID(res) == CMD_EBC_SEQ_GET);
            BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
            //BASIC_ASSERT(MSG_RET(res) == 0);
            //printf("%d\n", res->ret);
            if (res->ret == (seq_no+1)) {
                RELEASE_CMD_RES_MSG(res);
                break;
            } else {
                RELEASE_CMD_RES_MSG(res);
                retry++;
                BASIC_ASSERT(retry<1000);
            }
        } while (1);
        //get ebc
        {
            CMD_MSG_EBC_t *msg = (CMD_MSG_EBC_t *)GET_CMD_REQ_MSG(sizeof(CMD_MSG_EBC_t));
            msg->msg_hdr.id = get_cmd;
            CMD_REQ_SEND(msg);
            msg = (CMD_MSG_EBC_t *)msg_wait_and_receive(sys_hdl->task_hdl);
            //check response
            BASIC_ASSERT(MSG_ID(msg) == get_cmd);
            BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
            BASIC_ASSERT(MSG_RET(msg) == 0);
            //check value
            BASIC_ASSERT(msg->ebc.x == EBC_VAL_X(i));
            BASIC_ASSERT(msg->ebc.y == EBC_VAL_Y(i));
            //release msg
            RELEASE_CMD_RES_MSG(msg);
        }
    }
}

void tc_cmd_curr_ebc(sys_hdl_t *sys_hdl)
{
    msg_t *res;
    uint32_t seq_no;
    #define EBC_SET_TYPE 3
    #define EBC_VAL_X(a) ((a+1.0)/100)
    #define EBC_VAL_Y(a) ((a+2.0)/100)
    uint32_t set_cmd[3] = {CMD_CURR_EBC_SET, MSG_CURR_EBC_SET, ISR_CURR_EBC_SET};
    uint32_t get_cmd = CMD_CURR_EBC_GET;

    for(int i=0; i<EBC_SET_TYPE; i++) {
        //get seq no
        {
            msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
            msg->id = CMD_EBC_SEQ_GET;
            CMD_REQ_SEND(msg);
            msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
            //check response
            BASIC_ASSERT(MSG_ID(msg) == CMD_EBC_SEQ_GET);
            BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
            seq_no = msg->ret;
            //release msg
            RELEASE_CMD_RES_MSG(msg);
        }
        //set ebc
        {
            CMD_MSG_EBC_t *msg;
            if(i==0)        { msg = (CMD_MSG_EBC_t *)GET_CMD_REQ_MSG(sizeof(CMD_MSG_EBC_t)); }
            else if(i==1)   { msg = (CMD_MSG_EBC_t *)GET_TSK_MSG(sizeof(CMD_MSG_EBC_t)); }
            else if(i==2)   { msg = (CMD_MSG_EBC_t *)GET_ISR_MSG(sizeof(CMD_MSG_EBC_t)); }
            else            { BASIC_ASSERT(0); }
            msg->msg_hdr.id = set_cmd[i];
            msg->ebc.x = EBC_VAL_X(i);
            msg->ebc.y = EBC_VAL_Y(i);
            if(i==0)        { CMD_REQ_SEND(msg); }
            else if(i==1)   { MSG_SEND_HI(msg); }
            else if(i==2)   { ISR_SEND_HI(msg); }
            else            { BASIC_ASSERT(0); }
            //only CMD need waiting
            if(i==0) {
                msg = (CMD_MSG_EBC_t *)msg_wait_and_receive(sys_hdl->task_hdl);
                //check response
                BASIC_ASSERT(MSG_ID(msg) == set_cmd[i]);
                BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
                BASIC_ASSERT(MSG_RET(msg) == 0);
                //release msg
                RELEASE_CMD_RES_MSG(msg);
            }
        }
        //wait
        do
        {
            static int retry = 0;
            CMD_REQ_SEND_LITE(CMD_EBC_SEQ_GET);
            res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
            //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(res), MSG_FLAG(res), MSG_RET(res));
            BASIC_ASSERT(MSG_ID(res) == CMD_EBC_SEQ_GET);
            BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
            //BASIC_ASSERT(MSG_RET(res) == 0);
            //printf("%d\n", res->ret);
            if (res->ret == (seq_no+1)) {
                RELEASE_CMD_RES_MSG(res);
                break;
            } else {
                RELEASE_CMD_RES_MSG(res);
                retry++;
                BASIC_ASSERT(retry<1000);
            }
        } while (1);
        //get ebc
        {
            CMD_MSG_EBC_t *msg = (CMD_MSG_EBC_t *)GET_CMD_REQ_MSG(sizeof(CMD_MSG_EBC_t));
            msg->msg_hdr.id = get_cmd;
            CMD_REQ_SEND(msg);
            msg = (CMD_MSG_EBC_t *)msg_wait_and_receive(sys_hdl->task_hdl);
            //check response
            BASIC_ASSERT(MSG_ID(msg) == get_cmd);
            BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
            BASIC_ASSERT(MSG_RET(msg) == 0);
            //check value
            BASIC_ASSERT(msg->ebc.x == EBC_VAL_X(i));
            BASIC_ASSERT(msg->ebc.y == EBC_VAL_Y(i));
            //release msg
            RELEASE_CMD_RES_MSG(msg);
        }
    }
}

void tc_evt_gaze_predict(sys_hdl_t *sys_hdl)
{
    pupil_center_t golden1 = {11, 22, 1, 7777};
    pupil_center_t golden2 = {12, 33, 0, 8888};
    pupil_center_t golden3 = {13, 44, 1, 9999};
    msg_t *res;
    //set 1
    {
        CMD_REQ_SEND_LITE2(CMD_PREDICT_FLAG_SET, 1);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(res) == CMD_PREDICT_FLAG_SET);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 0);
        //release msg
        RELEASE_CMD_RES_MSG(res);
    }
    // ISR_MSG_PUPIL_CENTER_IN *3
    {
        MSG_PUPIL_CENTER_t *pupil_msg = (MSG_PUPIL_CENTER_t *)GET_ISR_MSG(sizeof(MSG_PUPIL_CENTER_t));
        pupil_msg->msg_hdr.id = ISR_MSG_PUPIL_CENTER_IN;
        pupil_msg->pupil_center = golden1;
        ISR_SEND_HI(pupil_msg);
    }
    {
        MSG_PUPIL_CENTER_t *pupil_msg = (MSG_PUPIL_CENTER_t *)GET_ISR_MSG(sizeof(MSG_PUPIL_CENTER_t));
        pupil_msg->msg_hdr.id = ISR_MSG_PUPIL_CENTER_IN;
        pupil_msg->pupil_center = golden2;
        ISR_SEND_HI(pupil_msg);
    }
    {
        MSG_PUPIL_CENTER_t *pupil_msg = (MSG_PUPIL_CENTER_t *)GET_ISR_MSG(sizeof(MSG_PUPIL_CENTER_t));
        pupil_msg->msg_hdr.id = ISR_MSG_PUPIL_CENTER_IN;
        pupil_msg->pupil_center = golden3;
        ISR_SEND_HI(pupil_msg);
    }
    {
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        EVT_GAZE_POS_t *get_gaze = (EVT_GAZE_POS_t *)res;
        BASIC_ASSERT(MSG_ID(res) == EVT_GAZE_POS);
        BASIC_ASSERT(get_gaze->gaze_pos.confi == golden1.confi);
        BASIC_ASSERT(get_gaze->gaze_pos.ts == golden1.ts);
        RELEASE_CMD_RES_MSG(res);
    }
    {
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        EVT_GAZE_POS_t *get_gaze = (EVT_GAZE_POS_t *)res;
        BASIC_ASSERT(MSG_ID(res) == EVT_GAZE_POS);
        BASIC_ASSERT(get_gaze->gaze_pos.confi == golden2.confi);
        BASIC_ASSERT(get_gaze->gaze_pos.ts == golden2.ts);
        RELEASE_CMD_RES_MSG(res);
    }
    {
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        EVT_GAZE_POS_t *get_gaze = (EVT_GAZE_POS_t *)res;
        BASIC_ASSERT(MSG_ID(res) == EVT_GAZE_POS);
        BASIC_ASSERT(get_gaze->gaze_pos.confi == golden3.confi);
        BASIC_ASSERT(get_gaze->gaze_pos.ts == golden3.ts);
        RELEASE_CMD_RES_MSG(res);
    }
}

void tc_evt_ref_pos_return(sys_hdl_t *sys_hdl)
{
    int retry = 0;
    msg_t *res;
    // CMD_CALIB_START
    {
        CMD_REQ_SEND_LITE(CMD_CALIB_START);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_CALIB_START);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 0);
        RELEASE_CMD_RES_MSG(res);
    }
    // CMD_REF_RETURN_FLAG_SET 1
    {
        msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
        msg->id = CMD_REF_RETURN_FLAG_SET;
        msg->val = 1;
        CMD_REQ_SEND(msg);
        msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_REF_RETURN_FLAG_SET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(msg) == 0);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
    // ISR_MSG_REF_IMAGE_IN ISR_MSG_REF_IMAGE_IN ISR_MSG_REF_IMAGE_IN
    for(int i=0; i<3; i++)
    {
        MSG_REF_IMAGE_t *ref_img = (MSG_REF_IMAGE_t *)GET_ISR_MSG(sizeof(MSG_REF_IMAGE_t));
        uint8_t *allocImg = (uint8_t *)MEM_ALLOC(4);
        ref_img->msg_hdr.id = ISR_MSG_REF_IMAGE_IN;
        ref_img->desc.start = allocImg;
        ref_img->desc.ts = 1200 + i;
        ISR_SEND_HI(ref_img);
    }
    // EVT_REF_POS EVT_REF_POS EVT_REF_POS
    get_evt_cnt = 0;
    retry = 0;
    do
    {
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("abcc id=%d, flag=0x%08X, ret=%d\n", MSG_ID(res), MSG_FLAG(res), MSG_RET(res));
        BASIC_ASSERT(MSG_ID(res) == EVT_REF_POS);
        get_evt_cnt++;
        //BASIC_ASSERT(MSG_RET(res) == 0);
        //printf("%d\n", res->ret);
        if (get_evt_cnt == 3) {
            RELEASE_EVT_MSG(res);
            break;
        } else {
            RELEASE_EVT_MSG(res);
            retry++;
            BASIC_ASSERT(retry<1000);
        }
    } while (1);
    // CMD_CALIB_REF_CNT_GET == 3
    retry = 0;
    do
    {
        CMD_REQ_SEND_LITE(CMD_CALIB_REF_CNT_GET);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(res), MSG_FLAG(res), MSG_RET(res));
        BASIC_ASSERT(MSG_ID(res) == CMD_CALIB_REF_CNT_GET);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        //BASIC_ASSERT(MSG_RET(res) == 0);
        //printf("%d\n", res->ret);
        if (res->ret == 3) {
            RELEASE_CMD_RES_MSG(res);
            break;
        } else {
            RELEASE_CMD_RES_MSG(res);
            retry++;
            BASIC_ASSERT(retry<1000);
        }
    } while (1);
    // CMD_REF_RETURN_FLAG_SET 0
    {
        msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
        msg->id = CMD_REF_RETURN_FLAG_SET;
        msg->val = 0;
        CMD_REQ_SEND(msg);
        msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_REF_RETURN_FLAG_SET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(msg) == 0);
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
    // ISR_MSG_REF_IMAGE_IN ISR_MSG_REF_IMAGE_IN ISR_MSG_REF_IMAGE_IN
    for(int i=0; i<3; i++)
    {
        MSG_REF_IMAGE_t *ref_img = (MSG_REF_IMAGE_t *)GET_ISR_MSG(sizeof(MSG_REF_IMAGE_t));
        uint8_t *allocImg = (uint8_t *)MEM_ALLOC(4);
        ref_img->msg_hdr.id = ISR_MSG_REF_IMAGE_IN;
        ref_img->desc.start = allocImg;
        ref_img->desc.ts = 1200 + i;
        ISR_SEND_HI(ref_img);
    }
    // CMD_CALIB_REF_CNT_GET == 3
    retry = 0;
    do
    {
        CMD_REQ_SEND_LITE(CMD_CALIB_REF_CNT_GET);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(res), MSG_FLAG(res), MSG_RET(res));
        BASIC_ASSERT(MSG_ID(res) == CMD_CALIB_REF_CNT_GET);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        //BASIC_ASSERT(MSG_RET(res) == 0);
        //printf("%d\n", res->ret);
        if (res->ret == 6) {
            RELEASE_CMD_RES_MSG(res);
            break;
        } else {
            RELEASE_CMD_RES_MSG(res);
            retry++;
            BASIC_ASSERT(retry<1000);
        }
    } while (1);
    {
        CMD_REQ_SEND_LITE(CMD_CALIB_END);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_CALIB_END);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 0);
        RELEASE_CMD_RES_MSG(res);
    }
}

int test_state_tc3()
{
    SIM_START_WITH_TOOL_LITE(tc_cmd_accu_state);
    SIM_START_WITH_TOOL_LITE(tc_cmd_accu_access);

    SIM_START_WITH_TOOL_LITE(tc_cmd_world_intrinsic);
    SIM_START_WITH_TOOL_LITE(tc_cmd_world_focal_len);
    SIM_START_WITH_TOOL_LITE(tc_cmd_compensate_flag);
    SIM_START_WITH_TOOL_LITE(tc_cmd_predict_flag);
    SIM_START_WITH_TOOL_LITE(tc_cmd_ref_return_flag);
    SIM_START_WITH_TOOL_LITE(tc_cmd_ebc_seq);
    SIM_START_WITH_TOOL_LITE(tc_cmd_calib_ebc);
    SIM_START_WITH_TOOL_LITE(tc_cmd_base_ebc);
    SIM_START_WITH_TOOL_LITE(tc_cmd_curr_ebc);
    SIM_START_WITH_TOOL_LITE(tc_evt_gaze_predict);
    SIM_START_WITH_TOOL_LITE(tc_evt_ref_pos_return);
    SIM_START_WITH_TOOL_LITE(tc_evt_ref_pos_return);

    return 0;
}