#ifndef _TEST_STATE_H_INCLUDED_
#define _TEST_STATE_H_INCLUDED_

#include "sys.h"
#include "sys_init.h"

void test_state();
int test_calib_list(void);
int test_compensate(void);


// Internal
int test_init_simple(function_t cb);
int test_uninit();

int test_state_main();
int test_state_tc();
int test_state_tc2();
int test_state_tc3();

#endif//_TEST_STATE_H_INCLUDED_