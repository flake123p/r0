#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include "pupil.h"
#include "sys_init.h"
#include "sys.h"
#include "state.h"
#include "test_state.h"

void *tc_compensate(void *)
{
    /*
        send
        wait_rx
        

    */
    //printf("[%s]555\n", __func__);
    {
        MSG_REF_IMAGE_t refMsg;
        MSG_REF_IMAGE_t *msg;
        uint32_t id, flag;
        msg = &refMsg;
        msg->msg_hdr.id = CMD_CALIB_REF_LIST_DUMP;
        OLD_send_command_req((msg_t *)msg);
        msg = (MSG_REF_IMAGE_t *)wait_and_receive_message(TASK_SIM_TOOL);
        id = msg->msg_hdr.id & 0x0000FFFF;
        flag = msg->msg_hdr.id & 0xFFFF0000;
        printf("msg id = %u (0x%08X) %d\n", id, flag, msg->msg_hdr.ret);
    }
    {
        MSG_PUPIL_CENTER_t pupilMsg;
        MSG_PUPIL_CENTER_t *msg;
        uint32_t id, flag;
        msg = &pupilMsg;
        msg->msg_hdr.id = CMD_CALIB_PUPIL_LIST_DUMP;
        OLD_send_command_req((msg_t *)msg);
        msg = (MSG_PUPIL_CENTER_t *)wait_and_receive_message(TASK_SIM_TOOL);
        id = msg->msg_hdr.id & 0x0000FFFF;
        flag = msg->msg_hdr.id & 0xFFFF0000;
        printf("msg id = %u (0x%08X) %d\n", id, flag, msg->msg_hdr.ret);
    }

    printf("PASS: %s\n", __func__);

    test_uninit();

    return NULL;
}



int test_compensate(void)
{
    test_init_simple(tc_compensate);
    //test_init_simple(tc_state_read_write);

    return 0;
}