#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include "pupil.h"
#include "sys_init.h"
#include "sys.h"
#include "state.h"
#include "pupil_tool.h"


void tc_sys_init_uninit(sys_hdl_t *sys_hdl)
{
    //do nothing, just test the init() & uninit()
}

void tc_cmd_resolution(sys_hdl_t *sys_hdl)
{
    uint32_t gold_h = 333444;
    uint32_t gold_w = 555666;
    //Set
    {
        CMD_RESOLUTION_t *msg = (CMD_RESOLUTION_t *)GET_CMD_REQ_MSG(sizeof(CMD_RESOLUTION_t));
        msg->msg_hdr.id = CMD_WORLD_RESOLUTION_SET;
        msg->reso.h = gold_h;
        msg->reso.w = gold_w;
        CMD_REQ_SEND(msg);
        msg = (CMD_RESOLUTION_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(msg) == CMD_WORLD_RESOLUTION_SET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(msg) == 0);
        RELEASE_CMD_RES_MSG(msg);
    }
    //Get
    {
        CMD_RESOLUTION_t *msg = (CMD_RESOLUTION_t *)GET_CMD_REQ_MSG(sizeof(CMD_RESOLUTION_t));
        msg->msg_hdr.id = CMD_WORLD_RESOLUTION_GET;
        CMD_REQ_SEND(msg);
        msg = (CMD_RESOLUTION_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(msg) == CMD_WORLD_RESOLUTION_GET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(msg) == 0);
        BASIC_ASSERT(msg->reso.h == gold_h);
        BASIC_ASSERT(msg->reso.w == gold_w);
        RELEASE_CMD_RES_MSG(msg);
    }
}

void tc_cmd_pupil_center(sys_hdl_t *sys_hdl)
{
    pupil_center_t golden1 = {11, 22, 1, 7777};
    pupil_center_t golden2 = {12, 33, 0, 8888};
    pupil_center_t golden3 = {13, 44, 1, 9999};
    msg_t *res;

    {
        CMD_REQ_SEND_LITE(CMD_CALIB_START);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_CALIB_START);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 0);
        RELEASE_CMD_RES_MSG(res);
    }
    {
        MSG_PUPIL_CENTER_t *pupil_msg = (MSG_PUPIL_CENTER_t *)GET_ISR_MSG(sizeof(MSG_PUPIL_CENTER_t));
        pupil_msg->msg_hdr.id = ISR_MSG_PUPIL_CENTER_IN;
        pupil_msg->pupil_center = golden1;
        ISR_SEND_HI(pupil_msg);
    }
    {
        MSG_PUPIL_CENTER_t *pupil_msg = (MSG_PUPIL_CENTER_t *)GET_ISR_MSG(sizeof(MSG_PUPIL_CENTER_t));
        pupil_msg->msg_hdr.id = ISR_MSG_PUPIL_CENTER_IN;
        pupil_msg->pupil_center = golden2;
        ISR_SEND_HI(pupil_msg);
    }
    {
        MSG_PUPIL_CENTER_t *pupil_msg = (MSG_PUPIL_CENTER_t *)GET_ISR_MSG(sizeof(MSG_PUPIL_CENTER_t));
        pupil_msg->msg_hdr.id = ISR_MSG_PUPIL_CENTER_IN;
        pupil_msg->pupil_center = golden3;
        ISR_SEND_HI(pupil_msg);
    }

    do
    {
        static int retry = 0;
        CMD_REQ_SEND_LITE(CMD_CALIB_PUPIL_CNT_GET);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(res), MSG_FLAG(res), MSG_RET(res));
        BASIC_ASSERT(MSG_ID(res) == CMD_CALIB_PUPIL_CNT_GET);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        //BASIC_ASSERT(MSG_RET(res) == 0);
        //printf("%d\n", res->ret);
        if (res->ret == 3) {
            RELEASE_CMD_RES_MSG(res);
            break;
        } else {
            RELEASE_CMD_RES_MSG(res);
            retry++;
            BASIC_ASSERT(retry<1000);
        }
    } while (1);

    {
        CMD_REQ_SEND_LITE(CMD_CALIB_ABORT);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_CALIB_ABORT);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 0);
        RELEASE_CMD_RES_MSG(res);
    }
}

void tc_cmd_state_read_write(sys_hdl_t *sys_hdl)
{
    msg_t *res;
    {
        CMD_REQ_SEND_LITE(CMD_STATE_GET);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_STATE_GET);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == STATE_DEFAULT);
        RELEASE_CMD_RES_MSG(res);
    }
    // {
    //     CMD_REQ_SEND_LITE2(CMD_STATE_SET, STATE_PREDICTING);
    //     res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
    //     //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
    //     BASIC_ASSERT(MSG_ID(res) == CMD_STATE_SET);
    //     BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
    //     BASIC_ASSERT(MSG_RET(res) == 0);
    //     RELEASE_CMD_RES_MSG(res);
    // }
    // {
    //     CMD_REQ_SEND_LITE(CMD_STATE_GET);
    //     res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
    //     //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
    //     BASIC_ASSERT(MSG_ID(res) == CMD_STATE_GET);
    //     BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
    //     BASIC_ASSERT(MSG_RET(res) == STATE_PREDICTING);
    //     RELEASE_CMD_RES_MSG(res);
    // }
    {
        CMD_REQ_SEND_LITE2(CMD_STATE_SET, STATE_CALIB);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_STATE_SET);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 0);
        RELEASE_CMD_RES_MSG(res);
    }
    {
        CMD_REQ_SEND_LITE(CMD_STATE_GET);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_STATE_GET);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == STATE_CALIB);
        RELEASE_CMD_RES_MSG(res);
    }

    // store STATE_CALIB
    {
        CMD_REQ_SEND_LITE(CMD_STATE_STORE);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_STATE_STORE);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 0);
        RELEASE_CMD_RES_MSG(res);
    }

    // set STATE_ACCU
    {
        CMD_REQ_SEND_LITE2(CMD_STATE_SET, STATE_ACCU);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_STATE_SET);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 0);
        RELEASE_CMD_RES_MSG(res);
    }
    {
        CMD_REQ_SEND_LITE(CMD_STATE_GET);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_STATE_GET);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == STATE_ACCU);
        RELEASE_CMD_RES_MSG(res);
    }

    // restore to STATE_CALIB
    {
        CMD_REQ_SEND_LITE(CMD_STATE_RESTORE);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_STATE_RESTORE);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 0);
        RELEASE_CMD_RES_MSG(res);
    }
    {
        CMD_REQ_SEND_LITE(CMD_STATE_GET);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_STATE_GET);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == STATE_CALIB);
        RELEASE_CMD_RES_MSG(res);
    }

    // reset to default
    {
        CMD_REQ_SEND_LITE(CMD_STATE_RESET);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_STATE_RESET);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 0);
        RELEASE_CMD_RES_MSG(res);
    }
    {
        CMD_REQ_SEND_LITE(CMD_STATE_GET);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_STATE_GET);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == STATE_DEFAULT);
        RELEASE_CMD_RES_MSG(res);
    }
}

void tc_cmd_do_predict(sys_hdl_t *sys_hdl)
{
    msg_t *res;
    {
        CMD_DO_GAZE_PREDICT_t *req = (CMD_DO_GAZE_PREDICT_t *)GET_CMD_REQ_MSG(sizeof(CMD_DO_GAZE_PREDICT_t));
        req->do_compensate = 1;
        req->msg_hdr.id = CMD_DO_GAZE_PREDICT;
        CMD_REQ_SEND(req);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_DO_GAZE_PREDICT);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 0);
        RELEASE_CMD_RES_MSG(res);
    }
}

void tc_cmd_do_compensate(sys_hdl_t *sys_hdl)
{
    // both ebc x/y diff = 1
    msg_t *res;
    uint32_t seq_no;
    //1. get seq no
    {
        msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
        msg->id = CMD_EBC_SEQ_GET;
        CMD_REQ_SEND(msg);
        msg = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //check response
        BASIC_ASSERT(MSG_ID(msg) == CMD_EBC_SEQ_GET);
        BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
        seq_no = msg->ret;
        //release msg
        RELEASE_CMD_RES_MSG(msg);
    }
    //2. set curr ebc
    {
        CMD_MSG_EBC_t *msg = (CMD_MSG_EBC_t *)GET_ISR_MSG(sizeof(CMD_MSG_EBC_t));
        msg->msg_hdr.id = ISR_CURR_EBC_SET;
        msg->ebc.x = 2;
        msg->ebc.y = 2;
        ISR_SEND_HI(msg);
        seq_no++;
    }
    //3. set base ebc
    {
        CMD_MSG_EBC_t *msg = (CMD_MSG_EBC_t *)GET_ISR_MSG(sizeof(CMD_MSG_EBC_t));
        msg->msg_hdr.id = ISR_BASE_EBC_SET;
        msg->ebc.x = 1;
        msg->ebc.y = 1;
        ISR_SEND_HI(msg);
        seq_no++;
    }
    //wait
    do
    {
        static int retry = 0;
        CMD_REQ_SEND_LITE(CMD_EBC_SEQ_GET);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(res), MSG_FLAG(res), MSG_RET(res));
        BASIC_ASSERT(MSG_ID(res) == CMD_EBC_SEQ_GET);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        //BASIC_ASSERT(MSG_RET(res) == 0);
        //printf("%d\n", res->ret);
        if (res->ret == seq_no) {
            RELEASE_CMD_RES_MSG(res);
            break;
        } else {
            RELEASE_CMD_RES_MSG(res);
            retry++;
            BASIC_ASSERT(retry<1000);
        }
    } while (1);
    //4. do compensate 1
    {
        FLOAT_T pupil_center_in_x = 8;
        FLOAT_T pupil_center_in_y = 8;
        FLOAT_T gold_out_x = 7;
        FLOAT_T gold_out_y = 7;
        CMD_DO_COMPENSATE_t *req = (CMD_DO_COMPENSATE_t *)GET_CMD_REQ_MSG(sizeof(CMD_DO_COMPENSATE_t));
        req->pupil_center.x = pupil_center_in_x;
        req->pupil_center.y = pupil_center_in_y;
        // req->pupil_center.ts = 1200;
        // req->pupil_center.confi = 1;
        req->msg_hdr.id = CMD_DO_COMPENSATE;
        CMD_REQ_SEND(req);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        // printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        CMD_DO_COMPENSATE_t *cpst_req = (CMD_DO_COMPENSATE_t *)res;
        BASIC_ASSERT(cpst_req->pupil_center.x == gold_out_x);
        BASIC_ASSERT(cpst_req->pupil_center.y == gold_out_y);
        BASIC_ASSERT(MSG_ID(res) == CMD_DO_COMPENSATE);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 0);
        RELEASE_CMD_RES_MSG(res);
    }
    //4. do compensate 2
    {
        FLOAT_T pupil_center_in_x = 15;
        FLOAT_T pupil_center_in_y = 15;
        FLOAT_T gold_out_x = 14;
        FLOAT_T gold_out_y = 14;
        CMD_DO_COMPENSATE_t *req = (CMD_DO_COMPENSATE_t *)GET_CMD_REQ_MSG(sizeof(CMD_DO_COMPENSATE_t));
        req->pupil_center.x = pupil_center_in_x;
        req->pupil_center.y = pupil_center_in_y;
        // req->pupil_center.ts = 1200;
        // req->pupil_center.confi = 1;
        req->msg_hdr.id = CMD_DO_COMPENSATE;
        CMD_REQ_SEND(req);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        // printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        CMD_DO_COMPENSATE_t *cpst_req = (CMD_DO_COMPENSATE_t *)res;
        BASIC_ASSERT(cpst_req->pupil_center.x == gold_out_x);
        BASIC_ASSERT(cpst_req->pupil_center.y == gold_out_y);
        BASIC_ASSERT(MSG_ID(res) == CMD_DO_COMPENSATE);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 0);
        RELEASE_CMD_RES_MSG(res);
    }
}

int test_state_tc()
{
    SIM_START_WITH_TOOL_LITE(tc_sys_init_uninit);
    SIM_START_WITH_TOOL_LITE(tc_cmd_resolution);
    SIM_START_WITH_TOOL_LITE(tc_cmd_pupil_center);
    SIM_START_WITH_TOOL_LITE(tc_cmd_state_read_write);
    SIM_START_WITH_TOOL_LITE(tc_cmd_do_predict);
    SIM_START_WITH_TOOL_LITE(tc_cmd_do_compensate);

    return 0;
}