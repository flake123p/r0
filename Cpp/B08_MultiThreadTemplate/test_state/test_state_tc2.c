#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include "pupil.h"
#include "sys_init.h"
#include "sys.h"
#include "state.h"
#include "pupil_tool.h"
#include "test_state.h"

//for tc_cmd_ref_img
//#include <opencv2/opencv.hpp>

void tc_cmd_fit_para(sys_hdl_t *sys_hdl)
{
    //set
    CMD_FIT_PARA_t *msg = (CMD_FIT_PARA_t *)GET_CMD_REQ_MSG(sizeof(CMD_FIT_PARA_t));
    msg->msg_hdr.id = CMD_FIT_PARA_SET;
    for(int i=0; i<2; i++) {
        for(int j=0; j<6; j++) {
            msg->fit_para.coef[i][j] = j;
        }
    }
    msg->fit_para.itrc[0] = 1;
    msg->fit_para.itrc[1] = 2;
    CMD_REQ_SEND((msg_t *)msg);
    msg = (CMD_FIT_PARA_t *)msg_wait_and_receive(sys_hdl->task_hdl);
    //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
    BASIC_ASSERT(MSG_ID(msg) == CMD_FIT_PARA_SET);
    BASIC_ASSERT(MSG_FLAG(msg) == FLAG_CMD_RES);
    BASIC_ASSERT(MSG_RET(msg) == 0);
    RELEASE_CMD_RES_MSG(msg);

    //get
    CMD_FIT_PARA_t *get_msg = (CMD_FIT_PARA_t *)GET_CMD_REQ_MSG(sizeof(CMD_FIT_PARA_t));
    get_msg->msg_hdr.id = CMD_FIT_PARA_GET;
    CMD_REQ_SEND((msg_t *)get_msg);
    get_msg = (CMD_FIT_PARA_t *)msg_wait_and_receive(sys_hdl->task_hdl);
    //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
    BASIC_ASSERT(MSG_ID(get_msg) == CMD_FIT_PARA_GET);
    BASIC_ASSERT(MSG_FLAG(get_msg) == FLAG_CMD_RES);
    BASIC_ASSERT(MSG_RET(get_msg) == 0);
    #if 1
    for(int i=0; i<2; i++) {
        for(int j=0; j<6; j++) {
            BASIC_ASSERT(get_msg->fit_para.coef[i][j] == j);
        }
    }
    BASIC_ASSERT(get_msg->fit_para.itrc[0] == 1);
    BASIC_ASSERT(get_msg->fit_para.itrc[1] == 2);
    #endif
    RELEASE_CMD_RES_MSG(get_msg);
}

void tc_cmd_calib_control(sys_hdl_t *sys_hdl)
{
    msg_t *res;

    // Error: abort before start
    CMD_REQ_SEND_LITE(CMD_CALIB_ABORT);
    res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
    //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
    BASIC_ASSERT(MSG_ID(res) == CMD_CALIB_ABORT);
    BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
    BASIC_ASSERT(MSG_RET(res) != 0);
    RELEASE_CMD_RES_MSG(res);

    // Error: end before start
    CMD_REQ_SEND_LITE(CMD_CALIB_END);
    res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
    //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
    BASIC_ASSERT(MSG_ID(res) == CMD_CALIB_END);
    BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
    BASIC_ASSERT(MSG_RET(res) != 0);
    RELEASE_CMD_RES_MSG(res);

    // Official start
    CMD_REQ_SEND_LITE(CMD_CALIB_START);
    res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
    //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
    BASIC_ASSERT(MSG_ID(res) == CMD_CALIB_START);
    BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
    BASIC_ASSERT(MSG_RET(res) == 0);
    RELEASE_CMD_RES_MSG(res);

    // Error: redundant start command
    CMD_REQ_SEND_LITE(CMD_CALIB_START);
    res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
    //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
    BASIC_ASSERT(MSG_ID(res) == CMD_CALIB_START);
    BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
    BASIC_ASSERT(MSG_RET(res) != 0);
    RELEASE_CMD_RES_MSG(res);

    // Official abort
    CMD_REQ_SEND_LITE(CMD_CALIB_ABORT);
    res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
    //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
    BASIC_ASSERT(MSG_ID(res) == CMD_CALIB_ABORT);
    BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
    BASIC_ASSERT(MSG_RET(res) == 0);
    RELEASE_CMD_RES_MSG(res);

    CMD_REQ_SEND_LITE(CMD_CALIB_START);
    res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
    //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
    BASIC_ASSERT(MSG_ID(res) == CMD_CALIB_START);
    BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
    BASIC_ASSERT(MSG_RET(res) == 0);
    RELEASE_CMD_RES_MSG(res);

    CMD_REQ_SEND_LITE(CMD_CALIB_END);
    res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
    //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
    BASIC_ASSERT(MSG_ID(res) == CMD_CALIB_END);
    BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
    BASIC_ASSERT(MSG_RET(res) == 0);
    RELEASE_CMD_RES_MSG(res);
}

void tc_cmd_accu_control(sys_hdl_t *sys_hdl)
{
    msg_t *res;

    // Error: abort before start
    CMD_REQ_SEND_LITE(CMD_ACCU_ABORT);
    res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
    //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
    BASIC_ASSERT(MSG_ID(res) == CMD_ACCU_ABORT);
    BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
    BASIC_ASSERT(MSG_RET(res) != 0);
    RELEASE_CMD_RES_MSG(res);

    // Error: end before start
    CMD_REQ_SEND_LITE(CMD_ACCU_END);
    res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
    //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
    BASIC_ASSERT(MSG_ID(res) == CMD_ACCU_END);
    BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
    BASIC_ASSERT(MSG_RET(res) != 0);
    RELEASE_CMD_RES_MSG(res);

    // Official start
    CMD_REQ_SEND_LITE(CMD_ACCU_START);
    res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
    //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
    BASIC_ASSERT(MSG_ID(res) == CMD_ACCU_START);
    BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
    BASIC_ASSERT(MSG_RET(res) == 0);
    RELEASE_CMD_RES_MSG(res);

    // Error: redundant start command
    CMD_REQ_SEND_LITE(CMD_ACCU_START);
    res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
    //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
    BASIC_ASSERT(MSG_ID(res) == CMD_ACCU_START);
    BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
    BASIC_ASSERT(MSG_RET(res) != 0);
    RELEASE_CMD_RES_MSG(res);

    // Official abort
    CMD_REQ_SEND_LITE(CMD_ACCU_ABORT);
    res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
    //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
    BASIC_ASSERT(MSG_ID(res) == CMD_ACCU_ABORT);
    BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
    BASIC_ASSERT(MSG_RET(res) == 0);
    RELEASE_CMD_RES_MSG(res);

    CMD_REQ_SEND_LITE(CMD_ACCU_START);
    res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
    //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
    BASIC_ASSERT(MSG_ID(res) == CMD_ACCU_START);
    BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
    BASIC_ASSERT(MSG_RET(res) == 0);
    RELEASE_CMD_RES_MSG(res);

    CMD_REQ_SEND_LITE(CMD_ACCU_END);
    res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
    //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
    BASIC_ASSERT(MSG_ID(res) == CMD_ACCU_END);
    BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
    BASIC_ASSERT(MSG_RET(res) == 0);
    RELEASE_CMD_RES_MSG(res);
}

void tc_cmd_ref_img(sys_hdl_t *sys_hdl)
{
    msg_t *res;
    //calibration start //necessary
    {
        CMD_REQ_SEND_LITE(CMD_CALIB_START);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_CALIB_START);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 0);
        RELEASE_CMD_RES_MSG(res);
    }
    //to hi
    {
        MSG_REF_IMAGE_t *ref_img = (MSG_REF_IMAGE_t *)GET_ISR_MSG(sizeof(MSG_REF_IMAGE_t));
        uint8_t *allocImg = (uint8_t *)MEM_ALLOC(4);
        ref_img->msg_hdr.id = ISR_MSG_REF_IMAGE_IN;
        ref_img->desc.start = allocImg;
        ISR_SEND_HI(ref_img);
    }
    do
    {
        static int retry = 0;
        CMD_REQ_SEND_LITE(CMD_CALIB_REF_CNT_GET);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(res), MSG_FLAG(res), MSG_RET(res));
        BASIC_ASSERT(MSG_ID(res) == CMD_CALIB_REF_CNT_GET);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        //BASIC_ASSERT(MSG_RET(res) == 0);
        //printf("%d\n", res->ret);
        if (res->ret == 1) {
            RELEASE_CMD_RES_MSG(res);
            break;
        } else {
            RELEASE_CMD_RES_MSG(res);
            retry++;
            BASIC_ASSERT(retry<1000);
        }
    } while (1);
    {
        CMD_REQ_SEND_LITE(CMD_CALIB_ABORT);
        res = (msg_t *)msg_wait_and_receive(sys_hdl->task_hdl);
        //printf("id=%d, flag=0x%08X, ret=%d\n", MSG_ID(msg), MSG_FLAG(msg), MSG_RET(msg));
        BASIC_ASSERT(MSG_ID(res) == CMD_CALIB_ABORT);
        BASIC_ASSERT(MSG_FLAG(res) == FLAG_CMD_RES);
        BASIC_ASSERT(MSG_RET(res) == 0);
        RELEASE_CMD_RES_MSG(res);
    }
}

int test_state_tc2()
{
    SIM_START_WITH_TOOL_LITE(tc_cmd_fit_para);
    SIM_START_WITH_TOOL_LITE(tc_cmd_calib_control);
    SIM_START_WITH_TOOL_LITE(tc_cmd_accu_control);
    SIM_START_WITH_TOOL_LITE(tc_cmd_ref_img);
    
    //sim_start_with_tool((task_cb_t)tc_cmd_resolution);

    return 0;
}