#ifndef __STATE_H_INCLUDED__
#define __STATE_H_INCLUDED__

#include <stdio.h>
#include <stdint.h>
#include "linked_list.h"
#include "pupil.h"
#include "sys.h"
#include "sys_init.h"

#define MSG_FLAG_MASK 0xFFFF0000
#define MSG_TYPE_MASK 0x0000F000
#define MSG_ID_MASK   0x0000FFFF

#define MSG_ID(msg) (((msg_t *)msg)->id & MSG_ID_MASK)
#define MSG_FLAG(msg) (((msg_t *)msg)->id & MSG_FLAG_MASK)
#define MSG_RET(msg) (((msg_t *)msg)->ret)
#define MSG_TYPE(msg) (((msg_t *)msg)->id & MSG_TYPE_MASK)

#define MSG_TYPE_CMD 0x1000
#define MSG_TYPE_EVT 0x2000
#define MSG_TYPE_TSK 0x3000
#define MSG_TYPE_ISR 0x4000
#define MSG_TYPE_HW  0x5000

typedef enum {
    CMD_START = MSG_TYPE_CMD,
    CMD_CALIB_START,
    CMD_CALIB_ABORT,
    CMD_CALIB_END,
    CMD_ACCU_START,
    CMD_ACCU_ABORT,
    CMD_ACCU_END,
    CMD_FIT_PARA_SET,
    CMD_FIT_PARA_GET,
    CMD_WORLD_INTRINSIC_SET,
    CMD_WORLD_INTRINSIC_GET,
    CMD_WORLD_RESOLUTION_SET,
    CMD_WORLD_RESOLUTION_GET,
    CMD_WORLD_FOCAL_LEN_SET,
    CMD_WORLD_FOCAL_LEN_GET,
    CMD_CALIB_REF_CNT_GET,
    CMD_CALIB_PUPIL_CNT_GET,
    CMD_CALIB_REF_LIST_DUMP,
    CMD_CALIB_PUPIL_LIST_DUMP,
    CMD_STATE_RESET,
    CMD_STATE_SET,
    CMD_STATE_GET,
    CMD_STATE_STORE,
    CMD_STATE_RESTORE,
    CMD_CALIB_EBC_SET, // lo will send to hi to set EBC
    CMD_BASE_EBC_SET, // lo will send to hi to set EBC
    CMD_CURR_EBC_SET, // lo will send to hi to set EBC
    CMD_CALIB_EBC_GET,
    CMD_CURR_EBC_GET,
    CMD_BASE_EBC_GET,
    CMD_EBC_SEQ_SET,
    CMD_EBC_SEQ_GET,
    CMD_PREDICT_FLAG_SET, // use msg->val to set the flag, 0 for off, 1 for on
    CMD_PREDICT_FLAG_GET, // use msg->ret to return the flag
    CMD_COMPENSATE_FLAG_SET, // use msg->val to set the flag, 0 for off, 1 for on
    CMD_COMPENSATE_FLAG_GET, // use msg->ret to return the flag
    CMD_REF_RETURN_FLAG_SET, // use msg->val to set the flag, 0 for off, 1 for on
    CMD_REF_RETURN_FLAG_GET, // use msg->ret to return the flag
    CMD_DO_GAZE_PREDICT,
    CMD_DO_COMPENSATE,
    CMD_ACCU_SET,
    CMD_ACCU_GET,

    EVT_START = MSG_TYPE_EVT,               // hi to external
    EVT_GAZE_POS,
    EVT_REF_POS,

    MSG_START = MSG_TYPE_TSK,
    MSG_REF_POINT_TO_CALIB,     // hi to lo
    MSG_PUPIL_CENTER_TO_CALIB,  // hi to lo
    MSG_TERMINATE_TASK,
    ISR_TIME_SET,
    MSG_EYE_BALL_CENTER_TO_CALIB,
    MSG_RESET_STATE,

    MSG_CALIB_EBC_SET, // lo to hi to set EBC
    MSG_BASE_EBC_SET, // lo to hi to set EBC
    MSG_CURR_EBC_SET, // lo to hi to set EBC

    //ISR
    ISR_START = MSG_TYPE_ISR,
    ISR_MSG_REF_IMAGE_IN,           // isr to hi
    ISR_MSG_PUPIL_CENTER_IN,        // isr to hi

    ISR_CALIB_EBC_SET, // isr to hi to set EBC
    ISR_BASE_EBC_SET, // isr to hi to set EBC
    ISR_CURR_EBC_SET, // isr to hi to set EBC

    SIM_ISR_REF_IMAGE_IN,       // sim to isr
    SIM_ISR_PUPIL_CENTER_IN,    // sim to isr

    HW_START = MSG_TYPE_HW,
    HW_MSG_REF_IMAGE_IN,           // isr to hi
    HW_MSG_PUPIL_CENTER_IN,        // isr to hi
    HW_MSG_EYE_BALL_CENTER_IN,     // to lo

    MSG_MAX, // msg_t is 16bits, TODO: assert(MSG_MAX<=0xFFFF)
} msg_id_t;

typedef enum {
    FLAG_CMD_RES = BIT_16,
    FLAG_CMD_RES_MD = BIT_17, // MD for more data
} msg_flag_t;

typedef struct {
    msg_t msg_hdr;
    fit_para_t fit_para;
} CMD_FIT_PARA_t;

typedef struct {
    msg_t msg_hdr;
    intrinsics_t intr;
} CMD_INTRINSIC_t;

typedef struct {
    msg_t msg_hdr;
    resolution_t reso;
} CMD_RESOLUTION_t;

typedef struct {
    msg_t msg_hdr;
    focal_len_t flen;
} CMD_FOCAL_LEN_t;

typedef struct {
    msg_t msg_hdr;
    uint32_t do_compensate;
    union {
        pupil_center_t pupil_center; //in
        gaze_pos_t gaze_pos; //out
    };
} CMD_DO_GAZE_PREDICT_t;

typedef struct {
    msg_t msg_hdr;
    pupil_center_t pupil_center; //in, out
} CMD_DO_COMPENSATE_t;

typedef struct {
    msg_t msg_hdr;
    int is_accu_null;
    FLOAT_T accu;
} CMD_ACCU_t;

typedef struct {
    msg_t msg_hdr;
    eye_ball_center_t ebc;
} CMD_MSG_EBC_t; // struct for both CMD & Task MSG

typedef struct {
    msg_t msg_hdr;
    gaze_pos_t gaze_pos;
} EVT_GAZE_POS_t;

typedef struct {
    msg_t msg_hdr;
    ref_node_t ref_point;
} EVT_REF_POINT_t;

typedef struct {
    msg_t msg_hdr;
    union {
        ref_img_desc_t desc;
        ref_node_t ref_node;
    };
} MSG_REF_IMAGE_t;

typedef struct {
    msg_t msg_hdr;
    union {
        pupil_center_t pupil_center;
        pupil_node_t pupil_node;
    };
} MSG_PUPIL_CENTER_t;

typedef struct {
    msg_t msg_hdr;
    eye_ball_center_t ebc;
} MSG_EYE_BALL_CENTER_t;

typedef struct {
    msg_t msg_hdr;
    TIMESTAMP_T time;
} MSG_TIME_t;

typedef enum {
    STATE_DEFAULT = 0,
//    STATE_PREDICTING,
    STATE_CALIB,
    STATE_ACCU,

    STATE_MAX, // for assert
} state_t;

extern ref_head_t g_rhead; //ref point
extern pupil_head_t g_phead; //pupil center point
extern uint32_t g_ref_cnt;
extern uint32_t g_pupil_cnt;
int state_cmd_handler(msg_t *msg);

void state_reset();
int state_set(state_t new_state);
state_t state_get();
void state_store();
void state_restore();

void calib_start();
void calib_ref_in(ref_node_t *ref);
void calib_pupil_in(pupil_node_t *pupil);
void calib_abort();
void calib_end();
void accu_start();
void accu_ref_in(ref_node_t *ref);
void accu_pupil_in(pupil_node_t *pupil);
void accu_abort();
void accu_end();

int state_hi(msg_t *msg);
int state_lo(msg_t *msg);

int cmd_req_send_lite(uint32_t msg_id, uint32_t val);
int cmd_res_send_lite(msg_t *req, uint32_t ret);

#define ISR_SEND_HI(msg) BASIC_ASSERT(MSG_TYPE(msg)==MSG_TYPE_ISR);msg_send(TASK_HI_HDL, (msg_t *)msg)
#define MSG_SEND_HI(msg) BASIC_ASSERT(MSG_TYPE(msg)==MSG_TYPE_TSK);msg_send(TASK_HI_HDL, (msg_t *)msg)
#define MSG_SEND_LO(msg) BASIC_ASSERT(MSG_TYPE(msg)==MSG_TYPE_TSK);msg_send(TASK_LO_HDL, (msg_t *)msg)
#define MSG_RECV(hdl) msg_receive(hdl)
#define MSG_WAIT(hdl) msg_wait(hdl)
#define MSG_WAIT_N_RECV(hdl) msg_wait_and_receive(hdl)
#define CMD_REQ_SEND(msg) BASIC_ASSERT(MSG_TYPE(msg)==MSG_TYPE_CMD);cmd_req_send((msg_t *)msg)
#define CMD_RES_SEND(msg) BASIC_ASSERT(MSG_TYPE(msg)==MSG_TYPE_CMD);cmd_res_send((msg_t *)msg)
#define CMD_REQ_SEND_LITE(msg_id) BASIC_ASSERT((msg_id&MSG_TYPE_MASK)==MSG_TYPE_CMD);cmd_req_send_lite(msg_id,0)
#define CMD_REQ_SEND_LITE2(msg_id,val) BASIC_ASSERT((msg_id&MSG_TYPE_MASK)==MSG_TYPE_CMD);cmd_req_send_lite(msg_id,(uint32_t)val)
#define CMD_RES_SEND_LITE(req,ret) BASIC_ASSERT(MSG_TYPE(req)==MSG_TYPE_CMD);cmd_res_send_lite((msg_t *)req,ret)
#define EVT_SEND(msg) BASIC_ASSERT(MSG_TYPE(msg)==MSG_TYPE_EVT);event_send((msg_t *)msg)

#if ( 0 )
#define MEM_ALLOC mem_alloc
#define MEM_FREE mem_free
#define GET_IMG(size) get_image(size)
#define RELEASE_IMG(p) release_image((uint8_t *)p)
#define GET_ISR_MSG(size) get_isr_msg(size)
#define RELEASE_ISR_MSG(p) release_isr_msg(p)
#define GET_TSK_MSG(size) get_task_msg(size)
#define RELEASE_TSK_MSG(p) release_tsk_msg(p)
#define GET_CMD_REQ_MSG(size) get_cmd_req_msg(size)
#define RELEASE_CMD_REQ_MSG(p) release_cmd_req_msg(p)
#define GET_CMD_RES_MSG(size) get_cmd_res_msg(size)
#define RELEASE_CMD_RES_MSG(p) release_cmd_res_msg(p)
#define GET_EVT_MSG(size) get_evt_msg(size)
#define RELEASE_EVT_MSG(p) release_evt_msg(p)
#else
#include "libmem.h"
#define MEM_ALLOC MM_ALLOC
#define MEM_FREE MM_FREE
#define GET_IMG(size) (uint8_t *)MM_ALLOC(size);BASIC_ASSERT((size)!=0)
#define RELEASE_IMG(p) MM_FREE(p)
#define GET_ISR_MSG(size) MM_ALLOC(size)
#define RELEASE_ISR_MSG(p) MM_FREE(p)
#define GET_TSK_MSG(size) MM_ALLOC(size)
#define RELEASE_TSK_MSG(p) MM_FREE(p)
#define GET_CMD_REQ_MSG(size) MM_ALLOC(size)
#define RELEASE_CMD_REQ_MSG(p) MM_FREE(p)
#define GET_CMD_RES_MSG(size) MM_ALLOC(size)
#define RELEASE_CMD_RES_MSG(p) MM_FREE(p)
#define GET_EVT_MSG(size) MM_ALLOC(size)
#define RELEASE_EVT_MSG(p) MM_FREE(p)
#endif

#endif//__STATE_H_INCLUDED__