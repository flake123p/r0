#ifndef __PUPIL_H_INCLUDED__
#define __PUPIL_H_INCLUDED__

#include <stdio.h>
#include <stdint.h>
#include "linked_list.h"
#include "macros.h"

#define FLOAT_T           double
#define TIMESTAMP_T       uint32_t
#define CONFI_T           uint32_t //0 or 1

#define TIMESTAMP_MULTIPLIER    (1000)  //the timestamp is micro second in json file.
#define CONFIDENCE_MULTIPLIER   (1)

typedef struct {
    FLOAT_T coef[2][6];
    FLOAT_T itrc[2];
} fit_para_t;

typedef struct {
    FLOAT_T x;
    FLOAT_T y;
    CONFI_T confi; //confidence
    TIMESTAMP_T ts; //timestamp
} gaze_pos_t;

typedef struct {
    FLOAT_T K[9];
    FLOAT_T D[6];
} intrinsics_t;

typedef struct {
    uint32_t w;
    uint32_t h;
} resolution_t;

typedef struct {
    uint32_t len;
} focal_len_t;

typedef struct {
    uint8_t *start;
    TIMESTAMP_T ts; //timestamp
} ref_img_desc_t;

typedef struct {
    FLOAT_T x;
    FLOAT_T y;
    CONFI_T confi; //confidence
    TIMESTAMP_T ts; //timestamp
} pupil_center_t;

typedef struct {
    FLOAT_T x;
    FLOAT_T y;
} eye_ball_center_t;

typedef struct {
    LList_Head_t hd;
} ref_head_t;

typedef struct {
    LList_Head_t hd;
} pupil_head_t;

typedef struct {
    LList_Entry_t en;
    FLOAT_T x;
    FLOAT_T y;
    CONFI_T confi; //confidence
    TIMESTAMP_T ts; //timestamp
} ref_node_t;

typedef struct {
    LList_Entry_t en;
    FLOAT_T x;
    FLOAT_T y;
    CONFI_T confi; //confidence
    TIMESTAMP_T ts; //timestamp
} pupil_node_t;





extern fit_para_t g_fit_para;
extern intrinsics_t g_world_intr;
extern resolution_t g_world_reso;
extern focal_len_t g_world_focal_len;

extern eye_ball_center_t g_base_ebc; // Note!! this variable is set by task_hi
extern eye_ball_center_t g_curr_ebc; // Note!! this variable is set by task_hi
extern eye_ball_center_t g_calib_ebc; // Note!! this variable is set by task_hi
extern uint32_t g_ebc_seqno;

extern int g_accu_is_null;
extern FLOAT_T g_accu_curr;

extern int g_enable_predict;
extern int g_enable_compensate;
extern int g_enable_ref_pos_return;

int do_compensate(pupil_center_t *pupil_center);

#endif//__PUPIL_H_INCLUDED__