#ifndef _SYS_INIT_H_INCLUDED_
#define _SYS_INIT_H_INCLUDED_

#include "sys.h"

//for task_hi/lo
int OLD_sys_init(void);
int OLD_sys_start(void);
int OLD_sys_wait(void);

void *task_generic_function(void *arg);

//for other tasks
int OLD_sys_ext_init(task_t task, msg_thread_t *taskHdl);
int OLD_sys_ext_start(msg_thread_t *taskHdl, function_t func);
int OLD_sys_ext_wait(msg_thread_t *taskHdl);

void OLD_set_exit_ret(task_t task, int ret);

typedef void *(*task_cb_t)(void *); //new
typedef int (*sm_cb_t)(msg_t *msg); //new

typedef struct {
    msg_task_handle_t task_hdl;
    task_cb_t task_cb;
    sm_cb_t sm_cb;
    int sm_ret; //deprecate in the future?
} sys_hdl_t;

typedef void (*task_cb_lite_t)(sys_hdl_t *); //new

int sys_init2(void);
int sys_start2(void);
int sys_wait2(void);
int sys_uninit2(void);
void *task_generic(void *hdl);

// Its better to use define
extern sys_hdl_t g_sys_hi;
extern sys_hdl_t g_sys_lo;
#define SYS_HI_HDL (&g_sys_hi)
#define SYS_LO_HDL (&g_sys_lo)
#define TASK_HI_HDL (g_sys_hi.task_hdl)
#define TASK_LO_HDL (g_sys_lo.task_hdl)
#define TASK_HI_CB (g_sys_hi.task_cb)
#define TASK_LO_CB (g_sys_lo.task_cb)
#define TASK_HI_SM (g_sys_hi.sm_cb)
#define TASK_LO_SM (g_sys_lo.sm_cb)
#define TASK_HI_RET (g_sys_hi.sm_ret)
#define TASK_LO_RET (g_sys_lo.sm_ret)

#endif//_SYS_INIT_H_INCLUDED_
