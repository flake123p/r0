#include "sys_init.h"
#include "sys.h"
#include "state.h"

#define LOCAL_DEBUG 0
#if LOCAL_DEBUG
    #define dbg_print printf
#else
    #define dbg_print
#endif

msg_thread_t msg_task_hi, msg_task_lo;

void *task_generic_function(void *arg)
{
    dbg_print("[%s]\n", __func__);
    msg_t *msg;
    task_para_handle_t *para = (task_para_handle_t *)arg;

    while(1)
    {
        wait_message(para->id);
        while (1) {
            msg = OLD_receive_message(para->id);
            if (msg) {
                msg_t *get_msg = (msg_t *)msg;
                dbg_print("[%s] recieve: 0x%x(%d) [%d]\n", __func__, get_msg->id, get_msg->id, SYS_SEQ_NO_GET(get_msg));
                if(MSG_ID(msg) == MSG_TERMINATE_TASK) {
                    dbg_print("[%s] get MSG_TERMINATE_TASK, exit!!\n", __func__);
                    task_exit(NULL);
                }
                if(para->cb_func != NULL)  para->cb_func(msg);
            } else {
                break;
            }
        }
    }
}

int OLD_sys_init(void)
{
    int ret = 0;
    //TODO
    //test case should be separated.
    mem_init();
    state_reset();

    LLIST_HEAD_RESET(&msg_task_hi.q_head);
    LLIST_HEAD_RESET(&msg_task_lo.q_head);

    task_new_handle(TASK_HI, &msg_task_hi.thre);
    task_new_handle(TASK_LO, &msg_task_lo.thre);

    ret = LibIPC_Event_Create(&msg_task_hi.q_evt);
    if(ret) return ret;
    ret = LibIPC_Event_Create(&msg_task_lo.q_evt);
    if(ret) return ret;

    ret = LibIPC_Mutex_Create(&msg_task_hi.q_mutex);
    if(ret) return ret;
    ret = LibIPC_Mutex_Create(&msg_task_lo.q_mutex);
    if(ret) return ret;

    msg_thread_handle(TASK_HI, &msg_task_hi);
    msg_thread_handle(TASK_LO, &msg_task_lo);

    return ret;
}

int OLD_sys_start(void)
{
    int ret = 0;
    //init_tasks();
    task_create(msg_task_hi.thre, task_generic_function);
    task_create(msg_task_lo.thre, task_generic_function);
    return ret;
}

int OLD_sys_wait(void)
{
    int ret = 0;
    task_para_handle_t *thread_hi = (task_para_handle_t *)msg_task_hi.thre;
    task_para_handle_t *thread_lo = (task_para_handle_t *)msg_task_lo.thre;
    task_wait(msg_task_hi.thre, NULL);
    task_wait(msg_task_lo.thre, NULL);
    return ret;
}


int OLD_sys_ext_init(task_t task, msg_thread_t *taskHdl)
{
    int ret = 0;
    int retVal;
    LLIST_HEAD_RESET(&taskHdl->q_head);
    task_new_handle(task, &taskHdl->thre);
    retVal = LibIPC_Event_Create(&taskHdl->q_evt);
    retVal = LibIPC_Mutex_Create(&taskHdl->q_mutex);
    msg_thread_handle(task, taskHdl);
    return ret;
}

int OLD_sys_ext_start(msg_thread_t *taskHdl, function_t func)
{
    int ret = 0;
    task_create(taskHdl->thre, func);
    return ret;
}

int OLD_sys_ext_wait(msg_thread_t *taskHdl)
{
    int ret = 0;
    task_wait(taskHdl->thre, NULL);
    return ret;
}

void OLD_set_exit_ret(task_t task, int ret)
{
    //msg_thread_t *hdl = get_msg_thread_handle(task);
    //hdl->exit_ret = ret;
    task_para_handle_t *p_task_handle = (task_para_handle_t *)get_msg_thread_handle(task)->thre;
    p_task_handle->exit_ret = ret;
}

void *task_generic(void *hdl)
{
    dbg_print("[%s]\n", __func__);
    sys_hdl_t *sys = (sys_hdl_t *)hdl;
    msg_t *msg;
    int ret;

    while(1)
    {
        msg_wait(sys->task_hdl);
        while (1) {
            msg = msg_receive(sys->task_hdl);
            if (msg) {
                dbg_print("[%s] recieve: 0x%x(%d) [%d]\n", __func__, msg->id, msg->id, SYS_SEQ_NO_GET(msg));
                BASIC_ASSERT(sys->sm_cb != NULL);
                sys->sm_ret = sys->sm_cb(msg);
                if (sys->sm_ret) {
                    return NULL;
                }
            } else {
                break;
            }
        }
    }
    return NULL;
}

sys_hdl_t g_sys_hi;
sys_hdl_t g_sys_lo;
#define PRI_HI 0
#define PRI_LO 1

int sys_init2(void)
{
    int ret = 0;
    //TODO
    //test case should be separated.
    mem_init();
    state_reset();

    TASK_HI_CB = task_generic;
    TASK_HI_SM = state_hi;

    TASK_LO_CB = task_generic;
    TASK_LO_SM = state_lo;

    msg_task_new_handle(&TASK_HI_HDL, PRI_HI);
    msg_task_new_handle(&TASK_LO_HDL, PRI_LO);

    BASIC_ASSERT(TASK_HI_HDL != NULL);
    BASIC_ASSERT(TASK_LO_HDL != NULL);

    cmd_internal_interface_config(TASK_HI_HDL, TASK_LO_HDL);

    return ret;
}

int sys_start2(void)
{
    BASIC_ASSERT(TASK_HI_HDL != NULL);
    BASIC_ASSERT(TASK_LO_HDL != NULL);
    msg_task_create(TASK_HI_HDL, TASK_HI_CB, SYS_HI_HDL);
    msg_task_create(TASK_LO_HDL, TASK_LO_CB, SYS_LO_HDL);
    return 0;
}

int sys_wait2(void)
{
    msg_task_wait(TASK_HI_HDL);
    msg_task_wait(TASK_LO_HDL);
    return 0;
}
int sys_uninit2(void)
{
    msg_task_destory_handle(TASK_HI_HDL);
    msg_task_destory_handle(TASK_LO_HDL);

    mem_uninit();

    BASIC_ASSERT(LibThread_GetCounter() == 0);
    BASIC_ASSERT(LibIPC_Event_GetCounter() == 0);
    BASIC_ASSERT(LibIPC_Mutex_GetCounter() == 0);
    return 0;
}
