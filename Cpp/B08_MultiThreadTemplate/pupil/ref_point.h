#ifndef _REF_POINT_H_INCLUDED_
#define _REF_POINT_H_INCLUDED_

#include "pupil.h"

int ref_point_calculate(ref_node_t *out, uint8_t *image);

#endif//_REF_POINT_H_INCLUDED_