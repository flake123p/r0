
#include "util.h"
#include "sys.h"
#include "state.h"

int ref_list_init(ref_head_t *head)
{
    LLIST_HEAD_RESET(head);
    return 0;
}

int ref_list_free(ref_head_t *head)
{
    do {
        LList_Entry_t *prev;
        LList_Entry_t *curr;
        LLIST_WHILE_START(head, curr, LList_Entry_t) {
            prev = curr;
            LLIST_WHILE_NEXT(curr, LList_Entry_t);
            MEM_FREE(prev);
        }
        LLIST_HEAD_RESET(head);
    } while(0);

    return 0;
}

int ref_list_append(ref_head_t *head, ref_node_t *node)
{
    LLIST_INSERT_LAST(head, node);
    return 0;
}

TIMESTAMP_T g_curr_time;
TIMESTAMP_T get_curr_time()
{
    // dummy function now
    return g_curr_time;
}

void set_curr_time(TIMESTAMP_T time)
{
    g_curr_time = time;
}

int pupil_list_init(pupil_head_t *head)
{
    LLIST_HEAD_RESET(head);
    return 0;
}

int pupil_list_free(pupil_head_t *head)
{
    do {
        LList_Entry_t *prev;
        LList_Entry_t *curr;
        LLIST_WHILE_START(head, curr, LList_Entry_t) {
            prev = curr;
            LLIST_WHILE_NEXT(curr, LList_Entry_t);
            MEM_FREE(prev);
        }
        LLIST_HEAD_RESET(head);
    } while(0);

    return 0;
}

int pupil_list_append(pupil_head_t *head, pupil_node_t *node)
{
    LLIST_INSERT_LAST(head, node);
    return 0;
}
