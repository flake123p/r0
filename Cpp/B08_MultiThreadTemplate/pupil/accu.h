#ifndef _ACCU_H_INCLUDED_
#define _ACCU_H_INCLUDED_

#include "pupil.h"

int calc_accu(FLOAT_T *out_accu, fit_para_t *para, ref_head_t *rhead, pupil_head_t *phead);

#endif//_ACCU_H_INCLUDED_