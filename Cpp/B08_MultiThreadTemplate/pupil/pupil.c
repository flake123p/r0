
#include "pupil.h"

fit_para_t g_fit_para;
intrinsics_t g_world_intr;
resolution_t g_world_reso;
focal_len_t g_world_focal_len;

eye_ball_center_t g_base_ebc; // Note!! this variable is set by task_hi
eye_ball_center_t g_curr_ebc; // Note!! this variable is set by task_hi
eye_ball_center_t g_calib_ebc; // Note!! this variable is set by task_hi
uint32_t g_ebc_seqno;

int g_accu_is_null = 1;
FLOAT_T g_accu_curr;

int g_enable_predict;
int g_enable_compensate;
int g_enable_ref_pos_return;

FLOAT_T minus_cap(FLOAT_T val, FLOAT_T diff)
{
    val -= diff;
    if (val < 0)
        val = 0;
    if (val > 1.0)
        val = 1.0;
    return val;
}

int do_compensate(pupil_center_t *pupil_center)
{
    FLOAT_T x_diff = g_curr_ebc.x - g_base_ebc.x;
    FLOAT_T y_diff = g_curr_ebc.y - g_base_ebc.y;

    pupil_center->x -= x_diff;
    pupil_center->y -= y_diff;

    printf("do_compensate\n");
    return 0;
}