#include "pupil.h"
#include "util.h"
#include "state.h"
#include "sys.h"
#include "fit.h"
#include "ref_point.h"
#include "accu.h"

extern state_t g_state;

ref_head_t g_rhead; //ref point
pupil_head_t g_phead; //pupil center point
uint32_t g_ref_cnt;
uint32_t g_pupil_cnt;

static void clear_ref_pupil()
{
    pupil_list_free(&g_phead);
    ref_list_free(&g_rhead);
    g_ref_cnt = 0;
    g_pupil_cnt = 0;
}

void calib_start()
{
    ref_list_init(&g_rhead);
    pupil_list_init(&g_phead);
    g_ref_cnt = 0;
    g_pupil_cnt = 0;
}
void calib_ref_in(ref_node_t *ref)
{
    ref_list_append(&g_rhead, ref);
    g_ref_cnt++;
}
void calib_pupil_in(pupil_node_t *pupil)
{
    pupil_list_append(&g_phead, pupil);
    g_pupil_cnt++;
}
void calib_abort()
{
    clear_ref_pupil();
}
void calib_end()
{
    //g_calib_ebc = g_curr_ebc; //simple compensation algorithm now
    //g_base_ebc = g_curr_ebc; //simple compensation algorithm now

    fit_ref_and_pupil(&g_fit_para, &g_rhead, &g_phead);
    g_accu_is_null = calc_accu(&g_accu_curr, &g_fit_para, &g_rhead, &g_phead);
    clear_ref_pupil();
}
void accu_start()
{
    ref_list_init(&g_rhead);
    pupil_list_init(&g_phead);
    g_ref_cnt = 0;
    g_pupil_cnt = 0;
}
void accu_ref_in(ref_node_t *ref)
{
    ref_list_append(&g_rhead, ref);
    g_ref_cnt++;
}
void accu_pupil_in(pupil_node_t *pupil)
{
    pupil_list_append(&g_phead, pupil);
    g_pupil_cnt++;
}
void accu_abort()
{
    clear_ref_pupil();
}
void accu_end()
{
    g_accu_is_null = calc_accu(&g_accu_curr, &g_fit_para, &g_rhead, &g_phead);
    clear_ref_pupil();
}

static int state_hi_set_ebc(msg_t *msg)
{
    g_ebc_seqno++;
    switch(msg->id)
    {
        case ISR_CALIB_EBC_SET:
        case MSG_CALIB_EBC_SET: {
            CMD_MSG_EBC_t *ebc_msg = (CMD_MSG_EBC_t *)msg;
            g_calib_ebc = ebc_msg->ebc;
        } return 0;

        case ISR_BASE_EBC_SET:
        case MSG_BASE_EBC_SET: {
            CMD_MSG_EBC_t *ebc_msg = (CMD_MSG_EBC_t *)msg;
            g_base_ebc = ebc_msg->ebc;
        } return 0;

        case ISR_CURR_EBC_SET:
        case MSG_CURR_EBC_SET: {
            CMD_MSG_EBC_t *ebc_msg = (CMD_MSG_EBC_t *)msg;
            g_curr_ebc = ebc_msg->ebc;
        } return 0;

        default:
            BASIC_ASSERT(0);
            break;
    }
    return 0;
}

int state_hi_isr_handler(msg_t *msg)
{
    switch(msg->id)
    {
        case ISR_MSG_REF_IMAGE_IN:           // to hi
            switch(g_state)
            {
                case STATE_CALIB:
                case STATE_ACCU:
                    /*
                        ref point calculate
                        free image buffer
                        to task_lo: MSG_REF_POINT_TO_CALIB
                    */
                    MSG_REF_IMAGE_t *get_node = (MSG_REF_IMAGE_t *)GET_TSK_MSG(sizeof(MSG_REF_IMAGE_t));
                    MSG_REF_IMAGE_t *ref_img = (MSG_REF_IMAGE_t *)msg;
                    ref_point_calculate(&get_node->ref_node, ref_img->desc.start);
                    get_node->ref_node.ts = ref_img->desc.ts;
                    RELEASE_IMG(ref_img->desc.start);
                    //get_curr_time();
                    get_node->msg_hdr.id = MSG_REF_POINT_TO_CALIB;
                    
                    if (g_enable_ref_pos_return) {
                        EVT_REF_POINT_t *ref_evt = (EVT_REF_POINT_t *)GET_EVT_MSG(sizeof(EVT_REF_POINT_t));
                        ref_evt->msg_hdr.id = EVT_REF_POS;
                        ref_evt->ref_point = get_node->ref_node;
                        EVT_SEND(ref_evt);
                    }

                    MSG_SEND_LO(get_node);
                    break;
            }
            return 0;

        case ISR_MSG_PUPIL_CENTER_IN:        // to hi
            switch(g_state)
            {
                case STATE_DEFAULT:
                    /*
                        adjust
                        read ts
                        call predict
                    */
                    //send_event(msg);
                    if (g_enable_predict) {
                        MSG_PUPIL_CENTER_t *pupil_ori = (MSG_PUPIL_CENTER_t *)msg;
                        EVT_GAZE_POS_t *evt = (EVT_GAZE_POS_t *)GET_EVT_MSG(sizeof(EVT_GAZE_POS_t));

                        if (g_enable_compensate) {
                            do_compensate(&pupil_ori->pupil_center);
                        }

                        predict_gaze_pos(&evt->gaze_pos, &g_fit_para, &pupil_ori->pupil_center);
                        evt->msg_hdr.id = EVT_GAZE_POS;
                        evt->gaze_pos.confi = pupil_ori->pupil_center.confi;
                        evt->gaze_pos.ts = pupil_ori->pupil_center.ts;
                        EVT_SEND(evt);
                    }
                    break;
                case STATE_CALIB:
                case STATE_ACCU: {
                    /*
                        adjust
                        read ts
                        no predict
                        to task_lo: MSG_PUPIL_CENTER_TO_CALIB
                    */
                    MSG_PUPIL_CENTER_t *pupil_ori = (MSG_PUPIL_CENTER_t *)msg;
                    MSG_PUPIL_CENTER_t *pupil_msg = (MSG_PUPIL_CENTER_t *)GET_TSK_MSG(sizeof(MSG_PUPIL_CENTER_t));
                    pupil_msg->msg_hdr.id = MSG_PUPIL_CENTER_TO_CALIB;
                    pupil_msg->pupil_center = pupil_ori->pupil_center;
                    MSG_SEND_LO(pupil_msg);
                } break;
            }
            break;

        case ISR_CALIB_EBC_SET:
        case ISR_BASE_EBC_SET:
        case ISR_CURR_EBC_SET: {
            state_hi_set_ebc(msg);
        } return 0;

        case ISR_TIME_SET:
        {
            MSG_TIME_t *time_msg = (MSG_TIME_t *)msg;
            set_curr_time(time_msg->time);
            break;
        }

        default:
            //ASSERT
            break;
    }
    return 0;
}

int state_hi_msg_handler(msg_t *msg)
{
    switch(msg->id)
    {
        case MSG_CALIB_EBC_SET:
        case MSG_BASE_EBC_SET:
        case MSG_CURR_EBC_SET: {
            state_hi_set_ebc(msg);
        } return 0;

        case MSG_TERMINATE_TASK:
            return 1; //end of simulation

        default:
            //ASSERT
            break;
    }
    return 0;
}

int state_hi(msg_t *msg)
{
    int ret;
    switch (MSG_TYPE(msg))
    {
        case MSG_TYPE_CMD:
            BASIC_ASSERT(0);
            return 0;

        case MSG_TYPE_TSK:
            ret = state_hi_msg_handler(msg);
            RELEASE_TSK_MSG(msg);
            return ret;

        case MSG_TYPE_ISR:
            ret = state_hi_isr_handler(msg);
            RELEASE_ISR_MSG(msg);
            return ret;
    }
    BASIC_ASSERT(0);
    return 0;
}

int state_lo_msg_handler(msg_t *msg)
{
    switch(msg->id)
    {
        case ISR_MSG_REF_IMAGE_IN:           // to hi
        case ISR_MSG_PUPIL_CENTER_IN:        // to hi
            BASIC_ASSERT(0);
            break;
        case MSG_REF_POINT_TO_CALIB:     // hi to lo
        {
            MSG_REF_IMAGE_t *ref_node = (MSG_REF_IMAGE_t *)msg;
            ref_node_t *node = (ref_node_t *)MEM_ALLOC(sizeof(ref_node_t));
            *node = ref_node->ref_node;
            //ref_list_append(&g_rhead, node);
            calib_ref_in(node);
            break;
        }
        case MSG_PUPIL_CENTER_TO_CALIB:  // hi to lo
        {
            MSG_PUPIL_CENTER_t *pupil_msg = (MSG_PUPIL_CENTER_t *)msg;
            pupil_node_t *node = (pupil_node_t *)MEM_ALLOC(sizeof(pupil_node_t));
            node->x = pupil_msg->pupil_center.x;
            node->y = pupil_msg->pupil_center.y;
            node->ts = pupil_msg->pupil_center.ts;
            node->confi = pupil_msg->pupil_center.confi;
            //pupil_list_append(&g_phead, node);
            calib_pupil_in(node);
            break;
        }

        case MSG_TERMINATE_TASK:
            return 1; //end of simulation
    }
    return 0;
}

int state_lo(msg_t *msg)
{
    int ret;
    switch (MSG_TYPE(msg))
    {
        case MSG_TYPE_CMD:
            ret = state_cmd_handler(msg);
            RELEASE_CMD_REQ_MSG(msg);
            return ret;

        case MSG_TYPE_TSK:
            ret = state_lo_msg_handler(msg);
            RELEASE_TSK_MSG(msg);
            return ret;
    }
    return 0;
}

int cmd_req_send_lite(uint32_t msg_id, uint32_t val)
{
    msg_t *msg = (msg_t *)GET_CMD_REQ_MSG(sizeof(msg_t));
    msg->id = msg_id;
    msg->val = val;
    return cmd_req_send((msg_t *)msg);
}

int cmd_res_send_lite(msg_t *req, uint32_t ret)
{
    msg_t *res = (msg_t *)GET_CMD_RES_MSG(sizeof(msg_t));
    res->id = req->id | FLAG_CMD_RES;
    res->ret = ret;
    return cmd_res_send(res);
}