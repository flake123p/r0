#include "pupil.h"
#include "util.h"
#include "state.h"
#include "sys.h"
#include "fit.h"

// write in task_lo
state_t g_state = STATE_DEFAULT;
state_t g_state_store = STATE_DEFAULT;

void state_reset()
{
    g_state = STATE_DEFAULT;
}

int state_set(state_t new_state)
{
    BASIC_ASSERT(new_state < STATE_MAX);
    g_state = new_state;

    return 0; // for success
}

state_t state_get()
{
    BASIC_ASSERT(g_state < STATE_MAX);
    return g_state;
}

void state_store()
{
    BASIC_ASSERT(g_state < STATE_MAX);
    g_state_store = g_state;
}

void state_restore()
{
    BASIC_ASSERT(g_state_store < STATE_MAX);
    g_state = g_state_store;
}

static int OLD_send_command_res_lite(msg_t *msg, uint32_t ret)
{
    msg->id = msg->id | FLAG_CMD_RES;
    msg->ret = ret;
    return OLD_send_command_res(msg);
}

// return zero for handled
// return non-zero for not handled
int state_cmd_handler(msg_t *req)
{
    switch(g_state)
    {
        case STATE_DEFAULT:
            switch(req->id)
            {
                case CMD_CALIB_START:
                    calib_start();
                    state_store();
                    state_set(STATE_CALIB);
                    CMD_RES_SEND_LITE(req, 0);
                    return 0;
                case CMD_CALIB_ABORT:
                case CMD_CALIB_END:
                    CMD_RES_SEND_LITE(req, 1); // command denied
                    return 0;
                case CMD_ACCU_START:
                    accu_start();
                    state_store();
                    state_set(STATE_ACCU);
                    CMD_RES_SEND_LITE(req, 0);
                    return 0;
                case CMD_ACCU_ABORT:
                case CMD_ACCU_END:
                    CMD_RES_SEND_LITE(req, 1); // command denied
                    return 0;
            }
            break;
        
        case STATE_CALIB:
            switch(req->id)
            {
                case CMD_CALIB_START:
                    CMD_RES_SEND_LITE(req, 1); // command denied
                    return 0;
                case CMD_CALIB_ABORT:
                    calib_abort();
                    state_restore();
                    CMD_RES_SEND_LITE(req, 0);
                    return 0;
                case CMD_CALIB_END:
                    calib_end();
                    state_restore();
                    CMD_RES_SEND_LITE(req, 0);
                    return 0;
                case CMD_ACCU_START:
                case CMD_ACCU_ABORT:
                case CMD_ACCU_END:
                    CMD_RES_SEND_LITE(req, 1); // command denied
                    return 0;
            }
            break;
        case STATE_ACCU:
            switch(req->id)
            {
                case CMD_CALIB_START:
                case CMD_CALIB_ABORT:
                case CMD_CALIB_END:
                case CMD_ACCU_START:
                    CMD_RES_SEND_LITE(req, 1); // command denied
                    return 0;
                case CMD_ACCU_ABORT:
                    state_restore();
                    accu_abort();
                    CMD_RES_SEND_LITE(req, 0);
                    return 0;
                case CMD_ACCU_END:
                    state_restore();
                    accu_end();
                    CMD_RES_SEND_LITE(req, 0);
                    return 0;
            }
            break;
    }

    //simple command
    switch(req->id)
    {
        case CMD_FIT_PARA_SET:
        {
            CMD_FIT_PARA_t *fit_req = (CMD_FIT_PARA_t *)req;
            msg_t *res = (msg_t *)GET_CMD_RES_MSG(sizeof(msg_t));
            g_fit_para = fit_req->fit_para;
            res->id = fit_req->msg_hdr.id | FLAG_CMD_RES;
            res->ret = 0;
            CMD_RES_SEND(res);
        } return 0;
        case CMD_FIT_PARA_GET:
        {
            CMD_FIT_PARA_t *res = (CMD_FIT_PARA_t *)GET_CMD_RES_MSG(sizeof(CMD_FIT_PARA_t));
            res->fit_para = g_fit_para;
            res->msg_hdr.id = req->id | FLAG_CMD_RES;
            res->msg_hdr.ret = 0;
            CMD_RES_SEND((msg_t *)res);
        } return 0;

        case CMD_WORLD_RESOLUTION_SET:
        {
            CMD_RESOLUTION_t *reso_req = (CMD_RESOLUTION_t *)req;
            msg_t *res = (msg_t *)GET_CMD_RES_MSG(sizeof(msg_t));
            //msg_t *res = OLD_get_message(sizeof(msg_t));

            g_world_reso.w = reso_req->reso.w;
            g_world_reso.h = reso_req->reso.h;

            res->id = reso_req->msg_hdr.id | FLAG_CMD_RES;
            res->ret = 0;

            CMD_RES_SEND(res);
        } return 0;

        case CMD_WORLD_RESOLUTION_GET:
        {
            //CMD_RESOLUTION_t *req = (CMD_RESOLUTION_t *)msg;
            CMD_RESOLUTION_t *res = (CMD_RESOLUTION_t *)GET_CMD_RES_MSG(sizeof(CMD_RESOLUTION_t));
            //msg_t *res = OLD_get_message(sizeof(msg_t));

            res->reso.w = g_world_reso.w;
            res->reso.h = g_world_reso.h;

            //res->id = msg->id | FLAG_CMD_RES;
            //res->ret = 0;
            res->msg_hdr.id = req->id | FLAG_CMD_RES;
            res->msg_hdr.ret = 0;
            CMD_RES_SEND((msg_t *)res);
        } return 0;

        case CMD_WORLD_FOCAL_LEN_SET: {
            CMD_FOCAL_LEN_t *flen_req = (CMD_FOCAL_LEN_t *)req;
            g_world_focal_len = flen_req->flen;
            CMD_RES_SEND_LITE(req, 0);
        } return 0;

        case CMD_WORLD_FOCAL_LEN_GET: {
            CMD_FOCAL_LEN_t *res = (CMD_FOCAL_LEN_t *)GET_CMD_RES_MSG(sizeof(CMD_FOCAL_LEN_t));
            res->msg_hdr.id = req->id | FLAG_CMD_RES;
            res->msg_hdr.ret = 0;
            res->flen = g_world_focal_len;
            CMD_RES_SEND(res);
        } return 0;

        case CMD_WORLD_INTRINSIC_SET: {
            CMD_INTRINSIC_t *flen_req = (CMD_INTRINSIC_t *)req;
            g_world_intr = flen_req->intr;
            CMD_RES_SEND_LITE(req, 0);
        } return 0;

        case CMD_WORLD_INTRINSIC_GET: {
            CMD_INTRINSIC_t *res = (CMD_INTRINSIC_t *)GET_CMD_RES_MSG(sizeof(CMD_INTRINSIC_t));
            res->msg_hdr.id = req->id | FLAG_CMD_RES;
            res->msg_hdr.ret = 0;
            res->intr = g_world_intr;
            CMD_RES_SEND(res);
        } return 0;

        case CMD_STATE_RESET: {
            state_reset();
            CMD_RES_SEND_LITE(req, 0);
        } return 0;
        case CMD_STATE_SET:{
            BASIC_ASSERT(req->val < STATE_MAX);
            state_set((state_t)req->val);
            CMD_RES_SEND_LITE(req, 0);
        } return 0;
        case CMD_STATE_GET: {
            CMD_RES_SEND_LITE(req, (uint32_t)state_get());
        } return 0;
        case CMD_STATE_STORE: {
            state_store();
            CMD_RES_SEND_LITE(req, 0);
        } return 0;
        case CMD_STATE_RESTORE: {
            state_restore();
            CMD_RES_SEND_LITE(req, 0);
        } return 0;

        case CMD_CALIB_REF_CNT_GET: {
            CMD_RES_SEND_LITE(req, g_ref_cnt);
        } return 0;
        case CMD_CALIB_PUPIL_CNT_GET: {
            // msg_t *res = (msg_t *)GET_CMD_RES_MSG(sizeof(msg_t));
            // res->id = req->id | FLAG_CMD_RES;
            // res->ret = g_pupil_cnt;
            // OLD_send_command_res(res);
            CMD_RES_SEND_LITE(req, g_pupil_cnt);
        } return 0;
        case CMD_CALIB_REF_LIST_DUMP: {
            uint32_t i;
            MSG_REF_IMAGE_t *ref_msg;
            ref_node_t *curr = (ref_node_t *)LLIST_FIRST(&g_rhead);
            if (g_ref_cnt) {
                for (i = 0; i < g_ref_cnt; i++) {
                    ref_msg = (MSG_REF_IMAGE_t *)GET_CMD_RES_MSG(sizeof(MSG_REF_IMAGE_t));
                    ref_msg->msg_hdr.id = req->id | FLAG_CMD_RES;
                    if (i+1 != g_ref_cnt) {
                        ref_msg->msg_hdr.id = req->id | FLAG_CMD_RES_MD;
                    }
                    if (curr) {
                        ref_msg->msg_hdr.ret = 0;
                        ref_msg->ref_node = *curr;
                        curr = (ref_node_t *)LLIST_NEXT(curr);
                        CMD_RES_SEND(ref_msg);
                    } else {
                        ref_msg->msg_hdr.ret = 2;
                        CMD_RES_SEND(ref_msg);
                        break;
                    }
                }
            } else {
                ref_msg = (MSG_REF_IMAGE_t *)GET_CMD_RES_MSG(sizeof(MSG_REF_IMAGE_t));
                ref_msg->msg_hdr.id = req->id | FLAG_CMD_RES;
                ref_msg->msg_hdr.ret = 1;
                CMD_RES_SEND(ref_msg);
            }
        } return 0;
        case CMD_CALIB_PUPIL_LIST_DUMP: {
            uint32_t i;
            MSG_PUPIL_CENTER_t *pupil_msg;
            pupil_node_t *curr = (pupil_node_t *)LLIST_FIRST(&g_phead);
            if (g_pupil_cnt) {
                for (i = 0; i < g_pupil_cnt; i++) {
                    pupil_msg = (MSG_PUPIL_CENTER_t *)GET_CMD_RES_MSG(sizeof(MSG_PUPIL_CENTER_t));
                    pupil_msg->msg_hdr.id = req->id | FLAG_CMD_RES;
                    if (i+1 != g_pupil_cnt) {
                        pupil_msg->msg_hdr.id = req->id | FLAG_CMD_RES_MD;
                    }
                    if (curr) {
                        pupil_msg->msg_hdr.ret = 0;
                        pupil_msg->pupil_node = *curr;
                        curr = (pupil_node_t *)LLIST_NEXT(curr);
                        CMD_RES_SEND(pupil_msg);
                    } else {
                        pupil_msg->msg_hdr.ret = 2;
                        CMD_RES_SEND(pupil_msg);
                        break;
                    }
                }
            } else {
                pupil_msg = (MSG_PUPIL_CENTER_t *)GET_CMD_RES_MSG(sizeof(MSG_PUPIL_CENTER_t));
                pupil_msg->msg_hdr.id = req->id | FLAG_CMD_RES;
                pupil_msg->msg_hdr.ret = 1;
                CMD_RES_SEND(pupil_msg);
            }
        } return 0;

        case CMD_DO_GAZE_PREDICT: {
            CMD_DO_GAZE_PREDICT_t *gaze_req = (CMD_DO_GAZE_PREDICT_t *)req;
            CMD_DO_GAZE_PREDICT_t *res = (CMD_DO_GAZE_PREDICT_t *)GET_CMD_RES_MSG(sizeof(CMD_DO_GAZE_PREDICT_t));

            if (gaze_req->do_compensate) {
                do_compensate(&gaze_req->pupil_center);
            }
            predict_gaze_pos(&res->gaze_pos, &g_fit_para, &gaze_req->pupil_center);

            res->msg_hdr.id = req->id | FLAG_CMD_RES;
            res->msg_hdr.ret = 0;
            CMD_RES_SEND((msg_t *)res);
        } return 0;

        case CMD_DO_COMPENSATE: {
            CMD_DO_COMPENSATE_t *cmpt_req = (CMD_DO_COMPENSATE_t *)req;
            CMD_DO_COMPENSATE_t *res = (CMD_DO_COMPENSATE_t *)GET_CMD_RES_MSG(sizeof(CMD_DO_COMPENSATE_t));

            res->pupil_center = cmpt_req->pupil_center;
            do_compensate(&res->pupil_center);

            res->msg_hdr.id = req->id | FLAG_CMD_RES;
            res->msg_hdr.ret = 0;
            CMD_RES_SEND((msg_t *)res);
        } return 0;

        case CMD_ACCU_SET: {
            CMD_ACCU_t *accu_req = (CMD_ACCU_t *)req;
            g_accu_is_null = accu_req->is_accu_null;
            g_accu_curr = accu_req->accu;
            CMD_RES_SEND_LITE(req, 0);
        } return 0;

        case CMD_ACCU_GET: {
            CMD_ACCU_t *res = (CMD_ACCU_t *)GET_CMD_RES_MSG(sizeof(CMD_ACCU_t));
            res->msg_hdr.id = req->id | FLAG_CMD_RES;
            res->msg_hdr.ret = 0;
            res->is_accu_null = g_accu_is_null;
            res->accu = g_accu_curr;
            CMD_RES_SEND((msg_t *)res);
        } return 0;

        case CMD_CALIB_EBC_SET:
        case CMD_BASE_EBC_SET:
        case CMD_CURR_EBC_SET: {
            CMD_MSG_EBC_t *ebc_req = (CMD_MSG_EBC_t *)req;
            CMD_MSG_EBC_t *msg_to_hi = (CMD_MSG_EBC_t *)GET_TSK_MSG(sizeof(CMD_MSG_EBC_t));
            if(req->id == CMD_CALIB_EBC_SET)
                msg_to_hi->msg_hdr.id = MSG_CALIB_EBC_SET;
            if(req->id == CMD_BASE_EBC_SET)
                msg_to_hi->msg_hdr.id = MSG_BASE_EBC_SET;
            if(req->id == CMD_CURR_EBC_SET)
                msg_to_hi->msg_hdr.id = MSG_CURR_EBC_SET;
            msg_to_hi->ebc = ebc_req->ebc;
            MSG_SEND_HI(msg_to_hi);
            CMD_RES_SEND_LITE(req, 0);
        } return 0;

        case CMD_CALIB_EBC_GET:
        case CMD_BASE_EBC_GET:
        case CMD_CURR_EBC_GET:{
            CMD_MSG_EBC_t *ebc_req = (CMD_MSG_EBC_t *)req;
            CMD_MSG_EBC_t *res = (CMD_MSG_EBC_t *)GET_CMD_RES_MSG(sizeof(CMD_MSG_EBC_t));
            res->msg_hdr.id = req->id | FLAG_CMD_RES;
            res->msg_hdr.ret = 0;
            if(req->id == CMD_CALIB_EBC_GET)
                res->ebc = g_calib_ebc;
            if(req->id == CMD_BASE_EBC_GET)
                res->ebc = g_base_ebc;
            if(req->id == CMD_CURR_EBC_GET)
                res->ebc = g_curr_ebc;
            CMD_RES_SEND((msg_t *)res);
        } return 0;

        case CMD_EBC_SEQ_SET: {
            g_ebc_seqno = req->val;
            CMD_RES_SEND_LITE(req, 0);
        } return 0;

        case CMD_EBC_SEQ_GET: {
            CMD_RES_SEND_LITE(req, (uint32_t)g_ebc_seqno);
        } return 0;

        case CMD_PREDICT_FLAG_SET: {
            g_enable_predict = req->val;
            CMD_RES_SEND_LITE(req, 0);
        } return 0;

        case CMD_PREDICT_FLAG_GET: {
            CMD_RES_SEND_LITE(req, (uint32_t)g_enable_predict);
        } return 0;

        case CMD_COMPENSATE_FLAG_SET: {
            g_enable_compensate = req->val;
            CMD_RES_SEND_LITE(req, 0);
        } return 0;

        case CMD_COMPENSATE_FLAG_GET: {
            CMD_RES_SEND_LITE(req, (uint32_t)g_enable_compensate);
        } return 0;

        case CMD_REF_RETURN_FLAG_SET: {
            g_enable_ref_pos_return = req->val;
            CMD_RES_SEND_LITE(req, 0);
        } return 0;

        case CMD_REF_RETURN_FLAG_GET: {
            CMD_RES_SEND_LITE(req, (uint32_t)g_enable_ref_pos_return);
        } return 0;

        default: {
            CMD_RES_SEND_LITE(req, 1); // command not support
        } break;
    }

    return 0;
}
