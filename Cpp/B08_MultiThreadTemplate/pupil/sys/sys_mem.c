#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "sys.h"
#include "libmem.h"
#include "libmt.h"

void mem_init()
{
    MM_INIT();
    //LibMT_Init();
    
}

void mem_uninit()
{
    //LibMT_Uninit();
    MM_UNINIT();
}

void *mem_alloc(size_t size)
{
    void *ret;
    ret = MM_ALLOC(size);
    return ret;
}

int mem_free(void *ptr)
{
    MM_FREE(ptr);
    return 0;
}

uint8_t *get_image(uint32_t size)
{
    return (uint8_t *)mem_alloc(size);
}

int release_image(uint8_t *ptr)
{
    mem_free(ptr);    
    return 0;
}

void *OLD_get_message(uint32_t size) //old
{
    return mem_alloc(size);
}

void *get_isr_msg(uint32_t size)
{
    return mem_alloc(size);
}

int release_isr_msg(void *ptr)
{
    mem_free(ptr);    
    return 0;
}

void *get_task_msg(uint32_t size)
{
    return mem_alloc(size);
}

int release_task_msg(void *ptr)
{
    mem_free(ptr);    
    return 0;
}

void *get_cmd_req_msg(uint32_t size)
{
    return mem_alloc(size);
}

int release_cmd_req_msg(void *ptr)
{
    mem_free(ptr);    
    return 0;
}

void *get_cmd_res_msg(uint32_t size)
{
    return mem_alloc(size);
}

int release_cmd_res_msg(void *ptr)
{
    mem_free(ptr);    
    return 0;
}

void *get_evt_msg(uint32_t size)
{
    return mem_alloc(size);
}

int release_evt_msg(void *ptr)
{
    mem_free(ptr);    
    return 0;
}