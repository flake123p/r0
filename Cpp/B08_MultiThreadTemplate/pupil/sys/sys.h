#ifndef _SYS_H_INCLUDED_
#define _SYS_H_INCLUDED_

#include <stdint.h>
#include <stdio.h>  // flake: remove this
#include "linked_list.h"
#include "macros.h"
#include "type.h"

#define OUT

#define SYS_DEBUG_EN (1)
#if SYS_DEBUG_EN
#define SYS_SEQ_NO_SET(msg, no) ((msg_t *)msg)->seq_no=(no)
#define SYS_SEQ_NO_GET(msg) (((msg_t *)msg)->seq_no)
#else
#define SYS_SEQ_NO_SET(msg, no)
#define SYS_SEQ_NO_GET(msg) 0
#endif

typedef struct {
    LList_Entry_t en;
    uint32_t id;
    union {
        uint32_t val;
        uint32_t ret;
    };
#if SYS_DEBUG_EN
    uint32_t seq_no;
#endif
    uint32_t size; // include sizeof(msg_t)
} msg_t;

typedef enum {
    TASK_HI = 0,
    TASK_LO,
    TASK_SIM_TOOL,
    TASK_SIM_ISR,
    TASK_MAX,
} task_t;

typedef void * task_handle_t;
typedef void * event_handle_t;
typedef void * mutex_handle_t;

typedef void * EVENT_HANDLE_t;
typedef void * MUTEX_HANDLE_t;

typedef void *(*function_t)(void *);
typedef int (*task_cb)(msg_t *msg);

int OLD_set_state_cb(task_t task, task_cb func);

typedef struct {
    unsigned long int pthread_id;
    const char *name;
    task_cb cb_func;
    task_t id;
    int exit_ret;
    void *arg;
} task_para_handle_t;

typedef struct {
    task_handle_t thre;     //only thread id now
    event_handle_t q_evt;
    mutex_handle_t q_mutex;
    LList_Head_t q_head;
    //const char *name;       //put into thre ??
    //task_t id;              //put into thre ??
    //int exit_ret;           //put into thre ??
    //void *arg;              //put into thre ??
} msg_thread_t;

#define OUT
#define RETURN_IF(retVal) if(retVal){return (retVal);}

int LibIPC_Event_Create(OUT event_handle_t *eventHdlPtr); // AUTO RESET EVENT !!
int LibIPC_Event_Destroy(event_handle_t eventHdl);
int LibIPC_Event_Set(event_handle_t eventHdl);
int LibIPC_Event_Wait(event_handle_t eventHdl);
int LibIPC_Event_BatchCreate(event_handle_t *eventHdlAry, uint32_t len);
int LibIPC_Event_BatchDestroy(event_handle_t *eventHdlAry, uint32_t len);
int LibIPC_Event_GetCounter(void);

int LibIPC_Mutex_Create(OUT mutex_handle_t *mutexHdlPtr);
int LibIPC_Mutex_Destroy(mutex_handle_t mutexHdl);
int LibIPC_Mutex_Lock(mutex_handle_t mutexHdl);
int LibIPC_Mutex_Unlock(mutex_handle_t mutexHdl);
int LibIPC_Mutex_GetCounter(void);

typedef struct {
    uint32_t word;
    struct {
        uint16_t flag;  // msg_flag_t
        uint16_t id;    // msg_id_t
    };
} message;

void *OLD_get_message(uint32_t size);

uint8_t *get_image(uint32_t size);
int release_image(uint8_t *ptr);
void *get_isr_msg(uint32_t size);
int release_isr_msg(void *ptr);
void *get_task_msg(uint32_t size);
int release_task_msg(void *ptr);
void *get_cmd_req_msg(uint32_t size);
int release_cmd_req_msg(void *ptr);
void *get_cmd_res_msg(uint32_t size);
int release_cmd_res_msg(void *ptr);
void *get_evt_msg(uint32_t size);
int release_evt_msg(void *ptr);

void OLD_send_simple_command_req(uint32_t cmd_id);
int OLD_send_command_req(msg_t *msg_hdl); //built-in tool
int OLD_send_command_res(msg_t *msg_hdl);

#define SEND_MSG(a,b) OLD_send_message(a, (msg_t *)b)
void OLD_send_simple_message(task_t task, uint32_t msg_id);
void OLD_send_message(task_t task, msg_t *msg);
msg_t *OLD_receive_message(task_t task);
void wait_message(task_t task);
msg_t *wait_and_receive_message(task_t task);

int msg_thread_handle(task_t task, msg_thread_t *hdl);
int send_event(void *msg_hdl);

msg_thread_t *get_msg_thread_handle(task_t task);

int task_new_handle(task_t task, task_handle_t *taskHdl);
int task_create(task_handle_t taskHdl, function_t func);
int task_wait(task_handle_t taskHdl, void *retval);
int task_exit(void *retval);

void mem_init();
void mem_uninit();
void *mem_alloc(size_t size);
int mem_free(void *ptr);

void sys_print_control(int en);
void sys_print(const char *format, ...);

void sys_assert(int expression, const char *file, int line);
//TODO: disable compiling switch
#define BASIC_ASSERT(a) sys_assert(a, __FILE__, __LINE__)

#define ASSERT_IF(retVal) if(retVal){BASIC_ASSERT(0);};
#define ASSERT_CHK(a, b)  a=b;ASSERT_IF(a)
#define RETURN_IF(retVal) if(retVal){return (retVal);}
#define RETURN_CHK(a, b)  a=b;RETURN_IF(a)
#define RETURN_WHEN(condition, retVal) if(condition){return (retVal);}


typedef void *(*ThreadEntryFunc)(void *);
typedef void * THREAD_HANDLE_t;
int LibThread_NewHandle(OUT THREAD_HANDLE_t *threadHdlPtr, uint32_t priority /* = TPRI_DEFAULT */);
int LibThread_Create(THREAD_HANDLE_t threadHdl, ThreadEntryFunc entry, void *arg /* = NULL */);
int LibThread_WaitThread(THREAD_HANDLE_t threadHdl);
int LibThread_DestroyHandle(THREAD_HANDLE_t threadHdl);
int LibThread_GetCounter(void);

typedef void * msg_task_handle_t; 
int msg_task_new_handle(OUT msg_task_handle_t *hdl_ptr, uint32_t priority);
int msg_task_destory_handle(msg_task_handle_t hdl);
int msg_task_create(msg_task_handle_t hdl, ThreadEntryFunc cb, void *arg);
int msg_task_wait(msg_task_handle_t hdl);

void msg_send(msg_task_handle_t hdl, msg_t *msg);
msg_t *msg_receive(msg_task_handle_t hdl);
void msg_wait(msg_task_handle_t hdl);
msg_t *msg_wait_and_receive(msg_task_handle_t hdl);

// cmd req src = tool
// cmd req dst = task_lo
// cmd res src = task_lo
// cmd res dst = tool
// evt src     = task_hi
// evt dst     = tool
int cmd_internal_interface_config(msg_task_handle_t task_hi_hdl, msg_task_handle_t task_lo_hdl);
int cmd_external_interface_config(msg_task_handle_t tool_hdl);
int cmd_req_send(msg_t *msg_hdl);
int cmd_res_send(msg_t *msg_hdl);
int event_send(msg_t *msg_hdl);

#define PRLOC prloc(__FILE__, __LINE__);
void prloc(const char *file, int line);

#endif//_SYS_H_INCLUDED_
