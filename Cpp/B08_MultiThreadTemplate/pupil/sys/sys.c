
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "sys.h"
//#include "pupil.h"
//#include "linked_list.h"

//Linux platform
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <assert.h>

//TODO: Using struct/array to manager task info.

int g_LibIPC_EventCtr = 0;

typedef struct {
	pthread_cond_t cond;
	pthread_mutex_t mutex;
	int flag;
}EVENT_HANDLE_LINUX_t;
int LibIPC_Event_Create(OUT event_handle_t *eventHdlPtr)
{
    int retVal = 0;
	EVENT_HANDLE_LINUX_t *eHdl = (EVENT_HANDLE_LINUX_t *)malloc(sizeof(EVENT_HANDLE_LINUX_t));
	if (eHdl == NULL)
		return -1;

	retVal = pthread_cond_init(&eHdl->cond, NULL);
	if(retVal) return retVal;
	retVal = pthread_mutex_init(&eHdl->mutex, NULL);
	if(retVal) return retVal;

	eHdl->flag = 0;

	*eventHdlPtr = eHdl;

	g_LibIPC_EventCtr++;

	return 0;
}

int LibIPC_Event_Destroy(EVENT_HANDLE_t eventHdl)
{
	EVENT_HANDLE_LINUX_t *eHdl = (EVENT_HANDLE_LINUX_t *)eventHdl;

	int retVal = pthread_mutex_destroy(&eHdl->mutex);
	RETURN_IF(retVal);

	retVal = pthread_cond_destroy(&eHdl->cond);
	RETURN_IF(retVal);

	free(eventHdl);

	g_LibIPC_EventCtr--;

	return 0;
}

int LibIPC_Event_Set(event_handle_t eventHdl)
{
	EVENT_HANDLE_LINUX_t *eHdl = (EVENT_HANDLE_LINUX_t *)eventHdl;
	//PRINT_FUNC;
	pthread_mutex_lock(&eHdl->mutex);
	pthread_cond_signal(&eHdl->cond);
	eHdl->flag = 1;
	pthread_mutex_unlock(&eHdl->mutex);

	return 0;
}

int LibIPC_Event_Wait(event_handle_t eventHdl)
{
	EVENT_HANDLE_LINUX_t *eHdl = (EVENT_HANDLE_LINUX_t *)eventHdl;
	//PRINT_FUNC;
	pthread_mutex_lock(&eHdl->mutex);
	if (eHdl->flag == 1) {
		eHdl->flag = 0;
	} else {
		pthread_cond_wait(&eHdl->cond, &eHdl->mutex);
	}
	pthread_mutex_unlock(&eHdl->mutex);

	return 0;
}

int LibIPC_Event_GetCounter(void)
{
	return g_LibIPC_EventCtr;
}


int g_LibIPC_MutexCtr = 0;

typedef struct {
	pthread_mutex_t mutex;
}MUTEX_HANDLE_LINUX_t;
int LibIPC_Mutex_Create(OUT MUTEX_HANDLE_t *mutexHdlPtr)
{
	MUTEX_HANDLE_LINUX_t *linuxMutexHdl = (MUTEX_HANDLE_LINUX_t *)malloc(sizeof(MUTEX_HANDLE_LINUX_t));

	int retVal = pthread_mutex_init(&linuxMutexHdl->mutex, NULL);

	*mutexHdlPtr = linuxMutexHdl;

	g_LibIPC_MutexCtr++;

	return retVal;
}

int LibIPC_Mutex_Destroy(MUTEX_HANDLE_t mutexHdl)
{
	int ret;

	MUTEX_HANDLE_LINUX_t *linuxMutexHdl = (MUTEX_HANDLE_LINUX_t *)mutexHdl;

	ret = pthread_mutex_destroy(&linuxMutexHdl->mutex);

	free(mutexHdl);

	g_LibIPC_MutexCtr--;

	return ret;
}

int LibIPC_Mutex_Lock(MUTEX_HANDLE_t mutexHdl)
{
	MUTEX_HANDLE_LINUX_t *linuxMutexHdl = (MUTEX_HANDLE_LINUX_t *)mutexHdl;

	return pthread_mutex_lock(&linuxMutexHdl->mutex);
}

int LibIPC_Mutex_Unlock(MUTEX_HANDLE_t mutexHdl)
{
	MUTEX_HANDLE_LINUX_t *linuxMutexHdl = (MUTEX_HANDLE_LINUX_t *)mutexHdl;

	return pthread_mutex_unlock(&linuxMutexHdl->mutex);
}

int LibIPC_Mutex_GetCounter(void)
{
	return g_LibIPC_MutexCtr;
}

/****************** message *********************/

static msg_thread_t *msg_task_hi;
static msg_thread_t *msg_task_lo;
static msg_thread_t *tool_evt;
static msg_thread_t *msg_task_isr;

msg_thread_t *get_msg_thread_handle(task_t task)
{
    switch (task) {
        case TASK_HI:       return msg_task_hi;
        case TASK_LO:       return msg_task_lo;
        case TASK_SIM_TOOL: return tool_evt;
        case TASK_SIM_ISR:  return msg_task_isr;
        default:            return NULL;
    }
}

void OLD_send_simple_command_req(uint32_t cmd_id)
{
    msg_t *msg = (msg_t *)mem_alloc(sizeof(msg_t));
    msg->id = cmd_id;
    OLD_send_command_req(msg);
}

int OLD_send_command_req(msg_t *msg_hdl)
{
    int ret = 0;
    msg_thread_t *p_msg_thread = get_msg_thread_handle(TASK_LO);
    if(p_msg_thread == NULL) return 1;

    // lock linked list
    LibIPC_Mutex_Lock(p_msg_thread->q_mutex);
    LLIST_INSERT_LAST(&p_msg_thread->q_head, msg_hdl);
    LibIPC_Mutex_Unlock(p_msg_thread->q_mutex);

    // wake thread
    LibIPC_Event_Set(p_msg_thread->q_evt);
    return ret;
}

int OLD_send_command_res(msg_t *msg_hdl)
{
    int ret = 0;
    msg_thread_t *p_msg_thread = get_msg_thread_handle(TASK_SIM_TOOL);
    if(p_msg_thread == NULL) return 1;

    // lock linked list
    LibIPC_Mutex_Lock(p_msg_thread->q_mutex);
    LLIST_INSERT_LAST(&p_msg_thread->q_head, msg_hdl);
    LibIPC_Mutex_Unlock(p_msg_thread->q_mutex);

    // wake thread
    LibIPC_Event_Set(p_msg_thread->q_evt);
    return ret;
}

int msg_thread_handle(task_t task, msg_thread_t *hdl)
{
    int ret = 0;
    switch (task) {
        case TASK_HI:       msg_task_hi = hdl;  break;
        case TASK_LO:       msg_task_lo = hdl;  break;
        case TASK_SIM_TOOL: tool_evt = hdl;     break;
        case TASK_SIM_ISR:  msg_task_isr = hdl; break;
        default:            ret = 1;            break;
    }
    return ret;
}

int send_event(void *msg_hdl)
{
    int ret = 0;
    msg_thread_t *p_msg_thread = get_msg_thread_handle(TASK_SIM_TOOL);
    if(p_msg_thread != NULL) {
        // lock linked list
        LibIPC_Mutex_Lock(p_msg_thread->q_mutex);
        LLIST_INSERT_LAST(&p_msg_thread->q_head, msg_hdl);
        LibIPC_Mutex_Unlock(p_msg_thread->q_mutex);

        // wake thread
        LibIPC_Event_Set(p_msg_thread->q_evt);
    }
    return ret;
}

void OLD_send_simple_message(task_t task, uint32_t msg_id)
{
    msg_t *msg = (msg_t *)mem_alloc(sizeof(msg_t));
    msg->id = msg_id;
    OLD_send_message(task, msg);
}

void OLD_send_message(task_t task, msg_t *msg)
{
    msg_thread_t *p_msg_thread = get_msg_thread_handle(task);
    if(p_msg_thread != NULL) {
        // lock linked list
        LibIPC_Mutex_Lock(p_msg_thread->q_mutex);
        LLIST_INSERT_LAST(&p_msg_thread->q_head, msg);
        LibIPC_Mutex_Unlock(p_msg_thread->q_mutex);

        // wake thread
        LibIPC_Event_Set(p_msg_thread->q_evt);
    }
}

msg_t *OLD_receive_message(task_t task)
{
    msg_thread_t *p_msg_thread = get_msg_thread_handle(task);
    if(p_msg_thread != NULL) {
        msg_t *msg;

        // lock linked list
        LibIPC_Mutex_Lock(p_msg_thread->q_mutex);
        if (LLIST_IS_EMPTY(&p_msg_thread->q_head)) {
            LibIPC_Mutex_Unlock(p_msg_thread->q_mutex);
            return NULL;
        } else {
            msg = (msg_t *)LLIST_FIRST(&p_msg_thread->q_head);
            LLIST_REMOVE_FIRST_SAFELY(&p_msg_thread->q_head);
        }
        LibIPC_Mutex_Unlock(p_msg_thread->q_mutex);

        return msg;
    }
    return NULL;
}

void wait_message(task_t task)
{
    msg_thread_t *p_msg_thread = get_msg_thread_handle(task);
    if(p_msg_thread != NULL) {
        LibIPC_Event_Wait(p_msg_thread->q_evt);
    }
}

msg_t *wait_and_receive_message(task_t task)
{
    msg_t *msg;
    while(1)
    {
        wait_message(task);
        while (1) {
            msg = OLD_receive_message(task);
            if (msg) {
                return msg;
            } else {
                break;
            }
        }
    }
}

/****************** task *********************/

int OLD_set_state_cb(task_t task, task_cb func)
{
    int ret = 0;
    task_para_handle_t *p_task_handle = (task_para_handle_t *)get_msg_thread_handle(task)->thre;
    p_task_handle->cb_func = func;
    return ret;
}

int task_new_handle(task_t task, task_handle_t *taskHdl)
{
    //TODO...
    task_para_handle_t *thread = (task_para_handle_t *)malloc(sizeof(task_para_handle_t));
    #if 1
    thread->id = task;
    thread->cb_func = NULL;
    #else
    thread->cb_func = NULL;
    switch (task) {
        case TASK_HI:
        {
            thread->name = "task_hi";
            thread->id = TASK_HI;
            break;
        }
        case TASK_LO:
        {
            break;
        }
        case TASK_SIM_TOOL:
        {
            break;
        }
        case TASK_SIM_ISR:
        {
            break;
        }
    }
    #endif
    *taskHdl = thread;
    return 0;
}

int task_create(task_handle_t taskHdl, function_t func)
{
    int ret = 0;
    //TODO...
    task_para_handle_t *thread = (task_para_handle_t *)taskHdl;
    ret = pthread_create( &thread->pthread_id, NULL, func, thread);
    //ret != 0 is something error happened.
    return ret;
}

int task_wait(task_handle_t taskHdl, void *retval)
{
    int ret = 0;
    void *exit_ret;
    task_para_handle_t *thread = (task_para_handle_t *)taskHdl;
    //TODO...
    //int pthread_join(pthread_t thread, void **retval);
    ret = pthread_join(thread->pthread_id, NULL);
    //printf("exit_ret [%d]\n", *(int *)exit_ret);
    //thread->exit_ret = *(int *)exit_ret;
    //ret != 0 is something error happened.
    return ret;
}

int task_exit(void *retval)
{
    //TODO...
    //noreturn void pthread_exit(void *retval);
    pthread_exit(NULL);
    return 0;
}

static int g_sys_print_enable = 1;
void sys_print_control(int en)
{
    g_sys_print_enable = en;
}

void sys_print(const char *format, ...)
{
    if (g_sys_print_enable == 0)
        return;

    //UART??
	va_list ap;

	va_start(ap, format);
	vprintf(format, ap);
	va_end(ap);
}

void sys_assert(int expression, const char *file, int line)
{
    if (!expression)
        printf("[%d]Assert in file: %s, line: %d\n", expression, file, line);
    assert(expression);
}

int g_LibThread_Ctr;

typedef struct {
	pthread_t thread;
	uint32_t priority;
}THREAD_HANDLE_LINUX_t;
int LibThread_NewHandle(OUT THREAD_HANDLE_t *threadHdlPtr, uint32_t priority /* = TPRI_DEFAULT */)
{
	THREAD_HANDLE_LINUX_t *linuxThreadHdl = (THREAD_HANDLE_LINUX_t *)malloc(sizeof(THREAD_HANDLE_LINUX_t));
	if (linuxThreadHdl == NULL)
		return -1;

	linuxThreadHdl->priority = priority;
	*threadHdlPtr = linuxThreadHdl;
	g_LibThread_Ctr++;
	return 0;
}

int LibThread_Create(THREAD_HANDLE_t threadHdl, ThreadEntryFunc entry, void *arg /* = NULL */)
{
	THREAD_HANDLE_LINUX_t *linuxThreadHdl = (THREAD_HANDLE_LINUX_t *)threadHdl;

	int retVal = pthread_create(&linuxThreadHdl->thread, NULL, entry, arg);

	return retVal;
}

int LibThread_WaitThread(THREAD_HANDLE_t threadHdl)
{
	THREAD_HANDLE_LINUX_t *linuxThreadHdl = (THREAD_HANDLE_LINUX_t *)threadHdl;

	/* wait for the thread to finish */
	int retVal = pthread_join(linuxThreadHdl->thread, NULL);

	return retVal;
}

int LibThread_DestroyHandle(THREAD_HANDLE_t threadHdl)
{
	free(threadHdl);
	g_LibThread_Ctr--;
	return 0;
}

int LibThread_GetCounter(void)
{
	return g_LibThread_Ctr;
}

// This sys.c is pthread + sim.
extern msg_task_handle_t g_task_hi_hdl;
extern msg_task_handle_t g_task_lo_hdl;
extern msg_task_handle_t g_tool_hdl;

int cmd_req_send(msg_t *msg_hdl)
{
    BASIC_ASSERT(g_tool_hdl != NULL);
    msg_send(g_task_lo_hdl, msg_hdl);
    return 0;
}

int cmd_res_send(msg_t *msg_hdl)
{
    BASIC_ASSERT(g_tool_hdl != NULL);
    msg_send(g_tool_hdl, msg_hdl);
    return 0;
}

int event_send(msg_t *msg_hdl)
{
    BASIC_ASSERT(g_tool_hdl != NULL);
    msg_send(g_tool_hdl, msg_hdl);
    return 0;
}

void prloc(const char *file, int line)
{
    printf("file: %s, line %d\n", file, line);
}