#ifndef __FIT_H_INCLUDED_
#define __FIT_H_INCLUDED_

#include "pupil.h"

int fit_ref_and_pupil(fit_para_t *out, ref_head_t *rhead, pupil_head_t *phead);
int predict_gaze_pos(gaze_pos_t *out, fit_para_t *para, pupil_center_t *pupil_center);

#endif//__FIT_H_INCLUDED_