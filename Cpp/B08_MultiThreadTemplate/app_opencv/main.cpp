#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/types_c.h>

//using namespace cv;
typedef unsigned char byte;

byte * matToBytes(cv::Mat image)
{
   int size = image.total() * image.elemSize();
   byte * bytes = new byte[size];  // you will have to delete[] that later
   memcpy(bytes,image.data,size * sizeof(byte));
   return bytes;
}

cv::Mat bytesToMat(byte *bytes, int width, int height)
{
    cv::Mat image = cv::Mat(height,width,CV_8UC3,bytes).clone(); // make a copy
    return image;
}

int main(int argc, char** argv)
{
    /*
    if ( argc != 2 )
    {
        printf("usage: DisplayImage.out <Image_Path>\n");
        return -1;
    }
    */

    cv::Mat image;
    //image = imread( argv[1], 1 );
    image = cv::imread("../../pupil_data/pieces/func3/pics/1.jpg", 1);
    if (image.elemSize() == 0) {
        image = cv::imread("../pupil_data/pieces/func3/pics/1.jpg", 1);
    }
    if (image.elemSize() == 0) {
        image = cv::imread("pupil_data/pieces/func3/pics/1.jpg", 1);
    }
    printf("h = %d\n", image.size().height);
    printf("w = %d\n", image.size().width);
    printf("total = %lu\n", image.total());
    printf("elemSize = %lu\n", image.elemSize());
    cv::cvtColor(image, image, CV_BGR2GRAY);
    printf("h = %d\n", image.size().height);
    printf("w = %d\n", image.size().width);
    printf("total = %lu\n", image.total());
    printf("elemSize = %lu\n", image.elemSize());
    if ( !image.data )
    {
        printf("No image data \n");
        return -1;
    }
    cv::namedWindow("Display Image", cv::WINDOW_AUTOSIZE );
    cv::imshow("Display Image", image);
    cv::waitKey(0);
    return 0;
}