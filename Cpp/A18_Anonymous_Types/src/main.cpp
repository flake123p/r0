
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

/*
https://docs.microsoft.com/en-us/cpp/cpp/anonymous-class-types?view=msvc-160

Anonymous classes:

    Cannot have a constructor or destructor.

    Cannot be passed as arguments to functions (unless type checking is defeated using ellipsis).

    Cannot be returned as return values from functions.

*/
class {
public:
	void show(){printf("This is anonymous class.\n");}
} AC;

//--
struct // anonymous
{
    unsigned x = 2;
} point;

//--
typedef struct
{
    unsigned x = 3;
} point2;
point2 p2;

//--
struct PTValue
{
    union // anonymous
    {
        u8  x;
        u32 y;
    }; // anonymous
};
PTValue ptv;

//-- Flake: need new compiler, dont use these!!!!!!
struct phone
{
    u32 number = 567;
};
struct person
{
    struct phone;    // Anonymous structure; no name needed
} Jim;

//--
struct // anonymous
{
    struct // anonymous
    {
        int  x = 9;
    }s;
} hybrid;

//--
int main(int argc, char *argv[])
{
	AC.show();

	DND(point.x);
	DND(p2.x);
	ptv.y = 0x103;
	DND(ptv.x);
	DND(sizeof(PTValue::y));
	
	// Flake: need new compiler
	//DND(Jim.number);
	DND(sizeof(Jim));
	
	DND(hybrid.s.x);
	//DND(hybrid.x); //Error, hybrid is not allowed

	return 0;
}
