
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>
#include <signal.h> // sigaction
#include <stdlib.h> //for exit()
#include <set>
#include <vector>
#include <algorithm>
#include <stdint.h>
#include <string>
#include <cstdarg> //va_start va_end
#include <cstring>

//
// ./run.sh: line 19: 65323 Segmentation fault      (core dumped) ./aout.exe
//
int main_rev0() 
{
    int *x = nullptr;

    printf("x[0] = %d\n", x[0]);

    return 0;
}

//
// Solution:
//
// https://stackoverflow.com/questions/2350489/how-to-catch-segmentation-fault-in-linux
//
void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
    printf("[FAILED] Caught segfault at address %p\n", si->si_addr);
    exit(0);
}

int main() 
{
    struct sigaction sa;

    memset(&sa, 0, sizeof(struct sigaction));
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = segfault_sigaction;
    sa.sa_flags   = SA_SIGINFO;

    sigaction(SIGSEGV, &sa, NULL);

    // int y = 99;
    int *x = nullptr;

    printf("x[0] = %d\n", x[0]);
    return 0;
}