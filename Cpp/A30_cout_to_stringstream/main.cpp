#include <iostream>
#include <cstdint>
#include <memory>
#include <vector>
#include <numeric>
#include <cmath>
#include <map>
#include <thread>
#include <algorithm>
#include <future>
#include <climits>
#include <cfloat>
#include <typeinfo>

#define COUT(a) std::cout << #a " = " << a << std::endl
#define PR(a)   std::cout << a << std::endl
#define TSIZE(a) std::cout << "sizeof(" #a ") = " << sizeof(a) << std::endl
#define TNAME(a) std::cout << "typeid(" #a ").name() = " << typeid(a).name() << std::endl
#define CDUMP(a) COUT(a);TSIZE(a);TNAME(a)
#define PRINT_FUNC printf("%s()\n", __func__);

//
// start
//
#include <string>  
#include <iostream> 
#include <sstream> 

// https://stackoverflow.com/questions/5193173/getting-cout-output-to-a-stdstring

int main()
{
    float a = 1.3;
    int b = 99;
    std::cout << a << ", " << b << std::endl;

    {
        std::stringstream buffer;
        buffer << "Text";

        printf("%s\n", buffer.str().c_str());
        printf("%30s\n", buffer.str().c_str());
        printf("%-30s\n", buffer.str().c_str());
    }

    {
        std::stringstream buffer;
        buffer << 1.333333333333333333333;

        printf("%s\n", buffer.str().c_str());
        printf("%30s\n", buffer.str().c_str());
        printf("%-30s\n", buffer.str().c_str());
        printf("%.20f\n", 1.333333333333333333333);
    }

    return 0;
}