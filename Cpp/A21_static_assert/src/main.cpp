
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

/*
static_assert : Tests a software assertion at compile time.

https://docs.microsoft.com/en-us/cpp/cpp/static-assert?view=msvc-160
*/

static_assert(sizeof(void *) == 8, "This code only support 64-bit.");

int main(int argc, char *argv[])
{

	return 0;
}
