
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

enum e0 {
	None = 0,
	Info = 1,
};
enum {
	//None = 999, //error: redeclaration
	Debug = 2,
}e1; //'e1' is not a class, namespace, or enumeration!!!!!!!!!!!!!!!!!!!, 'e1' is a global variable!!!!!!!!!
enum e2{
	//None = 999, //error: redeclaration
	Warning = 4,
}e2_t; //'e2_t' is a global variable!!!!!!!!!

typedef enum e3 { // warning: 'typedef' was ignored in this declaration
	//None = 999, //error: redeclaration
	Error = 8,
};
typedef enum {
	//None = 999, //error: redeclaration
	ABC = 10,
}e4;
typedef enum e5{
	//None = 999, //error: redeclaration
	DEF = 15,
}e5_t;


int main(int argc, char *argv[])
{
	DND(sizeof(e0));
	DND(sizeof(e1));
	DND(sizeof(e2));
	DND(sizeof(e2_t));
	DND(sizeof(e3));
	DND(sizeof(e4));
	DND(sizeof(e5));
	DND(sizeof(e5_t));
	//DND(e0); //error use, e0 is not variable
	DND(e1);
	//DND(e2); //error use, e2 is not variable
	DND(e2_t);
	//DND(e3); //error use, e3 is not variable
	//DND(e4); //error use, e4 is not variable
	//DND(e5); //error use, e4 is not variable
	//DND(e5_t); //error use, e4 is not variable

	DND(None);
	DND(Info);
	//DND(e0.None); //error use
	DND(e0::None);
	//DND(e1.Debug); //error use
	//DND(e1::Debug); //error use
	//DND(e2.Warning); //error use
	DND(e2::Warning);
	//DND(e2_t.Warning); //error use
	//DND(e2_t::Warning); //error use
	
	DND(e3::Error);
	DND(e4::ABC);
	DND(e5::DEF);
	DND(e5_t::DEF);
	
	return 0;
}
