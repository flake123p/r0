#include "basic/_fmt.h"
#include "basic/_assert.h"

#include <iostream>
#include <cstdint>
#include <memory>
#include <vector>
#include <numeric>
#include <cmath>
#include <map>
#include <thread>
#include <algorithm>
#include <future>
#include <climits>
#include <cfloat>
#include <typeinfo>

#define COUT(a)  std::cout << #a " = " << a << std::endl
#define PR(a)    std::cout << a << std::endl
#define TSIZE(a) std::cout << "sizeof(" #a ") = " << sizeof(a) << std::endl
#define TNAME(a) std::cout << "typeid(" #a ").name() = " << typeid(a).name() << std::endl
#define CDUMP(a) \
    COUT(a);     \
    TSIZE(a);    \
    TNAME(a)
#define PRINT_FUNC printf("%s()\n", __func__);

//
// start
//
#include <string>
#include <iostream>
#include <sstream>

// https://stackoverflow.com/questions/5193173/getting-cout-output-to-a-stdstring

int main()
{
    {
        float a = 10.12345678;
        StrFmt b = StrFmt(a);
        std::string c;

        c = b.pr();
        std::cout << c << std::endl;
        BASIC_ASSERT(c == std::string("10.123457"));
        c = b.pr(10, 0, 2);
        BASIC_ASSERT(c == std::string("     10.12"));
        c = b.pr(10, 1, 2);
        BASIC_ASSERT(c == std::string("10.12     "));
    }

    StreamFmt sfmt(std::cout);

    sfmt.Format(std::cout, 30);
    std::cout << "StreamFmt 1: ";
    sfmt.Format(std::cout, 30);
    std::cout << 1.123456789;
    sfmt.Format(std::cout, 30);
    std::cout << ",";
    sfmt.Format(std::cout, 30);
    std::cout << 33.33 << std::endl;

    sfmt.Format(std::cout, 30, 0);
    std::cout << "StreamFmt 2: ";
    sfmt.Format(std::cout, 30, 0);
    std::cout << 1.123456789;
    sfmt.Format(std::cout, 30, 0);
    std::cout << ",";
    sfmt.Format(std::cout, 30, 0);
    std::cout << 33.33 << std::endl;
    
    std::cout << "StreamFmt 2: " << 1.123456789 << std::endl;
    sfmt.Restore(std::cout);
    std::cout << "StreamFmt 3: " << 1.123456789 << std::endl;

    return 0;
}

/*
StrFmt 1: 5566.123535
StrFmt 2: 99
StreamFmt 1: 1.12346
StreamFmt 2: 1.123457
StreamFmt 3: 1.12346
*/