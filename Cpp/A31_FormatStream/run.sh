#!/bin/bash

# g++ -I../include $1 && ./a.out

# objdump -SlzafphxgeGWtTrRs a.out > log.1_ALL.log

g++ -I../../Cpp_Basic/mod main.cpp -o a.out

result=$?

echo ====== Build Done!! Errorlevel = $result ======

if [ $result == 0 ]; then
	./a.out
	echo ====== aout.exe Done!! Errorlevel = $? ======
else
	echo NOT execute aout.exe ...
fi