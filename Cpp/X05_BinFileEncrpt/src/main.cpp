
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>
#include <stdio.h>
#include <assert.h>
/*
          ↗ private ---> NOT AVAILABLE
  private -> protected -> NOT AVAILABLE
          ↘ public ----> NOT AVAILABLE
          ↗ private ---> private
protected -> protected -> protected
          ↘ public ----> protected
          ↗ private ---> private
   public -> protected -> protected
          ↘ public ----> public
*/
typedef struct File_Profiles{
    const char *fileName;
    const char *openMode;
    FILE       *fp;
} File_Profiles_t;
int LibFileIo_OpenFile(File_Profiles_t *fileProfile)
{
    fileProfile->fp = fopen(fileProfile->fileName, fileProfile->openMode); 

    if (fileProfile->fp == NULL) { 
        printf("Cannot open: %s in mode: %s. Exit Now!\n", fileProfile->fileName, fileProfile->openMode);
        return 1;
    }

    return 0;
}

void LibFileIo_CloseFile(File_Profiles_t *fileProfile)
{
    if (fileProfile->fp == NULL) { 
        printf("fp is NULL: %s in mode: %s. Exit Now!\n", fileProfile->fileName, fileProfile->openMode);
        return;
    }

    int closeResult = fclose(fileProfile->fp);
    if(closeResult == EOF) {
        assert(0);
    }

    fileProfile->fp = NULL;
}

void encrypt_rshift4(unsigned char *buf, long len)
{
    unsigned char temp;
    for (long i = 0; i < len; i++) {
        temp = buf[i] & 0x0F;
        temp = temp << 4;
        buf[i] = ((buf[i] >> 4) & 0x0F ) | temp;
    }
}

void decrypt_rshift4(unsigned char *buf, long len)
{
    unsigned char temp;
    for (long i = 0; i < len; i++) {
        temp = buf[i] & 0xF0;
        temp = temp >> 4;
        buf[i] = (buf[i] << 4) | temp;
    }
}

int main(int argc, char *argv[])
{
#define BUF_SIZE 1024
    int ret = 0;
    long lSize, currSize, readLen;
    unsigned char buf[BUF_SIZE];

    File_Profiles_t input = {"test.bin", "r+b", NULL};
    File_Profiles_t output = {"test2.bin", "w+b", NULL};
    
    if (LibFileIo_OpenFile(&input) ) {
        return 1;
    }
    if (LibFileIo_OpenFile(&output) ) {
        return 1;
    }

    // obtain file size:
    fseek (input.fp , 0 , SEEK_END);
    lSize = ftell (input.fp);
    rewind (input.fp);
    printf("lSize = %ld\n", lSize);

    int i = 0;
    int debugDump = 1;
    unsigned char before, after;
    while(lSize) {
        if (lSize > BUF_SIZE) {
            currSize = BUF_SIZE;
            lSize -= BUF_SIZE;
        } else {
            currSize = lSize;
            lSize = 0;
        }
        readLen = fread(buf, 1, currSize, input.fp);
        assert(readLen == currSize);

        if (debugDump) {
            if (i < 1) {
                printf("before:  ");
                for (int n = 0; n < 20; n ++) {
                    printf("%2x ", buf[n]);
                }
                printf("\n");
            } 
        }

        encrypt_rshift4(buf, currSize);
        //decrypt_rshift4(buf, currSize); //for test

        if (debugDump) {
            if (i < 1) {
                printf("after :  ");
                for (int n = 0; n < 20; n ++) {
                    printf("%2x ", buf[n]);
                }
                printf("\n");
            } 
            i++;
        }

        fwrite(buf, 1, currSize, output.fp);
    }

    LibFileIo_CloseFile(&input);
    LibFileIo_CloseFile(&output);

    return ret;
}
