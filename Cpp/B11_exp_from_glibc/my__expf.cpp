#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>

#ifndef WANT_ROUNDING
/* Correct special case results in non-nearest rounding modes.    */
# define WANT_ROUNDING 1
#endif
#ifndef WANT_ERRNO
/* Set errno according to ISO C with (math_errhandling & MATH_ERRNO) != 0.    */
# define WANT_ERRNO 1
#endif
#ifndef WANT_ERRNO_UFLOW
/* Set errno to ERANGE if result underflows to 0 (in all rounding modes).    */
# define WANT_ERRNO_UFLOW (WANT_ROUNDING && WANT_ERRNO)
#endif

typedef double double_t;
#  define INFINITY (__builtin_inff ())
# define attribute_hidden
#define NOINLINE

static inline uint32_t
asuint (float f)
{
  union
  {
    float f;
    uint32_t i;
  } u = {f};
  return u.i;
}

static inline uint32_t
top12 (float x)
{
    return asuint (x) >> 20;
}

/* NOINLINE reduces code size.  */
NOINLINE static float
with_errnof (float y, int e)
{
  errno = e;
  return y;
}

NOINLINE static float
xflowf (uint32_t sign, float y)
{
  y = (sign ? -y : y) * y;
  return with_errnof (y, ERANGE);
}

attribute_hidden float
__math_oflowf (uint32_t sign)
{
  return xflowf (sign, 0x1p97f);
}

attribute_hidden float
__math_uflowf (uint32_t sign)
{
  return xflowf (sign, 0x1p-95f);
}

#if WANT_ERRNO_UFLOW
/* Underflows to zero in some non-nearest rounding mode, setting errno
   is valid even if the result is non-zero, but in the subnormal range.  */
attribute_hidden float
__math_may_uflowf (uint32_t sign)
{
  return xflowf (sign, 0x1.4p-75f);
}
#endif

/* Shared between expf, exp2f, exp10f, and powf.  */
#define EXP2F_TABLE_BITS 5
#define EXP2F_POLY_ORDER 3
extern const struct exp2f_data
{
  uint64_t tab[1 << EXP2F_TABLE_BITS];
  double shift_scaled;
  double poly[EXP2F_POLY_ORDER];
  double shift;
  double invln2_scaled;
  double poly_scaled[EXP2F_POLY_ORDER];
} __exp2f_data attribute_hidden;

#define N (1 << EXP2F_TABLE_BITS)

const struct exp2f_data __exp2f_data = {
  /* tab[i] = uint(2^(i/N)) - (i << 52-BITS)
     used for computing 2^(k/N) for an int |k| < 150 N as
     double(tab[k%N] + (k << 52-BITS)) */
  .tab = {
0x3ff0000000000000, 0x3fefd9b0d3158574, 0x3fefb5586cf9890f, 0x3fef9301d0125b51,
0x3fef72b83c7d517b, 0x3fef54873168b9aa, 0x3fef387a6e756238, 0x3fef1e9df51fdee1,
0x3fef06fe0a31b715, 0x3feef1a7373aa9cb, 0x3feedea64c123422, 0x3feece086061892d,
0x3feebfdad5362a27, 0x3feeb42b569d4f82, 0x3feeab07dd485429, 0x3feea47eb03a5585,
0x3feea09e667f3bcd, 0x3fee9f75e8ec5f74, 0x3feea11473eb0187, 0x3feea589994cce13,
0x3feeace5422aa0db, 0x3feeb737b0cdc5e5, 0x3feec49182a3f090, 0x3feed503b23e255d,
0x3feee89f995ad3ad, 0x3feeff76f2fb5e47, 0x3fef199bdd85529c, 0x3fef3720dcef9069,
0x3fef5818dcfba487, 0x3fef7c97337b9b5f, 0x3fefa4afa2a490da, 0x3fefd0765b6e4540,
  },
  .shift_scaled = 0x1.8p+52 / N,
  .poly = { 0x1.c6af84b912394p-5, 0x1.ebfce50fac4f3p-3, 0x1.62e42ff0c52d6p-1 },
  .shift = 0x1.8p+52,
  .invln2_scaled = 0x1.71547652b82fep+0 * N,
  .poly_scaled = {
0x1.c6af84b912394p-5/N/N/N, 0x1.ebfce50fac4f3p-3/N/N, 0x1.62e42ff0c52d6p-1/N
  },
};

// #define N (1 << EXP2F_TABLE_BITS)
#define InvLn2N __exp2f_data.invln2_scaled
#define T __exp2f_data.tab
#define C __exp2f_data.poly_scaled

# define math_narrow_eval(x) (x)

static inline uint64_t
asuint64 (double f)
{
  union
  {
    double f;
    uint64_t i;
  } u = {f};
  return u.i;
}

static inline double
asdouble (uint64_t i)
{
  union
  {
    uint64_t i;
    double f;
  } u = {i};
  return u.f;
}

float
my__expf (float x)
{
    uint32_t abstop;
    uint64_t ki, t;
    /* double_t for better performance on targets with FLT_EVAL_METHOD==2.    */
    double_t kd, xd, z, r, r2, y, s;
    xd = (double_t) x;
    abstop = top12 (x) & 0x7ff;
    if (__glibc_unlikely (abstop >= top12 (88.0f)))
        {
            /* |x| >= 88 or x is nan.    */
            if (asuint (x) == asuint (-INFINITY))
                return 0.0f;
            if (abstop >= top12 (INFINITY))
                return x + x;
            if (x > 0x1.62e42ep6f) /* x > log(0x1p128) ~= 88.72 */
                return __math_oflowf (0);
            if (x < -0x1.9fe368p6f) /* x < log(0x1p-150) ~= -103.97 */
                return __math_uflowf (0);
#if WANT_ERRNO_UFLOW
            if (x < -0x1.9d1d9ep6f) /* x < log(0x1p-149) ~= -103.28 */
                return __math_may_uflowf (0);
#endif
        }
    /* x*N/Ln2 = k + r with r in [-1/2, 1/2] and int k.    */
    z = InvLn2N * xd;
    /* Round and convert z to int, the result is in [-150*N, 128*N] and
         ideally ties-to-even rule is used, otherwise the magnitude of r
         can be bigger which gives larger approximation error.    */
#if TOINT_INTRINSICS
    kd = roundtoint (z);
    ki = converttoint (z);
#else
# define SHIFT __exp2f_data.shift
    kd = math_narrow_eval ((double) (z + SHIFT)); /* Needs to be double.    */
    ki = asuint64 (kd);
    kd -= SHIFT;
#endif
    r = z - kd;
    /* exp(x) = 2^(k/N) * 2^(r/N) ~= s * (C0*r^3 + C1*r^2 + C2*r + 1) */
    t = T[ki % N];
    t += ki << (52 - EXP2F_TABLE_BITS);
    s = asdouble (t);
    z = C[0] * r + C[1];
    r2 = r * r;
    y = C[2] * r + 1;
    y = z * r2 + y;
    y = y * s;
    return (float) y;
}

/*
    -103.0 -> 89.0    (step 0.1)
    -104.00 -> 103.90 (step 0.01)

[torch.exp(   88.700) : 3.325976662e+38 / 01111111011110100011011111111011
cpu                                       01111111011110100011011111111100
my                                        01111111011110100011011111111100

[torch.exp(   89.000) :             inf / 01111111100000000000000000000000
cpu                                       01111111100000000000000000000000
my                                        01111111100000000000000000000000

[torch.exp( -103.970) : 1.401298464e-45 / 00000000000000000000000000000001
cpu                                       00000000000000000000000000000001
my                                        00000000000000000000000000000001

[torch.exp( -103.980) : 0.000000000e+00 / 00000000000000000000000000000000
cpu                                       00000000000000000000000000000000
my                                        00000000000000000000000000000000

exp(   88.70) = 3.325977e+38 / 01111111011110100011011111111100
exp(   89.00) =          inf / 01111111100000000000000000000000
exp( -103.97) = 1.401298e-45 / 00000000000000000000000000000001
exp( -103.98) = 0.000000e+00 / 00000000000000000000000000000000
*/



void exp_f32_dump(float input)
{
  float result = my__expf(input);
  printf("exp(%8.2f) = %e / ", input, result);

  uint32_t myU32 = asuint(result);

  for (int i = 0; i < 32; i++) {
    if (myU32 & 0x80000000) {
      printf("1");
    } else {
      printf("0");
    }
    myU32 = myU32 << 1;
  }

  printf("\n");
}

// #include "npy.hpp"

// template<typename T>
// void npy_mgr_read(T *dst_buf, std::string file_name) {
//     // printf("str = %s\n", file_name.c_str());
//     npy::npy_data d = npy::read_npy<T>(file_name);

//     std::vector<T> data = d.data;

//     for (size_t i = 0; i < data.size(); i++) {
//         // printf("%f\n", data[i]);
//         dst_buf[i] = data[i];
//     }
// }

void my__expf_demo()
{
  // float x = 1.3;

  std::cout << "exp(-inf): " << my__expf(-INFINITY) << std::endl;
  std::cout << "exp( inf): " << my__expf(INFINITY) << std::endl;

  printf("%19.9f\n", my__expf(0.2f));
  printf("%19.9f\n", my__expf(1.0f));
  printf("%19.9f\n", my__expf(1.3f));
  printf("%19.9f\n", my__expf(20.0f));
  printf("%e\n", my__expf(88.7));
  printf("%e\n", my__expf(89));
  printf("%e\n", my__expf(-103.97));
  printf("%e\n", my__expf(-103.98));

  exp_f32_dump(88.7);
  exp_f32_dump(89);
  exp_f32_dump(-103.97);
  exp_f32_dump(-103.98);

  /*
  tensor([-104.0000, -103.9000, -103.8000,  ...,   88.8000,   88.9000,
          89.0000])
  */
}