#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <cmath>
#include "npy.hpp"
extern void my__expf_demo();
extern float my__expf(float x);

template<typename T>
void npy_mgr_read(T *dst_buf, std::string file_name) {
    // printf("str = %s\n", file_name.c_str());
    npy::npy_data d = npy::read_npy<T>(file_name);

    std::vector<T> data = d.data;

    for (size_t i = 0; i < data.size(); i++) {
        // printf("%f\n", data[i]);
        dst_buf[i] = data[i];
    }
}

std::string bits32(float input) 
{
    union
    {
        float f;
        uint32_t i;
    } u;
    u.f = input;
    uint32_t inputU32 = u.i;
    std::string ret = "b'";
    for (int i = 0; i < 32; i++) {
        if (inputU32 & 0x80000000) {
            ret += "1";
        } else {
            ret += "0";
        }
        inputU32 <<= 1;
    }
    return ret;
}

int main()
{
    // my__expf_demo();

    /*
    tensor([-104.0000, -103.9000, -103.8000,  ...,   88.8000,   88.9000,
            89.0000])
    */
    #define LEN 1931
    float golden_input[LEN];
    float golden_gpu[LEN];
    // float golden_cpu[LEN];

    npy_mgr_read(golden_input, "../11_golden_input.npy");
    npy_mgr_read(golden_gpu, "../11_golden_gpu.npy");

    // for (int i = 0; i < LEN; i++) {
    //     printf("%e\n", golden_gpu[i]);
    // }

    // for (int i = 0; i < LEN; i++) {
    //     printf("%e (%d)\n", golden_gpu[i], std::isinf(golden_gpu[i]));
    // }

    FILE *fpt;
    fpt = fopen("MyFile.csv", "w+");
    fprintf(fpt,"Input, GPU Golden, MY expf(), Diff Ratio, GPU Bits, My Bits, Diff Abs Bits\n");

    float result, diff_abs;
    double diff_rate, diff_acc = 0;
    for (int i = 0; i < LEN; i++) {
        result = my__expf(golden_input[i]);
        diff_abs = 0;
        diff_rate = 0;
        if (std::isinf(golden_gpu[i])) {
            if (std::isinf(result)) {
                diff_abs = 0;
            } else {
                printf("Error 0: Not inf!\n");
                exit(1);
            }
        } else {
            if (std::isinf(result)) {
                printf("Error 1: Unexpected inf!\n");
                exit(1);
            } else {
                diff_abs = std::abs(result - golden_gpu[i]);
                if (golden_gpu[i] == 0) {
                    if (golden_input[i] == 0) {
                        diff_rate = 0;
                    } else {
                        diff_rate = (double)diff_abs / golden_input[i];
                    }
                } else {
                    diff_rate = (double)diff_abs / golden_gpu[i];
                }
                
            }
        }
        diff_acc += diff_rate;
        printf("%e (%d) %e, %f, %.20f, %.20f\n", golden_gpu[i], std::isinf(golden_gpu[i]), result, golden_input[i], diff_rate, diff_acc);
        fprintf(fpt,"%e, %e, %e, %e, %s, %s, %s\n", 
                golden_input[i], golden_gpu[i], result, diff_rate, 
                bits32(golden_gpu[i]).c_str(), bits32(result).c_str(), bits32(diff_abs).c_str());
    }
    printf("avg diff = %.15f ppm\n", (diff_acc / LEN) * 1000000);
    fclose(fpt);

    return 0;
}