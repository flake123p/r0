cmake_minimum_required(VERSION 3.12)

project(main)

#file(GLOB_RECURSE CFILES "${CMAKE_SOURCE_DIR}/*.c") SET_SOURCE_FILES_PROPERTIES(${CFILES} PROPERTIES LANGUAGE CXX )
file(GLOB main_SRC CONFIGURE_DEPENDS "*.c" "*.cpp" "*.cxx" "*.cc")
SET_SOURCE_FILES_PROPERTIES(${main_SRC} PROPERTIES LANGUAGE CXX )
add_executable(a ${main_SRC})

# add the binary tree to the search path for include files
# so that we will find TutorialConfig.h
target_include_directories(a PUBLIC
                           "${PROJECT_BINARY_DIR}"
                           "${PROJECT_SOURCE_DIR}/../test_4_fit"
                           )
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../test_4_fit ${CMAKE_CURRENT_BINARY_DIR}/test_4_fit)
target_link_libraries(a PUBLIC test_4_fit m)