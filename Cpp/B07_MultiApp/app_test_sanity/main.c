
#include <stdio.h>
#include <stdint.h>
#include <math.h>

#include "test_tool.h"
#include "test_misc.h"

int main(int argc, char* argv[])
{
  printf("Hello %s\n", __FILE__);

  test_tool();
  test_misc();
  return 0;
}
