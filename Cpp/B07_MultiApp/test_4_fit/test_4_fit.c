
#include <stdio.h>
#include <stdint.h>

#include "pupil.h"
#include "util.h"
#include "fit.h"

void test_4_fit()
{
  printf("Hello %s\n", __FILE__);

  //fit_ref_and_pupil(NULL, NULL, NULL);
  predict_gaze_pos(NULL, NULL);

  {
    ref_head_t hd;
    ref_node_t n0, n1, n2;
    n0.x = 555;
    n1.x = 777;
    n2.x = 999;
    ref_list_init(&hd);
    ref_list_append(&hd, &n0);
    ref_list_append(&hd, &n1);
    ref_list_append(&hd, &n2);
    fit_ref_and_pupil(NULL, &hd, NULL);
  }
}


