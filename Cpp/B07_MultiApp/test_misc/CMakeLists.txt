cmake_minimum_required(VERSION 3.12)
file(GLOB SRC CONFIGURE_DEPENDS "*.c" "*.cpp" "*.cxx" "*.cc")
SET_SOURCE_FILES_PROPERTIES(${SRC} PROPERTIES LANGUAGE CXX )
add_library(test_misc ${SRC})

target_include_directories(test_misc PUBLIC
                           )

target_link_libraries(test_misc PUBLIC)