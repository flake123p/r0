
#include "util.h"


int ref_list_init(ref_head_t *head)
{
    LLIST_HEAD_RESET(head);
    return 0;
}

int ref_list_free(ref_head_t *head)
{
    return 0;
}

int ref_list_append(ref_head_t *head, ref_node_t *node)
{
    LLIST_INSERT_LAST(head, node);
    return 0;
}