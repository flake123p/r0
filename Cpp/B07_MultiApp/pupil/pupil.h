#ifndef __PUPIL_H_INCLUDED__
#define __PUPIL_H_INCLUDED__

#include <stdio.h>
#include <stdint.h>
#include "linked_list.h"

#define FLOAT_T           double
#define TIMESTAMP_T       uint32_t
#define CONFI_T           uint32_t //0~100

typedef struct {
    FLOAT_T coef[2][6];
    FLOAT_T itrc[2];
} fit_para_t;

typedef struct {

} gaze_pos_t;

typedef struct {
    LList_Head_t hd;
} ref_head_t;

typedef struct {
    LList_Head_t hd;
} pupil_head_t;

typedef struct {
    LList_Entry_t en;
    FLOAT_T x;
    FLOAT_T y;
    CONFI_T confi; //confidence
    TIMESTAMP_T ts; //timestamp
} ref_node_t;

typedef struct {
    LList_Entry_t en;
    FLOAT_T x;
    FLOAT_T y;
    CONFI_T confi; //confidence
    TIMESTAMP_T ts; //timestamp
} pupil_node_t;

#endif//__PUPIL_H_INCLUDED__