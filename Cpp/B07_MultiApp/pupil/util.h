#ifndef __UTIL_H_INCLUDED__
#define __UTIL_H_INCLUDED__


#include "pupil.h"


int ref_list_init(ref_head_t *head);
int ref_list_free(ref_head_t *head);
int ref_list_append(ref_head_t *head, ref_node_t *node);
int pupil_list_init(pupil_head_t *head);
int pupil_list_free(pupil_head_t *head);
int pupil_list_append(pupil_head_t *head, pupil_node_t *node);

#endif//__PUPIL_H_INCLUDED__