/*
    https://github.com/opencv/opencv
 */

#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/types_c.h>

using namespace std;
using namespace cv;

void test_matrix_mul(void)
{
    cv::Mat A = cv::Mat(4, 4, CV_32F);
    cv::Mat B = cv::Mat(4, 4, CV_32F);

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            A.at<float>(i,j) = i+j;
            B.at<float>(i,j) = i+j+1;
        }
    }

    cv::Mat C = A * B;

    cout << "A:" << endl;
    cout << A << endl << endl;
    
    cout << "B:" << endl;
    cout << B << endl << endl;
    
    cout << "C:" << endl;
    cout << C << endl << endl;
}

void f(const Mat& A)
{
    cout << A << endl;
    Mat u, w, vt;
    SVD::compute(A, w, u, vt);

    cout << "u:" << endl;
    cout << u << endl << endl;

    cout << "w:" << endl;
    cout << w << endl << endl;

    Mat fullW = fullW.diag(w);
    cout << "w as rectangular diagonal matrix:" << endl;
    std::cout << fullW << endl << endl;

    cout << "vt:" << endl;
    cout <<vt << endl << endl;

    cv::Mat recoveredA = u * fullW * vt;
    cout << "Recovered A" << endl << recoveredA << endl << endl;
}

void test_matrix_svd()
{
    cv::Mat A = cv::Mat(4, 4, CV_32F)
    = (cv::Mat_<double>(4, 4, CV_32F)
    <<
    3, 2, 0, 0,
    10, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0);
    
    f(A);
}

int main(int argc, char** argv)
{
    //test_matrix_mul();
    test_matrix_svd();
    return 0;
}