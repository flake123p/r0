
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>
#include <csignal>
#include <stdlib.h> //for exit()
#include <set>
#include <vector>
#include <algorithm>
#include <stdint.h>

#define AT_DISPATCH_SWITCH(NAME) { \
    constexpr const char* at_dispatch_name = NAME; \
    printf("ker: %s\n",at_dispatch_name); \
}

#define STRCONCAT(a,b) a b

#define AT_DISPATCH_ALL_TYPES_AND_COMPLEX_AND4(NAME) AT_DISPATCH_SWITCH(NAME"_x27")

int main () 
{
    AT_DISPATCH_SWITCH("abc");

    AT_DISPATCH_ALL_TYPES_AND_COMPLEX_AND4("def");

    return 0;
}
