
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

/*
Ref: https://stackoverflow.com/questions/8121320/get-memory-address-of-member-function

// class declaration
class MyClass
{
public:
    void Func();
    void Func(int a, int b);
};

// get the pointer to the member function
void (__thiscall MyClass::* pFunc)(int, int) = &MyClass::Func;

// naive pointer cast
void* pPtr = (void*) pFunc; // oops! this doesn't compile!

// another try
void* pPtr = reinterpret_cast<void*>(pFunc); // Damn! Still doesn't compile (why?!)

// tricky cast
void* pPtr = (void*&) pFunc; // this works

*/

class base {
public:
	void base_func(void) { printf("class base base_func()\n"); };
};

class derivedA : public base {
public:
};

class derivedB : public base {
public:
};

int main(int argc, char *argv[])
{
	void *pp;
	void (derivedA::* func)(void) = &derivedA::base_func;
	pp = (void*&) func; // this works
	printf("Address of derivedA::base_func : ");
	DNP(pp);
	void (derivedB::* func2)(void) = &derivedB::base_func;
	pp = (void*&) func2; // this works
	printf("Address of derivedB::base_func : ");
	DNP(pp);
	
	return 0;
}
