#include <iostream>
#include <cstdint>
#include <memory>
#include <vector>
#include <numeric>
#include <cmath>
#include <map>
#include <thread>
#include <algorithm>
#include <future>
#include <climits>
#include <cfloat>
#include <typeinfo>

#define COUT(a) std::cout << #a " = " << a << std::endl
#define PR(a)   std::cout << a << std::endl
#define TSIZE(a) std::cout << "sizeof(" #a ") = " << sizeof(a) << std::endl
#define TNAME(a) std::cout << "typeid(" #a ").name() = " << typeid(a).name() << std::endl
#define CDUMP(a) COUT(a);TSIZE(a);TNAME(a)
#define PRINT_FUNC printf("%s()\n", __func__);

//
// start
//
#include <err.h>
#include <fcntl.h>
#include <stdio.h>

// https://unix.stackexchange.com/questions/450076/redirecting-a-string-to-current-tty
void demo_0() 
{
    int ttyfd;
    if ((ttyfd = open("/dev/tty", O_WRONLY)) == -1)
        err(1, "open /dev/tty failed");
    printf("test1\n");
    dprintf(ttyfd, " \rxxx");
    printf("test2\n");
}

void demo_1() 
{
    int ttyfd;
    if ((ttyfd = open("/dev/pts/21", O_WRONLY)) == -1)
        err(1, "open /dev/tty failed");
    printf("test1\n");
    dprintf(ttyfd, "xxx\n");
    printf("test2\n");
}

int main()
{
    demo_1();
    return 0;
}