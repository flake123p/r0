#include <stdio.h>
#include <vector>
#include <string>

extern uint8_t *get_heap();
extern void * _pvPortMalloc( size_t xWantedSize );
typedef struct xHeapStats
{
    size_t xAvailableHeapSpaceInBytes;      /* The total heap size currently available - this is the sum of all the free blocks, not the largest block that can be allocated. */
    size_t xSizeOfLargestFreeBlockInBytes;  /* The maximum size, in bytes, of all the free blocks within the heap at the time _vPortGetHeapStats() is called. */
    size_t xSizeOfSmallestFreeBlockInBytes; /* The minimum size, in bytes, of all the free blocks within the heap at the time _vPortGetHeapStats() is called. */
    size_t xNumberOfFreeBlocks;             /* The number of free memory blocks within the heap at the time _vPortGetHeapStats() is called. */
    size_t xMinimumEverFreeBytesRemaining;  /* The minimum amount of total free memory (sum of all free blocks) there has been in the heap since the system booted. */
    size_t xNumberOfSuccessfulAllocations;  /* The number of calls to _pvPortMalloc() that have returned a valid memory block. */
    size_t xNumberOfSuccessfulFrees;        /* The number of calls to _vPortFree() that has successfully freed a block of memory. */
} HeapStats_t;
extern void _vPortGetHeapStats( HeapStats_t * pxHeapStats );
extern void _vPortFree( void * pv );

void DumpHeapStats(void)
{
    HeapStats_t xHeapStats;
    _vPortGetHeapStats(&xHeapStats);
    printf("    xAvailableHeapSpaceInBytes      = %ld\n", xHeapStats.xAvailableHeapSpaceInBytes);
    printf("    xSizeOfLargestFreeBlockInBytes  = %ld\n", xHeapStats.xSizeOfLargestFreeBlockInBytes);
    printf("    xSizeOfSmallestFreeBlockInBytes = %ld\n", xHeapStats.xSizeOfSmallestFreeBlockInBytes);
    printf("    xNumberOfFreeBlocks             = %ld\n", xHeapStats.xNumberOfFreeBlocks);
    printf("    xMinimumEverFreeBytesRemaining  = %ld\n", xHeapStats.xMinimumEverFreeBytesRemaining);
    printf("    xNumberOfSuccessfulAllocations  = %ld\n", xHeapStats.xNumberOfSuccessfulAllocations);
    printf("    xNumberOfSuccessfulFrees        = %ld\n\n", xHeapStats.xNumberOfSuccessfulFrees);
}

int main()
{
    printf("aaa\n");
    printf("ucHeap = %p\n", get_heap());

    void *ptr0 = _pvPortMalloc(3);
    printf("ptr0 = %p\n", ptr0);

    DumpHeapStats();

    void *ptr1 = _pvPortMalloc(4);
    printf("ptr1 = %p\n", ptr1);

    DumpHeapStats();

    _vPortFree(ptr0);  // 2 free blocks now

    DumpHeapStats();
    
    return 0;
}