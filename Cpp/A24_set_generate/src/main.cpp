
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>
#include <csignal>
#include <stdlib.h> //for exit()
#include <set>
#include <vector>
#include <algorithm>
/*
    It isn't a reference to a reference: such a thing does not exist.

    It is an rvalue reference, a new feature added in C++11.
    http://thbecker.net/articles/rvalue_references/section_01.html
*/

/*
    https://stackoverflow.com/questions/5590978/reference-of-reference-in-c

    Search for "perfect forwarding"

    https://tjsw.medium.com/%E6%BD%AE-c-11-perfect-forwarding-%E5%AE%8C%E7%BE%8E%E8%BD%89%E7%99%BC%E4%BD%A0%E7%9A%84%E9%9C%80%E6%B1%82-%E6%B7%B1%E5%BA%A6%E8%A7%A3%E6%9E%90-f991830bcd84
    
    https://wdv4758h.github.io/notes/cpp/perfect-forwarding.html

    http://kksnote.logdown.com/posts/1653018-c-forward-11-perfect-forwarding-are-what
*/

void dump_set_int(std::set<int> &in, const char *name = "")
{
    printf("%s (size=%ld) :\n", name, in.size());

    for (auto it = in.begin(); it != in.end(); ++it) {
        printf("%d ", *it);
    }
    printf("\n");
}

void dump_vec_int(std::vector<int> &in, const char *name = "")
{
    printf("%s (size=%ld) :\n", name, in.size());

    for (auto it = in.begin(); it != in.end(); ++it) {
        printf("%d ", *it);
    }
    printf("\n");
}

int main () {

    std::set<int> seed_indices;

    seed_indices.emplace(3);
    //seed_indices.emplace(7);
    seed_indices.emplace(2);

    dump_set_int(seed_indices, "seed_indices");

    typedef std::set<int> Path;
    std::vector<Path> unknown(seed_indices.size());

    int n = 0;
    std::generate(unknown.begin(), unknown.end(), [&]() { return Path{n++}; }); // fill with increasing values, starting from 0

    printf("unknown size = %ld\n", unknown.size());
    dump_set_int(unknown[0], "unknown[0]");
    dump_set_int(unknown[1], "unknown[1]");
    //dump_set_int(unknown[2], "unknown[2]");

    int contours_size = 4;

    std::vector<int> mapping; // contains all indices, starting with seed_indices
    mapping.reserve(contours_size);
    dump_vec_int(mapping, "mapping s");
    mapping.insert(mapping.begin(), seed_indices.begin(), seed_indices.end());
    dump_vec_int(mapping, "mapping e");

    // add indices which are not used to the end of mapping
    for (int i = 0; i < contours_size; i++) {
        if (seed_indices.find(i) == seed_indices.end()) {
            mapping.push_back(i);
        }
    }
    dump_vec_int(mapping, "mapping ee");

    return 0;
}
