
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>
#include <string.h>

// nested_class_declarations.cpp
class BufferedIO
{
public:
   enum IOError { None, Access, General };

   // Declare nested class BufferedInput.
   class BufferedInput
   {
   public:
      int read();
      int good()
      {
         return _inputerror == None;
      }
   private:
       IOError _inputerror;
   };

   // Declare nested class BufferedOutput.
   class BufferedOutput
   {
      // Member list
   };
   
   class BufferedInput input;
};

int main(int argc, char *argv[])
{
	class BufferedIO io;
	DND(io.input.good());
	
	return 0;
}
