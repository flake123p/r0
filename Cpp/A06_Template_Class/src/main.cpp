
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

template <class Item> // typename Item -> works too!!
class Base { 
public:
	Item val;
	virtual Item verbose(){return val;};
	virtual void run(){printf("run() in Base class\n");};
}; 

void Base_Demo()
{
	class Base<u32> obj32;
	class Base<double> objDouble;
	obj32.val = 32;
	DND(obj32.val);
	objDouble.val = 32.99;
	DNF(objDouble.val);
}

template <class Item, typename REQ, typename RES> 
class Derived : public Base<Item> { 
public: 
	REQ req;
	RES res;
	virtual void run(){printf("run() in Derived class\n");};
};

struct req1 {
};

struct res1 {
	u32 ret;
};

struct req2 {
	u32 type;
};

struct res2 {
	u32 ret;
};

class CommandNo2 : public Derived<u32, struct req2, struct res2> { 
public: 
	u32 verbose(){ return req.type + res.ret;};
	virtual void run(){printf("run() in CommandNo2 class\n");};
};

void Derived_Demo()
{
	class Derived<u32, struct req1, struct res1> objA; //declaration of template class
	DND(sizeof(objA.req));
	DND(sizeof(objA.res));
	objA.res.ret = 999;
	DND(objA.res.ret);
	
	class CommandNo2 c2;
	c2.req.type = 100;
	c2.res.ret = 1000;
	DND(c2.verbose());
	
	c2.run();
	
	// This will error: must align typename Item to u32
	//class Base<double> *basePtr = dynamic_cast<class Base<double> *>(&c2);
	
	//class Base<u32> *basePtr = dynamic_cast<class Base<u32> *>(&c2); //this will work too
	class Base<u32> *basePtr = (class Base<u32> *)(&c2); //this will work too
	basePtr->run();
}

int main(int argc, char *argv[])
{
	Base_Demo();
	Derived_Demo();
	
	return 0;
}
