
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>
#include <csignal>
#include <stdlib.h> //for exit()

/*
   https://stackoverflow.com/questions/14613409/are-stdsignal-and-stdraise-thread-safe

   signal() is a method for inter-process communication, 
   
   not for inter-thread.  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!111

*/

/*
https://www.tutorialspoint.com/cplusplus/cpp_signal_handling.htm

1 SIGABRT
Abnormal termination of the program, such as a call to abort.

2 SIGFPE
An erroneous arithmetic operation, such as a divide by zero or an operation resulting in overflow.

3 SIGILL
Detection of an illegal instruction.

4 SIGINT
Receipt of an interactive attention signal.

5 SIGSEGV
An invalid access to storage.

6 SIGTERM
A termination request sent to the program.
*/

using namespace std;

void signalHandler( int signum ) {
   cout << "Interrupt signal (" << signum << ") received.\n";

   // cleanup and close up stuff here  
   // terminate program  

   exit(signum);  
}

int main () {
   // register signal SIGINT and signal handler  
   signal(SIGINT, signalHandler);  

   cout << "Going to sleep...." << endl;
   //raise( SIGINT);
   while(1) {
      //cout << "Going to sleep...." << endl;
      //sleep(1);
   }

   return 0;
}
