
#include "FixPoint.h"

fix32 fix_point_add(fix32 a, fix32 b)
{
    return a + b;
}

fix32 fix_point_sub(fix32 a, fix32 b)
{
    return a - b;
}

fix32 fix_point_mul(fix32 a, fix32 b, int precision)
{
    return (int64_t)a * b >> (precision);
}

fix32 fix_point_div(fix32 a, fix32 b, int precision)
{
    return (fix32)(((int64_t)a << precision ) / b);
}

fix32 fix32_encode(int32_t in, int precision)
{
    return in << precision;
};

int32_t fix32_decode(fix32 in, int precision)
{
    return in >> precision;
}