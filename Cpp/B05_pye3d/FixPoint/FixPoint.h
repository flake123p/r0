

#ifndef _FIX_POINT_H_INCLUDED_

#include <stdio.h>
#include <stdint.h>

#define PRECISION 16

typedef int32_t fix32; //eazy to read

fix32 fix_point_add(fix32 a, fix32 b);
fix32 fix_point_sub(fix32 a, fix32 b);
fix32 fix_point_mul(fix32 a, fix32 b, int precision);
fix32 fix_point_div(fix32 a, fix32 b, int precision);
fix32 fix32_encode(int32_t in, int precision);
int32_t fix32_decode(fix32 in, int precision);

#define FIX_ADD_32(a,b) fix_point_add((a),(b))
#define FIX_SUB_32(a,b) fix_point_sub((a),(b))
#define FIX_MUL_32(a,b) fix_point_mul((a),(b),PRECISION)
#define FIX_DIV_32(a,b) fix_point_div((a),(b),PRECISION)
#define FIX_ENC_32(a) fix32_encode(a,PRECISION)
#define FIX_DEC_32(a) fix32_decode(a,PRECISION)

#define FADD2(a,b) FIX_ADD_32(a,b)
#define FADD3(a,b,c) FADD2(FADD2(a,b),c)
#define FMUL2(a,b) FIX_MUL_32(a,b)
#define FMUL3(a,b,c) FMUL2(FMUL2(a,b),c)
#define FDIV2(a,b) FIX_DIV_32(a,b)
#define FSUB2(a,b) FIX_SUB_32(a,b)

// [a0, a1] @ [b0, b1]
#define FDOT1221(a0,a1,b0,b1) FADD2(FMUL2(a0,b0),FMUL2(a1,b1))
// [a0, a1, a2] @ [b0, b1, b2]
#define FDOT1331(a0,a1,a2,b0,b1,b2) FADD3(FMUL2(a0,b0),FMUL2(a1,b1),FMUL2(a2,b2))

#define F_V_X_VT2(a0,a1) FDOT1221(a0,a1,a0,a1)
#define F_V_X_VT3(a0,a1,a2) FDOT1331(a0,a1,a2,a0,a1,a2)

#define FPOW2(a) FMUL2(a,a)
#define FPOW3(a) FMUL3(a,a,a)
#define FPOW4(a) FMUL2(FMUL2(a,a),FMUL2(a,a))

#define FSQRARE(a) FPOW2(a)

#define DIV2(a) ((a)>>1)
#define DIV4(a) ((a)>>2)
#define DIV8(a) ((a)>>3)
#define DIV16(a) ((a)>>4)

#define MUL2(a) ((a)<<1)
#define MUL4(a) ((a)<<2)
#define MUL8(a) ((a)<<3)
#define MUL16(a) ((a)<<4)

#define _FIX_POINT_H_INCLUDED_
#endif//_FIX_POINT_H_INCLUDED_
