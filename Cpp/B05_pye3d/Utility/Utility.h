
#ifndef _UTILITY_H_INCLUDED_

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "LinkedList.h"

struct NP_Array
{
    int32_t shape_len;
    int32_t shape[];
};
#define NP_ARRAY(np_array_ptr) ((np_array_ptr->shape)+(np_array_ptr->shape_len))

struct NP_Array_Lite
{
    int32_t shape_len;
    int32_t shape[6];
    int32_t array[];
};

struct NP_Array_Lite2
{
    int32_t shape_len;
    int32_t shape[6];
    int32_t *array;
};

#define ary_arg(a) (a).array,(a).shape,&((a).shape_len)
#define p_ary_arg(a) (a)->array,(a)->shape,&((a)->shape_len)

#define SIMPLE_ASSERT(a) if(!(a)){printf("ASSERT IN FILE:%s, LINE:%d\n", __FILE__, __LINE__);exit(1);}


#define _UTILITY_H_INCLUDED_
#endif//_UTILITY_H_INCLUDED_
