
#include <stdio.h>
#include <stdint.h>
#include "Pye3d.h"
#include "FixPoint.h"
#include "FixPoint_Adapter.h"
#include "FixPoint_Numpy.h"
#include "Projections.h"

#define LITE_MEM_SIZE 6000
int32_t DummyMem[10][LITE_MEM_SIZE];

struct NP_Array_Lite *ArrayLiteAlloc(int num)
{
  SIMPLE_ASSERT(num<LITE_MEM_SIZE);
  //return (struct NP_Array_Lite *)DummyMem[0];
  return (struct NP_Array_Lite *)malloc(num*sizeof(int32_t));
}

int ArrayLiteFree(struct NP_Array_Lite *p)
{
  free(p);
  return 0;
}

int _estimate_sphere_center_2d(struct TwoSphereModel *model, int32_t projected_sphere_center_out[2])
{
  struct Observation *curr;
  struct NP_Array_Lite *aux_2d, *aux_2d_sum;
  int32_t aux_2d_sum_ary[sizeof(struct NP_Array_Lite)+6];
  int32_t *ptr;
  size_t len = sizeof(curr->aux_2d);
  int32_t ctr, axis;

  if (DLLIST_IS_EMPTY(&model->storage)) {
    projected_sphere_center_out[0] = 0;
    projected_sphere_center_out[1] = 0;
    return 0;
  }
  // Max estimation: 2x3x200 = 1200
  aux_2d = ArrayLiteAlloc(1500);
  ptr = aux_2d->array;
  ctr = 0;
  DLLIST_FOREACH(&model->storage, curr, struct Observation) {
    memcpy(ptr, curr->aux_2d, len);
    ptr += len;
    ctr += 1;
  }
  aux_2d->shape_len = 3;
  aux_2d->shape[0] = ctr;
  aux_2d->shape[1] = 2;
  aux_2d->shape[2] = 3;

  aux_2d_sum = (struct NP_Array_Lite *)aux_2d_sum_ary;
  axis = 0;
  np_sum(aux_2d->array, aux_2d->shape, &aux_2d->shape_len, &axis, aux_2d_sum->array, aux_2d_sum->shape, &aux_2d_sum->shape_len);
  {
    //array slicing
    int32_t aux_2d_sum_22[2][2], aux_2d_sum_22_sh[2], aux_2d_sum_22_sh_len;
    struct NP_Array_Lite2 aux_2d_sum_21, pinv_out, dot_out;
    int32_t b[2], outBuf[4];
    aux_2d_sum_21.array = b;
    aux_2d_sum_21.array[0] = aux_2d_sum->array[2];
    aux_2d_sum_21.array[1] = aux_2d_sum->array[5];
    //
    aux_2d_sum_22[0][0] = aux_2d_sum->array[0];
    aux_2d_sum_22[0][1] = aux_2d_sum->array[1];
    aux_2d_sum_22[1][0] = aux_2d_sum->array[3];
    aux_2d_sum_22[1][1] = aux_2d_sum->array[4];
    aux_2d_sum_22_sh[0] = 2;
    aux_2d_sum_22_sh[1] = 2;
    aux_2d_sum_22_sh_len = 2;
    //
    pinv_out.array = outBuf;
    np_pinv_22(aux_2d_sum_22, aux_2d_sum_22_sh, &aux_2d_sum_22_sh_len, ary_arg(pinv_out));
    dot_out.array = projected_sphere_center_out;
    np_dot(ary_arg(pinv_out), ary_arg(aux_2d_sum_21), ary_arg(dot_out));
  }
  ArrayLiteFree(aux_2d);
  return 0;
}

int _estimate_sphere_center_3d(struct TwoSphereModel *model, int32_t projected_sphere_center[2], int32_t sphere_center_out[3], int32_t *rms_residual_out)
{
  struct Observation *curr;
  size_t len = sizeof(curr->aux_3d);
  struct NP_Array_Lite *aux_3d, *gaze_2d_origins, *gaze_2d_directions, *aux_3d_disambiguated;
  int32_t *ptr, *ptr2, ctr, i, axis;
  struct NP_Array_Lite2 sum_aux_3d;
  int32_t sum_aux_3d_buf[12]; //3x4

  sum_aux_3d.array = sum_aux_3d_buf;

  aux_3d = ArrayLiteAlloc(5500); // 2x3x4x200 = 4800
  aux_3d_disambiguated = ArrayLiteAlloc(5500); // 2x3x4x200 = 4800
  gaze_2d_origins = ArrayLiteAlloc(1000); // 2x200 = 400
  gaze_2d_directions = ArrayLiteAlloc(1000); // 2x200 = 400

  //
  // _prep_data() start
  //
  //aux_3d = np.concatenate([obs.aux_3d for obs in observations])
  //aux_3d.shape = -1, 2, 3, 4
  ptr = aux_3d->array;
  ctr = 0;
  DLLIST_FOREACH(&model->storage, curr, struct Observation) {
    memcpy(ptr, curr->aux_3d, len);
    ptr += len;
    ctr += 1;
  }
  aux_3d->shape_len = 4;
  aux_3d->shape[0] = ctr;
  aux_3d->shape[1] = 2;
  aux_3d->shape[2] = 3;
  aux_3d->shape[3] = 4;
  //gaze_2d = np.concatenate([obs.gaze_2d_line for obs in observations])
  //gaze_2d.shape = -1, 4
  ptr = gaze_2d_origins->array;
  ptr2 = gaze_2d_directions->array;
  ctr = 0;
  len = 2; //sizeof(curr->gaze_2d_line);
  DLLIST_FOREACH(&model->storage, curr, struct Observation) {
    //gaze_2d_towards_center = sphere_center_2d - gaze_2d_origins
    ptr[0] = FSUB2(projected_sphere_center[0], curr->gaze_2d_line[0]);
    ptr[1] = FSUB2(projected_sphere_center[1], curr->gaze_2d_line[1]);

    ptr2[0] = curr->gaze_2d_line[2];
    ptr2[1] = curr->gaze_2d_line[3];

    //dot_products = np.sum(gaze_2d_towards_center * gaze_2d_directions, axis=1)
    ptr[0] = FADD2(FMUL2(ptr[0], ptr2[0]), FMUL2(ptr[1], ptr2[1]));

    //angle_larger_90_deg = dot_products < 0
    //disambiguation_indices = np.where(angle_larger_90_deg, 0, 1)
    ptr2[0] = ptr[0] < 0 ? 0 : 1;

    //disambiguation_indices

    ptr += 1;
    ptr2 += 1;
    ctr += 1;
  }
  //gaze_2d_origins->shape_len = 2;
  //gaze_2d_origins->shape[0] = ctr;
  //gaze_2d_origins->shape[1] = 2;
  gaze_2d_directions->shape_len = 1;
  gaze_2d_directions->shape[0] = ctr;
  //gaze_2d_directions->shape[1] = 2;
  //
  // _prep_data() end
  //

  #define disambiguation_indices gaze_2d_directions->array
  #define obs_idc gaze_2d_origins->array
  ptr = aux_3d->array;
  ptr2 = aux_3d_disambiguated->array;
  for (i = 0; i < ctr; i++) {
    //obs_idc[i] = i;
    
    //aux_3d_disambiguated = aux_3d[obs_idc, disambiguation_indices, :, :]
    if (disambiguation_indices[i]) {
      memcpy(ptr2, ptr, 12);
    } else {
      memcpy(ptr2, ptr+12, 12);
    }
    
    ptr += 24; //2x3x4
    ptr2 += 12; //3x4
  }

  axis = 0;
  np_sum(p_ary_arg(aux_3d_disambiguated), &axis, ary_arg(sum_aux_3d));
  //
  // _disambiguate_dierkes_lines() end
  //

  //
  // _calc_sphere_center(), prior_3d=None, prior_strength=0.0 case
  //
  {
    //struct NP_Array_Lite2 sum_aux_3d;
    //int32_t sum_aux_3d_buf[12]; //3x4
    struct NP_Array_Lite2 sum_aux_3d_33, sum_aux_3d_31;
    int32_t sum_aux_3d_33_buf[3][3]; //3x3
    int32_t sum_aux_3d_31_buf[3]; //3x1
    sum_aux_3d_33.array = (int32_t *)sum_aux_3d_33_buf;
    sum_aux_3d_31.array = sum_aux_3d_31_buf;
    
    // slicing
    sum_aux_3d_33.shape_len = 2;
    sum_aux_3d_33.shape[0] = 3;
    sum_aux_3d_33.shape[1] = 3;
    sum_aux_3d_31.shape_len = 2;
    sum_aux_3d_31.shape[0] = 3;
    sum_aux_3d_31.shape[1] = 1;
    #define SRC sum_aux_3d.array
    #define DST1 sum_aux_3d_33.array
    #define DST2 sum_aux_3d_31.array
    DST1[0] = SRC[0];
    DST1[1] = SRC[1];
    DST1[2] = SRC[2];
    DST2[0] = SRC[3];
    DST1[0+3] = SRC[0+4];
    DST1[1+3] = SRC[1+4];
    DST1[2+3] = SRC[2+4];
    DST2[0+1] = SRC[3+4];
    DST1[0+3+3] = SRC[0+4+4];
    DST1[1+3+3] = SRC[1+4+4];
    DST1[2+3+3] = SRC[2+4+4];
    DST2[0+1+1] = SRC[3+4+4];
    #undef SRC
    #undef DST1
    #undef DST2

    struct NP_Array_Lite2 sum_aux_3d_pinv, sum_aux_3d_out;
    int32_t sum_aux_3d_pinv_buf[9]; //3x3
    int32_t sum_aux_3d_out_buf[3]; //3x1
    sum_aux_3d_pinv.array = sum_aux_3d_pinv_buf;
    sum_aux_3d_out.array = sum_aux_3d_out_buf;
    np_pinv_33(sum_aux_3d_33_buf, sum_aux_3d_33.shape, &sum_aux_3d_33.shape_len, ary_arg(sum_aux_3d_pinv));
    np_dot(ary_arg(sum_aux_3d_pinv), ary_arg(sum_aux_3d_31), ary_arg(sum_aux_3d_out));

    //
    // _calc_rms_residual: TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //
    sphere_center_out[0] = sum_aux_3d_out.array[0];
    sphere_center_out[1] = sum_aux_3d_out.array[1];
    sphere_center_out[2] = sum_aux_3d_out.array[2];
  }

  // bottom
  ArrayLiteFree(aux_3d);
  ArrayLiteFree(aux_3d_disambiguated);
  ArrayLiteFree(gaze_2d_origins);
  ArrayLiteFree(gaze_2d_directions);
  return 0;
}

int _estimate_sphere_center_3d_manual(struct TwoSphereModel *model)
{
  int32_t projected_sphere_center[2];
  int32_t sphere_center[3];
  int32_t rms_residual_out;

  if (DLLIST_IS_EMPTY(&model->storage)) {
    model->sphere_center_3d[0] = 0;
    model->sphere_center_3d[1] = 0;
    model->sphere_center_3d[2] = 0; 
    return 0;
  }

  _estimate_sphere_center_2d(model, projected_sphere_center);
  _estimate_sphere_center_3d(model, projected_sphere_center, sphere_center, &rms_residual_out);

  model->sphere_center_3d[0] = sphere_center[0];
  model->sphere_center_3d[1] = sphere_center[1];
  model->sphere_center_3d[2] = sphere_center[2];

  return 0;
}

int _proj_sphere_center(struct TwoSphereModel *model)
{
  if (DLLIST_IS_EMPTY(&model->storage)) {
    model->sphere_center_2d[0] = 0;
    model->sphere_center_2d[1] = 0;
    return 1;
  }

  struct Sphere sphere;
  struct Ellipse ellipse_out;

  project_sphere_into_image_plane(&sphere, &model->camera, 1, &ellipse_out);

  model->sphere_center_2d[0] = ellipse_out.center[0];
  model->sphere_center_2d[1] = ellipse_out.center[1];
  return 0;
}

int TwoSphereModel_Init(struct TwoSphereModel *model, struct CameraModel *camera, int32_t manual_model_confi_thres)
{
  model->camera = *camera;
  DLLIST_HEAD_RESET(&model->storage)
  model->manual_model_confi_thres = manual_model_confi_thres;
  return 0;
}

int TwoSphereModel_AddObservation(struct TwoSphereModel *model, struct Observation *observation)
{
  //printf("add obs : %f %f\n", FIX_TO_FLOAT_32(observation->confidence), FIX_TO_FLOAT_32(model->manual_model_confi_thres));
  if (observation->confidence > model->manual_model_confi_thres) {
    //Observation_Parse(observation);
    DLLIST_INSERT_LAST(&model->storage, observation);
  }
  return 0;
}

/*
  PYTHON ORIGINAL:
  
    def proj_sphere_center_manual_model(self):
        sphere_center = self.manual_model.estimate_sphere_center_3d_manual()
        return self.proj_sphere_center(sphere_center)
*/
int TwoSphereModel_ProjSphereCenter(struct TwoSphereModel *model)
{
  _estimate_sphere_center_3d_manual(model);
  _proj_sphere_center(model);
  return 0;
}

int CameraModel_Dump(struct CameraModel *camera)
{
  printf("focal_length = %d\n", fix32_to_int32(camera->focal_length, PRECISION));
  printf("resolution[0] = %d\n", fix32_to_int32(camera->resolution[0], PRECISION));
  printf("resolution[1] = %d\n", fix32_to_int32(camera->resolution[1], PRECISION));
  return 0;
}

int TwoSphereModel_Dump(struct TwoSphereModel *model)
{
  CameraModel_Dump(&model->camera);
  struct Observation *curr;
  DLLIST_FOREACH(&model->storage, curr, struct Observation) {
    printf("center[0] = %lf\n", FIX_TO_FLOAT_32(curr->ellipse.center[0]));
  }
  return 0;
}

void Pye3d_Test(void)
{
  printf("%s\n", __func__);
}


//typedef int32_t fix32;

int unproject_conicoid(
    const int32_t a,
    const int32_t b,
    const int32_t c,
    const int32_t f,
    const int32_t g,
    const int32_t h,
    const int32_t u,
    const int32_t v,
    const int32_t w,
    const int32_t focal_length,
    const int32_t circle_radius,
    struct Circle circles_out[2])
{
  return 0;
}