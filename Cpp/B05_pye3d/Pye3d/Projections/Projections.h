
#ifndef _PROJECTIONS_H_INCLUDED_

#include <stdio.h>
#include <stdint.h>
#include "../Pye3d.h"

void projtest();

/*
    Coefficients A-F of the general equation (implicit form) of a conic
        Ax² + Bxy + Cy² + Dx + Ey + F = 0
    calculated from 5 ellipse parameters, see
    https://en.wikipedia.org/wiki/Ellipse#General_ellipse
*/
struct Conic
{
    int32_t A;
    int32_t B;
    int32_t C;
    int32_t D;
    int32_t E;
    int32_t F;
};

/*
    Coefficients of the general equation (implicit form) of a cone, given its vertex and
    base (ellipse/conic). Formulae follow equations (1)-(3) of:
        Safaee-Rad, R. et al.: "Three-Dimensional Location Estimation of Circular
        Features for Machine Vision", IEEE Transactions on Robotics and Automation,
        Vol.8(5), 1992, pp624-640.
*/
struct Conicoid
{
    int32_t A;
    int32_t B;
    int32_t C;
    int32_t F;
    int32_t G;
    int32_t H;
    int32_t U;
    int32_t V;
    int32_t W;
    int32_t D;
};

int Conic_Init(struct Ellipse *ellipse_in, struct Conic *conit_out);
int unproject_ellipse(struct Ellipse *ellipse, int32_t focal_length, int32_t radius, struct Circle circles_out[2]);
int project_line_into_image_plane(struct Line *line_in, int32_t focal_length, struct Line *line_out);
int project_sphere_into_image_plane(struct Sphere *sphere, struct CameraModel *cam, int transform, struct Ellipse *ellipse_out);

#define _PROJECTIONS_H_INCLUDED_
#endif//_PROJECTIONS_H_INCLUDED_
