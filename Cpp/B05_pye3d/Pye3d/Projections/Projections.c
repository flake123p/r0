
#include "Projections.h"
#include "../Pye3d.h"
#include "FixPoint.h"
#include "FixPoint_Numpy.h"

void projtest()
{
  printf("projtest\n");
}

// conic = Conic(ellipse)
int Conic_Init(struct Ellipse *ellipse_in, struct Conic *conic_out)
{
    int32_t ax = np_cos_fake(ellipse_in->angle);
    int32_t ay = np_sin_fake(ellipse_in->angle);
    int32_t a2 = FIX_MUL_32(ellipse_in->major_radius, ellipse_in->major_radius);
    int32_t b2 = FIX_MUL_32(ellipse_in->minor_radius, ellipse_in->minor_radius);
    int32_t temp1;
    int32_t temp2;
    // A
    temp1 = FIX_MUL_32(FIX_MUL_32(a2,ay),ay);
    temp2 = FIX_MUL_32(FIX_MUL_32(b2,ax),ax);
    conic_out->A = FIX_ADD_32(temp1, temp2);
    // B
    temp1 = FIX_SUB_32(b2,a2) << 1;
    temp2 = FIX_MUL_32(ax,ay);
    conic_out->B = FIX_MUL_32(temp1, temp2);
    // C
    temp1 = FIX_MUL_32(FIX_MUL_32(a2,ax),ax);
    temp2 = FIX_MUL_32(FIX_MUL_32(b2,ay),ay);
    conic_out->C = FIX_ADD_32(temp1, temp2);
    // D
    temp1 = FIX_MUL_32(conic_out->A, ellipse_in->center[0]) << 1;
    temp2 = FIX_MUL_32(conic_out->B, ellipse_in->center[1]);
    conic_out->D = FIX_SUB_32(-temp1, temp2);
    // E
    temp1 = -(FIX_MUL_32(conic_out->B, ellipse_in->center[0]));
    temp2 = FIX_MUL_32(conic_out->C, ellipse_in->center[1]) << 1;
    conic_out->E = FIX_SUB_32(temp1, temp2);
    // F
    temp1 = FIX_ADD_32(FMUL3(conic_out->A, ellipse_in->center[0], ellipse_in->center[0]), FMUL3(conic_out->B, ellipse_in->center[0], ellipse_in->center[1]));
    temp2 = FIX_SUB_32(FMUL3(conic_out->C, ellipse_in->center[1], ellipse_in->center[1]), FMUL2(a2, b2));
    conic_out->F = FIX_ADD_32(temp1, temp2);
    return 0;
}

int Conicoid_Init(struct Conic *conic_in, int32_t vertex_in[3], struct Conicoid * conicoid_out)
{
    int32_t alpha = vertex_in[0];
    int32_t beta = vertex_in[1];
    int32_t gamma = vertex_in[2];
    int32_t temp1, temp2;
    // A
    conicoid_out->A = FMUL3(gamma, gamma, conic_in->A);
    // B
    conicoid_out->B = FMUL3(gamma, gamma, conic_in->C);
    // C
    temp1 = FADD3(FMUL3(conic_in->A, alpha, alpha), FMUL3(conic_in->B, alpha, beta), FMUL3(conic_in->C, beta, beta));
    temp2 = FADD3(FMUL2(conic_in->D, alpha), FMUL2(conic_in->E, beta), conic_in->F);
    conicoid_out->C = FADD2(temp1, temp2);
    // self.F = -gamma * (conic.C * beta + conic.B / 2 * alpha + conic.E / 2)
    temp1 = -gamma;
    temp2 = FADD3(FMUL2(conic_in->C, beta), FMUL2(conic_in->B>>1, alpha), DIV2(conic_in->E));
    conicoid_out->F = FMUL2(temp1, temp2);
    // self.G = -gamma * (conic.B / 2 * beta + conic.A * alpha + conic.D / 2)
    temp1 = -gamma;
    temp2 = FADD3(FMUL2(conic_in->B>>1, beta), FMUL2(conic_in->A, alpha), DIV2(conic_in->D));
    conicoid_out->G = FMUL2(temp1, temp2);
    // self.H = (gamma**2) * conic.B / 2
    conicoid_out->H = FMUL3(gamma, gamma, conic_in->B)>>1;
    // self.U = (gamma**2) * conic.D / 2
    conicoid_out->U = FMUL3(gamma, gamma, conic_in->D)>>1;
    // self.V = (gamma**2) * conic.E / 2
    conicoid_out->V = FMUL3(gamma, gamma, conic_in->E)>>1;
    // self.W = -gamma * (conic.E / 2 * beta + conic.D / 2 * alpha + conic.F)
    temp1 = -gamma;
    temp2 = FADD3(FMUL2(conic_in->E>>1, beta), FMUL2(conic_in->D>>1, alpha), conic_in->F);
    conicoid_out->W = FMUL2(temp1, temp2);
    // self.D = (gamma**2) * conic.F
    conicoid_out->D = FMUL3(gamma, gamma, conic_in->F);

    return 0;
}

int unproject_ellipse(struct Ellipse *ellipse, int32_t focal_length, int32_t radius, struct Circle circles_out[2])
{
  /*
    conic = Conic(ellipse)
    pupil_cone = Conicoid(conic, [0, 0, -focal_length])
  */
  struct Conic conic;
  struct Conicoid conicoid;
  struct Circle circles[2];
  int32_t vertex[3] = {0,0,-focal_length};

  Conic_Init(ellipse, &conic);
  Conicoid_Init(&conic, vertex, & conicoid);

  /*
    circles = unproject_conicoid(
        pupil_cone.A,
        pupil_cone.B,
        pupil_cone.C,
        pupil_cone.F,
        pupil_cone.G,
        pupil_cone.H,
        pupil_cone.U,
        pupil_cone.V,
        pupil_cone.W,
        focal_length,
        radius
    )

    # cannot iterate over C++ std::pair, that's why this looks so ugly
    circle_A = Circle(
            center=(circles.first.center[0], circles.first.center[1], circles.first.center[2]),
            normal=(circles.first.normal[0], circles.first.normal[1], circles.first.normal[2]),
            radius=circles.first.radius
        )
    circle_B = Circle(
            center=(circles.second.center[0], circles.second.center[1], circles.second.center[2]),
            normal=(circles.second.normal[0], circles.second.normal[1], circles.second.normal[2]),
            radius=circles.second.radius
        )
  */
  return 0;
}

/*
def project_point_into_image_plane(point, focal_length):
    scale = focal_length / point[2]
    point_projected = scale * np.asarray(point)
    return point_projected[:2]
*/
int project_point_into_image_plane(int32_t point_in[3], int32_t focal_length, int32_t point_out[2])
{
  int32_t scale = FDIV2(focal_length, point_in[2]);
  point_out[0] = FMUL2(scale, point_in[0]);
  point_out[1] = FMUL2(scale, point_in[1]);
  return 0;
}

/*
def project_line_into_image_plane(line, focal_length):
    p1 = line.origin
    p2 = line.origin + line.direction

    p1_projected = project_point_into_image_plane(p1, focal_length)
    p2_projected = project_point_into_image_plane(p2, focal_length)

    return Line(p1_projected, p2_projected - p1_projected)
*/
int project_line_into_image_plane(struct Line *line_in, int32_t focal_length, struct Line *line_out)
{
  int32_t p1[3], p2[3], p1_projected[2], p2_projected[2], temp[2];
  p1[0] = line_in->origin[0];
  p1[1] = line_in->origin[1];
  p1[2] = line_in->origin[2];
  p2[0] = line_in->origin[0] + line_in->direction[0];
  p2[1] = line_in->origin[1] + line_in->direction[1];
  p2[2] = line_in->origin[2] + line_in->direction[2];

  project_point_into_image_plane(p1, focal_length, p1_projected);
  project_point_into_image_plane(p2, focal_length, p2_projected);

  temp[0] = p2_projected[0] - p1_projected[0];
  temp[1] = p2_projected[1] - p1_projected[1];

  Line_Init_2D(line_out, p1_projected, temp);

  return 0;
}

/*

def project_sphere_into_image_plane(
    sphere, focal_length, transform=True, width=0, height=0
):
    scale = focal_length / sphere.center[2]

    projected_sphere_center = scale * sphere.center
    projected_radius = scale * sphere.radius

    if transform:
        projected_sphere_center[0] += width / 2.0
        projected_sphere_center[1] += height / 2
        projected_radius *= 2.0

    return Ellipse(projected_sphere_center[:2], projected_radius, projected_radius, 0.0)

*/

int project_sphere_into_image_plane(struct Sphere *sphere, struct CameraModel *cam, int transform, struct Ellipse *ellipse_out)
{
  #define width cam->resolution[0]
  #define height cam->resolution[1]
  fix32 scale = FDIV2(cam->focal_length, sphere->center[2]);
  fix32 projected_sphere_center[3];
  fix32 projected_radius = FMUL2(scale, sphere->radius);

  projected_sphere_center[0] = FMUL2(scale, sphere->center[0]);
  projected_sphere_center[1] = FMUL2(scale, sphere->center[1]);
  //projected_sphere_center[2] = FMUL2(scale, sphere->center[2]);

  if (transform) {
    projected_sphere_center[0] = FADD2(projected_sphere_center[0], DIV2(width));
    projected_sphere_center[1] = FADD2(projected_sphere_center[1], DIV2(height));
    projected_radius = MUL2( projected_radius );
  }

  ellipse_out->center[0] = projected_sphere_center[0];
  ellipse_out->center[1] = projected_sphere_center[1];
  ellipse_out->major_radius = projected_radius;
  ellipse_out->minor_radius = projected_radius;
  ellipse_out->angle = 0;
  return 0;
}
