
#include <stdio.h>
#include <stdint.h>
#include "Pye3d.h"
#include "FixPoint.h"
#include "FixPoint_Adapter.h"
#include "FixPoint_Numpy.h"
#include "Projections.h"

int Line_Init_3D(struct Line *line, int32_t origin[3], int32_t dirctions[3])
{
    int32_t norm;

    norm = np_sqrt_fake(FADD3(FPOW2(dirctions[0]), FPOW2(dirctions[1]), FPOW2(dirctions[2])));

    line->origin[0] = origin[0];
    line->origin[1] = origin[1];
    line->origin[2] = origin[2];

    line->direction[0] = FDIV2(dirctions[0], norm);
    line->direction[1] = FDIV2(dirctions[1], norm);
    line->direction[2] = FDIV2(dirctions[2], norm);

    line->dim = 3;

    return 0;
}

int Line_Init_2D(struct Line *line, int32_t origin[2], int32_t dirctions[2])
{
    int32_t norm;

    norm = np_sqrt_fake(FADD2(FPOW2(dirctions[0]), FPOW2(dirctions[1])));

    line->origin[0] = origin[0];
    line->origin[1] = origin[1];

    line->direction[0] = FDIV2(dirctions[0], norm);
    line->direction[1] = FDIV2(dirctions[1], norm);

    line->dim = 2;

    return 0;
}