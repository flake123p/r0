
#ifndef _PYE3D_H_INCLUDED_

#include "FixPoint.h"
#include "Utility.h"

/*
PYTHON ORIGINAL:

class CameraModel(NamedTuple):
    focal_length: float
    resolution: Tuple[float, float]
*/
struct CameraModel
{
    /* data */
    int32_t focal_length;
    int32_t resolution[2];
};

/*
PYTHON ORIGINAL:

class TwoSphereModel(TwoSphereModelAbstract):
    def __init__(
        self,
        camera: CameraModel,
        storage_cls: T.Type[ObservationStorage] = None,
        storage_kwargs: T.Dict = None,
    ):
*/
struct TwoSphereModel
{
    struct CameraModel camera;
    DLList_Head_t storage;

    int32_t manual_model_confi_thres;

    int32_t sphere_center_2d[2]; // from: _proj_sphere_center(), final stage result
    int32_t sphere_center_3d[3];
};

struct Ellipse
{
    int32_t center[2];
    int32_t major_radius;
    int32_t minor_radius;
    int32_t angle;
};

struct Circle
{
    int32_t center[3];
    int32_t normal[3];
    int32_t radius;
};

struct Line
{
    int32_t origin[3]; // max is 3 dim for now
    int32_t direction[3];
    int32_t dim;
};

struct Sphere
{
    fix32 center[3];
    fix32 radius;
};

struct Observation
{
    DLList_Entry_t entry;
    struct Ellipse ellipse;
    int32_t confidence;
    int32_t timestamp;
    int32_t focal_length;
    // --- 2nd part ---  for parse
    int invalid;
    int32_t aux_2d[6]; // 2 x 3
    int32_t aux_3d[24]; // 2 x 3 x 4
    int32_t gaze_2d_line[4];

    struct Circle circle_3d_pair[2];
};

int TwoSphereModel_Init(struct TwoSphereModel *model, struct CameraModel *camera, int32_t manual_model_confi_thres);
int TwoSphereModel_AddObservation(struct TwoSphereModel *model, struct Observation *observation);
int TwoSphereModel_ProjSphereCenter(struct TwoSphereModel *model);

int CameraModel_Dump(struct CameraModel *camera);
int TwoSphereModel_Dump(struct TwoSphereModel *model);

void Pye3d_Test(void);

// --- Observation ---
#define _EYE_RADIUS_DEFAULT 0x000A646E
int Observation_Parse(struct Observation *obs);
int get_Dierkes_line(struct Observation *obs, int i, struct Line *line_out);

int Line_Init_3D(struct Line *line, int32_t origin[3], int32_t dirctions[3]);
int Line_Init_2D(struct Line *line, int32_t origin[2], int32_t dirctions[2]);



#define _PYE3D_H_INCLUDED_
#endif//_PYE3D_H_INCLUDED_
