
#include <stdio.h>
#include <stdint.h>
#include "Pye3d.h"
#include "FixPoint.h"
#include "FixPoint_Adapter.h"
#include "Projections.h"

// Observation __init__(), parse aux_2d, aux_3d ...
int Observation_Parse(struct Observation *obs)
{
  int ret, i, j;
  struct Line line_gaze_3d, line_gaze_2d, dLine;
  fix32 v_x_vT;

  obs->invalid = 1;

  //int unproject_ellipse(struct Ellipse *ellipse, int32_t focal_length, int32_t radius, struct Circle circles_out[2])
  ret = unproject_ellipse(&obs->ellipse, obs->focal_length, FIX_ENC_32(1), obs->circle_3d_pair);
  if (ret)
    return ret;

  obs->invalid = 0;

  Line_Init_3D(&line_gaze_3d, obs->circle_3d_pair[0].center, obs->circle_3d_pair[0].normal);

  project_line_into_image_plane(&line_gaze_3d, obs->focal_length, &line_gaze_2d);
  obs->gaze_2d_line[0] = line_gaze_2d.origin[0];
  obs->gaze_2d_line[1] = line_gaze_2d.origin[1];
  obs->gaze_2d_line[2] = line_gaze_2d.direction[0];
  obs->gaze_2d_line[3] = line_gaze_2d.direction[1];

  /*
    self.aux_2d = np.empty((2, 3))
    v = np.reshape(self.gaze_2d.direction, (2, 1))
    self.aux_2d[:, :2] = np.eye(2) - v @ v.T
    self.aux_2d[:, 2] = (np.eye(2) - v @ v.T) @ self.gaze_2d.origin
  */
  v_x_vT = F_V_X_VT2(line_gaze_2d.direction[0], line_gaze_2d.direction[1]);
  obs->aux_2d[0] = FSUB2( FIX_ENC_32(1), v_x_vT);
  obs->aux_2d[1] = -v_x_vT;
  obs->aux_2d[3] = -v_x_vT;
  obs->aux_2d[4] = FSUB2( FIX_ENC_32(1), v_x_vT);
  obs->aux_2d[2] = FDOT1221(obs->aux_2d[0], obs->aux_2d[1], line_gaze_2d.origin[0], line_gaze_2d.origin[1]);
  obs->aux_2d[5] = FDOT1221(obs->aux_2d[3], obs->aux_2d[4], line_gaze_2d.origin[0], line_gaze_2d.origin[1]);

  /*
    self.aux_3d = np.empty((2, 3, 4))
    for i in range(2):
        Dierkes_line = self.get_Dierkes_line(i)
        v = np.reshape(Dierkes_line.direction, (3, 1))
        self.aux_3d[i, :3, :3] = np.eye(3) - v @ v.T
        self.aux_3d[i, :3, 3] = (np.eye(3) - v @ v.T) @ Dierkes_line.origin
  */
  for (i = 0; i < 2; i++) {
    get_Dierkes_line(obs, i, &dLine);
    v_x_vT = F_V_X_VT3(dLine.direction[0], dLine.direction[1], dLine.direction[2]);
    j = i * 12;
    obs->aux_3d[j+0] = FSUB2( FIX_ENC_32(1), v_x_vT);
    obs->aux_3d[j+1] = -v_x_vT;
    obs->aux_3d[j+2] = -v_x_vT;
    obs->aux_3d[j+0+4] = -v_x_vT;
    obs->aux_3d[j+1+4] = FSUB2( FIX_ENC_32(1), v_x_vT);
    obs->aux_3d[j+2+4] = -v_x_vT;
    obs->aux_3d[j+0+8] = -v_x_vT;
    obs->aux_3d[j+1+8] = -v_x_vT;
    obs->aux_3d[j+2+8] = FSUB2( FIX_ENC_32(1), v_x_vT);
    obs->aux_3d[j+3] = FDOT1331(obs->aux_3d[j+0], obs->aux_3d[j+1], obs->aux_3d[j+2], dLine.origin[0], dLine.origin[1], dLine.origin[2]);
    obs->aux_3d[j+3+4] = FDOT1331(obs->aux_3d[j+0+4], obs->aux_3d[j+1+4], obs->aux_3d[j+2+4], dLine.origin[0], dLine.origin[1], dLine.origin[2]);
    obs->aux_3d[j+3+8] = FDOT1331(obs->aux_3d[j+0+8], obs->aux_3d[j+1+8], obs->aux_3d[j+2+8], dLine.origin[0], dLine.origin[1], dLine.origin[2]);
  }

  return 0;
}

/*
    def get_Dierkes_line(self, i):
        origin = (
            self.circle_3d_pair[i].center
            - _EYE_RADIUS_DEFAULT * self.circle_3d_pair[i].normal
        )
        direction = self.circle_3d_pair[i].center
        return Line(origin, direction)
*/
int get_Dierkes_line(struct Observation *obs, int i, struct Line *line_out)
{
#define ORIGIN_MAKER(j) line_out->origin[j] = (obs->circle_3d_pair[i].center[j], FMUL2(_EYE_RADIUS_DEFAULT, obs->circle_3d_pair[i].normal[j]));
  ORIGIN_MAKER(0);
  ORIGIN_MAKER(1);
  ORIGIN_MAKER(1);
#undef ORIGIN_MAKER

#define DIRECTION_MAKER(j) line_out->direction[j] = obs->circle_3d_pair[i].center[j]
  DIRECTION_MAKER(0);
  DIRECTION_MAKER(1);
  DIRECTION_MAKER(2);
#undef DIRECTION_MAKER
  line_out->dim = 3;
  return 0;
}