// A simple program that computes the square root of a number
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include "FixPoint.h"
#include "FixPoint_Adapter.h"
#include "FixPoint_Numpy.h"
#include "FixPoint_NumpyTester.hpp"
#include "Pye3d.h"

double g_data_in[][8] = {
  { 26.01351356661931, -2.118252432409747, 18.17567646353521, 13.676828015698586, 1.2587188024032447, 1.0, 139670.011208376, 1000 },
  { 24.227992272105084, -0.3126316179566828, 18.029316486370703, 13.941795918625582, 1.2942323368878117, 0.99, 139670.043216733, 1000 },
  { 22.86552383519185, 0.7433834258130716, 18.396497170552422, 14.456100644327094, 1.3279270521292028, 0.9847344756126404, 139670.079202779, 1000 },
  { 20.71425074104029, 2.792108624071332, 17.93087476054879, 14.475765703262534, 1.3639323082666852, 0.99, 139670.111125172, 1000 },
  { 19.245054090274834, 3.8664442509954284, 18.051586357276744, 14.813461034036964, 1.392622836037332, 0.900653600692749, 139670.147226008, 1000 },
  { 16.826791487270924, 5.685398809784189, 17.745821045549224, 14.908351284952511, 1.4329117081468215, 0.99, 139670.179234241, 1000 },
  { 15.193070967164545, 6.73649440609114, 17.972955911730207, 15.381310028603641, 1.4589265192847485, 0.8706532120704651, 139670.215207639, 1000 },
  { 12.967779217750916, 8.543910696251345, 17.624333215959584, 15.337223670301423, 1.5067892876331563, 0.99, 139670.247131154, 1000 },
  { 11.30127808186289, 9.45044707256136, 18.000859669489124, 15.85867728908385, 1.5261454758056383, 0.717686653137207, 139670.283218611, 1000 },
  { 9.082530007975436, 11.049677580845412, 17.509540324489326, 15.64131700758982, 1.5623428786934868, 0.99, 139670.315221214, 1000 },
};

double g_data_out[][5] = {
  {53.90153601891723, 72.4291102438727, 162.33479224434203, 162.33479224434203, 0.0}
};

struct Observation g_obs_in[10];
struct Ellipse g_elli_out[1];

void ParseObservation(double from[][8], struct Observation *to, int len)
{
  for (int i = 0; i < len; i++) {
    to[i].ellipse.center[0] = FLOAT_TO_FIX_32(from[i][0]);
    to[i].ellipse.center[1] = FLOAT_TO_FIX_32(from[i][1]);
    to[i].ellipse.major_radius = FLOAT_TO_FIX_32(from[i][2]);
    to[i].ellipse.minor_radius = FLOAT_TO_FIX_32(from[i][3]);
    to[i].ellipse.angle = FLOAT_TO_FIX_32(from[i][4]);
    to[i].confidence = FLOAT_TO_FIX_32(from[i][5]);
    //printf(">> %f, %f\n", FIX_TO_FLOAT_32(to[i].confidence), from[i][5]);
    to[i].timestamp = FLOAT_TO_FIX_32(from[i][6]);
    to[i].focal_length = FLOAT_TO_FIX_32(from[i][7]);
  }
}

void ParseEllipse(double from[][5], struct Ellipse *to, int len)
{
  for (int i = 0; i < len; i++) {
    to[i].center[0] = FLOAT_TO_FIX_32(from[i][0]);
    to[i].center[1] = FLOAT_TO_FIX_32(from[i][1]);
    to[i].major_radius = FLOAT_TO_FIX_32(from[i][2]);
    to[i].minor_radius = FLOAT_TO_FIX_32(from[i][3]);
    to[i].angle = FLOAT_TO_FIX_32(from[i][4]);
  }
}

void Test_00_ManualModel(void)
{

}

int main(int argc, char* argv[])
{
  printf("%f %f\n", g_data_in[0][0], g_data_in[1][0]);
  {
    //int32_t np_sin(int32_t fix_point_deg)
    int32_t in = float_to_fix32(45.0, PRECISION);
    int32_t out = np_sin(in);
    printf("0x%08X\n", out);
    printf("%.12f\n", fix32_to_float(out, PRECISION));
  }
  printf("Observation Make...\n");
  ParseObservation(g_data_in, g_obs_in, 10);
  ParseEllipse(g_data_out, g_elli_out, 1);

  struct CameraModel cam = {
    int32_to_fix32(1000, PRECISION),
    {
      int32_to_fix32(160, PRECISION),
      int32_to_fix32(120, PRECISION),
    }
  };
  struct TwoSphereModel model;
  printf("TwoSphereModel_Init...\n");
  TwoSphereModel_Init(&model, &cam, FLOAT_TO_FIX_32(0.9));
  
  printf("TwoSphereModel_AddObservation...\n");
  for (int i = 0; i < 10; i++) {
    TwoSphereModel_AddObservation(&model, &g_obs_in[i]);
  }

  printf("TwoSphereModel_Dump...\n");
  TwoSphereModel_Dump(&model);

  // !!!
  //TwoSphereModel_ProjSphereCenter(&model);

  printf("Verification start...\n");
  if (model.sphere_center_2d[0] != g_elli_out[0].center[0] || model.sphere_center_2d[1] != g_elli_out[0].center[1]) {
    printf("[[[ Verification Error: ]]]\n");
    printf("model.sphere_center_2d[0] = %f (0x%08X)\n", FIX_TO_FLOAT_32(model.sphere_center_2d[0]), model.sphere_center_2d[0]);
    printf("model.sphere_center_2d[1] = %f (0x%08X)\n", FIX_TO_FLOAT_32(model.sphere_center_2d[1]), model.sphere_center_2d[1]);
    printf("g_elli_out[0].center[0] = %f (0x%08X)\n", FIX_TO_FLOAT_32(g_elli_out[0].center[0]), g_elli_out[0].center[0]);
    printf("g_elli_out[0].center[1] = %f (0x%08X)\n", FIX_TO_FLOAT_32(g_elli_out[0].center[1]), g_elli_out[0].center[1]);
  }


  {
    np_func_test();
  }

  {
    double erd = 10.392304845413264;
    int32_t out;
    printf("_EYE_RADIUS_DEFAULT = 0x%08X\n", float_to_fix32(erd, 16));

    printf("np_pi = 0x%08X\n", float_to_fix32(3.1415926, 16));

    //printf("%ld\n", sizeof(Observation::aux_2d));
  }
  {
    FixPoint_NumpyTest();
  }
  return 0;
}
