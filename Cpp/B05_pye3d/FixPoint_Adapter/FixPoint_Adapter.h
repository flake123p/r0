


#ifndef _FIX_POINT_ADAPTER_H_INCLUDED_

#include <stdio.h>
#include <stdint.h>
#include "Utility.h"

int32_t float_to_fix32(double in, int precision);
double fix32_to_float(int32_t in, int precision);
int32_t int32_to_fix32(int32_t in, int precision);
int32_t fix32_to_int32(int32_t in, int precision);

void float_to_fix32_ary(int32_t *dst, double *src, size_t len, int precision);
void fix32_to_float_ary(double *dst, int32_t *src, size_t len, int precision);
void int32_to_fix32_ary(int32_t *dst, int32_t *src, size_t len, int precision);
void fix32_to_int32_ary(int32_t *dst, int32_t *src, size_t len, int precision);

//External symbol: PRECISION
#define FLOAT_TO_FIX_32(a) float_to_fix32((a),PRECISION)
#define FIX_TO_FLOAT_32(a) fix32_to_float((a),PRECISION)
#define INT_TO_FIX_32(a) int32_to_fix32((a),PRECISION)
#define FIX_TO_INT_32(a) fix32_to_int32((a),PRECISION)

#define FLOAT_TO_FIX_32_ARY(d,s,l) float_to_fix32_ary((d),(s),(l),PRECISION)
#define FIX_TO_FLOAT_32_ARY(d,s,l) fix32_to_float_ary((d),(s),(l),PRECISION)
#define INT_TO_FIX_32_ARY(d,s,l) int32_to_fix32_ary((d),(s),(l),PRECISION)
#define FIX_TO_INT_32_ARY(d,s,l) fix32_to_int32_ary((d),(s),(l),PRECISION)

#define _FIX_POINT_ADAPTER_H_INCLUDED_
#endif//_FIX_POINT_ADAPTER_H_INCLUDED_
