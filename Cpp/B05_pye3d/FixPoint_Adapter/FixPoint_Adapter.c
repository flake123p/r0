#include <stdio.h>
#include <stdint.h>
#include "FixPoint_Adapter.h"

int32_t float_to_fix32(double in, int precision)
{
    int32_t amptide = 1 << precision;
    return (int32_t)(in * amptide);
};

double fix32_to_float(int32_t in, int precision)
{
    return ((double)in/(1<<precision));
}

int32_t int32_to_fix32(int32_t in, int precision)
{
    return in << precision;
};

int32_t fix32_to_int32(int32_t in, int precision)
{
    return in >> precision;
}

void float_to_fix32_ary(int32_t *dst, double *src, size_t len, int precision)
{
    for (size_t i = 0; i < len; i ++) {
        dst[i] = float_to_fix32(src[i], precision);
    }
}

void fix32_to_float_ary(double *dst, int32_t *src, size_t len, int precision)
{
    for (size_t i = 0; i < len; i ++) {
        dst[i] = fix32_to_float(src[i], precision);
    }
}

void int32_to_fix32_ary(int32_t *dst, int32_t *src, size_t len, int precision)
{
    for (size_t i = 0; i < len; i ++) {
        dst[i] = int32_to_fix32(src[i], precision);
    }
}

void fix32_to_int32_ary(int32_t *dst, int32_t *src, size_t len, int precision)
{
    for (size_t i = 0; i < len; i ++) {
        dst[i] = fix32_to_int32(src[i], precision);
    }
}
