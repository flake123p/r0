
#include <math.h>
#include "FixPoint.h"
#include "FixPoint_Adapter.h"
#include "FixPoint_Numpy.h"
#include "FixPoint_NumpyTester.hpp"
#include "Utility.h"
#include <algorithm>
#include <functional>
#include <time.h>

template<typename DATATYPE, typename MAKER, typename RUNNER>
class Looper {
public:
    MAKER maker;
    RUNNER runner;
    void go() {
        DATATYPE data;
        int i = 0;
        while (1){
            if (maker(i, &data)) {
                break;
            } else {
                if (runner(i, &data))
                    break;
            }
            i += 1;
        }
    };
};

typedef std::function<int (int index, double *data)> Fp;

Fp DataMaker(double *a, int len) {
    return [a,len](int index, double *data)->int {
            if (index >= len) {
                return 1;
            }
            *data = a[index];
            return 0;
        };
}
Fp DataRunner_1_on_1(
    int32_t (*runner0)(int32_t), 
    double (*runner1)(double),
    std::function<int (int32_t a, double b)> checker,
    std::function<void (int32_t result)> logger
    ) {
    return [=](int index, double *data)->int {
            int32_t rc0 = runner0(FLOAT_TO_FIX_32(*data));
            double rc1 = runner1(*data);
            int result = checker(rc0,rc1);
            logger(result);
            return 0;
        };
}


// Test code 
double varA[] = {0.1, 0.3, 0.5, 0.7, 0.9};
int varA_len = 5;
double varB[] = {1, 3, 5, 7, 9, 100, 300, 500, 700, 900};
int varB_len = 10;
int precision = PRECISION;
double precision_margin = 1.0/(1<<PRECISION);


int Verifier_1_on_1(int32_t a, double b)
{
    double pmargin = precision_margin;
    double aD = FIX_TO_FLOAT_32(a);
    //double aCeil = FIX_TO_FLOAT_32(a+1);
    //double aFloor = FIX_TO_FLOAT_32(a-1);
    double aCeil = aD + pmargin;
    double aFloor = aD - pmargin;
    double diff = abs(aD-b);
    double diffp = diff*100/precision_margin;
    
    int resultInRange = 0;
    if ((b >= aFloor) && (b <= aCeil))
        resultInRange = 1;
    printf("%.9f / %.9f / %.9f [%d] --", aCeil, b, aFloor, resultInRange);

    pmargin *= 2;
    double diffm = diff-pmargin;
    printf("%.9f, %.9f, diff=%.9f(%f), m=%.9f, %d, [%d] %.9f\n", 
        aD, 
        b, 
        diff, diffp,
        pmargin,
        aD==b, 
        diff<=pmargin, 
        diffm);
    return resultInRange;
}

void np_func_test()
{
    class Looper<double, Fp, Fp> looper;
    looper.maker = DataMaker(varA, varA_len);
    int pass = 1;
    auto logger = [&pass](int res){if(res==0)pass=res;};
    
    pass = 1;
    looper.runner = DataRunner_1_on_1(np_sin_fake, sin, Verifier_1_on_1, logger);
    looper.go();
    printf("Verify: sin_fake(): pass = %d\n", pass);

    pass = 1;
    looper.runner = DataRunner_1_on_1(np_sin, sin, Verifier_1_on_1, logger);
    looper.go();
    printf("Verify: sin(): pass = %d\n", pass);

    pass = 1;
    looper.runner = DataRunner_1_on_1(np_cos_fake, cos, Verifier_1_on_1, logger);
    looper.go();
    printf("Verify: cos_fake(): pass = %d\n", pass);

    pass = 1;
    looper.runner = DataRunner_1_on_1(np_cos, cos, Verifier_1_on_1, logger);
    looper.go();
    printf("Verify: cos(): pass = %d\n", pass);
    
    pass = 1;
    looper.maker = DataMaker(varB, varB_len);
    looper.runner = DataRunner_1_on_1(np_sqrt_fake, sqrt, Verifier_1_on_1, logger);
    looper.go();
    printf("Verify: sqrt(): pass = %d\n", pass);

    pass = 1;
    looper.maker = DataMaker(varB, varB_len);
    looper.runner = DataRunner_1_on_1(np_sqrt, sqrt, Verifier_1_on_1, logger);
    looper.go();
    printf("Verify: sqrt(): pass = %d\n", pass);
}

void FixPoint_NumpyShapeDump(int32_t *shape, int32_t shape_length)
{
    int32_t ctr;
    printf("shape(%d) = [ ", shape_length);
    for (ctr = 0; ctr < shape_length; ctr++) {
        printf("%d ", shape[ctr]);
    }
    printf("]\n");
}

void FixPoint_NumpyArrayDump(int32_t *array, int32_t *shape, int32_t *shape_length)
{
    int32_t len = np_array_len(shape, shape_length);
    printf("[ ");
    for (int32_t i = 0; i < *shape_length; i++) {
        printf("%d ", shape[i]);
    }
    printf("] ");
    //printf("%d\n", len);
    for (int32_t i = 0; i < len; i++) {
        printf("%.3f ", FIX_TO_FLOAT_32(array[i]));
    }
    printf("\n");
}

void FixPoint_NumpyArrayLiteDump(struct NP_Array_Lite *a)
{
    int32_t *array = a->array;
    int32_t *shape = a->shape;
    int32_t *shape_length = &a->shape_len;
    int32_t len = np_array_len(shape, shape_length);
    printf("[ ");
    for (int32_t i = 0; i < *shape_length; i++) {
        printf("%d ", shape[i]);
    }
    printf("] ");
    //printf("%d\n", len);
    for (int32_t i = 0; i < len; i++) {
        printf("%.3f ", FIX_TO_FLOAT_32(array[i]));
    }
    printf("\n");
}

void FixPoint_NumpyArrayLiteDump(struct NP_Array_Lite2 *a)
{
    int32_t *array = a->array;
    int32_t *shape = a->shape;
    int32_t *shape_length = &a->shape_len;
    int32_t len = np_array_len(shape, shape_length);
    printf("[ ");
    for (int32_t i = 0; i < *shape_length; i++) {
        printf("%d ", shape[i]);
    }
    printf("] ");
    //printf("%d\n", len);
    for (int32_t i = 0; i < len; i++) {
        printf("%.3f ", FIX_TO_FLOAT_32(array[i]));
    }
    printf("\n");
}

void call_np_pinv(){

    int32_t a[9] = {
        INT_TO_FIX_32(9), 
        INT_TO_FIX_32(1),
        INT_TO_FIX_32(2), 
        INT_TO_FIX_32(3),
        INT_TO_FIX_32(4),
        INT_TO_FIX_32(5),
        INT_TO_FIX_32(6),
        INT_TO_FIX_32(7),
        INT_TO_FIX_32(8),
        };
    int32_t b[9];
    struct NP_Array_Lite2 in, out;
    in.array = a;
    in.shape_len = 2;
    in.shape[0] = 3;
    in.shape[1] = 3;
    out.array = b;

    np_pinv(ary_arg(in), ary_arg(out));

	//array_out = np_pinv_22(A, array_shape, shape_length, array_out, array_shape_out, &shape_length_out);
    //printf("%d %d\n", array_shape_out[0], array_shape_out[1]);
    printf("%s(): start\n", __func__);
    FixPoint_NumpyArrayLiteDump(&in);
    FixPoint_NumpyArrayLiteDump(&out);
    printf("%s(): end\n", __func__);
}

void call_np_pinv_22(){
/*
    int32_t A[M][N] = { {5,  -2,  2,  7,  9,  8,  0},
                       	{ 1,  0,  0,  3,  1,  0,  9},
                       	{-3,  1,  5,  0,  2,  1,  7},
                       	{ 3, -1, -9,  4,  6,  5,  2},
                       	{ 1,  0,  4,  4,  1,  0,  9},
                       	{ 8,  0,  3,  8,  6,  5,  2},
                  	   	{ 5,  6,  4,  1,  3,  2,  0}};
*/
    int32_t A[2][2] = {{INT_TO_FIX_32(9), INT_TO_FIX_32(1)},
                       {INT_TO_FIX_32(2), INT_TO_FIX_32(3)}};

    struct NP_Array_Lite2 in, out;
    int32_t outbuf[2*2];
    out.array = outbuf;
    in.array = (int32_t *)A;
    in.shape_len = 2;
    in.shape[0] = 2;
    in.shape[1] = 2;

    np_pinv_22(A, in.shape, &in.shape_len , ary_arg(out));
    //np_pinv(ary_arg(in), ary_arg(out));
    //printf("%d %d\n", array_shape_out[0], array_shape_out[1]);

    printf("%s(): start\n", __func__);
    FixPoint_NumpyArrayLiteDump(&in);
    FixPoint_NumpyArrayLiteDump(&out);
    printf("%s(): end\n", __func__);
}

void call_np_pinv_33()
{
    int32_t A[3][3] = {{INT_TO_FIX_32(9), INT_TO_FIX_32(1), INT_TO_FIX_32(2)},
                       {INT_TO_FIX_32(3), INT_TO_FIX_32(4), INT_TO_FIX_32(5)},
                       {INT_TO_FIX_32(6), INT_TO_FIX_32(7), INT_TO_FIX_32(8)},};

    struct NP_Array_Lite2 in, out;
    int32_t outbuf[3*3];
    out.array = outbuf;
    in.array = (int32_t *)A;
    in.shape_len = 2;
    in.shape[0] = 3;
    in.shape[1] = 3;

	np_pinv_33(A, in.shape, &in.shape_len , ary_arg(out));
    //printf("%d %d\n", array_shape_out[0], array_shape_out[1]);

    printf("%s(): start\n", __func__);
    FixPoint_NumpyArrayLiteDump(&in);
    FixPoint_NumpyArrayLiteDump(&out);
    printf("%s(): end\n", __func__);
}

void call_np_pinv_44()
{
    int32_t A[4][4] = {{INT_TO_FIX_32(9), INT_TO_FIX_32(1), INT_TO_FIX_32(2), INT_TO_FIX_32(3)},
                       {INT_TO_FIX_32(4), INT_TO_FIX_32(5), INT_TO_FIX_32(6), INT_TO_FIX_32(7)},
                       {INT_TO_FIX_32(8), INT_TO_FIX_32(9), INT_TO_FIX_32(10), INT_TO_FIX_32(11)},
                       {INT_TO_FIX_32(12), INT_TO_FIX_32(13), INT_TO_FIX_32(14), INT_TO_FIX_32(15)}};

    struct NP_Array_Lite2 in, out;
    int32_t outbuf[4*4];
    out.array = outbuf;
    in.array = (int32_t *)A;
    in.shape_len = 2;
    in.shape[0] = 4;
    in.shape[1] = 4;

	np_pinv_44(A, in.shape, &in.shape_len , ary_arg(out));
    //printf("%d %d\n", array_shape_out[0], array_shape_out[1]);

    printf("%s(): start\n", __func__);
    FixPoint_NumpyArrayLiteDump(&in);
    FixPoint_NumpyArrayLiteDump(&out);
    printf("%s(): end\n", __func__);
}

void FixPoint_NumpyTest()
{
    int32_t sh[] = {4,2,3};
    int32_t len = 3;
    int32_t sh_new[6];
    int32_t len_new;
    FixPoint_NumpyShapeDump(sh, len);
    np_array_axis_map(sh, &len, 0, sh_new, &len_new);
    FixPoint_NumpyShapeDump(sh_new, len_new);

    int32_t pos[] = {0,0,0};
    int32_t x = np_array_pos_flatten(sh, &len, pos);
    printf("pos x = %d\n", x);
    pos[2] = 1;
    x = np_array_pos_flatten(sh, &len, pos);
    printf("pos x = %d\n", x);
    pos[1] = 1;
    x = np_array_pos_flatten(sh, &len, pos);
    printf("pos x = %d\n", x);
    pos[0] = 1;
    x = np_array_pos_flatten(sh, &len, pos);
    printf("pos x = %d\n", x);
    // np_array_axis_map()

    if (0)
    {
        int32_t pos[] = {0,0,0};

        for (int i = 0; i < 24; i++) {
            np_array_increment(sh, len, pos);
            FixPoint_NumpyShapeDump(pos, len);
            x = np_array_pos_flatten(sh, &len, pos);
            printf("pos x = %d\n", x);
        }
    }
    
    if (1)
    {
        int32_t a[24];
        int32_t sh[] = {2,2,2,3};
        int32_t len = sizeof(sh)/sizeof(int32_t);
        int32_t a_out[100];
        int32_t a_sh[10];
        int32_t a_slen;

        for (int i = 0; i < 24; i++) {
            a[i] = INT_TO_FIX_32(i);
        }
        FixPoint_NumpyArrayDump(a, sh, &len);
        np_mean(a, sh, &len, NULL, a_out, a_sh, &a_slen);
        FixPoint_NumpyArrayDump(a_out, a_sh, &a_slen);
        int32_t axis = 1;
        np_mean(a, sh, &len, &axis, a_out, a_sh, &a_slen);
        FixPoint_NumpyArrayDump(a_out, a_sh, &a_slen);
    }

    {
        call_np_pinv_22();
        //extern void call_np_pinv();
        call_np_pinv_33();
        call_np_pinv_44();
        call_np_pinv();
    }

    {
        struct NP_Array_Lite2 a, b, ab;
        double aAry[] = {0,1,2,3,4,5,6,7};
        double bAry[] = {0,1,2,3,4,5,6,7,8,9,10,11};
        size_t al = sizeof(aAry)/sizeof(double);
        size_t bl = sizeof(bAry)/sizeof(double);
        int32_t abuf[al], bbuf[bl], abbuf[100];

        a.array = abuf;
        b.array = bbuf;
        ab.array = abbuf;
        FLOAT_TO_FIX_32_ARY(abuf, aAry, al);
        FLOAT_TO_FIX_32_ARY(bbuf, bAry, bl);
        a.shape_len = 2;
        a.shape[0] = 2;
        a.shape[1] = 4;
        b.shape_len = 2;
        b.shape[0] = 4;
        b.shape[1] = 3;

        np_dot(ary_arg(a), ary_arg(b), ary_arg(ab));
        FixPoint_NumpyArrayLiteDump(&a);
        FixPoint_NumpyArrayLiteDump(&b);
        FixPoint_NumpyArrayLiteDump(&ab);
    }
    {
        struct NP_Array_Lite2 in, out;
        double aAry[] = {0,1,2,3,4,5,6,7,8,9,10,11};
        int32_t inbuf[100], outbuf[100];
        FLOAT_TO_FIX_32_ARY(inbuf, aAry, sizeof(aAry)/sizeof(double));
        in.array = inbuf;
        in.shape_len = 2;
        in.shape[0] = 6;
        in.shape[1] = 2;
        out.array = outbuf;

        {
            clock_t gClock = clock();

            for (int i = 0; i < 1; i++) {
                np_transpose_2d(ary_arg(in), ary_arg(out));
            }

            gClock = clock() - gClock;
            printf ("It took me %4d clicks (%f seconds).\n", (int)gClock, ((double)gClock)/CLOCKS_PER_SEC);

        }
        
        FixPoint_NumpyArrayLiteDump(&in);
        FixPoint_NumpyArrayLiteDump(&out);
    }
}