#include <math.h> // for fake function
#include "Utility.h"
#include "FixPoint.h"
#include "FixPoint_Adapter.h"
#include "FixPoint_Numpy.h"
#define eps 1e-7

int32_t np_array_len(int32_t *array_shape, int32_t *shape_length)
{
    int32_t len = 1;
    for (int32_t i = 0; i < *shape_length; i++) {
        len = len * array_shape[i];
    }
    return len;
}

int np_array_axis_map(int32_t *shape, int32_t *shape_length, int32_t axis, int32_t *shape_new, int32_t *shape_length_new)
{
    int32_t oldCtr, newCtr;
    for (newCtr = 0, oldCtr = 0; oldCtr < *shape_length; oldCtr++) {
        if (oldCtr == axis)
            continue;
        shape_new[newCtr] = shape[oldCtr] ;
        newCtr++;
    }
    *shape_length_new = *shape_length - 1;
    return 0;
}

void np_array_get_shape_multiplier(int32_t *shape, int32_t *shape_length, int32_t *shape_mul)
{
    int32_t ctr, multiplier, temp;

    for (int32_t ctr = 0; ctr < *shape_length; ctr++) {
        shape_mul[ctr] = 1;
    }

    for (int32_t ctr = 0; ctr < *shape_length; ctr++) {
        if (ctr == 0)
            continue;
        multiplier = shape[ctr];
        for (int32_t i = 0; i < ctr; i++) {
            shape_mul[i] *= multiplier;
        }
    }
}

int32_t np_array_pos_flatten_by_mul_ary(int32_t *shape_mul, int32_t *shape_length, int32_t *pos)
{
    int32_t temp = 0;
    for (int32_t ctr = 0; ctr < *shape_length; ctr++) {
        temp += pos[ctr] * shape_mul[ctr];
    }

    return temp;
}

// return 1d pos
int32_t np_array_pos_flatten(int32_t *shape, int32_t *shape_length, int32_t *pos)
{
    int32_t shape_mul[MAX_SHAPE], ctr, multiplier, temp;

    SIMPLE_ASSERT(MAX_SHAPE >= *shape_length);

    np_array_get_shape_multiplier(shape, shape_length, shape_mul);
    return np_array_pos_flatten_by_mul_ary(shape_mul, shape_length, pos);
}

// return true for carry
int np_array_increment(int32_t *shape, int32_t length, int32_t *curr)
{
    if (length != 1) {
        int carry = np_array_increment(shape+1, length-1, curr+1);
        if (carry == 0)
            return 0;
    }
    if (*curr == *shape - 1) {
        *curr = 0;
        return 1;
    } else {
        *curr += 1;
        return 0;
    }
}

int32_t np_sin_fake(int32_t in)
{
    double float_in = FIX_TO_FLOAT_32(in);
    return FLOAT_TO_FIX_32(sin(float_in));
}

int32_t np_cos_fake(int32_t in)
{
    double float_in = FIX_TO_FLOAT_32(in);
    return FLOAT_TO_FIX_32(cos(float_in));
}

int32_t np_sqrt_fake(int32_t in)
{
    double float_in = FIX_TO_FLOAT_32(in);
    return FLOAT_TO_FIX_32(sqrt(float_in));
}

int32_t np_sqrt(int32_t n){
	int32_t i=0,j=0;
	while(fix_point_mul(i, i, 16) < n){
		i = fix_point_add(i, fix32_encode(1, 16));
	}
	int32_t left = i - fix32_encode(1, 16), right=i, mid;
	if(fix_point_mul(right, right, 16) == n){
		return right;
	}
	//while(fix_point_sub(right, left) > fix32_encode(eps, 16)){ 
    //while(fix_point_sub(right, left) > 0x00000001){
    for (int i = 0; i < 17; i++) {
		//mid=(left+right)/2;
		//mid = fix_point_div(fix_point_add(left, right), fix32_encode(2, 16), 16);
		mid = fix_point_add(left, right) >> 1;
		if(fix_point_mul(mid, mid, 16) > n){
			right=mid;
		} else {
			left=mid;
		}
	}
	return mid;
}

int32_t *np_sum(
    int32_t *array, 
    int32_t *array_shape, 
    int32_t *shape_length, 
    int32_t *axis,
    int32_t *array_out, 
    int32_t *array_shape_out, 
    int32_t *shape_length_out)
{
    int32_t i, len, temp, len_out, i_out, pos_out_shape;
    int32_t pos_in[6] = {0};
    int32_t pos_out[6] = {0};

    len = np_array_len(array_shape, shape_length);

    if (axis == NULL) {
        // all sum
        temp = 0;
        for (i = 0; i < len; i++) {
            temp = FADD2(temp, array[i]);
        }
        *shape_length_out = 0;
        array_shape_out[0] = 0;
        array_out[0] = temp;
        return 0;
    }
    // Calc output shape
    np_array_axis_map(array_shape, shape_length, *axis, array_shape_out, shape_length_out);
    // Set output array to 0 for accumulation
    len_out = np_array_len(array_shape_out, shape_length_out);
    for (i = 0; i < len_out; i++) {
        array_out[i] = 0;
    }
    // Traverse input array, and add the item into correct position
    for (i = 0; i < len; i++) {
        i_out = np_array_pos_flatten(array_shape_out, shape_length_out, pos_out);

        array_out[i_out] = FADD2(array_out[i_out], array[i]);

        //pos increment with i
        np_array_increment(array_shape, *shape_length, pos_in);
        //int np_array_axis_map(int32_t *shape, int32_t *shape_length, int32_t axis, int32_t *shape_new, int32_t *shape_length_new)
        np_array_axis_map(pos_in, shape_length, *axis, pos_out, &pos_out_shape);
    }

    return 0;
}

int32_t *np_mean(
    int32_t *array, 
    int32_t *array_shape, 
    int32_t *shape_length, 
    int32_t *axis,
    int32_t *array_out, 
    int32_t *array_shape_out, 
    int32_t *shape_length_out)
{
    int32_t i, len, temp, len_out, i_out, pos_out_shape;
    int32_t pos_in[6] = {0};
    int32_t pos_out[6] = {0};

    len = np_array_len(array_shape, shape_length);

    if (axis == NULL) {
        // all sum
        temp = 0;
        for (i = 0; i < len; i++) {
            temp = FADD2(temp, array[i]);
        }
        *shape_length_out = 0;
        array_shape_out[0] = 0;
        array_out[0] = temp/len;
        return 0;
    }
    // Calc output shape
    np_array_axis_map(array_shape, shape_length, *axis, array_shape_out, shape_length_out);
    // Set output array to 0 for accumulation
    len_out = np_array_len(array_shape_out, shape_length_out);
    for (i = 0; i < len_out; i++) {
        array_out[i] = 0;
    }
    // Traverse input array, and add the item into correct position
    for (i = 0; i < len; i++) {
        i_out = np_array_pos_flatten(array_shape_out, shape_length_out, pos_out);

        array_out[i_out] = FADD2(array_out[i_out], array[i]);

        //pos increment with i
        np_array_increment(array_shape, *shape_length, pos_in);
        //int np_array_axis_map(int32_t *shape, int32_t *shape_length, int32_t axis, int32_t *shape_new, int32_t *shape_length_new)
        np_array_axis_map(pos_in, shape_length, *axis, pos_out, &pos_out_shape);
    }

    {
        int32_t divisor = array_shape[*axis];
        for (i = 0; i < len_out; i++) {
            array_out[i] = array_out[i] / divisor;
        }
    }

    return 0;
}


// Generic function to display the matrix. We use it to display
// both adjoin and inverse. adjoin is integer matrix and inverse
// is a float.

// Function to get cofactor 
void np_getCofactor(int32_t *A, int32_t *temp, int p, int q, int n, int32_t MM, int32_t NN)
{
    int i = 0, j = 0;
 
    // Looping for each element of the matrix
    for (int row = 0; row < n; row++){
        for (int col = 0; col < n; col++){
            // Copying into temporary matrix only those element
            // which are not in given row and column
            if (row != p && col != q){
                temp[i * NN + j] = A[row * NN + col];
                j++;
                 
                // Row is filled, so increase row index and
                // reset col index
                if (j == n - 1){
                    j = 0;
                    i++;
                }
            }
        }
    }
}
 
// Recursive function for finding determinant of matrix.
int np_determinant(int32_t *A, int n, int32_t MM, int32_t NN)
{
    int D = 0; // Initialize result
 
    // Base case : if matrix contains single element
    if (n == 1)
        return A[0];
 
    int sign = 1; // To store sign multiplier
 
    // Iterate for each element of first row
    for (int f = 0; f < n; f++){
        // Getting Cofactor of A[0][f]
        int32_t temp[100]; // To store cofactors
        int32_t t;
        np_getCofactor(A, temp, 0, f, n, MM, NN);

        //t = fix_point_mul(sign, A[0 * NN + f], 16);
        t = sign * A[0 * NN + f];
        D += fix_point_mul(t, np_determinant(temp, n - 1, MM, NN), 16);
        // D += sign * A[0 * N + f] * determinant(temp, n - 1);
 
        // terms are to be added with alternate sign
        sign = -sign;
    }
 
    return D;
}
 
// Function to get adjoint
void np_adjoint(int32_t *A, int32_t *adj, int32_t MM, int32_t NN)
{
    if (NN == 1)
    {
        adj[0] = 1;
        return;
    }
 
    // temp is used to store cofactors 
    int sign = 1;
 
    for (int i = 0; i < NN; i++){
        for (int j = 0; j < NN; j++){
            // Get cofactor
            int32_t temp[100];
            np_getCofactor(A, temp, i, j, NN, MM, NN);
 
            // sign of adj positive if sum of row
            // and column indexes is even.
            sign = ((i+j)%2==0)? 1: -1;
 
            // Interchanging rows and columns to get the
            // transpose of the cofactor matrix
            adj[j * NN + i] = fix_point_mul(sign, np_determinant(temp, NN-1, MM, NN), 16);
            // adj[j * N + i] = (sign)*(determinant(temp, N-1));
        }
    }
}
 
// Function to calculate and store inverse, returns false if
// matrix is singular
int np_inverse(int32_t *A, int32_t *inverse, int32_t MM, int32_t NN)
{
    // Find determinant of A[][]
    int det = np_determinant(A, NN, MM, NN);
    //printf("\nDET = ");
	//printf("%d\n", det);
    if (det == 0)
    {
        printf("Singular matrix, can't find its inverse\n");
        return 0;
    }
 
    // Find adjoint
    //int32_t adj[NN*NN];
    int32_t adj[100];
    np_adjoint(A, adj, MM, NN);
 
    // Find Inverse using formula "inverse(A) = adj(A)/det(A)"
    for (int i=0; i<NN; i++)
        for (int j=0; j<NN; j++)
	        inverse[i * NN + j] = fix_point_div(adj[i * NN + j], det, PRECISION);
            // inverse[i * N + j] = adj[i * N + j]/ (int)(det);
 
    return 1;
}

//https://stackoverflow.com/questions/69259761/moore-penrose-pseudo-inverse
int np_pinv(
    int32_t *array, 
    int32_t *array_shape, 
    int32_t *shape_length, 
    int32_t *array_out, 
    int32_t *array_shape_out, 
    int32_t *shape_length_out)
{
    struct NP_Array_Lite2 temp;
    int32_t buf[100];
    temp.array = buf;
    struct NP_Array_Lite2 temp2;
    int32_t buf2[100];
    temp2.array = buf2;
    // int32_t *matrix        = (int32_t *)malloc(M * N * sizeof(int32_t));
    // int32_t *t_matrix      = (int32_t *)malloc(M * N * sizeof(int32_t));
    // int32_t *matrix_mult   = (int32_t *)malloc(N * N * sizeof(int32_t));
    // int32_t *pseudoinverse = (int32_t *)malloc(M * N * sizeof(int32_t));
    // int32_t *adj           = (int32_t *)malloc(M * N * sizeof(int32_t)); // To store adjoint 
    // int32_t *inv           = (int32_t *)malloc(N * N * sizeof(int32_t)); // To store inverse 
    
	// //printf("\nThe Matrix is :\n"); 
	// Trans_2D_1D(A, matrix);
 	// //display(matrix, 1); 	// M*N matrix
 	
    // //printf("\nThe Transpose is :\n");
    // Transpose(matrix, t_matrix);
    // //display(t_matrix, 2);	// N*M matrix
    np_transpose_2d(array, array_shape, shape_length, ary_arg(temp));
    #define t_matrix temp

    // //printf("\nThe product of the matrix is: \n");
    // MatrixMult(t_matrix, matrix, matrix_mult, 1); // (N*M) * (M*N) 
    // //display(matrix_mult, 0);// N*N matrix
    np_dot(ary_arg(t_matrix), array, array_shape, shape_length, array_out, array_shape_out, shape_length_out);
    #define matrix_mult array_out
 
    //printf("%d >> %d %d\n", __LINE__, array_shape_out[0], array_shape_out[1]);
    //if (inverse(matrix_mult, inv)){
    if (np_inverse(matrix_mult, temp2.array, array_shape_out[0], array_shape_out[1])){
        temp2.shape_len = 2;
        temp2.shape[0] = array_shape_out[1];
        temp2.shape[1] = array_shape_out[0];
        #define inv temp2
        
	    //printf("\nThe Inverse is :\n");
        //display(inv, 0);	// N*N matrix	
		printf("\nThe Monroe-penrose inverse is : xx\n");
        {
            extern void FixPoint_NumpyArrayLiteDump(struct NP_Array_Lite2 *a);
            FixPoint_NumpyArrayLiteDump(&inv);
            FixPoint_NumpyArrayLiteDump(&t_matrix);
        }
		//MatrixMult(inv,t_matrix, pseudoinverse, 2); // (N*N) * (N*M)
		//display(pseudoinverse, 2); // N*M matrix	
        np_dot(ary_arg(inv), ary_arg(t_matrix), array_out, array_shape_out, shape_length_out);
 	}

    // memcpy(array_out, pseudoinverse, N*M*sizeof(int32_t));
	// array_shape_out[0] = N;
	// array_shape_out[1] = M;
	// *shape_length_out = 2;

    // free(matrix);
    // free(t_matrix);
    // free(matrix_mult);
    // free(pseudoinverse);
    // free(adj);
    // free(inv);
 	// return pseudoinverse;
    return 0;
}

int np_dot(
    int32_t *array, 
    int32_t *array_shape, 
    int32_t *shape_length,
    int32_t *array2, 
    int32_t *array_shape2, 
    int32_t *shape_length2,
    int32_t *array_out, 
    int32_t *array_shape_out, 
    int32_t *shape_length_out)
{
    int32_t row = array_shape[0];
    int32_t col = array_shape[1];
    int32_t row2 = array_shape2[0];
    int32_t col2 = array_shape2[1];
    for (int i = 0; i < row; i++) {
        for (int j = 0; j < col2; j++) {
            array_out[i * col2 + j] = 0;
            
            for (int k = 0; k < col; k++) {
                array_out[i * col2 + j] += fix_point_mul(array[i * col + k], array2[k * col2 + j], PRECISION);
            }
        }
    }
    *shape_length_out = 2;
    array_shape_out[0] = row;
    array_shape_out[1] = col2;
    return 0;
}

int32_t fix_point_power(int32_t fix_point_base, int32_t n) {
    if(n < 0) {
        if(fix_point_base == 0)
            return -0; 
        return fix_point_div(1 , (fix_point_mul(fix_point_base , fix_point_power(fix_point_base, (-n) - 1),16)),16);
    }
    if(n == 0)
        return int32_to_fix32(1,16);
    if(n == 1)
        return fix_point_base;
    return fix_point_mul(fix_point_base , fix_point_power(fix_point_base, n - 1),16);
}

int factorial(int n) {
    return n <= 0 ? 1 : n * factorial(n-1);   
}

#define TERMS 7
int32_t np_sin(int32_t fix_point_deg) {
    //fix_point_deg %= int32_to_fix32(360,16);
    //int32_t rad = fix_point_div(fix_point_mul(fix_point_deg , NP_PI ,16) , int32_to_fix32(180,16),16);
    int32_t rad = fix_point_deg;
    int32_t sin = 0;

    int i;
    for(i = 0; i < TERMS; i++) { 
        sin += fix_point_div(fix_point_mul(fix_point_power(int32_to_fix32(-1,16), i) , fix_point_power(rad, 2 * i + 1), 16) , int32_to_fix32(factorial(2 * i + 1),16), 16);
    }
    return sin;
}

int32_t np_cos(int32_t fix_point_deg) {
    //fix_point_deg %= int32_to_fix32(360,16);
    //int32_t rad = fix_point_div(fix_point_mul(fix_point_deg , NP_PI ,16) , int32_to_fix32(180,16),16);
    int32_t rad = fix_point_deg;
    int32_t cos = int32_to_fix32(0,16);

    int i;
    for(i = 0; i < TERMS; i++) { 
        cos += fix_point_div(fix_point_mul(fix_point_power(int32_to_fix32(-1,16), i) , fix_point_power(rad, 2 * i), 16) , int32_to_fix32(factorial(2 * i    ),16), 16);
    }
    return cos;
}

void np_transpose_2d( 
    int32_t *array, 
    int32_t *array_shape, 
    int32_t *shape_len,
    int32_t *array_out,
    int32_t *array_shape_out,
    int32_t *shape_len_out)
{
    int32_t i, j;
    for (i = 0; i < array_shape[1]; i++)
    {
        for (j = 0; j < array_shape[0]; j++)
        {
            //*array_out = array[ i + j * array_shape[1] ]; 
            *array_out = *(&array[i]+j* *(array_shape+1));
            array_out++;
        }
    }
    array_shape_out[0] = *(array_shape+1);
    array_shape_out[1] = *(array_shape);
    *shape_len_out = 2;
}
