#include <math.h> // for fake function
#include "Utility.h"
#include "FixPoint.h"
#include "FixPoint_Adapter.h"
#include "FixPoint_Numpy.h"

#define M 3
#define N 3

// #define M 3
// #define N 2
static int precision = 16;

//static int array_shape[] = {7, 7};

static void Trans_2D_1D(int32_t matrix_2D[M][N], int32_t *matrix) {
 
    for (int i = 0; i < M; i++){
        for (int j = 0; j < N; j++){
            matrix[i * N + j] = matrix_2D[i][j];
        }
    }
    
     return;
 
}
 
// Generic function to display the matrix. We use it to display
// both adjoin and inverse. adjoin is integer matrix and inverse
// is a float.

static void display(int32_t *A, int mode){
	if (mode == 1){ // display M*N matrix
		for (int i=0; i < M; i++){
		    for (int j=0; j < N; j++){
		        //printf("%d\t", A[i * N + j]);
                printf("%f\t", FIX_TO_FLOAT_32(A[i * N + j]));
		    }
		    printf("\n");
		}
	}
	else if (mode == 2){ // display N*M matrix
		for (int i=0; i < N; i++){
				for (int j=0; j < M; j++){
				    //printf("%d\t", A[i * M + j]);
                    printf("%f\t", FIX_TO_FLOAT_32(A[i * N + j]));
				}
				printf("\n");
			}
	}
	else{ // display N*N matrix
		for (int i=0; i < N; i++){
				for (int j=0; j < N; j++){
				    //printf("%d\t", A[i * N + j]);
                    printf("%f\t", FIX_TO_FLOAT_32(A[i * N + j]));
				}
				printf("\n");
			}
	}

} 

static void Transpose(int32_t *matrix, int32_t *t_matrix) {
 
    for (int i = 0; i < M; i++){
        for (int j = 0; j < N; j++){
             t_matrix[j * M + i] = matrix[i * N + j];
        }
    }
    return;
}

static void MatrixMult(int32_t *matrix_1, int32_t *matrix_2, int32_t *matrix_product, int mode) {
    int k;
    if (mode == 1){ // (N*M) * (M*N)
		for (int i = 0; i < N; i++) {
		    for (int j = 0; j < N; j++) {             // not j<M
		        matrix_product[i * N + j] = 0;
		        
		        for (k = 0; k < M; k++) {
		            // matrix_product[i * N + j] += matrix_1[i * M + k] * matrix_2[k * N + j];
					matrix_product[i * N + j] += fix_point_mul(matrix_1[i * N + k], matrix_2[k * N + j], precision);
		        }
		    }
		}
	}
	else{ // (N*N) * (N*M)
		for (int i = 0; i < N; i++) {
		    for (int j = 0; j < M; j++) {             // not j<M
		        matrix_product[i * N + j] = 0;
		        
		        for (k = 0; k < N; k++) {
		            // matrix_product[i * N + j] += matrix_1[i * N + k] * matrix_2[k * N + j];
		            matrix_product[i * N + j] += fix_point_mul(matrix_1[i * N + k], matrix_2[k * N + j], precision);
		        }
		    }
		}
	}
    return;
}

// Function to get cofactor 
static void getCofactor(int32_t *A, int32_t *temp, int p, int q, int n)
{
    int i = 0, j = 0;
 
    // Looping for each element of the matrix
    for (int row = 0; row < n; row++){
        for (int col = 0; col < n; col++){
            // Copying into temporary matrix only those element
            // which are not in given row and column
            if (row != p && col != q){
                temp[i * N + j] = A[row * N + col];
                j++;
                 
                // Row is filled, so increase row index and
                // reset col index
                if (j == n - 1){
                    j = 0;
                    i++;
                }
            }
        }
    }
}
 
// Recursive function for finding determinant of matrix.
static int determinant(int32_t *A, int n)
{
    int D = 0; // Initialize result
 
    // Base case : if matrix contains single element
    if (n == 1)
        return A[0];
 
    int sign = 1; // To store sign multiplier
 
    // Iterate for each element of first row
    for (int f = 0; f < n; f++){
        // Getting Cofactor of A[0][f]
        int32_t temp[N * N]; // To store cofactors
        int32_t t;
        getCofactor(A, temp, 0, f, n);

        t = fix_point_mul(sign, A[0 * N + f], 16);
        D += fix_point_mul(t, determinant(temp, n - 1), 16);
        // D += sign * A[0 * N + f] * determinant(temp, n - 1);
 
        // terms are to be added with alternate sign
        sign = -sign;
    }
 
    return D;
}
 
// Function to get adjoint
static void adjoint(int32_t *A, int32_t *adj)
{
    if (N == 1)
    {
        adj[0] = 1;
        return;
    }
 
    // temp is used to store cofactors 
    int sign = 1;
 
    for (int i = 0; i < N; i++){
        for (int j = 0; j < N; j++){
            // Get cofactor
            int32_t temp[N*N];
            getCofactor(A, temp, i, j, N);
 
            // sign of adj positive if sum of row
            // and column indexes is even.
            sign = ((i+j)%2==0)? 1: -1;
 
            // Interchanging rows and columns to get the
            // transpose of the cofactor matrix
            adj[j * N + i] = fix_point_mul(sign, determinant(temp, N-1), 16);
            // adj[j * N + i] = (sign)*(determinant(temp, N-1));
        }
    }
}
 
// Function to calculate and store inverse, returns false if
// matrix is singular
static int inverse(int32_t *A, int32_t *inverse)
{
    // Find determinant of A[][]
    int det = determinant(A, N);
    //printf("\nDET = ");
	//printf("%d\n", det);
    if (det == 0)
    {
        printf("Singular matrix, can't find its inverse\n");
        return 0;
    }
 
    // Find adjoint
    int32_t adj[N*N];
    adjoint(A, adj);
 
    // Find Inverse using formula "inverse(A) = adj(A)/det(A)"
    for (int i=0; i<M; i++)
        for (int j=0; j<N; j++)
	        inverse[i * N + j] = fix_point_div(adj[i * N + j], det, precision);
            // inverse[i * N + j] = adj[i * N + j]/ (int)(det);
 
    return 1;
}

//https://stackoverflow.com/questions/69259761/moore-penrose-pseudo-inverse
int np_pinv_33(
    int32_t A[3][3], 
    int32_t *array_shape, 
    int32_t *shape_length, 
    int32_t *array_out, 
    int32_t *array_shape_out, 
    int32_t *shape_length_out)
{
    int32_t *matrix        = (int32_t *)malloc(M * N * sizeof(int32_t));
    int32_t *t_matrix      = (int32_t *)malloc(M * N * sizeof(int32_t));
    int32_t *matrix_mult   = (int32_t *)malloc(N * N * sizeof(int32_t));
    int32_t *pseudoinverse = (int32_t *)malloc(M * N * sizeof(int32_t));
    int32_t *adj           = (int32_t *)malloc(M * N * sizeof(int32_t)); // To store adjoint 
    int32_t *inv           = (int32_t *)malloc(N * N * sizeof(int32_t)); // To store inverse 
    
	//printf("\nThe Matrix is :\n"); 
	Trans_2D_1D(A, matrix);
 	//display(matrix, 1); 	// M*N matrix
 	
    //printf("\nThe Transpose is :\n");
    Transpose(matrix, t_matrix);
    //display(t_matrix, 2);	// N*M matrix
 
    //printf("\nThe product of the matrix is: \n");
    MatrixMult(t_matrix, matrix, matrix_mult, 1); // (N*M) * (M*N) 
    //display(matrix_mult, 0);// N*N matrix
 

    if (inverse(matrix_mult, inv)){
	    //printf("\nThe Inverse is :\n");
        //display(inv, 0);	// N*N matrix	
		//printf("\nThe Monroe-penrose inverse is :\n");
		MatrixMult(inv,t_matrix, pseudoinverse, 2); // (N*N) * (N*M)
		//display(pseudoinverse, 2); // N*M matrix	
 	}

    memcpy(array_out, pseudoinverse, N*M*sizeof(int32_t));
	array_shape_out[0] = N;
	array_shape_out[1] = M;
	*shape_length_out = 2;

    free(matrix);
    free(t_matrix);
    free(matrix_mult);
    free(pseudoinverse);
    free(adj);
    free(inv);
 	//return pseudoinverse;
    return 0;
}

#undef M
#undef N