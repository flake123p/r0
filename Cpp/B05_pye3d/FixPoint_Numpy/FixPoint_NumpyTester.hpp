

#ifndef _FIX_POINT_NUMPY_TESTER_H_INCLUDED_

#include <stdio.h>
#include <stdint.h>
#include "FixPoint.h"

typedef int32_t (*fp_1_on_1)(int32_t);

void np_func_test();

void FixPoint_NumpyTest();

#define _FIX_POINT_NUMPY_TESTER_H_INCLUDED_
#endif//_FIX_POINT_NUMPY_TESTER_H_INCLUDED_



