
#ifndef _FIX_POINT_NUMPY_H_INCLUDED_

#include <stdio.h>
#include <stdint.h>
#include "FixPoint.h"

#define MAX_SHAPE (6)

#define NP_PI 0x0003243F

int32_t np_array_len(int32_t *array_shape, int32_t *shape_length);
int np_array_axis_map(int32_t *shape, int32_t *shape_length, int32_t axis, int32_t *shape_new, int32_t *shape_length_new);
void np_array_get_shape_multiplier(int32_t *shape, int32_t *shape_length, int32_t *shape_mul);
int32_t np_array_pos_flatten_by_mul_ary(int32_t *shape_mul, int32_t *shape_length, int32_t *pos);
int32_t np_array_pos_flatten(int32_t *shape, int32_t *shape_length, int32_t *pos);
int np_array_increment(int32_t *shape, int32_t length, int32_t *curr);

int32_t np_sin_fake(int32_t in);
int32_t np_sin(int32_t fix_point_deg);

int32_t np_cos_fake(int32_t in);
int32_t np_cos(int32_t fix_point_deg);

int32_t np_sqrt_fake(int32_t in);
int32_t np_sqrt(int32_t n);

int32_t *np_sum(
    int32_t *array, 
    int32_t *array_shape, 
    int32_t *shape_length, 
    int32_t *axis,
    int32_t *array_out, 
    int32_t *array_shape_out, 
    int32_t *shape_length_out);

int32_t *np_mean(
    int32_t *array, 
    int32_t *array_shape, 
    int32_t *shape_length, 
    int32_t *axis,
    int32_t *array_out, 
    int32_t *array_shape_out, 
    int32_t *shape_length_out);

int np_dot(
    int32_t *array, 
    int32_t *array_shape, 
    int32_t *shape_length,
    int32_t *array2, 
    int32_t *array_shape2, 
    int32_t *shape_length2,
    int32_t *array_out, 
    int32_t *array_shape_out, 
    int32_t *shape_length_out);

void np_transpose_2d( int32_t *array, 
    int32_t *array_shape, 
    int32_t *shape_len,
    int32_t *array_out,
    int32_t *array_shape_out,
    int32_t *shape_len_out);

int np_pinv_22(
    int32_t A[2][2], 
    int32_t *array_shape, 
    int32_t *shape_length, 
    int32_t *array_out, 
    int32_t *array_shape_out, 
    int32_t *shape_length_out);

int np_pinv_33(
    int32_t A[3][3], 
    int32_t *array_shape, 
    int32_t *shape_length, 
    int32_t *array_out, 
    int32_t *array_shape_out, 
    int32_t *shape_length_out);

int np_pinv_44(
    int32_t A[4][4], 
    int32_t *array_shape, 
    int32_t *shape_length, 
    int32_t *array_out, 
    int32_t *array_shape_out, 
    int32_t *shape_length_out);

int np_pinv(
    int32_t *array, 
    int32_t *array_shape, 
    int32_t *shape_length, 
    int32_t *array_out, 
    int32_t *array_shape_out, 
    int32_t *shape_length_out);
#define _FIX_POINT_NUMPY_H_INCLUDED_
#endif//_FIX_POINT_NUMPY_H_INCLUDED_
