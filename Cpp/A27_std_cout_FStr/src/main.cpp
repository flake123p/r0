
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>
#include <csignal>
#include <stdlib.h> //for exit()
#include <set>
#include <vector>
#include <algorithm>
#include <stdint.h>
#include <string>
#include <cstdarg> //va_start va_end

class FStr {
public:
    char *buf;
    int ret;
    int len;
    // int re = 0;
    // int relen[5];
    FStr() {
        const int default_len = 256;
        buf = (char *)malloc(default_len+1);
        BASIC_ASSERT(buf != nullptr);
        len = default_len;
    }
    ~FStr() {
        if (buf != nullptr) {
            free(buf);
        }
    }
    char *pr(const char* format, ...) {
        while (1) {
            {
                va_list vl;
                va_start(vl, format);
                ret = vsnprintf(buf, len, format, vl); // the ret DOESN'T include final character '\0' !!!
                va_end(vl);
            }
            // printf("ret= %d\n", ret);
            if (ret >= len) {
                if (buf != nullptr) {
                    free(buf);
                }
                buf = (char *)malloc(len * 2);
                BASIC_ASSERT(buf != nullptr);
                len = len * 2;
                // relen[re] = len;
                // re++;
                continue;
            } else {
                break;
            }
        }
        
        return buf;
    }
};

int main () 
{
    char abc[80];
    sprintf(abc, "xxx %d", 12);
    {
        FStr s0;

        s0.pr("yyy %d", 98);

        #if 1
        std::cout << \
            s0.pr("yyy %d", 98) << \
            s0.pr(" zzz %03d", 23) << \
            std::endl;
        #endif
        // printf("ret = %d\n", s0.ret);
        // printf("buf = %s\n", s0.buf);
        // printf("re  = %d\n", s0.re);
        // printf("relen[0] = %d\n", s0.relen[0]);
        // printf("relen[1] = %d\n", s0.relen[1]);
    }
    return 0;
}
