#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <string.h>
#include <openssl/applink.c>

#define PUBLICKEY "rsa_public_key.pem"
#define PRIVATEKEY "rsa_private_key.pem"

#define MAX_DATA_LEN 1024
#define SHA256_LENTH 32

void printHex(unsigned char *md, int len)
{
    int i = 0;
    for (i = 0; i < len; i++)
        printf("%02x", md[i]);
    printf("\n");
}

int main(int argc, char *argv[])
{
    char data[] = "This_Is_SHA_RSA_Testing";
    FILE *fp = NULL;
    RSA *publicRsa = NULL;
    RSA *privateRsa = NULL;
	SHA256_CTX sha256_ctx;
	unsigned char SHA256result[SHA256_LENTH];

	printf("sdfdsff\n");

	SHA256_Init(&sha256_ctx);
	printf("222sdfdsff\n");

	

	SHA256_Update(&sha256_ctx, data, strlen(data));
	SHA256_Final(SHA256result,&sha256_ctx);
	printf("do sha with msg: %s\n", data);
	printHex(SHA256result, SHA256_LENTH);

	//read public key
    if ((fp = fopen(PUBLICKEY, "r")) == NULL) {
        printf("public key path error\n");
        return -1;
    }
    if ((publicRsa = PEM_read_RSA_PUBKEY(fp, NULL, NULL, NULL)) == NULL) {
        printf("PEM_read_RSA_PUBKEY error \n");
        return -1;
    }
    fclose(fp);

	//read private key
    if ((fp = fopen(PRIVATEKEY, "r")) == NULL) {
        printf("private key path error\n");
        return -1;
    }
    if ((privateRsa = PEM_read_RSAPrivateKey(fp, NULL, NULL, NULL)) == NULL) {
        printf("PEM_read_RSAPrivateKey error\n");
        return 0;
    }
    fclose(fp);

	//encrypt data
    int rsa_len = RSA_size(publicRsa);
    unsigned char *encryptMsg = (unsigned char *)malloc(rsa_len);
    memset(encryptMsg, 0, rsa_len);
    if (RSA_public_encrypt(SHA256_LENTH, SHA256result, encryptMsg, publicRsa, RSA_PKCS1_PADDING) < 0) {
        printf("RSA_public_encrypt error\n");
		RSA_free(publicRsa);
		RSA_free(privateRsa);
		return -1;
    }
	printf("encrtypt data:\n");
	printHex(encryptMsg, rsa_len);

	//decrypt data
	rsa_len = RSA_size(privateRsa);
	unsigned char *decryptMsg = (unsigned char *)malloc(rsa_len);
	memset(decryptMsg, 0, rsa_len);
	if (RSA_private_decrypt(rsa_len, encryptMsg, decryptMsg, privateRsa, RSA_PKCS1_PADDING) < 0) {
		printf("RSA_private_decrypt error\n");
		RSA_free(publicRsa);
		RSA_free(privateRsa);
		return -1;
	}
	printf("decrtypt data:\n");
	printHex(decryptMsg, SHA256_LENTH);

	//compare result
	if(memcmp(SHA256result, decryptMsg, SHA256_LENTH) == 0) {
		printf("compare: [PASS]\r\n");
	} else {
		printf("compare: [FAIL]\r\n");
	}

    RSA_free(publicRsa);
    RSA_free(privateRsa);
    return 0;
}
