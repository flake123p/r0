
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

/*
Ref: https://docs.microsoft.com/en-us/cpp/cpp/dynamic-cast-operator?view=msvc-160
*/
#if 0
A -> B \
        --> D
A -> C /

How to downcast to the Class A of Class B?

class D* pd = new class D;
class A* pa; //How???
#endif

class A {
public:
	int position;
	//virtual void show(){printf("position = %d\n", position);};
};

class B : public A {
public:
	B(){position=100;};
	//virtual void show(){printf("position = %d\n", position);};
};

class C : public A {
public:
	C(){position=999;};
	//virtual void show(){printf("position = %d\n", position);};
};

class D : public B , public C{
public:
	//virtual void show(){printf("position = %d\n", B::position);};
	//virtual void show(){/*B::show();C::show();*/};
};

int main(int argc, char *argv[])
{
	class D* pd = new class D;
	class A* pa; //How???
	
	class B* pb = dynamic_cast<class B*>(pd);
	pa = dynamic_cast<class A*>(pb);
#if 0
	Another way:
	pa = dynamic_cast<class A*>(dynamic_cast<class B*>(pd));
#endif
	DND(pa->position); //print 100
	
	return 0;
}
