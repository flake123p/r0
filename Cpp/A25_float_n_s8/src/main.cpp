
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>
#include <csignal>
#include <stdlib.h> //for exit()
#include <set>
#include <vector>
#include <algorithm>
#include <stdint.h>

int main () 
{
    uint8_t u8buf[] = {0x1, 0x2, 0x7f, 0x80, 0x81, 0xF0, 0xFF};
    
    int8_t *s8ptr = (int8_t *)u8buf;

    printf("U8    : ");
    for (int i = 0; i < 7; i++) {
        printf("%7x ", u8buf[i]);
    }
    printf("\n");

    printf("U8    : ");
    for (int i = 0; i < 7; i++) {
        printf("%7d ", u8buf[i]);
    }
    printf("\n");

    printf("S8    : ");
    for (int i = 0; i < 7; i++) {
        printf("%7d ", s8ptr[i]);
    }
    printf("\n");

    float f32FromU8[7];
    for (int i = 0; i < 7; i++) {
        f32FromU8[i] = (float)u8buf[i];
    }
    printf("U8 F32: ");
    for (int i = 0; i < 7; i++) {
        printf("%7.4f ", f32FromU8[i]);
    }
    printf("\n");

    float f32FromS8[7];
    for (int i = 0; i < 7; i++) {
        f32FromS8[i] = (float)s8ptr[i];
    }
    printf("S8 F32: ");
    for (int i = 0; i < 7; i++) {
        printf("%7.4f ", f32FromS8[i]);
    }
    printf("\n");

    printf("S8 F32: ");
    for (int i = 0; i < 7; i++) {
        printf("%7.4f ", f32FromS8[i]/16);
    }
    printf("\n");

    return 0;
}
