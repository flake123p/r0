
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

// Abstract class = there is virtual function inside
class media
{
	public:
		int chHdl;
		media(){chHdl=122;};
		virtual void TxIn() = 0; //pure virtual
		virtual void TxGo() = 0; //pure virtual
		virtual void TxEnd() = 0; //pure virtual
};

class csma
{
	public:
		virtual void StartOfTxRound() = 0; //pure virtual
		virtual void BackoffCheck() = 0; //pure virtual
		virtual void CarrierSense() = 0; //pure virtual
		virtual void TxStart() = 0; //pure virtual
		virtual void TxCollisionSense() = 0; //pure virtual
		virtual void EndOfTxRound() = 0; //pure virtual
};

// JUST a demo, not really like this in Sim4
class csma_sw : public media, public csma
{
	public:
		void StartOfTxRound(){printf("%s()\n", __func__);};
		void BackoffCheck(){printf("%s()\n", __func__);};
		void CarrierSense(){printf("%s()\n", __func__);};
		void TxStart(){printf("%s()\n", __func__);};
		void TxCollisionSense(){printf("%s()\n", __func__);};
		void EndOfTxRound(){printf("%s()\n", __func__);};
		
		void TxIn()
		{
			StartOfTxRound();
			BackoffCheck();
			CarrierSense();
		}
		void TxGo()
		{
			TxStart();
		}
		void TxEnd()
		{
			TxCollisionSense();
			EndOfTxRound();
		}
		
};

int main(int argc, char *argv[])
{
	class csma_sw obj;
	obj.TxIn();
	obj.TxGo();
	obj.TxEnd();
	printf("chHdl = %d\n", obj.chHdl);
	return 0;
}
