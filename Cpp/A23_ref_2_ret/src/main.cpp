
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>
#include <csignal>
#include <stdlib.h> //for exit()
/*
   It isn't a reference to a reference: such a thing does not exist.

   It is an rvalue reference, a new feature added in C++11.
   http://thbecker.net/articles/rvalue_references/section_01.html
*/

/*
   https://stackoverflow.com/questions/5590978/reference-of-reference-in-c

   Search for "perfect forwarding"

   https://tjsw.medium.com/%E6%BD%AE-c-11-perfect-forwarding-%E5%AE%8C%E7%BE%8E%E8%BD%89%E7%99%BC%E4%BD%A0%E7%9A%84%E9%9C%80%E6%B1%82-%E6%B7%B1%E5%BA%A6%E8%A7%A3%E6%9E%90-f991830bcd84
   
   https://wdv4758h.github.io/notes/cpp/perfect-forwarding.html

   http://kksnote.logdown.com/posts/1653018-c-forward-11-perfect-forwarding-are-what
*/
int main () {


   return 0;
}
