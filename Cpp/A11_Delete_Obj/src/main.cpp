
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>
#include <map>
#include <cstring>

using namespace std;

//
// Wrong
//
// Should every class have a virtual destructor? YES:
//		https://stackoverflow.com/questions/353817/should-every-class-have-a-virtual-destructor
//
class base {
public:
	base(void){printf("base constructor\n");};
	~base(void){printf("base destructor\n");};
};

class derived : public base {
public:
	derived(void){printf("derived constructor\n");};
	~derived(void){printf("derived destructor\n");};
};

void inherit_delete_test_wrong(void)
{
	printf("inherit_delete_test_wrong : (start, no derived destructor, it's wrong.)\n");
	class base *ptr = (class base *)new(class derived);
	delete(ptr);
	printf("inherit_delete_test_wrong : (end)\n");
}

//
// Correct
//
class base2 {
public:
	base2(void){printf("base2 constructor\n");};
	virtual ~base2(void){printf("base2 destructor\n");}; // add virtual here
};

class derived2 : public base2 {
public:
	derived2(void){printf("derived2 constructor\n");};
	~derived2(void){printf("derived2 destructor\n");};
};

void inherit_delete_test_right(void)
{
	printf("inherit_delete_test_right : (start, because base2 destrucotr is virtual now.)\n");
	class base2 *ptr2 = (class base2 *)new(class derived2);
	delete(ptr2);
	printf("inherit_delete_test_right : (end)\n");
}

// no destructor
// with destructor
// virtual destructor
class no_des {
public:
	no_des(void){printf("no_des constructor\n");};
};
class with_des {
public:
	with_des(void){printf("with_des constructor\n");};
	~with_des(void){printf("with_des destructor\n");};
};
class vir_des {
public:
	vir_des(void){printf("vir_des constructor\n");};
	virtual ~vir_des(void){printf("vir_des destructor\n");};
};
class d1 : public no_des {
public:
	d1(void){printf("d1 constructor\n");};
};
class d2 : public with_des {
public:
	d2(void){printf("d2 constructor\n");};
};
class d3 : public vir_des {
public:
	d3(void){printf("d3 constructor\n");};
};
class da : public no_des {
public:
	da(void){printf("da constructor\n");};
	~da(void){printf("da destructor\n");};
};
class db : public with_des {
public:
	db(void){printf("db constructor\n");};
	~db(void){printf("db destructor\n");};
};
class dc : public vir_des {
public:
	dc(void){printf("dc constructor\n");};
	~dc(void){printf("dc destructor\n");};
};

void inherit_desctructor_test(void) //doesn't mean much
{
	printf("inherit_desctructor_test : (start, doesn't mean much)\n");
	printf("=== === no_des inherit no_des === ===\n");
	{class d1 a;}
	printf("=== === no_des inherit with_des === ===\n");
	{class d2 b;}
	printf("=== === no_des inherit vir_des === ===\n");
	{class d3 c;}
	printf("=== === with_des inherit no_des === ===\n");
	{class da x;}
	printf("=== === with_des inherit with_des === ===\n");
	{class db y;}
	printf("=== === with_des inherit vir_des === ===\n");
	{class dc z;}
}

//
//
//
class AA {
public:
	~AA(void){printf("AA destructor\n");};
};

class BB {
public:
	~BB(void){printf("BB destructor\n");};
};

int main(int argc, char *argv[])
{
	class AA *pa = new(class AA);
	class BB *pb = new(class BB);
	
	//void *p = (void *)pa;
	//delete(p); not allowed !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	delete(pa);
	delete(pb);
	
	inherit_delete_test_wrong();
	
	inherit_delete_test_right();

	inherit_desctructor_test(); //doesn't mean much
	return 0;
}
