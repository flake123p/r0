
#include <iostream>
#include <cstdint>
#include <memory>

#define BASIC_PRINT printf

/*
    https://kheresy.wordpress.com/2012/03/03/c11_smartpointer_p1/
*/

std::shared_ptr<int> MemoryAlloc()
{
  std::shared_ptr<int> a( new int(1000) );
 
  return a;
}

int memory_demo(int in)
{
    static std::unique_ptr<int> __attribute__((used)) a( new int(5) )  ;
    static std::shared_ptr<int> __attribute__((used)) aa = MemoryAlloc();
    int b = *a + *aa;
    BASIC_PRINT("b = %d\n", b);
    return b + in;
}

int main()
{
    memory_demo(0);

    return 0;
}