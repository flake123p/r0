#include <stdio.h>
#include <stdint.h>

int32_t float_to_fix32(double in, int precision)
{
    int32_t amptide = 1 << precision;
    return (int32_t)(in * amptide);
}

int32_t fix_point_add(int32_t a, int32_t b)
{
    return a + b;
}

int32_t fix_point_sub(int32_t a, int32_t b)
{
    return a - b;
}

int32_t fix_point_mul(int32_t a, int32_t b, int precision)
{
    return (int64_t)a * b >> (precision);
}

int32_t fix_point_div(int32_t a, int32_t b, int precision)
{
    return ((int64_t)a << precision ) / b;
}

double fix32_to_float(int32_t in, int precision)
{
    return ((double)in/(1<<precision));
}

int main(int argc, char *argv[])
{
    int precision = 24;
    int32_t a = float_to_fix32(-1.5, precision);
    int32_t b = float_to_fix32(1.4, precision);
    int32_t result = fix_point_sub(a, b);

    printf("result = %lf\n", fix32_to_float(result, precision));
    //printf("0x%X\n", 1<<16);
    return 0;
}

//https://numpy.org/doc/stable/reference/generated/numpy.sum.html




void np_sum(
    int32_t *array, 
    int32_t *array_shape, 
    int32_t *shape_length, 
    int32_t *axis,
    int32_t *array_out, 
    int32_t *array_shape_out, 
    int32_t *shape_length_out)
{
    // ...
}

void call_np_sum(void)
{
    int32_t a[] = {1, 2, 2, 3,};
    int32_t a_shape[] = {2, 2};
    int32_t a_shape_len = sizeof(a_shape)/sizeof(int32_t);
    int32_t axis = 0;
    int32_t array_out[100];
    int32_t array_shape_out[20];
    int32_t shape_length_out;

    np_sum(a, a_shape, &a_shape_len, &axis, array_out, array_shape_out, &shape_length_out);
}
