#include <iostream>
#include <string>

// https://stackoverflow.com/questions/7663709/how-can-i-convert-a-stdstring-to-int

int main() {
    std::string s = "123";

    printf("s = %s\n", s.c_str());

    int i = std::stoi(s);

    printf("i = %d\n", i);

    return 0;
}