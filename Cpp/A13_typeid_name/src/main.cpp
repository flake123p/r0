
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

#include <typeinfo>

/*
Usage:
    #include <typeinfo>

    typeid(your_class).name()
    or
    typeid(*this).name()
*/
class base {
public:
    base()
    {
        printf("Class name = (%s)\n", typeid(*this).name()); // weird
        std::cout << typeid(*this).name() << '\n';           // weird still
        
        {
            const char *className = typeid(*this).name(); // correct weird stuff
            className++;
            printf("className = (%s)\n", className);
        }
    };
};

class ClassName {
public:
    const char *name;
    ClassName(int skipFirstChar = 1)
    {
        name = typeid(*this).name();
        if (skipFirstChar) // correct weird stuff
            name++;
        printf("ClassName Constructor, typeid name = %s, pretty func = %s\n", name, __PRETTY_FUNCTION__);
    };
};

class MyClass : public ClassName {
public:
    MyClass(){printf("MyClass Constructor\n");};
};

int main(int argc, char *argv[])
{
    
    class base b;
    printf("Object b name = (%s)\n", typeid(b).name());
    
    class MyClass c;
    printf("Object c name = (%s)\n", c.name);
    
    return 0;
}
