
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

// Abstract class = there is virtual function inside
class abc
{
	public:
		virtual void show() = 0; //pure virtual
		virtual void simp() {printf("simple - parent\n");}; //simple virtual
		void noneVir() {printf("noneVir - parent\n");}; //none virtual
};

class v1:public abc
{
	public:
		// Must implement show()
		void show() { std::cout << "this is v1" << std::endl; }
		void simp() { std::cout << "simple - v1" << std::endl; }
		void noneVir() { std::cout << "noneVir - v1" << std::endl; }
};

class v2:public abc
{
	public:
		// Must implement show()
		void show() { std::cout << "this is v2" << std::endl; }
};

int main(int argc, char *argv[])
{
	abc *p;
	v1 obj1;
	v2 obj2;
	
	printf("obj1:\n");
	p = &obj1;
	p->show();
	p->simp();
	p->noneVir();
	obj1.noneVir();
	
	printf("obj2:\n");
	p = &obj2;
	p->show();
	p->simp();
	p->noneVir();
	
	return 0;
}
