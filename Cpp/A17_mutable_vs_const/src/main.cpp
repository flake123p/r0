
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>

// mutable.cpp
class X
{
public:
   bool GetFlag() const
   {
      m_accessCount++;
	  DND(m_accessCount);
      return m_flag;
   }
   X(){m_flag=true;m_accessCount=10;};
private:
   bool m_flag;
   mutable int m_accessCount; //mutable make "const-function GetFlag()" can write this variable
};

// Example 2
class Base
{
public:
	mutable int abc; //mutable make "const-function SetAbc()" can write this variable
	virtual void SetAbc(int in) const = 0;
	virtual ~Base(){};
};

class Derived : public Base {
public:
	void SetAbc(int in) const {abc = in;DND(abc);};
};

int main(int argc, char *argv[])
{
	class X x;
	DND(x.GetFlag());
	
	class Derived d;
	d.SetAbc(11);
	
	return 0;
}
