
#include <setjmp.h>
#include <iostream>
#include <My_Basics.hpp>


class base {
public:
	base() { printf("class base Constructor\n"); };
	~base() { printf("class base Destructor\n"); };
};

class derived : public base {
public:
	derived() { printf("class derived Constructor\n"); };
	~derived() { printf("class derived Destructor\n"); };
};

class dderived : public derived {
public:
	dderived() { printf("class dderived Constructor\n"); };
	~dderived() { printf("class dderived Destructor\n"); };
};

int main(int argc, char *argv[])
{
	class dderived obj;
	
	return 0;
}
