# Ref
https://www.baeldung.com/linux/git-ignore-file-mode


To make Git ignore all the file mode changes, we can set the core.fileMode configuration to false using git config:

    $ git config core.fileMode false (Good! 20240408)


Additionally, we can make that configuration a default for every repository in the system by passing the –global flag:

    $ git config --global core.fileMode flag

    $ git config --global core.fileMode false


# vim .git/config

CHANGE the 'filemode' below to false!!!!!!!!!

[core]
        repositoryformatversion = 0
        filemode = false
        bare = false
        logallrefupdates = true