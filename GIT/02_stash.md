
# List
    git stash list


# Save
    git stash save
or
    git stash
or
    git stash [msg]


# Pop
    git stash pop
or
    git stash pop 0 or 1 or ...


# Drop
    git stash drop
or
    git stash drop 0 or 1 or ...


# Clear
    git stash clear