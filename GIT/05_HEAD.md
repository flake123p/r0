# Git Detached Head
https://www.cloudbees.com/blog/git-detached-head#git-detached-head-is-a-weird-error-message-to-receive

# Resolution
git checkout main

==============================================================================
What is a HEAD?

Fundamentals:
L1:
    objects 
        commits
        blobs
        trees
    references
        branches (label on commit)
        HEAD (keep track of the current point in a Git repo, usefull on "git log")

