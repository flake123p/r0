#
# https://gitbook.tw/chapters/rewrite-history/reset-revert-and-rebase
#

# Revert Approch 1
git stash
git stash drop


#
# https://www.git-tower.com/learn/git/faq/git-discard-changes
#

# Discarding Local Changes in a File
git restore index.html

# Discarding All Local Changes
git restore .

# Discarding All Local Changes and untracked (= new) files
git clean -f

# Saving Changes on the Stash
git stash --include-untracked
git stash pop