# Link:
https://github.com/doggy8088/Learn-Git-in-30-days/blob/master/zh-tw/01.md


## 01
1. Reference
2. Git Magic (zh_TW!!) 
    http://www-cs-students.stanford.edu/~blynn/gitmagic/intl/zh_tw/


## 02
GUI: 
1. SourceTree (Mercurial)
2. TortoiseGit


## 03
Local repo:
    git init
Local shared repo:
    git init --bare


## 05
Object: blob, tree
Index: (for commit)