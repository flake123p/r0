
# How to configure an existing git repo to be shared
https://stackoverflow.com/questions/3242282/how-to-configure-an-existing-git-repo-to-be-shared-by-a-unix-group

chgrp -R foo repodir                      # set the group
chmod -R g+rw repodir                     # allow the group to read/write
find repodir -type d -exec chmod g+s {} + # new files get group id of directory
git init --bare --shared=all repodir      # sets some important variables in repodir/config ("core.sharedRepository=2" and "receive.denyNonFastforwards=true")

sudo usermod -aG user0 user1