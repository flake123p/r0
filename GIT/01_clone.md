#
# Recurrsive Clone:
#
git clone --recursive https://github.com/KhronosGroup/OpenCL-SDK.git

or

git clone https://github.com/KhronosGroup/OpenCL-SDK.git
git submodule init
git submodule update


#
# Another Example:
#
git clone --recursive https://github.com/pytorch/pytorch
cd pytorch
# if you are updating an existing checkout
git submodule sync
git submodule update --init --recursive