#
# https://medium.com/@back_to_basics/cmake-functions-and-macros-22293041519f
#
# https://crascit.com/2019/01/29/forwarding-command-arguments-in-cmake/
#
function(myfunc P1 P2)
    message(STATUS "heya")
    message("Argument count: ${ARGC}")
    message("all arguments: ${ARGV}")
    message("optional arguments: ${ARGN}")
    message("P1 = ${P1}")
    message("P2 = ${P2}")
    message("ARG0 = ${ARG0}")
    message("ARG1 = ${ARG1}")
    message("ARGV dump:")
    foreach(i IN LISTS ARGV)
        message("  ${i}")
    endforeach()
    message("CMAKE_PROJECT_NAME = ${CMAKE_PROJECT_NAME}")
endfunction()