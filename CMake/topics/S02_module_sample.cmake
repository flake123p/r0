cmake_minimum_required(VERSION 3.6)

#################################################################
file(GLOB SRC CONFIGURE_DEPENDS "*.c" "*.cpp" "*.cxx" "*.cc")
if(DEFINED ENV{using_c_lang})
    SET_SOURCE_FILES_PROPERTIES(${SRC} PROPERTIES LANGUAGE C )
else()
    SET_SOURCE_FILES_PROPERTIES(${SRC} PROPERTIES LANGUAGE CXX )
endif()
add_library(test_tool ${SRC})

#################################################################
target_include_directories(
    test_tool PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR}"
)

if(DEFINED ENV{using_sys_r5})
    target_include_directories(test_tool PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/../sys_r5/inc")
    if(NOT DEFINED ENV{sys_added})
        add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../sys_r5 ${CMAKE_CURRENT_BINARY_DIR}/sys_r5)
        set(ENV{sys_added} 1)
    endif()
else()
    target_include_directories(test_tool PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/../sys_pc/inc")
    if(NOT DEFINED ENV{sys_added})
        add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../sys_pc ${CMAKE_CURRENT_BINARY_DIR}/sys_pc)
        set(ENV{sys_added} 1)
    endif()
endif()

target_include_directories(test_tool PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/../pupil/inc")
if(NOT DEFINED ENV{pupil_added})
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../pupil ${CMAKE_CURRENT_BINARY_DIR}/pupil)
    set(ENV{pupil_added} 1)
endif()

target_include_directories(test_tool PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/../pupil_tool")
if(NOT DEFINED ENV{pupil_tool_added})
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../pupil_tool ${CMAKE_CURRENT_BINARY_DIR}/pupil_tool)
    set(ENV{pupil_tool_added} 1)
endif()

target_include_directories(test_tool PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/../embcv/inc")
if(NOT DEFINED ENV{embcv_added})
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../embcv ${CMAKE_CURRENT_BINARY_DIR}/embcv)
    set(ENV{embcv_added} 1)
endif()

if(DEFINED ENV{using_d2d_ecv})
    target_include_directories(test_tool PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/../d2d_ecv")
    if(NOT DEFINED ENV{d2d_added})
        add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../d2d_ecv ${CMAKE_CURRENT_BINARY_DIR}/d2d_ecv)
        set(ENV{d2d_added} 1)
    endif()
else()
    target_include_directories(test_tool PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/../d2d")
    if(NOT DEFINED ENV{d2d_added})
        add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../d2d ${CMAKE_CURRENT_BINARY_DIR}/d2d)
        set(ENV{d2d_added} 1)
    endif()
endif()

# Flake: remove opencv in the future!!!!!!!!!!!!!!!!!!!!!!!!!!!
if(DEFINED ENV{DISABLE_OPENCV})
    message("Disable OpenCV linking = $ENV{DISABLE_OPENCV} ... on ${CMAKE_CURRENT_LIST_DIR}")
else()
    if(${LOCAL_OPENCV})
        message("Use local OpenCV ...")
        find_package( OpenCV REQUIRED PATHS "${PROJECT_SOURCE_DIR}/../cv")
    else()
        message("Use system OpenCV ...")
        find_package( OpenCV REQUIRED )
    endif()
endif()

#################################################################
target_link_libraries(test_tool PUBLIC sys pupil pupil_tool embcv d2d ${OpenCV_LIBS})

install(
    TARGETS test_tool
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
    RUNTIME DESTINATION bin
)
