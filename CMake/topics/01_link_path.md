
#
# Try these:
#
add_link_options("-L/opt/glip/lib -lglip")
link_directories(/opt/glip/lib)
link_directories(/opt/glip)
link_directories(${CMAKE_SOURCE_DIR}/build)
link_directories(${CMAKE_SOURCE_DIR})
link_directories("/opt/glip/lib")
target_link_libraries( yolo_bare_metal ${OpenCV_LIBS} glip)
#target_link_libraries( yolo_bare_metal ${OpenCV_LIBS} /opt/glip/lib/libglip.so)
#target_link_libraries( yolo_bare_metal ${OpenCV_LIBS} /opt/glip/lib/libglip.so.0.0.0)