# Basic

    cmake .. -D CMAKE_CXX_FLAGS="-g -O0"



# With shell script:

    Shell script outside:

        #!/bin/bash
        export MY_COMPILE_FLAGS="-DDISABLE_DBG_MSG"
        ./run.sh


    Shell script inside:

        cmake ../../../mod/buda/buda_rt -D CMAKE_CXX_FLAGS="-g -O0 $MY_COMPILE_FLAGS"

    
    Example2:

        COMPILE_FLAGS='-g -O0 $MY_COMPILE_FLAGS'

        cmake ../../../mod/buda/buda_rt -D CMAKE_C_FLAGS="$COMPILE_FLAGS" -D CMAKE_CXX_FLAGS="$COMPILE_FLAGS"