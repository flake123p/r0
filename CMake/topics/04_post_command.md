
add_custom_command(
    TARGET a 
    POST_BUILD
    COMMAND "objdump" -S a>a.S
    COMMENT "objdump to a.S ... Done"
    COMMAND "size" a
    #COMMAND cd sys_r5 && riscv32-unknown-elf-objdump -S libsys.a>libsys.S
    COMMAND "readelf" -ah a>a.E
)