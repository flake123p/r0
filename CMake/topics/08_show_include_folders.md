
https://stackoverflow.com/questions/6902149/listing-include-directories-in-cmake

#
# Dump with target
#
get_property(dirs TARGET pp_so PROPERTY INCLUDE_DIRECTORIES)
message(INCLUDE_FOLDERS:)
foreach(dir ${dirs})
    message(STATUS "dir='${dir}'")
endforeach()



#
# Dump with directory
#
get_property(dirs DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY INCLUDE_DIRECTORIES)
message(INCLUDE_FOLDERS:)
foreach(dir ${dirs})
    message(STATUS "dir='${dir}'")
endforeach()