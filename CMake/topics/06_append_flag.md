# Link

https://stackoverflow.com/questions/29901352/appending-to-cmake-c-flags



# Option 1:

if(SINGLE_MODE)
    SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -lglapi")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -lglapi")
endif(SINGLE_MODE)



# Option 2 (Global):

string(APPEND CMAKE_CXX_FLAGS " -lglapi")



# for all languages (Global):

add_compile_options(-lglapi)



# for only one target

target_compile_options(my_lib PUBLIC -lglapi)
