cmake_minimum_required(VERSION 3.6)
  
project(main)
set(BUILD_SHARED_LIBS ON)
set(CMAKE_INSTALL_PREFIX .)

# set(ENV{_NO_OS_} 1 ) # deprecated
add_compile_options ( -D_NO_OS_ )


#
# Install additional .so
#
Use this:
install(
    FILES
    ${CMAKE_CURRENT_BINARY_DIR}/pf2/libpf2.so DESTINATION lib
)

in sub-library.