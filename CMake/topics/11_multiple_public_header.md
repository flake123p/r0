
# Multiple
set_target_properties(
    pp_so PROPERTIES
    PUBLIC_HEADER
        "pp_so.h;../buda_if/buda.h"
)

# Single
set_target_properties(
    pp_so PROPERTIES
    PUBLIC_HEADER
        pp_so.h
)