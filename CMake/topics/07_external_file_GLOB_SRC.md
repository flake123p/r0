# Another cmake file

set(EXT_MESH_NODE_ROW 3)

file(GLOB SRC2 CONFIGURE_DEPENDS "test_sanity/*.cpp" "test_sanity/src/*.cpp")



# include another one

if(DEFINED EXT_CMAKE_CONF)
    include(${EXT_CMAKE_CONF})
    message("EXT_CMAKE_CONF = ${EXT_CMAKE_CONF}")
else()
    include(CMakeConf.cmake)
endif()



# build

add_executable(a.out ${SRC} ${SRC2})



# Shell script (clean_build.sh)

#
# Clean goldens for auto-gen
#
cd ../pytests
./01_clear_goldens.sh
cd ..

mkdir build_sanity -p
rm build_sanity/* -rf
cd build_sanity
cmake .. \
    -D CMAKE_CXX_FLAGS="-D_SANITY_TEST_BUILD_" \
    -D EXT_CMAKE_CONF="test_sanity/CMakeConf.cmake"

make -j10
result=$?

echo ====== Build Done!! Errorlevel = $result ======



# Shell script (go.sh)

cd ../build_sanity

make -j10
result=$?

echo ====== Build Done!! Errorlevel = $result ======

if [ $result == 0 ]; then
	./a.out
	echo ====== aout.exe Done!! Errorlevel = $? ======
else
	echo NOT execute aout.exe ...
fi



# Shell script (run.sh)

../build_sanity/a.out