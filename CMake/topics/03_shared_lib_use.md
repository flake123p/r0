#
# https://stackoverflow.com/questions/28597351/how-do-i-add-a-library-path-in-cmake
#

add_subdirectory(${PROJECT_SOURCE_DIR}/../fx3_rw ${CMAKE_CURRENT_BINARY_DIR}/fx3_rw)

# add_library( fx3_rw_so SHARED IMPORTED )
# set_target_properties( fx3_rw_so PROPERTIES IMPORTED_LOCATION "${PROJECT_SOURCE_DIR}/build/fx3_rw/libfx3_rw.so" )
# target_link_libraries(a PUBLIC fx3_rw_so m)

# add_library(fx3_rw SHARED IMPORTED)
# set_property(TARGET fx3_rw PROPERTY IMPORTED_LOCATION "${PROJECT_SOURCE_DIR}/libfx3_rw.so")

# target_link_libraries( a PUBLIC ${OpenCV_LIBS} glip)
target_link_libraries( a PUBLIC fx3_rw /opt/glip/lib/libglip.so)
# target_link_libraries( yolo_bare_metal ${OpenCV_LIBS} fx3_rw /opt/glip/lib/libglip.so)



#
# export PATH for multi .so case (Not help)
#
add_custom_command(
    TARGET a.out
    POST_BUILD
    COMMAND "export" LD_LIBRARY_PATH=../../../lib/pp_so/lib/
    COMMENT "export LD_LIBRARY_PATH=../../../lib/pp_so/lib/ ... Done"
)



#
# Set PATH via vscode
#
        {
            "splitTerminals": [
                {
                    "name": "app_pp_so",
                    "commands": [
                        "cd cpp/projects/p01/app/app_pp_so", 
                        "cd build",
                        "export LD_LIBRARY_PATH=../../../lib/pp_so/lib/"
                    ]
                },
            ]
        },