
file(GLOB SRC CONFIGURE_DEPENDS "*.c" "*.cpp" "*.cxx" "*.cc")
# AUX_SOURCE_DIRECTORY(${CMAKE_CURRENT_SOURCE_DIR} SRC) # Flake, don't use AUX, it will append parent source files
AUX_SOURCE_DIRECTORY(lib_mgr SRC)
message("UNIX = " ${UNIX})
if(UNIX)
    message("In UNIX")
    AUX_SOURCE_DIRECTORY(lib_linux SRC)
else()
    message("Not In UNIX")
    AUX_SOURCE_DIRECTORY(lib_win SRC)
endif()