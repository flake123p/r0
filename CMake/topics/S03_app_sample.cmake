cmake_minimum_required(VERSION 3.6)

project(main)

#file(GLOB_RECURSE CFILES "${CMAKE_SOURCE_DIR}/*.c") SET_SOURCE_FILES_PROPERTIES(${CFILES} PROPERTIES LANGUAGE CXX )
file(GLOB SRC CONFIGURE_DEPENDS "*.c" "*.cpp" "*.cxx" "*.cc")

if(DEFINED ENV{using_c_lang})
    SET_SOURCE_FILES_PROPERTIES(${SRC} PROPERTIES LANGUAGE C )
else()
    SET_SOURCE_FILES_PROPERTIES(${SRC} PROPERTIES LANGUAGE CXX )
endif()
add_executable(a ${SRC})

# Add the binary tree to the search path for include files
target_include_directories(
    a PUBLIC
    "${PROJECT_BINARY_DIR}"
    "${PROJECT_SOURCE_DIR}/../test_tool"
    "${PROJECT_SOURCE_DIR}/../test_3_ref"
)

add_compile_options ( -D_NO_OS_ )
# add_compile_options ( -D_USE_EMBCV_BLUR_ )
# add_compile_options ( -D_USE_EMBCV_THRESHOLD_ )
# add_compile_options ( -D_USE_EMBCV_FIT_ELLIPSE_ )
# add_compile_options ( -D_USE_EMBCV_FIND_CONTOURS_ )

# For pupil with func 2
add_compile_options ( -D_D2D_VER_200_)
add_compile_options ( -D_USING_D2D_ECV_)
set(ENV{using_d2d_ecv} 1)
set(ENV{using_embcvqq} 1)

set(CMAKE_CXX_FLAGS "-O2 -Wall -Wextra")

if(DEFINED ENV{test_tool_added})
    message("skip add test_tool ...")
else()
    set(ENV{test_tool_added} 1)
    add_subdirectory(${PROJECT_SOURCE_DIR}/../test_tool ${CMAKE_CURRENT_BINARY_DIR}/test_tool)
endif()

if(DEFINED ENV{test_3_ref_added})
    message("skip add test_3_ref ...")
else()
    set(ENV{test_3_refl_added} 1)
    add_subdirectory(${PROJECT_SOURCE_DIR}/../test_3_ref ${CMAKE_CURRENT_BINARY_DIR}/test_3_ref)
endif()

target_link_libraries(a PUBLIC test_tool test_3_ref m)
