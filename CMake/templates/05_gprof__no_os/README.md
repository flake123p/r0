
https://hackmd.io/-aw8V9iZT5GMFpUTl2zNaw

Step-1 : Profiling enabled while compilation
```
gcc -Wall -pg test_gprof.c test_gprof_new.c -o test_gprof
```

CMake:
```
cmake -DCMAKE_CXX_FLAGS=-pg -DCMAKE_EXE_LINKER_FLAGS=-pg -DCMAKE_SHARED_LINKER_FLAGS=-pg <SOURCE_DIR>
```

Step-2 : Execute the code
```
$ ./test_gprof 
```

Step-3 : Run the gprof tool
```
$  gprof test_gprof gmon.out > analysis.txt
```