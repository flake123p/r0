cmake_minimum_required(VERSION 3.6)
file(GLOB SRC CONFIGURE_DEPENDS "*.c" "*.cpp" "*.cxx" "*.cc")
SET_SOURCE_FILES_PROPERTIES(${SRC} PROPERTIES LANGUAGE CXX )
add_library(mod1 ${SRC})

#set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} " -O2 -Wall -Wextra")

target_include_directories(
    mod1 PUBLIC
    "${PROJECT_SOURCE_DIR}/../lib"
)

if(DEFINED ENV{lib_added})
    message("skip add lib...")
else()
    set(ENV{lib_added} added)
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../lib ${CMAKE_CURRENT_BINARY_DIR}/lib)
endif()


target_link_libraries(mod1 PUBLIC lib)

# set_target_properties(
#     mod1 PROPERTIES
#     PUBLIC_HEADER 
#         pupil_api.h
# )

install(
    TARGETS mod1
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
    RUNTIME DESTINATION bin
    PUBLIC_HEADER DESTINATION inc
)
