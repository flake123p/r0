#include <stdio.h>
#include <stdint.h>
#include "mod1.h"
#include "mod2.h"

int main()
{
    printf("Hello %s\n", __FILE__);
    mod1_demo();
    mod2_demo();
}