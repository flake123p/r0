function(mod3_cmake curr_target)
#message("curr_target = " ${curr_target})
target_include_directories(
    ${curr_target} PUBLIC
    "${PROJECT_SOURCE_DIR}/../mod/mod3"
)

if (NOT TARGET mod3)
add_subdirectory(${PROJECT_SOURCE_DIR}/../mod/mod3 ${CMAKE_CURRENT_BINARY_DIR}/mod/mod3)
#add_subdirectory(${PROJECT_SOURCE_DIR}/../mod/mod3 ${CMAKE_BINARY_DIR}/mod/mod3)
endif()

target_link_libraries(${curr_target} PUBLIC mod3)

endfunction()
