cmake_minimum_required(VERSION 3.6)
file(GLOB SRC CONFIGURE_DEPENDS "*.c" "*.cpp" "*.cxx" "*.cc")
SET_SOURCE_FILES_PROPERTIES(${SRC} PROPERTIES LANGUAGE CXX )
add_library(lib ${SRC})

# message("no os ...")
# message(ENV{_NO_OS_}) # wrong
# message(ENV${_NO_OS_}) # wrong
# message($ENV{_NO_OS_}) # correct

if($ENV{_NO_OS_} EQUAL 1)
    target_include_directories(
        lib PUBLIC
        "${CMAKE_CURRENT_SOURCE_DIR}/sys_no_os"
    )
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/sys_no_os)
else()
    target_include_directories(
        lib PUBLIC
        "${CMAKE_CURRENT_SOURCE_DIR}/sys"
    )
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/sys)
endif()

target_link_libraries(lib PUBLIC sys)

# set(pub_hdrs
#     "pupil_def.h"
#     "pupil_msg.h"
# )

# set_target_properties (
#     lib PROPERTIES
#     PUBLIC_HEADER 
#         "${pub_hdrs}"
# )

# install(
#     TARGETS lib
#     LIBRARY DESTINATION lib
#     ARCHIVE DESTINATION lib
#     RUNTIME DESTINATION bin
#     PUBLIC_HEADER DESTINATION inc
# )
