#!/bin/bash

install_path=$PWD
echo install_path = $install_path

echo "install ..."

mkdir build
cd build

cmake ../../03_shared_library/test -D BUILD_SHARED_LIBS=ON -D CMAKE_INSTALL_PREFIX=$install_path -DUSE_EXTERNAL_INSTALL_PREFIX=1 -D CMAKE_CXX_FLAGS="-g -O0"
result1=$?
echo "Step 1: cmake ... result = " $result1
if [ $result1 != 0 ]; then
	echo Failed ...
    exit
fi

make -j4
result2=$?
echo "Step 2: make ... result = " $result2
if [ $result2 != 0 ]; then
	echo Failed ...
    exit
fi

make install
result3=$?
echo "Step 3: make install ... result = " $result3
if [ $result3 != 0 ]; then
	echo Failed ...
    exit
fi