https://stackoverflow.com/questions/17511496/how-to-create-a-shared-library-with-cmake

https://cgold.readthedocs.io/en/latest/tutorials/libraries/static-shared.html

https://www.cprogramming.com/tutorial/shared-libraries-linux-gcc.html

cd test
mkdir build
cd build
cmake ..
make
make install
cd ../..

gcc -o a -Itest/inc main.c -lhello -Ltest/lib

export LD_LIBRARY_PATH=$(pwd)/test/lib

./a