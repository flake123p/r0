cmake_minimum_required(VERSION 3.6)
file(GLOB SRC CONFIGURE_DEPENDS "*.c" "*.cpp" "*.cxx" "*.cc")
SET_SOURCE_FILES_PROPERTIES(${SRC} PROPERTIES LANGUAGE CXX )
add_library(mod3 ${SRC})

set(CMAKE_CXX_FLAGS "-O2 -Wall -Wextra")

target_link_libraries(mod3 PUBLIC)

# set_target_properties(
#     mod3 PROPERTIES
#     PUBLIC_HEADER 
#         pupil_api.h
# )

install(
    TARGETS mod3
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
    RUNTIME DESTINATION bin
    PUBLIC_HEADER DESTINATION inc
)
