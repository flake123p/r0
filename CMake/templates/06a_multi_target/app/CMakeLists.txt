cmake_minimum_required(VERSION 3.6)

project(main)
set(BUILD_SHARED_LIBS ON)
set(CMAKE_INSTALL_PREFIX .)

#
# if you change this, don't have to "cmake" again, just "make"
#
set(ENV{_NO_OS_} 1 )

add_compile_options ( -D_NO_OS_ )

#file(GLOB_RECURSE CFILES "${CMAKE_SOURCE_DIR}/*.c") SET_SOURCE_FILES_PROPERTIES(${CFILES} PROPERTIES LANGUAGE CXX )
file(GLOB main_SRC CONFIGURE_DEPENDS "*.c" "*.cpp" "*.cxx" "*.cc")
SET_SOURCE_FILES_PROPERTIES(${main_SRC} PROPERTIES LANGUAGE CXX )

#set(CMAKE_CXX_FLAGS "-O0 -Wall -Wextra")
set(CMAKE_CXX_FLAGS "-O2 -Wall -Wextra")

add_executable(a ${main_SRC})

# add the binary tree to the search path for include files
# so that we will find TutorialConfig.h
target_include_directories(
    a PUBLIC
    "${PROJECT_BINARY_DIR}"
    "${PROJECT_SOURCE_DIR}/../mod/mod1"
    "${PROJECT_SOURCE_DIR}/../mod/mod2"
)

add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../mod/mod1 ${CMAKE_CURRENT_BINARY_DIR}/mod/mod1)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../mod/mod2 ${CMAKE_CURRENT_BINARY_DIR}/mod/mod2)

target_link_libraries(a PUBLIC mod1 mod2)

install(
    TARGETS a
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
    RUNTIME DESTINATION bin
)
