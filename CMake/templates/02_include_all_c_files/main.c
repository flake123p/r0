// A simple program that computes the square root of a number
#include <stdio.h>
#include <stdint.h>
#include "MathFunctions.h"

int main(int argc, char* argv[])
{
  // calculate square root
  const double outputValue = mysqrt(10);
  printf("mysqrt 10 = %f\n", outputValue);
  return 0;
}
