
# Use Keyword: STATIC

Example:

    cmake_minimum_required(VERSION 3.6)
    project(pp_phy)

    set(MAIN_PROJECT_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../..)

    file(GLOB SRC CONFIGURE_DEPENDS "*.c" "*.cpp" "*.cxx" "*.cc")
    SET_SOURCE_FILES_PROPERTIES(${SRC} PROPERTIES LANGUAGE CXX )

    add_library(pp_phy STATIC ${SRC})

    target_include_directories(
        pp_phy PUBLIC
        "${CMAKE_CURRENT_SOURCE_DIR}"
        "${CMAKE_CURRENT_SOURCE_DIR}/../basic/"
        "${CMAKE_CURRENT_SOURCE_DIR}/../pp_defs/"
    )

    include(${MAIN_PROJECT_DIR}/modlists/route_tbl.cmake)
    # include(../../modlists/route_tbl.cmake)
    route_tbl_cmake(pp_phy) # include, subdirectory, link

    target_link_libraries(pp_phy PUBLIC systemc)

