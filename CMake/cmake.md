# Include (gcc -I_your_dir)
https://stackoverflow.com/questions/13703647/how-to-properly-add-include-directories-with-cmake
Examples:
include_directories(${_your_dir})
include_directories("/usr/local/cuda-12.1/targets/x86_64-linux/include/")


# DEBUG build
set (CMAKE_BUILD_TYPE Debug)


# CXX Standard
set (CMAKE_CXX_STANDARD 14)


# C Standdard
??


# CUDA Cpp Version
https://stackoverflow.com/questions/61540127/set-cxx-standard-to-c17-when-combining-c-and-cuda-in-cmakelists
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CUDA_STANDARD 14)
set(CMAKE_CUDA_STANDARD_REQUIRED TRUE)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)


# Set gcc path
set(CMAKE_C_COMPILER /usr/bin/gcc)
set(CMAKE_CXX_COMPILER /usr/bin/g++)
