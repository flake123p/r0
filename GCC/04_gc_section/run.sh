#!/bin/bash

#g++ -std=c++11 func.cpp -I../../../Cpp_Platform2/_Library/include -c
#g++ -std=c++11 main.cpp -I../../../Cpp_Platform2/_Library/include -c
g++ -std=c++11 func.cpp -I../../../Cpp_Platform2/_Library/include -c -fdata-sections -ffunction-sections
g++ -std=c++11 main.cpp -I../../../Cpp_Platform2/_Library/include -c -fdata-sections -ffunction-sections

#ld --entry main main.o func.o -lc -lm --gc-sections

g++ main.o func.o -lc -lm -Wl,--gc-sections

objdump -S a.out>a.S

size a.out

./a.out