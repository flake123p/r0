
#include <stdio.h>

__attribute__((weak)) int abxx = 0;

void __attribute__((weak)) abcc() 
{
    printf("I am weak.\n");
}

int main()
{
    abcc();
    printf("abxx = %d\n", abxx);
    return 0;
}