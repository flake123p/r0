# 1. Just compile, no link [-c]
    
    $ gcc -c llist.cpp
    
    Default output is llist.o.


# 2. link [N/A]

    $ gcc a.o b.o -o a.out


# 3. Common attribute

https://www.quora.com/What-are-the-most-frequently-used-gcc-attribute-directives

__attribute__((always_inline))
__attribute__((noinline))
__attribute__((packed))
__attribute__((weak))

Example:

    int add(int,int) __attribute__((always_inline)); 

    int add(int x,int y) 
    { 
        return (x+y);
    } 

    struct test 
    { 
        int a; 
        char b; 
    }__attribute__((packed)); 

    typedef struct __attribute__((packed)) {
        uint8_t core_busy[2];
        uint8_t core_op[10]; // 5 for vec_add
        uint8_t mem_busy;
        uint8_t mem_dst;
        uint8_t mem_src;
        uint8_t mem_op;
    } TimingData ;

    __attribute__((weak)) int op_1(int x,int y) 
    { 
        return (x+y); 
    }

    int __attribute__((weak)) op_1(int x,int y) 
    { 
        return (x+y); 
    } 
