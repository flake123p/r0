#
# https://www.cprogramming.com/tutorial/shared-libraries-linux-gcc.html
#

#
# Step 1: Compiling with Position Independent Code
#
g++ -c func.cpp -fPIC

#
# Step 2: Creating a shared library from an object file
#
g++ -shared -o libfunc.so func.o

#
# Step 3: Linking with a shared library
#
g++ main.cpp -lfunc -L.


#
# Path:
# Using LD_LIBRARY_PATH
# Using rpath
#

#
# Put library to /usr/lib or /usr/local/lib ...
#
# ... update the cache: ldconfig
#