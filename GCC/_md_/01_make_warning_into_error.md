
# Link
https://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html


# -Werror

    -Werror
        Make all warnings into errors.