
# Link
https://stackoverflow.com/questions/3599160/how-can-i-suppress-unused-parameter-warnings-in-c


# Example 1
(void)threadHdlArray;


# Exampel 2
#define UNUSED(x) (void)(x)


# Example 3
I'm using #define UNUSED(...) (void)(__VA_ARGS__) which allows me to apply this to multiple variables.


# Example 4 : __attribute__((unused))
int Scrn00AAA_OnEntry(IN __attribute__ ((unused)) u32 ScrnEvtId, IN __attribute__ ((unused)) void *ScrnEvtHdl)
{
	return 0;
}

