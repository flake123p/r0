
# Silence WCast-function-type on a function structure
https://stackoverflow.com/questions/59041367/silence-wcast-function-type-on-a-function-structure


In brief:

#pragma GCC diagnostic ignored "-Wcast-function-type"

// some affected code

#pragma GCC diagnostic pop



# CXX Flag
-Wno-unused-parameter