#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

/*
    GCC Func Attr : https://gcc.gnu.org/onlinedocs/gcc/Function-Attributes.html

    GCC Variable Attr : https://gcc.gnu.org/onlinedocs/gcc/Variable-Attributes.html

    Common Var Attr (this demo):
        https://gcc.gnu.org/onlinedocs/gcc/Common-Variable-Attributes.html#Common-Variable-Attributes

 */

struct super_man {
    uint32_t a;
    uint8_t b;
    uint32_t c;
};
void dump_super_man()
{
    struct super_man _sm;
    printf("%s()\n", __func__);
    printf("a in %p\n", &_sm.a);
    printf("b in %p\n", &_sm.b);
    printf("c in %p\n", &_sm.c);
}

struct pack_man {
    uint32_t a;
    uint8_t b;
    uint32_t c;
} __attribute__ ((packed));
void dump_pack_man()
{
    struct pack_man _pm;
    printf("%s()\n", __func__);
    printf("a in %p (NOTE!! the first may be NOTE aligned!!!!!!)\n", &_pm.a);
    printf("b in %p\n", &_pm.b);
    printf("c in %p\n", &_pm.c);
}

struct apack_man {
    uint32_t a;
    uint8_t b;
    uint32_t c;
} __attribute__ ((packed, aligned (16)));
void dump_apack_man()
{
    struct apack_man _m;
    printf("%s()\n", __func__);
    printf("a in %p (NOTE!! aligned (16) here!!!!!!)\n", &_m.a);
    printf("b in %p\n", &_m.b);
    printf("c in %p\n", &_m.c);
}

struct confuse_man {
    uint32_t a __attribute__ ((aligned (16)));
    uint8_t  b __attribute__ ((aligned (16)));
    uint32_t c __attribute__ ((aligned (16)));
} __attribute__ ((packed));
void dump_confuse_man()
{
    struct confuse_man _m;
    printf("%s()\n", __func__);
    printf("a in %p (NOTE!! ALL aligned (16) here!!!!!!)\n", &_m.a);
    printf("b in %p\n", &_m.b);
    printf("c in %p\n", &_m.c);
}

int main()
{
    dump_super_man();
    dump_pack_man();
    dump_apack_man();
    dump_confuse_man();

    return 0;
}