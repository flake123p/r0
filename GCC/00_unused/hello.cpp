#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

/*
    GCC Func Attr : https://gcc.gnu.org/onlinedocs/gcc/Function-Attributes.html

    GCC Variable Attr : https://gcc.gnu.org/onlinedocs/gcc/Variable-Attributes.html

 */

/*
    __attribute__((__retain__)) is not for these cases!!!
*/
__attribute__((__unused__)) static void super_man() // This is "unused" !!!!!!!!!!!!!!!!
{
    ;
}

// you can put attr in this location:
static void __attribute__((__used__)) bat_man()// This is "used" !!!!!!!!!!!!!!!!
{
    ;
}

int foo(__attribute__((__unused__)) int abc) // Can't use "used"
{
    return 101;
}

int bar(int __attribute__((__unused__)) def) // you can put attr in middle
{
    return 30;
}

int ho(int ghi __attribute__((__unused__))) // you can put attr in tail
{
    return 1;
}

int main()
{
    int aqua_man __attribute__((__unused__)) = 9 ;

    int flash = 10;
    (void)flash; // Popular usage !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

    //int ww;
    int ww = 0;
    ww = ww; // ww may be used uninitialized !!

    //int a_man __attribute__((__retain__)) = 9 ; // no such use

    return foo(3) + bar(3) + ho(0);
}
