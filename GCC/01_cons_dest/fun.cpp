#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

/*
    GCC Func Attr : https://gcc.gnu.org/onlinedocs/gcc/Function-Attributes.html

    GCC Variable Attr : https://gcc.gnu.org/onlinedocs/gcc/Variable-Attributes.html

constructor
destructor
constructor (priority)
destructor (priority)
 */


__attribute__((__constructor__(1000))) static void super_man_1000()
{
    printf("this is %s\n", __func__);
}

__attribute__((__constructor__(1001))) void super_man_1001()
{
    printf("this is %s (no static)\n", __func__);
}

// you can put attr in this location:
static void __attribute__((__destructor__ (1000))) bat_man_1000()
{
    printf("this is %s\n", __func__);
}

void __attribute__((__destructor__ (1001))) bat_man_1001()
{
    printf("this is %s (no static)\n", __func__);
}

int foo()
{
    return 3;
}