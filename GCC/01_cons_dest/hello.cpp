#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

__attribute__((__constructor__(500))) static void super_man_500()
{
    printf("this is %s\n", __func__);
}

// you can put attr in this location:
static void __attribute__((__destructor__ (500))) bat_man_500()//
{
    printf("this is %s\n", __func__);
}

int main()
{
    //extern int foo();
    //printf("123, %d\n", foo());
    printf("456\n");

    return 0;
}
