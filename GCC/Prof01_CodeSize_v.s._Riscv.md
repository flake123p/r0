# -g
Don't Matter

# -g -O2
	text_cen = 139215
	data_cen = 710
	bss__cen = 263893

	text_mesh = 4882
	data_mesh = 24
	bss__mesh = 4635
t =     37.773740 sec
t =     35.342634 sec
t =     35.241010 sec

# -g -O3
	text_cen = 161790
	data_cen = 598
	bss__cen = 263893

	text_mesh = 4834
	data_mesh = 24
	bss__mesh = 4635
t =     37.185096 sec (Sometimes fail? Break my threading safety?)
t =     35.437194 sec
t =     35.437938 sec

# -g -Os
	text_cen = 92181
	data_cen = 710
	bss__cen = 263893

	text_mesh = 3820
	data_mesh = 24
	bss__mesh = 4635
t =     37.723718 sec
t =     35.525587 sec
t =     35.464183 sec