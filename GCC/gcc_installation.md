# PPA
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo apt update

# MORE: https://phoenixnap.com/kb/install-gcc-ubuntu
# MORE: https://www.linuxcapable.com/how-to-install-gcc-compiler-on-ubuntu-linux/
(from source ...)

```
sudo apt install gcc-10
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-10 50
sudo update-alternatives --config gcc
```

```
sudo apt install g++-10
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-10 50
sudo update-alternatives --config g++
```


sudo apt install gcc-12 g++-12
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-12 50
sudo update-alternatives --config gcc
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-12 50
sudo update-alternatives --config g++

sudo apt install gcc-13 g++-13
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-13 65
sudo update-alternatives --config gcc
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-13 65
sudo update-alternatives --config g++

sudo apt install gcc-14 g++-14
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-14 70
sudo update-alternatives --config gcc
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-14 70
sudo update-alternatives --config g++